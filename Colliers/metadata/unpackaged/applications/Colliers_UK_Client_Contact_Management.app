<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Colliers UK - Client Contact Management</label>
    <logo>Colliers_Logo/Colliers_Logo_SFDC_gif.gif</logo>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>Activities</tab>
    <tab>Feedback__c</tab>
    <tab>Support__c</tab>
    <tab>standard-Campaign</tab>
    <tab>standard-Chatter</tab>
    <tab>Change_Control__c</tab>
    <tab>External_Inspection__c</tab>
    <tab>Outstanding_Task__c</tab>
    <tab>Account_Plan__c</tab>
    <tab>Floor_Unit__c</tab>
    <tab>Jobs_Suite_Unit_Affiliation__c</tab>
    <tab>Company_Lease_Deal_Affiliation__c</tab>
    <tab>Company_Suite_Unit_Affiliation__c</tab>
    <tab>Contact_Lease_Deal_Affiliation__c</tab>
</CustomApplication>
