trigger cs_forecastTrigger on Forecasting__c (before insert,before update) {
    if(checkRecursiveForecast.runOnce()){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
               // cs_forecastTriggerHandler.insertForecastAmount(Trigger.new);    
                cs_forecastTriggerHandler.beforeinsert(Trigger.new);    
                checkRecursiveForecast.run = false;
               
            }
            if(Trigger.isUpdate){
                cs_forecastTriggerHandler.updateForecastAmount(Trigger.oldMap,Trigger.newMap);    
                cs_forecastTriggerHandler.beforeupdate(Trigger.NewMap,Trigger.OldMap);     
                checkRecursiveForecast.run = false;             
                    
            }
        }
    }
}