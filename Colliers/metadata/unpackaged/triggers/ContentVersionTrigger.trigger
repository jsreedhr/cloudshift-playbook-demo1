/**
*  Class Name: ContentVersionTrigger
*  Description: This is a Trigger for  ContentVersion.
 *  @Company:       dQuotient
 *  CreatedDate:    27/11/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Swetha	   
 */
trigger ContentVersionTrigger on ContentVersion (before insert) {

 if(Trigger.isBefore)
   {
       //To replace the special char in file name being posted/uploaded.
      ContentVersionTriggerFunction.RemoveSpecialCharFromFileName(trigger.new);
   }
}