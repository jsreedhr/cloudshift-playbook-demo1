/**
*  Trigger Name: SharingRuleTrigger
*  Description: Trigger for processing all events related to Sharing Rules (custom obkject)

*  Company: CloudShift
*  CreatedDate: 28/06/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  ----------------------------------------------------------- 
*  Kaspars Rezgalis		28/06/2018					Created
*
*/
trigger SharingRuleTrigger on Sharing_Rule__c (before insert, before update, after insert, after update, after delete, after undelete){
	Map<Id, Sharing_Rule__c>evalCustomLogicSharingRules = new Map<Id, Sharing_Rule__c>();

	if(Trigger.isAfter){
		if(Trigger.isUndelete || Trigger.isUpdate){
			
			for(Sharing_Rule__c rule : Trigger.new){
				//ensure that rules with custom sharing logic are correct (Custom logic criteria contains all and only relevant data and that sharing conditions indexes are correct)
				if(rule.Logic__c == SharingEngine.SHARING_LOGIC_CUSTOM && rule.Change_Status__c==SharingEngine.SHARING_RULE_CHANGE_STATUS_COMPLETED){
					evalCustomLogicSharingRules.put(rule.Id, rule);
				}
			}
		}
	}

	if(!evalCustomLogicSharingRules.isEmpty()){
		Map<Id, List<String>>rulesWithErrors = SharingRuleTriggerHandler.findInvalidCustomLogicRules(evalCustomLogicSharingRules);

		for(Id ruleId : rulesWithErrors.KeySet()){
			Sharing_Rule__c ruleWithError = evalCustomLogicSharingRules.get(ruleId);
			for(String errorMsg : rulesWithErrors.get(ruleId)){
				ruleWithError.addError(errorMsg);
			}
		}
	}
}