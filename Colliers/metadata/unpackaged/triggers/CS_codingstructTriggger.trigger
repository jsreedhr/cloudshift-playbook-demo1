trigger CS_codingstructTriggger on Coding_Structure__c (before Insert, before update, after update) {
   if(checkRecursiveCodingStructure.runOnce()){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                cs_codingStructureTriggerHandler.insertCompanyCostCentre(Trigger.new);
                cs_codingStructureTriggerHandler.insertcodingStructure(Trigger.new);
                checkRecursiveCodingStructure.run = true;
            }
            if(Trigger.isUpdate){
                cs_codingStructureTriggerHandler.updateCompanyCostCentre(Trigger.oldMap,Trigger.newMap);
            }
        }
        if(Trigger.isAfter){
              if(Trigger.isUpdate){
                cs_codingStructureTriggerHandler.updateAllocation(Trigger.oldMap,Trigger.newMap);
                checkRecursiveCodingStructure.run = true;
            }
            
        }
    }

}