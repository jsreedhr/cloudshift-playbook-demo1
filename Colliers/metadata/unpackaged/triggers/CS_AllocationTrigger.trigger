trigger CS_AllocationTrigger on Allocation__c (before insert,
	before update,after insert,after update,after delete,after undelete) {
	if(checkRecursiveAllocation.runOnce()){
		 allocationTriggerHandler cnst= new allocationTriggerHandler();
		 if(Trigger.isBefore){
			if(Trigger.isInsert){
				cnst.populateCodingStructure(Trigger.New);
				cnst.checkUniqueCodingStructure(Trigger.New);
				// uniquestringHelperclass.oninsert(trigger.new);
			//	CurrencyDefaulter.copyParentCurrencyCodes(Trigger.new);
			}
			if(Trigger.isUpdate){
				cnst.populateCodingStructure(Trigger.New);
				cnst.checkUniqueCodingStrucute(Trigger.oldMap, Trigger.NewMap);
			}
		   
		 }
		 
		 if(Trigger.isAfter){
			if(Trigger.isInsert){
				cnst.checkMLR(Trigger.newMap);
			}
			if(Trigger.isUpdate){ 
				cnst.updateAllocationComplete(Trigger.oldMap, Trigger.NewMap);
			}
			if(Trigger.isInsert || Trigger.isUndelete || Trigger.isUpdate)
				cnst.populateFieldsOnOpp(Trigger.New);
				if(ConfigurationManager.getInstance().isSharingOn() && SharingEngine.checkRecursiveRunOK){
					SharingEngine cs = new SharingEngine();
					cs.updateSharingForSObjects(Trigger.New);
				}
			if(Trigger.isDelete){
				cnst.populateFieldsOnOpp(Trigger.Old);
				//checkRecursiveAllocation.run = false;
			}
		 }
	}
}