trigger CS_AccuralTrigger on Accrual__c(before insert,before update,After insert ,After update) {
    
    if(checkRecursiveAccural.runOnce()){
        CS_AccuralTriggerHandler objAccuralTriggerHandler = new CS_AccuralTriggerHandler();
        
        if(Trigger.isAfter){
            //After Update
            if(Trigger.isUpdate){
                
                CS_AccuralTriggerHandler.updateReverseAccuralCreation(Trigger.oldMap, Trigger.newMap);
                CS_AccuralTriggerHandler.updateAccuralStatus(Trigger.oldMap, Trigger.newMap);
                CS_AccuralTriggerHandler.updateApprovalProcess(Trigger.oldMap, Trigger.newMap);
                checkRecursiveAccural.run = false;
            }
            //After Insert
            if(Trigger.isInsert){
                CS_AccuralTriggerHandler.insertcodingStructure(Trigger.new);
                CS_AccuralTriggerHandler.insertReverseAccuralCreation(Trigger.new);
                CS_AccuralTriggerHandler.insertAccuralStatus(Trigger.new);
                CS_AccuralTriggerHandler.createCustomSetting(Trigger.new);
                checkRecursiveAccural.run = false;
            }
        }
    }
}