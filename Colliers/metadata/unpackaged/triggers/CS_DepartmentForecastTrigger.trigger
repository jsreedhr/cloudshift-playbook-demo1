trigger CS_DepartmentForecastTrigger on Department_Forecast__c(before insert,before update,After insert ,After update) {
    
        
    if(Trigger.isBefore){
        //After Update
        if(Trigger.isUpdate){
            CS_DepartmentForecastTriggerHandler.updateCompanyCostCentre(Trigger.oldMap, Trigger.newMap);
        }
        //After Insert
        if(Trigger.isInsert){
            CS_DepartmentForecastTriggerHandler.insertCompanyCostCentre(Trigger.new);
        }
    }
}