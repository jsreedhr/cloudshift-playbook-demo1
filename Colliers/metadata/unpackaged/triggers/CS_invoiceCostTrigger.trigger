trigger CS_invoiceCostTrigger  on Invoice_Cost_Junction__c (before insert,before update) {

      if(checkRecursiveInvoiceCost.runOnce()){
        
        if(Trigger.isBefore){
            
            if(Trigger.isInsert){
                CS_CostInvoiceTriggerHandler.setStatus(Trigger.new);
                 checkRecursiveInvoiceCost.run = false;
            }
            
            if(Trigger.isUpdate){
                CS_CostInvoiceTriggerHandler.setStatus(Trigger.new);
                checkRecursiveInvoiceCost.run = false;
            }
        }
    }
}