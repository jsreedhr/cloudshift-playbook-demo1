trigger MLRTrigger on MLR__c (after update) {
    if(Trigger.isUpdate && Trigger.isAfter){
		MLRTriggerHandler.updateValidMLR(Trigger.new,Trigger.oldMap);
    }

}