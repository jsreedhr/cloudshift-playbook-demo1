trigger CS_CreditNoteTrigger on Credit_Note__c(before insert,before update,After insert ,After update) {
     
    if(checkRecursiveCreditNote.runOnce()){
        if(Trigger.isBefore){
                
            // Update Trigger
            if(Trigger.isUpdate){
                //CS_CreditNoteTriggerHandler.updateInvoiceStatus(Trigger.new);   
                CS_CreditNoteTriggerHandler.updateRejectedReason(Trigger.oldMap, Trigger.newMap);
            } 
            
               // insert Trigger
            if(Trigger.isInsert){
                CS_CreditNoteTriggerHandler.insertRejectedReason(Trigger.new);
            }
        }
            
        system.debug('Trigger.new--->'+Trigger.new);
        
        if(Trigger.isAfter){
            
            //After Update
            if(Trigger.isUpdate){
               
                //CS_CreditNoteTriggerHandler.updateInvoiceStatus(Trigger.new);
                CS_CreditNoteTriggerHandler.updateInvoiceStatus(Trigger.new);
                if(!CS_CreditNoteTriggerHandler.isInsrt){
                    CS_CreditNoteTriggerHandler.insertInvoiceClone(Trigger.new);
                }
                checkRecursiveCreditNote.run = false;
            }
            //After Insert
            if(Trigger.isInsert){
                CS_CreditNoteTriggerHandler.submitCreditNoteForApproval(Trigger.new);
                checkRecursiveCreditNote.run = false;
            }
        }
    }
}