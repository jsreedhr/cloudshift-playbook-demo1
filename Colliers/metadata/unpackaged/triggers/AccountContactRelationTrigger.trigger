trigger AccountContactRelationTrigger on AccountContactRelation (after insert) {
    if(Trigger.isInsert && Trigger.isAfter){
        if(AccountContactRelationTriggerHandler.isFirstRun()){
            AccountContactRelationTriggerHandler.insertContactUltimateParentRelation(Trigger.new);
            
        }
    }

}