trigger CS_WhoKnowsWhoTrigger on Who_Knows_Who__c(After insert ,After update, After delete, After undelete) {
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            CS_WhoKnowsWhoTriggerHandler.updateFollowContactForUser(trigger.oldMap, trigger.newMap);
            }
        //After Insert
        if(Trigger.isInsert){
            CS_WhoKnowsWhoTriggerHandler.insertFollowContactForUser(Trigger.new);
        }

        if(Trigger.isDelete){
            CS_WhoKnowsWhoTriggerHandler.deleteFollowContactForUser(Trigger.oldMap);
        }
        if(Trigger.isUndelete)
            CS_WhoKnowsWhoTriggerHandler.undeleteFollowContactForUser(Trigger.new);
    }
}