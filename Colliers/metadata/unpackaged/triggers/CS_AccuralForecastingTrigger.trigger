trigger CS_AccuralForecastingTrigger on Accrual_Forecasting_Junction__c(before insert, before update) {
    
    if(checkRecursiveAccuralForecasting.runOnce()){
        
        if(Trigger.isBefore){
            //After Insert
            if(Trigger.isInsert){
                CS_AccuralForecastingTriggerHandler.updateAccuralStatus(Trigger.new);
                checkRecursiveAccuralForecasting.run = false;
            }
            
            if(Trigger.isUpdate){
                CS_AccuralForecastingTriggerHandler.updateAccuralStatus(Trigger.new);
                checkRecursiveAccuralForecasting.run = false;
            }
        }
    }
}