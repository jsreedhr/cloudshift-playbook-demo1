trigger cs_staffTrigger on Staff__c (before Insert, before update) {
    // if(cs_staffTriggerHandler.runOnce()){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                cs_staffTriggerHandler.insertCompanyCostCentre(Trigger.new);
            }
            if(Trigger.isUpdate){
                cs_staffTriggerHandler.updateStaffStatus(Trigger.oldMap,Trigger.newMap);
                cs_staffTriggerHandler.updateCompanyCostCentre(Trigger.oldMap,Trigger.newMap);
                
            }
        }
    // }
}