trigger CS_InvoiceForecastingTrigger on Invoice_Allocation_Junction__c(before insert, before update) {
    
    if(checkRecursiveInvoiceForecasting.runOnce()){
        
        if(Trigger.isBefore){
            //After Insert
            if(Trigger.isInsert){
                CS_InvoiceForecastingTriggerHandler.updateInvoiceStatus(Trigger.new);
                checkRecursiveInvoiceForecasting.run = false;
            }
            
            if(Trigger.isUpdate){
                CS_InvoiceForecastingTriggerHandler.updateInvoiceStatus(Trigger.new);
                checkRecursiveInvoiceForecasting.run = false;
            }
        }
    }
}