trigger CS_LineItemTrigger on Invoice_Line_Item__c(before insert,before update) {
    
        
    if(Trigger.isBefore){
        //After Update
        if(Trigger.isUpdate){
            CS_LineItemTriggerHandler.updateVATAmount(Trigger.oldMap, Trigger.newMap);
        }
        //After Insert
        if(Trigger.isInsert){
            CS_LineItemTriggerHandler.calculateVATAmount(Trigger.new);
        }
    }
}