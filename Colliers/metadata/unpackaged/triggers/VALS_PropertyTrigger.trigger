trigger VALS_PropertyTrigger on Property__c 
( after Update) 
{
	if(Trigger.isAfter && Trigger.isUpdate)
	{
		List<ID> propertyIdList = New List<ID>();
		List<Job_Property_Junction__c> jobProperyList = New List<Job_Property_Junction__c>();

		for(Property__c propObj : Trigger.New)
			propertyIdList.add(propObj.ID);

		jobProperyList = [Select id,Job__c, Property__c,Sent_to_ShareDo__c
						  from Job_Property_Junction__c
						  where Property__c IN :propertyIdList
						  AND Property__c!=NULL
						  AND Job__c!=NULL LIMIT 10];

		if(jobProperyList.size()>0)
		{
			Integer count = 0;
			for(Job_Property_Junction__c jobPropObj : jobProperyList)
			{
				if(jobPropObj.Job__c!=NULL && jobPropObj.Property__c!=NULL && jobPropObj.Sent_to_ShareDo__c==false && count<=10)
				{
					VALS_ComparableJobProcessor comparablesCallout = New VALS_ComparableJobProcessor(jobPropObj);
					count+=1;
				}
			}
		}
	}
}