trigger ExperianDataQuality_Lead_AIAU on Lead (after insert, after update) {
    EDQ.DataQualityService.ExecuteWebToObject(Trigger.New, 2, Trigger.IsUpdate);
}