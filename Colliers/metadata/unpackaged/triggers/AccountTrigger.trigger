trigger AccountTrigger on Account (after update) {
    if(Trigger.isUpdate && Trigger.isAfter){
        if(AccountTriggerHandler.isFirstRun() ){
          
            AccountTriggerHandler.afterUpdateMethod(Trigger.new,Trigger.oldMap,Trigger.newMap);
            AccountTriggerHandler.isFirstRun();
        }
    }
}