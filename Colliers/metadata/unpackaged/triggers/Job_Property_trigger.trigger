trigger Job_Property_trigger on  Job_Property_Junction__c(after insert,after delete,after undelete){
    // call the function 
    job_property_triggerHandler construct = new job_property_triggerHandler();
    if(trigger.isAfter){
        if(trigger.isInsert){
            construct.afterinsertfunction(Trigger.new);
        }
        if(trigger.isUndelete){
            construct.afterinsertfunction(Trigger.old);
        }
        if(trigger.isdelete){
            construct.afterdeletefunction(Trigger.old);
        }
    }
}