/**
*  Trigger Name: OpportunityTrigger
*  Description: Trigger for processing all events related to Opportunities

*  Company: CloudShift
*  CreatedDate: 30/05/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  ----------------------------------------------------------- 
*  Kaspars Rezgalis		30/05/2018					Created; refactored opportunityAllocTrigger into this trigger
*
*/
trigger OpportunityTrigger on Opportunity (before insert, before update, after insert, after update, after delete, after undelete){
	opportunityAllocTriggerHandler construct = new opportunityAllocTriggerHandler();

	if(Trigger.isAfter){
		if(opportunityAllocTriggerHandler.checkrecursiveopp){
			if(Trigger.isInsert){
				construct.insertfunction(Trigger.newMap);   
			}

			if(Trigger.isUpdate){
				construct.updatefunction(Trigger.oldMap,Trigger.newMap);  
				construct.updateinvoicingAddress(Trigger.oldMap,Trigger.newMap);   
				construct.updateFinalInvoice(Trigger.oldMap,Trigger.newMap);   
			}

			opportunityAllocTriggerHandler.checkrecursiveopp = false;
		}
			
		if(Trigger.isInsert || Trigger.isUndelete || Trigger.isUpdate){
			if(ConfigurationManager.getInstance().isSharingOn()	&& SharingEngine.checkRecursiveRunOK){
				SharingEngine cs = new SharingEngine();
				cs.updateSharingForSObjects(Trigger.New);
			}
		}

	}else if(Trigger.isBefore){ 
		if(opportunityAllocTriggerHandler.checkrecursiveopp){
			construct.UpdateOppOWner(Trigger.isInsert?new Map<id, Opportunity>():Trigger.Oldmap,Trigger.newmap);
			construct.updateaccoutID(Trigger.new);
			
			if(Trigger.isUpdate){
				construct.checkMLR(Trigger.newMap);
			}
		}
	}
}