trigger CS_InvoiceTrigger on Invoice__c(before update, After insert ,After update) {
    if(checkRecursiveInvoice.runOnce()){
        CS_InvoiceTriggerHandler objInvoiceTriggerHandler = new CS_InvoiceTriggerHandler();
        if(Trigger.isAfter){
        //After Update
            if(Trigger.isUpdate){
                  
                CS_InvoiceTriggerHandler.updatePurchaseAmount(Trigger.oldMap, Trigger.newMap);
                CS_InvoiceTriggerHandler.onApproveinvoice(trigger.new,trigger.oldMap);
                CS_InvoiceTriggerHandler.updateInvoiceStatus(Trigger.oldMap, Trigger.newMap);
                CS_InvoiceTriggerHandler.createCustomSetting(trigger.new,trigger.oldMap);
              
                CS_InvoiceTriggerHandler.oncancelinvoice(trigger.oldMap, trigger.newMap);
                checkRecursiveInvoice.run = false;
                }
            //After Insert
            if(Trigger.isInsert){
             
                CS_InvoiceTriggerHandler.insertPurchaseAmount(Trigger.new);
                CS_InvoiceTriggerHandler.onfinalinvoice(trigger.new);
                CS_InvoiceTriggerHandler.insertnvoiceStatus(Trigger.new);
               
                checkRecursiveInvoice.run = false;
            }
        }
        if(Trigger.isBefore){
            if(Trigger.isUpdate){
                CS_InvoiceTriggerHandler.updateinvoicepdfvalue(Trigger.oldMap, Trigger.newMap);
                CS_InvoiceTriggerHandler.updateSendMLRMail(Trigger.oldMap, Trigger.newMap);
            }
            // if(Trigger.isInsert){
            //     CS_InvoiceTriggerHandler.insertInvocingCompanyAddress(Trigger.new);
            // }
        }
    }
}