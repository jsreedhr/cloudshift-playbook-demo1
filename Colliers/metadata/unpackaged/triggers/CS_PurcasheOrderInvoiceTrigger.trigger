trigger CS_PurcasheOrderInvoiceTrigger on Purchase_Order_Invoice_Junction__c(before insert, before update, after delete) {
    
    if(checkRecursivePurchaseOrderInvoice.runOnce()){
        
        if(Trigger.isBefore){
            //After Insert
            if(Trigger.isInsert){
                CS_PurchaseOrderInvoiceTriggerHandler.updateInvoiceStatus(Trigger.new);
                CS_PurchaseOrderInvoiceTriggerHandler.insertPOAmount(Trigger.new);
               // checkRecursivePurchaseOrderInvoice.run = false;
            }
            
            if(Trigger.isUpdate){
                CS_PurchaseOrderInvoiceTriggerHandler.updateInvoiceStatus(Trigger.new);
                //checkRecursivePurchaseOrderInvoice.run = false;
            }
            
            
        }
        
        if(Trigger.isAfter){
            if(Trigger.isDelete){
                CS_PurchaseOrderInvoiceTriggerHandler.deletePOAmount(Trigger.old);
                checkRecursivePurchaseOrderInvoice.run = false;
            }
            
        }
    }
}