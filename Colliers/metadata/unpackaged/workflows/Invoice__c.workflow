<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Job_Manager_Invoice_Finalized</fullName>
        <description>Notify Job Manager Invoice Finalized</description>
        <protected>false</protected>
        <recipients>
            <field>Job_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Invoice_Finalized_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Payment_charges_to_none</fullName>
        <field>Payment_Charges__c</field>
        <name>Payment charges to none</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Release_Bad_Debt_Amount</fullName>
        <field>Bad_Debt_Amount_Legacy__c</field>
        <name>Release Bad Debt Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Invoice_to_Paid_status</fullName>
        <field>Status__c</field>
        <literalValue>Paid</literalValue>
        <name>Set Invoice to Paid status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Paid_Date_to_null</fullName>
        <field>Paid_Date__c</field>
        <name>Set Paid Date to null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_invoice_status_to_Partial</fullName>
        <field>Status__c</field>
        <literalValue>Partial</literalValue>
        <name>Set invoice status to Partial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_invoice_status_to_Printed</fullName>
        <field>Status__c</field>
        <literalValue>Printed</literalValue>
        <name>Set invoice status to Printed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Id_time_stamp</fullName>
        <description>When Invoice get approve we need to populate job.AccountId in Account Id Time stamp field.</description>
        <field>Account_Id_Time_Stamp__c</field>
        <formula>Opportunity__r.Account_id__c</formula>
        <name>Update Account Id time stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_of_Invoice</fullName>
        <field>Date_of_Invoice__c</field>
        <formula>TODAY()</formula>
        <name>Update Date of Invoice</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Invoice_Job_Manager_Email</fullName>
        <field>Job_Manager_Email__c</field>
        <formula>Opportunity__r.Manager__r.Email__c</formula>
        <name>Update Invoice Job Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Paid_Date</fullName>
        <field>Paid_Date__c</field>
        <formula>Today()</formula>
        <name>Update Paid Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CS_Invoice Account Id Time Stamp</fullName>
        <actions>
            <name>Update_Account_Id_time_stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Invoice__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>To populate the Account Id value  when Invoice get approved.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS_Invoice Date Today</fullName>
        <actions>
            <name>Update_Date_of_Invoice</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To populate the Date of Invoice with the date the Assigned Invoice Number was populated</description>
        <formula>AND(PRIORVALUE(  Assigned_Invoice_Number__c ) = NULL,      NOT(ISBLANK(Assigned_Invoice_Number__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS_Invoice finalized notification</fullName>
        <actions>
            <name>Notify_Job_Manager_Invoice_Finalized</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Invoice_Job_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To send an email alert to the Job Manager when the Secretary finalizes an invoice on their behalf.</description>
        <formula>AND(  LastModifiedBy.Profile.Name = &quot;CS_UK Support&quot;,  Assigned_Invoice_Number__c &lt;&gt; &quot;&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Invoice to Paid Status</fullName>
        <actions>
            <name>Set_Invoice_to_Paid_status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Paid_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF( !ISPICKVAL(Payment_Charges__c , &apos;&apos;) &amp;&amp; (ISPICKVAL(Status__c,&apos;Approved&apos;) || ISPICKVAL(Status__c, &apos;Partial&apos;)|| ISPICKVAL(Status__c, &apos;Printed&apos;)), true , Paid_Amount__c &gt; 0 &amp;&amp; Paid_Amount__c &gt;= (Amount_Inc_VAT__c - 12)&amp;&amp; (ISPICKVAL(Status__c,&apos;Approved&apos;) || ISPICKVAL(Status__c, &apos;Partial&apos;)|| ISPICKVAL(Status__c, &apos;Printed&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Invoice to Partial Status</fullName>
        <actions>
            <name>Set_invoice_status_to_Partial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(ISPICKVAL(Payment_Charges__c, &apos;&apos;) , Paid_Amount__c &gt; 0 &amp;&amp; Paid_Amount__c &lt; (Amount_Inc_VAT__c - 12) &amp;&amp; (ISPICKVAL(Status__c,&apos;Approved&apos;) || ISPICKVAL(Status__c, &apos;Printed&apos;)), false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Invoice to Printed Status</fullName>
        <actions>
            <name>Payment_charges_to_none</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Paid_Date_to_null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_invoice_status_to_Printed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(Paid_Amount__c = 0 &amp;&amp; (ISPICKVAL(Status__c,&apos;Paid&apos;) || ISPICKVAL(Status__c, &apos;Partial&apos;)),true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
