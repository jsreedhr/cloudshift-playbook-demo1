<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Invoice_Payment_Due_Date_Field_Update</fullName>
        <description>Initial Implementation Requested by FR - 06/2016</description>
        <field>Payment_Due_Date__c</field>
        <formula>IF(

Payment_Terms_Code__c = &quot;M030&quot;,

DATE(
     YEAR( Date_of_Invoice__c),
     MONTH(Date_of_Invoice__c) + 1,
     1
) - 1

+30,

IF(

Payment_Terms_Code__c = &quot;M045&quot;,

DATE(
     YEAR( Date_of_Invoice__c),
     MONTH(Date_of_Invoice__c) + 1,
     1
) - 1

+45,

IF(
Payment_Terms_Code__c =&quot;M060&quot;,

DATE(
     YEAR( Date_of_Invoice__c),
     MONTH(Date_of_Invoice__c) + 1,
     1
) - 1

+60,

IF(
Payment_Terms_Code__c =&quot;M075&quot;,

DATE(
     YEAR( Date_of_Invoice__c),
     MONTH(Date_of_Invoice__c) + 1,
     1
) - 1

+75,

IF(
Payment_Terms_Code__c =&quot;M090&quot;,

DATE(
     YEAR( Date_of_Invoice__c),
     MONTH(Date_of_Invoice__c) + 1,
     1
) - 1

+90,

IF(
Payment_Terms_Code__c = &quot;M120&quot;,

DATE(
     YEAR( Date_of_Invoice__c),
     MONTH(Date_of_Invoice__c) + 1,
     1
) - 1

+120,

IF(
Payment_Terms_Code__c = &quot;C000&quot;,

Date_of_Invoice__c,

IF(
Payment_Terms_Code__c =&quot;I015&quot;,

Date_of_Invoice__c + 15,

IF(
Payment_Terms_Code__c =&quot;I030&quot;,

Date_of_Invoice__c + 30,

IF(
Payment_Terms_Code__c =&quot;I045&quot;,

Date_of_Invoice__c + 45,

IF(
Payment_Terms_Code__c =&quot;I060&quot;,

Date_of_Invoice__c + 60,

IF(
Payment_Terms_Code__c =&quot;I075&quot;,

Date_of_Invoice__c + 75,

IF(
Payment_Terms_Code__c =&quot;I090&quot;,

Date_of_Invoice__c + 90,

IF(
Payment_Terms_Code__c =&quot;I120&quot;,

Date_of_Invoice__c + 120,

TODAY()))))))))))))))</formula>
        <name>Invoice Payment Due Date Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>FR Invoice Due Date Field Update</fullName>
        <actions>
            <name>Invoice_Payment_Due_Date_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Invoice_France__c.Payment_Terms_Code__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Invoice_France__c.Date_of_Invoice__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Initial Implementation Requested by FR - 06/2016</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
