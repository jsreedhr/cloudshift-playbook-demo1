<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_RV_with_Hereditament_RV</fullName>
        <field>X2010_RV__c</field>
        <formula>Hereditament__r.X2010_RV__c</formula>
        <name>Update RV with Hereditament RV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X2017_RV_update</fullName>
        <field>X2017_RV__c</field>
        <formula>Hereditament__r.X2017_RV__c</formula>
        <name>2017 RV update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update RV with Hereditament RV</fullName>
        <actions>
            <name>Update_RV_with_Hereditament_RV</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>X2017_RV_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Hereditament_Affiliation__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Requested by UK Rating</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
