<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FR_Opportunity_Service_Line_is_Contract_Signed</fullName>
        <ccEmails>frederique.menier@colliers.com</ccEmails>
        <ccEmails>Ghislain.Devron@colliers.com</ccEmails>
        <ccEmails>sabrina.palmero@colliers.com</ccEmails>
        <ccEmails>corentin.liorzou@colliers.com</ccEmails>
        <description>FR Opportunity Service Line is Contract Signed</description>
        <protected>false</protected>
        <recipients>
            <recipient>richard.judd@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FR/FR_Opportunity_Service_Line_Contract_Signed</template>
    </alerts>
    <alerts>
        <fullName>FR_Opportunity_Service_Line_is_Contract_Signed_ALL_USERS</fullName>
        <ccEmails>frederique.menier@colliers.com</ccEmails>
        <ccEmails>corentin.liorzou@colliers.com</ccEmails>
        <ccEmails>Ghislain.Devron@colliers.com</ccEmails>
        <ccEmails>sabrina.palmero@colliers.com</ccEmails>
        <ccEmails>Alexandre.Fraigneau@colliers.com</ccEmails>
        <description>FR Opportunity Service Line is Contract Signed ALL USERS</description>
        <protected>false</protected>
        <recipients>
            <recipient>FR_Management</recipient>
            <type>roleSubordinatesInternal</type>
        </recipients>
        <recipients>
            <recipient>otto.buchinger@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FR/FR_Opportunity_Service_Line_Contract_Signed</template>
    </alerts>
    <alerts>
        <fullName>FR_Opportunity_Service_Line_is_Contract_Signed_ALL_USERS_2</fullName>
        <ccEmails>Cedric.Chaignaud@colliers.com</ccEmails>
        <ccEmails>Paul-Eric.Roubaud@colliers.com</ccEmails>
        <ccEmails>caroline.fleury@colliers.com</ccEmails>
        <ccEmails>Romain.Fremont@colliers.com</ccEmails>
        <ccEmails>Thierry.Ledieu@colliers.com</ccEmails>
        <description>FR Opportunity Service Line is Contract Signed ALL USERS 2</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>FR/FR_Opportunity_Service_Line_Contract_Signed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Closing_Date</fullName>
        <field>Close_Date__c</field>
        <formula>TODAY()</formula>
        <name>Closing Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cross_Obj_Update_Oppty_Mod_Date_with_NOW</fullName>
        <field>Modified_Date__c</field>
        <formula>NOW()</formula>
        <name>Cross Obj Update Oppty Mod Date with NOW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GM_Value</fullName>
        <field>GM__c</field>
        <formula>CASE( Service_Line__c,
&quot;Capital Market - honoraires fixes&quot;,1,
&quot;Capital Market - Success Fees&quot;,1,
&quot;FL - AMO&quot;,0.91,
&quot;FL - Architecture intérieur&quot;,0.91,
&quot;FL - Building Sales&quot;,0.95,
&quot;FL - Communication&quot;,0.91,
&quot;FL - Conduite du changement&quot;,0.91,
&quot;FL - Design &amp; Build&quot;,0.1,
&quot;FL - Foncière Logement&quot;,0.95,
&quot;FL - Gestion des Transferts&quot;,0.91,
&quot;FL - Image 3D&quot;,0.91,
&quot;FL - International&quot;,0.95,
&quot;FL - Mobilité&quot;,0.91,
&quot;FL - Organization&quot;,0.95,
&quot;FL - Project Management&quot;,0.91,
&quot;FL - Retail&quot;,0.95,
&quot;FL - Space-planning&quot;,0.91,
&quot;FL - Starter Job&quot;,0.95,
&quot;FL - Strategy&quot;,0.95,
&quot;FL - Success Fees RES (Lyon)&quot;,1,
&quot;FL - Tenant Rep&quot;,0.95,
&quot;FL - Travaux Maïtrise d&apos;œuvre&quot;,0.91,
&quot;FL - Workplace Consulting&quot;,0.95,
&quot;FR - AMO&quot;,0.91,
&quot;FR - Architecture intérieur&quot;,0.91,
&quot;FR - Building Sales&quot;,0.95,
&quot;FR - Communication&quot;,0.91,
&quot;FR - Conduite du changement&quot;,0.91,
&quot;FR - CREM&quot;,0.95,
&quot;FR - Design &amp; Build&quot;,0.1,
&quot;FR - Facilities Management&quot;,0.9,
&quot;FR - Foncière Logement&quot;,0.95,
&quot;FR - Gestion des Transferts&quot;,0.91,
&quot;FR - Image 3D&quot;,0.91,
&quot;FR - International&quot;,0.95,
&quot;FR - MAP&quot;,0.91,
&quot;FR - Mobilité&quot;,0.91,
&quot;FR - Organization&quot;,0.95,
&quot;FR - Project Management&quot;,0.91,
&quot;FR - Retail&quot;,0.95,
&quot;FR - Space-planning&quot;,0.91,
&quot;FR - Starter Job&quot;,0.95,
&quot;FR - Strategy&quot;,0.95,
&quot;FR - Success Fees PM&quot;,1,
&quot;FR - Success Fees RES&quot;,1,
&quot;FR - Success Fees Retail&quot;,1,
&quot;FR - Tenant Rep&quot;,0.95,
&quot;FR - Travaux Maïtrise d&apos;œuvre&quot;,0.91,
&quot;FR - Workplace Consulting&quot;,0.95,
&quot;TO - AMO&quot;,0.91,
&quot;TO - Architecture Intérieur&quot;,0.91,
&quot;TO - Building Sales&quot;,0.95,
&quot;TO - Design &amp; Build&quot;,0.1,
&quot;TO - Image 3D&quot;,0.91,
&quot;TO - Project Management&quot;,0.91,
&quot;TO - Space Planning&quot;,0.91,
&quot;TO - Strategy&quot;,0.95,
&quot;TO - Success Fees PM&quot;,1,
&quot;TO - Success Fees RES&quot;,1,
&quot;TO - Tenant Rep&quot;,0.95,
&quot;TO - Travaux Maîtrise d&apos;Oeuvre&quot;,0.91,
&quot;TO - Worplace Consulting&quot;,0.95,
&quot;Conception design&quot;,0.91,
&quot;Space planning / Charte / analyse architecturale&quot;,0.91,
&quot;Accompagnement du changement&quot;,0.95,
&quot;Communication&quot;,0.95,
&quot;Cadrage stratégique&quot;,0.95,
&quot;Vente investisseur&quot;,0.91,
&quot;Acquisition investisseur&quot;,0.91,
&quot;Capital advisory&quot;,0.95,
&quot;CREMC&quot;,0.95,
&quot;Schéma directeur&quot;,0.95,
&quot;Transfert&quot;,0.91,
&quot;Mission Développement Durable&quot;,0.91,
&quot;Etude de Faisabilité&quot;,0.91,
&quot;Audits/Due Dil Technique et Fonctionnel&quot;,0.91,
&quot;AMO Construction Proprietaire - Investisseur&quot;,0.95,
&quot;AMO Construction Utilisateur&quot;,0.95,
&quot;MOD&quot;,0.075,
&quot;Etude de Faisabilité&quot;,0.91,
&quot;Audits/Due Dil Technique et Fonctionnel&quot;,0.91,
&quot;Workplace Efficiency&quot;,0.1,
&quot;PMO / Gestion de projet&quot;,0.95,
&quot;Design &amp; Build / Contractant Général&quot;,0.1,
&quot;Maitrise d&apos;œuvre de conception&quot;,0.91,
&quot;Maitrise d&apos;œuvre d&apos;exécution&quot;,0.91,
&quot;Transfert&quot;,0.91,
&quot;AMO Aménagement Utilisateur&quot;,0.95,
&quot;AMO Construction Utilisateur&quot;,0.95,
&quot;Schéma directeur&quot;,0.95,
&quot;Transaction locative&quot;,0.95,
&quot;Désengagement&quot;,0.95,
&quot;Acquisition utilisateur&quot;,0.91,
&quot;Cession utilisateur&quot;,0.91,
&quot;Autre missions de conseil RES&quot;,0.95,
&quot;Central desk&quot;,0.95,
&quot;Crossing data&quot;,0.95,
&quot;Valuation - Full report&quot;,0.95,
&quot;Valuation - Desktop valuation&quot;,0.95,
&quot;Valuation - Report&quot;,0.95,
&quot;Valuation - Advisory services&quot;,0.95,
&quot;Crossing data&quot;,0.95,
&quot;Space planning / Charte / analyse architecturale&quot;,0.91,
&quot;Cadrage stratégique&quot;,0.95,
&quot;Audits/Etudes/Enquêtes des modes de travail&quot;,0.95,
&quot;R&amp;D / observatoire&quot;,0.95,
&quot;Central desk&quot;,0.95,
0)</formula>
        <name>GM% Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Opp_Service_Line_Name</fullName>
        <field>Service_Line_Name__c</field>
        <formula>Opportunity_Name__c &amp; &quot;-&quot; &amp;  TEXT(Service_Line__c)</formula>
        <name>Stamp Opp Service Line Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Opp_Service_Line_Name_with_SAPName</fullName>
        <field>Service_Line_Name__c</field>
        <formula>LEFT(Company__c,10) &amp; &quot; - &quot; &amp; 

CASE(Service_Line__c, 
&quot;Accompagnement du changement&quot;,&quot;CHANG&quot;,
&quot;Acquisition investisseur&quot;,&quot;ACQ-I&quot;,
&quot;Acquisition utilisateur&quot;,&quot;ACQ-U&quot;,
&quot;AMO Aménagement Utilisateur&quot;,&quot;AMO-A&quot;,
&quot;AMO Construction Proprietaire - Investisseur&quot;,&quot;AMO-I&quot;,
&quot;AMO Construction Utilisateur&quot;,&quot;AMO-U&quot;,
&quot;Audits/Due Dil Technique et Fonctionnel&quot;,&quot;ATF&quot;,
&quot;Audits/Etudes/Enquêtes des modes de travail&quot;,&quot;AMT&quot;,
&quot;Autre missions de conseil RES&quot;,&quot;RES-C&quot;,
&quot;Cadrage stratégique&quot;,&quot;CSTRA&quot;,
&quot;Capital advisory&quot;,&quot;CMT-C&quot;,
&quot;Central desk&quot;,&quot;CD&quot;,
&quot;Cession utilisateur&quot;,&quot;VEN-U&quot;,
&quot;Communication&quot;,&quot;COM&quot;,
&quot;Conception design&quot;,&quot;CONC&quot;,
&quot;CREMC&quot;,&quot;CREMC&quot;,
&quot;Crossing data&quot;,&quot;XDATA&quot;,
&quot;Désengagement&quot;,&quot;DESG&quot;,
&quot;Design &amp; Build / Contractant Général&quot;,&quot;CG&quot;,
&quot;Etude de Faisabilité&quot;,&quot;FAISA&quot;,
&quot;Maitrise d&apos;œuvre de conception&quot;,&quot;MOE-C&quot;,
&quot;Maitrise d&apos;œuvre d&apos;exécution&quot;,&quot;MOE-E&quot;,
&quot;Mission Développement Durable&quot;,&quot;DD&quot;,
&quot;MOD&quot;,&quot;MOD&quot;,
&quot;PMO / Gestion de projet&quot;,&quot;PMO&quot;,
&quot;R&amp;D / observatoire &quot;,&quot;OBSER&quot;,
&quot;Schéma directeur&quot;,&quot;SDI&quot;,
&quot;Space planning / Charte / analyse architecturale&quot;,&quot;CH-SP&quot;,
&quot;Transaction locative&quot;,&quot;TREP&quot;,
&quot;Transfert&quot;,&quot;TRSF&quot;,
&quot;Valuation - Advisory services&quot;,&quot;VAL-C&quot;,
&quot;Valuation - Desktop valuation&quot;,&quot;VAL-1&quot;,
&quot;Valuation - Full report&quot;,&quot;VAL-3&quot;,
&quot;Valuation - Report&quot;,&quot;VAL-2&quot;,
&quot;Vente investisseur&quot;,&quot;VEN-I&quot;,
&quot;Workplace Efficiency&quot;,&quot;WPE&quot;,
TEXT(Service_Line__c))

&amp; &quot; &quot; &amp; LEFT(City_Building_Flr_Name_of_Project__c,8) &amp; &quot; &quot; &amp; TEXT(Area_in_m2__c)</formula>
        <name>Stamp Opp Service Line Name with SAPName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Probability_0</fullName>
        <description>Initial Implementation Requested by FR - 06/2016</description>
        <field>Probability__c</field>
        <literalValue>0</literalValue>
        <name>Stamp Probability 0%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Probability_100</fullName>
        <description>Initial Implementation Requested by FR - 06/2016</description>
        <field>Probability__c</field>
        <literalValue>100</literalValue>
        <name>Stamp Probability 100%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Today_s_Date</fullName>
        <field>Close_Date__c</field>
        <formula>TODAY()</formula>
        <name>Today&apos;s Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Job_Close_Date</fullName>
        <field>CloseDate</field>
        <formula>Close_Date__c</formula>
        <name>Update Job Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_Line_RT_to_Accurates</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Accurates</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Service Line RT to Accurates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_Line_RT_to_Rating</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Rating</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Service Line RT to Rating</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BEL - Stamp Opp Service Line Name</fullName>
        <actions>
            <name>Stamp_Opp_Service_Line_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>BEL</value>
        </criteriaItems>
        <description>Initial implementation for Belgium 02/03/18</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BEL Stamp Close Date on Signed%2FDropped%2FLost</fullName>
        <actions>
            <name>Today_s_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>BEL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Service_Line_Stage__c</field>
            <operation>equals</operation>
            <value>Contract Signed,Dropped,Lost</value>
        </criteriaItems>
        <description>Initial Implementation Requested by BEL- 01/2018</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BEL Stamp Probability 0%25</fullName>
        <actions>
            <name>Stamp_Probability_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>BEL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Service_Line_Stage__c</field>
            <operation>equals</operation>
            <value>Dropped,Lost</value>
        </criteriaItems>
        <description>Initial Implementation Requested by BEL - 01/2018
System Field Update - Stamp 0% in Service Line Probability Field on &apos;Lost&apos; Stages</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BEL Stamp Probability 100%25</fullName>
        <actions>
            <name>Stamp_Probability_100</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>BEL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Service_Line_Stage__c</field>
            <operation>equals</operation>
            <value>Won,Contract Signed</value>
        </criteriaItems>
        <description>Initial Implementation Requested by BEL - 01/2018
System Field Update - Stamp 100% in Service Line Probability Field on &apos;Won&apos; Stages</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BEL Update Oppty Modified Date when OSL modified</fullName>
        <actions>
            <name>Cross_Obj_Update_Oppty_Mod_Date_with_NOW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Initial Implementation Requested by BEL - 01/2018</description>
        <formula>AND(  $User.ProfileId&lt;&gt;&quot;00e24000000rgcx&quot;,  RecordTypeId=&quot;0120E0000006lSd&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR - Stamp Rating Manager into Opportunity Manager</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <description>Requested by FR 02/03/18 - so rating manager=opp manager</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR Closing Date</fullName>
        <actions>
            <name>Closing_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Service_Line_Stage__c</field>
            <operation>equals</operation>
            <value>Contract Signed,Mandate / Mission Signed</value>
        </criteriaItems>
        <description>Update the field Closed date when the Opportunity Service Line is closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FR Opportunity Service Line is Contract Signed</fullName>
        <actions>
            <name>FR_Opportunity_Service_Line_is_Contract_Signed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Service_Line_Stage__c</field>
            <operation>equals</operation>
            <value>Contract Signed</value>
        </criteriaItems>
        <description>Initial Implementation Requested by FR - 06/2016
Email to SAP Administrator</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FR Opportunity Service Line is Contract Signed - ALL USERS</fullName>
        <actions>
            <name>FR_Opportunity_Service_Line_is_Contract_Signed_ALL_USERS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FR_Opportunity_Service_Line_is_Contract_Signed_ALL_USERS_2</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Service_Line_Stage__c</field>
            <operation>equals</operation>
            <value>Contract Signed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Business_Line__c</field>
            <operation>notEqual</operation>
            <value>Capital Market,Valuation</value>
        </criteriaItems>
        <description>Initial Implementation Requested by FR - 06/2016
Email to All Users including SAP Admins</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FR Opportunity Service Line is Contract Signed - VALS%2C AM%2C CM</fullName>
        <actions>
            <name>FR_Opportunity_Service_Line_is_Contract_Signed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Service_Line_Stage__c</field>
            <operation>equals</operation>
            <value>Contract Signed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Business_Line__c</field>
            <operation>equals</operation>
            <value>Valuation,Capital Market</value>
        </criteriaItems>
        <description>Initial Implementation Requested by FR - 06/2016
Business Line equals Capital Markets, Valuation, Asset Management: Email to SAP Administrator</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FR Stamp Close Date on Signed%2FDropped%2FLost</fullName>
        <actions>
            <name>Today_s_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Service_Line_Stage__c</field>
            <operation>equals</operation>
            <value>Contract Signed,Dropped,Lost</value>
        </criteriaItems>
        <description>Initial Implementation Requested by FR - 06/2016</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR Stamp GM%25 value with Formula value</fullName>
        <actions>
            <name>GM_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <description>Initial Implementation Requested by FR - 06/2016</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR Stamp Opp Service Line Name with SAP Name</fullName>
        <actions>
            <name>Stamp_Opp_Service_Line_Name_with_SAPName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <description>Initial Implementation Requested by FR - 06/2016</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR Stamp Probability 0%25</fullName>
        <actions>
            <name>Stamp_Probability_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Service_Line_Stage__c</field>
            <operation>equals</operation>
            <value>Dropped,Lost</value>
        </criteriaItems>
        <description>Initial Implementation Requested by FR - 06/2016
System Field Update - Stamp 0% in Service Line Probability Field on &apos;Lost&apos; Stages</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR Stamp Probability 100%25</fullName>
        <actions>
            <name>Stamp_Probability_100</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Service_Line_Stage__c</field>
            <operation>equals</operation>
            <value>Won,Contract Signed</value>
        </criteriaItems>
        <description>Initial Implementation Requested by FR - 06/2016
System Field Update - Stamp 100% in Service Line Probability Field on &apos;Won&apos; Stages</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR Update Oppty Modified Date when OSL modified</fullName>
        <actions>
            <name>Cross_Obj_Update_Oppty_Mod_Date_with_NOW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Initial Implementation Requested by FR - 06/2016</description>
        <formula>AND(  $User.ProfileId&lt;&gt;&quot;00e24000000rgcx&quot;,  RecordTypeId=&quot;012240000006rtu&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Close Date On Job</fullName>
        <actions>
            <name>Update_Job_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>BEL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Service_Line_Stage__c</field>
            <operation>notEqual</operation>
            <value>Contract Signed,Won,Dropped,Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Service Line RT to Accurates</fullName>
        <actions>
            <name>Update_Service_Line_RT_to_Accurates</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Enquiry_Type__c</field>
            <operation>equals</operation>
            <value>Audit,EPR,Efficiency Review,Cost Recovery</value>
        </criteriaItems>
        <description>System: Set correct RT for Service Line (UK Accurates)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Service Line RT to Rating</fullName>
        <actions>
            <name>Update_Service_Line_RT_to_Rating</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Service_Line__c.Enquiry_Type__c</field>
            <operation>equals</operation>
            <value>Rating 2010,Rating 2017,MCC,RAM</value>
        </criteriaItems>
        <description>System: Set correct RT for Service Line (UK Rating)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>NOT_VALS</fullName>
        <assignedTo>richard.judd@colliers.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Not VALS</description>
        <dueDateOffset>50</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity_Service_Line__c.Close_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Not Vals etc</subject>
    </tasks>
</Workflow>
