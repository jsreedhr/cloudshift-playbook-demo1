<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Change_Control_Product_Reject</fullName>
        <description>Change Control Product Reject</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Change_Control_Product_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Control_Technical_Stage_Update</fullName>
        <field>Stage__c</field>
        <literalValue>Technical Approval</literalValue>
        <name>Change Control - Technical Stage Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deployment_Completed</fullName>
        <field>Deployment_Completed__c</field>
        <literalValue>1</literalValue>
        <name>Deployment Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deployment_Completed_By</fullName>
        <field>Deployment_Completed_By__c</field>
        <formula>$User.FirstName &amp;&quot; &quot;&amp; $User.LastName</formula>
        <name>Deployment Completed By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deployment_Completed_Date</fullName>
        <field>Deployment_Completed_Date__c</field>
        <formula>NOW()</formula>
        <name>Deployment Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deployment_Stage_Update</fullName>
        <field>Stage__c</field>
        <literalValue>Closed</literalValue>
        <name>Deployment Stage Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Approved</fullName>
        <description>field update to check the box for Product Approved</description>
        <field>Product_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Product Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Approved_By</fullName>
        <field>Product_Approved_By__c</field>
        <formula>$User.FirstName &amp;&quot; &quot;&amp; $User.LastName</formula>
        <name>Product Approved By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Approved_Date</fullName>
        <description>date stamp of when the product approval happened</description>
        <field>Product_Approved_Date__c</field>
        <formula>NOW()</formula>
        <name>Product Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Approved_Stage_Update</fullName>
        <field>Stage__c</field>
        <literalValue>Product Approval</literalValue>
        <name>Product Approved Stage Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Technical_Approval</fullName>
        <field>Technical_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Technical Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Technical_Approval_By</fullName>
        <field>Technical_Approval_By__c</field>
        <formula>$User.FirstName &amp;&quot; &quot;&amp; $User.LastName</formula>
        <name>Technical Approval By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Technical_Approval_Date</fullName>
        <field>Technical_Approval_Date__c</field>
        <formula>NOW()</formula>
        <name>Technical Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Testing_Completed</fullName>
        <field>Testing_Completed__c</field>
        <literalValue>1</literalValue>
        <name>Testing Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Testing_Completed_By</fullName>
        <field>Testing_Completed_By__c</field>
        <formula>$User.FirstName &amp;&quot; &quot;&amp; $User.LastName</formula>
        <name>Testing Completed By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Testing_Completed_Date</fullName>
        <field>Testing_Completed_Date__c</field>
        <formula>NOW()</formula>
        <name>Testing Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Testing_Completed_Stage</fullName>
        <field>Stage__c</field>
        <literalValue>Testing</literalValue>
        <name>Testing Completed Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
