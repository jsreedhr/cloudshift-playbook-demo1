<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Forecast_amount_change_notification</fullName>
        <description>Forecast amount change notification</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_to_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Notification_of_Forecast_Amount_Change</template>
    </alerts>
    <alerts>
        <fullName>Forecasting_Notification_when_amount_changes</fullName>
        <description>Forecasting - Notification when amount changes</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Rating/Forecasting_Notification_when_amount_changes</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Forecast_Assigned_to_Email</fullName>
        <field>Assigned_to_Email__c</field>
        <formula>Allocation__r.Assigned_To__r.Email__c</formula>
        <name>Update Forecast Assigned to Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Forecast_Amount</fullName>
        <field>Previous_Forecast_Amount__c</field>
        <formula>PRIORVALUE( Amount__c )</formula>
        <name>Update Previous Forecast Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CS Forecasting Update Assigned to email</fullName>
        <actions>
            <name>Update_Forecast_Assigned_to_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Forecasting__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
