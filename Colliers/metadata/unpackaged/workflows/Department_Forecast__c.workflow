<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Department_Cost_Centre_Code_1</fullName>
        <field>Department_Cost_Centre_Code__c</field>
        <formula>CASE( 
Department__c , 
&quot;Accurates&quot;, &quot;656&quot;, 
&quot;Asset &amp; Property Management&quot;, &quot;640&quot;, 
&quot;Auctions&quot;, &quot;630&quot;, 
&quot;Automotive and Roadside&quot;, &quot;670&quot;, 
&quot;BB Building Consultancy&quot;, &quot;654&quot;, 
&quot;Bid Team&quot;, &quot;862&quot;, 
&quot;Business Projects&quot;, &quot;851&quot;, 
&quot;Business Space Lease Advisory&quot;, &quot;655&quot;, 
&quot;Business Systems&quot;, &quot;811&quot;, 
&quot;Business Systems and IT&quot;, &quot;810&quot;, 
&quot;Central London&quot;, &quot;690&quot;, 
&quot;London Offices Lease Advisory&quot;, &quot;692&quot;, 
&quot;Central London Retail&quot;, &quot;601&quot;, 
&quot;CIPC Canada&quot;, &quot;886&quot;, 
&quot;City Fringe&quot;, &quot;698&quot;, 
&quot;City Investment&quot;, &quot;691&quot;, 
&quot;City Offices&quot;, &quot;696&quot;, 
&quot;Colliers Capital UK&quot;, &quot;627&quot;, 
&quot;Colliers EMEA&quot;, &quot;780&quot;, 
&quot;Colliers Godfrey Vaughan&quot;, &quot;690&quot;, 
&quot;Colliers Godfrey Vaughan - City&quot;, &quot;610&quot;, 
&quot;Colliers International Hotels&quot;, &quot;675&quot;, 
&quot;Colliers Robert Barry&quot;, &quot;675&quot;, 
&quot;Corporate&quot;, &quot;900&quot;, 
&quot;Advisory &amp; Restructuring&quot;, &quot;638&quot;, 
&quot;Corporate Solutions&quot;, &quot;647&quot;, 
&quot;Data Centres&quot;, &quot;697&quot;, 
&quot;David Izett&apos;s Office&quot;, &quot;775&quot;, 

&quot;Deanwater Estates&quot;, &quot;770&quot;, 
&quot;Destination Consulting&quot;, &quot;637&quot;, 
&quot;Development Consulting&quot;, &quot;637&quot;, 
&quot;Development Advisory&quot;, &quot;660&quot;, 
&quot;Development Solutions&quot;, &quot;637&quot;, 
&quot;Digital Marketing&quot;, &quot;863&quot;, 
&quot;EMEA&quot;, &quot;886&quot;, 
&quot;EMEA Corporate Solutions&quot;, &quot;887&quot;, 
&quot;EMEA Head Office&quot;, &quot;885&quot;, 
&quot;EMEA IT&quot;, &quot;884&quot;, 
&quot;European Valuation&quot;, &quot;650&quot;, 
&quot;Executive Group&quot;, &quot;775&quot;, 
&quot;Facilities&quot;, &quot;840&quot;, 
&quot;Finance&quot;, &quot;800&quot;, 
&quot;Healthcare&quot;, &quot;666&quot;, 
&quot;Hotel&quot;, &quot;675&quot;, 
&quot;Hotels &amp; Resorts Consulting&quot;, &quot;678&quot;, 
&quot;Hotels Agency&quot;, &quot;676&quot;, 
&quot;Hotels Valuation&quot;, &quot;677&quot;, 
&quot;Human Resources&quot;, &quot;870&quot;, 
&quot;Industrial and Logistics&quot;, &quot;611&quot;, 
&quot;Information&quot;, &quot;850&quot;, 
&quot;Information Technology Services&quot;, &quot;810&quot;, 
&quot;International Business Development&quot;, &quot;780&quot;, 
&quot;International Development Solutions&quot;, &quot;637&quot;, 
&quot;International Properties&quot;, &quot;888&quot;, 
&quot;Investment Market Place&quot;, &quot;620&quot;, 
&quot;Investment Property Management&quot;, &quot;640&quot;, 
&quot;JR&apos;s Office&quot;, &quot;775&quot;, 
&quot;Landlord &amp; Tenant&quot;, &quot;655&quot;, 
&quot;Legal and Compliance&quot;, &quot;777&quot;, 
&quot;Leisure Management&quot;, &quot;667&quot;, 
&quot;Licensed and Leisure&quot;, &quot;664&quot;, 
&quot;Locum Consulting&quot;, &quot;628&quot;, 
&quot;Locum UK&quot;, &quot;628&quot;, 
&quot;Marketing&quot;, &quot;860&quot;, 
&quot;Metals &amp; Mining&quot;, &quot;663&quot;, 
&quot;National Capital Markets&quot;, &quot;620&quot;, 
&quot;National Offices&quot;, &quot;610&quot;, 
&quot;Nile Management&quot;, &quot;646&quot;, 
&quot;Offices - West End&quot;, &quot;612&quot;, 
&quot;Parks, Sports and Marinas&quot;, &quot;665&quot;, 
&quot;Pitching Team&quot;, &quot;862&quot;, 
&quot;Planning&quot;, &quot;658&quot;, 
&quot;Planning - Historic&quot;, &quot;658&quot;, 
&quot;Project &amp; Building Consultancy&quot;, &quot;652&quot;, 
&quot;Project Management&quot;, &quot;653&quot;, 
&quot;Property Management Accounts&quot;, &quot;645&quot;, 
&quot;Public Relations&quot;, &quot;861&quot;, 
&quot;Rating&quot;, &quot;656&quot;, 
&quot;Recreational Property&quot;, &quot;665&quot;, 
&quot;Regional Admin Support&quot;, &quot;840&quot;, 
&quot;Regional Admin Support - Birmingham&quot;, &quot;702&quot;, 
&quot;Regional Admin Support - Bristol&quot;, &quot;730&quot;, 
&quot;Regional Admin Support - City&quot;, &quot;915&quot;, 
&quot;Regional Admin Support - Edinburgh&quot;, &quot;716&quot;, 
&quot;Regional Admin Support - Glasgow&quot;, &quot;722&quot;, 
&quot;Regional Admin Support - Leeds&quot;, &quot;706&quot;, 
&quot;Regional Admin Support - Manchester&quot;, &quot;710&quot;, 
&quot;Regional Admin Support - Uxbridge&quot;, &quot;735&quot;, 
&quot;Regional Admin Support - West End&quot;, &quot;912&quot;, 
&quot;Regional IT Operations&quot;, &quot;812&quot;, 
&quot;Research and Forecasting&quot;, &quot;852&quot;, 


&quot;&quot; 
)</formula>
        <name>Update Department Cost Centre Code 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Department_Cost_Centre_Code_2</fullName>
        <field>Department_Cost_Centre_Code__c</field>
        <formula>IF( 
Department_Cost_Centre_Code__c = &quot;&quot;, 

CASE( 
Department__c , 

&quot;Residential&quot;, &quot;662&quot;, 
&quot;Residential - Development&quot;, &quot;612&quot;, 
&quot;Residential – New Homes&quot;, &quot;613&quot;, 
&quot;Retail&quot;, &quot;609&quot;, 
&quot;Retail &amp; Leisure Agency - South&quot;, &quot;623&quot;, 
&quot;Retail - Lease Advisory&quot;, &quot;622&quot;, 
&quot;Retail - Scotland&quot;, &quot;600&quot;, 
&quot;Retail – Central London Lease Advisory&quot;, &quot;621&quot;, 
&quot;Retail Agency&quot;, &quot;602&quot;, 
&quot;Retail Agency - Central London&quot;, &quot;614&quot;, 
&quot;Retail Agency - North&quot;, &quot;615&quot;, 
&quot;Retail Agency - Out of Town&quot;, &quot;617&quot;, 
&quot;Retail Agency - South&quot;, &quot;619&quot;, 
&quot;Retail Belfast&quot;, &quot;600&quot;, 
&quot;Retail Capital Markets&quot;, &quot;616&quot;, 
&quot;Retail Development&quot;, &quot;603&quot;, 
&quot;Retail Europe&quot;, &quot;606&quot;, 
&quot;Retail International&quot;, &quot;604&quot;, 
&quot;Retail Investment&quot;, &quot;616&quot;, 
&quot;Retail Out of Town (England &amp; Wales)&quot;, &quot;605&quot;, 
&quot;Retail Out of Town (Scotland)&quot;, &quot;600&quot;, 
&quot;Retail Scotland&quot;, &quot;600&quot;, 
&quot;Retail Shopping Centre Agency&quot;, &quot;618&quot;, 
&quot;Strategic Consulting&quot;, &quot;659&quot;, 
&quot;Sustainability&quot;, &quot;864&quot;, 
&quot;UK IT Operations&quot;, &quot;810&quot;, 
&quot;Valuation&quot;, &quot;650&quot;, 
&quot;Valuation and Advisory Services&quot;, &quot;650&quot;, 
&quot;Vista Architecture&quot;, &quot;653&quot;, 
&quot;Waste, Energy &amp; Minerals | Natural Resources&quot;, &quot;651&quot;, 
&quot;London Offices Capital Markets&quot;, &quot;695&quot;, 
&quot;London Offices Agency &amp; Development&quot;, &quot;694&quot;, 
&quot;PRS&quot;, &quot;608&quot;,
&quot;City Agency&quot;,&quot;688&quot;,
&quot;City Capital Markets&quot;,&quot;689&quot;,
&quot;Debt Advisory&quot;,&quot;999&quot;,
&quot;&quot; 
), 
&quot;&quot; 
)</formula>
        <name>Update Department Cost Centre Code 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Department_Integer_ID</fullName>
        <field>Department_Integer_Id__c</field>
        <formula>CASE( 
Department__c, 
&quot;Accurates&quot;, &quot;69&quot;, 
&quot;Automotive and Roadside&quot;, &quot;3&quot;, 
&quot;BB Building Consultancy&quot;, &quot;178&quot;, 
&quot;Bid Team&quot;, &quot;82&quot;, 
&quot;Business Space Lease Advisory&quot;, &quot;83&quot;, 
&quot;London Offices Lease Advisory&quot;, &quot;161&quot;, 
&quot;City Fringe&quot;, &quot;177&quot;, 
&quot;City Investment&quot;, &quot;128&quot;, 
&quot;City Offices&quot;, &quot;127&quot;, 
&quot;Colliers Capital UK&quot;, &quot;7&quot;, 
&quot;Colliers International Hotels&quot;, &quot;70&quot;, 
&quot;Advisory &amp; Restructuring&quot;, &quot;54&quot;, 
&quot;Corporate Solutions&quot;, &quot;49&quot;, 
&quot;Data Centres&quot;, &quot;164&quot;, 
&quot;Deanwater Estates&quot;, &quot;141&quot;, 
&quot;Destination Consulting&quot;, &quot;120&quot;, 
&quot;Development Advisory&quot;, &quot;110&quot;, 
&quot;Digital Marketing&quot;, &quot;97&quot;, 
&quot;EMEA&quot;, &quot;90&quot;, 
&quot;EMEA Corporate Solutions&quot;, &quot;115&quot;, 
&quot;EMEA IT&quot;, &quot;144&quot;, 
&quot;Executive Group&quot;, &quot;20&quot;, 
&quot;Facilities&quot;, &quot;10&quot;, 
&quot;Finance&quot;, &quot;11&quot;, 
&quot;Healthcare&quot;, &quot;12&quot;, 
&quot;Hotels &amp; Resorts Consulting&quot;, &quot;147&quot;, 
&quot;Hotels Agency&quot;, &quot;146&quot;, 
&quot;Hotels Valuation&quot;, &quot;148&quot;, 
&quot;Human Resources&quot;, &quot;13&quot;, 
&quot;Industrial and Logistics&quot;, &quot;18&quot;, 
&quot;Information&quot;, &quot;14&quot;, 
&quot;Information Technology Service&quot;, &quot;122&quot;, 
&quot;International Properties&quot;, &quot;118&quot;, 
&quot;Investment Property Management&quot;, &quot;47&quot;, 
&quot;Legal and Compliance&quot;, &quot;149&quot;, 
&quot;Licensed and Leisure&quot;, &quot;17&quot;, 
&quot;Marketing&quot;, &quot;19&quot;, 
&quot;Metals &amp; Mining&quot;, &quot;163&quot;, 
&quot;National Capital Markets&quot;, &quot;96&quot;, 
&quot;National Offices&quot;, &quot;22&quot;, 
&quot;Nile Management&quot;, &quot;34&quot;, 
&quot;Parks, Sports and Marinas&quot;, &quot;142&quot;, 
&quot;Planning&quot;, &quot;25&quot;, 
&quot;Project &amp; Building Consultancy&quot;, &quot;4&quot;, 
&quot;Project Management&quot;, &quot;145&quot;, 
&quot;Property Management Accounts&quot;, &quot;50&quot;, 
&quot;Public Relations&quot;, &quot;26&quot;, 
&quot;Rating&quot;, &quot;27&quot;, 
&quot;Research and Forecasting&quot;, &quot;29&quot;, 
&quot;Residential&quot;, &quot;30&quot;, 
&quot;Residential - Development&quot;, &quot;134&quot;, 
&quot;Residential – New Homes&quot;, &quot;160&quot;, 
&quot;Retail&quot;, &quot;150&quot;, 
&quot;Retail - Lease Advisory&quot;, &quot;158&quot;, 
&quot;Retail – Central London Lease Advisory&quot;, &quot;152&quot;, 
&quot;Retail Agency&quot;, &quot;65&quot;, 
&quot;Retail Agency - Central London&quot;, &quot;153&quot;, 
&quot;Retail Agency - North&quot;, &quot;154&quot;, 
&quot;Retail Agency - Out of Town&quot;, &quot;151&quot;, 
&quot;Retail Agency - South&quot;, &quot;157&quot;, 
&quot;Retail Belfast&quot;, &quot;31&quot;, 
&quot;Retail Capital Markets&quot;, &quot;155&quot;, 
&quot;Retail Development&quot;, &quot;61&quot;, 
&quot;Retail Scotland&quot;, &quot;68&quot;, 
&quot;Retail Shopping Centre Agency&quot;, &quot;156&quot;, 
&quot;Sustainability&quot;, &quot;126&quot;, 
&quot;Valuation and Advisory Services&quot;, &quot;105&quot;, 
&quot;Waste, Energy &amp; Minerals | Natural Resources&quot;, &quot;162&quot;, 
&quot;London Office Capital Markets&quot;, &quot;131&quot;, 
&quot;London Offices Agency &amp; Development&quot;, &quot;130&quot;, 
&quot;PRS&quot;,&quot;9866&quot;,
&quot;City Agency&quot;,&quot;197&quot;,
&quot;City Capital Markets&quot;,&quot;198&quot;,
&quot;Debt Advisory&quot;,&quot;400&quot;,
&quot;&quot; 
)</formula>
        <name>Update Department Integer ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Division_on_Department_Forecast</fullName>
        <field>Division__c</field>
        <formula>CASE( Department__c, 

&quot;Automotive and Roadside&quot;, &quot;ALTERNATIVE MARKETS&quot;, 
&quot;BB Building Consultancy&quot;, &quot;PROJECT &amp; BUILDING CONSULTANCY&quot;, 
&quot;Bid Team&quot;, &quot;MARKETING AND BUSINESS DEVELOPMENT&quot;, 
&quot;Business Space Lease Advisory&quot;, &quot;PROFESSIONAL&quot;, 
&quot;London Offices Lease Advisory&quot;, &quot;CENTRAL LONDON&quot;, 
&quot;City Fringe&quot;, &quot;CENTRAL LONDON&quot;, 
&quot;City Investment&quot;, &quot;CENTRAL LONDON&quot;, 
&quot;City Offices&quot;, &quot;CENTRAL LONDON&quot;, 
&quot;Colliers Capital UK&quot;, &quot;COLLIERS CAPITAL&quot;, 
&quot;Advisory &amp; Restructuring&quot;, &quot;VALUATION AND ADVISORY SERVICES&quot;, 
&quot;Corporate Solutions&quot;, &quot;PROFESSIONAL&quot;, 
&quot;Data Centres&quot;, &quot;BUSINESS SUPPORT&quot;, 
&quot;Deanwater Estates&quot;, &quot;DEANWATER&quot;, 
&quot;Destination Consulting&quot;, &quot;ALTERNATIVE MARKETS&quot;, 
&quot;Development Advisory&quot;, &quot;BUSINESS SPACE&quot;, 
&quot;Digital Marketing&quot;, &quot;MARKETING AND BUSINESS DEVELOPMENT&quot;, 
&quot;Facilities&quot;, &quot;BUSINESS SUPPORT&quot;, 
&quot;Finance&quot;, &quot;BUSINESS SUPPORT&quot;, 
&quot;Healthcare&quot;, &quot;ALTERNATIVE MARKETS&quot;, 
&quot;Hotels &amp; Resorts Consulting&quot;, &quot;ALTERNATIVE MARKETS&quot;, 
&quot;Hotels Agency&quot;, &quot;ALTERNATIVE MARKETS&quot;, 
&quot;Hotels Valuation&quot;, &quot;ALTERNATIVE MARKETS&quot;, 
&quot;Human Resources&quot;, &quot;BUSINESS SUPPORT&quot;, 
&quot;Industrial and Logistics&quot;, &quot;BUSINESS SPACE&quot;, 
&quot;Information Technology Service&quot;, &quot;BUSINESS SUPPORT&quot;, 
&quot;Investment Property Management&quot;, &quot;PROFESSIONAL&quot;, 
&quot;Legal and Compliance&quot;, &quot;BUSINESS SUPPORT&quot;, 
&quot;Licensed and Leisure&quot;, &quot;ALTERNATIVE MARKETS&quot;, 
&quot;Marketing&quot;, &quot;MARKETING AND BUSINESS DEVELOPMENT&quot;, 
&quot;National Capital Markets&quot;, &quot;NATIONAL CAPITAL MARKETS&quot;, 
&quot;Debt Advisory&quot;,&quot;NATIONAL CAPITAL MARKETS&quot;,
&quot;National Offices&quot;, &quot;BUSINESS SPACE&quot;, 
&quot;Parks, Sports and Marinas&quot;, &quot;ALTERNATIVE MARKETS&quot;, 
&quot;Planning&quot;, &quot;PROFESSIONAL&quot;, 
&quot;PR&quot;, &quot;COMMUNICATIONS&quot;, 
&quot;Project &amp; Building Consultancy&quot;, &quot;PROJECT &amp; BUILDING CONSULTANCY&quot;, 
&quot;Project Management&quot;, &quot;PROJECT &amp; BUILDING CONSULTANCY&quot;, 
&quot;Property Management Accounts&quot;, &quot;PROFESSIONAL&quot;, 
&quot;Public Relations&quot;, &quot;COMMUNICATIONS&quot;, 
&quot;Rating&quot;, &quot;RATING&quot;, 
&quot;Research and Forecasting&quot;, &quot;BUSINESS SUPPORT&quot;, 
&quot;Residential&quot;, &quot;BUSINESS SPACE&quot;, 
&quot;Residential - Development&quot;, &quot;BUSINESS SPACE&quot;, 
&quot;Residential – New Homes&quot;, &quot;BUSINESS SPACE&quot;, 
&quot;Retail&quot;, &quot;RETAIL&quot;, 
&quot;Retail - Lease Advisory&quot;, &quot;RETAIL&quot;, 
&quot;Retail - Scotland&quot;, &quot;RETAIL&quot;, 
&quot;Retail – Central London Lease Advisory&quot;, &quot;RETAIL&quot;, 
&quot;Retail Agency&quot;, &quot;RETAIL&quot;, 
&quot;Retail Agency - Central London&quot;, &quot;RETAIL&quot;, 
&quot;Retail Agency - North&quot;, &quot;RETAIL&quot;, 
&quot;Retail Agency - Out of Town&quot;, &quot;RETAIL&quot;, 
&quot;Retail Agency - South&quot;, &quot;RETAIL&quot;, 
&quot;Retail Belfast&quot;, &quot;RETAIL&quot;, 
&quot;Retail Capital Markets&quot;, &quot;RETAIL&quot;, 
&quot;Retail Development&quot;, &quot;RETAIL&quot;, 
&quot;Retail Investment&quot;, &quot;RETAIL&quot;, 
&quot;Retail Scotland&quot;, &quot;RETAIL&quot;, 
&quot;Retail Shopping Centre Agency&quot;, &quot;RETAIL&quot;, 
&quot;Sustainability&quot;, &quot;BUSINESS SUPPORT&quot;, 
&quot;Valuation and Advisory Services&quot;, &quot;VALUATION AND ADVISORY SERVICES&quot;, 
&quot;Waste, Energy &amp; Minerals | Natural Resources&quot;, &quot;VALUATION AND ADVISORY SERVICES&quot;, 
&quot;London Offices Capital Markets&quot;, &quot;CENTRAL LONDON&quot;, 
&quot;London Offices Agency &amp; Development&quot;, &quot;CENTRAL LONDON&quot;, 
&quot;PRS&quot;, &quot;BUSINESS SPACE&quot;,
&quot;City Agency&quot;,&quot;CENTRAL LONDON&quot;,
&quot;City Capital Markets&quot;,&quot;CENTRAL LONDON&quot;,
&quot;&quot; 
)</formula>
        <name>Update Division on Department Forecast</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Office_Cost_Centre_Code</fullName>
        <field>Office_Cost_Centre_Code__c</field>
        <formula>CASE( 
officename__c, 
&quot;Bristol&quot;, &quot;06&quot;, 
&quot;Glasgow&quot;, &quot;05&quot;, 
&quot;Uxbridge&quot;, &quot;07&quot;, 
&quot;London - City Fringe&quot;, &quot;17&quot;, 
&quot;Manchester&quot;, &quot;03&quot;, 
&quot;Belfast&quot;, &quot;55&quot;, 
&quot;Edinburgh&quot;, &quot;04&quot;, 
&quot;Birmingham&quot;, &quot;01&quot;, 
&quot;Welbeck Street&quot;, &quot;00&quot;, 
&quot;Plymouth&quot;, &quot;13&quot;, 
&quot;London - West End&quot;, &quot;00&quot;, 
&quot;Leeds&quot;, &quot;02&quot;, 
&quot;London - City&quot;, &quot;16&quot;, 
&quot;Cirencester&quot;, &quot;11&quot;, 
&quot;Jersey&quot;, &quot;18&quot;,
&quot;&quot; 
)</formula>
        <name>Update Office Cost Centre Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Office_Integer_ID</fullName>
        <field>Office_Integer_Id__c</field>
        <formula>CASE( 
officename__c, 
&quot;Bristol&quot;, &quot;6&quot;, 
&quot;Glasgow&quot;, &quot;10&quot;, 
&quot;Uxbridge&quot;, &quot;32&quot;, 
&quot;London - City Fringe&quot;, &quot;73&quot;, 
&quot;Manchester&quot;, &quot;14&quot;, 
&quot;Belfast&quot;, &quot;67&quot;, 
&quot;Edinburgh&quot;, &quot;9&quot;, 
&quot;Birmingham&quot;, &quot;57&quot;, 
&quot;Welbeck Street&quot;, &quot;17&quot;, 
&quot;Plymouth&quot;, &quot;15&quot;, 
&quot;London - West End&quot;, &quot;16&quot;, 
&quot;Leeds&quot;, &quot;12&quot;, 
&quot;London - City&quot;, &quot;70&quot;, 
&quot;Cirencester&quot;, &quot;11&quot;, 
&quot;Jersey&quot;, &quot;18&quot;,
&quot;&quot; 
)</formula>
        <name>Update Office Integer ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CS_Update Department Forecast fields 1</fullName>
        <actions>
            <name>Update_Department_Integer_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Division_on_Department_Forecast</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Department_Forecast__c.Department__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Division, Department Integer ID based on Department.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS_Update Department Forecast fields 2</fullName>
        <actions>
            <name>Update_Department_Cost_Centre_Code_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Department_Forecast__c.Department__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Department_Forecast__c.Department_Cost_Centre_Code__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update Department Cost Centre Code Part 2</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS_Update Department Forecast fields 3</fullName>
        <actions>
            <name>Update_Office_Cost_Centre_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Office_Integer_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Department_Forecast__c.officename__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Office Integer ID and Update Office Cost Centre Code based on Office.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
