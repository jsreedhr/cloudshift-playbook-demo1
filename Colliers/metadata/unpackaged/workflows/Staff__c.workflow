<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Licence_Assigned</fullName>
        <description>Licence Assigned</description>
        <protected>false</protected>
        <recipients>
            <recipient>justine.loftus@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Licence_Assigned_Email</template>
    </alerts>
    <alerts>
        <fullName>License_Removal_Reminder</fullName>
        <description>License Removal Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>System_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>UK/License_Removal_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Notification_Email_to_BDM_for_Opportunity_Allocation_New</fullName>
        <description>Notification Email to BDM for Opportunity Allocation</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Rating/Opportunity_Allocated_to_Opportunity_Manager</template>
    </alerts>
    <alerts>
        <fullName>Staff_Leaving_Date</fullName>
        <description>Staff Leaving Date</description>
        <protected>false</protected>
        <recipients>
            <recipient>concep@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Leave_Date</template>
    </alerts>
    <alerts>
        <fullName>Training_Complete_release_Licence</fullName>
        <description>Training Complete release Licence</description>
        <protected>false</protected>
        <recipients>
            <recipient>System_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Training_Complete_release_Licence</template>
    </alerts>
    <fieldUpdates>
        <fullName>Date_Stamp_License_Allocated</fullName>
        <field>Allocated_License_Date__c</field>
        <formula>TODAY()</formula>
        <name>Date Stamp - License Allocated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>License_Deactivated_Date_Stamp</fullName>
        <field>License_Removed_Date__c</field>
        <formula>TODAY()</formula>
        <name>License Deactivated Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>License_Frozen_Date_Stamp</fullName>
        <field>License_Frozen_Date__c</field>
        <formula>TODAY()</formula>
        <name>License Frozen Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>License_Reassigned_Date</fullName>
        <field>Date_Reassigned__c</field>
        <formula>Reassigned_to__r.Allocated_License_Date__c</formula>
        <name>License Reassigned Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Full_Name_with_Staff_Name</fullName>
        <field>Full_Name__c</field>
        <formula>FIRST_NAME__c &amp; &quot; &quot; &amp; SURNAME__c</formula>
        <name>Stamp Full Name with Staff Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Staff_Email_with_User_Email_Addres</fullName>
        <field>Email__c</field>
        <formula>User__r.Email</formula>
        <name>Stamp Staff Email with User Email Addres</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>If License Reassigned then fill in date</fullName>
        <actions>
            <name>License_Reassigned_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Staff__c.License_Reassigned__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Leave Date</fullName>
        <actions>
            <name>Staff_Leaving_Date</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Staff__c.Leave_Date__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Licence - Training Completed</fullName>
        <actions>
            <name>Training_Complete_release_Licence</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Staff__c.Date_of_Training__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Notification to Salesforce team that training has completed today and release licence.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Licence Assigned Email</fullName>
        <actions>
            <name>Licence_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Staff__c.License_Stage__c</field>
            <operation>equals</operation>
            <value>Licence Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Staff__c.Date_of_Training__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>License Allocated Date Stamp</fullName>
        <actions>
            <name>Date_Stamp_License_Allocated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Staff__c.License_Stage__c</field>
            <operation>equals</operation>
            <value>Allocated</value>
        </criteriaItems>
        <description>Rule to stamp date when license stage = allocated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>License Deactivated Date Stamp</fullName>
        <actions>
            <name>License_Deactivated_Date_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Staff__c.License_Stage__c</field>
            <operation>equals</operation>
            <value>Deactivated</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>License Frozen Date Stamp</fullName>
        <actions>
            <name>License_Frozen_Date_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Staff__c.License_Stage__c</field>
            <operation>equals</operation>
            <value>Frozen</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>License Removal Reminder</fullName>
        <actions>
            <name>License_Removal_Reminder</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Date_to_Remove_License__c =  TODAY()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Stamp Full Name with First Name %26 Surname</fullName>
        <actions>
            <name>Stamp_Full_Name_with_Staff_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Staff__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>System: Populate &apos;Full Name&apos; field with Staff Object &apos;Name&apos; 
Initial Configuration was for UK Pilot Rating</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stamp Full Name with Staff Name</fullName>
        <actions>
            <name>Stamp_Full_Name_with_Staff_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Staff__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>System: Populate &apos;Full Name&apos; field with Staff Object &apos;Name&apos; 
Initial Configuration was for UK Pilot Rating</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stamp Staff Email field with Linked User Email Address</fullName>
        <actions>
            <name>Stamp_Staff_Email_with_User_Email_Addres</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Staff__c.User__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
