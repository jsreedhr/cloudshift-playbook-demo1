<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Reminder_Email</fullName>
        <description>Reminder Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>System_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Reminder_Email</template>
    </alerts>
    <rules>
        <fullName>Reminder for Task</fullName>
        <active>false</active>
        <description>workflow to alert users that an outstanding task needs to be completed</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Outstanding_Task__c.Reminder_Date__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
