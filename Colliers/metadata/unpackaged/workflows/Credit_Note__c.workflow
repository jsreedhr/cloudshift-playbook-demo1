<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Credit_Note_Approved</fullName>
        <description>Email Credit Note Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Record_Approved</template>
    </alerts>
    <alerts>
        <fullName>Email_Credit_Note_Rejected</fullName>
        <description>Email Credit Note Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Record_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>CS_Update_Credit_Note_Rejected_Date</fullName>
        <field>Rejected_Date__c</field>
        <formula>NOW()</formula>
        <name>CS_Update Credit Note Rejected Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Note_Approved_By</fullName>
        <field>Approved_By__c</field>
        <formula>$User.FirstName &amp;&quot; &quot;&amp;  $User.LastName</formula>
        <name>Update Credit Note Approved By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Note_Approved_Date</fullName>
        <field>Approved_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Credit Note Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Note_Date</fullName>
        <field>Credit_Note_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Credit Note Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Note_Rejected_by</fullName>
        <field>Rejected_By__c</field>
        <formula>$User.FirstName &amp;&quot; &quot;&amp;  $User.LastName</formula>
        <name>Update Credit Note Rejected by</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Note_Requested_Date</fullName>
        <description>To stamp the date on which the credit note was sent for approval</description>
        <field>Date_Requested__c</field>
        <formula>NOW()</formula>
        <name>Update Credit Note Requested Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Note_Status_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Credit Note Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Note_Status_Awaiting</fullName>
        <field>Status__c</field>
        <literalValue>Awaiting Approval</literalValue>
        <name>Update Credit Note Status Awaiting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Note_Status_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Credit  Note Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
