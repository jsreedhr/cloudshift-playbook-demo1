<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FR_Notification_of_New_Feedback_Submitted</fullName>
        <description>FR Notification of New Feedback Submitted</description>
        <protected>false</protected>
        <recipients>
            <recipient>richard.judd@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FR/FR_Notification_New_Feedback_Submitted</template>
    </alerts>
    <rules>
        <fullName>FR New Feedback Submitted</fullName>
        <actions>
            <name>FR_Notification_of_New_Feedback_Submitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Initial Implementation Requested by FR - 10/2016</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
