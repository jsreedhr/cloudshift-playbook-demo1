<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_AML_Team_MLR_Expired</fullName>
        <ccEmails>richard.allison@colliers.com.test</ccEmails>
        <description>Email Alert AML Team MLR Expired</description>
        <protected>false</protected>
        <recipients>
            <recipient>justine.loftus@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/MLR_Expired_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_AML_Team_MLR_about_to_expire</fullName>
        <ccEmails>richard.allison@colliers.com.test</ccEmails>
        <description>Email Alert AML Team MLR about to expire</description>
        <protected>false</protected>
        <recipients>
            <recipient>justine.loftus@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/MLR</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Expiry_Date</fullName>
        <field>Expiry_Date__c</field>
        <formula>IF(
AND(Month( MLR_Set_Date__c )=2,Day(MLR_Set_Date__c)=29),
DATE(YEAR(MLR_Set_Date__c) + 3, Month(MLR_Set_Date__c),Day(MLR_Set_Date__c)-1),
DATE(YEAR(MLR_Set_Date__c) + 3, Month(MLR_Set_Date__c),Day(MLR_Set_Date__c))
)</formula>
        <name>Update Expiry Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MLR_Expiry_Date</fullName>
        <field>Expiry_Date__c</field>
        <formula>ID_Expiry_Date__c</formula>
        <name>Update MLR Expiry Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MLR_Set_Date</fullName>
        <field>MLR_Set_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update MLR Set Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MLR_Status_Expiring_Soon</fullName>
        <field>Status__c</field>
        <literalValue>Expiring Soon</literalValue>
        <name>Update MLR Status Expiring Soon</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MLR_Status_to_Expired</fullName>
        <field>Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Update MLR Status to Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CS_Update MLR Expiry Date with ID</fullName>
        <actions>
            <name>Update_MLR_Expiry_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISBLANK(TEXT(Form_of_ID__c))), NOT( ISNULL( MLR_Set_Date__c ) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_AML_Team_MLR_Expired</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_MLR_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>MLR__c.Expiry_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CS_Update MLR Expiry Date without ID</fullName>
        <actions>
            <name>Update_Expiry_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISBLANK(TEXT(Form_of_ID__c)), NOt( ISNULL( MLR_Set_Date__c ) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_AML_Team_MLR_about_to_expire</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_MLR_Status_Expiring_Soon</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>MLR__c.Expiry_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_AML_Team_MLR_Expired</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_MLR_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>MLR__c.Expiry_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CS_Update MLR Set Date</fullName>
        <actions>
            <name>Update_MLR_Set_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED( Status__c ), ISPICKVAL( Status__c, &apos;Pass&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
