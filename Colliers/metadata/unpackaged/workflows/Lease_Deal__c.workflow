<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Lease_Deal_Name</fullName>
        <field>Name</field>
        <formula>LEFT( ( TEXT(Letting_Sale_Type__c)  &amp;&apos; &apos;&amp; TEXT(Date_of_Completion__c) &amp;&apos; &apos;&amp;  Suite_Unit__r.Name &amp;&apos; &apos;&amp;
 Property__r.Name  ), 79)</formula>
        <name>Update Lease/Deal Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Lease%2FDeal Name</fullName>
        <actions>
            <name>Update_Lease_Deal_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lease_Deal__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Letting,Sale</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
