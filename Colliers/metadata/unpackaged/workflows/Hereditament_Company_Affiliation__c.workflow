<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Company_Name_on_Hereditament</fullName>
        <description>Update Company Name on Hereditament Record</description>
        <field>Rate_Payer__c</field>
        <formula>Company__r.Name</formula>
        <name>Update Company Name on Hereditament</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Hereditament__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Update Company Name on Hereditament Record</fullName>
        <actions>
            <name>Update_Company_Name_on_Hereditament</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Hereditament_Company_Affiliation__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Enables user to see last company record associated with Hereditament
Requested by UK Rating</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
