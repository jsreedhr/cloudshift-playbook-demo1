<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Payment_Notification</fullName>
        <description>Email Payment Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Job_Manager_Assistant_s_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Job_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Email_Payment_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Populate_Job_Manager_Assistant_s_Email</fullName>
        <description>Populates the Job Manager Assistant&apos;s Email field on the invoice payment object</description>
        <field>Job_Manager_Assistant_s_email__c</field>
        <formula>Invoice__r.Opportunity__r.Manager__r.ASSISTANT__r.Email__c</formula>
        <name>Populate Job Manager Assistant&apos;s Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Job_Manager_Email</fullName>
        <description>Populates the Job Manager Email field on the invoice payment object</description>
        <field>Job_Manager_Email__c</field>
        <formula>Invoice__r.Opportunity__r.Manager__r.Email__c</formula>
        <name>Populate Job Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Invoice payment - populate email fields</fullName>
        <actions>
            <name>Email_Payment_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Populate_Job_Manager_Assistant_s_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Job_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Invoice payment is created trigger this workflow.
This will then populate the Job Manager and Job Manager Assistant&apos;s email fields on the new invoice payment record</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
