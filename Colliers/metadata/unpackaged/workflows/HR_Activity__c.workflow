<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_HR_Activity_Name</fullName>
        <field>Name</field>
        <formula>LEFT( (&quot;Follow Up&quot;&amp;&quot; &quot;&amp;(TEXT(Chased_Date__c))&amp;&quot; &quot;&amp;
Chased_By__r.Name), 79)</formula>
        <name>Update HR Activity Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update HR Activity Name</fullName>
        <actions>
            <name>Update_HR_Activity_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
