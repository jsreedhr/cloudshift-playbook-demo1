<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Allocation_Notification</fullName>
        <description>Allocation Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_to_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Allocation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Allocation_Notification_New</fullName>
        <description>Allocation Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_to_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Allocation_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Assigned_To_of_allocation_amount_change</fullName>
        <description>Email Notification to Assigned To of allocation amount change</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_to_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Allocation_Amount_Change_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Assistant_of_allocation_amount_change</fullName>
        <description>Email Notification to Assistant of allocation amount change</description>
        <protected>false</protected>
        <recipients>
            <field>Assistant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Allocation_Amount_Change_Notification</template>
    </alerts>
    <alerts>
        <fullName>Forecasting_Notification_when_amount_changes</fullName>
        <description>Allocation- Notification when amount changes</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_to_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Rating/Forecasting_Notification_when_amount_changes</template>
    </alerts>
    <alerts>
        <fullName>Notify_Assistant</fullName>
        <description>Notify Assistant</description>
        <protected>false</protected>
        <recipients>
            <field>Assistant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Allocation_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Allocation_Amount_Update</fullName>
        <field>Allocation_Amount__c</field>
        <formula>Total_Amount__c+Total_Accured_Amount__c+Total_Journal_Amount__c+Total_Invoice_Amount__c</formula>
        <name>Allocation Amount Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Allocation_Polpulate_assigned_to_emai</fullName>
        <field>Assigned_to_Email__c</field>
        <formula>Assigned_To__r.Email__c</formula>
        <name>Allocation- Polpulate assigned to emai</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_send_allocation_email_to_false</fullName>
        <field>Send_notification_email__c</field>
        <literalValue>0</literalValue>
        <name>Set send allocation email to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_send_allocation_email_to_true</fullName>
        <field>Send_notification_email__c</field>
        <literalValue>1</literalValue>
        <name>Set send allocation email to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_HR_Number</fullName>
        <field>Allocation_Assigned_To_HR_No__c</field>
        <formula>Assigned_To__r.HR_No__c</formula>
        <name>Stamp HR Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_JM_Allocation_Department</fullName>
        <description>stamps the job managers department into the JM allocation department field</description>
        <field>JM_Allocation_Department__c</field>
        <formula>Job__r.Department_Reporting__c</formula>
        <name>Stamp JM Allocation Department</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_JM_Allocation_Office</fullName>
        <description>stamps the job manager&apos;s office into the JM Allocation Office field</description>
        <field>JM_Allocation_Office__c</field>
        <formula>Job__r.Office_Reporting__c</formula>
        <name>Stamp JM Allocation Office</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Alloc_Department_to_Job_Manager</fullName>
        <field>Department_Allocation__c</field>
        <formula>Job__r.Department_Reporting__c</formula>
        <name>Update Alloc Department to Job Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Allocation_Amount</fullName>
        <field>Allocation_Amount__c</field>
        <formula>Total_Accured_Amount__c + Total_Amount__c +  Total_Invoice_Amount__c + Total_Journal_Amount__c</formula>
        <name>Update Allocation Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Assigned_To_email</fullName>
        <description>To update the Assigned To email address on the Allocation</description>
        <field>Assigned_to_Email__c</field>
        <formula>Assigned_To__r.Email__c</formula>
        <name>Update Assigned To email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Assistant_Email</fullName>
        <field>Assistant_Email__c</field>
        <formula>Assigned_To__r.ASSISTANT__r.Email__c</formula>
        <name>Update Assistant Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Allocation_Amount</fullName>
        <field>Previous_Allocation_Amount__c</field>
        <formula>PRIORVALUE( Allocation_Amount__c )</formula>
        <name>Update Previous Allocation Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CS Allocation Amount Change Notification</fullName>
        <actions>
            <name>Email_Notification_to_Assigned_To_of_allocation_amount_change</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Notification_to_Assistant_of_allocation_amount_change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Allocation_Amount__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS_Allocation Notification</fullName>
        <actions>
            <name>Allocation_Notification_New</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Notify_Assistant</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Assigned_To_email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Assistant_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To inform staff member when an allocation has been assigned to them</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CS_Allocation_Amount_Update</fullName>
        <actions>
            <name>Allocation_Amount_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ischanged( Total_Journal_Amount__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS_Update Allocation Amount</fullName>
        <actions>
            <name>Update_Allocation_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Previous_Allocation_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Updates Allocation Amount from Forecasts invoiced accrued and journaled, also records previous amount and notifies the user of the change in allocation amount,</description>
        <formula>OR( ISCHANGED( Total_Accured_Amount__c ), ISCHANGED( Total_Amount__c ), ISCHANGED( Total_Invoice_Amount__c ), ISCHANGED( Total_Journal_Amount__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS_Update Previous Allocation Amount</fullName>
        <actions>
            <name>Update_Previous_Allocation_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Allocation_Amount__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stamp Allocation HR Number</fullName>
        <actions>
            <name>Stamp_HR_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stamp External Allocation Department</fullName>
        <actions>
            <name>Stamp_JM_Allocation_Department</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Stamp_JM_Allocation_Office</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
