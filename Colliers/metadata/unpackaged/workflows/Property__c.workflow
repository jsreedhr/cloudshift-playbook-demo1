<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Full_Street</fullName>
        <field>Full_Street__c</field>
        <formula>Street_No__c + &apos; &apos; + Street__c</formula>
        <name>Populate Full Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Street</fullName>
        <field>Street__c</field>
        <formula>SUBSTITUTE(Right( Full_Street__c ,
LEN(Full_Street__c ) - FIND(&quot;,&quot;, Full_Street__c )), &quot;,&quot;, &quot;&quot;)</formula>
        <name>Populate Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Street_No</fullName>
        <field>Street_No__c</field>
        <formula>SUBSTITUTE(LPAD( Full_Street__c ,
FIND(&quot;,&quot;, Full_Street__c )), &quot;,&quot;, &quot;&quot;)</formula>
        <name>Populate Street No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Property_Name</fullName>
        <field>Name</field>
        <formula>LEFT(
(Floor_No__c &amp;&apos; &apos;&amp;
Suite_Unit__c &amp;&apos; &apos;&amp;
Building_Number_Additional__c &amp;&apos; &apos;&amp;
Sub_Building_Name__c &amp;&apos; &apos;&amp;
Building_Name__c &amp;&apos; &apos;&amp;
Estate__c  &amp;&apos; &apos;&amp;
Full_Street__c  &amp;&apos; &apos;&amp;
Area__c &amp;&apos; &apos;&amp;
Town__c &amp;&apos; &apos;&amp;   
County__c &amp;&apos; &apos;&amp; 
Post_Code__c &amp;&apos; &apos;&amp;
PO_Box__c &amp;&apos; &apos;&amp;
Country__c), 79
)</formula>
        <name>Update Property Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CS_Update Property Name</fullName>
        <actions>
            <name>Update_Property_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name =&quot;Colliers UK&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Street and Street No</fullName>
        <actions>
            <name>Populate_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Street_No</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( RecordType.Name=&quot;Colliers UK&quot;, ISCHANGED( Full_Street__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate full street</fullName>
        <actions>
            <name>Populate_Full_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name=&quot;Colliers UK&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Property Type to %27Mixed Properties%27</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Property__c.Property_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
