<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Copy_Event_Description</fullName>
        <field>Comments_Summary__c</field>
        <formula>IF( 
LEN(Description) &gt; 252, 
LEFT(Description,252)&amp;&quot;…&quot;, 
LEFT(Description,255))</formula>
        <name>Copy Event Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Event_Completed_Date_with_TODAY</fullName>
        <field>Closed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Event Completed Date with TODAY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copy Event Description</fullName>
        <actions>
            <name>Copy_Event_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.Description</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <description>Used so that users may be able to see comments on Activities Related List on Opportunity Page Layout. Not possible on Comments standard field.
Requested by UK Rating</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
