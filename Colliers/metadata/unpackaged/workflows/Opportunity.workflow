<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_AML_Team_MLR_Required</fullName>
        <description>Email Alert AML Team MLR Required</description>
        <protected>false</protected>
        <recipients>
            <recipient>justine.loftus@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maryam@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/MLR_No_Valid_MLR_on_Invoicing_Company</template>
    </alerts>
    <alerts>
        <fullName>FR_New_Opportunity</fullName>
        <ccEmails>corentin.liorzou@colliers.com</ccEmails>
        <description>FR New Opportunity</description>
        <protected>false</protected>
        <recipients>
            <recipient>arnaud.violette@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>fr.sffinance@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pauline.martin@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FR/FR_New_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>FR_Opp_GM_100k</fullName>
        <ccEmails>otto.buchinger@colliers.com</ccEmails>
        <description>FR Opportunity GM &gt; 100,000 EUR</description>
        <protected>false</protected>
        <recipients>
            <recipient>arnaud.violette@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pauline.martin@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FR/FR_New_Opportunity_GM_100k</template>
    </alerts>
    <alerts>
        <fullName>FR_Opportunity_Close_Date_is_1_week</fullName>
        <description>FR Opportunity Close Date is 1 week</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>fr.sffinance@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pauline.martin@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FR/FR_Close_Date_is_1_Week</template>
    </alerts>
    <alerts>
        <fullName>Job_Lite_Creation_Admin_Alert</fullName>
        <description>Job Lite Creation - Admin Alert</description>
        <protected>false</protected>
        <recipients>
            <field>JM_Assistant__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Job_Lite_Creation_Admin</template>
    </alerts>
    <alerts>
        <fullName>Notification_Email_to_Rachael_for_Opportunity_Stage_at_Terms_Signed</fullName>
        <description>Notification Email to Rachael Dempster for Opportunity Stage at Terms Signed</description>
        <protected>false</protected>
        <recipients>
            <recipient>rachael.dempster@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Rating/Email_Notification_Opportunity_is_Terms_Signed_Email_to_Rachael_Dempster</template>
    </alerts>
    <alerts>
        <fullName>Oppty_Stage_Return_to_Data_Email_Notification_to_Data_Team</fullName>
        <description>Oppty Stage Return to Data Email Notification to Data Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>cicely.barker-williams@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rachael.dempster@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sherise.titley@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Rating/Oppty_Stage_Return_to_Data_Email_Notification_to_Data_Team</template>
    </alerts>
    <alerts>
        <fullName>SOE_Compliance_Notification</fullName>
        <ccEmails>maryam@cloudshiftgroup.com</ccEmails>
        <description>SOE Compliance Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>justine.loftus@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/SOE_Compliance_Notification</template>
    </alerts>
    <alerts>
        <fullName>Sent_Email_when_opportunity_is_created</fullName>
        <description>Sent Email when opportunity is created</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FR/FR_Email_Notification_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>X48_Hr_Notification_Opportunity_Stage_at_Meeting_Changed_Need_to_Chase</fullName>
        <description>48 Hr Notification Opportunity Stage at &apos;Meeting Changed - Need to Chase&apos;</description>
        <protected>false</protected>
        <recipients>
            <field>Booker__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Rating/X48_HR_Email_Notification_Opportunity_at_Stage_Meeting_Changed_Need_to_Chase</template>
    </alerts>
    <fieldUpdates>
        <fullName>CS_Update_MLR_Required</fullName>
        <field>MLR_Required__c</field>
        <literalValue>1</literalValue>
        <name>CS_Update_MLR_Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Conflict_Check_Date_Stamp</fullName>
        <field>Conflict_Check_Date_Stamp__c</field>
        <formula>TODAY()</formula>
        <name>Conflict Check - Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Stamp_Expectations</fullName>
        <field>Expectations_Date_Stamp__c</field>
        <formula>TODAY()</formula>
        <name>Date Stamp Expectations</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Stamp_Recommend</fullName>
        <field>Recommend_Date_Stamp__c</field>
        <formula>TODAY()</formula>
        <name>Date Stamp Recommend</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Stamp_Results</fullName>
        <field>Result_Date_Stamp__c</field>
        <formula>TODAY()</formula>
        <name>Date Stamp Results</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMEA_CS_Close_Date</fullName>
        <description>when EMEA CS opp closed then close date changes to today</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>EMEA CS Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Invoicing_Company_Stamp</fullName>
        <field>Invoicing_Company_Stamp__c</field>
        <formula>Invoicing_Company2__r.Name</formula>
        <name>Invoicing Company Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Job_Manager_Assistant</fullName>
        <field>JM_Assistant__c</field>
        <formula>Manager__r.ASSISTANT__r.Email__c</formula>
        <name>Job Manager Assistant</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Job_Naming_Convention</fullName>
        <field>Name</field>
        <formula>LEFT( Account.Name &amp; &quot; - &quot; &amp;     Manager__r.Name    &amp; &quot; - &quot; &amp;  TEXT(Department__c)  &amp;&quot; - &quot; &amp;    TEXT(Office__c)       &amp;&quot; - &quot; &amp;   Work_Type__c, 120)</formula>
        <name>Job - Naming Convention</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Today_s_Date</fullName>
        <description>Update close date to reflect current date when opp stage changes to closed won/lost</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Opp Today&apos;s Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reporting_Client_Stamp</fullName>
        <field>Reporting_Client_Stamp__c</field>
        <formula>Invoicing_Care_Of__r.Reporting_Client__r.Name</formula>
        <name>Reporting Client Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Oppty_Close_Date_with_TODAY</fullName>
        <description>Stamp Oppty Close Date with TODAY</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Stamp Oppty Close Date with TODAY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Oppty_Modified_Date_with_NOW</fullName>
        <field>Modified_Date__c</field>
        <formula>NOW()</formula>
        <name>Stamp Oppty Modified Date with NOW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_24_Hr_Field_with_24_Hr_Exc_WE</fullName>
        <description>This date/time field is used to indicate when exactly the email for the UK Rating Data Team should receive a 24hr notification to EXCLUDE weekends.</description>
        <field>X24_Hrs_Email_Date_Time_Send__c</field>
        <formula>IF( 
FLOOR(MOD( (NOW() + 1) - DATETIMEVALUE(&apos;1900-01-07 00:00:00&apos;), 7))=6 , NOW() + 3, 
IF( 
FLOOR(MOD( (NOW() + 1) - DATETIMEVALUE(&apos;1900-01-07 00:00:00&apos;), 7))=0 , NOW() + 2,NOW()+1))</formula>
        <name>Update 24 Hr Field with 24 Hr Exc WE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Terms_Signed</fullName>
        <field>Date_Terms_Signed__c</field>
        <formula>TODAY()</formula>
        <name>Update Date Terms Signed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Time_of_Stage_Change</fullName>
        <field>Date_Time_of_Stage_Change__c</field>
        <formula>NOW()</formula>
        <name>Update Date/Time of Stage Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_of_Invoice</fullName>
        <field>Date_of_Company_Change__c</field>
        <formula>TODAY()</formula>
        <name>Update Date of Invoice</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Engagement_Letter</fullName>
        <field>Engagement_Letter_Sent_Date__c</field>
        <formula>DATETIMEVALUE(  Date_Instructed__c )</formula>
        <name>Update Engagement Letter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Job_Manager_Email</fullName>
        <field>Job_Manager_Email__c</field>
        <formula>Manager__r.Email__c</formula>
        <name>Update Job Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Job_Manager_HR_No</fullName>
        <field>Job_Manager_HR_Number__c</field>
        <formula>Manager__r.HR_No__c</formula>
        <name>Update Job Manager HR No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Job_Number</fullName>
        <field>Job_Number__c</field>
        <formula>Temp_Job_Number__c</formula>
        <name>Update Job Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Job_record_type_Instructed</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Colliers_UK_Instructed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Job record type Instructed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Job_record_type_Job_Lite</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Colliers_UK_Job_Lite</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Job record type to Job Lite</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Job_record_type_to_pitching</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Colliers_UK_Pitching</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Job record type to pitching</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Stage_to_Closed_Lost</fullName>
        <field>StageName</field>
        <literalValue>Closed Lost</literalValue>
        <name>Update Opp Stage to Closed Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Stage_to_Closed_Won</fullName>
        <description>Initial Implementation Requested by FR - 01/2018</description>
        <field>StageName</field>
        <literalValue>Closed Won</literalValue>
        <name>Update Opp Stage to Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Stage_to_Open</fullName>
        <field>StageName</field>
        <literalValue>Open</literalValue>
        <name>Update Opp Stage to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Stage_to_Closed_Lost</fullName>
        <field>StageName</field>
        <literalValue>Closed Lost</literalValue>
        <name>Update Opportunity Stage to Closed Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Stage_to_Closed_Won</fullName>
        <description>Initial Implementation Requested by FR - 06/2016</description>
        <field>StageName</field>
        <literalValue>Closed Won</literalValue>
        <name>Update Opportunity Stage to Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Stage_to_Open</fullName>
        <description>Initial Implementation Requested by FR - 06/2016</description>
        <field>StageName</field>
        <literalValue>Open</literalValue>
        <name>Update Opportunity Stage to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_Prior_Value</fullName>
        <field>Stage_Prior_Value__c</field>
        <formula>TEXT(PRIORVALUE(StageName))</formula>
        <name>Update Stage Prior Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BEL Update Opportunity Stage to Closed Lost When All SL%27s Align</fullName>
        <actions>
            <name>Update_Opp_Stage_to_Closed_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Initial Implementation Requested by BEL - 01/2017
Update Opportunity Stage to Closed Lost When All Service Lines are Lost or Dropped</description>
        <formula>AND( Number_of_Service_Lines_Lost_Dropped__c = Number_of_Service_Lines__c, Number_of_Service_Lines_Lost_Dropped__c &lt;&gt; 0,RecordType.Id=&apos;0120E0000006lSO&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BEL Update Opportunity Stage to Closed Won When All SL%27s Align</fullName>
        <actions>
            <name>Update_Opp_Stage_to_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Initial Implementation Requested by BEL - 01/2018
Update Opportunity Stage to Closed Won when ALL service lines close and at least one is Closed Won (Contract Signed)</description>
        <formula>AND( Number_of_Service_Lines_Closed__c = Number_of_Service_Lines__c, Number_of_Service_Lines_Won__c &gt; 0,RecordType.Id=&apos;0120E0000006lSO&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BEL Update Opportunity Stage to Open When All SL%27s Align</fullName>
        <actions>
            <name>Update_Opp_Stage_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Initial Implementation Requested by FR - 01/2018</description>
        <formula>AND( Number_of_Service_Lines_Open__c &gt; Number_of_Service_Lines_Lost_Dropped__c,RecordType.Id=&apos;0120E0000006lSO&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS Job Update Manager email</fullName>
        <actions>
            <name>Job_Manager_Assistant</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Job_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Job_Manager_HR_No</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Job Manager Email field</description>
        <formula>OR(  RecordType.Name = &quot;Colliers UK Instructed&quot;, RecordType.Name = &quot;Colliers UK Job Lite&quot;, RecordType.Name = &quot;Colliers UK Pitching&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS Update Job record type to Instructed</fullName>
        <actions>
            <name>Update_Job_record_type_Instructed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Job record type to Instructed</description>
        <formula>AND( ISPICKVAL(StageName, &apos;Instructed&apos;), OR( RecordType.Name = &quot;Colliers UK Job Lite&quot;, RecordType.Name = &quot;Colliers UK Pitching&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS_Invoicing Company MLR Required</fullName>
        <actions>
            <name>Email_Alert_AML_Team_MLR_Required</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>To alert AML team that the updated invoicing company does not have a valid mlr.</description>
        <formula>AND(  ISCHANGED( Invoicing_Company2__c ),  MLR_Required__c = TRUE,  Invoicing_Company2__r.Valid_MLR__c = FALSE, OR(  RecordType.Name = &quot;Colliers UK Instructed&quot;,  RecordType.Name = &quot;Colliers UK Job Lite&quot;,  RecordType.Name = &quot;Colliers UK Pitching&quot;  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS_Invoicing Company MLR Required v2</fullName>
        <actions>
            <name>Email_Alert_AML_Team_MLR_Required</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>To alert AML team that the updated invoicing company does not have a valid mlr.</description>
        <formula>AND(  MLR_Required__c = TRUE,  Invoicing_Company2__r.Valid_MLR__c = FALSE, OR(  RecordType.Name = &quot;Colliers UK Instructed&quot;,  RecordType.Name = &quot;Colliers UK Job Lite&quot;,  RecordType.Name = &quot;Colliers UK Pitching&quot;  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS_Job - Naming Convention</fullName>
        <actions>
            <name>Job_Naming_Convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Job - Naming Convention</description>
        <formula>AND( Name = &quot;x&quot;, OR(  RecordType.Name = &quot;Colliers UK Instructed&quot;,  RecordType.Name = &quot;Colliers UK Job Lite&quot;,  RecordType.Name = &quot;Colliers UK Pitching&quot;  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS_Notification if SOE Company Job created</fullName>
        <actions>
            <name>SOE_Compliance_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>CS_Notification if SOE Company Job created</description>
        <formula>AND(  Account.Is_Government_Body__c = TRUE, OR(  RecordType.Name = &quot;Colliers UK Instructed&quot;,  RecordType.Name = &quot;Colliers UK Job Lite&quot;,  RecordType.Name = &quot;Colliers UK Pitching&quot;  ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CS_Update job record type to Job Lite</fullName>
        <actions>
            <name>Update_Job_record_type_Job_Lite</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Job record type to Job Lite</description>
        <formula>AND ( ISCHANGED( StageName ), ISPICKVAL( StageName, &apos;Job Lite&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS_Update_MLR_Required</fullName>
        <actions>
            <name>CS_Update_MLR_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Amount) &amp;&amp; Amount &gt;= 7500</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS_update pitching record type</fullName>
        <actions>
            <name>Update_Job_record_type_to_pitching</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update job record type to pitching</description>
        <formula>AND( TEXT(PRIORVALUE( StageName )) = &quot;Job Lite&quot;, ISPICKVAL(StageName, &apos;Pitching&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Conflict Check - Date Stamp</fullName>
        <actions>
            <name>Conflict_Check_Date_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.No_property_conflict_found_for_this_job__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>rule to stamp the date of when a conflict check is performed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cs_Ddate company changed</fullName>
        <actions>
            <name>Update_Date_of_Invoice</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>to stamp the date that the invoicing or the instructing company changed</description>
        <formula>OR( AND(ISCHANGED(AccountId),  Account.Is_Government_Body__c = TRUE),   AND(ISCHANGED(  Invoicing_Company2__c ),  Invoicing_Company2__r.Is_Government_Body__c = TRUE )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cs_Update Engagement Letter Date</fullName>
        <actions>
            <name>Update_Engagement_Letter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISCHANGED( Engagement_Letter__c ), Engagement_Letter__c = TRUE, NOT(ISBLANK( Date_Instructed__c )), OR(  RecordType.Name = &quot;Colliers UK Instructed&quot;,  RecordType.Name = &quot;Colliers UK Job Lite&quot;,  RecordType.Name = &quot;Colliers UK Pitching&quot;  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cs_Update_TempJobNumber</fullName>
        <actions>
            <name>Update_Job_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To update a Job Number if the Job Number field is blank</description>
        <formula>AND( ISBLANK( Job_Number__c ), OR(  RecordType.Name = &quot;Colliers UK Instructed&quot;,  RecordType.Name = &quot;Colliers UK Job Lite&quot;,  RecordType.Name = &quot;Colliers UK Pitching&quot;  ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMEA CS Won%2FLost - Close Date</fullName>
        <actions>
            <name>EMEA_CS_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Stage_of_CRM__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Stage_of_CRM__c</field>
            <operation>equals</operation>
            <value>Lost</value>
        </criteriaItems>
        <description>rule so that when EMEA CS close a RFP - wither Won or Lost then the close date is changed to today</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert Opportunity</fullName>
        <actions>
            <name>Sent_Email_when_opportunity_is_created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost,Dropped</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email to BDM for Opportunity Allocation</fullName>
        <active>false</active>
        <description>Email Alert to Opportunity Manager if Opportunity Target Priority is High and Stage is changed
Requested by UK Rating</description>
        <formula>ISCHANGED(  Opportunity_Manager_Staff__c )  &amp;&amp;  ISPICKVAL(Target_Priority__c ,&quot;1&quot;)&amp;&amp;   ISCHANGED(StageName)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email to Rachael Dempster for Opportunity Stage is Terms Signed</fullName>
        <actions>
            <name>Notification_Email_to_Rachael_for_Opportunity_Stage_at_Terms_Signed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notification Email to Rachael Dempster for Opportunity Stage at Terms Signed
Requested by UK Rating</description>
        <formula>ISPICKVAL(StageName, &quot;Terms Signed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Expectations Date Stamp</fullName>
        <actions>
            <name>Date_Stamp_Expectations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Did_our_service_meet_your_expectations__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR Opportunity Close Date in 1 Week</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Initial Implementation 01/2017</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>FR_Opportunity_Close_Date_is_1_week</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>FR Opportunity Created</fullName>
        <actions>
            <name>FR_New_Opportunity</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <description>FR Implementation Request 01/2017</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FR Opportunity GM %3E EUR 100%2C000</fullName>
        <actions>
            <name>FR_Opp_GM_100k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.FR_Fixed_Fee_Tot_Succ_Fee_Tot_GM__c</field>
            <operation>greaterOrEqual</operation>
            <value>&quot;EUR 100,000&quot;</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FR Stamp Close Date on Opp Stage Closed</fullName>
        <actions>
            <name>Opp_Today_s_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <description>Requested by Pauline Martin 22/12/2016 - implemented by RJ 22/12/2016</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FR Update Modified%2FClose Date Field</fullName>
        <actions>
            <name>Stamp_Oppty_Modified_Date_with_NOW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Initial Implementation Requested by FR - 06/2016
Used to sort Opportunity related list as per requirement</description>
        <formula>AND( $User.ProfileId&lt;&gt;&quot;00e24000000rgcx&quot;, RecordTypeId=&quot;012240000006rtt&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR Update Opportunity Stage to Closed Lost When All SL%27s Align</fullName>
        <actions>
            <name>Update_Opportunity_Stage_to_Closed_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Initial Implementation Requested by FR - 06/2016
Update Opportunity Stage to Closed Lost When All Service Lines are Lost or Dropped</description>
        <formula>AND( Number_of_Service_Lines_Lost_Dropped__c = Number_of_Service_Lines__c, Number_of_Service_Lines_Lost_Dropped__c &lt;&gt; 0,RecordType.Id=&apos;012240000006rtt&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR Update Opportunity Stage to Closed Won When All SL%27s Align</fullName>
        <actions>
            <name>Update_Opportunity_Stage_to_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Initial Implementation Requested by FR - 06/2016
Update Opportunity Stage to Closed Won when ALL service lines close and at least one is Closed Won (Contract Signed)</description>
        <formula>AND( Number_of_Service_Lines_Closed__c = Number_of_Service_Lines__c, Number_of_Service_Lines_Won__c &gt; 0,RecordType.Id=&apos;012240000006rtt&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR Update Opportunity Stage to Open When All SL%27s Align</fullName>
        <actions>
            <name>Update_Opportunity_Stage_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Initial Implementation Requested by FR - 06/2016</description>
        <formula>AND( Number_of_Service_Lines_Open__c &gt; Number_of_Service_Lines_Lost_Dropped__c,RecordType.Id=&apos;012240000006rtt&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>High Priority Opportunity Allocation Notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Target_Priority__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Return to Data</value>
        </criteriaItems>
        <description>Notify Opportunity Manager if Target Priority is 1 and Stage is not equal to Return to Data
Requested by UK Rating</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Job Lite Creation - Notification</fullName>
        <actions>
            <name>Job_Lite_Creation_Admin_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Colliers UK Job Lite</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Stage is Return to Data</fullName>
        <actions>
            <name>Oppty_Stage_Return_to_Data_Email_Notification_to_Data_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Return to Data</value>
        </criteriaItems>
        <description>Email Notification to Data Team when Stage is &apos;Return to Data&apos;
Requested by UK Rating</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Stamp When Stage is Changed</fullName>
        <actions>
            <name>Update_Date_Time_of_Stage_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Stage_Prior_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>System: Stamp Opportunity Stage Prior Values inc. Date/Time
Initial Configuration at Pilot UK Rating</description>
        <formula>AND(     ISCHANGED( StageName ),     NOT(ISBLANK(TEXT(StageName))) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Recommend Date Stamp</fullName>
        <actions>
            <name>Date_Stamp_Recommend</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Would_you_recommend_us__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Result Date Stamp</fullName>
        <actions>
            <name>Date_Stamp_Results</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Did_we_deliver_the_intended_result__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stamp Close Date with Today When Oppty Close Won</fullName>
        <actions>
            <name>Stamp_Oppty_Close_Date_with_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>equals</operation>
            <value>100</value>
        </criteriaItems>
        <description>System: When Opportunity Stage is Closed Won Stamp Close Date with Today&apos;s Date
Set by Otto Buchinger as Good Practice</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Stamp Invoicing Name %26 Reporting Client Name</fullName>
        <actions>
            <name>Invoicing_Company_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reporting_Client_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Date Terms Signed on Opp Closed Won</fullName>
        <actions>
            <name>Update_Date_Terms_Signed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Terms Signed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Date_Terms_Signed__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update field &apos;Date Terms Signed&apos; is TODAY on Opportunity Stage is &apos;Terms Signed&apos;
Requested by UK Rating</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
