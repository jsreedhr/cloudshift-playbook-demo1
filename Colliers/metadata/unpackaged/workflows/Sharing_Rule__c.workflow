<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Sharing_rule_unique_name</fullName>
        <field>Unique_Name__c</field>
        <formula>Name</formula>
        <name>Set Sharing rule unique name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set sharing rule unique name</fullName>
        <actions>
            <name>Set_Sharing_rule_unique_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set unique name for sharing rule (based on rule name) - this is necessary to allow use of sharing rule name as unique identifier which is useful e.g. for DataLoader operations on rules &amp; conditions</description>
        <formula>ISNEW() || ISCHANGED(Name) || ISBLANK(Unique_Name__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
