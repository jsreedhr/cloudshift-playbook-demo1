<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Suite_Unit_Name</fullName>
        <field>Name</field>
        <formula>LEFT( (Suite_Unit_Name__c &amp;&apos; &apos;&amp;
 TEXT(Floor__c) &amp;&apos; &apos;&amp;
 Property__r.Name ), 79)</formula>
        <name>Update Suite/Unit Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Suite%2FUnit Name</fullName>
        <actions>
            <name>Update_Suite_Unit_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Floor_Unit__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Colliers UK</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
