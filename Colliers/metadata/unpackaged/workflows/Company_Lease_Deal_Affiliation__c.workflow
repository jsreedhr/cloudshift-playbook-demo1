<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_to_Radius_Sectors</fullName>
        <description>formula to map Colliers Sectors to Radius Sectors</description>
        <field>Radius_Sector__c</field>
        <formula>CASE( Company_Sector__c ,
 &quot;Agriculture&quot;, &quot;Agriculture, Forestry and Fishing&quot;, &quot;Automotive&quot;, &quot;Repair of Motor Vehicles and Motorcycles&quot; ,
&quot;Business Services&quot;, &quot;Administrative and Support Service Activities&quot;,
&quot;Church/Religious Premises&quot;, &quot;Other Service Activities&quot;,
&quot;Computer &amp; IT Companies&quot;, &quot;Manufacturing&quot;,
&quot;Data Centre&quot;, &quot;Information and Communication&quot;,
&quot;Developer&quot;, &quot;Construction&quot;,
&quot;Education&quot;, &quot;Education&quot;,
&quot;Financial Services&quot;, &quot;Financial and Insurance Activities&quot;,
&quot;Fishery/Lake&quot;, &quot;Agriculture, Forestry and Fishing&quot;,
&quot;Government&quot;, &quot;Public Administration and Defence&quot;,
&quot;Healthcare&quot;, &quot;Human Health and Social Work Activities&quot;,
&quot;Hotel&quot;, &quot;Accommodation and Food Service Activities&quot;,
&quot;Industry Body&quot;, &quot;Other Service Providers&quot;,
&quot;Landed Estates &amp; City Liveries&quot;, &quot;Real Estate Activities&quot;,
&quot;Leisure Operators&quot;, &quot;Arts, Entertainment and Recreation&quot;,
&quot;Life Sciences&quot;, &quot;Professional, Scientific and Technical Activities&quot;,
&quot;Logistics &amp; Distribution&quot;, &quot;Transportation and Storage&quot;,
&quot;Manufacturing&quot;, &quot;Manufacturing&quot;,
&quot;Media &amp; Advertising&quot;, &quot;Professional, Scientific and Technical Activities&quot;,
&quot;Mining, Minerals &amp; Waste&quot;, &quot;Mining and Quarrying&quot;,
&quot;Music &amp; Record Company&quot;, &quot;Information and Communication&quot;,
&quot;Non-Governmental Organisation&quot;, &quot;Human Health and Social Work Activities&quot;,
&quot;Private Equity&quot;, &quot;Financial and Insurance Activities&quot;,
&quot;Private Property Companies&quot;, &quot;Real Estate Activities&quot;,
&quot;Private Wealth&quot;, &quot;Financial and Insurance Activities&quot;,
&quot;Professional Services&quot;, &quot;Professional, Scientific and Technical Activities&quot;,
&quot;Real Estate&quot;, &quot;Real Estate Activities&quot;,
&quot;Retailer&quot;, &quot;Wholesale and Retail Trade&quot;,
&quot;Riverside &amp; Canal Operator&quot;, &quot;Other Service Activities&quot;,
&quot;Storage&quot;, &quot;Transportation and Storage&quot;,
&quot;Trade Counter&quot;, &quot;Wholesale and Retail Trade&quot;,
&quot;Transport &amp; Infrastructure&quot;, &quot;Transportation and Storage&quot;,
&quot;Utilities&quot;, &quot;Information and Communication&quot;,
&quot;Waste Management&quot;, &quot;Water Supply, Sewerage, Waste Management and Remediation Services&quot;,
&quot;-&quot;)</formula>
        <name>Update to Radius Sectors</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update to Radius Sector</fullName>
        <actions>
            <name>Update_to_Radius_Sectors</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
