<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Unique_Index</fullName>
        <field>Unique_Index__c</field>
        <formula>CASESAFEID(Sharing_Rule__c) + &apos;_&apos; + TEXT(Index__c)</formula>
        <name>Update Unique Index</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Unique Index</fullName>
        <actions>
            <name>Update_Unique_Index</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When sharing condition is created/updated, update unique index</description>
        <formula>ISNEW() || ISCHANGED(Index__c) || ISBLANK(Unique_Index__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
