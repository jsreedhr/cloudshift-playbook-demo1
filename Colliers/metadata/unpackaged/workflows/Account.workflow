<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Company_Approval_Notification_UK</fullName>
        <description>Company Approval Notification UK</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/UK_Company_Approval</template>
    </alerts>
    <alerts>
        <fullName>Company_Archived_Notification_Process</fullName>
        <description>Company Archived Notification Process</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/UK_Company_Archived</template>
    </alerts>
    <alerts>
        <fullName>Notification_Email_to_UK_Rating_Admin</fullName>
        <description>Notification Email to UK Rating Admin for New Company Record</description>
        <protected>false</protected>
        <recipients>
            <recipient>UK_Rating_Data_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Rating/Notification_of_New_Company_Record_Post_Lead_Conversion</template>
    </alerts>
    <alerts>
        <fullName>Notification_of_New_FR_Company_Record</fullName>
        <ccEmails>laurence.desantis@colliers.com</ccEmails>
        <ccEmails>pauline.martin@colliers.com</ccEmails>
        <ccEmails>otto.buchinger@colliers.com</ccEmails>
        <description>Notification of New FR Company Record</description>
        <protected>false</protected>
        <recipients>
            <recipient>fr.sfmarketing@colliers.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FR/New_Company_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Record_Owner_IJ</fullName>
        <description>When account archived change record owner to Justine Loftus</description>
        <field>OwnerId</field>
        <lookupValue>justine.loftus@colliers.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Account Record Owner - IJ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAF_Label_1</fullName>
        <field>NAF_Label__c</field>
        <formula>CASE( NAF_Code__c , &quot;0111Z&quot;,&quot;Culture de céréales (à l&apos;exception du riz), de légumineuses et de graines oléagineuses&quot;,
&quot;0112Z&quot;,&quot;Culture du riz&quot;,
&quot;0113Z&quot;,&quot;Culture de légumes, de melons, de racines et de tubercules&quot;,
&quot;0114Z&quot;,&quot;Culture de la canne à sucre&quot;,
&quot;0115Z&quot;,&quot;Culture du tabac&quot;,
&quot;0116Z&quot;,&quot;Culture de plantes à fibres&quot;,
&quot;0119Z&quot;,&quot;Autres cultures non permanentes&quot;,
&quot;0121Z&quot;,&quot;Culture de la vigne&quot;,
&quot;0122Z&quot;,&quot;Culture de fruits tropicaux et subtropicaux&quot;,
&quot;0123Z&quot;,&quot;Culture d&apos;agrumes&quot;,
&quot;0124Z&quot;,&quot;Culture de fruits à pépins et à noyau&quot;,
&quot;0125Z&quot;,&quot;Culture d&apos;autres fruits d&apos;arbres ou d&apos;arbustes et de fruits à coque&quot;,
&quot;0126Z&quot;,&quot;Culture de fruits oléagineux&quot;,
&quot;0127Z&quot;,&quot;Culture de plantes à boissons&quot;,
&quot;0128Z&quot;,&quot;Culture de plantes à épices, aromatiques, médicinales et pharmaceutiques&quot;,
&quot;0129Z&quot;,&quot;Autres cultures permanentes&quot;,
&quot;0130Z&quot;,&quot;Reproduction de plantes&quot;,
&quot;0141Z&quot;,&quot;Élevage de vaches laitières&quot;,
&quot;0142Z&quot;,&quot;Élevage d&apos;autres bovins et de buffles&quot;,
&quot;0143Z&quot;,&quot;Élevage de chevaux et d&apos;autres équidés&quot;,
&quot;0144Z&quot;,&quot;Élevage de chameaux et d&apos;autres camélidés&quot;,
&quot;0145Z&quot;,&quot;Élevage d&apos;ovins et de caprins&quot;,
&quot;0146Z&quot;,&quot;Élevage de porcins&quot;,
&quot;0147Z&quot;,&quot;Élevage de volailles&quot;,
&quot;0149Z&quot;,&quot;Élevage d&apos;autres animaux&quot;,
&quot;0150Z&quot;,&quot;Culture et élevage associés&quot;,
&quot;0161Z&quot;,&quot;Activités de soutien aux cultures&quot;,
&quot;0162Z&quot;,&quot;Activités de soutien à la production animale&quot;,
&quot;0163Z&quot;,&quot;Traitement primaire des récoltes&quot;,
&quot;0164Z&quot;,&quot;Traitement des semences&quot;,
&quot;0170Z&quot;,&quot;Chasse, piégeage et services annexes&quot;,
&quot;0210Z&quot;,&quot;Sylviculture et autres activités forestières&quot;,
&quot;0220Z&quot;,&quot;Exploitation forestière&quot;,
&quot;0230Z&quot;,&quot;Récolte de produits forestiers non ligneux poussant à l&apos;état sauvage&quot;,
&quot;0240Z&quot;,&quot;Services de soutien à l&apos;exploitation forestière&quot;,
&quot;0311Z&quot;,&quot;Pêche en mer&quot;,
&quot;0312Z&quot;,&quot;Pêche en eau douce&quot;,
&quot;0321Z&quot;,&quot;Aquaculture en mer&quot;,
&quot;0322Z&quot;,&quot;Aquaculture en eau douce&quot;,
&quot;0510Z&quot;,&quot;Extraction de houille&quot;,
&quot;0520Z&quot;,&quot;Extraction de lignite&quot;,
&quot;0610Z&quot;,&quot;Extraction de pétrole brut&quot;,
&quot;0620Z&quot;,&quot;Extraction de gaz naturel&quot;,
&quot;0710Z&quot;,&quot;Extraction de minerais de fer&quot;,
&quot;0721Z&quot;,&quot;Extraction de minerais d&apos;uranium et de thorium&quot;,
&quot;0729Z&quot;,&quot;Extraction d&apos;autres minerais de métaux non ferreux&quot;,
&quot;0811Z&quot;,&quot;Extraction de pierres ornementales et de construction, de calcaire industriel, de gypse, de craie et d&apos;ardoise&quot;,
&quot;0812Z&quot;,&quot;Exploitation de gravières et sablières, extraction d’argiles et de kaolin&quot;,
&quot;0891Z&quot;,&quot;Extraction des minéraux chimiques et d&apos;engrais minéraux &quot;,
&quot;0892Z&quot;,&quot;Extraction de tourbe&quot;,
&quot;0893Z&quot;,&quot;Production de sel &quot;,
&quot;0899Z&quot;,&quot;Autres activités extractives n.c.a.&quot;,
&quot;0910Z&quot;,&quot;Activités de soutien à l&apos;extraction d&apos;hydrocarbures&quot;,
&quot;0990Z&quot;,&quot;Activités de soutien aux autres industries extractives &quot;,
&quot;1011Z&quot;,&quot;Transformation et conservation de la viande de boucherie&quot;,
&quot;1012Z&quot;,&quot;Transformation et conservation de la viande de volaille&quot;,
&quot;1013A&quot;,&quot;Préparation industrielle de produits à base de viande&quot;,
&quot;1013B&quot;,&quot;Charcuterie&quot;,
&quot;1020Z&quot;,&quot;Transformation et conservation de poisson, de crustacés et de mollusques&quot;,
&quot;1031Z&quot;,&quot;Transformation et conservation de pommes de terre&quot;,
&quot;1032Z&quot;,&quot;Préparation de jus de fruits et légumes&quot;,
&quot;1039A&quot;,&quot;Autre transformation et conservation de légumes&quot;, 
&quot;1039B&quot;,&quot;Transformation et conservation de fruits&quot;, 
&quot;1041A&quot;,&quot;Fabrication d&apos;huiles et graisses brutes&quot;, 
&quot;1041B&quot;,&quot;Fabrication d&apos;huiles et graisses raffinées&quot;, 
&quot;1042Z&quot;,&quot;Fabrication de margarine et graisses comestibles similaires&quot;, 
&quot;1051A&quot;,&quot;Fabrication de lait liquide et de produits frais&quot;, 
&quot;1051B&quot;,&quot;Fabrication de beurre&quot;, 
&quot;1051C&quot;,&quot;Fabrication de fromage&quot;, 
&quot;1051D&quot;,&quot;Fabrication d&apos;autres produits laitiers&quot;, 
&quot;1052Z&quot;,&quot;Fabrication de glaces et sorbets&quot;, 
&quot;1061A&quot;,&quot;Meunerie&quot;, 
&quot;1061B&quot;,&quot;Autres activités du travail des grains&quot;, 
&quot;1062Z&quot;,&quot;Fabrication de produits amylacés&quot;, 
&quot;1071A&quot;,&quot;Fabrication industrielle de pain et de pâtisserie fraîche&quot;, 
&quot;1071B&quot;,&quot;Cuisson de produits de boulangerie&quot;, 
&quot;Null&quot;)</formula>
        <name>NAF Label 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAF_Label_10</fullName>
        <field>NAF_Label__c</field>
        <formula>CASE(NAF_Code__c,
&quot;6820B&quot;,&quot;Location de terrains et d&apos;autres biens immobiliers&quot;,
&quot;6831Z&quot;,&quot;Agences immobilières&quot;,
&quot;6832A&quot;,&quot;Administration d&apos;immeubles et autres biens immobiliers&quot;,
&quot;6832B&quot;,&quot;Supports juridiques de gestion de patrimoine immobilier&quot;,
&quot;6910Z&quot;,&quot;Activités juridiques&quot;,
&quot;6920Z&quot;,&quot;Activités comptables&quot;,
&quot;7010Z&quot;,&quot;Activités des sièges sociaux&quot;,
&quot;7021Z&quot;,&quot;Conseil en relations publiques et communication&quot;,
&quot;7022Z&quot;,&quot;Conseil pour les affaires et autres conseils de gestion&quot;,
&quot;7111Z&quot;,&quot;Activités d&apos;architecture &quot;,
&quot;7112A&quot;,&quot;Activité des géomètres&quot;,
&quot;7112B&quot;,&quot;Ingénierie, études techniques&quot;,
&quot;7120A&quot;,&quot;Contrôle technique automobile&quot;,
&quot;7120B&quot;,&quot;Analyses, essais et inspections techniques&quot;,
&quot;7211Z&quot;,&quot;Recherche-développement en biotechnologie&quot;,
&quot;7219Z&quot;,&quot;Recherche-développement en autres sciences physiques et naturelles&quot;,
&quot;7220Z&quot;,&quot;Recherche-développement en sciences humaines et sociales&quot;,
&quot;7311Z&quot;,&quot;Activités des agences de publicité&quot;,
&quot;7312Z&quot;,&quot;Régie publicitaire de médias&quot;,
&quot;7320Z&quot;,&quot;Études de marché et sondages&quot;,
&quot;7410Z&quot;,&quot;Activités spécialisées de design&quot;,
&quot;7420Z&quot;,&quot;Activités photographiques&quot;,
&quot;7430Z&quot;,&quot;Traduction et interprétation&quot;,
&quot;7490A&quot;,&quot;Activité des économistes de la construction&quot;,
&quot;7490B&quot;,&quot;Activités spécialisées, scientifiques et techniques diverses&quot;,
&quot;7500Z&quot;,&quot;Activités vétérinaires&quot;,
&quot;7711A&quot;,&quot;Location de courte durée de voitures et de véhicules automobiles légers&quot;,
&quot;7711B&quot;,&quot;Location de longue durée de voitures et de véhicules automobiles légers&quot;,
&quot;7712Z&quot;,&quot;Location et location-bail de camions&quot;,
&quot;7721Z&quot;,&quot;Location et location-bail d&apos;articles de loisirs et de sport &quot;,
&quot;7722Z&quot;,&quot;Location de vidéocassettes et disques vidéo&quot;,
&quot;7729Z&quot;,&quot;Location et location-bail d&apos;autres biens personnels et domestiques&quot;,
&quot;7731Z&quot;,&quot;Location et location-bail de machines et équipements agricoles&quot;,
&quot;7732Z&quot;,&quot;Location et location-bail de machines et équipements pour la construction&quot;,
&quot;7733Z&quot;,&quot;Location et location-bail de machines de bureau et de matériel informatique&quot;,
&quot;7734Z&quot;,&quot;Location et location-bail de matériels de transport par eau&quot;,
&quot;7735Z&quot;,&quot;Location et location-bail de matériels de transport aérien&quot;,
&quot;7739Z&quot;,&quot;Location et location-bail d&apos;autres machines, équipements et biens matériels n.c.a. &quot;,
&quot;7740Z&quot;,&quot;Location-bail de propriété intellectuelle et de produits similaires, à l&apos;exception des œuvres soumises à copyright&quot;,
&quot;7810Z&quot;,&quot;Activités des agences de placement de main-d&apos;œuvre &quot;,
&quot;7820Z&quot;,&quot;Activités des agences de travail temporaire &quot;,
&quot;7830Z&quot;,&quot;Autre mise à disposition de ressources humaines&quot;,
&quot;7911Z&quot;,&quot;Activités des agences de voyage&quot;,
&quot;7912Z&quot;,&quot;Activités des voyagistes&quot;,
&quot;7990Z&quot;,&quot;Autres services de réservation et activités connexes&quot;,
&quot;8010Z&quot;,&quot;Activités de sécurité privée &quot;,
&quot;8020Z&quot;,&quot;Activités liées aux systèmes de sécurité &quot;,
&quot;8030Z&quot;,&quot;Activités d&apos;enquête&quot;,
&quot;8110Z&quot;,&quot;Activités combinées de soutien lié aux bâtiments &quot;,
&quot;8121Z&quot;,&quot;Nettoyage courant des bâtiments&quot;,
&quot;8122Z&quot;,&quot;Autres activités de nettoyage des bâtiments et nettoyage industriel&quot;,
&quot;8129A&quot;,&quot;Désinfection, désinsectisation, dératisation&quot;,
&quot;8129B&quot;,&quot;Autres activités de nettoyage n.c.a.&quot;,
&quot;8130Z&quot;,&quot;Services d&apos;aménagement paysager &quot;,
&quot;8211Z&quot;,&quot;Services administratifs combinés de bureau&quot;,
&quot;8219Z&quot;,&quot;Photocopie, préparation de documents et autres activités spécialisées de soutien de bureau&quot;,
&quot;8220Z&quot;,&quot;Activités de centres d&apos;appels&quot;,
&quot;8230Z&quot;,&quot;Organisation de foires, salons professionnels et congrès&quot;,
&quot;8291Z&quot;,&quot;Activités des agences de recouvrement de factures et des sociétés d&apos;information financière sur la clientèle&quot;,
&quot;8292Z&quot;,&quot;Activités de conditionnement&quot;,
&quot;8299Z&quot;,&quot;Autres activités de soutien aux entreprises n.c.a.&quot;,
&quot;8411Z&quot;,&quot;Administration publique générale&quot;,
&quot;8412Z&quot;,&quot;Administration publique (tutelle) de la santé, de la formation, de la culture et des services sociaux, autre que sécurité sociale &quot;,
&quot;Null&quot;)</formula>
        <name>NAF Label 10</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAF_Label_11</fullName>
        <field>NAF_Label__c</field>
        <formula>CASE(NAF_Code__c, 
&quot;8413Z&quot;,&quot;Administration publique (tutelle) des activités économiques&quot;,
&quot;8421Z&quot;,&quot;Affaires étrangères&quot;,
&quot;8422Z&quot;,&quot;Défense&quot;,
&quot;8423Z&quot;,&quot;Justice&quot;,
&quot;8424Z&quot;,&quot;Activités d’ordre public et de sécurité&quot;,
&quot;8425Z&quot;,&quot;Services du feu et de secours&quot;,
&quot;8430A&quot;,&quot;Activités générales de sécurité sociale&quot;,
&quot;8430B&quot;,&quot;Gestion des retraites complémentaires&quot;,
&quot;8430C&quot;,&quot;Distribution sociale de revenus&quot;,
&quot;8510Z&quot;,&quot;Enseignement pré-primaire&quot;,
&quot;8520Z&quot;,&quot;Enseignement primaire&quot;,
&quot;8531Z&quot;,&quot;Enseignement secondaire général&quot;,
&quot;8532Z&quot;,&quot;Enseignement secondaire technique ou professionnel&quot;,
&quot;8541Z&quot;,&quot;Enseignement post-secondaire non supérieur&quot;,
&quot;8542Z&quot;,&quot;Enseignement supérieur&quot;,
&quot;8551Z&quot;,&quot;Enseignement de disciplines sportives et d&apos;activités de loisirs&quot;,
&quot;8552Z&quot;,&quot;Enseignement culturel&quot;, 
&quot;8553Z&quot;,&quot;Enseignement de la conduite&quot;, 
&quot;8559A&quot;,&quot;Formation continue d&apos;adultes&quot;, 
&quot;8559B&quot;,&quot;Autres enseignements&quot;, 
&quot;8560Z&quot;,&quot;Activités de soutien à l&apos;enseignement&quot;, 
&quot;8610Z&quot;,&quot;Activités hospitalières&quot;, 
&quot;8621Z&quot;,&quot;Activité des médecins généralistes&quot;, 
&quot;8622A&quot;,&quot;Activités de radiodiagnostic et de radiothérapie&quot;, 
&quot;8622B&quot;,&quot;Activités chirurgicales&quot;, 
&quot;8622C&quot;,&quot;Autres activités des médecins spécialistes&quot;, 
&quot;8623Z&quot;,&quot;Pratique dentaire&quot;, 
&quot;8690A&quot;,&quot;Ambulances&quot;, 
&quot;8690B&quot;,&quot;Laboratoires d&apos;analyses médicales&quot;, 
&quot;8690C&quot;,&quot;Centres de collecte et banques d&apos;organes&quot;, 
&quot;8690D&quot;,&quot;Activités des infirmiers et des sages-femmes&quot;, 
&quot;8690E&quot;,&quot;Activités des professionnels de la rééducation, de l’appareillage et des pédicures-podologues&quot;, 
&quot;8690F&quot;,&quot;Activités de santé humaine non classées ailleurs&quot;, 
&quot;8710A&quot;,&quot;Hébergement médicalisé pour personnes âgées&quot;, 
&quot;8710B&quot;,&quot;Hébergement médicalisé pour enfants handicapés &quot;, 
&quot;8710C&quot;,&quot;Hébergement médicalisé pour adultes handicapés et autre hébergement médicalisé&quot;, 
&quot;8720A&quot;,&quot;Hébergement social pour handicapés mentaux et malades mentaux &quot;, 
&quot;8720B&quot;,&quot;Hébergement social pour toxicomanes&quot;, 
&quot;8730A&quot;,&quot;Hébergement social pour personnes âgées&quot;, 
&quot;8730B&quot;,&quot;Hébergement social pour handicapés physiques&quot;, 
&quot;8790A&quot;,&quot;Hébergement social pour enfants en difficultés &quot;, 
&quot;8790B&quot;,&quot;Hébergement social pour adultes et familles en difficultés et autre hébergement social &quot;, 
&quot;8810A&quot;,&quot;Aide à domicile &quot;, 
&quot;8810B&quot;,&quot;Accueil ou accompagnement sans hébergement d’adultes handicapés ou de personnes âgées&quot;, 
&quot;8810C&quot;,&quot;Aide par le travail &quot;, 
&quot;8891A&quot;,&quot;Accueil de jeunes enfants&quot;, 
&quot;8891B&quot;,&quot;Accueil ou accompagnement sans hébergement d’enfants handicapés&quot;, 
&quot;8899A&quot;,&quot;Autre accueil ou accompagnement sans hébergement d’enfants et d’adolescents&quot;, 
&quot;8899B&quot;,&quot;Action sociale sans hébergement n.c.a.&quot;, 
&quot;9001Z&quot;,&quot;Arts du spectacle vivant&quot;, 
&quot;9002Z&quot;,&quot;Activités de soutien au spectacle vivant&quot;, 
&quot;9003A&quot;,&quot;Création artistique relevant des arts plastiques&quot;, 
&quot;9003B&quot;,&quot;Autre création artistique&quot;, 
&quot;9004Z&quot;,&quot;Gestion de salles de spectacles&quot;, 
&quot;9101Z&quot;,&quot;Gestion des bibliothèques et des archives&quot;, 
&quot;9102Z&quot;,&quot;Gestion des musées&quot;, 
&quot;9103Z&quot;,&quot;Gestion des sites et monuments historiques et des attractions touristiques similaires&quot;, 
&quot;9104Z&quot;,&quot;Gestion des jardins botaniques et zoologiques et des réserves naturelles&quot;, 
&quot;9200Z&quot;,&quot;Organisation de jeux de hasard et d&apos;argent&quot;, 
&quot;9311Z&quot;,&quot;Gestion d&apos;installations sportives&quot;, 
&quot;9312Z&quot;,&quot;Activités de clubs de sports&quot;, 
&quot;9313Z&quot;,&quot;Activités des centres de culture physique&quot;, 
&quot;9319Z&quot;,&quot;Autres activités liées au sport&quot;, 
&quot;Null&quot;)</formula>
        <name>NAF Label 11</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAF_Label_12</fullName>
        <field>NAF_Label__c</field>
        <formula>CASE(NAF_Code__c, 
&quot;9321Z&quot;,&quot;Activités des parcs d&apos;attractions et parcs à thèmes&quot;,
&quot;9329Z&quot;,&quot;Autres activités récréatives et de loisirs&quot;,
&quot;9411Z&quot;,&quot;Activités des organisations patronales et consulaires&quot;,
&quot;9412Z&quot;,&quot;Activités des organisations professionnelles&quot;,
&quot;9420Z&quot;,&quot;Activités des syndicats de salariés&quot;,
&quot;9491Z&quot;,&quot;Activités des organisations religieuses&quot;,
&quot;9492Z&quot;,&quot;Activités des organisations politiques&quot;,
&quot;9499Z&quot;,&quot;Autres organisations fonctionnant par adhésion volontaire&quot;,
&quot;9511Z&quot;,&quot;Réparation d&apos;ordinateurs et d&apos;équipements périphériques&quot;,
&quot;9512Z&quot;,&quot;Réparation d&apos;équipements de communication&quot;,
&quot;9521Z&quot;,&quot;Réparation de produits électroniques grand public&quot;,
&quot;9522Z&quot;,&quot;Réparation d&apos;appareils électroménagers et d&apos;équipements pour la maison et le jardin&quot;,
&quot;9523Z&quot;,&quot;Réparation de chaussures et d&apos;articles en cuir&quot;,
&quot;9524Z&quot;,&quot;Réparation de meubles et d&apos;équipements du foyer&quot;,
&quot;9525Z&quot;,&quot;Réparation d&apos;articles d&apos;horlogerie et de bijouterie&quot;,
&quot;9529Z&quot;,&quot;Réparation d&apos;autres biens personnels et domestiques&quot;,
&quot;9601A&quot;,&quot;Blanchisserie-teinturerie de gros&quot;,
&quot;9601B&quot;,&quot;Blanchisserie-teinturerie de détail&quot;,
&quot;9602A&quot;,&quot;Coiffure&quot;,
&quot;9602B&quot;,&quot;Soins de beauté&quot;,
&quot;9603Z&quot;,&quot;Services funéraires&quot;,
&quot;9604Z&quot;,&quot;Entretien corporel&quot;,
&quot;9609Z&quot;,&quot;Autres services personnels n.c.a.&quot;,
&quot;9700Z&quot;,&quot;Activités des ménages en tant qu&apos;employeurs de personnel domestique&quot;,
&quot;9810Z&quot;,&quot;Activités indifférenciées des ménages en tant que producteurs de biens pour usage propre&quot;,
&quot;9820Z&quot;,&quot;Activités indifférenciées des ménages en tant que producteurs de services pour usage propre&quot;,
&quot;9900Z&quot;,&quot;Activités des organisations et organismes extraterritoriaux&quot;,
&quot;Null&quot;)</formula>
        <name>NAF Label 12</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAF_Label_2</fullName>
        <field>NAF_Label__c</field>
        <formula>CASE( NAF_Code__c ,
&quot;1071C&quot;,&quot;Boulangerie et boulangerie-pâtisserie&quot;,
&quot;1071D&quot;,&quot;Pâtisserie&quot;,
&quot;1072Z&quot;,&quot;Fabrication de biscuits, biscottes et pâtisseries de conservation&quot;,
&quot;1073Z&quot;,&quot;Fabrication de pâtes alimentaires&quot;,
&quot;1081Z&quot;,&quot;Fabrication de sucre&quot;,
&quot;1082Z&quot;,&quot;Fabrication de cacao, chocolat et de produits de confiserie&quot;,
&quot;1083Z&quot;,&quot;Transformation du thé et du café&quot;,
&quot;1084Z&quot;,&quot;Fabrication de condiments et assaisonnements&quot;,
&quot;1085Z&quot;,&quot;Fabrication de plats préparés&quot;,
&quot;1086Z&quot;,&quot;Fabrication d&apos;aliments homogénéisés et diététiques&quot;,
&quot;1089Z&quot;,&quot;Fabrication d&apos;autres produits alimentaires n.c.a.&quot;,
&quot;1091Z&quot;,&quot;Fabrication d&apos;aliments pour animaux de ferme&quot;,
&quot;1092Z&quot;,&quot;Fabrication d&apos;aliments pour animaux de compagnie&quot;,
&quot;1101Z&quot;,&quot;Production de boissons alcooliques distillées&quot;,
&quot;1102A&quot;,&quot;Fabrication de vins effervescents&quot;,
&quot;1102B&quot;,&quot;Vinification&quot;,
&quot;1103Z&quot;,&quot;Fabrication de cidre et de vins de fruits &quot;,
&quot;1104Z&quot;,&quot;Production d&apos;autres boissons fermentées non distillées&quot;,
&quot;1105Z&quot;,&quot;Fabrication de bière&quot;,
&quot;1106Z&quot;,&quot;Fabrication de malt&quot;,
&quot;1107A&quot;,&quot;Industrie des eaux de table&quot;,
&quot;1107B&quot;,&quot;Production de boissons rafraîchissantes&quot;,
&quot;1200Z&quot;,&quot;Fabrication de produits à base de tabac&quot;,
&quot;1310Z&quot;,&quot;Préparation de fibres textiles et filature&quot;,
&quot;1320Z&quot;,&quot;Tissage&quot;,
&quot;1330Z&quot;,&quot;Ennoblissement textile&quot;,
&quot;1391Z&quot;,&quot;Fabrication d&apos;étoffes à mailles&quot;,
&quot;1392Z&quot;,&quot;Fabrication d&apos;articles textiles, sauf habillement&quot;,
&quot;1393Z&quot;,&quot;Fabrication de tapis et moquettes&quot;,
&quot;1394Z&quot;,&quot;Fabrication de ficelles, cordes et filets&quot;,
&quot;1395Z&quot;,&quot;Fabrication de non-tissés, sauf habillement&quot;,
&quot;1396Z&quot;,&quot;Fabrication d&apos;autres textiles techniques et industriels&quot;,
&quot;1399Z&quot;,&quot;Fabrication d&apos;autres textiles n.c.a.&quot;,
&quot;1411Z&quot;,&quot;Fabrication de vêtements en cuir&quot;,
&quot;1412Z&quot;,&quot;Fabrication de vêtements de travail&quot;,
&quot;1413Z&quot;,&quot;Fabrication de vêtements de dessus&quot;,
&quot;1414Z&quot;,&quot;Fabrication de vêtements de dessous&quot;,
&quot;1419Z&quot;,&quot;Fabrication d&apos;autres vêtements et accessoires&quot;,
&quot;1420Z&quot;,&quot;Fabrication d&apos;articles en fourrure&quot;,
&quot;1431Z&quot;,&quot;Fabrication d&apos;articles chaussants à mailles&quot;,
&quot;1439Z&quot;,&quot;Fabrication d&apos;autres articles à mailles&quot;,
&quot;1511Z&quot;,&quot;Apprêt et tannage des cuirs ; préparation et teinture des fourrures&quot;,
&quot;1512Z&quot;,&quot;Fabrication d&apos;articles de voyage, de maroquinerie et de sellerie&quot;,
&quot;1520Z&quot;,&quot;Fabrication de chaussures&quot;,
&quot;1610A&quot;,&quot;Sciage et rabotage du bois, hors imprégnation&quot;,
&quot;1610B&quot;,&quot;Imprégnation du bois&quot;,
&quot;1621Z&quot;,&quot;Fabrication de placage et de panneaux de bois&quot;,
&quot;1622Z&quot;,&quot;Fabrication de parquets assemblés&quot;, 
&quot;1623Z&quot;,&quot;Fabrication de charpentes et d&apos;autres menuiseries&quot;, 
&quot;1624Z&quot;,&quot;Fabrication d&apos;emballages en bois&quot;, 
&quot;1629Z&quot;,&quot;Fabrication d&apos;objets divers en bois ; fabrication d&apos;objets en liège, vannerie et sparterie&quot;, 
&quot;1711Z&quot;,&quot;Fabrication de pâte à papier&quot;, 
&quot;1712Z&quot;,&quot;Fabrication de papier et de carton&quot;, 
&quot;1721A&quot;,&quot;Fabrication de carton ondulé&quot;, 
&quot;1721B&quot;,&quot;Fabrication de cartonnages &quot;, 
&quot;1721C&quot;,&quot;Fabrication d&apos;emballages en papier&quot;, 
&quot;1722Z&quot;,&quot;Fabrication d&apos;articles en papier à usage sanitaire ou domestique&quot;, 
&quot;1723Z&quot;,&quot;Fabrication d&apos;articles de papeterie&quot;, 
&quot;1724Z&quot;,&quot;Fabrication de papiers peints&quot;, 
&quot;1729Z&quot;,&quot;Fabrication d&apos;autres articles en papier ou en carton&quot;, 
&quot;1811Z&quot;,&quot;Imprimerie de journaux&quot;, 
&quot;1812Z&quot;,&quot;Autre imprimerie (labeur)&quot;, 
&quot;1813Z&quot;,&quot;Activités de pré-presse &quot;, 
&quot;1814Z&quot;,&quot;Reliure et activités connexes&quot;, 
&quot;1820Z&quot;,&quot;Reproduction d&apos;enregistrements&quot;, 
&quot;1910Z&quot;,&quot;Cokéfaction&quot;,
&quot;1920Z&quot;,&quot;Raffinage du pétrole&quot;,
&quot;2011Z&quot;,&quot;Fabrication de gaz industriels&quot;,  
&quot;2012Z&quot;,&quot;Fabrication de colorants et de pigments&quot;, 
&quot;2013A&quot;,&quot;Enrichissement et retraitement de matières nucléaires&quot;, 
&quot;2013B&quot;,&quot;Fabrication d&apos;autres produits chimiques inorganiques de base n.c.a.&quot;, 
&quot;2014Z&quot;,&quot;Fabrication d&apos;autres produits chimiques organiques de base&quot;, 
&quot;2015Z&quot;,&quot;Fabrication de produits azotés et d&apos;engrais&quot;, 
&quot;2016Z&quot;,&quot;Fabrication de matières plastiques de base&quot;, 
&quot;2017Z&quot;,&quot;Fabrication de caoutchouc synthétique&quot;,  
&quot;Null&quot;)</formula>
        <name>NAF Label 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAF_Label_3</fullName>
        <field>NAF_Label__c</field>
        <formula>CASE( NAF_Code__c ,
&quot;2020Z&quot;,&quot;Fabrication de pesticides et d’autres produits agrochimiques&quot;,
&quot;2030Z&quot;,&quot;Fabrication de peintures, vernis, encres et mastics&quot;,
&quot;2041Z&quot;,&quot;Fabrication de savons, détergents et produits d&apos;entretien&quot;,
&quot;2042Z&quot;,&quot;Fabrication de parfums et de produits pour la toilette&quot;,
&quot;2051Z&quot;,&quot;Fabrication de produits explosifs&quot;,
&quot;2052Z&quot;,&quot;Fabrication de colles&quot;,
&quot;2053Z&quot;,&quot;Fabrication d&apos;huiles essentielles&quot;,
&quot;2059Z&quot;,&quot;Fabrication d&apos;autres produits chimiques n.c.a.&quot;,
&quot;2060Z&quot;,&quot;Fabrication de fibres artificielles ou synthétiques&quot;,
&quot;2110Z&quot;,&quot;Fabrication de produits pharmaceutiques de base&quot;,
&quot;2120Z&quot;,&quot;Fabrication de préparations pharmaceutiques&quot;,
&quot;2211Z&quot;,&quot;Fabrication et rechapage de pneumatiques&quot;,
&quot;2219Z&quot;,&quot;Fabrication d&apos;autres articles en caoutchouc&quot;,
&quot;2221Z&quot;,&quot;Fabrication de plaques, feuilles, tubes et profilés en matières plastiques&quot;,
&quot;2222Z&quot;,&quot;Fabrication d&apos;emballages en matières plastiques&quot;,
&quot;2223Z&quot;,&quot;Fabrication d&apos;éléments en matières plastiques pour la construction&quot;,
&quot;2229A&quot;,&quot;Fabrication de pièces techniques à base de matières plastiques&quot;,
&quot;2229B&quot;,&quot;Fabrication de produits de consommation courante en matières plastiques&quot;,
&quot;2311Z&quot;,&quot;Fabrication de verre plat&quot;,
&quot;2312Z&quot;,&quot;Façonnage et transformation du verre plat&quot;,
&quot;2313Z&quot;,&quot;Fabrication de verre creux&quot;,
&quot;2314Z&quot;,&quot;Fabrication de fibres de verre&quot;,
&quot;2319Z&quot;,&quot;Fabrication et façonnage d&apos;autres articles en verre, y compris verre technique&quot;,
&quot;2320Z&quot;,&quot;Fabrication de produits réfractaires&quot;,
&quot;2331Z&quot;,&quot;Fabrication de carreaux en céramique&quot;,
&quot;2332Z&quot;,&quot;Fabrication de briques, tuiles et produits de construction, en terre cuite&quot;,
&quot;2341Z&quot;,&quot;Fabrication d&apos;articles céramiques à usage domestique ou ornemental&quot;,
&quot;2342Z&quot;,&quot;Fabrication d&apos;appareils sanitaires en céramique&quot;,
&quot;2343Z&quot;,&quot;Fabrication d&apos;isolateurs et pièces isolantes en céramique&quot;,
&quot;2344Z&quot;,&quot;Fabrication d&apos;autres produits céramiques à usage technique&quot;,
&quot;2349Z&quot;,&quot;Fabrication d&apos;autres produits céramiques&quot;,
&quot;2351Z&quot;,&quot;Fabrication de ciment&quot;,
&quot;2352Z&quot;,&quot;Fabrication de chaux et plâtre&quot;,
&quot;2361Z&quot;,&quot;Fabrication d&apos;éléments en béton pour la construction&quot;,
&quot;2362Z&quot;,&quot;Fabrication d&apos;éléments en plâtre pour la construction&quot;, 
&quot;2363Z&quot;,&quot;Fabrication de béton prêt à l&apos;emploi&quot;, 
&quot;2364Z&quot;,&quot;Fabrication de mortiers et bétons secs&quot;, 
&quot;2365Z&quot;,&quot;Fabrication d&apos;ouvrages en fibre-ciment&quot;, 
&quot;2369Z&quot;,&quot;Fabrication d&apos;autres ouvrages en béton, en ciment ou en plâtre&quot;, 
&quot;2370Z&quot;,&quot;Taille, façonnage et finissage de pierres&quot;, 
&quot;2391Z&quot;,&quot;Fabrication de produits abrasifs&quot;, 
&quot;2399Z&quot;,&quot;Fabrication d&apos;autres produits minéraux non métalliques n.c.a.&quot;, 
&quot;2410Z&quot;,&quot;Sidérurgie&quot;, 
&quot;2420Z&quot;,&quot;Fabrication de tubes, tuyaux, profilés creux et accessoires correspondants en acier &quot;, 
&quot;2431Z&quot;,&quot;Étirage à froid de barres&quot;, 
&quot;2432Z&quot;,&quot;Laminage à froid de feuillards&quot;, 
&quot;2433Z&quot;,&quot;Profilage à froid par formage ou pliage&quot;, 
&quot;2434Z&quot;,&quot;Tréfilage à froid&quot;, 
&quot;2441Z&quot;,&quot;Production de métaux précieux&quot;, 
&quot;2442Z&quot;,&quot;Métallurgie de l&apos;aluminium&quot;, 
&quot;2443Z&quot;,&quot;Métallurgie du plomb, du zinc ou de l&apos;étain&quot;, 
&quot;2444Z&quot;,&quot;Métallurgie du cuivre&quot;, 
&quot;2445Z&quot;,&quot;Métallurgie des autres métaux non ferreux&quot;,
&quot;2446Z&quot;,&quot;Élaboration et transformation de matières nucléaires&quot;,
&quot;2451Z&quot;,&quot;Fonderie de fonte&quot;,
&quot;2452Z&quot;,&quot;Fonderie d&apos;acier&quot;,
&quot;2453Z&quot;,&quot;Fonderie de métaux légers&quot;,
&quot;2454Z&quot;,&quot;Fonderie d&apos;autres métaux non ferreux&quot;,
&quot;2511Z&quot;,&quot;Fabrication de structures métalliques et de parties de structures&quot;,
&quot;2512Z&quot;,&quot;Fabrication de portes et fenêtres en métal&quot;,
&quot;2521Z&quot;,&quot;Fabrication de radiateurs et de chaudières pour le chauffage central&quot;,
&quot;2529Z&quot;,&quot;Fabrication d&apos;autres réservoirs, citernes et conteneurs métalliques&quot;,
&quot;2530Z&quot;,&quot;Fabrication de générateurs de vapeur, à l&apos;exception des chaudières pour le chauffage central&quot;,
&quot;2540Z&quot;,&quot;Fabrication d&apos;armes et de munitions&quot;,
&quot;2550A&quot;,&quot;Forge, estampage, matriçage ; métallurgie des poudres&quot;,
&quot;2550B&quot;,&quot;Découpage, emboutissage&quot;,
&quot;2561Z&quot;,&quot;Traitement et revêtement des métaux&quot;,
&quot;Null&quot;)</formula>
        <name>NAF Label 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAF_Label_4</fullName>
        <field>NAF_Label__c</field>
        <formula>CASE( NAF_Code__c ,
&quot;2562A&quot;,&quot;Décolletage&quot;,
&quot;2562B&quot;,&quot;Mécanique industrielle&quot;,
&quot;2571Z&quot;,&quot;Fabrication de coutellerie&quot;,
&quot;2572Z&quot;,&quot;Fabrication de serrures et de ferrures&quot;,
&quot;2573A&quot;,&quot;Fabrication de moules et modèles&quot;,
&quot;2573B&quot;,&quot;Fabrication d&apos;autres outillages&quot;,
&quot;2591Z&quot;,&quot;Fabrication de fûts et emballages métalliques similaires&quot;,
&quot;2592Z&quot;,&quot;Fabrication d&apos;emballages métalliques légers&quot;,
&quot;2593Z&quot;,&quot;Fabrication d&apos;articles en fils métalliques, de chaînes et de ressorts&quot;,
&quot;2594Z&quot;,&quot;Fabrication de vis et de boulons&quot;,
&quot;2599A&quot;,&quot;Fabrication d&apos;articles métalliques ménagers&quot;,
&quot;2599B&quot;,&quot;Fabrication d&apos;autres articles métalliques&quot;,
&quot;2611Z&quot;,&quot;Fabrication de composants électroniques&quot;,
&quot;2612Z&quot;,&quot;Fabrication de cartes électroniques assemblées&quot;,
&quot;2620Z&quot;,&quot;Fabrication d&apos;ordinateurs et d&apos;équipements périphériques&quot;,
&quot;2630Z&quot;,&quot;Fabrication d&apos;équipements de communication &quot;,
&quot;2640Z&quot;,&quot;Fabrication de produits électroniques grand public&quot;,
&quot;2651A&quot;,&quot;Fabrication d&apos;équipements d&apos;aide à la navigation&quot;,
&quot;2651B&quot;,&quot;Fabrication d&apos;instrumentation scientifique et technique&quot;,
&quot;2652Z&quot;,&quot;Horlogerie&quot;,
&quot;2660Z&quot;,&quot;Fabrication d&apos;équipements d&apos;irradiation médicale, d&apos;équipements électromédicaux et électrothérapeutiques &quot;,
&quot;2670Z&quot;,&quot;Fabrication de matériels optique et photographique&quot;,
&quot;2680Z&quot;,&quot;Fabrication de supports magnétiques et optiques&quot;,
&quot;2711Z&quot;,&quot;Fabrication de moteurs, génératrices et transformateurs électriques&quot;,
&quot;2712Z&quot;,&quot;Fabrication de matériel de distribution et de commande électrique&quot;,
&quot;2720Z&quot;,&quot;Fabrication de piles et d&apos;accumulateurs électriques&quot;,
&quot;2731Z&quot;,&quot;Fabrication de câbles de fibres optiques&quot;,
&quot;2732Z&quot;,&quot;Fabrication d&apos;autres fils et câbles électroniques ou électriques&quot;,
&quot;2733Z&quot;,&quot;Fabrication de matériel d&apos;installation électrique&quot;,
&quot;2740Z&quot;,&quot;Fabrication d&apos;appareils d&apos;éclairage électrique&quot;,
&quot;2751Z&quot;,&quot;Fabrication d&apos;appareils électroménagers&quot;,
&quot;2752Z&quot;,&quot;Fabrication d&apos;appareils ménagers non électriques&quot;,
&quot;2790Z&quot;,&quot;Fabrication d&apos;autres matériels électriques&quot;,
&quot;2811Z&quot;,&quot;Fabrication de moteurs et turbines, à l&apos;exception des moteurs d’avions et de véhicules&quot;,
&quot;2812Z&quot;,&quot;Fabrication d&apos;équipements hydrauliques et pneumatiques&quot;,
&quot;2813Z&quot;,&quot;Fabrication d&apos;autres pompes et compresseurs&quot;,
&quot;2814Z&quot;,&quot;Fabrication d&apos;autres articles de robinetterie&quot;,
&quot;2815Z&quot;,&quot;Fabrication d&apos;engrenages et d&apos;organes mécaniques de transmission&quot;,
&quot;2821Z&quot;,&quot;Fabrication de fours et brûleurs&quot;,
&quot;2822Z&quot;,&quot;Fabrication de matériel de levage et de manutention&quot;,
&quot;2823Z&quot;,&quot;Fabrication de machines et d&apos;équipements de bureau (à l&apos;exception des ordinateurs et équipements périphériques)&quot;,
&quot;2824Z&quot;,&quot;Fabrication d&apos;outillage portatif à moteur incorporé&quot;,
&quot;2825Z&quot;,&quot;Fabrication d&apos;équipements aérauliques et frigorifiques industriels&quot;,
&quot;2829A&quot;,&quot;Fabrication d&apos;équipements d&apos;emballage, de conditionnement et de pesage &quot;,
&quot;2829B&quot;,&quot;Fabrication d&apos;autres machines d&apos;usage général&quot;,
&quot;2830Z&quot;,&quot;Fabrication de machines agricoles et forestières&quot;,
&quot;2841Z&quot;,&quot;Fabrication de machines-outils pour le travail des métaux&quot;,
&quot;2849Z&quot;,&quot;Fabrication d&apos;autres machines-outils &quot;,
&quot;2891Z&quot;,&quot;Fabrication de machines pour la métallurgie&quot;,
&quot;2892Z&quot;,&quot;Fabrication de machines pour l&apos;extraction ou la construction&quot;,
&quot;2893Z&quot;,&quot;Fabrication de machines pour l&apos;industrie agro-alimentaire&quot;,
&quot;2894Z&quot;,&quot;Fabrication de machines pour les industries textiles&quot;,
&quot;2895Z&quot;,&quot;Fabrication de machines pour les industries du papier et du carton&quot;,
&quot;2896Z&quot;,&quot;Fabrication de machines pour le travail du caoutchouc ou des plastiques&quot;,
&quot;2899A&quot;,&quot;Fabrication de machines d&apos;imprimerie&quot;,
&quot;2899B&quot;,&quot;Fabrication d&apos;autres machines spécialisées&quot;,
&quot;2910Z&quot;,&quot;Construction de véhicules automobiles&quot;,
&quot;2920Z&quot;,&quot;Fabrication de carrosseries et remorques&quot;,
&quot;2931Z&quot;,&quot;Fabrication d&apos;équipements électriques et électroniques automobiles&quot;,
&quot;2932Z&quot;,&quot;Fabrication d&apos;autres équipements automobiles&quot;,
&quot;3011Z&quot;,&quot;Construction de navires et de structures flottantes&quot;,
&quot;Null&quot;)</formula>
        <name>NAF Label 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAF_Label_5</fullName>
        <field>NAF_Label__c</field>
        <formula>CASE(NAF_Code__c,&quot;3012Z&quot;,&quot;Construction de bateaux de plaisance&quot;,
&quot;3020Z&quot;,&quot;Construction de locomotives et d&apos;autre matériel ferroviaire roulant &quot;,
&quot;3030Z&quot;,&quot;Construction aéronautique et spatiale &quot;,
&quot;3040Z&quot;,&quot;Construction de véhicules militaires de combat &quot;,
&quot;3091Z&quot;,&quot;Fabrication de motocycles&quot;,
&quot;3092Z&quot;,&quot;Fabrication de bicyclettes et de véhicules pour invalides&quot;,
&quot;3099Z&quot;,&quot;Fabrication d’autres équipements de transport n.c.a.&quot;,
&quot;3101Z&quot;,&quot;Fabrication de meubles de bureau et de magasin&quot;,
&quot;3102Z&quot;,&quot;Fabrication de meubles de cuisine &quot;,
&quot;3103Z&quot;,&quot;Fabrication de matelas&quot;,
&quot;3109A&quot;,&quot;Fabrication de sièges d&apos;ameublement d&apos;intérieur&quot;,
&quot;3109B&quot;,&quot;Fabrication d’autres meubles et industries connexes de l’ameublement&quot;,
&quot;3211Z&quot;,&quot;Frappe de monnaie&quot;,
&quot;3212Z&quot;,&quot;Fabrication d’articles de joaillerie et bijouterie&quot;,
&quot;3213Z&quot;,&quot;Fabrication d’articles de bijouterie fantaisie et articles similaires&quot;,
&quot;3220Z&quot;,&quot;Fabrication d&apos;instruments de musique&quot;,
&quot;3230Z&quot;,&quot;Fabrication d&apos;articles de sport&quot;,
&quot;3240Z&quot;,&quot;Fabrication de jeux et jouets&quot;,
&quot;3250A&quot;,&quot;Fabrication de matériel médico-chirurgical et dentaire&quot;,
&quot;3250B&quot;,&quot;Fabrication de lunettes&quot;,
&quot;3291Z&quot;,&quot;Fabrication d’articles de brosserie&quot;,
&quot;3299Z&quot;,&quot;Autres activités manufacturières n.c.a. &quot;,
&quot;3311Z&quot;,&quot;Réparation d&apos;ouvrages en métaux&quot;,
&quot;3312Z&quot;,&quot;Réparation de machines et équipements mécaniques&quot;,
&quot;3313Z&quot;,&quot;Réparation de matériels électroniques et optiques&quot;,
&quot;3314Z&quot;,&quot;Réparation d&apos;équipements électriques&quot;,
&quot;3315Z&quot;,&quot;Réparation et maintenance navale&quot;,
&quot;3316Z&quot;,&quot;Réparation et maintenance d&apos;aéronefs et d&apos;engins spatiaux &quot;,
&quot;3317Z&quot;,&quot;Réparation et maintenance d&apos;autres équipements de transport&quot;,
&quot;3319Z&quot;,&quot;Réparation d&apos;autres équipements&quot;,
&quot;3320A&quot;,&quot;Installation de structures métalliques, chaudronnées et de tuyauterie&quot;,
&quot;3320B&quot;,&quot;Installation de machines et équipements mécaniques&quot;,
&quot;3320C&quot;,&quot;Conception d&apos;ensemble et assemblage sur site industriel d&apos;équipements de contrôle des processus industriels &quot;,
&quot;3320D&quot;,&quot;Installation d&apos;équipements électriques, de matériels électroniques et optiques ou d&apos;autres matériels&quot;,
&quot;3511Z&quot;,&quot;Production d&apos;électricité&quot;,
&quot;3512Z&quot;,&quot;Transport d&apos;électricité&quot;,
&quot;3513Z&quot;,&quot;Distribution d&apos;électricité&quot;,
&quot;3514Z&quot;,&quot;Commerce d&apos;électricité&quot;,
&quot;3521Z&quot;,&quot;Production de combustibles gazeux&quot;,
&quot;3522Z&quot;,&quot;Distribution de combustibles gazeux par conduites&quot;,
&quot;3523Z&quot;,&quot;Commerce de combustibles gazeux par conduites&quot;,
&quot;3530Z&quot;,&quot;Production et distribution de vapeur et d&apos;air conditionné &quot;,
&quot;3600Z&quot;,&quot;Captage, traitement et distribution d&apos;eau&quot;,
&quot;3700Z&quot;,&quot;Collecte et traitement des eaux usées&quot;,
&quot;3811Z&quot;,&quot;Collecte des déchets non dangereux&quot;,
&quot;3812Z&quot;,&quot;Collecte des déchets dangereux&quot;,
&quot;3821Z&quot;,&quot;Traitement et élimination des déchets non dangereux&quot;,
&quot;3822Z&quot;,&quot;Traitement et élimination des déchets dangereux&quot;,
&quot;3831Z&quot;,&quot;Démantèlement d&apos;épaves&quot;,
&quot;3832Z&quot;,&quot;Récupération de déchets triés&quot;,
&quot;3900Z&quot;,&quot;Dépollution et autres services de gestion des déchets&quot;,
&quot;4110A&quot;,&quot;Promotion immobilière de logements&quot;,
&quot;4110B&quot;,&quot;Promotion immobilière de bureaux&quot;,
&quot;4110C&quot;,&quot;Promotion immobilière d&apos;autres bâtiments&quot;,
&quot;4110D&quot;,&quot;Supports juridiques de programmes&quot;,
&quot;4120A&quot;,&quot;Construction de maisons individuelles&quot;,
&quot;4120B&quot;,&quot;Construction d&apos;autres bâtiments&quot;,
&quot;4211Z&quot;,&quot;Construction de routes et autoroutes&quot;,
&quot;4212Z&quot;,&quot;Construction de voies ferrées de surface et souterraines&quot;,
&quot;4213A&quot;,&quot;Construction d&apos;ouvrages d&apos;art&quot;,
&quot;4213B&quot;,&quot;Construction et entretien de tunnels&quot;,
&quot;4221Z&quot;,&quot;Construction de réseaux pour fluides&quot;,
&quot;4222Z&quot;,&quot;Construction de réseaux électriques et de télécommunications&quot;,
&quot;4291Z&quot;,&quot;Construction d&apos;ouvrages maritimes et fluviaux&quot;,
&quot;4299Z&quot;,&quot;Construction d&apos;autres ouvrages de génie civil n.c.a.&quot;,
&quot;4311Z&quot;,&quot;Travaux de démolition&quot;,
&quot;4312A&quot;,&quot;Travaux de terrassement courants et travaux préparatoires&quot;,
&quot;4312B&quot;,&quot;Travaux de terrassement spécialisés ou de grande masse&quot;,
&quot;4313Z&quot;,&quot;Forages et sondages&quot;,

&quot;Null&quot;)</formula>
        <name>NAF Label 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAF_Label_6</fullName>
        <field>NAF_Label__c</field>
        <formula>CASE(NAF_Code__c,
&quot;4321A&quot;,&quot;Travaux d&apos;installation électrique dans tous locaux&quot;,
&quot;4321B&quot;,&quot;Travaux d&apos;installation électrique sur la voie publique&quot;,
&quot;4322A&quot;,&quot;Travaux d&apos;installation d&apos;eau et de gaz en tous locaux&quot;,
&quot;4322B&quot;,&quot;Travaux d&apos;installation d&apos;équipements thermiques et de climatisation&quot;,
&quot;4329A&quot;,&quot;Travaux d&apos;isolation&quot;,
&quot;4329B&quot;,&quot;Autres travaux d&apos;installation n.c.a.&quot;,
&quot;4331Z&quot;,&quot;Travaux de plâtrerie&quot;,
&quot;4332A&quot;,&quot;Travaux de menuiserie bois et PVC&quot;,
&quot;4332B&quot;,&quot;Travaux de menuiserie métallique et serrurerie&quot;,
&quot;4332C&quot;,&quot;Agencement de lieux de vente&quot;,
&quot;4333Z&quot;,&quot;Travaux de revêtement des sols et des murs&quot;,
&quot;4334Z&quot;,&quot;Travaux de peinture et vitrerie&quot;,
&quot;4339Z&quot;,&quot;Autres travaux de finition&quot;,
&quot;4391A&quot;,&quot;Travaux de charpente&quot;,
&quot;4391B&quot;,&quot;Travaux de couverture par éléments&quot;,
&quot;4399A&quot;,&quot;Travaux d&apos;étanchéification&quot;,
&quot;4399B&quot;,&quot;Travaux de montage de structures métalliques&quot;,
&quot;4399C&quot;,&quot;Travaux de maçonnerie générale et gros œuvre de bâtiment&quot;,
&quot;4399D&quot;,&quot;Autres travaux spécialisés de construction&quot;,
&quot;4399E&quot;,&quot;Location avec opérateur de matériel de construction&quot;, 
&quot;4511Z&quot;,&quot;Commerce de voitures et de véhicules automobiles légers&quot;, 
&quot;4519Z&quot;,&quot;Commerce d&apos;autres véhicules automobiles&quot;, 
&quot;4520A&quot;,&quot;Entretien et réparation de véhicules automobiles légers&quot;, 
&quot;4520B&quot;,&quot;Entretien et réparation d&apos;autres véhicules automobiles&quot;, 
&quot;4531Z&quot;,&quot;Commerce de gros d&apos;équipements automobiles&quot;, 
&quot;4532Z&quot;,&quot;Commerce de détail d&apos;équipements automobiles&quot;, 
&quot;4540Z&quot;,&quot;Commerce et réparation de motocycles&quot;, 
&quot;4611Z&quot;,&quot;Intermédiaires du commerce en matières premières agricoles, animaux vivants, matières premières textiles et produits semi-finis&quot;, 
&quot;4612A&quot;,&quot;Centrales d&apos;achat de carburant&quot;, 
&quot;4612B&quot;,&quot;Autres intermédiaires du commerce en combustibles, métaux, minéraux et produits chimiques&quot;, 
&quot;4613Z&quot;,&quot;Intermédiaires du commerce en bois et matériaux de construction&quot;, 
&quot;4614Z&quot;,&quot;Intermédiaires du commerce en machines, équipements industriels, navires et avions&quot;, 
&quot;4615Z&quot;,&quot;Intermédiaires du commerce en meubles, articles de ménage et quincaillerie&quot;, 
&quot;4616Z&quot;,&quot;Intermédiaires du commerce en textiles, habillement, fourrures, chaussures et articles en cuir&quot;, 
&quot;4617A&quot;,&quot;Centrales d&apos;achat alimentaires&quot;,
&quot;4617B&quot;,&quot;Autres intermédiaires du commerce en denrées, boissons et tabac&quot;,
&quot;4618Z&quot;,&quot;Intermédiaires spécialisés dans le commerce d&apos;autres produits spécifiques&quot;,
&quot;4619A&quot;,&quot;Centrales d&apos;achat non alimentaires&quot;, 
&quot;4619B&quot;,&quot;Autres intermédiaires du commerce en produits divers&quot;,
&quot;4621Z&quot;,&quot;Commerce de gros (commerce interentreprises) de céréales, de tabac non manufacturé, de semences et d&apos;aliments pour le bétail &quot;,
&quot;4622Z&quot;,&quot;Commerce de gros (commerce interentreprises) de fleurs et plantes&quot;,
&quot;4623Z&quot;,&quot;Commerce de gros (commerce interentreprises) d&apos;animaux vivants&quot;,
&quot;4624Z&quot;,&quot;Commerce de gros (commerce interentreprises) de cuirs et peaux&quot;,
&quot;4631Z&quot;,&quot;Commerce de gros (commerce interentreprises) de fruits et légumes&quot;,
&quot;4632A&quot;,&quot;Commerce de gros (commerce interentreprises) de viandes de boucherie&quot;,
&quot;4632B&quot;,&quot;Commerce de gros (commerce interentreprises) de produits à base de viande&quot;,
&quot;4632C&quot;,&quot;Commerce de gros (commerce interentreprises) de volailles et gibier&quot;,
&quot;4633Z&quot;,&quot;Commerce de gros (commerce interentreprises) de produits laitiers, œufs, huiles et matières grasses comestibles&quot;,
&quot;4634Z&quot;,&quot;Commerce de gros (commerce interentreprises) de boissons&quot;,
&quot;4635Z&quot;,&quot;Commerce de gros (commerce interentreprises) de produits à base de tabac&quot;,
&quot;4636Z&quot;,&quot;Commerce de gros (commerce interentreprises) de sucre, chocolat et confiserie&quot;,
&quot;4637Z&quot;,&quot;Commerce de gros (commerce interentreprises) de café, thé, cacao et épices&quot;,
&quot;4638A&quot;,&quot;Commerce de gros (commerce interentreprises) de poissons, crustacés et mollusques&quot;,
&quot;Null&quot;)</formula>
        <name>NAF Label 6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAF_Label_7</fullName>
        <field>NAF_Label__c</field>
        <formula>CASE(NAF_Code__c, 
&quot;4638B&quot;,&quot;Commerce de gros (commerce interentreprises) alimentaire spécialisé divers&quot;,
&quot;4639A&quot;,&quot;Commerce de gros (commerce interentreprises) de produits surgelés&quot;,
&quot;4639B&quot;,&quot;Commerce de gros (commerce interentreprises) alimentaire non spécialisé&quot;,
&quot;4641Z&quot;,&quot;Commerce de gros (commerce interentreprises) de textiles&quot;,
&quot;4642Z&quot;,&quot;Commerce de gros (commerce interentreprises) d&apos;habillement et de chaussures&quot;,
&quot;4643Z&quot;,&quot;Commerce de gros (commerce interentreprises) d&apos;appareils électroménagers&quot;,
&quot;4644Z&quot;,&quot;Commerce de gros (commerce interentreprises) de vaisselle, verrerie et produits d&apos;entretien&quot;,
&quot;4645Z&quot;,&quot;Commerce de gros (commerce interentreprises) de parfumerie et de produits de beauté&quot;,
&quot;4646Z&quot;,&quot;Commerce de gros (commerce interentreprises) de produits pharmaceutiques&quot;,
&quot;4647Z&quot;,&quot;Commerce de gros (commerce interentreprises) de meubles, de tapis et d&apos;appareils d&apos;éclairage &quot;,
&quot;4648Z&quot;,&quot;Commerce de gros (commerce interentreprises) d&apos;articles d&apos;horlogerie et de bijouterie&quot;,
&quot;4649Z&quot;,&quot;Commerce de gros (commerce interentreprises) d&apos;autres biens domestiques &quot;,
&quot;4651Z&quot;,&quot;Commerce de gros (commerce interentreprises) d&apos;ordinateurs, d&apos;équipements informatiques périphériques et de logiciels&quot;,
&quot;4652Z&quot;,&quot;Commerce de gros (commerce interentreprises) de composants et d&apos;équipements électroniques et de télécommunication&quot;,
&quot;4661Z&quot;,&quot;Commerce de gros (commerce interentreprises) de matériel agricole&quot;,
&quot;4662Z&quot;,&quot;Commerce de gros (commerce interentreprises) de machines-outils&quot;,
&quot;4663Z&quot;,&quot;Commerce de gros (commerce interentreprises) de machines pour l&apos;extraction, la construction et le génie civil &quot;,
&quot;4664Z&quot;,&quot;Commerce de gros (commerce interentreprises) de machines pour l&apos;industrie textile et l&apos;habillement&quot;,
&quot;4665Z&quot;,&quot;Commerce de gros (commerce interentreprises) de mobilier de bureau&quot;,
&quot;4666Z&quot;,&quot;Commerce de gros (commerce interentreprises) d&apos;autres machines et équipements de bureau &quot;,
&quot;4669A&quot;,&quot;Commerce de gros (commerce interentreprises) de matériel électrique&quot;, 
&quot;4669B&quot;,&quot;Commerce de gros (commerce interentreprises) de fournitures et équipements industriels divers&quot;, 
&quot;4669C&quot;,&quot;Commerce de gros (commerce interentreprises) de fournitures et équipements divers pour le commerce et les services&quot;, 
&quot;4671Z&quot;,&quot;Commerce de gros (commerce interentreprises) de combustibles et de produits annexes&quot;, 
&quot;4672Z&quot;,&quot;Commerce de gros (commerce interentreprises) de minerais et métaux&quot;, 
&quot;4673A&quot;,&quot;Commerce de gros (commerce interentreprises) de bois et de matériaux de construction &quot;, 
&quot;4673B&quot;,&quot;Commerce de gros (commerce interentreprises) d&apos;appareils sanitaires et de produits de décoration&quot;, 
&quot;4674A&quot;,&quot;Commerce de gros (commerce interentreprises) de quincaillerie&quot;, 
&quot;4674B&quot;,&quot;Commerce de gros (commerce interentreprises) de fournitures pour la plomberie et le chauffage&quot;, 
&quot;4675Z&quot;,&quot;Commerce de gros (commerce interentreprises) de produits chimiques&quot;, 
&quot;4676Z&quot;,&quot;Commerce de gros (commerce interentreprises) d&apos;autres produits intermédiaires&quot;, 
&quot;4677Z&quot;,&quot;Commerce de gros (commerce interentreprises) de déchets et débris&quot;, 
&quot;4690Z&quot;,&quot;Commerce de gros (commerce interentreprises) non spécialisé&quot;, 
&quot;4711A&quot;,&quot;Commerce de détail de produits surgelés&quot;, 
&quot;4711B&quot;,&quot;Commerce d&apos;alimentation générale&quot;, 
&quot;4711C&quot;,&quot;Supérettes&quot;, 
&quot;4711D&quot;,&quot;Supermarchés&quot;, 
&quot;4711E&quot;,&quot;Magasins multi-commerces&quot;, 
&quot;4711F&quot;,&quot;Hypermarchés&quot;, 
&quot;4719A&quot;,&quot;Grands magasins&quot;, 
&quot;4719B&quot;,&quot;Autres commerces de détail en magasin non spécialisé&quot;, 
&quot;4721Z&quot;,&quot;Commerce de détail de fruits et légumes en magasin spécialisé&quot;, 
&quot;4722Z&quot;,&quot;Commerce de détail de viandes et de produits à base de viande en magasin spécialisé&quot;, 
&quot;4723Z&quot;,&quot;Commerce de détail de poissons, crustacés et mollusques en magasin spécialisé&quot;, 
&quot;4724Z&quot;,&quot;Commerce de détail de pain, pâtisserie et confiserie en magasin spécialisé&quot;,  
&quot;Null&quot;)</formula>
        <name>NAF Label 7</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAF_Label_8</fullName>
        <field>NAF_Label__c</field>
        <formula>CASE(NAF_Code__c,
&quot;4725Z&quot;,&quot;Commerce de détail de boissons en magasin spécialisé&quot;,
&quot;4726Z&quot;,&quot;Commerce de détail de produits à base de tabac en magasin spécialisé&quot;,
&quot;4729Z&quot;,&quot;Autres commerces de détail alimentaires en magasin spécialisé &quot;,
&quot;4730Z&quot;,&quot;Commerce de détail de carburants en magasin spécialisé&quot;,
&quot;4741Z&quot;,&quot;Commerce de détail d&apos;ordinateurs, d&apos;unités périphériques et de logiciels en magasin spécialisé&quot;,
&quot;4742Z&quot;,&quot;Commerce de détail de matériels de télécommunication en magasin spécialisé&quot;,
&quot;4743Z&quot;,&quot;Commerce de détail de matériels audio et vidéo en magasin spécialisé&quot;,
&quot;4751Z&quot;,&quot;Commerce de détail de textiles en magasin spécialisé&quot;,
&quot;4752A&quot;,&quot;Commerce de détail de quincaillerie, peintures et verres en petites surfaces (moins de 400 m2)&quot;,
&quot;4752B&quot;,&quot;Commerce de détail de quincaillerie, peintures et verres en grandes surfaces (400 m2et plus)&quot;,
&quot;4753Z&quot;,&quot;Commerce de détail de tapis, moquettes et revêtements de murs et de sols en magasin spécialisé&quot;,
&quot;4754Z&quot;,&quot;Commerce de détail d&apos;appareils électroménagers en magasin spécialisé&quot;,
&quot;4759A&quot;,&quot;Commerce de détail de meubles&quot;,
&quot;4759B&quot;,&quot;Commerce de détail d&apos;autres équipements du foyer&quot;,
&quot;4761Z&quot;,&quot;Commerce de détail de livres en magasin spécialisé&quot;,
&quot;4762Z&quot;,&quot;Commerce de détail de journaux et papeterie en magasin spécialisé&quot;,
&quot;4763Z&quot;,&quot;Commerce de détail d&apos;enregistrements musicaux et vidéo en magasin spécialisé&quot;,
&quot;4764Z&quot;,&quot;Commerce de détail d&apos;articles de sport en magasin spécialisé&quot;,
&quot;4765Z&quot;,&quot;Commerce de détail de jeux et jouets en magasin spécialisé&quot;,
&quot;4771Z&quot;,&quot;Commerce de détail d&apos;habillement en magasin spécialisé&quot;,
&quot;4772A&quot;,&quot;Commerce de détail de la chaussure&quot;,
&quot;4772B&quot;,&quot;Commerce de détail de maroquinerie et d&apos;articles de voyage&quot;,
&quot;4773Z&quot;,&quot;Commerce de détail de produits pharmaceutiques en magasin spécialisé&quot;,
&quot;4774Z&quot;,&quot;Commerce de détail d&apos;articles médicaux et orthopédiques en magasin spécialisé&quot;,
&quot;4775Z&quot;,&quot;Commerce de détail de parfumerie et de produits de beauté en magasin spécialisé&quot;,
&quot;4776Z&quot;,&quot;Commerce de détail de fleurs, plantes, graines, engrais, animaux de compagnie et aliments pour ces animaux en magasin spécialisé&quot;,
&quot;4777Z&quot;,&quot;Commerce de détail d&apos;articles d&apos;horlogerie et de bijouterie en magasin spécialisé&quot;,
&quot;4778A&quot;,&quot;Commerces de détail d&apos;optique&quot;,
&quot;4778B&quot;,&quot;Commerces de détail de charbons et combustibles&quot;,
&quot;4778C&quot;,&quot;Autres commerces de détail spécialisés divers&quot;,
&quot;4779Z&quot;,&quot;Commerce de détail de biens d&apos;occasion en magasin&quot;,
&quot;4781Z&quot;,&quot;Commerce de détail alimentaire sur éventaires et marchés&quot;,
&quot;4782Z&quot;,&quot;Commerce de détail de textiles, d&apos;habillement et de chaussures sur éventaires et marchés&quot;,
&quot;4789Z&quot;,&quot;Autres commerces de détail sur éventaires et marchés&quot;,
&quot;4791A&quot;,&quot;Vente à distance sur catalogue général&quot;,
&quot;4791B&quot;,&quot;Vente à distance sur catalogue spécialisé&quot;,
&quot;4799A&quot;,&quot;Vente à domicile&quot;,
&quot;4799B&quot;,&quot;Vente par automates et autres commerces de détail hors magasin, éventaires ou marchés n.c.a.&quot;,
&quot;4910Z&quot;,&quot;Transport ferroviaire interurbain de voyageurs&quot;,
&quot;4920Z&quot;,&quot;Transports ferroviaires de fret &quot;,
&quot;4931Z&quot;,&quot;Transports urbains et suburbains de voyageurs&quot;,
&quot;4932Z&quot;,&quot;Transports de voyageurs par taxis&quot;,
&quot;4939A&quot;,&quot;Transports routiers réguliers de voyageurs&quot;,
&quot;4939B&quot;,&quot;Autres transports routiers de voyageurs &quot;,
&quot;4939C&quot;,&quot;Téléphériques et remontées mécaniques&quot;,
&quot;4941A&quot;,&quot;Transports routiers de fret interurbains&quot;,
&quot;4941B&quot;,&quot;Transports routiers de fret de proximité&quot;,
&quot;4941C&quot;,&quot;Location de camions avec chauffeur&quot;,
&quot;4942Z&quot;,&quot;Services de déménagement&quot;,
&quot;4950Z&quot;,&quot;Transports par conduites&quot;,
&quot;5010Z&quot;,&quot;Transports maritimes et côtiers de passagers&quot;,
&quot;5020Z&quot;,&quot;Transports maritimes et côtiers de fret&quot;,
&quot;5030Z&quot;,&quot;Transports fluviaux de passagers&quot;,
&quot;5040Z&quot;,&quot;Transports fluviaux de fret &quot;,
&quot;5110Z&quot;,&quot;Transports aériens de passagers&quot;,
&quot;5121Z&quot;,&quot;Transports aériens de fret&quot;,
&quot;5122Z&quot;,&quot;Transports spatiaux&quot;,
&quot;Null&quot;)</formula>
        <name>NAF Label 8</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NAF_Label_9</fullName>
        <field>NAF_Label__c</field>
        <formula>CASE(NAF_Code__c,
&quot;5210A&quot;,&quot;Entreposage et stockage frigorifique&quot;,
&quot;5210B&quot;,&quot;Entreposage et stockage non frigorifique&quot;,
&quot;5221Z&quot;,&quot;Services auxiliaires des transports terrestres&quot;,
&quot;5222Z&quot;,&quot;Services auxiliaires des transports par eau&quot;,
&quot;5223Z&quot;,&quot;Services auxiliaires des transports aériens&quot;,
&quot;5224A&quot;,&quot;Manutention portuaire&quot;,
&quot;5224B&quot;,&quot;Manutention non portuaire&quot;,
&quot;5229A&quot;,&quot;Messagerie, fret express&quot;,
&quot;5229B&quot;,&quot;Affrètement et organisation des transports &quot;,
&quot;5310Z&quot;,&quot;Activités de poste dans le cadre d&apos;une obligation de service universel &quot;,
&quot;5320Z&quot;,&quot;Autres activités de poste et de courrier&quot;,
&quot;5510Z&quot;,&quot;Hôtels et hébergement similaire &quot;,
&quot;5520Z&quot;,&quot;Hébergement touristique et autre hébergement de courte durée &quot;,
&quot;5530Z&quot;,&quot;Terrains de camping et parcs pour caravanes ou véhicules de loisirs&quot;,
&quot;5590Z&quot;,&quot;Autres hébergements &quot;,
&quot;5610A&quot;,&quot;Restauration traditionnelle&quot;,
&quot;5610B&quot;,&quot;Cafétérias et autres libres-services&quot;,
&quot;5610C&quot;,&quot;Restauration de type rapide&quot;,
&quot;5621Z&quot;,&quot;Services des traiteurs &quot;,
&quot;5629A&quot;,&quot;Restauration collective sous contrat&quot;,
&quot;5629B&quot;,&quot;Autres services de restauration n.c.a.&quot;,
&quot;5630Z&quot;,&quot;Débits de boissons&quot;,
&quot;5811Z&quot;,&quot;Édition de livres&quot;,
&quot;5812Z&quot;,&quot;Édition de répertoires et de fichiers d&apos;adresses&quot;,
&quot;5813Z&quot;,&quot;Édition de journaux&quot;,
&quot;5814Z&quot;,&quot;Édition de revues et périodiques&quot;,
&quot;5819Z&quot;,&quot;Autres activités d&apos;édition&quot;,
&quot;5821Z&quot;,&quot;Édition de jeux électroniques&quot;,
&quot;5829A&quot;,&quot;Édition de logiciels système et de réseau&quot;,
&quot;5829B&quot;,&quot;Edition de logiciels outils de développement et de langages&quot;,
&quot;5829C&quot;,&quot;Edition de logiciels applicatifs&quot;,
&quot;5911A&quot;,&quot;Production de films et de programmes pour la télévision &quot;,
&quot;5911B&quot;,&quot;Production de films institutionnels et publicitaires&quot;,
&quot;5911C&quot;,&quot;Production de films pour le cinéma&quot;,
&quot;5912Z&quot;,&quot;Post-production de films cinématographiques, de vidéo et de programmes de télévision&quot;,
&quot;5913A&quot;,&quot;Distribution de films cinématographiques&quot;,
&quot;5913B&quot;,&quot;Edition et distribution vidéo&quot;,
&quot;5914Z&quot;,&quot;Projection de films cinématographiques&quot;,
&quot;5920Z&quot;,&quot;Enregistrement sonore et édition musicale &quot;,
&quot;6010Z&quot;,&quot;Édition et diffusion de programmes radio&quot;,
&quot;6020A&quot;,&quot;Edition de chaînes généralistes&quot;,
&quot;6020B&quot;,&quot;Edition de chaînes thématiques&quot;,
&quot;6110Z&quot;,&quot;Télécommunications filaires&quot;,
&quot;6120Z&quot;,&quot;Télécommunications sans fil &quot;,
&quot;6130Z&quot;,&quot;Télécommunications par satellite&quot;,
&quot;6190Z&quot;,&quot;Autres activités de télécommunication &quot;,
&quot;6201Z&quot;,&quot;Programmation informatique&quot;,
&quot;6202A&quot;,&quot;Conseil en systèmes et logiciels informatiques&quot;,
&quot;6202B&quot;,&quot;Tierce maintenance de systèmes et d’applications informatiques&quot;,
&quot;6203Z&quot;,&quot;Gestion d&apos;installations informatiques&quot;,
&quot;6209Z&quot;,&quot;Autres activités informatiques&quot;,
&quot;6311Z&quot;,&quot;Traitement de données, hébergement et activités connexes&quot;,
&quot;6312Z&quot;,&quot;Portails Internet&quot;,
&quot;6391Z&quot;,&quot;Activités des agences de presse&quot;,
&quot;6399Z&quot;,&quot;Autres services d&apos;information n.c.a.&quot;,
&quot;6411Z&quot;,&quot;Activités de banque centrale&quot;,
&quot;6419Z&quot;,&quot;Autres intermédiations monétaires&quot;,
&quot;6420Z&quot;,&quot;Activités des sociétés holding&quot;,
&quot;6430Z&quot;,&quot;Fonds de placement et entités financières similaires&quot;,
&quot;6491Z&quot;,&quot;Crédit-bail &quot;,
&quot;6492Z&quot;,&quot;Autre distribution de crédit&quot;,
&quot;6499Z&quot;,&quot;Autres activités des services financiers, hors assurance et caisses de retraite, n.c.a.&quot;,
&quot;6511Z&quot;,&quot;Assurance vie&quot;,
&quot;6512Z&quot;,&quot;Autres assurances&quot;,
&quot;6520Z&quot;,&quot;Réassurance&quot;,
&quot;6530Z&quot;,&quot;Caisses de retraite&quot;,
&quot;6611Z&quot;,&quot;Administration de marchés financiers&quot;,
&quot;6612Z&quot;,&quot;Courtage de valeurs mobilières et de marchandises&quot;,
&quot;6619A&quot;,&quot;Supports juridiques de gestion de patrimoine mobilier&quot;,
&quot;6619B&quot;,&quot;Autres activités auxiliaires de services financiers, hors assurance et caisses de retraite, n.c.a.&quot;,
&quot;6621Z&quot;,&quot;Évaluation des risques et dommages&quot;,
&quot;6622Z&quot;,&quot;Activités des agents et courtiers d&apos;assurances&quot;,
&quot;6629Z&quot;,&quot;Autres activités auxiliaires d&apos;assurance et de caisses de retraite&quot;,
&quot;6630Z&quot;,&quot;Gestion de fonds&quot;,
&quot;6810Z&quot;,&quot;Activités des marchands de biens immobiliers&quot;,
&quot;6820A&quot;,&quot;Location de logements&quot;,
&quot;Null&quot;)</formula>
        <name>NAF Label 9</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Outre_mer</fullName>
        <description>Update Country code</description>
        <field>County__c</field>
        <formula>CASE( LEFT(Post_Code__c, 3) ,
&quot;971&quot;, &quot;Guadeloupe&quot;,
&quot;972&quot;, &quot;Martinique&quot;,
&quot;973&quot;, &quot;Guyane&quot;,
&quot;974&quot;, &quot;La Réunion&quot;,
&quot;975&quot;, &quot;Saint-Pierre-et-Miquelon&quot;,
&quot;976&quot;, &quot;Mayotte&quot;,
&quot;977&quot;, &quot;Saint-Barthélemy&quot;,
&quot;978&quot;, &quot;Saint-Martin&quot;,
&quot;984&quot;,  &quot;Terres australes et antarctiques françaises&quot;,
&quot;986&quot;, &quot;Wallis-et-Futuna&quot;,
&quot;987&quot;, &quot;Polynésie française&quot;,
&quot;988&quot;, &quot;Nouvelle-Calédonie&quot;,
&quot;989&quot;, &quot;Île de Clipperton&quot;,
&quot;Not found&quot;)</formula>
        <name>Outre mer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Post_Code_County_2</fullName>
        <field>County__c</field>
        <formula>CASE( Post_Code_Region__c ,
&quot;08&quot;, &quot;Ardennes&quot;,
&quot;Null&quot;)</formula>
        <name>Post Code County 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_County</fullName>
        <field>County__c</field>
        <formula>CASE( LEFT(Post_Code__c, 2), 
&quot;01&quot;, &quot;Ain&quot;,	
&quot;02&quot;, &quot;Aisne&quot;,
&quot;03&quot;, &quot;Allier&quot;,
&quot;04&quot;, &quot;Alpes-de-Haute-Provence&quot;,
&quot;05&quot;, &quot;Hautes-Alpes&quot;,
&quot;06&quot;, &quot;Alpes-Maritimes&quot;,
&quot;07&quot;, &quot;Ardèche&quot;,
&quot;08&quot;, &quot;Ardennes&quot;,	
&quot;09&quot;, &quot;Ariège&quot;,
&quot;10&quot;, &quot;Aube&quot;,
&quot;11&quot;, &quot;Aude&quot;,
&quot;12&quot;, &quot;Aveyron&quot;,
&quot;13&quot;, &quot;Bouches-du-Rhône&quot;,
&quot;14&quot;, &quot;Calvados&quot;,
&quot;15&quot;, &quot;Cantal&quot;,
&quot;16&quot;, &quot;Charente&quot;,
&quot;17&quot;, &quot;Charente-Maritime&quot;,
&quot;18&quot;, &quot;Cher&quot;,
&quot;19&quot;, &quot;Corrèze&quot;,
&quot;2A&quot;, &quot;Corse-du-Sud&quot;,
&quot;2B&quot;, &quot;Haute-Corse&quot;,
&quot;21&quot;, &quot;Côte-d&apos;Or&quot;,
&quot;22&quot;, &quot;Côtes-d&apos;Armor &quot;,
&quot;23&quot;, &quot;Creuse&quot;,
&quot;24&quot;, &quot;Dordogne&quot;,
&quot;25&quot;, &quot;Doubs&quot;,
&quot;26&quot;, &quot;Drôme&quot;,
&quot;27&quot;, &quot;Eure&quot;,
&quot;28&quot;, &quot;Eure-et-Loir&quot;,
&quot;29&quot;, &quot;Finistère&quot;,
&quot;30&quot;, &quot;Gard&quot;,
&quot;31&quot;, &quot;Haute-Garonne&quot;,
&quot;32&quot;, &quot;Gers&quot;,
&quot;33&quot;, &quot;Gironde&quot;,
&quot;34&quot;, &quot;Hérault&quot;,
&quot;35&quot;, &quot;Ille-et-Vilaine&quot;,
&quot;36&quot;, &quot;Indre&quot;,
&quot;37&quot;, &quot;Indre-et-Loire&quot;,
&quot;38&quot;, &quot;Isère&quot;,
&quot;39&quot;, &quot;Jura&quot;,
&quot;40&quot;, &quot;Landes&quot;,
&quot;41&quot;, &quot;Loir-et-Cher&quot;,
&quot;42&quot;, &quot;Loire&quot;,
&quot;43&quot;, &quot;Haute-Loire&quot;,
&quot;44&quot;, &quot;Loire-Atlantique&quot;,
&quot;45&quot;, &quot;Loiret&quot;,
&quot;46&quot;, &quot;Lot&quot;,
&quot;47&quot;, &quot;Lot-et-Garonne&quot;,
&quot;48&quot;, &quot;Lozère&quot;,
&quot;49&quot;, &quot;Maine-et-Loire&quot;,
&quot;50&quot;, &quot;Manche&quot;,
&quot;51&quot;, &quot;Marne&quot;,
&quot;52&quot;, &quot;Haute-Marne&quot;,
&quot;53&quot;, &quot;Mayenne&quot;,
&quot;54&quot;, &quot;Meurthe-et-Moselle&quot;,
&quot;55&quot;, &quot;Meuse&quot;,
&quot;56&quot;, &quot;Morbihan&quot;,
&quot;57&quot;, &quot;Moselle&quot;,
&quot;58&quot;, &quot;Nièvre&quot;,
&quot;59&quot;, &quot;Nord&quot;,
&quot;60&quot;, &quot;Oise&quot;,
&quot;61&quot;, &quot;Orne&quot;,
&quot;62&quot;, &quot;Pas-de-Calais&quot;,
&quot;63&quot;, &quot;Puy-de-Dôme&quot;,
&quot;64&quot;, &quot;Pyrénées-Atlantiques&quot;,
&quot;65&quot;, &quot;Hautes-Pyrénées&quot;,
&quot;66&quot;, &quot;Pyrénées-Orientales&quot;,
&quot;67&quot;, &quot;Bas-Rhin&quot;,
&quot;68&quot;, &quot;Haut-Rhin&quot;,
&quot;69&quot;, &quot;Rhône&quot;,
&quot;70&quot;, &quot;Metropolitan Lyon&quot;,
&quot;71&quot;, &quot;Haute-Saône&quot;,
&quot;72&quot;, &quot;Saône-et-Loire&quot;,
&quot;72&quot;, &quot;Sarthe&quot;,
&quot;73&quot;, &quot;Savoie&quot;,
&quot;74&quot;, &quot;Haute-Savoie&quot;,
&quot;75&quot;, &quot;Paris&quot;,
&quot;76&quot;, &quot;Seine-Maritime&quot;,
&quot;77&quot;, &quot;Seine-et-Marne&quot;,
&quot;78&quot;, &quot;Yvelines&quot;,
&quot;79&quot;, &quot;Deux-Sèvres&quot;,
&quot;80&quot;, &quot;Somme&quot;,
&quot;81&quot;, &quot;Tarn&quot;,
&quot;82&quot;, &quot;Tarn-et-Garonne&quot;,
&quot;83&quot;, &quot;Var&quot;,
&quot;84&quot;, &quot;Vaucluse&quot;,
&quot;85&quot;, &quot;Vendée&quot;,
&quot;86&quot;, &quot;Vienne&quot;,
&quot;87&quot;, &quot;Haute-Vienne&quot;,
&quot;88&quot;, &quot;Vosges&quot;,
&quot;89&quot;, &quot;Yonne&quot;,
&quot;90&quot;, &quot;Territoire de Belfort&quot;,
&quot;91&quot;, &quot;Essonne&quot;,
&quot;92&quot;, &quot;Hauts-de-Seine&quot;,
&quot;93&quot;, &quot;Seine-Saint-Denis&quot;,
&quot;94&quot;, &quot;Val-de-Marne&quot;,
&quot;95&quot;, &quot;Val-d&apos;Oise&quot;,
&quot;Not found&quot;)</formula>
        <name>Update County</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Region</fullName>
        <field>Region__c</field>
        <formula>CASE( Post_Code_Region__c , 
&quot;01&quot;, &quot;Auvergne-Rhône-Alpes&quot;,	
&quot;02&quot;, &quot;Hauts-de-France&quot;, 
&quot;03&quot;, &quot;Auvergne-Rhône-Alpes&quot;, 
&quot;04&quot;, &quot;Provence-Alpes-Côte d&apos;Azur&quot;, 
&quot;05&quot;, &quot;Provence-Alpes-Côte d&apos;Azur&quot;, 
&quot;06&quot;, &quot;Provence-Alpes-Côte d&apos;Azur&quot;, 
&quot;07&quot;, &quot;Auvergne-Rhône-Alpes&quot;, 
&quot;08&quot;, &quot;Grand Est&quot;,	
&quot;09&quot;, &quot;Occitanie&quot;, 
&quot;10&quot;, &quot;Grand Est&quot;, 
&quot;11&quot;, &quot;Occitanie&quot;, 
&quot;12&quot;, &quot;Occitanie&quot;, 
&quot;13&quot;, &quot;Provence-Alpes-Côte d&apos;Azur&quot;, 
&quot;14&quot;, &quot;Normandie&quot;, 
&quot;15&quot;, &quot;Auvergne-Rhône-Alpes&quot;, 
&quot;16&quot;, &quot;Nouvelle-Aquitaine&quot;, 
&quot;17&quot;, &quot;Nouvelle-Aquitaine&quot;, 
&quot;18&quot;, &quot;Centre-Val de Loire&quot;, 
&quot;19&quot;, &quot;Nouvelle-Aquitaine&quot;, 
&quot;2A&quot;, &quot;Corsica&quot;, 
&quot;2B&quot;, &quot;Corsica&quot;, 
&quot;21&quot;, &quot;Bourgogne-Franche-Comté&quot;, 
&quot;22&quot;, &quot;Bretagne&quot;, 
&quot;23&quot;, &quot;Nouvelle-Aquitaine&quot;, 
&quot;24&quot;, &quot;Nouvelle-Aquitaine&quot;, 
&quot;25&quot;, &quot;Bourgogne-Franche-Comté&quot;, 
&quot;26&quot;, &quot;Auvergne-Rhône-Alpes&quot;, 
&quot;27&quot;, &quot;Normandie&quot;, 
&quot;28&quot;, &quot;Centre-Val de Loire&quot;, 
&quot;29&quot;, &quot;Bretagne&quot;, 
&quot;30&quot;, &quot;Occitanie&quot;, 
&quot;31&quot;, &quot;Occitanie&quot;, 
&quot;32&quot;, &quot;Occitanie&quot;, 
&quot;33&quot;, &quot;Nouvelle-Aquitaine&quot;, 
&quot;34&quot;, &quot;Occitanie&quot;, 
&quot;35&quot;, &quot;Bretagne&quot;, 
&quot;36&quot;, &quot;Centre-Val de Loire&quot;, 
&quot;37&quot;, &quot;Centre-Val de Loire&quot;, 
&quot;38&quot;, &quot;Auvergne-Rhône-Alpes&quot;, 
&quot;39&quot;, &quot;Bourgogne-Franche-Comté&quot;, 
&quot;40&quot;, &quot;Nouvelle-Aquitaine&quot;, 
&quot;41&quot;, &quot;Centre-Val de Loire&quot;, 
&quot;42&quot;, &quot;Auvergne-Rhône-Alpes&quot;, 
&quot;43&quot;, &quot;Auvergne-Rhône-Alpes&quot;, 
&quot;44&quot;, &quot;Pays de la Loire&quot;, 
&quot;45&quot;, &quot;Centre-Val de Loire&quot;, 
&quot;46&quot;, &quot;Occitanie&quot;, 
&quot;47&quot;, &quot;Nouvelle-Aquitaine&quot;, 
&quot;48&quot;, &quot;Occitanie&quot;, 
&quot;49&quot;, &quot;Pays de la Loire&quot;, 
&quot;50&quot;, &quot;Normandie&quot;, 
&quot;51&quot;, &quot;Grand Est&quot;, 
&quot;52&quot;, &quot;Grand Est&quot;, 
&quot;53&quot;, &quot;Pays de la Loire&quot;, 
&quot;54&quot;, &quot;Grand Est&quot;, 
&quot;55&quot;, &quot;Grand Est&quot;, 
&quot;56&quot;, &quot;Bretagne&quot;, 
&quot;57&quot;, &quot;Grand Est&quot;, 
&quot;58&quot;, &quot;Bourgogne-Franche-Comté&quot;, 
&quot;59&quot;, &quot;Hauts-de-France&quot;, 
&quot;60&quot;, &quot;Hauts-de-France&quot;, 
&quot;61&quot;, &quot;Normandie&quot;, 
&quot;62&quot;, &quot;Hauts-de-France&quot;, 
&quot;63&quot;, &quot;Auvergne-Rhône-Alpes&quot;, 
&quot;64&quot;, &quot;Nouvelle-Aquitaine&quot;, 
&quot;65&quot;, &quot;Occitanie&quot;, 
&quot;66&quot;, &quot;Occitanie&quot;, 
&quot;67&quot;, &quot;Grand Est&quot;, 
&quot;68&quot;, &quot;Grand Est&quot;, 
&quot;69&quot;, &quot;Auvergne-Rhône-Alpes&quot;, 
&quot;70&quot;, &quot;Bourgogne-Franche-Comté&quot;, 
&quot;71&quot;, &quot;Bourgogne-Franche-Comté&quot;, 
&quot;72&quot;, &quot;Saône-et-Loire&quot;, 
&quot;72&quot;, &quot;Pays de la Loire&quot;, 
&quot;73&quot;, &quot;Auvergne-Rhône-Alpes&quot;, 
&quot;74&quot;, &quot;Auvergne-Rhône-Alpes&quot;, 
&quot;75&quot;, &quot;Île-de-France&quot;, 
&quot;76&quot;, &quot;Normandie&quot;, 
&quot;77&quot;, &quot;Île-de-France&quot;, 
&quot;78&quot;, &quot;Île-de-France&quot;, 
&quot;79&quot;, &quot;Nouvelle-Aquitaine&quot;, 
&quot;80&quot;, &quot;Hauts-de-France&quot;, 
&quot;81&quot;, &quot;Occitanie&quot;, 
&quot;82&quot;, &quot;Occitanie&quot;, 
&quot;83&quot;, &quot;Provence-Alpes-Côte d&apos;Azur&quot;, 
&quot;84&quot;, &quot;Provence-Alpes-Côte d&apos;Azur&quot;, 
&quot;85&quot;, &quot;Pays de la Loire&quot;, 
&quot;86&quot;, &quot;Nouvelle-Aquitaine&quot;, 
&quot;87&quot;, &quot;Nouvelle-Aquitaine&quot;, 
&quot;88&quot;, &quot;Grand Est&quot;, 
&quot;89&quot;, &quot;Bourgogne-Franche-Comté&quot;, 
&quot;90&quot;, &quot;Bourgogne-Franche-Comté&quot;, 
&quot;91&quot;, &quot;Île-de-France&quot;, 
&quot;92&quot;, &quot;Île-de-Francee&quot;, 
&quot;93&quot;, &quot;Île-de-France&quot;, 
&quot;94&quot;, &quot;Île-de-France&quot;, 
&quot;95&quot;, &quot;Île-de-France&quot;, 
&quot;97&quot;, &quot;Outre Mer&quot;,
&quot;98&quot;, &quot;Outre Mer&quot;,
&quot;Not found&quot;)</formula>
        <name>Update Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Archiving Change Record Owner</fullName>
        <actions>
            <name>Account_Record_Owner_IJ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Company_Status__c</field>
            <operation>equals</operation>
            <value>Archived</value>
        </criteriaItems>
        <description>When company is archived record owner is changed to Justine Loftus</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Company Approval Process UK</fullName>
        <actions>
            <name>Company_Approval_Notification_UK</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Company_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>UK,Colliers UK</value>
        </criteriaItems>
        <description>Workflow to notify account owners (should be admin&apos;s) when a company they created has been approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FR - Post Code - Outre Mer</fullName>
        <actions>
            <name>Outre_mer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.County__c</field>
            <operation>equals</operation>
            <value>Not Found</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FR New Company Notification</fullName>
        <actions>
            <name>Notification_of_New_FR_Company_Record</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Not Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>00e24000000rgcx</value>
        </criteriaItems>
        <description>Initial Implementation Requested by FR - 06/2016</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FR Post Code Update</fullName>
        <actions>
            <name>Update_County</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <description>Update the &quot;Departement&quot; Label based on the 2 first digits of the Post Code</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NAF Code</fullName>
        <actions>
            <name>NAF_Label_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NAF_Label_10</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NAF_Label_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NAF_Label_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NAF_Label_4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NAF_Label_5</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NAF_Label_6</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NAF_Label_7</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NAF_Label_8</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NAF_Label_9</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <description>Workflow to update NAF Label, Category and Sub-Category based on NAF Code - requested by France</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NAF Code 2</fullName>
        <actions>
            <name>NAF_Label_11</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NAF_Label_12</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <description>Workflow to update NAF Label, Category and Sub-Category based on NAF Code - requested by France</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
