<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_PO_Amount_Change_Approved</fullName>
        <description>Email PO Amount Change Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Record_Approved</template>
    </alerts>
    <alerts>
        <fullName>Email_PO_Amount_Change_Rejected</fullName>
        <description>Email PO Amount Change Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Record_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>CS_Update_PO_Number_to_blank</fullName>
        <field>Name</field>
        <formula>&apos;000000&apos;</formula>
        <name>CS_Update PO Number to blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Prevent_Process_False</fullName>
        <field>Prevent_Process__c</field>
        <literalValue>0</literalValue>
        <name>Prevent Process False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Prevent_Process_True</fullName>
        <description>To ensure that PO is not submitted twice for the approval process</description>
        <field>Prevent_Process__c</field>
        <literalValue>1</literalValue>
        <name>Prevent Process True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Revert_PO_Amount</fullName>
        <field>Amount__c</field>
        <formula>Previous_PO_Amount__c</formula>
        <name>Revert PO Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PO_Approved</fullName>
        <field>PO_Status__c</field>
        <literalValue>Amended - Approved</literalValue>
        <name>Update PO Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PO_Approved_By</fullName>
        <field>Approved_By__c</field>
        <formula>$User.FirstName &amp;&quot; &quot;&amp; $User.LastName</formula>
        <name>Update PO Approved By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PO_Approved_Date</fullName>
        <field>Approved_Date__c</field>
        <formula>NOW()</formula>
        <name>Update PO Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PO_Awaiting_Approval</fullName>
        <field>PO_Status__c</field>
        <literalValue>Amended - Approval Required</literalValue>
        <name>Update PO Awaiting Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PO_Rejected</fullName>
        <field>PO_Status__c</field>
        <literalValue>Amended - Rejected</literalValue>
        <name>Update PO Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PO_Rejected_By</fullName>
        <field>Rejected_By__c</field>
        <formula>$User.FirstName &amp;&quot; &quot;&amp; $User.LastName</formula>
        <name>Update PO Rejected By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PO_Rejected_Date</fullName>
        <field>Rejected_Date__c</field>
        <formula>NOW()</formula>
        <name>Update PO Rejected Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PO_Requested_Date</fullName>
        <field>Date_Requested__c</field>
        <formula>NOW()</formula>
        <name>Update PO Requested Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CS_Update new PO to approved</fullName>
        <actions>
            <name>CS_Update_PO_Number_to_blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_PO_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
