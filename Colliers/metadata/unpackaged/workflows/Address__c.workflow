<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Address_Name</fullName>
        <field>Name</field>
        <formula>LEFT(
(Flat_Number__c &amp;&apos; &apos;&amp; 
Building__c &amp;&apos; &apos;&amp; 
Estate__c &amp;&apos; &apos;&amp; 
Street_Number__c &amp;&apos; &apos;&amp; 
Street__c &amp;&apos; &apos;&amp; 
Area__c &amp;&apos; &apos;&amp; 
Town__c &amp;&apos; &apos;&amp; 
County__c &amp;&apos; &apos;&amp; 
Postcode__c &amp;&apos; &apos;&amp; 
TEXT(Country__c))

, 79)</formula>
        <name>Update Address Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CS_Update Address Name</fullName>
        <actions>
            <name>Update_Address_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
