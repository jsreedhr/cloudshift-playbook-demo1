<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Archiving_Contact_record_owner_change</fullName>
        <field>OwnerId</field>
        <lookupValue>richard.judd@colliers.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Archiving Contact record owner change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Do_Not_Call_Date_Stamp</fullName>
        <field>Do_Not_Call_Date__c</field>
        <formula>TODAY()</formula>
        <name>Do Not Call Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Opt_Out_Date_Stamp</fullName>
        <field>Email_Opt_Out_Date__c</field>
        <formula>TODAY()</formula>
        <name>Email Opt Out Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Staff_as_contact_Email</fullName>
        <field>Email</field>
        <formula>Staff_Member__r.Email__c</formula>
        <name>Staff as contact - Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Initial_on_Contact</fullName>
        <field>Initial__c</field>
        <formula>left(FirstName,1)</formula>
        <name>Stamp Initial on Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Archiving Contact record owner change</fullName>
        <actions>
            <name>Archiving_Contact_record_owner_change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Contact_Status__c</field>
            <operation>equals</operation>
            <value>Archived</value>
        </criteriaItems>
        <description>when contact is archived change record owner to richard judd</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Do Not Call Date tamp</fullName>
        <actions>
            <name>Do_Not_Call_Date_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.DoNotCall</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Do_Not_Call_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Opt Out Date Stamp</fullName>
        <actions>
            <name>Email_Opt_Out_Date_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Email_Opt_Out_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Staff Email update</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Staff_Department__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Staff as contacts - email update</fullName>
        <actions>
            <name>Staff_as_contact_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Staff_Department__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Stamp Initial if Initial Not Populated</fullName>
        <actions>
            <name>Stamp_Initial_on_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Initial__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.FirstName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
