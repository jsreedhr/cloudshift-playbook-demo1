<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_Email_to_BDM_for_Lead_Allocation</fullName>
        <description>Notification Email to BDM for Lead Allocation</description>
        <protected>false</protected>
        <recipients>
            <field>Lead_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Rating/Lead_Allocated_to_Lead_Manager</template>
    </alerts>
</Workflow>
