<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_to_Task_Creator</fullName>
        <description>Alert to Task Creator</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FR/Alert_on_Tasks</template>
    </alerts>
    <alerts>
        <fullName>Sent_email_when_Created_By_and_Assigned_To_are_different</fullName>
        <description>Sent email when Created By and Assigned To are different</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FR/Alert_on_Tasks</template>
    </alerts>
    <fieldUpdates>
        <fullName>Copy_Comments_Summary</fullName>
        <field>Comments_Summary__c</field>
        <formula>IF(
LEN(Description) &gt; 252,
LEFT(Description,252)&amp;&quot;…&quot;,
LEFT(Description,255))</formula>
        <name>Copy Comments Summary</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Task_Closed_Date_TODAY</fullName>
        <field>Closed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Task Closed Date with TODAY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copy Comments Summary</fullName>
        <actions>
            <name>Copy_Comments_Summary</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Description</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <description>System: Used so that users may be able to see comments on Activities Related List on Opportunity Page Layout. Not possible on Comments standard field.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Created By different from Assigned To</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.CreatedById</field>
            <operation>notEqual</operation>
            <value>Task: Assigned To</value>
        </criteriaItems>
        <description>Created By different from Assigned To</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert Task Creator</fullName>
        <actions>
            <name>Sent_email_when_Created_By_and_Assigned_To_are_different</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send an email alert when the Owner of the task is different from the creator of the task.</description>
        <formula>AND(RecordType.Name = &quot;FR Task&quot;, CreatedBy.ContactId &lt;&gt;  Owner:User.ContactId,   ActivityDate  =   TODAY())</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Task Closed is TRUE Stamp Closed Date</fullName>
        <actions>
            <name>Update_Task_Closed_Date_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>System: Used to create date stamp on Data Task Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
