<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CS_Update_Account_Id_Checkbox</fullName>
        <field>is_Account_Id_Changed__c</field>
        <literalValue>1</literalValue>
        <name>CS Update Account Id Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CS_Update_Account_Id_Date</fullName>
        <field>Account_Id_Changed_Date__c</field>
        <formula>TODAY()</formula>
        <name>CS Update Account Id Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CS Update Account Id Change</fullName>
        <actions>
            <name>CS_Update_Account_Id_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CS_Update_Account_Id_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(ISBLANK(PRIORVALUE(Account_ID__c)) &amp;&amp; NOT(ISBLANK(Account_ID__c)) , true, false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
