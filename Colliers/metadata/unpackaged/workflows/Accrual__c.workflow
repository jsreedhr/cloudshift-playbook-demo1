<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_of_record_approval</fullName>
        <description>Notification of record approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Accrual_Record_Approved</template>
    </alerts>
    <alerts>
        <fullName>Notification_of_record_rejection</fullName>
        <description>Notification of record rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Accrual_Record_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Notify_Job_Manager_of_an_Active_Accrual</fullName>
        <description>Notify Job Manager of an Active Accrual</description>
        <protected>false</protected>
        <recipients>
            <field>Job_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK/Accrual_Active_Time_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Accrual_Date_Requested</fullName>
        <description>Update Accrual Request Date</description>
        <field>Date_Requested__c</field>
        <formula>NOW()</formula>
        <name>Update Accrual Date Requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Accrual_Status_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Accrual Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Accrual_Status_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Accrual Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approved_Date</fullName>
        <field>Approved_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approver</fullName>
        <field>Approved_By__c</field>
        <formula>$User.FirstName &amp;&quot; &quot;&amp; $User.LastName</formula>
        <name>Update Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Job_Manager_Email</fullName>
        <field>Job_Manager_Email__c</field>
        <formula>Job__r.Manager__r.Email__c</formula>
        <name>Update Job Manager Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Month_to_Accrue</fullName>
        <field>Month_To_Accrue__c</field>
        <formula>TODAY()</formula>
        <name>Update Month to Accrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Rejected_By</fullName>
        <field>Rejected_By__c</field>
        <formula>$User.FirstName &amp;&quot; &quot;&amp; $User.LastName</formula>
        <name>Update Rejected By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Rejected_Date</fullName>
        <field>Rejected_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Rejected Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Accrual_Reversed</fullName>
        <field>Reversed__c</field>
        <literalValue>1</literalValue>
        <name>Updated Accrual Reversed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CS_Accrual Active Notification 05 months</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Accrual__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Accrual__c.Reversed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Job_Manager_of_an_Active_Accrual</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Accrual__c.Approved_Date__c</offsetFromField>
            <timeLength>150</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CS_Accrual Active Notification 06 months</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Accrual__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Accrual__c.Reversed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Job_Manager_of_an_Active_Accrual</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Accrual__c.Approved_Date__c</offsetFromField>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CS_Accrual Active Notification 12 months</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Accrual__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Accrual__c.Reversed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Job_Manager_of_an_Active_Accrual</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Accrual__c.Approved_Date__c</offsetFromField>
            <timeLength>365</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CS_Accrual Active Notification 18 months</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Accrual__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Accrual__c.Reversed__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_Job_Manager_of_an_Active_Accrual</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Accrual__c.Approved_Date__c</offsetFromField>
            <timeLength>548</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CS_Update Accrual Reversed Date</fullName>
        <actions>
            <name>Updated_Accrual_Reversed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Accrual__c.Status__c</field>
            <operation>equals</operation>
            <value>Reversed By Invoice,Reversed By User,Reversed By User JM</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cs_Update Accrual Job Manager Email</fullName>
        <actions>
            <name>Update_Job_Manager_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Accrual__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Cs_Update Accrual Job Manager Email</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
