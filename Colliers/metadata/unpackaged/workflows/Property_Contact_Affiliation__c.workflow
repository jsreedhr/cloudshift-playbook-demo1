<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Contact_Area_1</fullName>
        <field>Area__c</field>
        <formula>Property__r.Area__c</formula>
        <name>Update Contact Area</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Building_Name_1</fullName>
        <field>Building_Name__c</field>
        <formula>Property__r.Building_Name__c</formula>
        <name>Update Contact Building Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Country_1</fullName>
        <field>Country__c</field>
        <formula>Property__r.Country__c</formula>
        <name>Update Contact Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_County_1</fullName>
        <field>County__c</field>
        <formula>Property__r.County__c</formula>
        <name>Update Contact County</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Estate_1</fullName>
        <field>Estate__c</field>
        <formula>Property__r.Estate__c</formula>
        <name>Update Contact Estate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Flat_No_1</fullName>
        <field>Floor_No__c</field>
        <formula>Property__r.Floor_No__c</formula>
        <name>Update Contact Flat No.</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Post_Code_1</fullName>
        <field>Post_Code__c</field>
        <formula>Property__r.Post_Code__c</formula>
        <name>Update Contact Post Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Street_1</fullName>
        <field>Street__c</field>
        <formula>Property__r.Street__c</formula>
        <name>Update Contact Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Street_No_1</fullName>
        <field>Street_No__c</field>
        <formula>Property__r.Street_No__c</formula>
        <name>Update Contact Street No.</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Town_1</fullName>
        <field>Town__c</field>
        <formula>Property__r.Town__c</formula>
        <name>Update Contact Town</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Update Contact Address with Primary Property Address</fullName>
        <actions>
            <name>Update_Contact_Area_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Contact_Building_Name_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Contact_Country_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Contact_County_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Contact_Estate_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Contact_Flat_No_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Contact_Post_Code_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Contact_Street_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Contact_Street_No_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Contact_Town_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Property_Contact_Affiliation__c.Primary__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates Contact&apos;s Address with the Primary Hereditament Address
Requested by UK Rating</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
