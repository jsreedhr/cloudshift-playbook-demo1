<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Invoice_Status_to_Signed</fullName>
        <field>Invoice_Status__c</field>
        <literalValue>Invoice Signed</literalValue>
        <name>Update Invoice Status to Signed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Invoice Status</fullName>
        <actions>
            <name>Update_Invoice_Status_to_Signed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Saving_Subcontractor_Affiliation__c.Date_Subcontractor_Invoice_Signed__c</field>
            <operation>greaterThan</operation>
            <value>1/1/1985</value>
        </criteriaItems>
        <description>Update Saving &amp; Subcontractor Affiliation: Invoice Status field to &apos;Invoice Signed&apos;
Requested by UK Accurates</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
