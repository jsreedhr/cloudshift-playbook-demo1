<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Accrual_Forecasting_Junction__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Accrual Forecasting Junction</label>
        <referenceTo>Accrual_Forecasting_Junction__c</referenceTo>
        <relationshipLabel>Fee Income Staging Table</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Accrual__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Accrual</label>
        <referenceTo>Accrual__c</referenceTo>
        <relationshipLabel>Fee Income Staging Table</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Acting_on_Behalf_of__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Allocation__r.Job__r.Instructing_Company_Role__c)</formula>
        <label>Acting on Behalf of</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Alloc_Dept__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISBLANK(Fee_Allocated_To__c), &quot;EXTERNAL&quot;, Allocation__r.Department_Allocation__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Alloc Dept</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Allocation_Net__c</fullName>
        <externalId>false</externalId>
        <label>Allocation Net</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Allocation_Type__c</fullName>
        <externalId>false</externalId>
        <formula>IF((Allocation__r.externalCompany__c != null), 
&quot;External&quot;, 
IF((Allocation__r.Assigned_To__c != null),
&quot;Internal&quot;,
&quot;Other&quot;))</formula>
        <label>Allocation Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Allocation__c</fullName>
        <externalId>false</externalId>
        <label>Allocation</label>
        <referenceTo>Allocation__c</referenceTo>
        <relationshipLabel>Fee Income Staging Table</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Client__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Client</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Fee Income Staging Table</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Country__c</fullName>
        <externalId>false</externalId>
        <label>Country</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>UK</fullName>
                    <default>false</default>
                    <label>UK</label>
                </value>
                <value>
                    <fullName>Spain</fullName>
                    <default>false</default>
                    <label>Spain</label>
                </value>
                <value>
                    <fullName>Germany</fullName>
                    <default>false</default>
                    <label>Germany</label>
                </value>
                <value>
                    <fullName>Belgium</fullName>
                    <default>false</default>
                    <label>Belgium</label>
                </value>
                <value>
                    <fullName>France</fullName>
                    <default>false</default>
                    <label>France</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Credit_Note__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Credit Note</label>
        <referenceTo>Credit_Note__c</referenceTo>
        <relationshipLabel>Fee Income Staging Table</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Department__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Allocation__r.Job__r.Department__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Department</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>External_Company__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>External Company</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Fee Income Staging Table (External Company)</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Fee_Allocated_To_Formula__c</fullName>
        <externalId>false</externalId>
        <formula>IF(
NOT(ISBLANK( Fee_Allocated_To__c )),  Fee_Allocated_To__r.Full_Name__c,  External_Company__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Fee Allocated To</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Fee_Allocated_To__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Fee Allocated To</label>
        <referenceTo>Staff__c</referenceTo>
        <relationshipLabel>Fee Income Staging Table (Fee Allocated To)</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Forecasting__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Forecasting</label>
        <referenceTo>Forecasting__c</referenceTo>
        <relationshipLabel>Fee Income Staging Table</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Instructing_Client__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Instructing_Client</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Fee Income Staging Table (Account)</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Invoice_Allocation_Junction__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Invoice Allocation Junction</label>
        <referenceTo>Invoice_Allocation_Junction__c</referenceTo>
        <relationshipLabel>Fee Income Staging Table</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Invoice_Date__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(Type__c, &quot;Invoice&quot;), 
Invoice__r.Date_of_Invoice__c , 
IF(ISPICKVAL(Type__c, &quot;Accrual&quot;),
 Accrual__r.Date_Requested__c ,
IF(ISPICKVAL(Type__c, &quot;Accrual-Reversal&quot;),
Accrual__r.Reversed_Date__c,
IF(ISPICKVAL(Type__c, &quot;Credit Note&quot;),
 Credit_Note__r.Approved_Date__c  , 
IF(ISPICKVAL(Type__c, &quot;Journal&quot;),
    Journal__r.Date_Requested__c   , 
IF(ISPICKVAL(Type__c, &quot;Journal Reversal&quot;),
     Journal__r.Credit_Note_Approved_Date__c   ,
null
))))))</formula>
        <label>Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Invoice_Number__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(Type__c, &quot;Invoice&quot;), 
Invoice__r.Assigned_Invoice_Number__c, 
IF(ISPICKVAL(Type__c, &quot;Accural&quot;), 
null, 
IF(ISPICKVAL(Type__c, &quot;Accural-Reversal&quot;), 
null,
IF(ISPICKVAL(Type__c, &quot;Credit Note&quot;), 
Credit_Note__r.Name, 
IF(ISPICKVAL(Type__c, &quot;Journal&quot;), 
Journal__r.Invoice__r.Assigned_Invoice_Number__c , 
IF(ISPICKVAL(Type__c, &quot;Journal Reversal&quot;), 
Journal__r.Invoice__r.Assigned_Invoice_Number__c ,
null 
))))))</formula>
        <label>Invoice Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Invoice__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Invoice</label>
        <referenceTo>Invoice__c</referenceTo>
        <relationshipLabel>Fee Income Staging Table</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Is_Logged_in_User_Fee_Allocated_To__c</fullName>
        <externalId>false</externalId>
        <formula>IF( Fee_Allocated_To__r.Full_Name__c = $User.FirstName &amp; &quot; &quot; &amp;  $User.LastName , True, False)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Is Logged in User Fee Allocated To</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Job_Manager_Allocation__c</fullName>
        <externalId>false</externalId>
        <label>Job Manager Allocation</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Job_Manager__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Job Manager</label>
        <referenceTo>Staff__c</referenceTo>
        <relationshipLabel>Fee Income Staging Table</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Job_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Allocation__r.Job_Number__c</formula>
        <label>Job Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Job_Title__c</fullName>
        <externalId>false</externalId>
        <formula>Allocation__r.Job__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Job Title</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Journal_2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Journal</label>
        <referenceTo>Journal__c</referenceTo>
        <relationshipLabel>Fee Income Staging Table (Journal)</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Journal__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Journal</label>
        <referenceTo>Journal__c</referenceTo>
        <relationshipLabel>Fee Income Staging Table</relationshipLabel>
        <relationshipName>Fee_Income_Staging_Table</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Less_Sub_Contractor__c</fullName>
        <externalId>false</externalId>
        <label>Less Sub Contractor</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Main_Department_Allocation__c</fullName>
        <externalId>false</externalId>
        <formula>If (Department__c = Alloc_Dept__c , TRUE , FALSE)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Main Department Allocation</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Net_Fees__c</fullName>
        <externalId>false</externalId>
        <label>Net Fees</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Notes__c</fullName>
        <externalId>false</externalId>
        <label>Notes</label>
        <length>100000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Notes_for_Accounts_Payable__c</fullName>
        <externalId>false</externalId>
        <label>Notes for Accounts Payable</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Office__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISBLANK(Fee_Allocated_To__c), &quot;EXTERNAL&quot;, Allocation__r.Office__c)</formula>
        <label>Office</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Other_Allocations__c</fullName>
        <externalId>false</externalId>
        <label>Other Allocations</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>P_BC_Referral__c</fullName>
        <externalId>false</externalId>
        <formula>IF( AND( OR( Department__c = &quot;BB Building Consultancy&quot;, Department__c = &quot;Building Surveying&quot; , Department__c = &quot;Project &amp; Building Consultancy&quot;, Department__c = &quot;Project Management&quot;), OR(Alloc_Dept__c = &quot;BB Building Consultancy&quot; ,Alloc_Dept__c = &quot;Building Surveying&quot;, Alloc_Dept__c = &quot;Project &amp; Building Consultancy&quot;, Alloc_Dept__c = &quot;Project Management&quot;) ), TRUE, FALSE)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>P&amp;BC Referral</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Paid_Date__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(Type__c, &quot;Invoice&quot;), 
Invoice__r.Paid_Date__c, 
IF(ISPICKVAL(Type__c, &quot;Journal&quot;),
Invoice__r.Paid_Date__c , 
null
))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Paid Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Power_of_One__c</fullName>
        <externalId>false</externalId>
        <formula>1</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Power of One</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Rank_Name__c</fullName>
        <externalId>false</externalId>
        <formula>if(len(text( Client__r.Top_250_Ranking_UK__c ))=1,&quot;00&quot; &amp; text(Client__r.Top_250_Ranking_UK__c),if(len(text(Client__r.Top_250_Ranking_UK__c))=2,&quot;0&quot; &amp; text(Client__r.Top_250_Ranking_UK__c),text(Client__r.Top_250_Ranking_UK__c))) 

&amp; &quot;,&quot; &amp;   Client__r.Ultimate_Parent_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Rank &amp; Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Relates_To__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Allocation__r.Job__r.Relates_To__c)</formula>
        <label>Relates To</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Residential_Referral__c</fullName>
        <externalId>false</externalId>
        <formula>IF( AND(  OR(Department__c = &quot;International Properties&quot;,Department__c = &quot;PRS&quot;,Department__c = &quot;Residential - New Homes&quot;, Department__c = &quot;Residential - Development&quot;, Department__c = &quot;Residential&quot;, Department__c = &quot;Residential - International Properties&quot;, Department__c = &quot;Residential - Research&quot;), OR(Alloc_Dept__c = &quot;International Properties&quot;,Alloc_Dept__c = &quot;PRS&quot;,Alloc_Dept__c = &quot;Residential - New Homes&quot;, Alloc_Dept__c = &quot;Residential - Development&quot;, Alloc_Dept__c = &quot;Residential&quot;, Alloc_Dept__c = &quot;Residential - International Properties&quot;, Alloc_Dept__c = &quot;Residential - Research&quot;) ), TRUE, FALSE)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Residential Referral</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Retail_Referral__c</fullName>
        <externalId>false</externalId>
        <formula>IF( AND(  CONTAINS( Department__c , &quot;Retail&quot; ), CONTAINS( Alloc_Dept__c , &quot;Retail&quot;)), TRUE, FALSE)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Retail Referral</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(Type__c, &quot;Invoice&quot;), 
TEXT(Invoice__r.Status__c) , 
IF(ISPICKVAL(Type__c, &quot;Accural&quot;), 
TEXT(Accrual__r.Status__c), 
IF(ISPICKVAL(Type__c, &quot;Accural-Reversal&quot;), 
TEXT(Accrual__r.Status__c), 
IF(ISPICKVAL(Type__c, &quot;Credit Note&quot;), 
TEXT(Credit_Note__r.Status__c) , 
IF(ISPICKVAL(Type__c, &quot;Journal&quot;), 
TEXT(Invoice__r.Status__c) ,
IF(ISPICKVAL(Type__c, &quot;Journal Reversal&quot;), 
&apos;Journal Reversal&apos; , 
null 
))))))</formula>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Third_Party_Subcontractor_paid__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Third Party Subcontractor paid?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Total_Fees__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(Type__c, &quot;Invoice&quot;), 
Invoice__r.Total_Fee__c, 
IF(ISPICKVAL(Type__c, &quot;Accural&quot;), 
0.00, 
IF(ISPICKVAL(Type__c, &quot;Accural-Reversal&quot;), 
0.00,
IF(ISPICKVAL(Type__c, &quot;Credit Note&quot;), 
-(Credit_Note__r.Invoice__r.Total_Fee__c), 
IF(ISPICKVAL(Type__c, &quot;Journal&quot;), 
0.00, 
0.00
)))))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Fees</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Invoice</fullName>
                    <default>false</default>
                    <label>Invoice</label>
                </value>
                <value>
                    <fullName>Accrual</fullName>
                    <default>false</default>
                    <label>Accrual</label>
                </value>
                <value>
                    <fullName>Journal</fullName>
                    <default>false</default>
                    <label>Journal</label>
                </value>
                <value>
                    <fullName>Credit Note</fullName>
                    <default>false</default>
                    <label>Credit Note</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
                <value>
                    <fullName>Accrual-Reversal</fullName>
                    <default>false</default>
                    <label>Accrual-Reversal</label>
                </value>
                <value>
                    <fullName>Journal Reversal</fullName>
                    <default>false</default>
                    <label>Journal Reversal</label>
                </value>
                <value>
                    <fullName>Accural</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Accural</label>
                </value>
                <value>
                    <fullName>Accural-Reversal</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Accural-Reversal</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Ulimate_Instructing_Parent__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK(&quot;/&quot;&amp;Ultimate_Instructing_ParentId__c, 	
Ultimate_Instructing_Parent_Name__c,&quot;_parent&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Ultimate Instructing Parent</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ulimate_Parent_Name_del__c</fullName>
        <externalId>false</externalId>
        <formula>Client__r.Ultimate_Parent_Name__c</formula>
        <label>Ultimate Parent Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ultimate_Instructing_ParentId__c</fullName>
        <externalId>false</externalId>
        <label>Ultimate Instructing ParentId</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ultimate_Instructing_Parent_Class__c</fullName>
        <externalId>false</externalId>
        <label>Ultimate Instructing Parent Class</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ultimate_Instructing_Parent_Geo_Class__c</fullName>
        <externalId>false</externalId>
        <label>Ultimate Instructing Parent Geo Class</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ultimate_Instructing_Parent_Name__c</fullName>
        <externalId>false</externalId>
        <label>Ultimate Instructing Parent Name</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ultimate_Instructing_Parent_Sector__c</fullName>
        <externalId>false</externalId>
        <label>Ultimate Instructing Parent Sector</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ultimate_Instructing_Parent_Sub_Sector__c</fullName>
        <externalId>false</externalId>
        <label>Ultimate Instructing Parent Sub-Sector</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ultimate_Parent_Client_Class__c</fullName>
        <externalId>false</externalId>
        <formula>IF(INCLUDES(  Client__r.Company_Classification__c , &quot;Developer&quot;), &quot;Developer, &quot;, NULL)  +
IF(INCLUDES( Client__r.Company_Classification__c, &quot;Lender&quot;), &quot;Lender, &quot;, NULL)   +
IF(INCLUDES( Client__r.Company_Classification__c, &quot;Occupier&quot;), &quot;Occupier, &quot;, NULL)  +
IF(INCLUDES( Client__r.Company_Classification__c, &quot;Owner&quot;), &quot;Owner, &quot;, NULL) +
IF(INCLUDES( Client__r.Company_Classification__c, &quot;Service Provider&quot;), &quot;Service Provider&quot;,NULL)</formula>
        <label>Ultimate Parent Client Class</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ultimate_Parent_Geo_Class__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Client__r.Geographical_Client__c)</formula>
        <label>Ultimate Parent Geo Class</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ultimate_Parent_Sector__c</fullName>
        <externalId>false</externalId>
        <formula>Client__r.Ultimate_Parent_Sector__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Ultimate Parent Sector</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ultimate_Parent_Sub_Sector__c</fullName>
        <externalId>false</externalId>
        <formula>Client__r.Ultimate_Parent_Sub_Sector__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Ultimate Parent Sub-Sector</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ultimate_Parent_del__c</fullName>
        <externalId>false</externalId>
        <formula>Client__r.Ultimate_Parent__c</formula>
        <label>Ultimate Parent</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Work_Type__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISBLANK(Fee_Allocated_To__c), &quot;EXTERNAL&quot;, Allocation__r.workType_Allocation__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Work Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>X18_Digit_ID__c</fullName>
        <externalId>false</externalId>
        <formula>CASESAFEID(Id)</formula>
        <label>18 Digit ID</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>legacy_data_check__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>legacy data  check</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Fee Income Staging Table</label>
    <listViews>
        <fullName>Accruals2</fullName>
        <columns>NAME</columns>
        <columns>Accrual__c</columns>
        <columns>Invoice_Date__c</columns>
        <filterScope>Everything</filterScope>
        <label>Accruals</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>FI-{0000000}</displayFormat>
        <label>Fee Income Staging Table Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Fee Income Staging Table</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
