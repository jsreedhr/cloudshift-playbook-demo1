<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Accrual__c</fullName>
        <externalId>false</externalId>
        <label>Accrual</label>
        <referenceTo>Accrual__c</referenceTo>
        <relationshipLabel>Accrual Forecasting Junction</relationshipLabel>
        <relationshipName>Accrual_Forecasting_Junction</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Accural_Status__c</fullName>
        <externalId>false</externalId>
        <label>Accrual Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                    <label>Approved</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
                <value>
                    <fullName>Reversed By User</fullName>
                    <default>false</default>
                    <label>Reversed By User</label>
                </value>
                <value>
                    <fullName>Reversed By Invoice</fullName>
                    <default>false</default>
                    <label>Reversed By Invoice</label>
                </value>
                <value>
                    <fullName>Awaiting Approval</fullName>
                    <default>false</default>
                    <label>Awaiting Approval</label>
                </value>
                <value>
                    <fullName>Reversed By User JM</fullName>
                    <default>false</default>
                    <label>Reversed By User JM</label>
                </value>
                <value>
                    <fullName>Accrual - Reversal</fullName>
                    <default>false</default>
                    <label>Accrual - Reversal</label>
                </value>
                <value>
                    <fullName>Cancelled</fullName>
                    <default>false</default>
                    <label>Cancelled</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Allocation_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Forecasting__r.CS_Assigned_To__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Allocation Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Amount__c</fullName>
        <description>The amount accrued from the forecast</description>
        <externalId>false</externalId>
        <label>Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>External_Id_Custom__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>External Id Custom</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Forecasting__c</fullName>
        <externalId>false</externalId>
        <label>Forecasting</label>
        <referenceTo>Forecasting__c</referenceTo>
        <relationshipLabel>Accrual Forecasting Junction</relationshipLabel>
        <relationshipName>Accrual_Forecasting_Junction</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Staging_Table_Record__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Staging Table Record</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Accrual Forecasting Junction</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Accrual__c</columns>
        <columns>Forecasting__c</columns>
        <columns>Amount__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>Acc Forecast-{0000}</displayFormat>
        <label>Accrual Forecasting Junction Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Accrual Forecasting Junction</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
