<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>Mobile_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>Mobile_Layout</fullName>
        <fields>Client__c</fields>
        <fields>Job_Name__c</fields>
        <fields>Job_Number__c</fields>
        <fields>Property__c</fields>
        <label>Mobile Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Vals Inspection object, main place holder for mobile app survey</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Client__c</fullName>
        <externalId>false</externalId>
        <label>Client</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Content_Version_ID__c</fullName>
        <externalId>false</externalId>
        <label>Content Version ID</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Count_of_External_Inspection__c</fullName>
        <externalId>false</externalId>
        <label>No.of External Inspection</label>
        <summaryForeignKey>External_Inspection__c.Inspection__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Date_Time_of_Inspection__c</fullName>
        <externalId>false</externalId>
        <label>Date Time of Inspection</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>External_Label_del__c</fullName>
        <externalId>false</externalId>
        <label>External Label</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Inspecting_Surveyor__c</fullName>
        <externalId>false</externalId>
        <label>Inspecting Surveyor</label>
        <length>18</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Inspection_Type_del__c</fullName>
        <externalId>false</externalId>
        <label>Type of Inspection</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Full</fullName>
                    <default>false</default>
                    <label>Full</label>
                </value>
                <value>
                    <fullName>External Only</fullName>
                    <default>false</default>
                    <label>External Only</label>
                </value>
                <value>
                    <fullName>Drive By</fullName>
                    <default>false</default>
                    <label>Drive By</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Internal_Label__c</fullName>
        <externalId>false</externalId>
        <label>Internal Label</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Is_Development_Land_Subtype__c</fullName>
        <externalId>false</externalId>
        <label>Development/Land Subtype</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Retail</fullName>
                    <default>false</default>
                    <label>Retail</label>
                </value>
                <value>
                    <fullName>Office</fullName>
                    <default>false</default>
                    <label>Office</label>
                </value>
                <value>
                    <fullName>Residential</fullName>
                    <default>false</default>
                    <label>Residential</label>
                </value>
                <value>
                    <fullName>Industrial</fullName>
                    <default>false</default>
                    <label>Industrial</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Is_Development__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is the Property Development/Land?</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Job_Name__c</fullName>
        <externalId>false</externalId>
        <label>Job Name</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Job_Number__c</fullName>
        <externalId>false</externalId>
        <label>Job Number</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Job__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The Job that this inspection relates to.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Job that this inspection relates to.</inlineHelpText>
        <label>Job</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Inspections</relationshipLabel>
        <relationshipName>Inspections</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Measurement_Required__c</fullName>
        <externalId>false</externalId>
        <label>Measurement Required</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Mixed_Use_Subtype__c</fullName>
        <externalId>false</externalId>
        <label>Mixed Use Subtype</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Retail</fullName>
                    <default>false</default>
                    <label>Retail</label>
                </value>
                <value>
                    <fullName>Office</fullName>
                    <default>false</default>
                    <label>Office</label>
                </value>
                <value>
                    <fullName>Residential</fullName>
                    <default>false</default>
                    <label>Residential</label>
                </value>
                <value>
                    <fullName>Industrial</fullName>
                    <default>false</default>
                    <label>Industrial</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Mixed_Use__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is the Property Mixed Use?</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>No_of_Internal_Inspections__c</fullName>
        <externalId>false</externalId>
        <label>No of Internal Inspections</label>
        <summarizedField>External_Inspection__c.Count_of_Internal_Inspection__c</summarizedField>
        <summaryForeignKey>External_Inspection__c.Inspection__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Property_Address__c</fullName>
        <externalId>false</externalId>
        <formula>Property__r.Property_Address__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Property Address</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Property__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The Property that this Inspection relates to.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Property that this Inspection relates to.</inlineHelpText>
        <label>Property</label>
        <referenceTo>Property__c</referenceTo>
        <relationshipLabel>Inspections</relationshipLabel>
        <relationshipName>Inspections</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Sent_to_ShareDo__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Sent to ShareDo</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Site_Contact__c</fullName>
        <externalId>false</externalId>
        <label>Site Contact</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Site_Email__c</fullName>
        <externalId>false</externalId>
        <label>Site Email</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Site_Landline__c</fullName>
        <externalId>false</externalId>
        <label>Site Landline</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Site_Mobile__c</fullName>
        <externalId>false</externalId>
        <label>Site Mobile</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Special_Requirements__c</fullName>
        <externalId>false</externalId>
        <label>Client Special Requirements</label>
        <length>1000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <label>Inspection</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Inspecting_Surveyor__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Inspection Name</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Inspections</pluralLabel>
    <searchLayouts>
        <searchResultsAdditionalFields>Inspecting_Surveyor__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>ShareDo_Callout</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Vals Express</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;https://code.jquery.com/jquery-1.10.2.js&quot;)} 
{!REQUIRESCRIPT(&quot;https://code.jquery.com/ui/1.10.4/jquery-ui.js&quot;)} 






var j$ = jQuery.noConflict();
var currentUrl = window.location.href
;
var hostIndex = currentUrl.indexOf(window.location.host+&apos;/&apos;)+(window.location.host+&apos;/&apos;).length;
var oppId= currentUrl.substring(hostIndex,hostIndex+15);  



j$(function(){
    /*Insert the jQuery style sheets in the Head.*/
    /*Insert the Modal dialog along with the VF as an iframe inside the div.*/
    j$(&quot;head&quot;).after(
        j$(&quot;&lt;link&gt;&quot;,{rel:&quot;stylesheet&quot;,
                    href:&quot;https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css&quot;}));
    j$(&quot;body&quot;).after(
        j$(&quot;&lt;div&gt;&quot;,{id:&quot;modalDiv&quot;,
                    style:&quot;display:none;&quot;
           }).append(
            j$(&quot;&lt;iframe&gt;&quot;,{id:&quot;vfFrame&quot;,
                         src:&quot;/apex/Vals_sharedoPopUPForInspection?oppId=&quot;+oppId,
                         height:400,
                         width:550,
                         frameBorder:0})
           ));
    /*Initialize the Dialog window.*/
    j$(&quot;#modalDiv&quot;).dialog({
        autoOpen: false,
        height: 410,
        width: 600,
        modal:true
    });
});

j$(&quot;#modalDiv&quot;).dialog(&apos;option&apos;,&apos;title&apos;,&apos;ShareDo&apos;).dialog(&apos;open&apos;);</url>
    </webLinks>
    <webLinks>
        <fullName>VIEW_SUMMARY</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>VIEW SUMMARY</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>window.open(&apos;/apex/Vals_SummaryPage?id={!Inspection__c.Id}&apos;, &apos;_blank&apos;);</url>
    </webLinks>
</CustomObject>
