<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object used to store Terms &amp; Conditions data, including scanned contractual documents. T&amp;C&apos;s are rules, that one must agree to abide by in order to use a service.
Initial Implementation Requested by UK Rating (Pilot 07/2015)</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>If the checkbox is ticked the Terms &amp; Conditions is Active and live</inlineHelpText>
        <label>Active</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Company__c</fullName>
        <externalId>false</externalId>
        <label>Company</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Terms &amp; Conditions</relationshipLabel>
        <relationshipName>Terms_Conditions</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>File_Path__c</fullName>
        <externalId>false</externalId>
        <label>File Path</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Link_to_File__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK(&quot;file:///&quot;&amp;File_Path__c,&quot;View&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Link to File</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reason_for_Inactive__c</fullName>
        <externalId>false</externalId>
        <label>Reason for Inactive</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Rating 2005</fullName>
                    <default>false</default>
                    <label>Rating 2005</label>
                </value>
                <value>
                    <fullName>Rating 2005 &amp; 2010</fullName>
                    <default>false</default>
                    <label>Rating 2005 &amp; 2010</label>
                </value>
                <value>
                    <fullName>Rating 2010</fullName>
                    <default>false</default>
                    <label>Rating 2010</label>
                </value>
                <value>
                    <fullName>Rating 2010 &amp; 2017</fullName>
                    <default>false</default>
                    <label>Rating 2010 &amp; 2017</label>
                </value>
                <value>
                    <fullName>Rating 2017</fullName>
                    <default>false</default>
                    <label>Rating 2017</label>
                </value>
                <value>
                    <fullName>MCC</fullName>
                    <default>false</default>
                    <label>MCC</label>
                </value>
                <value>
                    <fullName>Former Clients Pre-2005</fullName>
                    <default>false</default>
                    <label>Former Clients Pre-2005</label>
                </value>
                <value>
                    <fullName>Accurates</fullName>
                    <default>false</default>
                    <label>Accurates</label>
                </value>
                <value>
                    <fullName>Rating Appeals</fullName>
                    <default>false</default>
                    <label>Rating Appeals</label>
                </value>
                <value>
                    <fullName>NI Reval 2015</fullName>
                    <default>false</default>
                    <label>NI Reval 2015</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Terms &amp; Conditions</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Company__c</columns>
        <columns>Active__c</columns>
        <columns>Type__c</columns>
        <columns>CREATED_DATE</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>{000000}</displayFormat>
        <label>Terms &amp; Conditions ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Terms &amp; Conditions</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Company__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Type__c</customTabListAdditionalFields>
        <searchResultsAdditionalFields>Company__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Active__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
