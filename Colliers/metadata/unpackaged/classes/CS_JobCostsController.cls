/*  Class Name: CS_JobCostsController 
 *  Description: This class is a controller for EditOverride Page
 *  Company: dQuotient
 *  CreatedDate: 18/08/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Aparna               21/09/2016                  Orginal Version
 *
 */
public with sharing class CS_JobCostsController{

    public List<CostWrapperClass> lstCostWrapper{get;set;}
    
    public map<string,CostWrapperClass> MapCostWrapperId{get;set;}  
    public string recordId{get;set;} 
    public boolean stagestatus{get;set;}
    public opportunity jobObject{get;set;}
    public CostWrapperClass costdetailwrper{get;set;}
    public boolean renderValue{get;set;}
    public Disbursements__c jobDetailObj{get;set;}
    public integer detailWrpId{get;set;}
    public string selectedObject{get;set;}
    public boolean showPanel{get;set;}
    public boolean isactivecost{get;set;}
    public List<selectoption> allocationoptions{get;set;}
    private Map<string,string> nameTocode ;
    private Map<string,Allocation__c> alloctowork;
    private Set<String> allLst;
    public Opportunity oppobject{get;set;}
    private set<string> worktypenames;
    public ErrorHandling ObjModal{get;set;}
    public List<selectoption> AllocationOpts{get;set;}
    public List<selectoption> RiasedForOpts{get;set;}
    public CS_JobCostsController(){
        RiasedForOpts = new List<selectoption>();
        isactivecost = true;
        AllocationOpts = new List<selectoption>();
        initCost(); 
        //newJobb();
       // getRiasedForOpts();
        //getAllocationOpts();
       // system.debug('---------------------');
    }
    
    public void initCost(){
        ObjModal=new ErrorHandling();
        allocationoptions = new List<selectoption>();
        RiasedForOpts = new List<selectoption>();
        AllocationOpts = new List<selectoption>();
        stagestatus = false;
        showPanel = false;
        isactivecost = true;
        lstCostWrapper=new List<CostWrapperClass>();
        jobObject=new opportunity();
        MapCostWrapperId=new map<string,CostWrapperClass>();
        jobDetailObj=new Disbursements__c(Recoverable__c=true);
        costdetailwrper=new CostWrapperClass();
        allLst = new Set<String>();
        alloctowork = new Map<string,Allocation__c>();
        worktypenames = new set<string>();
        if(ApexPages.currentPage().getParameters().containsKey('id')){
                recordId = ApexPages.currentPage().getParameters().get('id');
                if(!String.isBlank(recordId)){
                    recordId = String.escapeSingleQuotes(recordId);
                    if(recordId InstanceOf Id){
                        List<Allocation__c> alloclist = new List<Allocation__c>();
                        worktypenames  = new set<string>();
                        List<Work_Type__c> workTypelst = new List<Work_Type__c>();
                        oppobject = new Opportunity();
                        oppobject = [SELECT Id, AccountId,isClosed,Account.Name, Name,Relates_To__c,StageName,Amount,Manager__c,Manager__r.Name,Coding_Structure__r.Status__c,Job_Number__c FROM Opportunity WHERE Id = : recordId];
                        if(oppobject.Coding_Structure__r.Status__c != null && string.isNotBlank(oppobject.Coding_Structure__r.Status__c)){
                            if(oppobject.Coding_Structure__r.Status__c == 'Active'){
                                isactivecost = true;
                            }else{
                                isactivecost = false;
                            }
                            
                        }else{
                            isactivecost = false;
                        }
                        alloclist = [SELECT Id,Assigned_To__r.name,workType_Allocation__c FROM Allocation__c WHERE Job__c=:recordId];
                        system.debug('alloclist---->'+alloclist);
                        if(!alloclist.isEmpty()){
                            for(Allocation__c objalloc :alloclist){
                                worktypenames.add(objalloc.workType_Allocation__c);
                            }
                        }
                        nameTocode = new Map<string,string>();
                        alloctowork = new Map<string,Allocation__c>();
                        workTypelst = [SELECT Id,Name,Work_Type_Code__c FROM Work_Type__c WHERE Name IN:worktypenames ];
                        if(!workTypelst.isEmpty()){
                            for(Work_Type__c objwork:workTypelst){
                                nameTocode.put(objwork.Name,objwork.Work_Type_Code__c);
                                
                            }
                        }
                        if(!alloclist.isEmpty()){
                            string staffwork;
                            for(Allocation__c objalloc :alloclist){
                                if(objalloc.workType_Allocation__c !=''){
                                    if(string.isNotBlank(objalloc.Assigned_To__r.name) && string.isNotBlank(nameTocode.get(objalloc.workType_Allocation__c))){
                                    staffwork = ''+objalloc.Assigned_To__r.name+' - '+objalloc.workType_Allocation__c;
                                    }
                                    alloctowork.put(staffwork,objalloc);
                              
                                    allLst.add(staffwork);
                                }
                                
                                worktypenames.add(objalloc.workType_Allocation__c);
                            }
                        }else{
                            allocationOptions.add(new SelectOption('','None',True));
                        }
                        allocationlist();
                            /*List<SelectOption> options = new List<SelectOption>();
                            allLst = new set<String>();
                            system.debug('----------'+costdetailwrper.costObject.Created_For__c);
                            system.debug('----------'+jobObject.id);
                            List<Allocation__c> Sprints =  [Select id, Name, Assigned_To__c, Assigned_To__r.Name, externalCompany__c,workType_Allocation__c From Allocation__c WHERE externalCompany__c =null AND job__c = :recordId AND Assigned_To__c= :costdetailwrper.costObject.Created_For__c AND Complete__c = false ];
                            if(!Sprints.isEmpty()){
                                string staffwork;
                                for(Allocation__c objalloc :Sprints){
                                    if(objalloc.workType_Allocation__c !=''){
                                        system.debug('----------1'+allLst);
                                        if(string.isNotBlank(objalloc.Assigned_To__r.name) && string.isNotBlank(nameTocode.get(objalloc.workType_Allocation__c))){
                                            //staffwork = ''+objalloc.Assigned_To__r.name+' '+nameTocode.get(objalloc.workType_Allocation__c);
                                            staffwork = ''+objalloc.Assigned_To__r.name+' - '+objalloc.workType_Allocation__c;
                                        
                                             system.debug('----------2'+staffwork);
                                        }
                                        else if(string.isNotBlank(objalloc.Assigned_To__r.name) && string.isNotBlank(objalloc.workType_Allocation__c)){
                                             staffwork = ''+objalloc.Assigned_To__r.name+' - '+objalloc.workType_Allocation__c;
                                        }
                                        alloctowork.put(staffwork,objalloc);
                                        
                                        if(!allLst.contains(staffwork) && !string.isBlank(staffwork)){
                                            AllocationOpts.add(new SelectOption(objalloc.Id,staffwork));
                                            system.debug('----------'+options);
                                             allLst.add(staffwork);
                                        }
                                       
                                    }
                                    
                                    worktypenames.add(objalloc.workType_Allocation__c);
                                }
                                 AllocationOpts.add(new SelectOption('','None',True));
                            }else{
                                AllocationOpts.add(new SelectOption('','None',True));
                            }
                            
                            if(AllocationOpts!=null&&AllocationOpts.size()==2){
                                costdetailwrper.costObject.Allocation__c = AllocationOpts[0].getValue();
                                system.debug('Values');
                            }*/
                            RiasedForOpts = new List<selectoption>();
                            List<Allocation__c> Sprints2 = new List<Allocation__c>();
                            Sprints2 =  [Select id, Name, Assigned_To__c, Assigned_To__r.Name, externalCompany__c From Allocation__c WHERE externalCompany__c =null AND job__c = :recordId AND Complete__c= false ];
                            system.debug('--------------------------'+Sprints2);
                            set<id> DuplicateCheck = new set<id>();
                            if(Sprints2!=null&&Sprints2.size()==1){
                                if(costdetailwrper.costObject!=null){
                                    costdetailwrper.costObject.Created_For__c = Sprints2[0].Assigned_To__c;
                                     RiasedForOpts.add(new SelectOption('', '--None--'));
                                    allocationlist();
                                    system.debug('-****************'+costdetailwrper.costObject.Created_For__c+'********'+Sprints2[0].Assigned_To__c);
                                }
                            }
                            else{
                            RiasedForOpts.add(new SelectOption('', '--None--'));
                            }
                            for(Allocation__c spr : Sprints2 ){
                                if(spr.Assigned_To__c !=null && !DuplicateCheck.contains(spr.Assigned_To__c)){
                                    System.debug('----'+spr.Assigned_To__r.Name);
                                    RiasedForOpts.add(new  SelectOption(spr.Assigned_To__c, spr.Assigned_To__r.Name));
                                    DuplicateCheck.add(spr.Assigned_To__c);
                                }
                            }
                             // RiasedForOpts = new List<selectoption>();
                            buildListWrapper();                                                   
                        /*List<Allocation__c> objects = [SELECT Id,Name FROM Allocation__c Where Job__c=:recordId];
                        if(objects.size()>0&&objects!=null){
                            for(Allocation__c allct:objects){
                                allocationOptions.add(new SelectOption(allct.Id,allct.Name));
                            }
                        }
                        else
                            allocationOptions.add(new SelectOption('','None',True));                        
                        system.debug('--->listcheck'+lstCostWrapper);  */              
                }else{
                    // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.InvalidParamterError));
                }
            }else{
               // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.InvalidParamterError));
            }
        }else{
           // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.InvalidParamterError));
        }
        
    }
    
    public void buildListWrapper(){
         list<Disbursements__c> lstCostObject=[SELECT Id,Foreign_Exchange_Rate__c,Allocation__c,job__c,job__r.StageName,Created_For__r.Name,Created_For__c,Description__c,Category__c,
                        //Disbursement_Category_SIFT_Corrected__c,
                        Invoice_Details__c,Foreign_amount__c,Sub_Category__c,job__r.isClosed,vat_amount__c,vatamt__c,Invoiceid__r.Assigned_Invoice_Number__c,Credit_Noted__c ,Purchase_Date__c,Purchase_Cost__c,CCY_values__c,Recoverable__c,Recharge_Cost__c,Status__c,Foreign_Disbursement__c FROM Disbursements__c WHERE Job__c=:recordId AND (Status__c != 'Invoiced' AND Status__c != 'Non Recoverable') order by CreatedDate desc Limit 1000];
                        system.debug('--->costsss'+lstCostObject);
                        jobObject=[SELECT Manager__c, Manager__r.Name,Name,isClosed,Job_Number__c  FROM opportunity WHERE ID=:recordId];
                        
                        if(!lstCostObject.isEmpty()){
                            if(lstCostObject[0].job__r.StageName == 'Closed Won' || lstCostObject[0].job__r.StageName == 'Closed Lost' ){
                                stagestatus = true;
                            }else{
                                stagestatus = false;
                            }
                           
                        }
                        if(jobObject.isClosed){
                            stagestatus = true;
                        }
                        updateCostTable(lstCostObject);   
    }
    public void allocationlist(){
        AllocationOpts = new List<selectoption>();
        AllocationOpts = allocationlistvalues();
    }
    public void updateCostTable(list<Disbursements__c> lstCostObject){
        integer counter=0;
        lstCostWrapper.clear();
        for(Disbursements__c obj:lstCostObject){
            if(costdetailwrper!=null&&costdetailwrper.costObject!=null){
                system.debug(costdetailwrper.ischecked+'----'+costdetailwrper.costObject.id);
            }
                            counter++;
                            if(costdetailwrper!=null&&costdetailwrper.costObject!=null&&(costdetailwrper.costObject.id==obj.id)){
                                costdetailwrper=new CostWrapperClass(counter,true,obj,obj.Foreign_Disbursement__c);
                                lstCostWrapper.add(costdetailwrper); 
                                MapCostWrapperId.put(obj.id,new CostWrapperClass(counter,true,obj,obj.Foreign_Disbursement__c));                        
                            }else{ 
                                lstCostWrapper.add(new CostWrapperClass(counter,false,obj,obj.Foreign_Disbursement__c));
                                MapCostWrapperId.put(obj.id,new CostWrapperClass(counter,false,obj,obj.Foreign_Disbursement__c));
                            }                       
                        } 
        
    }                         
    public void detailJob(){
        system.debug('heyyyy'+detailWrpId);
        //integer extId = integer.valueOf(detailWrpId);
        //
        integer extId = detailWrpId;        
        system.debug('heyyyy'+extId);       
            for(CostWrapperClass wrapperObject : lstCostWrapper){                
                if(wrapperObject.wrapperId == extId){                                   
                    wrapperObject.isChecked= true;
                    costdetailwrper=wrapperObject;              
                }else{
                    wrapperObject.isChecked= false;  
                }                   
            }
            if(!costdetailwrper.costObject.Recoverable__c){
                costdetailwrper.costObject.Recharge_Cost__c=0; 
            }
            allocationlist();
            system.debug('-->heyy'+costdetailwrper);
            system.debug('updatedList'+lstCostWrapper);
            buildListWrapper();
    }
    public void newJobb(){
        //initCost();
        
        List<CostWrapperClass> newLst = new List<CostWrapperClass>();
        Disbursements__c newJob = new Disbursements__c(Recoverable__c=true);
         integer counterfnl=0;
         showPanel = true;
         if(!lstCostWrapper.isEmpty()){
             
            newJob.Job__c=recordId;
            newJob.Recoverable__c = true;
            newLst.add(new CostWrapperClass(counterfnl+1,true,newJob,false));
            costdetailwrper=new CostWrapperClass(counterfnl+1,false,newJob,false);
            for(CostWrapperClass jobObj : lstCostWrapper){
                jobObj.isChecked=false;
                //counterfnl=jobObj.wrapperId;
                jobObj.wrapperId = jobObj.wrapperId +1;
                newLst.add(jobObj);
            }
         }else{
            newJob.Job__c=recordId;
            newJob.Recoverable__c = true;
            newLst.add(new CostWrapperClass(counterfnl+1,true,newJob,false));
            costdetailwrper=new CostWrapperClass(counterfnl+1,false,newJob,false);   
             
         }
         if(RiasedForOpts.size() ==1){
             if(RiasedForOpts[0].getvalue() != null && RiasedForOpts[0].getvalue() != ''){
                 costdetailwrper.costObject.Created_For__c = RiasedForOpts[0].getvalue();
                allocationlist();
             }           
         }
         system.debug('recordId--->'+recordId);
         system.debug('recordId--->'+RiasedForOpts[0].getvalue());
         system.debug('recordId---'+costdetailwrper.costObject.Created_For__c);
         lstCostWrapper.clear();
         lstCostWrapper.addAll(newLst);
         
        //  getRiasedForOpts();
        
    }

    public void save(){
        list<Disbursements__c> lstofObjectInsert = new list<Disbursements__c>();
        set<String> insrtLstSet = new set<string>();
        integer counterfnl;        
        integer detilno=costdetailwrper.wrapperId;
        system.debug('detailno.----->'+costdetailwrper);
        system.debug('detailno.----->'+costdetailwrper);
        /*for(CostWrapperClass wrapperObject : lstCostWrapper){                
                if(wrapperObject.wrapperId == detilno){                                   
                       wrapperObject.costObject=costdetailwrper.costObject;
                       system.debug( '--->detailchanged'+wrapperObject.costObject);
                       //update wrapperObject.costObject;
                }                
            }  */
         //update lstCostWrapper;
        system.debug('-->inside save and new');     
        for(CostWrapperClass jobObj : lstCostWrapper){
            if(jobObj.costObject.Job__c==null){           
                jobObj.costObject.put('Job__c',recordId);
            }
           if(jobObj.wrapperId==detilno){
                jobObj.isChecked=true;
            
            }else{
                jobObj.isChecked=false;
            }
            
            counterfnl=jobObj.wrapperId;
        }
            lstofObjectInsert.add(costdetailwrper.costObject);
            system.debug('costdetailwrper.costObject---->'+costdetailwrper.costObject);
            system.debug('costdetailwrper.costObject.Created_For__c----->'+costdetailwrper.costObject.Created_For__c);
            system.debug('costdetailwrper.costObject.Allocation__c----->'+costdetailwrper.costObject.Allocation__c);
            system.debug('costdetailwrper.costObject.Purchase_Date__c----->'+costdetailwrper.costObject.Purchase_Date__c);
        if(isactivecost){
            try{
               // if(lstofObjectInsert != null && lstofObjectInsert.size()>0){
               Boolean isInsrt = validate(costdetailwrper.costObject);
               
                    if(isInsrt){
                        if(lstofObjectInsert[0].id == null){
                            lstofObjectInsert[0].Status__c = 'To be Invoiced';
                        }
                        upsert lstofObjectInsert;
                        
                     //   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, )); 
                          ObjModal=new ErrorHandling(true,'Cost Record created Successfully','Success'); 
                        if(!costdetailwrper.isChecked){
                            costdetailwrper.isChecked=true;
                            lstCostWrapper.add(costdetailwrper);
                        }
                        buildListWrapper();
                      //   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, ));
                        //  ObjModal=new ErrorHandling(true,'Record Saved!','Success'); 
                    }
    
                    System.debug('saveRecords inserted'+lstofObjectInsert);
                    
                
               /* for(CostWrapperClass jobObj1 : lstCostWrapper){
                    if(jobObj1.wrapperId==detilno){
                        string st=jobObj1.costObject.Id;
                        Disbursements__c cstobjqry=[SELECT Id,Foreign_Exchange_Rate__c,job__c,Allocation__c,Created_For__r.Name,Created_For__c,Description__c,Category__c,Disbursement_Category_SIFT_Corrected__c,Invoice_Details__c,Foreign_amount__c,Sub_Category__c,Purchase_Date__c,Credit_Noted__c ,Purchase_Cost__c,Recoverable__c,Recharge_Cost__c,CCY_values__c,Status__c,Foreign_Disbursement__c FROM Disbursements__c WHERE Job__c=:recordId AND ID=:st];
                        jobObj1.costObject=cstobjqry;
                    }
                }*/
            }
            catch(Exception e){
              //ApexPages.addmessages(e); 
                 ObjModal=new ErrorHandling(true,e.getMessage(),'Error'); 
              //return null;
            }
        }else{
             ObjModal=new ErrorHandling(true,'Job Manager’s Coding Structure is Inactive – Please contact the System Administrator to change the job manager on the job','Error');
        }
    }
    
    
    public boolean validate(Disbursements__c JCost){
        Boolean isInsrt = true;
        
                ObjModal = new ErrorHandling();
                if(!string.isBLANK(JCost.Created_For__c)){
                      
                }else{
                    isInsrt = false;
                    ObjModal.ErrorHandling(true,'Raised for: You must select a value.','Error',true); 
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ));     
                }
                if(!string.isBLANK(JCost.Allocation__c)){
                     
                }else{
                    isInsrt = false;
                    ObjModal.ErrorHandling(true,'Allocation: You must select a value.','Error',true); 
                  //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ));
                }
                if(JCost.Purchase_Date__c != null){
                    
                }else{
                    isInsrt = false;
                   // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ));
                    ObjModal.ErrorHandling(true,'Please Select the Purchase Date.','Error',true); 
                }
                if(!string.isBLANK(JCost.Category__c)){
                    
                }else{
                    isInsrt = false;
                    ObjModal.ErrorHandling(true,'Please Select the Category.','Error',true); 
                   // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ));
                }
                if(!string.isBLANK(JCost.Sub_Category__c)){
                    
                }else{
                    isInsrt = false;
                     ObjModal.ErrorHandling(true,'Please Select the Sub Category.','Error',true); 
                   // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ));
                }
                if(!string.isBLANK(JCost.Description__c)){
                    
                }else{
                    isInsrt = false;
                     ObjModal.ErrorHandling(true,'Description : You must enter a value.','Error',true); 
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ));
                }
                if(JCost.Purchase_Cost__c != null&&JCost.Purchase_Cost__c >0){
                    
                }else{
                    isInsrt = false;
                    ObjModal.ErrorHandling(true,'Purchase Cost : You must enter a value.','Error',true); 
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ));
                }
                if(!string.isBLANK(JCost.Invoice_Details__c)){
                    
                }else{
                    isInsrt = false;
                    ObjModal.ErrorHandling(true,'Invoice Details : You must enter a value. ','Error',true); 
                   // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ));
                }
                // if(JCost.Recharge_Cost__c!=null){
                    // if(JCost.Recharge_Cost__c>0&&JCost.Recharge_Cost__c<=JCost.Purchase_Cost__c){
                    
                    // }else{
                        // isInsrt = false;
                        // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Recharge cost : Value must be positive and less than Purchase cost'));
                    // }
                // }else{
                    // isInsrt = false;
                    // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Recharge cost : You must enter a value.'));
                // }
        
        
            return isInsrt;
    }
    
    public void close_ErrorModal(){
        ObjModal=new ErrorHandling();
    }
    
    
    public void saveAndNew(){
        try{
            save();
            Boolean isInsrt = validate(costdetailwrper.costObject);
            if(isInsrt){
                newJobb();
            }              
                        
                  
        }           
        catch(Exception e){
            // ApexPages.addmessages(e);   
            ObjModal=new ErrorHandling(true,e.getMessage(),'Error'); 
        }
    }
        // Logic for populating Allocations
        public List<SelectOption> allocationlistvalues() {
            List<SelectOption> options = new List<SelectOption>();
            allLst = new set<String>();
            system.debug('----------'+costdetailwrper.costObject.Created_For__c);
            //system.debug('----------'+jobObject.id);
            List<Allocation__c> Sprints =  [Select id, Name, Assigned_To__c, Assigned_To__r.Name, externalCompany__c,workType_Allocation__c From Allocation__c WHERE externalCompany__c =null AND job__c = :jobObject.id AND Assigned_To__c= :costdetailwrper.costObject.Created_For__c AND Complete__c = false ];
            if(!Sprints.isEmpty()){
                            string staffwork;
                            options.add(new SelectOption('','---None---'));
                            if(Sprints != null && Sprints.size()==1){
                                costdetailwrper.costObject.Allocation__c = Sprints[0].Id;
                                staffwork =  ''+Sprints[0].Assigned_To__r.name+' - '+Sprints[0].workType_Allocation__c;
                                options.add(new SelectOption(Sprints[0].Id,staffwork));
                                
                            }else{
                                for(Allocation__c objalloc :Sprints){
                                    if(objalloc.workType_Allocation__c !=''){
                                        system.debug('----------1'+allLst);
                                        if(string.isNotBlank(objalloc.Assigned_To__r.name) && string.isNotBlank(nameTocode.get(objalloc.workType_Allocation__c))){
                                            //staffwork = ''+objalloc.Assigned_To__r.name+' '+nameTocode.get(objalloc.workType_Allocation__c);
                                            staffwork = ''+objalloc.Assigned_To__r.name+' - '+objalloc.workType_Allocation__c;
                                        
                                             system.debug('----------2'+staffwork);
                                        }
                                        else if(string.isNotBlank(objalloc.Assigned_To__r.name) && string.isNotBlank(objalloc.workType_Allocation__c)){
                                             staffwork = ''+objalloc.Assigned_To__r.name+' - '+objalloc.workType_Allocation__c;
                                        }
                                        alloctowork.put(staffwork,objalloc);
                                        
                                        if(!allLst.contains(staffwork) && !string.isBlank(staffwork)){
                                            options.add(new SelectOption(objalloc.Id,staffwork));
                                            system.debug('----------'+options);
                                             allLst.add(staffwork);
                                        }
                                       
                                    }
                                    
                                    worktypenames.add(objalloc.workType_Allocation__c);
                                }
                            }
                            
                            
            }else{
                options.add(new SelectOption('','---None---'));
            }
            
            /*if(options!=null&&options.size()==2){
                costdetailwrper.costObject.Allocation__c = options[0].getValue();
                system.debug('Values');
            }*/
            return options;
            
        }
        // Logic for populating the raised for
        
        // public List<SelectOption> getRiasedForOpts() {     
        // List<SelectOption> options = new List<SelectOption>();
        // List<Allocation__c> Sprints =  [Select id, Name, Assigned_To__c, Assigned_To__r.Name, externalCompany__c From Allocation__c WHERE externalCompany__c =null AND job__c = :jobObject.id AND Complete__c= false ];
        // system.debug('--------------------------'+Sprints);
        // set<id> DuplicateCheck = new set<id>();
        // if(Sprints!=null&&Sprints.size()==1){
            // if(costdetailwrper.costObject!=null){
                // costdetailwrper.costObject.Created_For__c = Sprints[0].Assigned_To__c;
                 // options.add(new SelectOption('', '--None--'));
                // getAllocationOpts();
                // system.debug('-****************'+costdetailwrper.costObject.Created_For__c+'********'+Sprints[0].Assigned_To__c);
            // }
        // }
        // else{
        // options.add(new SelectOption('', '--None--'));
        // }
        // for(Allocation__c spr : Sprints ){
            // if(spr.Assigned_To__c !=null && !DuplicateCheck.contains(spr.Assigned_To__c)){
            // options.add(new  SelectOption(spr.Assigned_To__c, spr.Assigned_To__r.Name));
            // DuplicateCheck.add(spr.Assigned_To__c);
            // }
        // }
        // return options;
    // }
        // Logic to manage REcharge cost and Purchase Cost
        public void UpdateRechargeCost(){
            if(costdetailwrper!=null&&costdetailwrper.costObject!=null&&costdetailwrper.costObject.Recoverable__c){
                costdetailwrper.costObject.Recharge_Cost__c = costdetailwrper.costObject.Purchase_Cost__c;
            }
            else{
                costdetailwrper.costObject.Recharge_Cost__c = 0;
            }
        }
        
        // Logic to flow Description to Invoice Details
        public void UpdateInvoiceDetail(){
            
            if(costdetailwrper!=null&&costdetailwrper.costObject!=null&&costdetailwrper.costObject.Description__c!=null){
                costdetailwrper.costObject.Invoice_Details__c = costdetailwrper.costObject.Description__c;
            }
           
        }
    
    
     public void deleteRow(){       
        String delId = ApexPages.currentPage().getParameters().get('deletedID');
        System.debug('delId---->'+delId);
        Integer index = 0;
        Disbursements__c s ;
        Boolean hasRec = false;
        for(CostWrapperClass r: lstCostWrapper){
            if(String.valueof(r.wrapperId )== delId){
                s = r.costObject;
                hasRec = true;
                break;
            }
            index++;
        }
        if(hasRec){
            boolean checkvalue  = false;
            if(lstCostWrapper[index].isChecked){
                
                if(lstCostWrapper.size()>1 && !lstCostWrapper.isEmpty()){
                    lstCostWrapper[index-1].isChecked = true;
                    costdetailwrper = lstCostWrapper.get(index-1);
                }else if(lstCostWrapper.size()==1 && costdetailwrper != null){
                    checkvalue = true;
                    
                }
                
            }
            else if(lstCostWrapper.size()==1 && costdetailwrper.costObject == null){
                Disbursements__c newJob = new Disbursements__c(Recoverable__c=true);
                newJob.Job__c=recordId;    
                costdetailwrper=new CostWrapperClass(1,false,newJob,false);
            }
            lstCostWrapper.remove(index);
            if(checkvalue){
                Disbursements__c newJob = new Disbursements__c(Recoverable__c=true);
                newJob.Job__c=recordId;    
                costdetailwrper=new CostWrapperClass(1,false,newJob,false);
            }
            if(s.get('id') != null){
                try{
                delete s;
                
                }
                catch(Exception e){
                      ObjModal=new ErrorHandling(true,e.getMessage(),'Error'); 
                   // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,));    
                }
            }
        }
        
    }
    
    /* public PageReference cancel(){
       PageReference OpporunityPageCancel = new PageReference('/apex/CS_ManageMyJobPage?id='+recordId);
       OpporunityPageCancel.setRedirect(true);
       return OpporunityPageCancel; 
       return null;
    } */
    public void renderpanel(){
       string boolvalue = System.currentPagereference().getParameters().get('checkedvalue'); 
       if(boolvalue == 'true'){
           costdetailwrper.rendervalue = true;
       }else{
           costdetailwrper.rendervalue = false;
       }
    }
    
    public class CostWrapperClass {        
        public Boolean isChecked {get;set;}
        public Disbursements__c costObject{get;set;}
        public string InvoiceNumber{get;set;}
        Public decimal VAT{get;set;}
        public integer wrapperId{get;set;}
        public Boolean rendervalue {get;set;}
        public CostWrapperClass(integer wrapperId2,Boolean isChecked2,Disbursements__c costObject2,Boolean render) {
            isChecked = isChecked2;
            rendervalue = render;
            costObject=costObject2;    
            wrapperId=wrapperId2;
            if(costObject.id!=null&&costObject.Allocation_Cost_Junctions__r!=null&&costObject.Allocation_Cost_Junctions__r.size()>0){
                VAT = costObject.Allocation_Cost_Junctions__r[0].Invoice__r.VAT_Amount__c;
                InvoiceNumber = costObject.Allocation_Cost_Junctions__r[0].Invoice__r.Name; 
                 
            }
        }
        public CostWrapperClass(){}    
    }  
    
    public class ErrorHandling{
        public boolean displayerror{get;set;}
        public string Errormsg{get;set;}
        public string Title{get;set;}
        Public List<string> ErrorList{get;set;}
        public boolean hasmore{get;set;}
        public ErrorHandling(boolean displayerror, string Errormsg,string Title){
            this.displayerror =displayerror;
            this.Errormsg=Errormsg;
            this.Title=Title;
            this.hasmore=false;
            ErrorList = new List<string>();
        }
        public  void ErrorHandling(boolean displayerror, string Errormsg,string Title,boolean hasmore){
            this.displayerror =displayerror;
            ErrorList.add(Errormsg);
            this.Title=Title;
            this.hasmore=hasmore;
        }
        public ErrorHandling(){
            this.displayerror =false;
            this.Errormsg='';
            ErrorList = new List<string>();
        }
    }
}