/**
 *  Class Name: SharingRuleConfigurationExtension
 *  Description: Extension for hte VF page SharingRuleConfiguration
 *  Company: CloudShift
 *  CreatedDate: 29/05/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Gavin Palmer             29/05/2018                 Orginal Version
 *
 */
public without sharing class SharingRuleConfigurationExtension {

    @testVisible private static final String MISSING_SHARING_CONDITIONS = 'You must include some sharing conditions when creating a sharing rule';
    @testVisible private final Integer NUM_OF_NEW_CONDITIONS_WHEN_ADDING_NEW;

	@testVisible private final Sharing_Rule__c sharingRule;
    public final List<Sharing_Condition__c> sharingConditions {get; set;}
    @testVisible private final List<Sharing_Condition__c> sharingConditionsToDelete = new List<Sharing_Condition__c>();
    public String selectedGroupType {get; set;}
    public String conditionToDeleteIndex {get; set;}
    public Boolean isNewRecord {get; set;}

    public SharingRuleConfigurationExtension(ApexPages.StandardController controller) {
        sharingRule = (Sharing_Rule__c) controller.getRecord();
        if(String.isNotBlank(sharingRule.Shared_With__c) && sharingRule.Shared_With__c instanceof Id){
            Id groupId = sharingRule.Shared_With__c;
            sharingRule.Shared_With__c = groupId;
        }

        Boolean emptyConditionsRowsIsValidNumber = (String.isNotBlank(Label.Sharing_Conditions_Empty_Rows_Add_Count) && Label.Sharing_Conditions_Empty_Rows_Add_Count.isNumeric());
        Integer tmpInt = (emptyConditionsRowsIsValidNumber ? Integer.valueOf(Label.Sharing_Conditions_Empty_Rows_Add_Count) : 1);
        NUM_OF_NEW_CONDITIONS_WHEN_ADDING_NEW = (tmpInt>0 ? tmpInt : 1);
                
        isNewRecord = String.isBlank(sharingRule.Id);
        sharingConditions = isNewRecord ? new List<Sharing_Condition__c>() : [
            SELECT Field_API_Name__c, Operator__c, Value__c, Sharing_Rule__c, Index__c
            FROM Sharing_Condition__c
            WHERE Sharing_Rule__c = :sharingRule.Id ORDER BY Index__c ASC
        ];
        if (!isNewRecord) {
            defaultSelectedGroupType();
        }
    }

    private void defaultSelectedGroupType() {
        List<Group> groups = [
            SELECT Type
            FROM Group
            WHERE Id = :sharingRule.Shared_With__c
        ];
        selectedGroupType = groups.isEmpty() ? '' : groups[0].Type;
    }

    public List<SelectOption> getGroupTypes() {
        List<SelectOption> groupTypes = new List<SelectOption>();
        groupTypes.add(new SelectOption('', '--None--'));
        for (PicklistEntry picklistValue : Group.Type.getDescribe().getPicklistValues()) {
            String groupName = picklistValue.getValue();
            String groupLabel = (groupName=='Regular' ? Label.Sharing_Rules_Public_Group : groupName);
            groupTypes.add(new SelectOption(groupName, groupLabel));
        }
        return groupTypes;
    }

    public List<SelectOption> getGroupOptions() {
        List<SelectOption> groupOptions = new List<SelectOption>();
        groupOptions.add(new SelectOption('', '--None--'));
        if (String.isNotBlank(selectedGroupType)) {
            for (Group sharingGroup : [
                SELECT DeveloperName, Name
                FROM Group
                WHERE Type = :selectedGroupType
                LIMIT 999
            ]) {
                String name = String.isBlank(sharingGroup.Name) ? sharingGroup.DeveloperName : sharingGroup.Name;
                groupOptions.add(new SelectOption(sharingGroup.Id, name));
            }
        }
        return groupOptions;
    }

    public List<SelectOption> getObjectFields() {
        List<SelectOption> objectFieldOptions = new List<SelectOption>();
        objectFieldOptions.add(new SelectOption('', '--None--'));
        if (String.isNotBlank(sharingRule.Object__c)) {
            Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
            if (globalDescribe.containsKey(sharingRule.Object__c)) {
                for (Schema.SObjectField field : globalDescribe.get(sharingRule.Object__c).getDescribe().fields.getMap().values()) {
                    Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
                    String fieldType = fieldDescribe.getType().name().toLowerCase();
                    fieldType = (fieldType=='reference' ? 'id' : fieldType);
                    objectFieldOptions.add(new SelectOption(fieldDescribe.getName(), fieldDescribe.getLabel() + ' (' + fieldType.capitalize() + ')'));
                }
            }
        }
        objectFieldOptions.sort();
        return objectFieldOptions;
    }

    public void removeSelectedCondition() {
        Sharing_Condition__c conditionToRemove = sharingConditions.remove(Integer.valueOf(conditionToDeleteIndex));
        if (String.isNotBlank(conditionToRemove.Id)) {
            sharingConditionsToDelete.add(conditionToRemove);
        }
    }

    public void addSharingCondition(){
        for(Integer i=0; i<NUM_OF_NEW_CONDITIONS_WHEN_ADDING_NEW; i++){
            sharingConditions.add(new Sharing_Condition__c());
        }
    }

    public PageReference save() {
        PageReference page;
        if (isValidSharingRule()) {
            Database.UpsertResult sharingRuleSaveResult;
            Savepoint beforeSave = Database.setSavepoint();
            try {
                sharingRule.Change_Status__c = SharingEngine.SHARING_RULE_CHANGE_STATUS_IN_PROGRESS;
                sharingRuleSaveResult = Database.upsert(sharingRule);

                addSharingRuleIdToConditions();

                if (!sharingConditionsToDelete.isEmpty()) {
                    delete sharingConditionsToDelete;
                }

               /*
               if getting error on save "Data Not Available" 
                List<Sharing_Condition__c>tmpUpdate = new List<Sharing_Condition__c>();
                List<Sharing_Condition__c>tmpInsert = new List<Sharing_Condition__c>();
                for(Sharing_Condition__c c : sharingConditions){
                    if(String.isBlank(c.Id)){
                        c.Sharing_Rule__c = sharingRule.Id;
                        tmpInsert.add(c);
                    }else{
                        tmpUpdate.add(c);
                    }
                }

                //
                upsert tmpUpdate; //upsert for existing records works
                upsert tmpInsert; //upsert for new records fails */
                upsert sharingConditions;

                sharingRule.Change_Status__c = SharingEngine.SHARING_RULE_CHANGE_STATUS_COMPLETED;
                update sharingRule;

                page = (new ApexPages.StandardController(sharingRule)).view();
            } catch(Exception e) {
                System.debug('ex >>> ' + e);
                System.debug('ex >>> ' + e.getStackTraceString());
                Database.rollback(beforeSave);
                clearSharingRuleIdsOnCreate(sharingRuleSaveResult);
                ApexPages.addMessages(e);
            }
        }
        return page;
    }

    // Clear sharing rule ids to allow successive save attempts
    private void clearSharingRuleIdsOnCreate(Database.UpsertResult saveResult) {
        if (saveResult != null && saveResult.isCreated()) {
            sharingRule.Id = null;
            List<Sharing_Condition__c> conditionsWithoutLookup = new List<Sharing_Condition__c>();
            for (Sharing_Condition__c condition : sharingConditions) {
                Sharing_Condition__c clonedCondition = condition.clone(false, true);
                clonedCondition.Sharing_Rule__c = null;
                conditionsWithoutLookup.add(clonedCondition);
            }
            sharingConditions.clear();
            sharingConditions.addAll(conditionsWithoutLookup);
        }
    }

    private Boolean isValidSharingRule() {
        Boolean isValidSharingRule = true;
        if (sharingConditions.isEmpty()) {
            isValidSharingRule = false;
            ApexPages.addMessage(
                new ApexPages.message(ApexPages.Severity.ERROR, MISSING_SHARING_CONDITIONS)
            );
        }
        return isValidSharingRule;
    }

    private void addSharingRuleIdToConditions() {
        for (Sharing_Condition__c condition : sharingConditions) {
            // we should only update the sharing rule id if it doesn't already exist as salesforce will interpret this as an id change (even if they are the same)
            if (String.isBlank(condition.Sharing_Rule__c)) {
                condition.Sharing_Rule__c = sharingRule.Id;
            }
        }
    }

    public String getSHARING_LOGIC_CUSTOM_LABEL(){
        return SharingEngine.SHARING_LOGIC_CUSTOM;
    }
}