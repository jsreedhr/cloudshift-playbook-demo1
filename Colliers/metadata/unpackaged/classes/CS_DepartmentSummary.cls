/**
 *  Class Name:  CS_DepartmentSummary 
 *  Description: This is a class to show All information related to a company in one page 
 *  Company: dQuotient
 *  CreatedDate: 8/12/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer      Modification Date      Comments
 *  -----------------------------------------------------------
 *  Anand M          9/12/2016           Orginal Version
 *
 *
 */
public with sharing class CS_DepartmentSummary{
    
    
    private List<id> lstAccountDisplayId;
    private List<Opportunity> lstOpportunityDisplay;
    private List<Event> lstMeeting;
    private list<CampaignMember> lstCampaignMember;
    private map<id, List<Opportunity>> mapAccountOpportunity;
    
    public set<String> setDept{get;set;}
    public integer deptNo{get;set;}
    public integer deptOpenNo{get;set;}
    public integer deptPitchingNo{get;set;}
    public integer deptClosedNo{get;set;}
    public integer deptInstructedNo{get;set;}
    public integer deptJobLiteNo{get;set;}
    public integer deptOtherNo{get;set;}
    public map<String,integer> mapDeptJObNo{get;set;}
    public map<String,integer> mapDeptOpen{get;set;}
    public map<String,integer> mapDeptPitching{get;set;}
    public map<String,integer> mapDeptClosed{get;set;}
    public map<String,integer> mapDeptInstructed{get;set;}
    public map<String,integer> mapDeptJobLite{get;set;}
    public map<String,integer> mapDeptOther{get;set;}
    public List<Account> lstAccountDisplay{get;set;}
    /**
    *  Method Name: CS_CompanyInformationPDFController
    *  Description: Constructor for CS_CompanyInformationPDFController
    *  Param:  None
    *  Return: None
    */
    public CS_DepartmentSummary(ApexPages.StandardController stdController) {
        
        
        Account objAccount = new Account();
        set<id> setAccountIds = new set<id>();
        set<id> setContactIds = new set<id>();
        lstAccountDisplay = new List<Account>();
        List<Account> lstAccount = new List<Account>();
        List<Contact> lstContact = new List<Contact>();
        lstOpportunityDisplay = new List<Opportunity>();
        
        
        mapAccountOpportunity = new map<id, List<Opportunity>>();
        setDept = new set<String>();
        mapDeptJObNo = new map<String,integer>();
        mapDeptOpen = new map<String,integer>();
        mapDeptPitching = new map<String,integer>();
        mapDeptClosed = new map<String,integer>();
        mapDeptInstructed = new map<String,integer>();
        mapDeptJobLite = new map<String,integer>();
        mapDeptOther = new map<String,integer>();
        lstAccountDisplayId = new List<id>();
        deptNo = 0;
        deptOpenNo = 0;
        deptPitchingNo = 0;
        deptClosedNo = 0;
        deptInstructedNo = 0;
        deptJobLiteNo = 0;
        deptOtherNo = 0;
        if(stdController != null){
            objAccount = (Account)stdController.getRecord();
            integer noOFIterations = 1;
            setAccountIds.add(objAccount.id);
            
            while(!setAccountIds.isEmpty()){
                lstAccount = new List<Account>();
                if(noOFIterations == 1){
                    lstAccount =[Select id, name,
                                            ParentId,
                                            Parent.Name,
                                            Parent.ParentId,
                                            Parent.Parent.Name, 
                                            Parent.Parent.ParentId,
                                            Parent.Parent.Parent.Name,
                                            Parent.Parent.Parent.ParentId,
                                            Parent.Parent.Parent.Parent.Name,
                                            Parent.Parent.Parent.Parent.ParentId,
                                            Parent.Parent.Parent.Parent.Parent.Name
                                            From 
                                            Account
                                            where 
                                            id in: setAccountIds
                                            or ParentId in: setAccountIds
                                            or ParentId in: setAccountIds
                                            or Parent.ParentId in: setAccountIds
                                            or Parent.Parent.ParentId in: setAccountIds
                                            or Parent.Parent.Parent.ParentId in: setAccountIds
                                            or Parent.Parent.Parent.Parent.ParentId in: setAccountIds
                                            order by Parent.Parent.Parent.Parent.Parent.Name, Parent.Parent.Parent.Parent.Name, Parent.Parent.Parent.Name,
                                            Parent.Parent.Name, Parent.Name, Name Limit 100];
                                        
               
                }else{
                    lstAccount =[Select id, name,
                                            ParentId,
                                            Parent.Name,
                                            Parent.ParentId,
                                            Parent.Parent.Name, 
                                            Parent.Parent.ParentId,
                                            Parent.Parent.Parent.Name,
                                            Parent.Parent.Parent.ParentId,
                                            Parent.Parent.Parent.Parent.Name,
                                            Parent.Parent.Parent.Parent.ParentId,
                                            Parent.Parent.Parent.Parent.Parent.Name
                                            From 
                                            Account
                                            where 
                                            ParentId in: setAccountIds
                                            or ParentId in: setAccountIds
                                            or Parent.ParentId in: setAccountIds
                                            or Parent.Parent.ParentId in: setAccountIds
                                            or Parent.Parent.Parent.ParentId in: setAccountIds
                                            or Parent.Parent.Parent.Parent.ParentId in: setAccountIds
                                            order by Parent.Parent.Parent.Parent.Parent.Name, Parent.Parent.Parent.Parent.Name, Parent.Parent.Parent.Name,
                                            Parent.Parent.Name, Parent.Name, Name Limit 100];
                    
                }
                                        
                setAccountIds = new set<id>();                     
                for(Account objAccountTopParent: lstAccount){
                    if(objAccountTopParent.Parent.Parent.Parent.Parent.Parentid != null){
                        setAccountIds.add(objAccountTopParent.id);
                    }
                }
                lstAccountDisplay.addAll(lstAccount);
                
                if(noOFIterations >= 20){
                    break;
                }
                noOFIterations = noOFIterations+1;
                if(setAccountIds.size() == 0){
                    break;
                }
            }
            
            for(Account objAccountFinal: lstAccountDisplay){
                lstAccountDisplayId.add(objAccountFinal.id);
                // mapAccountOpportunity.put(objAccountFinal.id, new List<Opportunity>());
            }
            
            System.debug('----->'+lstAccountDisplayId);
            List<Opportunity> lstOpportunityTemp = new List<Opportunity>();
            if(!lstAccountDisplayId.isEmpty()){
                lstOpportunityDisplay = new List<Opportunity>();
                lstOpportunityDisplay = [Select id, name,
                                        AccountId,
                                        Account.Name,
                                        Date_Instructed__c,
                                        StageName,
                                        Manager__c,
                                        Manager__r.Name,
                                        Manager__r.Department__c,
                                        Manager__r.Office__c,
                                        Department__c,
                                        Office__c,
                                        Work_Type__c,
                                        Job_Number__c
                                        From Opportunity
                                        where
                                        AccountId in: lstAccountDisplayId
                                        and CloseDate > LAST_FISCAL_YEAR
                                        and Manager__c != null
                                        order by Date_Instructed__c desc Limit 5000];
                                                
            }
            // System.debug('----->'+lstOpportunityDisplay);
            
            for(Opportunity objOppDisplay: lstOpportunityDisplay){
                deptNo +=1;
                // if(objOppDisplay.Manager__c  != null)
                if(!String.isBlank(objOppDisplay.Manager__r.Department__c)){
                    setDept.add(objOppDisplay.Manager__r.Department__c);
                    if(mapDeptJObNo.containsKey(objOppDisplay.Manager__r.Department__c)){
                        integer currentNo = mapDeptJObNo.get(objOppDisplay.Manager__r.Department__c);
                        currentNo += 1;
                        mapDeptJObNo.put(objOppDisplay.Manager__r.Department__c, currentNo);
                    }else{
                        mapDeptJObNo.put(objOppDisplay.Manager__r.Department__c, 1);
                    }
                    
                    if(!String.isBlank(objOppDisplay.StageName)){
                    
                        if(objOppDisplay.StageName.equalsIgnoreCase('Open')){
                            deptOpenNo+= 1;
                             if(mapDeptOpen.containsKey(objOppDisplay.Manager__r.Department__c)){
                                integer currentNo = mapDeptOpen.get(objOppDisplay.Manager__r.Department__c);
                                currentNo += 1;
                                mapDeptOpen.put(objOppDisplay.Manager__r.Department__c, currentNo);
                            }else{
                                mapDeptOpen.put(objOppDisplay.Manager__r.Department__c, 1);
                            }
                            
                        }else if(objOppDisplay.StageName.equalsIgnoreCase('Pitching')){
                            deptPitchingNo+= 1;
                             if(mapDeptPitching.containsKey(objOppDisplay.Manager__r.Department__c)){
                                integer currentNo = mapDeptPitching.get(objOppDisplay.Manager__r.Department__c);
                                currentNo += 1;
                                mapDeptPitching.put(objOppDisplay.Manager__r.Department__c, currentNo);
                            }else{
                                mapDeptPitching.put(objOppDisplay.Manager__r.Department__c, 1);
                            }
                        }else if(objOppDisplay.StageName.equalsIgnoreCase('Closed') || objOppDisplay.StageName.equalsIgnoreCase('Closed Won') || objOppDisplay.StageName.equalsIgnoreCase(' Closed Lost')){
                            deptClosedNo+= 1;
                             if(mapDeptClosed.containsKey(objOppDisplay.Manager__r.Department__c)){
                                integer currentNo = mapDeptClosed.get(objOppDisplay.Manager__r.Department__c);
                                currentNo += 1;
                                mapDeptClosed.put(objOppDisplay.Manager__r.Department__c, currentNo);
                            }else{
                                mapDeptClosed.put(objOppDisplay.Manager__r.Department__c, 1);
                            }
                        }else if(objOppDisplay.StageName.equalsIgnoreCase('Instructed')){
                            deptInstructedNo+= 1;
                             if(mapDeptInstructed.containsKey(objOppDisplay.Manager__r.Department__c)){
                                integer currentNo = mapDeptInstructed.get(objOppDisplay.Manager__r.Department__c);
                                currentNo += 1;
                                mapDeptInstructed.put(objOppDisplay.Manager__r.Department__c, currentNo);
                            }else{
                                mapDeptInstructed.put(objOppDisplay.Manager__r.Department__c, 1);
                            }
                        }else if(objOppDisplay.StageName.equalsIgnoreCase('Job Lite')){
                            deptJobLiteNo+= 1;
                             if(mapDeptJobLite.containsKey(objOppDisplay.Manager__r.Department__c)){
                                integer currentNo = mapDeptJobLite.get(objOppDisplay.Manager__r.Department__c);
                                currentNo += 1;
                                mapDeptJobLite.put(objOppDisplay.Manager__r.Department__c, currentNo);
                            }else{
                                mapDeptJobLite.put(objOppDisplay.Manager__r.Department__c, 1);
                            }
                        }else{
                            deptOtherNo+= 1;
                             if(mapDeptOther.containsKey(objOppDisplay.Manager__r.Department__c)){
                                integer currentNo = mapDeptOther.get(objOppDisplay.Manager__r.Department__c);
                                currentNo += 1;
                                mapDeptOther.put(objOppDisplay.Manager__r.Department__c, currentNo);
                            }else{
                                mapDeptOther.put(objOppDisplay.Manager__r.Department__c, 1);
                            }
                            
                        }
                    
                    }
                    
                }else{
                    
                    if(mapDeptJObNo.containsKey('None')){
                        integer currentNone = mapDeptJObNo.get('None');
                        currentNone += 1;
                        mapDeptJObNo.put('None', currentNone);
                    }else{
                        mapDeptJObNo.put('None', 1);
                    }
                    
                    
                        if(!String.isBlank(objOppDisplay.StageName)){
                    
                        if(objOppDisplay.StageName.equalsIgnoreCase('Open')){
                            deptOpenNo+= 1;
                             if(mapDeptOpen.containsKey('None')){
                                integer currentNo = mapDeptOpen.get('None');
                                currentNo += 1;
                                mapDeptOpen.put('None', currentNo);
                            }else{
                                mapDeptOpen.put('None', 1);
                            }
                            
                        }else if(objOppDisplay.StageName.equalsIgnoreCase('Pitching')){
                            deptPitchingNo+= 1;
                             if(mapDeptPitching.containsKey('None')){
                                integer currentNo = mapDeptPitching.get('None');
                                currentNo += 1;
                                mapDeptPitching.put('None', currentNo);
                            }else{
                                mapDeptPitching.put('None', 1);
                            }
                        }else if(objOppDisplay.StageName.equalsIgnoreCase('Closed') || objOppDisplay.StageName.equalsIgnoreCase('Closed Won') || objOppDisplay.StageName.equalsIgnoreCase(' Closed Lost')){
                            deptClosedNo+= 1;
                             if(mapDeptClosed.containsKey('None')){
                                integer currentNo = mapDeptClosed.get('None');
                                currentNo += 1;
                                mapDeptClosed.put('None', currentNo);
                            }else{
                                mapDeptClosed.put('None', 1);
                            }
                        }else if(objOppDisplay.StageName.equalsIgnoreCase('Instructed')){
                            deptInstructedNo+= 1;
                             if(mapDeptInstructed.containsKey('None')){
                                integer currentNo = mapDeptInstructed.get('None');
                                currentNo += 1;
                                mapDeptInstructed.put('None', currentNo);
                            }else{
                                mapDeptInstructed.put('None', 1);
                            }
                        }else if(objOppDisplay.StageName.equalsIgnoreCase('Job Lite')){
                            deptJobLiteNo+= 1;
                             if(mapDeptJobLite.containsKey('None')){
                                integer currentNo = mapDeptJobLite.get('None');
                                currentNo += 1;
                                mapDeptJobLite.put('None', currentNo);
                            }else{
                                mapDeptJobLite.put('None', 1);
                            }
                        }else{
                            deptOtherNo+= 1;
                             if(mapDeptOther.containsKey('None')){
                                integer currentNo = mapDeptOther.get('None');
                                currentNo += 1;
                                mapDeptOther.put('None', currentNo);
                            }else{
                                mapDeptOther.put('None', 1);
                            }
                            
                        }
                    
                    }
                }
                
            }
            for(String departmentName: mapDeptJObNo.keyset()){
                if(!mapDeptOpen.containsKey(departmentName)){
                    mapDeptOpen.put(departmentName,0);
                }
                if(!mapDeptPitching.containsKey(departmentName)){
                    mapDeptPitching.put(departmentName,0);
                }
                if(!mapDeptClosed.containsKey(departmentName)){
                    mapDeptClosed.put(departmentName,0);
                }
                if(!mapDeptInstructed.containsKey(departmentName)){
                    mapDeptInstructed.put(departmentName,0);
                }
                if(!mapDeptJobLite.containsKey(departmentName)){
                    mapDeptJobLite.put(departmentName,0);
                }
                if(!mapDeptOther.containsKey(departmentName)){
                    mapDeptOther.put(departmentName,0);
                }
            }
            
            if(mapDeptJObNo.containsKey('None')){
                setDept.add('None');
            }
           
            
        }
        
    }
    
    
    
    
    
}