/**
*  Class Name: SharingRuleTriggerHandler
*  Description: Generic class for handling events related to insert/update/delete/undelete of sharing rules
*  By most part it is necessary to trigger batch execution which re-runs sharing rules on all existing records.
*
*  Tests: SharingRuleTriggerHandlerTest
*  
*  Company: CloudShift
*  CreatedDate: 28/06/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  ----------------------------------------------------------- 
*  Kaspars Rezgalis		28/06/2018					Created
*
*/
public class SharingRuleTriggerHandler{
	@testVisible private static final String ERROR_INVALID_INDEX_IN_CONDITION_TEXT = 'The following index is added to custom logic condition, but is not found in sharing conditions indexes: ';
	@testVisible private static final String ERROR_UNUSED_INDEX_IN_CONDITION_RECORDS = 'The following index is added to one of the sharing conditions but is not used in custom logic condition: ';

	public static Map<Id, List<String>> findInvalidCustomLogicRules(Map<Id, Sharing_Rule__c>rules){
		Map<Id, List<String>>retValues = new Map<Id, List<String>>(); //sharing rule Id -> list of error messages
		
		Map<Id, Set<Integer>>ruleIDsWithSharingConditionIndexes = new Map<Id, Set<Integer>>();
		for(Sharing_Condition__c condition : [SELECT Sharing_Rule__c, Index__c FROM Sharing_Condition__c 
													WHERE Sharing_Rule__c IN :rules.KeySet()
													AND Sharing_Rule__r.Change_Status__c=:SharingEngine.SHARING_RULE_CHANGE_STATUS_COMPLETED
													AND Sharing_Rule__r.Logic__c=:SharingEngine.SHARING_LOGIC_CUSTOM]){

			Set<Integer>tmpSet = (ruleIDsWithSharingConditionIndexes.containsKey(condition.Sharing_Rule__c) ? ruleIDsWithSharingConditionIndexes.get(condition.Sharing_Rule__c) : new Set<Integer>());
			tmpSet.add(Integer.ValueOf(condition.Index__c));
			ruleIDsWithSharingConditionIndexes.put(condition.Sharing_Rule__c, tmpSet);
		}

		for(Sharing_Rule__c ruleToCheck : rules.Values()){
			Set<Integer>usedConditionIndexesInCustomLogicText = new Set<Integer>();
			Set<Integer>availableConditionIndexesFromRecords = (ruleIDsWithSharingConditionIndexes.containsKey(ruleToCheck.Id) ? ruleIDsWithSharingConditionIndexes.get(ruleToCheck.Id) : new Set<Integer>());

			List<String>conditionTokens = SharingEngine.splitConditionTextIntoTokens(ruleToCheck.Custom_Logic__c);
			for(String s : conditionTokens){
				if(s.isNumeric()){
					usedConditionIndexesInCustomLogicText.add(Integer.ValueOf(s));
				}
			}

			//if condition (text field) has integer, but that index is not found in conditions records, add error
			for(Integer indexToCheck : usedConditionIndexesInCustomLogicText){
				if(!availableConditionIndexesFromRecords.contains(indexToCheck)){
					List<String>tmp = (retValues.containsKey(ruleToCheck.Id) ? retValues.get(ruleToCheck.Id) : new List<String>());
					tmp.add(ERROR_INVALID_INDEX_IN_CONDITION_TEXT + indexToCheck);
					retValues.put(ruleToCheck.Id, tmp);
				}
			}

			//if condition record index exists, but it has not been added to condition text field, add error
			for(Integer indexToCheck : availableConditionIndexesFromRecords){
				if(!usedConditionIndexesInCustomLogicText.contains(indexToCheck)){
					List<String>tmp = (retValues.containsKey(ruleToCheck.Id) ? retValues.get(ruleToCheck.Id) : new List<String>());
					tmp.add(ERROR_UNUSED_INDEX_IN_CONDITION_RECORDS + indexToCheck);
					retValues.put(ruleToCheck.Id, tmp);
				}
			}
		}
	
		return retValues;
	}
}