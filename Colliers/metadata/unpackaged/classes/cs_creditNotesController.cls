/* Class: cs_creditNotesController
 * Created By : Jyoti Hiremath/04-10-2016
 * Purpose : Credit Notes Tab Functionality
 */
 
public with sharing class cs_creditNotesController{

    public List<Invoice__c> InvoiceList{get;set;} 
    Public String pdfPageURLString{get;set;}
    public String invoicPdfId{get;set;}
    public String creditPDFId{get;set;}
    public Credit_Note__c creditNotesDetails{get;set;}
    public Credit_Note__c objCreditNoteCancel{get;set;}
    public boolean isCanceled{get;set;}
    public List<CreditDetailsWrapper> LstCreditDetails{get;set;}
    public CreditDetailsWrapper creditDetailsWrap{get;set;}
    public String oppId{get;set;}
    public Opportunity jb{get;set;}
    public integer detailWrpId{get;set;}
    public Boolean reissue{get;set;}
    public Boolean isactivecredit{get;set;}
    public String jobManager {get;set;}
    public List<Credit_Note__c> creditNotesLst {get;set;}
    public String invoiceId{get;set;}
     public ErrorHandling ObjModal{get;set;}
    //Error Messages String
    public String errorMsg_cancelReasons{get;set;}
    public String errorMsg_newAmt{get;set;}
    public String errorMsg_reasonAmt{get;set;}
    public string isIntr{get;set;}
    public string oppManager{get;set;}
    
    public boolean stagestatus{get;set;}
        
    public cs_creditNotesController() {
         initCreditNotes(); 
         ObjModal=new ErrorHandling();     
    }
    
    public void initCreditNotes(){
        oppId = ApexPages.currentPage().getParameters().get('id');
        fetchInvoiceList();
        isactivecredit = true;
        jobManager='';
        //fetchCreditNotesList();
    }

    //Method to get the List of Invoice associted with the Job   
    public void fetchInvoiceList(){
        ObjModal=new ErrorHandling();
        invoicPdfId ='';
        creditPDFId ='';
        isIntr = '';
        string jb_mngr = '';
        Integer cnt=1;
        jb = new Opportunity();
        isactivecredit = true;
        LstCreditDetails = new List<CreditDetailsWrapper>();
        InvoiceList = new List<Invoice__c>();
        creditDetailsWrap = new CreditDetailsWrapper();
        Credit_Note__c creditNotesDetails= new Credit_Note__c();
        objCreditNoteCancel =  new Credit_Note__c();
        isCanceled = false;
        
        
        if(!string.isBlank(oppId)){
        jb = [SELECT Id, AccountId,isClosed,Account.Name, Name,Relates_To__c,StageName,Amount,Manager__c,Manager__r.Name,Coding_Structure__r.Status__c,Job_Number__c from Opportunity where id=:oppId ];
        if(jb != null){
            oppManager = jb.Manager__c;
            if(jb.Coding_Structure__r.Status__c != null && String.isNotBlank(jb.Coding_Structure__r.Status__c)){
                if(jb.Coding_Structure__r.Status__c == 'Active'){
                    isactivecredit = true;
                }else{
                    isactivecredit = false;
                }
            }else{
                isactivecredit = false;
            }
        }

        
        // logic to add the count of credit notes
            Map<Id, Integer> CNcountMap= new Map<Id, Integer>();
            for (AggregateResult aggRes : [
                  SELECT COUNT(id)cn,Invoice__c,Invoice__r.Opportunity__c
                  FROM Credit_Note__c where Invoice__r.Opportunity__c=:oppId 
                  GROUP BY Invoice__c,Invoice__r.Opportunity__c
            ]) {
                Id userId = (Id) aggRes.get('Invoice__c');
                Integer numCN = (Integer) aggRes.get('cn');
                CNcountMap.put(userId, numCN);
            }
            
            system.debug('CNcountMap---->'+CNcountMap);
        //
        
        if(jb.StageName== 'Closed Won' || jb.StageName== 'Closed Lost' || jb.isClosed ){
            stagestatus = true;
                  //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'This Job is Closed.')); 
        }else{
            stagestatus = false;
        }
            InvoiceList = [select id,Opportunity__c, Total_Amount_Inc_Vat__c,Opportunity__r.Manager__r.name,Opportunity__r.Manager__c,Date_of_Invoice__c,VAT__c,Name,Total__c,Amount_Inc_VAT__c,Job_Title__c,Manager__r.name,Opportunity__r.name,Opportunity__r.Job_Number__c,Opportunity__r.Account.Name,Status__c,Total_cost__c,Total_Fee__c,Net_Fee__c,Net_Fees__c,Total_amount__c, is_International__c,Assigned_Invoice_Number__c,(select id,name,Allocation__r.Worktype__c from Invoice_Allocation_Junctions__r limit 1) from Invoice__c where Opportunity__c=:oppId AND (status__c='Approved' OR status__c = 'Paid' OR status__c = 'Partial' OR status__c = 'Cancelled' OR status__c = 'Printed') ORDER BY CreatedDate desc]; 
            integer counter=0;
            if(InvoiceList.size()>0){
                for(Invoice__c invObj :InvoiceList){
                    counter++;
                 
                     if(invObj.is_International__c ==true){
                         isIntr = 'International';
                     }else{
                         isIntr  ='';
                     }
                     if(invObj ==InvoiceList.get(0)){
                         invoiceId = invObj.id;
                         if(invObj.status__c.equalsIgnoreCase('Cancelled')){
                             isCanceled = true;
                         }
                         creditDetailsWrap =new CreditDetailsWrapper(counter,true,invObj,creditNotesDetails,isIntr,CNcountMap.get(invObj.id));
                         LstCreditDetails.add(new CreditDetailsWrapper(counter,true,invObj,creditNotesDetails,isIntr,CNcountMap.get(invObj.id))); 
                     }else{
                         LstCreditDetails.add(new CreditDetailsWrapper(counter,false,invObj,creditNotesDetails,isIntr,CNcountMap.get(invObj.id)));
                     } 
                }
            }else{
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No Approved Invoice Record exist for the Job.')); 
            }
            system.debug('invoiceId ----->'+invoiceId );
            if(!string.isBLANK(invoiceId)){
                creditNotesLst =[Select id,name,Credit_Note_Date__c,Invoice__r.Assigned_Invoice_Number__c,Invoice__r.Name,CN_Reason__c,VAT__c,Total__c,Status__c,Reason_For_Amount_Changed__c,Approved_By__c,Rejected_By__c,Approved_Date__c,Rejected_Reason__c,Invoice__c, Net_Amount__c,Gross_Amount__c from Credit_Note__c where Invoice__c=:invoiceId];
            }
        }
    }
    
    /* Name :Cancel
     * Return Type: PageReference
     * Function : Redirect to the ManageMyJob
     */
    public PageReference cancel(){
       PageReference OpporunityPageCancel = new PageReference('/'+oppId );
       OpporunityPageCancel.setRedirect(true);
       return OpporunityPageCancel;
       return null; 
    }   
    
    //method to pre populate the credit note detail fields
    public void fetchInvoiceDetails(){
        isCanceled = false;
        string inv_id;
        system.debug('LstCreditDetails----------------->'+LstCreditDetails);
        system.debug('invoiceId---->'+invoiceId);
        system.debug('detailWrpId---->'+detailWrpId);
        
        List<String> wrk_type = new List<String>();
        try{
            for(CreditDetailsWrapper obj :LstCreditDetails){
                if(obj.wrapperId== detailWrpId){
                    obj.isChecked =true;
                    //creditDetailsWrap = new CreditDetailsWrapper(obj.wrapperId,true,obj.invoice,obj.creditNotes,obj.isInternational,obj.CNcount);
                    creditDetailsWrap= obj;
                    inv_id = obj.invoice.id;
                    invoiceId =obj.invoice.id;
                    if(obj.invoice.status__c.equalsIgnoreCase('Cancelled')){
                        isCanceled = true;
                     }
                }
                else{
                     obj.isChecked= false;
                }
            }
            system.debug('inv_id------>'+inv_id);
            system.debug('oppManager--->'+oppManager);
            List<Invoice_Allocation_Junction__c> lstInvJunction = new List<Invoice_Allocation_Junction__c>();
            lstInvJunction = [SELECT id,name,Allocation__c,Allocation__r.workType_Allocation__c,Invoice__c,Invoice__r.Opportunity__r.Manager__c,Allocation__r.Assigned_To__c from Invoice_Allocation_Junction__c where Invoice__c =:invoiceId AND Allocation__r.Assigned_To__c=:oppManager];
            for(Invoice_Allocation_Junction__c ia : lstInvJunction){
        
                wrk_type.add(ia.Allocation__r.workType_Allocation__c);
                System.debug('Object_work_type'+ia.Allocation__r.workType_Allocation__c);
            }
            System.debug('Object_work_type'+wrk_type);
            jobManager = String.join(wrk_type, ',');
            system.debug('jobManager---->'+jobManager);
        }catch(Exception e){
            ApexPages.addmessages(e);     
        }
        fetchCreditNotesList();
        // isCanceled = false;
        // isCanceled = true;
    }
    
    // method to save the Credit note record   
    public void save(){
        ObjModal = new ErrorHandling();
        Credit_Note__c cd= new Credit_Note__c();
        List<String> wrk_type = new List<String>();
        if(isactivecredit){
            try{
                Boolean insrt = true;
                creditNotesLst = new List<Credit_Note__c>();
                creditNotesLst = [Select id,name,Credit_Note_Date__c,CN_Reason__c,Invoice__r.Assigned_Invoice_Number__c,Invoice__r.Name,VAT__c,Total__c,Status__c,Reason_For_Amount_Changed__c,Approved_By__c,Rejected_By__c,Approved_Date__c,Rejected_Reason__c,Invoice__c, Net_Amount__c,Gross_Amount__c from Credit_Note__c where Invoice__c=:invoiceId AND Status__c='Awaiting Approval'];
                system.debug('creditNotesLst ---->'+creditNotesLst );
                
                if(creditNotesLst.size()>0){
                    ObjModal = new ErrorHandling();
                    ObjModal.ErrorHandling(true,'Credit Note Record Already Exists for the Selected Invoice.','Error',true); 
                    // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Credit Note Record Already Exists for the Selected Invoice.'));
                }else{
                    if(InvoiceList.size()>0){
                        if(!string.isBlank(creditDetailsWrap.invoice.id)){
                            //cd.Invoice__c = creditDetailsWrap.invoice.id;
                            cd.Invoice__c = invoiceId;
                            cd.Is_Invoice_Reissued__c=reissue;
                            
                            
                           /* if(creditDetailsWrap.creditNotes.New_Invoice_Amount__c==NULL){
                                errorMsg_newAmt = 'New Amount is Required.';
                                insrt = false;
                            }else{
                                cd.New_Invoice_Amount__c=creditDetailsWrap.creditNotes.New_Invoice_Amount__c;
                                errorMsg_newAmt='';
                            }
                            if(!String.isBlank(creditDetailsWrap.creditNotes.Reason_For_Amount_Changed__c)){
                                cd.Reason_For_Amount_Changed__c=creditDetailsWrap.creditNotes.Reason_For_Amount_Changed__c;
                                errorMsg_cancelReasons='';
                            }else{
                                errorMsg_cancelReasons = 'Reason for Amount Cancellation is Required.';
                                insrt = false;
                            }*/
                            
                            if(creditDetailsWrap.invoice.Total_Amount_Inc_Vat__c != null){
                                cd.Total__c = creditDetailsWrap.invoice.Total_Amount_Inc_Vat__c;
                            }else{
                            
                            }
                            
                            if(creditDetailsWrap.invoice.VAT__c != null){
                                cd.VAT__c = creditDetailsWrap.invoice.VAT__c;
                            }else{
                            
                            }
                            
                            if(!String.isBlank(creditDetailsWrap.creditNotes.CN_Reason__c)){
                                cd.CN_Reason__c= creditDetailsWrap.creditNotes.CN_Reason__c;
                                errorMsg_cancelReasons='';
                            }else{
                               errorMsg_cancelReasons='Reason for Amount Cancellation is Required.';
                               ObjModal.ErrorHandling(true,'Reason for Amount Cancellation is Required.','Error',true); 
                               insrt = false;
                            }
                    
                            //cd.Credit_Note_Date__c=creditDetailsWrap.invoice.Date_of_Invoice__c;
                            cd.Credit_Note_Date__c = date.today();
                            cd.Status__c='Awaiting Approval';
                        }else{
                            ObjModal.ErrorHandling(true,'Please select the invoice Record','Error',true); 
                            // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'Please select the inVoice Record '));    
                        }
                        if(insrt == true){         
                            upsert cd;
                            ObjModal=new ErrorHandling(true,'Credit Note Request Created for Invoice : '+creditDetailsWrap.invoice.Assigned_Invoice_Number__c,'Success'); 
                            // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'Credit Note Request Created for Invoice : '+creditDetailsWrap.invoice.Assigned_Invoice_Number__c));
                            //fetchInvoiceDetails();
                            fetchCreditNotesList();
                            for(CreditDetailsWrapper objCreditDetailsWrapper :LstCreditDetails){
                                if(objCreditDetailsWrapper.invoice != null){
                                    if(objCreditDetailsWrapper.invoice.id == invoiceId){
                                        integer currentcount = 0;
                                        if(objCreditDetailsWrapper.CNcount != null){
                                            currentcount =objCreditDetailsWrapper.CNcount;
                                        }
                                        currentcount = currentcount+1;
                                        objCreditDetailsWrapper.CNcount = currentcount;
                                    }
                                }
                                
                            }
                            List<Invoice_Allocation_Junction__c> lstInvJunction = new List<Invoice_Allocation_Junction__c>();
                            lstInvJunction = [SELECT id,name,Allocation__c,Allocation__r.workType_Allocation__c,Invoice__c,Invoice__r.Opportunity__r.Manager__c,Allocation__r.Assigned_To__c from Invoice_Allocation_Junction__c where Invoice__c =:invoiceId AND Allocation__r.Assigned_To__c=:oppManager];
                            for(Invoice_Allocation_Junction__c ia : lstInvJunction){
                        
                                wrk_type.add(ia.Allocation__r.workType_Allocation__c);        
                            }
                            System.debug('Object_work_type'+wrk_type);
                            jobManager = String.join(wrk_type, ',');
                        }else{
                        }
                        
                    }else{
                        ObjModal.ErrorHandling(true,'No Invoice Record Exist.Cannot create a Credit Note.','Error',true); 
                        // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No Invoice Record Exist.Cannot create a Credit Note.'));
                    }
                }
            }catch(Exception e){
                 ObjModal=new ErrorHandling(true,e.getMessage(),'Error'); 
                //ApexPages.addmessages(e); 
            }
        }else{
            ObjModal.ErrorHandling(true,'Job Manager’s Coding Structure is Inactive – Please contact the System Administrator to change the job manager on the job.','Error',true);
        }
            
    }
    
     public void GenerateCreditNotePDf(){
         
             PageReference pagepdf = Page.CS_CreditNodePDFView;
                pagepdf.getParameters().put('invid',invoicPdfId);
                 pagepdf.getParameters().put('id', creditPDFId);
                pagepdf.setRedirect(true);
                pdfPageURLString = pagepdf.getUrl();
         
     }
     
      public void close_ErrorModal(){
        ObjModal=new ErrorHandling();
    }
    
    //method to fetch the creditnotes associated with the invoice
    public void fetchCreditNotesList(){
    
        system.debug('invoiceId---->'+invoiceId);
        isCanceled = false;
        try{
            if(!String.isBlank(invoiceId)){
                creditNotesLst = new List<Credit_Note__c>();
                creditNotesLst = [Select id,name,Credit_Note_Date__c,CN_Reason__c,Invoice__r.Assigned_Invoice_Number__c,Invoice__r.Name,VAT__c,Total__c,Status__c,Reason_For_Amount_Changed__c,Approved_By__c,Rejected_By__c,Approved_Date__c,Rejected_Reason__c,Invoice__c, Net_Amount__c,Gross_Amount__c  from Credit_Note__c where Invoice__c=:invoiceId];
                List<Invoice__c> lstInvoice = new List<Invoice__c>();
                lstInvoice = [Select id, Status__c From INvoice__c where id = :invoiceId limit 1];
                if(!lstInvoice.isEmpty()){
                    if(lstInvoice[0].Status__c.equalsIgnoreCase('Cancelled')){
                        isCanceled = true;
                    }
                    
                }
            }
        system.debug('creditNotesLst---->'+creditNotesLst);
           
        }catch(Exception e){
            ApexPages.addmessages(e);   
        }
        
        // isCanceled = true;
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('true','YES'));
        options.add(new SelectOption('false','NO'));
        return options;
    }
    
    public class CreditDetailsWrapper{
        public Boolean isChecked {get;set;}
        public Invoice__c invoice{get;set;}
        public integer wrapperId{get;set;}
        public Credit_Note__c creditNotes {get;set;}
        public string isInternational{get;set;}
        public Integer CNcount{get;set;}
        public CreditDetailsWrapper(integer wrap,Boolean ck,invoice__c inv, Credit_Note__c cd,string str,Integer cnt) {
        //public CreditDetailsWrapper(integer wrap,Boolean ck,invoice__c inv, Credit_Note__c cd,string str) {
            isChecked = ck;
            invoice =inv;    
            wrapperId=wrap;   
            creditNotes=cd;
            isInternational=str;  
            CNcount = cnt;
        }
        public CreditDetailsWrapper(){}
        
    }
    
      public class ErrorHandling{
        public boolean displayerror{get;set;}
        public string Errormsg{get;set;}
        public string Title{get;set;}
        Public List<string> ErrorList{get;set;}
        public boolean hasmore{get;set;}
        public ErrorHandling(boolean displayerror, string Errormsg,string Title){
            this.displayerror =displayerror;
            this.Errormsg=Errormsg;
            this.Title=Title;
            this.hasmore=false;
            ErrorList = new List<string>();
        }
        public  void ErrorHandling(boolean displayerror, string Errormsg,string Title,boolean hasmore){
            this.displayerror =displayerror;
            ErrorList.add(Errormsg);
            this.Title=Title;
            this.hasmore=hasmore;
        }
        public ErrorHandling(){
            this.displayerror =false;
            this.Errormsg='';
            ErrorList = new List<string>();
        }
    }
    
    
    
}