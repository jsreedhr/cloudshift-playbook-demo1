/**
*  Class Name: Test_CS_InvoiceTriggerHandler  
*  Description: This is a Test Class for CS_InvoiceTriggerHandler  
*  Company: dQuotient
*  CreatedDate:29/12/2016 
*
*/
@isTest
private class Test_CS_InvoiceTriggerHandler
{
    static Account accountObj;
    static Account accountObj1;
    static Opportunity oppObj;
    static Opportunity oppObj1;
    static Contact contactObj;
    static Invoice__c invoiceObj;
    static User userObj;
    static Staff__c staffObj;
    static Allocation__c allocationObj;
    static Property__c propertyObj;
    static Purchase_Order__c purOrderObj;
    static Job_Property_Junction__c jobPropObj;
    static Forecasting__c forecastObj;
    static Disbursements__c disbursementObj;

    static void createData()
    {
        accountObj = TestObjectHelper.createAccount();
        accountObj.PO_Required__c = 'Yes';
        accountObj.Company_Status__c = 'Active';
        
        insert accountobj;
        System.assert(accountObj.id!=NULL);
        
         accountObj1 = TestObjectHelper.createAccount();
        //accountObj.PO_Required__c = 'Yes';
        accountObj1.Company_Status__c = 'Active';
        insert accountobj1;
        // create custom setting department
        Department__c debtobj = new Department__c();
        debtobj.Cost_Centre__c = '620';
        debtobj.Department__c = 'Accurates';
        debtobj.Name = 'Test';
        insert debtobj;
        // create Invoice_PDF_Value__c record
        Invoice_PDF_Value__c invpdf = new Invoice_PDF_Value__c();
        invpdf.Dept_Cost_Centre__c = '620';
        invpdf.Currency__c = 'GBP';
        insert invpdf;
        
         AssignAccountId__c objAssignAccount = new AssignAccountId__c();
        objAssignAccount.Name = 'test';
        objAssignAccount.AccountNumber__c = 123;
        objAssignAccount.Account_Pattern__c = '123';
        insert objAssignAccount;
        propertyObj = TestObjecthelper.createProperty();
        propertyObj=TestObjectHelper.createProperty();
        propertyObj.Street_No__c ='Park Lane';
        propertyObj.Post_Code__c ='W1K 3DD';
        propertyObj.Country__c='United Kingdom';
        propertyObj.Geolocation__Latitude__s=51.51;
        propertyObj.Geolocation__Longitude__s=-0.15;
        insert propertyObj;
        System.assert(propertyObj.id!=NULL);
        
        Address__c addressObj = TestObjectHelper.createAddress(accountObj);
        insert addressObj;
         // create worktype 
        Work_Type__c workObj = new Work_Type__c();
        workObj.Name = 'Valuation - Redbook';
        workObj.Active__c = true;
        insert workObj;
        // create user
        userObj = TestObjectHelper.createAdminUser(true);
        userobj.UserName ='Test_123@dquotient.com';
        insert userObj;
        //create staff
        staffObj = TestObjecthelper.createStaff();
        staffObj.User__c = userObj.id;
        staffObj.Active__c = true;
        insert staffObj;
        //create coding structure
        Coding_Structure__c codobj = new Coding_Structure__c();
        codobj.Staff__c = staffObj.Id;
        codobj.Work_Type__c = workObj.Id;
        codobj.Department__c = 'Accurates';
        codobj.Office__c = 'London - West End';
        codobj.Status__c = 'Active';
        codobj.Department_Cost_Code_Centre__c = '620';
        insert codobj;
        //create Opportunity
        oppObj = TestObjectHelper.createOpportunity(accountObj);
        oppObj.Invoicing_Company2__c = accountObj.id;
        oppObj.Invoicing_Address__c = addressObj.id;
        oppObj.Coding_Structure__c = codobj.Id;
        insert oppObj;
         oppObj1 = TestObjectHelper.createOpportunity(accountObj1);
        insert oppObj1;
        System.assert(oppObj.id!=NULL);
        
        Account_Address_Junction__c accAddresjn = TestObjecthelper.createAccountAddressJunction(accountObj,addressObj);
        insert accAddresjn;
        
        jobPropObj = TestObjecthelper.JobPrptyJn(oppObj,propertyObj);
        insert jobPropObj;
        System.assert(jobPropObj.id!=NULL);
        contactObj = TestObjectHelper.createContact(accountObj);
        contactobj.lastName='Test';
        contactobj.Email = 'test@test.com';
        insert contactobj;
        System.assert(contactobj.id!=NULL);
        
        System.assert(userObj.id!=NULL);
        System.assert(staffObj.id!=NULL);
        allocationObj = TestObjectHelper.createAllocation(oppObj,staffObj);
        insert allocationObj;
        System.assert(allocationObj.id!=NULL);
        purOrderObj = TestObjecthelper.createPurchaseOrder(oppObj);
        purOrderObj.PO_Status__c = 'Original';
        purOrderObj.Amount__c =1000;
        purOrderObj.Used_Amount__c=500;
        insert purOrderObj;
        System.assert(purOrderObj.id!=NULL);
        forecastObj = TestObjecthelper.createForecast(allocationObj);
        forecastObj.CS_Forecast_Date__c = system.today().toStartOfMonth();
        insert forecastObj;
        System.assert(forecastObj.id!=NULL);
        disbursementObj = TestObjecthelper.createDisbursement(oppObj);
        disbursementObj.Purchase_Cost__c = 1000;
        disbursementObj.Recharge_Cost__c = 1000;
        disbursementObj.Category__c ='Motor Car';
        disbursementObj.Created_For__c = staffObj.id;
        disbursementObj.Sub_Category__c='Damage';
        disbursementObj.Purchase_Date__c  = System.today();
        disbursementObj.Description__c = 'Tsts';
        insert disbursementObj;
        System.assert(disbursementObj.id!=NULL);

    }
    
    @isTest 
    static void testMethod1()
    {
        createData();
        Invoice__c invoiceObj1 = TestObjecthelper.createInvoice(oppObj,contactobj);
        invoiceObj1.Status__c='Draft';
        invoiceObj1.Final_Invoice__c=true;
        insert invoiceObj1;
        System.assert(invoiceObj1.id!=NULL);

        checkRecursiveInvoice.run = true;
        invoiceObj1.Status__c='Draft';
        update invoiceObj1;
    }

    @isTest 
    static void testMethod2()
    {
        createData();
        Group groupObj = new Group();
        groupObj.name = 'AML Team';
        insert groupObj;
        System.assert(groupObj.id!=NULL);
    
        Invoice__c invoiceObj1 = TestObjecthelper.createInvoice(oppObj,contactobj);
        invoiceObj1.toSendeMLRMail__c=false;
        checkRecursiveInvoice.run = true;
        insert invoiceObj1;
        System.assert(invoiceObj1.id!=NULL);

        invoiceObj1.toSendeMLRMail__c=true;
        checkRecursiveInvoice.run = true;
        update invoiceObj1;
    }

    @isTest
    static void testMethod3()
    {
        createData();
        oppObj.Manager__c=staffObj.id;
        oppObj.Job_Manager_Email__c='JamesBond@MI6.com';
       // oppObj.Account = accountObj.id;
        update oppObj;

        Staff__c assistantObj = TestObjecthelper.createStaff();
        assistantObj.Email__c = 'JasonBourne@Treadstone.com';
        insert assistantObj;
        System.assert(assistantObj.id!=NULL);

        staffObj.ASSISTANT__c=assistantObj.id;
        update staffObj;
        
        
        Test.startTest();

        Invoice__c invoiceObj1 = TestObjecthelper.createInvoice(oppObj,contactobj);
        invoiceObj1.Status__c = 'Paid';
        invoiceObj1.Invoice_Currency__c = 'GBP';
        //invoiceObj1.Total_Fee__c = 15;
        checkRecursiveInvoice.run = true;
        insert invoiceObj1;
        
        Invoice__c invoiceObj3 = TestObjecthelper.createInvoice(oppObj,contactobj);
        invoiceObj3.Status__c = 'Paid';
        invoiceObj3.Invoice_Currency__c = 'GBP';
        //invoiceObj1.Total_Fee__c = 15;
        checkRecursiveInvoice.run = true;
        insert invoiceObj3;
        
        Invoice__c invoiceObj2 = TestObjecthelper.createInvoice(oppObj1,contactobj);
        invoiceObj2.Status__c = 'paid';
        //invoiceObj1.Total_Fee__c = 15;
        checkRecursiveInvoice.run = true;
        insert invoiceObj2;

        Invoice_Line_Item__c invLineItemObj = TestObjecthelper.createInvoiceLineItem(invoiceObj1);
        invLineItemObj.Amount_ex_VAT__c=100;
        invLineItemObj.VAT_Amount__c=10;
        insert invLineItemObj;
        System.assert(invLineItemObj.id!=NULL);
        
         Invoice_Line_Item__c invLineItemObj1 = TestObjecthelper.createInvoiceLineItem(invoiceObj1);
        invLineItemObj1.International_Amount__c=100;
        invLineItemObj1.Type__c='fee';
        insert invLineItemObj1;
        
        
         Invoice_Line_Item__c invLineItemObj2 = TestObjecthelper.createInvoiceLineItem(invoiceObj2);
        invLineItemObj1.International_Amount__c=100;
        invLineItemObj1.Type__c='fee';
        insert invLineItemObj2;
        
        Invoice_Line_Item__c invLineItemObj3 = TestObjecthelper.createInvoiceLineItem(invoiceObj3);
        invLineItemObj3.International_Amount__c=100;
        invLineItemObj3.Type__c='fee';
        insert invLineItemObj3;

        invoiceObj1.Status__c = 'Printed';
        checkRecursiveInvoice.run = true;
        update invoiceObj1;
        
        invoiceObj3.Status__c = 'Printed';
        checkRecursiveInvoice.run = true;
        update invoiceObj3;

        Invoice_Payment__c invPayObj = new Invoice_Payment__c();
        invPayObj.Invoice__c = invoiceObj1.id;
        insert invPayObj;
        System.assert(invPayObj.id!=NULL);

        Invoice_Allocation_Junction__c invAllocJunObj = TestObjecthelper.createInvoiceAllocationJunction(allocationObj,invoiceObj1);
       
        invAllocJunObj.Forecasting__c = forecastObj.id;
         insert invAllocJunObj;
        checkRecursiveInvoiceForecasting.run = true;
        invAllocJunObj.Amount__c = 100;
        update invAllocJunObj;
        
        System.assert(invAllocJunObj.id!=NULL);

        Purchase_Order_Invoice_Junction__c purOrderInvJunObj = TestObjecthelper.createPurchaseOrderInvoiceJunction(purOrderObj,invoiceObj1);
        purOrderInvJunObj.Amount_Paid__c = 100;
        checkRecursivePurchaseOrderInvoice.run = true;
        insert purOrderInvJunObj;
        System.assert(purOrderInvJunObj.id!=NULL);


        Purchase_Order_Invoice_Junction__c purOrderInvJunObj1 = TestObjecthelper.createPurchaseOrderInvoiceJunction(purOrderObj,invoiceObj2);
        purOrderInvJunObj1.Amount_Paid__c = 100;
        checkRecursivePurchaseOrderInvoice.run = true;
        insert purOrderInvJunObj1;
        System.assert(purOrderInvJunObj1.id!=NULL);

        Invoice_Cost_Junction__c invCostJunObj = TestObjecthelper.createInvoiceCostJunction(disbursementObj,invoiceObj1);
        insert invCostJunObj;
        
        Invoice_Cost_Junction__c invCostJunObj1 = [select disbursement__r.Purchase_Cost__c from Invoice_Cost_Junction__c limit 1];
        
        invCostJunObj1.disbursement__r.Purchase_Cost__c = 100;
        checkRecursiveInvoiceCost.run = true;
        update invCostJunObj1;
        System.assert(invCostJunObj.id!=NULL);

        
        
             purOrderObj.Amount__c =1000;
            purOrderObj.Used_Amount__c=500;
            update purOrderObj;

            //checkRecursiveInvoice.run=true;
            invoiceObj1.Status__c = 'Approved';
            invoiceObj1.Invoice_Currency__c = 'GBP';
            update invoiceObj1;
            
            

        purOrderObj.Amount__c =10000;
            purOrderObj.Used_Amount__c=500;
            update purOrderObj;
            
            //checkRecursiveInvoice.run=true;
            invoiceObj2.Status__c = 'Approved';
            update invoiceObj2;
           

            checkRecursiveInvoice.run=true;
            invoiceObj1.Status__c = 'Cancelled';
            update invoiceObj1;

        Test.stopTest();
    }
     @isTest 
    static void testMethod4()
    {
         createData();
        
         Invoice__c invoiceObj1 = TestObjecthelper.createInvoice(oppObj,contactobj);
        invoiceObj1.Status__c = 'Draft';
        invoiceObj1.Invoice_Currency__c = 'GBP';
        checkRecursiveInvoice.run = false;
        insert invoiceObj1;
        
        
         Invoice__c invoiceObj2 = TestObjecthelper.createInvoice(oppObj,contactobj);
        invoiceObj2.Status__c = 'Draft';
        checkRecursiveInvoice.run = false;
        insert invoiceObj2;
        
         Invoice__c invoiceObj3 = TestObjecthelper.createInvoice(oppObj,contactobj);
        invoiceObj3.Status__c = 'Draft';
        checkRecursiveInvoice.run = false;
        insert invoiceObj3;
        
         Invoice__c invoiceObj4 = TestObjecthelper.createInvoice(oppObj,contactobj);
        invoiceObj4.Status__c = 'Draft';
        checkRecursiveInvoice.run = false;
        insert invoiceObj4;
        
        Invoice__c invoiceObj5 = TestObjecthelper.createInvoice(oppObj,contactobj);
        invoiceObj5.Status__c = 'Draft';
        checkRecursiveInvoice.run = false;
        insert invoiceObj5;

       Disbursements__c disbursementObj1 = TestObjecthelper.createDisbursement(oppObj);
        disbursementObj1.Purchase_Cost__c = 1000;
        disbursementObj1.Recharge_Cost__c = 1000;
        disbursementObj1.Category__c ='Motor Car';
        disbursementObj1.Created_For__c = staffObj.id;
        disbursementObj1.Sub_Category__c='Damage';
        disbursementObj1.Purchase_Date__c  = System.today();
        disbursementObj1.Description__c = 'Tsts';
        insert disbursementObj1;
        
        Disbursements__c disbursementObj2 = TestObjecthelper.createDisbursement(oppObj);
        disbursementObj2.Purchase_Cost__c = 1000;
        disbursementObj2.Recharge_Cost__c = 1000;
        disbursementObj2.Category__c ='Motor Car';
        disbursementObj2.Created_For__c = staffObj.id;
        disbursementObj2.Sub_Category__c='Damage';
        disbursementObj2.Purchase_Date__c  = System.today();
        disbursementObj2.Description__c = 'Tsts';
        insert disbursementObj2;
        
        Disbursements__c disbursementObj3 = TestObjecthelper.createDisbursement(oppObj);
        disbursementObj3.Purchase_Cost__c = 1000;
        disbursementObj3.Recharge_Cost__c = 1000;
        disbursementObj3.Category__c ='Motor Car';
        disbursementObj3.Created_For__c = staffObj.id;
        disbursementObj3.Sub_Category__c='Damage';
        disbursementObj3.Purchase_Date__c  = System.today();
        disbursementObj3.Description__c = 'Tsts';
        insert disbursementObj3;
        
       
        
        Invoice_Cost_Junction__c invCostJunObj = TestObjecthelper.createInvoiceCostJunction(disbursementObj,invoiceObj1);
        checkRecursiveInvoice.run = false;
        insert invCostJunObj;
        
        Invoice_Cost_Junction__c invCostJunObj1 = TestObjecthelper.createInvoiceCostJunction(disbursementObj,invoiceObj2);
        checkRecursiveInvoice.run = false;
        insert invCostJunObj1;
        
        
        Invoice_Cost_Junction__c invCostJunObj2 = TestObjecthelper.createInvoiceCostJunction(disbursementObj1,invoiceObj2);
        checkRecursiveInvoice.run = false;
        insert invCostJunObj2;
        
        Invoice_Cost_Junction__c invCostJunObj3 = TestObjecthelper.createInvoiceCostJunction(disbursementObj2,invoiceObj2);
        checkRecursiveInvoice.run = false;
        insert invCostJunObj3;
        
        Invoice_Cost_Junction__c invCostJunObj4 = TestObjecthelper.createInvoiceCostJunction(disbursementObj1,invoiceObj3);
        checkRecursiveInvoice.run = false;
        insert invCostJunObj4;
        
         Invoice_Cost_Junction__c invCostJunObj5 = TestObjecthelper.createInvoiceCostJunction(disbursementObj2,invoiceObj4);
         checkRecursiveInvoice.run = false;
        insert invCostJunObj5;
        
        Invoice_Cost_Junction__c invCostJunObj6 = TestObjecthelper.createInvoiceCostJunction(disbursementObj,invoiceObj5);
        checkRecursiveInvoice.run = false;
        insert invCostJunObj6;
        
        Invoice_Cost_Junction__c invCostJunObj7 = TestObjecthelper.createInvoiceCostJunction(disbursementObj3,invoiceObj3);
        checkRecursiveInvoice.run = false;
        insert invCostJunObj7;
        
        Invoice_Cost_Junction__c invCostJunObj8 = TestObjecthelper.createInvoiceCostJunction(disbursementObj3,invoiceObj4);
        checkRecursiveInvoice.run = false;
        insert invCostJunObj8;
        
        Invoice_Cost_Junction__c invCostJunObj9 = TestObjecthelper.createInvoiceCostJunction(disbursementObj3,invoiceObj5);
        checkRecursiveInvoice.run = false;
        insert invCostJunObj9;
       
       

        Invoice_Allocation_Junction__c invAllocJunObj = TestObjecthelper.createInvoiceAllocationJunction(allocationObj,invoiceObj1);
        invAllocJunObj.Amount__c = 100;
         invAllocJunObj.Forecasting__c = forecastObj.id;
        checkRecursiveInvoice.run = false;
        checkRecursiveForecast.run = false;
        insert invAllocJunObj;
        
         Invoice_Allocation_Junction__c invAllocJunObj1 = TestObjecthelper.createInvoiceAllocationJunction(allocationObj,invoiceObj2);
        invAllocJunObj1.Amount__c = 100;
         invAllocJunObj1.Forecasting__c = forecastObj.id;
        checkRecursiveInvoice.run = false;
        checkRecursiveForecast.run = false;
        insert invAllocJunObj1;
        
         Invoice_Line_Item__c invLineItemObj = TestObjecthelper.createInvoiceLineItem(invoiceObj1);
        invLineItemObj.Amount_ex_VAT__c=1000;
        invLineItemObj.International_Amount__c = 1000;
        invLineItemObj.Type__c = 'Cost';
        invLineItemObj.Cost_Type__c = 'Breakdown';
        invLineItemObj.VAT_Amount__c= 10;
        checkRecursiveInvoice.run = false;
        insert invLineItemObj;
        System.assert(invLineItemObj.id!=NULL);
         Invoice_Line_Item__c invLineItemObj3 = TestObjecthelper.createInvoiceLineItem(invoiceObj2);
        invLineItemObj3.Amount_ex_VAT__c=4000;
        invLineItemObj3.Type__c = 'Cost';
        invLineItemObj3.International_Amount__c = 4000;
        invLineItemObj3.Type__c = 'Cost';
        invLineItemObj3.Cost_Type__c = 'Category';
        invLineItemObj3.VAT_Amount__c= 10;
        checkRecursiveInvoice.run = false;
        insert invLineItemObj3;
        
        
         Invoice_Line_Item__c invLineItemObj1 = TestObjecthelper.createInvoiceLineItem(invoiceObj3);
        invLineItemObj1.Amount_ex_VAT__c=2000;
        invLineItemObj1.International_Amount__c = 2000;
        invLineItemObj1.Type__c = 'Cost';
        invLineItemObj1.Cost_Type__c = 'Category';
        invLineItemObj1.VAT_Amount__c= 10;
        checkRecursiveInvoice.run = false;
        insert invLineItemObj1;
       
        Invoice_Line_Item__c invLineItemObj2 = TestObjecthelper.createInvoiceLineItem(invoiceObj4);
        invLineItemObj2.Amount_ex_VAT__c=2000;
        invLineItemObj2.International_Amount__c = 2000;
        invLineItemObj2.Type__c = 'Cost';
        invLineItemObj2.Cost_Type__c = 'Summary';
        invLineItemObj2.VAT_Amount__c= 10;
        checkRecursiveInvoice.run = false;
        insert invLineItemObj2;
        
         Invoice_Line_Item__c invLineItemObj4 = TestObjecthelper.createInvoiceLineItem(invoiceObj5);
        invLineItemObj4.Amount_ex_VAT__c=1000;
        invLineItemObj4.Type__c = 'Cost';
        invLineItemObj4.International_Amount__c = 1000;
        invLineItemObj4.Cost_Type__c = 'Breakdown';
        invLineItemObj4.VAT_Amount__c= 10;
        checkRecursiveInvoice.run = false;
        insert invLineItemObj4;
       
        
        Accrual__c accrualobj1 = TestObjecthelper.createAccural(forecastObj);
        insert accrualobj1;
        Accrual_Forecasting_Junction__c accrualforecastingobj1 = TestObjecthelper.createAccrualForecastJunction(accrualobj1,forecastObj);
        checkRecursiveAccural.run = false;
        insert accrualforecastingobj1;
       
        accrualforecastingobj1.amount__c = 100; 
        checkRecursiveAccuralForecasting.run = false;
        checkRecursiveForecast.run = false;
        update accrualforecastingobj1;
           checkRecursiveAccuralForecasting.run = false;
        checkRecursiveForecast.run = false;
        
        Test.startTest();
        
         accrualobj1.Status__c = 'Approved';
        checkRecursiveAccural.run = true;
        checkRecursiveForecast.run = true;
        update accrualobj1;
        
        
        
         invoiceObj1.Status__c = 'Approved';
        checkRecursiveInvoice.run = true;
        checkRecursiveForecast.run = true;
        update invoiceObj1;
        
        
         invoiceObj2.Status__c = 'Approved';
        checkRecursiveInvoice.run = true;
        checkRecursiveForecast.run = true;
        update invoiceObj2;
        
        
        
        Test.stopTest();
       
    }
}