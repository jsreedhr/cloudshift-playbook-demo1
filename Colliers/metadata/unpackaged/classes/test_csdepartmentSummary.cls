/**
 *  Class Name: test_csdepartmentForecastClass 
 *  Description: Test class for the controller of journalPage
 *  Company: dQuotient
 *  CreatedDate: 13/10/2016 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  ----------------------------------------------------------- 
 *  Nidheesh               17/10/2016                  Orginal Version          
 *
 */
@isTest
private class test_csdepartmentSummary {

static account a;
 //@testsetup
    static void setupData(){
        //create account
         a = TestObjectHelper.createAccount();
        insert a;
        
        Schema.DescribeFieldResult fieldResult = Coding_Structure__c.Department__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        Schema.DescribeFieldResult fieldResulthead = Staff__c.headOfDepartment__c.getDescribe();
        List<Schema.PicklistEntry> plehead = fieldResult.getPicklistValues();
        Schema.DescribeFieldResult fieldResultofffice = Coding_Structure__c.Office__c.getDescribe();
        List<Schema.PicklistEntry> pleoffice = fieldResultofffice.getPicklistValues();
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
       
        System.debug('opps---->'+opps);
        //create realted Opportunity
        List <Contact> cons = TestObjectHelper.createMultipleContact(a, 10);
        integer loopNo = 1;
        for(Contact objContact: cons){
            objContact.Email = 'test'+loopNo+'@test.com';
        }
        insert cons;
        System.debug('cons---->'+cons);
        Staff__c staffObj =  new Staff__c();
        staffObj.HR_No__c = String.valueOf(Math.round(Math.random()*1000));
        staffObj.Email__c = 'stvcxa145236423ff@gmail.com';
        staffObj.Name = 'staffdummy';
        if(!ple.isEmpty()){
            staffObj.headOfDepartment__c = 'Accurates';
            staffObj.Department__c = 'Accurates';
            
        }
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
   String uniqueName = 'orgId' + 'dateString' + 'randomInt';
        staffObj.Email__c =uniqueName+randomInt+ 'staff@gmail.com';
        staffobj.active__c = true;
        insert staffObj;
        
         opps[0].stagename = 'open';
          opps[0].manager__c = staffObj.id;
                opps[1].stagename = 'open';
                 opps[1].manager__c = staffObj.id;
        opps[2].stagename = 'Closed';
         opps[2].manager__c = staffObj.id;
        opps[3].stagename = 'Instructed';
         opps[3].manager__c = staffObj.id;
        opps[4].stagename = 'Job Lite';
         opps[4].manager__c = staffObj.id;
          opps[5].stagename = 'Closed';
                 opps[5].manager__c = staffObj.id;
                  opps[6].stagename = 'Instructed';
                 opps[6].manager__c = staffObj.id;
                  opps[7].stagename = 'Job Lite';
                 opps[7].manager__c = staffObj.id;
                  opps[7].stagename = 'Pitching';
                 opps[7].manager__c = staffObj.id;
                 
        update opps;
               
        
        
    }
    static testMethod void testCS_allocationControllerForOpps(){
        System.debug('opp test---->');
        setupData();
        Schema.DescribeFieldResult fieldResult = Coding_Structure__c.Department__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        Schema.DescribeFieldResult fieldResultofffice = Coding_Structure__c.Office__c.getDescribe();
        List<Schema.PicklistEntry> pleoffice = fieldResultofffice.getPicklistValues();
        opportunity opp = [Select id from opportunity Limit 1];
        Staff__c staffobject = [Select id from Staff__c Limit 1];
        Department_Forecast__c  newobj = new Department_Forecast__c ();
        newobj.Month_1_Name__c = system.today();
        if(!ple.isEmpty()){
            newobj.Department__c = ple[0].getValue();
        }
        if(!pleoffice.isEmpty()){
            newobj.officename__c =  pleoffice[0].getValue();
        }
        newobj.Adjusted_Month_1_Amount__c = 10;
        newobj.Adjusted_Month_2_Amount__c = 10;
        newobj.Adjusted_Month_3_Amount__c =10;
        insert newobj;
        
         Staff__c staffObj1 =  new Staff__c();
        staffObj1.Name = 'stafffsvdummy';
        staffObj1.HR_No__c = String.valueOf(Math.round(Math.random()*1000));
       
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
   String uniqueName = 'orgId' + 'dateString' + 'randomInt';
        staffObj1.Email__c =uniqueName+randomInt+ 'staff@gmail.com';
        staffobj1.active__c = true;
        insert staffObj1;
        
        
         List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
         opps[0].stagename = 'open';
          opps[0].manager__c = staffObj1.id;
                opps[1].stagename = 'open';
                 opps[1].manager__c = staffObj1.id;
        opps[2].stagename = 'Closed';
         opps[2].manager__c = staffObj1.id;
        opps[3].stagename = 'Instructed';
         opps[3].manager__c = staffObj1.id;
        opps[4].stagename = 'Job Lite';
         opps[4].manager__c = staffObj1.id;
          opps[5].stagename = 'Closed';
                 opps[5].manager__c = staffObj1.id;
                  opps[6].stagename = 'Instructed';
                 opps[6].manager__c = staffObj1.id;
                  opps[7].stagename = 'Job Lite';
                 opps[7].manager__c = staffObj1.id;
                  opps[7].stagename = 'Pitching';
                 opps[7].manager__c = staffObj1.id;
                 
        update opps;
        User objuser = TestObjectHelper.createAdminUser(true);
        objuser.username = 'test@gmat.com';
        insert objuser;
        staffobject.user__c = objuser.Id;
        staffobject.active__C = true;
        upsert staffobject;
        system.runAs(objuser){
        Test.startTest();
           
            CS_DepartmentSummary con = new CS_DepartmentSummary(new ApexPages.StandardController(a));
            
            Test.stopTest();
        }
    }
    
    
    }