public class VALS_ReportValueHandler2{


//for updating Internal Inspection Rep Values
      public static void UpdateInternalBCRRepValue(List<Internal_Inspection__c> IntInspecLst){
        
        for(Internal_Inspection__c Ex:IntInspecLst ){
  //ancialliry staircases
           if(EX.Wholly_MainlyAncillaryStaircases__c!=null && EX.Ancillary_Staircases_Partly__c==null)
          {
            EX.Ancillary_Staircases_RepValue__c='Ancillary staircases are '+ EX.Wholly_MainlyAncillaryStaircases__c;
           }
            else if(EX.Ancillary_Staircases_Partly__c!=null && EX.Wholly_MainlyAncillaryStaircases__c==null){
              String PartVal=EX.Ancillary_Staircases_Partly__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Ancillary_Staircases_RepValue__c='Ancillary staircases are partly '+splitstr;
            }
             else if(EX.Wholly_MainlyAncillaryStaircases__c!=null && EX.Ancillary_Staircases_Partly__c!=null){
              String MainVal=EX.Wholly_MainlyAncillaryStaircases__c;
              String PartVal=EX.Ancillary_Staircases_Partly__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Ancillary staircases are mainly '+MainVal+', partly '+splitstr;
              EX.Ancillary_Staircases_RepValue__c= fullstr;
             }
             else if(EX.Wholly_MainlyAncillaryStaircases__c==null && EX.Ancillary_Staircases_Partly__c==null){
              EX.Ancillary_Staircases_RepValue__c=null;
             }

//Construction
        if(EX.Wholly_MainlyConstruction__c!=null && EX.PartlyConstruction__c==null)
          {
            EX.Construction_RepValue__c='Floor construction is '+ EX.Wholly_MainlyConstruction__c+' throughout';
           }
            else if(EX.PartlyConstruction__c!=null && EX.Wholly_MainlyConstruction__c==null){
              String PartVal=EX.PartlyConstruction__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Construction_RepValue__c='Floor construction is partly '+splitstr;
            }
             else if(EX.Wholly_MainlyConstruction__c!=null && EX.PartlyConstruction__c!=null){
              String MainVal=EX.Wholly_MainlyConstruction__c;
              String PartVal=EX.PartlyConstruction__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Floor construction is mainly '+MainVal+', partly '+splitstr;
              EX.Construction_RepValue__c= fullstr;
             }
             else if(EX.Wholly_MainlyConstruction__c==null && EX.PartlyConstruction__c==null){
              EX.Construction_RepValue__c=null;
             }
 //covered

      if(EX.Wholly_MainlyCovering__c!=null && EX.PartlyCovering__c==null)
        {
        EX.Covering_RepValue__c='Floors are '+ EX.Wholly_MainlyCovering__c+' throughout';
        }
            else if(EX.PartlyCovering__c!=null && EX.Wholly_MainlyCovering__c==null){
              String PartVal=EX.PartlyCovering__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Covering_RepValue__c='Floors are partly '+splitstr;
            }
             else if(EX.Wholly_MainlyCovering__c!=null && EX.PartlyCovering__c!=null){
              String MainVal=EX.Wholly_MainlyCovering__c;
              String PartVal=EX.PartlyCovering__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Floors are mainly '+MainVal+', partly '+splitstr;
              EX.Covering_RepValue__c= fullstr;
             }
             else if(EX.Wholly_MainlyCovering__c==null && EX.PartlyCovering__c==null){
              EX.Covering_RepValue__c=null;
             }
//Heating

      if(EX.Wholly_MainlyHeating__c!=null && EX.PartlyHeating__c==null)
        {
        EX.Heating_RepValue__c='Heating is '+ EX.Wholly_MainlyHeating__c+' throughout';
        }
            else if(EX.PartlyHeating__c!=null && EX.Wholly_MainlyHeating__c==null){
              String PartVal=EX.PartlyHeating__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Heating_RepValue__c='Heating is partly '+splitstr;
            }
             else if(EX.Wholly_MainlyHeating__c!=null && EX.PartlyHeating__c!=null){
              String MainVal=EX.Wholly_MainlyHeating__c;
              String PartVal=EX.PartlyHeating__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Heating is mainly '+MainVal+', partly '+splitstr;
              EX.Heating_RepValue__c= fullstr;
             }
             else if(EX.Wholly_MainlyHeating__c==null && EX.PartlyHeating__c==null){
              EX.Heating_RepValue__c=null;
             }

//Light Fixing

      if(EX.Light_Fixing_Wholly_Mainly__c!=null && EX.LightPartlyFixing__c==null)
        {
        EX.Light_Fixing_RepValue__c ='Light fixings are '+ EX.Light_Fixing_Wholly_Mainly__c+' throughout';
        }
            else if(EX.LightPartlyFixing__c!=null && EX.Light_Fixing_Wholly_Mainly__c==null){
              String PartVal=EX.LightPartlyFixing__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Light_Fixing_RepValue__c   ='Light fixings are partly '+splitstr;
            }
             else if(EX.Light_Fixing_Wholly_Mainly__c!=null && EX.LightPartlyFixing__c!=null){
              String MainVal=EX.Light_Fixing_Wholly_Mainly__c;
              String PartVal=EX.LightPartlyFixing__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Light fixings are mainly '+MainVal+', partly '+splitstr;
              EX.Light_Fixing_RepValue__c= fullstr;
             }
             else if(EX.Light_Fixing_Wholly_Mainly__c==null && EX.LightPartlyFixing__c==null){
              EX.Light_Fixing_RepValue__c=null;
             }

//Lighting Type

      if(EX.Lighting_type_Entirely_Mainly__c!=null && EX.Lighting_Type_Partly__c==null)
        {
        EX.Lighting_Type_RepValue__c    ='Lighting is '+ EX.Lighting_type_Entirely_Mainly__c+' throughout';
        }
            else if(EX.Lighting_Type_Partly__c!=null && EX.Lighting_type_Entirely_Mainly__c==null){
              String PartVal=EX.Lighting_Type_Partly__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Lighting_Type_RepValue__c  ='Lighting is partly '+splitstr;
            }
             else if(EX.Lighting_type_Entirely_Mainly__c!=null && EX.Lighting_Type_Partly__c!=null){
              String MainVal=EX.Lighting_type_Entirely_Mainly__c;
              String PartVal=EX.Lighting_Type_Partly__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Lighting is mainly '+MainVal+', partly '+splitstr;
              EX.Lighting_Type_RepValue__c= fullstr;
             }
             else if(EX.Lighting_type_Entirely_Mainly__c==null && EX.Lighting_Type_Partly__c==null){
              EX.Lighting_Type_RepValue__c=null;
             }
//Stair Cases

        if(EX.Wholly_MainlyStair__c !=null && EX.PartlyStair__c==null)
        {
        EX.Main_Staircases_RepValue__c='Main staircases are '+ EX.Wholly_MainlyStair__c  ;
        }
            else if(EX.PartlyStair__c!=null && EX.Wholly_MainlyStair__c ==null){
              String PartVal=EX.PartlyStair__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Main_Staircases_RepValue__c='Main staircases are partly '+splitstr;
            }
             else if(EX.Wholly_MainlyStair__c   !=null && EX.PartlyStair__c!=null){
              String MainVal=EX.Wholly_MainlyStair__c;
              String PartVal=EX.PartlyStair__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Main staircases are mostly '+MainVal+', partly '+splitstr;
              EX.Main_Staircases_RepValue__c= fullstr;
             }
             else if(EX.Wholly_MainlyStair__c==null && EX.PartlyStair__c==null){
              EX.Main_Staircases_RepValue__c=null;
             }        


//Power Distribution

        if(EX.Wholly_MainlyPowerDisribution__c  !=null && EX.PartlyPowerDisribution__c==null)
        {
        EX.Power_Distribution_RepValue__c='Power distribution is '+ EX.Wholly_MainlyPowerDisribution__c    ;
        }
            else if(EX.PartlyPowerDisribution__c!=null && EX.Wholly_MainlyPowerDisribution__c   ==null){
              String PartVal=EX.PartlyPowerDisribution__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Power_Distribution_RepValue__c='Power distribution is partly '+splitstr;
            }
             else if(EX.Wholly_MainlyPowerDisribution__c!=null && EX.PartlyPowerDisribution__c!=null){
              String MainVal=EX.Wholly_MainlyPowerDisribution__c;
              String PartVal=EX.PartlyPowerDisribution__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Power distribution is mainly '+MainVal+', partly '+splitstr;
              EX.Power_Distribution_RepValue__c= fullstr;
             }
             else if(EX.Wholly_MainlyPowerDisribution__c==null && EX.PartlyPowerDisribution__c==null){
              EX.Power_Distribution_RepValue__c=null;
             }         

//Ceilings

        if(EX.Wholly_Mainly__c  !=null && EX.PartlyCeiling__c==null)
        {
        EX.The_Ceilings_are_RepValue__c='Entirely '+ EX.Wholly_Mainly__c    ;
        }
            else if(EX.PartlyCeiling__c!=null && EX.Wholly_Mainly__c    ==null){
              String PartVal=EX.PartlyCeiling__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.The_Ceilings_are_RepValue__c='Ceilings are partly '+splitstr;
            }
             else if(EX.Wholly_Mainly__c    !=null && EX.PartlyCeiling__c!=null){
              String MainVal=EX.Wholly_Mainly__c;
              String PartVal=EX.PartlyCeiling__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Ceilings are mainly '+MainVal+', partly '+splitstr;
              EX.The_Ceilings_are_RepValue__c= fullstr;
             }
             else if(EX.Wholly_Mainly__c    ==null && EX.PartlyCeiling__c==null){
              EX.The_Ceilings_are_RepValue__c=null;
             }        
    

//Internal Walls

        if(EX.Wholly_MainlyInternal__c  !=null && EX.PartlyInternal__c==null)
        {
        EX.The_Internal_Walls_are_RepValue__c=EX.Wholly_MainlyInternal__c+' throughout';
        }
            else if(EX.PartlyInternal__c!=null && EX.Wholly_MainlyInternal__c   ==null){
              String PartVal=EX.PartlyInternal__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.The_Internal_Walls_are_RepValue__c='Internal walls are partly '+splitstr;
            }
             else if(EX.Wholly_MainlyInternal__c    !=null && EX.PartlyInternal__c!=null){
              String MainVal=EX.Wholly_MainlyInternal__c;
              String PartVal=EX.PartlyInternal__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Internal walls are mainly '+MainVal+', partly '+splitstr;
              EX.The_Internal_Walls_are_RepValue__c= fullstr;
             }
             else if(EX.Wholly_MainlyInternal__c ==null && EX.PartlyInternal__c==null){
              EX.The_Internal_Walls_are_RepValue__c=null;
             }        
   }
    }


}