/**
 *  Class Name: UtilClass 
 *  Description: utility class for .... functionality
 *  Company: dQuotient
 *  CreatedDate: 19/08/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  G Aparna                19/08/2016
 *
 */

public class UtilClass{
    
    /**
     *  Method Name:    findOjectFromId 
     *  Description:    Finding sObjectType from recordId
     *  Param:  String recordId
     *  Return: Schema.sObjectType
     */
     
    public static Schema.sObjectType  findOjectFromId(String recordId){
        if ( recordId != null) {
            Id idValue = Id.valueOf(recordId);
            //get sObjectType from idValue passed
            return(idValue.getSobjectType());   
        }
        else 
            return null;
    }
    
    /**
     *  Method Name:    findChildsFromParent
     *  Description:    Finding childrelationships from sObjectType
     *  Param:  Schema.sObjectType objectName
     *  Return: list<Schema.ChildRelationship>
     */
        
    public static list<Schema.ChildRelationship> findChildsFromParent(Schema.sObjectType objectName){
        if ( objectName != null){
            
            DescribeSObjectResult objectDescribe = objectName.getDescribe();
            list<sObject> childList;
            //getting childrelationships using getDescribe method 
            
            //System.debug('letscheck'+objectDescribe.getChildRelationships());
            //listofchildrelstion  = objectDescribe.getChildRelationships();
            return objectDescribe.getChildRelationships();  
            
        }
        
        else
            return null;
    }
    
    /**
     *  Method Name:    findChildRelationship
     *  Description:    Find childrelationship from parent and child sObjectTypes
     *  Param:  Schema.sObjectType parentName,Schema.sObjectType childName
     *  Return: Schema.ChildRelationship
     */
     
    public static Schema.ChildRelationship findChildRelationship(Schema.sObjectType parentName,Schema.sObjectType childName){
        if ( parentName != null && childName!=null ){
            //gives parentObject properties and describe results
            Schema.DescribeSObjectResult   parentDescribe = parentName.getDescribe();
            //getting list of childrelationships of parentObject
            List<Schema.ChildRelationship>   listChildRelations =parentDescribe.getChildRelationships();
            Schema.ChildRelationship relName;
            //comparing the childsObjects from list of childrelationships with the chilname given
            for(Schema.ChildRelationship schemaRelName:listChildRelations){
                if(schemaRelName.getChildSObject()==childName)
                {
                    relName=schemaRelName;
                    break;
                }
        }
       
        return relName;     
        }
        else
            return null;
    }   
        
    /**
     *  Method Name:    findRequiredFields
     *  Description:    Finding Fields from sObjectType
     *  Param:  Schema.sObjectType objectName
     *  Return: list<Schema.SObjectField>
     */
     
    public static list<Schema.SObjectField> findRequiredFields(Schema.SObjectType objectName){
        if (objectName != null){
            list<Schema.SObjectField> reqfieldlst = new list<Schema.SObjectField>();
            List<Schema.SObjectField> fieldlist = objectName.getDescribe().fields.getMap().values();
            for(Schema.SObjectField fields:fieldlist){
                if(!fields.getDescribe().isNillable()){
                    reqfieldlst.add(fields);
                }
            }
            return(fieldlist );
            // return list change from reqfieldlst to fieldlist
        }                 
        else 
            return null;
    }
    
    /**
     *  Method Name:    readFieldSet
     *  Description:    Finding Fields from fieldset name
     *  Param:  String fieldSetName, String ObjectName
     *  Return: List<Schema.FieldSetMember>
     */
    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName)
    {
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
    
        //system.debug('====>' + DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName));
    
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        return fieldSetObj.getFields(); 
    }
    
    /**
     *  Method Name:    getFieldSetNameFromCustomSetting
     *  Description:    Find Fieldset Name from object Api name
     *  Param:  String ObjectName
     *  Return: String
     */
    public static String getFieldSetNameFromCustomSetting(String objectName){    
        if(objectName!=null){
            List<ObjectFieldSetMapping__c> objCustom=new List<ObjectFieldSetMapping__c>();
            objCustom=[SELECT Field_Set_Name__c FROM ObjectFieldSetMapping__c WHERE Object_API_Name__c= :objectName];
            if(!objCustom.isEmpty()){
                return ((String)objCustom[0].get('Field_Set_Name__c'));
            }
            else
                return null;
        }
        else
            return null;
    }
}