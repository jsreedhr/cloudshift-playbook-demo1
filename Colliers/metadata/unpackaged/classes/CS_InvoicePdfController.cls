/**
	*  Class Name: CS_InvoicePdfController
	*  Description: This is a class to edit all invoice related to the jobs
	*  Company: dQuotient
	*  CreatedDate: 27/09/2016
	*
	*  Modification Log
	*  -----------------------------------------------------------
	*  Developer           Modification Date           Comments
	*  -----------------------------------------------------------
	*  Hemant garg         17/10/2016                  Orginal Version
	*  M Rizwan			   13/10/2017				   Changes to display User entered values for some Invoices
	*  Nidheesh N          06/11/2017                  Changes to dispaly the new line for Invoice payment footer
	*
	*/
public with sharing class CS_InvoicePdfController{

	Public String prospectId{get;set;}
	Public String InvId{get;set;}
	//public string invoicewording{get;set;}
	//Public boolean summery{get;set;}
	//Public string CreatedFor{get;set;}
	Public List<invoice__c> listInvoive {get;set;}
	public List<Invoice_PDF_Value__c> InvPdf{get;set;}
	public List<string> Remitence{get;set;}
	public string remitanceAddress{get;set;}
	public string invAddress{get;set;}
	public List<string> invoiceAddress{get;set;}
	public string FooterMsg{get;set;}
	public string accountcode{get;set;}
	public string companyName{get;set;}
	public string contactName{get;set;}
	public Decimal totalcost{get;set;}
	public Decimal CostFee{get;set;}
	public Decimal totalfee{get;set;}
	public Decimal totalvat{get;set;}
	public Decimal totalamount{get;set;}
	public Decimal lineFee{get;set;}
	public date todaysdate{get;set;}
	public Boolean isCapital{get;set;}
	public Boolean isnilemanage{get;set;}
	public string IsDraftOrApproved{get;set;}
	public string Paymentfoot{get;set;}
	public string Footer{get;set;}
	public string faoname{get;set;}
	public string careofClient{get;set;}
	public string currencysymbiol{get;set;}
	public string PoRef{get;set;}
	public Opportunity oppobject{get;set;}
	public string status{get;set;}
	public string currencyCode {get;set;}
	public List<Invoice_Line_Item__c>FeeList{get;set;}
	public List<Invoice_Line_Item__c>CostList{get;set;}
	public currency_Symbol__c currencyobj {get;set;}
	public boolean showCareof{get;set;}
	public List<string>  remittanceFooter{get;set;}

	Public Boolean showChangedValues{get;set;}
	Public Double feeTotal{get;set;}
	Public Double costTotal{get;set;}
	Public Double vatTotal{get;set;}



	/**
		*   Method Name:    CS_InvoicePdfController
		*   Description:    Constructor
		*/
	public CS_InvoicePdfController() {
		isCapital = false;
		//invoicewording = '';
		companyName = '';
		contactName = '';
		isnilemanage = false;
		accountcode ='';
		todaysdate=date.today();
		IsDraftOrApproved = '';
		faoname = '';
		totalamount = 0.00;
		totalfee = 0.00;
		totalvat = 0.00;
		totalcost = 0.00;
		currencyobj = new currency_Symbol__c();
		oppobject = new Opportunity();
		PoRef='';
		currencysymbiol = '';
		invAddress = '';
		careofClient = '';
		FooterMsg= Label.Default_FooterMsg;
		InvId = apexpages.currentpage().getparameters().get('Invid');
		CostList= new List<Invoice_Line_Item__c>();
		FeeList= new List<Invoice_Line_Item__c>();
		prospectId = apexpages.currentpage().getparameters().get('id');
		status = apexpages.currentpage().getparameters().get('Copy');
		listInvoive = new List < invoice__c > ();
		InvPdf= new List<Invoice_PDF_Value__c>();
		linefee=0;
		Costfee=0;
		currencyCode = '';
		Remitence= new List<string> ();
		remitanceAddress = '';
		invoiceAddress = new List<string> ();
		List<Account_Address_Junction__c> accAdressobj  = new List<Account_Address_Junction__c>();
		string colliervatlabel = Label.colliers_Vat_number;
		List<colliers_Vat_Number__c> listofcolliersvat = new List<colliers_Vat_Number__c>();

		If(colliervatlabel != null && string.isNotBlank(colliervatlabel)){
			listofcolliersvat = [SELECT Type_Of_Vat_Number__c,Vat_Value__c FROM colliers_Vat_Number__c WHERE Type_Of_Vat_Number__c =:colliervatlabel];
		}

		//checking whether to show actual data or changed values (13-Oct-2017)
		showChangedValues = false;
		feeTotal = 0;
		costTotal = 0;
		vatTotal = 0;
		Invoice__c pdfInvoice = new Invoice__c();
		/*
		* Label.Allocation_Department_Option1 = Residential - Development
		* Label.Allocation_Department_Option2 = Residential - New Homes
		* Label.Allocation_Department_Option3 = International Properties
		* Label.Invoice_Cost_Category = Cost Flow Through (Residential only)
		*/
		try{
			pdfInvoice = [Select Id, Name, Total_PDF_Amount__c,
				(Select Invoice__c From Invoice_Allocation_Junctions__r
					Where Allocation__r.Main_Allocation__c = TRUE AND
					(Allocation__r.Department_Allocation__c =: Label.Allocation_Department_Option1 OR Allocation__r.Department_Allocation__c =: Label.Allocation_Department_Option2 OR Allocation__r.Department_Allocation__c =: Label.Allocation_Department_Option3)
				),
				(Select Invoice__c From Invoice_Cost_Junctions__r
				Where Disbursement__r.Category__c =: Label.Invoice_Cost_Category)
				From Invoice__c
				Where Id =: InvId Limit 1
			];
		}catch(Exception ex){
			System.debug('-- Error while fetching data for PDF Values tab : '+ ex);
		}
		if(pdfInvoice.Invoice_Allocation_Junctions__r.size()> 0 && pdfInvoice.Invoice_Cost_Junctions__r.size()> 0){
			showChangedValues = true;
		}
		//end of block1 of code

		listInvoive = [select Id,Invoice_Company_Name__c,Care_Of_Information__r.Account_ID__c,Opportunity__r.Invoicing_Address_Formula__c,Care_Of_Information__r.Care_of_Company_Name_Full__c,Time_Stamp_remittance__c,Invoice_contact_Name__c, Total_PDF_Amount__c,Care_Of_Information__c,
		Opportunity__r.Manager__r.Name,Opportunity__r.Department_Cost_Centre_Code__c,Care_Of_Information__r.Client__c, Care_Of_Information__r.client__r.Name,  Opportunity__r.Invoicing_Care_Of__c, Opportunity__r.Invoicing_Care_Of__r.Client__c,   Total_Amount_Inc_Vat__c,Description__c,Date_of_Invoice__c ,Total_cost_Ex_Vat__c,Total_Fee_Ex_Vat__c, Invoice_Currency__c,Invoice_Address__c,Address__c,Company__r.name,Opportunity__r.InstructingCompanyAddress__c,Opportunity__r.Invoicing_Company__c,Care_Of_Information__r.name,Invoice_contact__r.name,OriginalInvoicePrinted__c,Final_Invoice__c , Company__c,Opportunity__c,CurrencyIsoCode,PO_Number__r.name,
			(select Name, Purchase_Order__c, Purchase_Order__r.Name, Invoice__c
			From Purchase_Order_Invoice_Junctions__r),
		Opportunity__r.Job_Number__c,Opportunity__r.Invoicing_Company2__r.MLR_Number__c,Opportunity__r.Invoicing_Company2__c,Total_Fee__c,Total_cost__c,Total_amount__c,Total_Internaional_Vat_Amount__c,VAT_Amount__c,Opportunity__r.Invoicing_Company2__r.Client_VAT_Number__c,Invoice_Wording__c,Opportunity__r.name,Opportunity__r.Manager__r.HR_No__c,Opportunity__r.Invoicing_Company2__r.name,Opportunity__r.Manager__r.Coding_Structure__r.Department__c,Opportunity__r.Manager__r.Department__c,Opportunity__r.id,Assigned_Invoice_Number__c,File_Reference__c,Vat_Reg_No__c ,Foreign_Country__c,Opportunity__r.Department_Integer_Id__c,Opportunity__r.Manager__r.id, name,  Contact__c, Contact__r.name, FE_Write_Of_Created_Date__c, is_International__c, Payable_By__c,status__c,VAT__c, createdDate,
			(select id,fee__c,type__c,Cost_Type__c,International_Amount__c,Amount_ex_VAT__c,VAT_Amount__c,Description__c,International_VAT_Amount_Formula__c, PDF_Amount__c, PDF_VAT__c
			From Invoice_Line_Items__r where Type__c='Fee' Or Type__c='Cost')
		From invoice__c where id=:InvId limit 1];

		//check whether to show original valu or changed value
		for(Invoice_Line_Item__c inv: listInvoive[0].Invoice_Line_Items__r){
			if(showChangedValues == true && (inv.PDF_Amount__c == null || inv.PDF_Amount__c<=0) ){
				showChangedValues = false;
				break;
			}
		}
		//if showChangedValues is still true then calculate new feeTotal, costTotal & VATTotal
		if(showChangedValues){
			feeTotal = costTotal = vatTotal = 0;
			for(Invoice_Line_Item__c inv: listInvoive[0].Invoice_Line_Items__r){
				vatTotal += inv.PDF_VAT__c;
				if(inv.Type__c == 'Fee')
					feeTotal += inv.PDF_Amount__c;
				else if(inv.Type__c == 'Cost')
					costTotal += inv.PDF_Amount__c;
			}
			/*vatTotal = vatTotal.toScale(2);
			feeTotal = feeTotal.toScale(2);
			costTotal = costTotal.toScale(2);
			*/
		}

		if(listInvoive[0].Opportunity__c != null){
			oppobject = [SELECT Id,Invoicing_Company__r.name,Invoicing_Company__r.Address__r.Building__c,Invoicing_Company__r.Address__r.Street__c,Invoicing_Company__r.Address__r.Town__c,Invoicing_Company__r.Address__r.country__c FROM Opportunity WHERE Id=:listInvoive[0].Opportunity__c];

		}
		/*if(!String.isBlank(listInvoive[0].Invoice_Wording__c)){
				invoicewording = listInvoive[0].Invoice_Wording__c.replace('&nbsp;',' ');
			}*/
        // added remittance footer as per the change ---- NIDHEESH 03/11/2017
        if(listInvoive[0].Assigned_Invoice_Number__c != null){
            String labelValue = Label.Remittancefooter;
            labelValue = labelValue.replace('InvoiceNumber',listInvoive[0].Assigned_Invoice_Number__c);
            remittanceFooter = labelValue.split('<br/>');
        }else{
           remittanceFooter = new List<String>();
        }
		if(listInvoive[0].Invoice_contact_Name__c !=null && String.isNotBlank(listInvoive[0].Invoice_contact_Name__c) ){
			contactName = listInvoive[0].Invoice_contact_Name__c;
		}else{
			if(listInvoive[0].Invoice_contact__r.name !=null && String.isNotBlank(listInvoive[0].Invoice_contact__r.name) ){
				contactName = listInvoive[0].Invoice_contact__r.name;
			}
		}
		if(listInvoive[0].Invoice_Company_Name__c !=null && String.isNotBlank(listInvoive[0].Invoice_Company_Name__c) ){
			faoname = listInvoive[0].Invoice_Company_Name__c;
			showCareof = false;
		}else if(listInvoive[0].Company__r.name!= null && string.isNotBlank(listInvoive[0].Company__r.name)){
			faoname = listInvoive[0].Company__r.name;
			showCareof = false;
		}else if(listInvoive[0].Care_Of_Information__r.Care_of_Company_Name_Full__c!= null && string.isNotBlank(listInvoive[0].Care_Of_Information__r.Care_of_Company_Name_Full__c)){
			faoname = listInvoive[0].Care_Of_Information__r.Care_of_Company_Name_Full__c;
			careofClient = listInvoive[0].Care_Of_Information__r.client__r.name;//Defect-191
			showCareof=true;
		}


		if(listInvoive[0].Company__c != null){

			accAdressobj =  [SELECT Account_ID__c,Id,Name FROM Account_Address_Junction__c WHERE Account__c =: listInvoive[0].Company__c AND Address__c =:listInvoive[0].Address__c ];
			if(!accAdressobj.isEmpty()){
				accountcode  = accAdressobj[0].Account_ID__c;
			}
			system.debug('Object msg inside'+accountcode);
		}else if(listInvoive[0].Care_Of_Information__c != null){
			if(listInvoive[0].Care_Of_Information__r.Account_ID__c != null && string.isNotBlank(listInvoive[0].Care_Of_Information__r.Account_ID__c)){
				accountcode =listInvoive[0].Care_Of_Information__r.Account_ID__c;
			}
			else if(listInvoive[0].Care_Of_Information__r.client__c != null){
				accAdressobj =  [SELECT Account_ID__c,Id,Name FROM Account_Address_Junction__c WHERE Account__c =: listInvoive[0].Care_Of_Information__r.client__c AND Address__c =:listInvoive[0].Address__c ];

				if(!accAdressobj.isEmpty()){
					accountcode = accAdressobj[0].Account_ID__c;
				}
			}
			// system.debug('Object msg outside'+accAdressobj[0].id);

		}
		if(listInvoive[0].Opportunity__r.Manager__r.Department__c != null && string.isNotBlank(listInvoive[0].Opportunity__r.Manager__r.Department__c)){
			if(listInvoive[0].Opportunity__r.Manager__r.Department__c.contains('Colliers Capital')){
				isCapital = true;
			}
		}

		if(listInvoive[0].VAT__c== null){
			listInvoive[0].VAT__c=0;
		}

		List<String> addresslist= new List<string> ();
		if(listInvoive[0].Invoice_Address__c != null && string.isNotBlank(listInvoive[0].Invoice_Address__c)){
			addresslist = listInvoive[0].Invoice_Address__c.split(',');
		}else if(listInvoive[0].Opportunity__r.Invoicing_Address_Formula__c != null && string.isNotBlank(listInvoive[0].Opportunity__r.Invoicing_Address_Formula__c)){
			addresslist = listInvoive[0].Opportunity__r.Invoicing_Address_Formula__c.split(',');
		}
		for(string objstr: addresslist){
			if(string.isNotBlank(objstr) && objstr != null){
				invoiceAddress.add(objstr);
			}
		}
		if(!listInvoive.isEmpty()){
			if(listInvoive[0].Invoice_Currency__c != null){
				currencyCode = listInvoive[0].Invoice_Currency__c;
			}else{
				currencyCode = 'GBP';
			}
		}
		LisT<currency_Symbol__c> lstCurrencyCode = new LisT<currency_Symbol__c>();
		lstCurrencyCode = [SELECT Id,currencyName__c,currencySymbol__c FROM currency_Symbol__c WHERE currencyName__c=:currencyCode LIMIT 1];
		if(!lstCurrencyCode.isEmpty()){
			currencyobj = lstCurrencyCode[0];
			if(currencyobj != null){
				currencysymbiol = currencyobj.currencySymbol__c;
			}
		}



		System.debug('-------'+listInvoive[0].Opportunity__r.Invoicing_Company2__c);
		System.debug('-------'+listInvoive[0].Opportunity__r.Invoicing_Company__c);
		if(listInvoive != null && listInvoive.size()>0){

			system.debug('pdf value'+InvPdf);
			if(status == 'COPY'){
				IsDraftOrApproved='CERTIFIED COPY';
			}
			else if(status == 'DRAFT'){
				IsDraftOrApproved='DRAFT INVOICE';
			}
			else if(status== 'FINAL'){
				IsDraftOrApproved='';//changed for D-161
			}
			system.debug('invoice pdf values'+invpdf);
			string nilepdf = Label.Nile_manager_pdf;

			if(listInvoive[0].Opportunity__r.Manager__r.id != null ){

				InvPdf = [SELECT Id,Cheque_Footer__c,  External_Id_JM_UK__c,Footer__c, Currency__c,Remittance__c,Payment_Footer__c,Header_Details__c,  Dept_Cost_Centre__c, Company_Name__c from Invoice_PDF_Value__c WHERE  HrNo__c=:listInvoive[0].Opportunity__r.Manager__r.id];

			}if(InvPdf.size() > 0){
				/*if(InvPdf[0].Company_Name__c != null && String.isNotBlank(InvPdf[0].Company_Name__c)){
							if(InvPdf[0].Company_Name__c.contains('COLLIERS CAPITAL' )){
								isCapital = true;
							}
						}*/

				if(InvPdf[0].Company_Name__c != null && String.isNotBlank(InvPdf[0].Company_Name__c) && InvPdf[0].Company_Name__c.contains('NILE MANAGEMENT')){
					isnilemanage = true;
				}
				string RemitanceBlank=Label.Default_Remittance;

				//isCapital=InvPdf[0].Remittance__c.contains('COLLIERS CAPITAL');


				if(listInvoive[0].Time_Stamp_remittance__c != null && String.isNotBlank(listInvoive[0].Time_Stamp_remittance__c)){
					Remitence=listInvoive[0].Time_Stamp_remittance__c.split(',');
				}else if(InvPdf[0].Remittance__c != null && InvPdf[0].Remittance__c!=''){
					Remitence=InvPdf[0].Remittance__c.split(',');
					// remitanceAddress = InvPdf[0].Remittance__c;
				}
				else{
					Remitence=RemitanceBlank.split('  ');
					// remitanceAddress = RemitanceBlank;
				}

				if(InvPdf[0].Payment_Footer__c != null && InvPdf[0].Payment_Footer__c != ''){
					Paymentfoot=invPdf[0].Payment_Footer__c;
				}
				else if(InvPdf[0].Payment_Footer__c==null || InvPdf[0].Payment_Footer__c==''){
					Paymentfoot= Label.Default_Paymentfoot;

				}

				if(InvPdf[0].Footer__c != null && InvPdf[0].Footer__c != ''){
					Footer=invPdf[0].Footer__c;
				}
				else if(InvPdf[0].Footer__c==null || InvPdf[0].Footer__c==''){
					Footer= Label.Default_Footer;

				}
			}

			else{
				InvPdf = new List<Invoice_PDF_Value__c>();
				InvPdf = [SELECT Id,Cheque_Footer__c,  External_Id_JM_UK__c,Footer__c, Currency__c,Remittance__c,Payment_Footer__c,Header_Details__c,  Dept_Cost_Centre__c, Company_Name__c from Invoice_PDF_Value__c WHERE  Dept_Cost_Centre__c includes (:listInvoive[0].Opportunity__r.Department_Cost_Centre_Code__c ) AND Currency__c =: listInvoive[0].Invoice_Currency__c AND  Dept_Cost_Centre__c != '' AND Currency__c != NULL ];
				system.debug('invoice pdf value----'+InvPdf);
				if(InvPdf.size() > 0){
					/*if(InvPdf[0].Company_Name__c != null && String.isNotBlank(InvPdf[0].Company_Name__c)){
								if(InvPdf[0].Company_Name__c.contains('COLLIERS CAPITAL' )){
									isCapital = true;
								}
							}*/



					string RemitanceBlank=Label.Default_Remittance;

					//isCapital=InvPdf[0].Remittance__c.contains('COLLIERS CAPITAL');


					if(listInvoive[0].Time_Stamp_remittance__c != null && String.isNotBlank(listInvoive[0].Time_Stamp_remittance__c)){
						Remitence=listInvoive[0].Time_Stamp_remittance__c.split(',');
					}else if(InvPdf[0].Remittance__c != null && InvPdf[0].Remittance__c!=''){
						Remitence=InvPdf[0].Remittance__c.split(',');
						// remitanceAddress = InvPdf[0].Remittance__c;
					}
					else{
						Remitence=RemitanceBlank.split('  ');
						// remitanceAddress = RemitanceBlank;
					}
					if(InvPdf[0].Payment_Footer__c != null && InvPdf[0].Payment_Footer__c != ''){
						Paymentfoot=invPdf[0].Payment_Footer__c;
					}
					else if(InvPdf[0].Payment_Footer__c==null || InvPdf[0].Payment_Footer__c==''){
						Paymentfoot= Label.Default_Paymentfoot;

					}

					if(InvPdf[0].Footer__c != null && InvPdf[0].Footer__c != ''){
						Footer=invPdf[0].Footer__c;
					}
					else if(InvPdf[0].Footer__c==null || InvPdf[0].Footer__c==''){
						Footer= Label.Default_Footer;

					}

				}
				else{
					InvPdf = new List<Invoice_PDF_Value__c>();
					InvPdf = [SELECT Id,Cheque_Footer__c,  External_Id_JM_UK__c,Footer__c, Currency__c,Remittance__c,Payment_Footer__c,Header_Details__c,  Dept_Cost_Centre__c, Company_Name__c from Invoice_PDF_Value__c WHERE  Dept_Cost_Centre__c includes (:listInvoive[0].Opportunity__r.Department_Cost_Centre_Code__c ) AND Currency__c = 'GBP' AND  Dept_Cost_Centre__c != '' AND Currency__c != NULL ];
					system.debug('invoice pdf value----'+InvPdf);
					if(InvPdf.size() > 0){
						/*if(InvPdf[0].Company_Name__c != null && String.isNotBlank(InvPdf[0].Company_Name__c)){
								if(InvPdf[0].Company_Name__c.contains('COLLIERS CAPITAL' )){
									isCapital = true;
								}
							}*/


						string RemitanceBlank=Label.Default_Remittance;

						//isCapital=InvPdf[0].Remittance__c.contains('COLLIERS CAPITAL');


						if(listInvoive[0].Time_Stamp_remittance__c != null && String.isNotBlank(listInvoive[0].Time_Stamp_remittance__c)){
							Remitence=listInvoive[0].Time_Stamp_remittance__c.split(',');
						}else if(InvPdf[0].Remittance__c != null && InvPdf[0].Remittance__c!=''){
							Remitence=InvPdf[0].Remittance__c.split(',');
							// remitanceAddress = InvPdf[0].Remittance__c;
						}
						else{
							Remitence=RemitanceBlank.split('  ');
							// remitanceAddress = RemitanceBlank;
						}
						if(InvPdf[0].Payment_Footer__c != null && InvPdf[0].Payment_Footer__c != ''){
							Paymentfoot=invPdf[0].Payment_Footer__c;
						}
						else if(InvPdf[0].Payment_Footer__c==null || InvPdf[0].Payment_Footer__c==''){
							Paymentfoot= Label.Default_Paymentfoot;

						}

						if(InvPdf[0].Footer__c != null && InvPdf[0].Footer__c != ''){
							Footer=invPdf[0].Footer__c;
						}
						else if(InvPdf[0].Footer__c==null || InvPdf[0].Footer__c==''){
							Footer= Label.Default_Footer;

						}

					}else{

						string RemitanceBlank=Label.Default_Remittance;
						Remitence=RemitanceBlank.split('  ');
						// remitanceAddress = RemitanceBlank;
						Paymentfoot= Label.Default_Paymentfoot;
						Footer = Label.Default_Footer;
					}

				}
			}
			if(!listofcolliersvat.isEmpty()){
				Footer += ' , VAT Reg No: '+listofcolliersvat[0].Vat_Value__c+' ';
			}
			for(Purchase_Order_Invoice_Junction__c PoJn: listInvoive[0].Purchase_Order_Invoice_Junctions__r){

				if(PoRef==''){
					PoRef= PoJn.Purchase_Order__r.Name;
				}
				else{
					PoRef=PoRef+', '+PoJn.Purchase_Order__r.Name;
				}

			}
			for(Invoice_Line_Item__c cst: listInvoive[0].Invoice_Line_Items__r){
				if( cst.type__c=='Cost'){
					CostList.add(cst);
				}
				else{
					if(cst.Amount_ex_VAT__c >00){
						FeeList.add(cst);
					}

				}

			}
		}
	}
}