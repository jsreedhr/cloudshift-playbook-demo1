/**
 *  Class Name: SharingRuleConfigurationExtension
 *  Description: Test class for SharingRuleConfigurationExtension
 *  Company: CloudShift
 *  CreatedDate: 29/05/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Gavin Palmer             29/05/2018                 Orginal Version
 *
 */
@isTest
private class SharingRuleConfigurationExtensionTest {

    private static final Integer SHARING_CONDITION_SIZE = 5;

    @testSetup
    private static void setup() {
        SharingRuleTestHelper.SHARED_WITH_ID = getGroup().Id;
        Sharing_Rule__c testSharingRule = SharingRuleTestHelper.insertSharingRule();
        SharingConditionTestHelper.SHARING_RULE_ID = testSharingRule.Id;
        List<Sharing_Condition__c> sharingConditions = SharingConditionTestHelper.insertMultipleSharingConditions(SHARING_CONDITION_SIZE);
    }

    @isTest
    private static void constructorTest_NewSharingRule() {
        SharingRuleConfigurationExtension extension = new SharingRuleConfigurationExtension(
            new ApexPages.StandardController(new Sharing_Rule__c())
        );

        System.assertNotEquals(
            null, extension.sharingConditions, 
            'The sharing conditions list should be instansiated for new sharing rules'
        );
        System.assert(
            extension.sharingConditions.isEmpty(), 
            'The sharing conditions list should be empty for new sharing rules'
        );

        System.assert(String.isNotBlank(extension.getSHARING_LOGIC_CUSTOM_LABEL()), 'Label for Custom sharing should be returned.');
    }

    @isTest
    private static void constructorTest_ExistingSharingRule() {
        
        SharingRuleConfigurationExtension extension = new SharingRuleConfigurationExtension(
            new ApexPages.StandardController(getSharingRule())
        );

        System.assertNotEquals(
            null, extension.sharingConditions, 
            'The sharing conditions list should be instansiated for new sharing rules'
        );
        System.assertEquals(
            SHARING_CONDITION_SIZE, extension.sharingConditions.size(), 
            'The sharing conditions list should be equal to those created in the setup'
        );
        System.assertEquals(
            getGroup().Type, extension.selectedGroupType, 
            'The group type for the existing shared with field should be defaulted on page load'
        );
    }

    @isTest
    private static void getGroupTypesTest() {
        SharingRuleConfigurationExtension extension = new SharingRuleConfigurationExtension(
            new ApexPages.StandardController(new Sharing_Rule__c())
        );

        // we cannot garuntee the values that are returned in the group types so just validate that the list is not empty
        System.assert(
            1 < extension.getGroupTypes().size(), 
            'There should be at least one group type returned'
        );
    }

    @isTest
    private static void getGroupOptionsTest_NoTypeSelected() {
        SharingRuleConfigurationExtension extension = new SharingRuleConfigurationExtension(
            new ApexPages.StandardController(new Sharing_Rule__c())
        );

        System.assertEquals(
            1, extension.getGroupOptions().size(), 
            'There should be only 1 picklist value when no group type is selected'
        );
    }

    @isTest
    private static void getGroupOptionsTest_TypeSelected() {
        SharingRuleConfigurationExtension extension = new SharingRuleConfigurationExtension(
            new ApexPages.StandardController(new Sharing_Rule__c())
        );
        final String groupType = 'Regular';
        extension.selectedGroupType = groupType;

        // we cannot garuntee the values that are returned in the group types so just validate that the list is not empty
        System.assert(
            1 < extension.getGroupOptions().size(), 
            'There should be at least one group returned when ' + groupType + ' is selected'
        );
    }

    @isTest
    private static void getObjectFieldsTest_NoTypeSelected() {
        SharingRuleConfigurationExtension extension = new SharingRuleConfigurationExtension(
            new ApexPages.StandardController(new Sharing_Rule__c())
        );

        System.assertEquals(
            1, extension.getObjectFields().size(), 
            'There should be only 1 picklist value when no object is selected'
        );
    }

    @isTest
    private static void getObjectFieldsTest_TypeSelected() {
        SharingRuleConfigurationExtension extension = new SharingRuleConfigurationExtension(
            new ApexPages.StandardController(new Sharing_Rule__c())
        );
        final String objectName = 'Opportunity';
        final String fieldName = 'name';
        extension.sharingRule.Object__c = objectName;

        // check that at least the name field is present in the select options
        Boolean hasNameField = false;
        for (SelectOption currentOption : extension.getObjectFields()) {
            if (currentOption.getValue().equalsIgnoreCase(fieldName)) {
                hasNameField = true;
                break;
            }
        }
        System.assert(
            hasNameField, 
            'The ' + fieldName + ' field should be within the list of select options for ' + objectName
        );
    }

    @isTest
    private static void addSharingConditionTest() {
        SharingRuleConfigurationExtension extension = new SharingRuleConfigurationExtension(
            new ApexPages.StandardController(new Sharing_Rule__c())
        );
        System.assertEquals(
            0, extension.sharingConditions.size(), 
            'The sharing condition list should be empty before any are added on a new sharing rule'
        );
        extension.addSharingCondition();
        System.assertEquals(
            extension.NUM_OF_NEW_CONDITIONS_WHEN_ADDING_NEW, extension.sharingConditions.size(),
            'The sharing conditions list should have incremented by ' + extension.NUM_OF_NEW_CONDITIONS_WHEN_ADDING_NEW  + 'when the add conditions method is called'
        );
        Sharing_Condition__c sharingConditionToCheck = extension.sharingConditions[0];
        System.assertEquals(
            null, sharingConditionToCheck.Id,
            'The sharing condition id should be blank when a new sharing condition is added'
        );
        System.assertEquals(
            null, sharingConditionToCheck.Field_API_Name__c,
            'The sharing condition Field_API_Name__c should be blank when a new sharing condition is added'
        );
        System.assertEquals(
            null, sharingConditionToCheck.Operator__c,
            'The sharing condition Operator__c should be blank when a new sharing condition is added'
        );
        System.assertEquals(
            null, sharingConditionToCheck.Value__c,
            'The sharing condition Value__c should be blank when a new sharing condition is added'
        );
    }

    @isTest
    private static void removeSelectedConditionTest_NewSharingCondition() {
        SharingRuleConfigurationExtension extension = new SharingRuleConfigurationExtension(
            new ApexPages.StandardController(getSharingRule())
        );
        System.assertEquals(
            SHARING_CONDITION_SIZE, extension.sharingConditions.size(),
            'All sharing conditions created in setup should be present'
        );
        System.assertEquals(
            0, extension.sharingConditionsToDelete.size(),
            'There should be no sharing rules in the list for deletion after instansiation'
        );

        extension.conditionToDeleteIndex = '0';
        extension.removeSelectedCondition();
        System.assertEquals(
            SHARING_CONDITION_SIZE - 1, extension.sharingConditions.size(),
            'One of the sharing rules should have been removed from the sharing conditions list'
        );
        System.assertEquals(
            1, extension.sharingConditionsToDelete.size(),
            'One sharing rule should have been added to the sharing conditions to delete list'
        );
    }

    @isTest
    private static void saveTest_Positive() {

        final String newName = 'A changed name';
        Sharing_Rule__c sharingRule = getSharingRule();
        System.assertEquals(
            SHARING_CONDITION_SIZE, [SELECT COUNT() FROM Sharing_Condition__c WHERE Sharing_Rule__c = :sharingRule.Id],
            'All the sharing conditions should have been created in the setup method'
        );
        SharingRuleConfigurationExtension extension = new SharingRuleConfigurationExtension(
            new ApexPages.StandardController(sharingRule)
        );
        extension.sharingRule.Name = newName;

        extension.conditionToDeleteIndex = '0';
        extension.removeSelectedCondition();

        Test.startTest();
        PageReference savePage = extension.save();
        Test.stopTest();

        System.assertNotEquals(
            null, savePage, 
            'The page should have redirected to the record view page'
        );
        System.assertEquals(
            newName, getSharingRule().Name,
            'The new name should have saved against the sharing rule'
        );
        System.assertEquals(
            SHARING_CONDITION_SIZE - 1, [SELECT COUNT() FROM Sharing_Condition__c WHERE Sharing_Rule__c = :sharingRule.Id],
            'One of the sharing conditions should have been deleted after the save was called'
        );
    }

    @isTest
    private static void saveTest_Negative_InvalidSharingConditions() {

        SharingRuleConfigurationExtension extension = new SharingRuleConfigurationExtension(
            new ApexPages.StandardController(SharingRuleTestHelper.getSharingRule())
        );
        // don't fill any fields on the sharing condition so that an error is thrown
        extension.addSharingCondition();

        Test.startTest();
        extension.sharingRule.Name = 'saveTest_Negative_InvalidSharingConditions';
        PageReference savePage = extension.save();
        Test.stopTest();

        System.assertEquals(
            null, savePage, 
            'The page should not have redirected'
        );
        System.assertEquals(
            1, [SELECT COUNT() FROM Sharing_Rule__c],
            'No new sharing rules should have been created'
        );
        System.assertEquals(
            SHARING_CONDITION_SIZE, [SELECT COUNT() FROM Sharing_Condition__c],
            'No new sharing conditions should have been created'
        );
        System.assertEquals(
            null, extension.sharingRule.Id, 
            'The sharing rule id should have been cleared after any errors'
        );
        // we should see the missing field value in the page messages
        Assert.pageMessageExists('Operator__c');
    }

    @isTest
    private static void saveTest_Negative_NoSharingConditions() {

        SharingRuleConfigurationExtension extension = new SharingRuleConfigurationExtension(
            new ApexPages.StandardController(SharingRuleTestHelper.getSharingRule())
        );

        Test.startTest();
        PageReference savePage = extension.save();
        Test.stopTest();

        System.assertEquals(
            null, savePage, 
            'The page should not have redirected'
        );
        System.assertEquals(
            1, [SELECT COUNT() FROM Sharing_Rule__c],
            'No new sharing rules should have been created'
        );
        System.assertEquals(
            SHARING_CONDITION_SIZE, [SELECT COUNT() FROM Sharing_Condition__c],
            'No new sharing conditions should have been created'
        );
        System.assertEquals(
            null, extension.sharingRule.Id, 
            'The sharing rule id should have been cleared after any errors'
        );
        Assert.pageMessageExists(SharingRuleConfigurationExtension.MISSING_SHARING_CONDITIONS);
    }

    private static Sharing_Rule__c getSharingRule() {
        return [
            SELECT Name, Shared_With__c
            FROM Sharing_Rule__c
            LIMIT 1
        ];
    }

    private static Group getGroup() {
        // Assumption is that ordering them will return the same group for multiple seperate calls
        List<Group> allGroups = [
            SELECT Id, Type
            FROM Group 
            ORDER BY Id
            LIMIT 1
        ];
        return allGroups[0];
    }
}