public class MLRTriggerHandler{
	public static void updateValidMLR(List<MLR__c> mlrList,Map<Id,MLR__c> mlrMap){
		List<Opportunity> oppList = new List<Opportunity>();
		Set<Id> acIdSet = new Set<Id>();
		Set<Id> mlrIdSet = new Set<Id>();
		for(MLR__c mlr : mlrList){
			if(mlr.Status__c != mlrMap.get(mlr.id).Status__c){
				mlrIdSet.add(mlr.id);
			}
			
		}
		/*List<Account> acList = new List<Account>([select id,Valid_MLR__c,MLR_Id__r.MLR_Set_Date__c,MLR_Id__r.Expiry_Date__c from account where MLR_Id__c =: mlrIdSet]);
		for(Account acc : acList){
			if(acc.id != null){
				acIdSet.add(acc.id);
			}
			
		}*/
		List<MLR__c> listMLR = [select id,Status__c,Expiry_Date__c,MLR_Set_Date__c,(select id,Valid_MLR__c from Accounts__r) from MLR__c where id =: mlrIdSet];
		Map<Id,MLR__c> acMlrMap = new Map<Id,MLR__c>();
		for(MLR__c ml : listMLR){
			for(Account ac : ml.Accounts__r){
				acMlrMap.put(ac.id,ml);
			}
		}
		
		List<Opportunity> opList = [Select id,Valid_MLR__c,MLR_Set_Date__c,MLR_Expiry_Date__c,Invoicing_Company2__c,Invoicing_Care_Of__r.Reporting_Client__c from Opportunity where Invoicing_Company2__c =: acMlrMap.keyset() or Invoicing_Care_Of__r.Reporting_Client__c =: acMlrMap.keyset() ];
		
		
		for(MLR__c ml : mlrList){
			
			for(Opportunity op : opList){
				{	if(ml.Status__c == 'Pass'){
						op.Valid_MLR__c = true;
					}
					else{ 
						op.Valid_MLR__c = false;
					}
					op.MLR_Set_Date__c = ml.MLR_Set_Date__c;
					op.MLR_Expiry_Date__c =ml.Expiry_Date__c;
					oppList.add(op);
				}
			}
					
		}
		
		if(oppList!=NULL && !oppList.isEmpty()){
            update oppList;
		}
	}
}