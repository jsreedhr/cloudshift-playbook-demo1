public class ConfigurationManager{
    private static final ConfigurationManager instance = new ConfigurationManager();
    
    public static ConfigurationManager getInstance(){
        return instance;
    }

    private final Bypass_Configuration__c configurationInstance = Bypass_Configuration__c.getInstance();

    // this is necessary to be declared, even though it doesn't do anything just to enforce its private
    private ConfigurationManager(){
    }

    public Boolean isSharingOff(){
        return configurationInstance.Is_Sharing_Off__c;
    }

    public Boolean isSharingOn() {
        return !isSharingOff();
    }

    public void turnSharingOff() {
        configurationInstance.Is_Sharing_Off__c = true;
    }

    public void turnSharingOn() {
        configurationInstance.Is_Sharing_Off__c = false;
    }

    public void commitConfigurationChanges() {
        upsert configurationInstance;
    }
}