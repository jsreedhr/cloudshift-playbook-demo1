@isTest
private class TestAutoCompleteController{
    static void CreateAccounts(){
          //create some test account
        List<Account> accountList = new List<Account>();
        for(Integer i =0 ; i < 200 ; i++){
            accountList.add(new Account(Name='Test'+i,Accounts_ID__c='aID'+i,SicDesc='testtt'+i));
        }
        //insert test records
        insert accountList;
        Contact cont = new Contact();
        cont.AccountId = accountList[1].id;
        cont.LastName  = 'Test contact';
        insert cont;
        
        AccountContactRelation accobj = new AccountContactRelation();
        accobj.ContactId = cont.Id;
        accobj.AccountId = accountList[0].id;
        insert accobj;
        system.debug('----->acccc'+accountList);
    }
    
   static testMethod void testFindSObjects(){
       CreateAccounts();
       Test.startTest();                    
           autoCompleteController.findSObjects('Account','Test1','SicDesc,Accounts_ID__c','','Companystatus','True');           
           //System.assertEquals(10,autoCompleteController.findSObjects('Account','Test1','SicDesc,Accounts_ID__c','','','').size());
           //controller.setCacheField(null);
       Test.stopTest();
       
   }
   static testMethod void testFindSObjects1(){
       CreateAccounts();
       Test.startTest();                    
           autoCompleteController.findSObjects('Account','Test1','SicDesc,Accounts_ID__c','','Active','');           
           //System.assertEquals(10,autoCompleteController.findSObjects('Account','Test1','SicDesc,Accounts_ID__c','','','').size());
           //controller.setCacheField(null);
       Test.stopTest();
       
   }
    static testMethod void testFindSObjectsJM(){
       CreateAccounts();
       Test.startTest();                    
           autoCompleteControllerJM.findSObjects('Account','Test1','SicDesc,Accounts_ID__c','','','True');           
           //System.assertEquals(20,autoCompleteControllerJM.findSObjects('Account','Test1','SicDesc,Accounts_ID__c','','','').size());
           //controller.setCacheField(null);
       Test.stopTest();
       
   }
   static testMethod void testFindSObjectsJM_ARContact(){
       CreateAccounts();
       List<Account> lstofaccount = new List<Account>([SELECT Id from Account]);
       string accountId = '';
       if(lstofaccount.isEmpty()){
           accountId = lstofaccount[0].id;
       }
       
       Test.startTest();                    
           autoCompleteController_ARContact.findSObjects('Account','Tes','SicDesc,Accounts_ID__c','','AccountContactRelation','Contact',accountId);           
           //System.assertEquals(20,autoCompleteControllerJM.findSObjects('Account','Test1','SicDesc,Accounts_ID__c','','','').size());
           //controller.setCacheField(null);
       Test.stopTest();
       
   }
   static testMethod void testFindSObjectsJM_ARContact_sharing(){
       CreateAccounts();
       List<Account> lstofaccount = new List<Account>([SELECT Id from Account]);
       string accountId = '';
       if(lstofaccount.isEmpty()){
           accountId = lstofaccount[0].id;
       }
       
       Test.startTest();                    
           autoCompleteController_ARContact_sharing.findSObjects('Account','Tes','SicDesc,Accounts_ID__c','','AccountContactRelation','Contact',accountId);           
           //System.assertEquals(20,autoCompleteControllerJM.findSObjects('Account','Test1','SicDesc,Accounts_ID__c','','','').size());
           //controller.setCacheField(null);
       Test.stopTest();
       
   }
}