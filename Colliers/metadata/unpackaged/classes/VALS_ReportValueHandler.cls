public class VALS_ReportValueHandler{
    //for updating Internal Inspection Rep Values
    public static void UpdateExternalBCRRepValue(List<External_Inspection__c> ExInspecLst){
      String parkingStr;
      if(ExInspecLst.size()>0){
        for(External_Inspection__c EX: ExInspecLst)
        {
//Building Construction    
           if(EX.Wholly_Mainly__c!=null && EX.Partly__c==null)
            {
            EX.Building_Construction_RepValue__c='Entirely '+ EX.Wholly_Mainly__c+' construction';
            }
            else if(EX.Partly__c!=null && EX.Wholly_Mainly__c==null){
              String PartVal=EX.Partly__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Building_Construction_RepValue__c='Partly '+splitstr;
            }
             else if(EX.Wholly_Mainly__c!=null && EX.Partly__c!=null){
              String MainVal=EX.Wholly_Mainly__c;
              String PartVal=EX.Partly__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Mainly '+MainVal+', partly '+splitstr;
              EX.Building_Construction_RepValue__c= fullstr;
             }
              else if(EX.Wholly_Mainly__c==null && EX.Partly__c==null){
              EX.Building_Construction_RepValue__c=null;
             }
 
 //Front Elevation

        if(EX.Wholly_MainlyFront__c!=null && EX.PartlyFront__c==null)
        {
        EX.The_Front_Elevation_is_RepValue__c='Front elevation is entirely '+ EX.Wholly_MainlyFront__c;
        }
            else if(EX.PartlyFront__c!=null && EX.Wholly_MainlyFront__c==null){
              String PartVal=EX.PartlyFront__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.The_Front_Elevation_is_RepValue__c='Partly '+splitstr;
            }
             else if(EX.Wholly_MainlyFront__c!=null && EX.PartlyFront__c!=null){
              String MainVal=EX.Wholly_MainlyFront__c;
              String PartVal=EX.PartlyFront__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Front elevation is mainly '+MainVal+', partly '+splitstr;
              EX.The_Front_Elevation_is_RepValue__c= fullstr;
             }
             else if(EX.Wholly_MainlyFront__c==null && EX.PartlyFront__c==null){
              EX.The_Front_Elevation_is_RepValue__c=null;
             }
             
//Other Elevation

       if(EX.Wholly_MainlyOtherEve__c!=null && EX.PartllyOtherEle__c==null)
        {
        EX.Other_Elevations_are_RepValue__c='Other elevations are entirely '+ EX.Wholly_MainlyOtherEve__c;
        }
            else if(EX.PartllyOtherEle__c!=null && EX.Wholly_MainlyOtherEve__c==null){
              String PartVal=EX.PartllyOtherEle__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Other_Elevations_are_RepValue__c='Other elevations are partly '+splitstr;
            }
             else if(EX.Wholly_MainlyOtherEve__c!=null && EX.PartllyOtherEle__c!=null){
              String MainVal=EX.Wholly_MainlyOtherEve__c;
              String PartVal=EX.PartllyOtherEle__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Other elevations are mainly '+MainVal+', partly '+splitstr;
              EX.Other_Elevations_are_RepValue__c= fullstr;
             }
             else if(EX.Wholly_MainlyOtherEve__c==null && EX.PartllyOtherEle__c==null){
              EX.Other_Elevations_are_RepValue__c=null;
             }

//Roof

     if(EX.Wholly_MainlyRoof__c!=null && EX.PartllyRoof__c==null)
        {
        EX.The_Roof_is_RepValue__c='Entirely '+ EX.Wholly_MainlyRoof__c;
        }
            else if(EX.PartllyRoof__c!=null && EX.Wholly_MainlyRoof__c==null){
              String PartVal=EX.PartllyRoof__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.The_Roof_is_RepValue__c='Partly '+splitstr;
            }
             else if(EX.Wholly_MainlyRoof__c!=null && EX.PartllyRoof__c!=null){
              String MainVal=EX.Wholly_MainlyRoof__c;
              String PartVal=EX.PartllyRoof__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Mainly '+MainVal+', partly '+splitstr;
              EX.The_Roof_is_RepValue__c= fullstr;
             }
             else if(EX.Wholly_MainlyRoof__c==null && EX.PartllyRoof__c==null){
              EX.The_Roof_is_RepValue__c=null;
             }
             
 //covered

      if(EX.Wholly_MainlyCovered__c!=null && EX.PartlyConvered__c==null)
        {
        EX.Covered_in_RepValue__c='Covered in entirely '+ EX.Wholly_MainlyCovered__c;
        }
            else if(EX.PartlyConvered__c!=null && EX.Wholly_MainlyCovered__c==null){
              String PartVal=EX.PartlyConvered__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Covered_in_RepValue__c='Covered in partly '+splitstr;
            }
             else if(EX.Wholly_MainlyCovered__c!=null && EX.PartlyConvered__c!=null){
              String MainVal=EX.Wholly_MainlyCovered__c;
              String PartVal=EX.PartlyConvered__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Covered in mainly '+MainVal+', partly '+splitstr;
              EX.Covered_in_RepValue__c= fullstr;
             }
             else if(EX.Wholly_MainlyCovered__c==null && EX.PartlyConvered__c==null){
              EX.Covered_in_RepValue__c=null;
             }

//Windows

        if(EX.Wholly_MainlyWindows__c!=null && !(EX.PartlyWindows__c!=null))
        {
        EX.The_windows_are_Rep_Value__c=EX.Wholly_MainlyWindows__c+' throughout';
        }
            else if(EX.PartlyWindows__c!=null && EX.Wholly_MainlyWindows__c==null){
              String PartVal=EX.PartlyWindows__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.The_windows_are_Rep_Value__c='Partly '+splitstr;
            }
             else if(EX.Wholly_MainlyWindows__c!=null && EX.PartlyWindows__c!=null){
              String MainVal=EX.Wholly_MainlyWindows__c;
              String PartVal=EX.PartlyWindows__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Mainly '+MainVal+', partly '+splitstr;
              EX.The_windows_are_Rep_Value__c= fullstr;
             }
             else if(EX.Wholly_MainlyWindows__c==null && EX.PartlyWindows__c==null){
              EX.The_windows_are_Rep_Value__c=null;
             }
             
 // Frames
        if(EX.Wholly_MainlyFrames__c!=null && EX.PartlyFrames__c==null)
        {
        EX.Frames_are_RepValue__c=EX.Wholly_MainlyFrames__c+' throughout';
        }
            else if(EX.PartlyFrames__c!=null && EX.Wholly_MainlyFrames__c==null){
              String PartVal=EX.PartlyFrames__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Frames_are_RepValue__c='Partly '+splitstr;
            }
             else if(EX.Wholly_MainlyFrames__c!=null && EX.PartlyFrames__c!=null){
              String MainVal=EX.Wholly_MainlyFrames__c;
              String PartVal=EX.PartlyFrames__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Mainly '+MainVal+', partly '+splitstr;
              EX.Frames_are_RepValue__c= fullstr;
             } 
             else if(EX.Wholly_MainlyFrames__c==null && EX.PartlyFrames__c==null){
              EX.Frames_are_RepValue__c=null;
             }

//Stair Cases

        if(EX.Wholly_MainlyStaircases__c!=null && EX.PartlyStaircases__c==null)
        {
        EX.Staircases_RepValue__c='Staircases are entirely '+ EX.Wholly_MainlyStaircases__c;
        }
            else if(EX.PartlyStaircases__c!=null && EX.Wholly_MainlyStaircases__c==null){
              String PartVal=EX.PartlyStaircases__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Staircases_RepValue__c='Staircases are partly '+splitstr;
            }
             else if(EX.Wholly_MainlyStaircases__c!=null && EX.PartlyStaircases__c!=null){
              String MainVal=EX.Wholly_MainlyStaircases__c;
              String PartVal=EX.PartlyStaircases__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Staircases are mainly '+MainVal+', partly '+splitstr;
              EX.Staircases_RepValue__c= fullstr;
             }
             else if(EX.Wholly_MainlyStaircases__c==null && EX.PartlyStaircases__c==null){
              EX.Staircases_RepValue__c=null;
             }
             
//Loading Bays

     if(EX.Loading_Bays__c!=null)
        {
        String PartVal=EX.Loading_Bays__c;
        String splitstr=PartVal.replace(';', ' and ');
        EX.Loading_Bays_RepValue__c='Loading bays are '+splitstr;
            }
             else if(EX.Loading_Bays__c==null){
              EX.Loading_Bays_RepValue__c=null;
             }
             
//Parking

     if(EX.Parking_Type__c!=null)
        {
        String PartVal=EX.Parking_Type__c;
        String splitstr=PartVal.replace(';', ', ');
        parkingStr=splitstr.substring(0,1).toUpperCase()+ splitstr.substring(1);
        EX.Parking_type_RepValue__c=parkingStr;
            }
             else if(EX.Parking_Type__c==null){
              EX.Parking_type_RepValue__c=null;
             }

//Parking Spaces

     if(EX.No_Parking_Spaces__c!=null){
        EX.No_parking_spaces_RepValue__c=EX.No_Parking_Spaces__c+' parking spaces';
        }
    else if(EX.No_parking_spaces_RepValue__c==null){
            EX.No_parking_spaces_RepValue__c=null;
        }
             
//Parking Surface

        if(EX.Parking_Surface_Is__c!=null && EX.Parking_Surface_is_Partly__c==null)
        {
        EX.Parking_surface_RepValue__c='Entirely '+ EX.Parking_Surface_Is__c;
        }
            else if(EX.Parking_Surface_is_Partly__c!=null && EX.Parking_Surface_Is__c==null){
              String PartVal=EX.Parking_Surface_is_Partly__c;
              string splitstr=PartVal.replace(';', ', partly ');
              EX.Parking_surface_RepValue__c='Partly '+splitstr;
            }
             else if(EX.Parking_Surface_Is__c!=null && EX.Parking_Surface_is_Partly__c!=null){
              String MainVal=EX.Parking_Surface_Is__c;
              String PartVal=EX.Parking_Surface_is_Partly__c;
              string splitstr=PartVal.replace(';', ', partly ');
              String fullstr='Parking surface is mainly '+MainVal+', partly '+splitstr;
              EX.Parking_surface_RepValue__c= fullstr;
             }
             else if(EX.Parking_Surface_Is__c==null && EX.Parking_Surface_is_Partly__c==null){
              EX.Parking_surface_RepValue__c=null;
             }
//External Areas (Single)
        if(EX.External_Areas__c!=null){
            EX.External_Area_Single_RepValue__c= 'External area is '+EX.No_Parking_Spaces__c;
        }
        else if(EX.External_Areas__c==null){
            EX.External_Area_Single_RepValue__c=null;
        }
        
//External Areas (Multi)
         if(EX.External_Areas_Multi__c!=null){
            String PartVal=EX.External_Areas_Multi__c;
            String splitstr=PartVal.replace(';', ', ');
            String externalStr=splitstr.substring(0,1).toUpperCase()+ splitstr.substring(1);
            EX.External_Area_Multi_RepValue__c=parkingStr;
        }
        else if(EX.External_Areas_multi__c==null){
            EX.External_Area_Multi_RepValue__c=null;
        }
    }}}
}