@isTest 
public class TestAccountContactRelationBatch 
{
    static testMethod void CheckContactRelationship(){
        Account acc = new Account();
        acc.Name='test acc';
        insert acc;
        
        Account acc1 = new Account();
        acc1.name = 'testaccount1';
        acc1.ParentId=acc.id;
        insert acc1;
        
        Account acc2 = new Account();
        acc2.Name ='testaccount2';
        acc2.ParentId=acc1.Id;
        insert acc2;
       
        Account acc3 = new Account();
        acc3.Name ='testaccount2';
        acc3.ParentId=acc2.Id;
        insert acc3;
    
    
        Contact con = new Contact();
        con.lastname = 'testdata';
        con.AccountId = acc3.Id;
        insert con;
        
        AccountContactRelation acr1 = new AccountContactRelation();
        acr1.AccountId= acc2.id;
        acr1.contactid=con.id;
        insert acr1;
        
        
		
		
		
        Test.startTest();
            AccountContactRelationBatch obj = new AccountContactRelationBatch();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
}