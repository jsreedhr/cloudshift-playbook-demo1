public class CS_SiteLoginClass {
    public String Username{get;set;}
     public String Password{get;set;}

 public String loginMessage{get;set;}
public CS_SiteLoginClass()
{
    
}

public Pagereference loginApexPage()
{
    
    List <Site_User_Login__c> contLists = [Select id ,Login_Email__c, Password__c from Site_User_Login__c where Login_Email__c=:username   ];
   System.debug('Hormese '+contLists);
    System.debug(Username+'Hormese '+Password);
   if(contLists!=null && !contLists.isEmpty())
   {
    for(Site_User_Login__c cn : contLists)
    {
        if(cn.Password__c==Password)
        {
            String authcode= generateRandomString();
            Map<String, UserLogin__c> mcs = UserLogin__c.getAll();
            if(!mcs.containsKey(cn.id))
            
            {
                UserLogin__c us = new UserLogin__c();
            us.name=cn.id;
             us.Auth_Code__c=authcode;
             insert us;
            }
            else
            {
                authcode=mcs.get(cn.id).Auth_Code__c;
            }
          return new Pagereference('/apex/cs_MainJobLitePage?auth='+authcode);  
        }
        else{
            loginMessage='Incorrect Username/Password. Please contact your Admin to reset password';
            return null;
        }
        
    }
   }
    loginMessage='Incorrect Username/Password. Please contact your Admin to reset password';
    return null;
}

public static String generateRandomString() {
    Integer len=16;
    final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
    String randStr = '';
    while (randStr.length() < len) {
       Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
       randStr += chars.substring(idx, idx+1);
    }
    return randStr; 
}

}