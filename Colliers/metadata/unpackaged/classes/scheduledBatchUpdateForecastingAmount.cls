global class scheduledBatchUpdateForecastingAmount implements Schedulable  {
    global void execute(SchedulableContext SC) {
        BatchUpdateForecastingAmount objBatch = new BatchUpdateForecastingAmount();
        database.executeBatch(objBatch, 50);
    }
}