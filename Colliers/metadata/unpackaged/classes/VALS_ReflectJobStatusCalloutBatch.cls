global class VALS_ReflectJobStatusCalloutBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{

   global Database.QueryLocator start(Database.BatchableContext BC){
   
   List<Id> OppIds= new List<Id>();
   Date todaydate = Date.today();
  todaydate=todaydate.addDays(0);
   
   for(Invoice__C Inv:[select id,name,Opportunity__c from Invoice__C where Date_of_Invoice__c=:todaydate and Opportunity__r.Sent_to_ShareDo__c=True ]){
       OppIds.add(Inv.Opportunity__c);
   } 
   
   for(OpportunityFieldHistory Inv:[SELECT OpportunityId FROM OpportunityFieldHistory WHERE Field = 'StageName' And CreatedDate >= YESTERDAY]){
       OppIds.add(Inv.OpportunityId);
   } 
   
    DateTime Past24 = system.now().AddDays(-1);
    
     return Database.getQueryLocator([Select id,Name, job_number__c,StageName,Work_Category__c,Work_Purpose__c,Description,Date_Instructed__c,
          Property_Address__c,AccountId,InstructingCompanyAddress__c,
          Instructing_Contact__c,Manager__c
          from Opportunity where Sent_to_ShareDo__c=True AND Id IN :OppIds
            ]);



   }

     global void execute(Database.BatchableContext BC, List<Opportunity> scope){
     
     Map<string,ShareDo_Credentials__c> shareDo = ShareDo_Credentials__c.getAll();
    // List<Opportunity> OppList = [Select id,Name, job_number__c,StageName,Work_Category__c,Work_Purpose__c,Description,Date_Instructed__c,
    //       Property_Address__c,AccountId,InstructingCompanyAddress__c,
    //       Instructing_Contact__c,Manager__c
    //       from Opportunity
    //       where id IN:scope LIMIT 1];
    system.debug('---'+scope);
    List<Opportunity> OppList=scope;
    List<jobStatusesWrapper> jobstatusesList = new  List<jobStatusesWrapper>();
    
    For(Opportunity opp : OppList)
    {
        jobStatusesWrapper jostatus = new jobStatusesWrapper(opp.id, 'invoiced'); 
        jobstatusesList.add(jostatus);
    }
    jobStatusesListWrap jobwrap = new jobStatusesListWrap(jobstatusesList);
    String jsonData; 
    String responsestring;
    String apiKey = EncodingUtil.base64Encode(Blob.valueOf(shareDo.get('Shared URL Job Update').API_Key__c));
    String authorizationHeader = 'bearer '+apiKey;
    //String username = shareDo.get('Shared URL Job Update').ShareDo_Username__c;
    String username = 'EU\\svc_sharedoAPI';
    String password = shareDo.get('Shared URL Job Update').ShareDo_Password__c;
    Blob headerValue = Blob.valueOf(username + ':' + password);
    String authorizationHeaderBasic = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
 
    jsonData = JSON.serialize(jobwrap);
    System.debug('Le JSON: '+jsonData);
    HttpRequest req = new HttpRequest();
    req.setMethod('PUT');
    req.setHeader('Content-Type','application/json');
    req.setHeader('Accept','application/json');
    req.setHeader('Authorization', authorizationHeaderBasic);
    req.setHeader('X-Authorization', authorizationHeader);
    req.setBody(jsonData);
    System.debug('Le AuthKey: '+req.getHeader('Authorization'));   
    req.setEndpoint(shareDo.get('Shared URL Job Update').shareDoUrl__c);
    System.debug('Le JSONEnd: '+shareDo.get('Shared URL Job Update').shareDoUrl__c);
    try{
      Http http = new Http();
      HTTPResponse res = http.send(req);
      responseString = res.getBody();
       Vals__c log   = new Vals__c();
        log.Response_String__c = responseString; 
       log.JsonData__c = jsonData;
      insert log ;
       System.debug('log---------->' +log);
      System.debug('ShareDo Response:' + res.getbody());
    }catch(Exception e)
    {
      System.Debug('Exception on Call out: '+e);
    }    
    //System.Debug('Data Error: '+dataValidationError);               
    
  }
     global void finish(Database.BatchableContext BC){
    }
    
    public class jobStatusesWrapper
    {
        public string jobReference;
        public string salesforceStatus;
        public jobStatusesWrapper(string jobref, string salesforcstatus)
        {
            jobReference= jobref;
            salesforceStatus= salesforcstatus;
            
            
        }
    }
      public class jobStatusesListWrap
      {
         public List<jobStatusesWrapper> jobStatuses;
         
         public jobStatusesListWrap(List<jobStatusesWrapper> jobStatuses )
         {
             this.jobStatuses=jobStatuses;
         }
      }
   }