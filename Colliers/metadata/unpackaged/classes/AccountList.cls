public with sharing class AccountList {
public list<Account> objAccounts {get;set;}
public List<SelectOption> Countries{get;set;}
public List<SelectOption> cities{get;set;}
public string country{get;set;}
public string city{get;set;}
    public AccountList(){
        Countries = new List<SelectOption>();
        cities = new List<SelectOption>();
        country = '';
        city = '';
        try{
            objAccounts = new list<Account>();  
            objAccounts.addAll([SELECT Name,BillingStreet,BillingCity,BillingPostalCode,
                                BillingCountry FROM Account 
                                Where BillingCountry <> null limit 20]);
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error:'
                                                        +ex.getMessage()));
        }
        set<string> setOfCountry= new set<string>();
        set<string> setOfCity = new set<string>();
        
        if(objAccounts != null && objAccounts.size()>0){
                for(Account objaccount : objAccounts){
                    setOfCountry.add(objaccount.BillingCountry);
                    setOfCity.add(objaccount.BillingCity);
                    //Countries.add(new SelectOption(objaccount.BillingCountry,objaccount.BillingCountry));
                    //cities.add(new SelectOption(objaccount.BillingCity,objaccount.BillingCity));
                }
                for(string objstring:setOfCountry){
                        Countries.add(new SelectOption(objstring,objstring));
                }
                for(string objstring:setOfCity){
                        cities.add(new SelectOption(objstring,objstring));
                }
        }
        system.debug('---cities'+cities);
        system.debug('---Countries'+Countries);
    }
    public void filterData(){
            if(objAccounts != null && objAccounts.size()>0){
                for(Integer j = 0; j < objAccounts.size(); j++){
                    if((objAccounts.get(j).BillingCountry != null && country != null) || (objAccounts.get(j).BillingCity != null && city != null)){
                        objAccounts.remove(j);
                    }
                    
                }
            }
    }
}