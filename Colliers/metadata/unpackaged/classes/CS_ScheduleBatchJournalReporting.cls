global class CS_ScheduleBatchJournalReporting implements Schedulable  {
    global void execute(SchedulableContext SC) {
        CS_BatchJournalReporting objBatch = new CS_BatchJournalReporting();
        database.executeBatch(objBatch, 100);
    }
}