/**
*  Class Name: Test_FeeIncomeStagingTableBatchProcess  
*  Description: This is a Test Class for testing Fee Income Staging Table Batch process  
*  Company: dQuotient
*  CreatedDate:29/12/2016 
*
*/
@isTest
private class Test_FeeIncomeStagingTableBatchProcess{
    
    static Account objAccount1;
    static Opportunity oppObj1;
    static Opportunity oppObj2;
    static Contact contactObj;
    static Invoice__c invoiceObj1;
    static Invoice__c invoiceObj2;
    static User userObj1;
    static User userObj2;
    static Staff__c staffObj1;
    static Staff__c staffObj2;
    static Allocation__c allocationObj1;
    static Allocation__c allocationObj2;
    static Allocation__c allocationObj3;
    static Property__c propertyObj;
    static Purchase_Order__c purOrderObj;
    static Job_Property_Junction__c jobPropObj;
    static Forecasting__c forecastObj1;
    static Forecasting__c forecastObj2;
    static Forecasting__c forecastObj3;
    static Disbursements__c disbursementObj;

    static void createData()
    {
         // create user
        userObj1 = TestObjectHelper.createAdminUser(true);
        userobj1.UserName ='Test_123@dquotient.com';
        userobj1.Email = 'abc@gmail.com';
        userobj1.External_Id_Email__c = '1213';
        insert userObj1;
        System.runAs(userObj1){
        // create user 2
        userObj2 = TestObjectHelper.createAdminUser(true);
        userobj2.UserName ='Test_12311@dquotient.com';
        userobj2.Email ='abc123@gmail.com';
        userobj2.External_Id_Email__c= '222';
        //insert userObj2;
        
        Account acc = TestObjectHelper.createAccount();
        acc.PO_Required__c = 'No';
        acc.Company_Status__c = 'Active';
        insert acc;
        
        Account acc1 = TestObjectHelper.createAccount();
        acc1.PO_Required__c = 'No';
        acc1.ParentId = acc.Id;
        acc1.Company_Status__c = 'Active';
        insert acc1;
        
        objAccount1 = TestObjectHelper.createAccount();
        objAccount1.PO_Required__c = 'No';
        objAccount1.ParentId = acc1.Id;
        objAccount1.Company_Status__c = 'Active';
        
        insert objAccount1;
        System.assert(objAccount1.id!=NULL);

       
        
        
        //create staff
        staffObj1 = TestObjecthelper.createStaff();
        staffObj1.User__c = userObj1.id;
        staffObj1.Active__c = true;
        insert staffObj1;
        
        //create staff
        staffObj2 = TestObjecthelper.createStaff();
        staffObj2.User__c = userobj1.id;
        staffObj2.Active__c = true;
        //insert staffObj2;
        
        
        // create worktype 
        Work_Type__c workObj = new Work_Type__c();
        workObj.Name = 'Valuation - Redbook';
        workObj.Active__c = true;
        insert workObj;
        
        //create coding structure
        Coding_Structure__c codobj1 = new Coding_Structure__c();
        codobj1.Staff__c = staffObj1.Id;
        codobj1.Work_Type__c = workObj.Id;
        codobj1.Department__c = 'Accurates';
        codobj1.Office__c = 'London - West End';
        codobj1.Status__c = 'Active';
        codobj1.Department_Cost_Code_Centre__c = '620';
        insert codobj1;
        
        //create coding structure
        Coding_Structure__c codobj2 = new Coding_Structure__c();
        codobj2.Staff__c = staffObj1.Id;
        codobj2.Work_Type__c = workObj.Id;
        codobj2.Department__c = 'Accurates';
        codobj2.Office__c = 'London - West End';
        codobj2.Status__c = 'Active';
        codobj2.Department_Cost_Code_Centre__c = '620';
        insert codobj2;
        
        
        Address__c addressObj = TestObjectHelper.createAddress(objAccount1);
        insert addressObj;
      
        //create Opportunity
        oppObj1 = TestObjectHelper.createOpportunity(objAccount1);
        oppObj1.Invoicing_Company2__c = objAccount1.id;
        oppObj1.Invoicing_Address__c = addressObj.id;
        oppObj1.Coding_Structure__c = codobj1.Id;
        insert oppObj1;
        System.assert(oppObj1.id!=NULL);
        
        
        oppObj2 = TestObjectHelper.createOpportunity(objAccount1);
        oppObj2.Invoicing_Company2__c = objAccount1.id;
        oppObj2.Invoicing_Address__c = addressObj.id;
        oppObj2.Coding_Structure__c = codobj2.Id;
        oppObj2.Manager__c = staffObj1.id;
        insert oppObj2;
        System.assert(oppObj2.id!=NULL);
        
        Account_Address_Junction__c accAddresjn = TestObjecthelper.createAccountAddressJunction(objAccount1,addressObj);
        insert accAddresjn;

        contactObj = TestObjectHelper.createContact(objAccount1);
        contactobj.lastName='Test';
        contactobj.Email = 'test@test.com';
        insert contactobj;
        System.assert(contactobj.id!=NULL);
        
        allocationObj1 = TestObjectHelper.createAllocation(oppObj1,staffObj1);
        allocationObj1.Coding_Structure__c = codobj1.Id;
        insert allocationObj1;
        System.assert(allocationObj1.id!=NULL);
        
        allocationObj2 = TestObjectHelper.createAllocation(oppObj1,staffObj1);
        allocationObj2.Coding_Structure__c = codobj2.Id;
        
        insert allocationObj2;
        System.assert(allocationObj2.id!=NULL);
        
        allocationObj3 = TestObjectHelper.createAllocation(oppObj1,staffObj1);
        allocationObj3.externalCompany__c = objAccount1.id;
        insert allocationObj3;
        System.assert(allocationObj3.id!=NULL);
        
        purOrderObj = TestObjecthelper.createPurchaseOrder(oppObj1);
        purOrderObj.PO_Status__c = 'Original';
        purOrderObj.Amount__c =1000;
        purOrderObj.Used_Amount__c=500;
        insert purOrderObj;
        System.assert(purOrderObj.id!=NULL);
        
        forecastObj1 = TestObjecthelper.createForecast(allocationObj1);
        forecastObj1.CS_Forecast_Date__c = system.today().toStartOfMonth();
        forecastObj1.Amount__c = 1000;
        insert forecastObj1;
        
        forecastObj2 = TestObjecthelper.createForecast(allocationObj2);
        forecastObj2.CS_Forecast_Date__c = system.today().toStartOfMonth();
        forecastObj2.Amount__c = 1000;
        insert forecastObj2;
        
        forecastObj3 = TestObjecthelper.createForecast(allocationObj3);
        forecastObj3.CS_Forecast_Date__c = system.today().toStartOfMonth();
        forecastObj3.Amount__c = 1000;
        insert forecastObj3;
        
        invoiceObj1 = TestObjecthelper.createInvoice(oppObj1,contactObj);
        invoiceObj1.status__c ='Approved';
        invoiceObj1.Invoice_Wording__c = 'testWord';
        insert invoiceObj1;
        
        invoiceObj2 = TestObjecthelper.createInvoice(oppObj1,contactObj);
        invoiceObj2.status__c ='Approved';
        invoiceObj2.Invoice_Wording__c = 'testWord';
        insert invoiceObj2;
        
        Invoice_Allocation_Junction__c invall = TestObjecthelper.createInvoiceAllocationJunction(allocationObj1,invoiceObj1);
        invall.Forecasting__c = forecastObj1.id;
        invall.Amount__c = 50;
        insert invall;
        
        Invoice_Allocation_Junction__c invall1 = TestObjecthelper.createInvoiceAllocationJunction(allocationObj2,invoiceObj1);
        invall1.Forecasting__c = forecastObj2.id;
        invall1.Amount__c = 50;
        insert invall1;
        
        Invoice_Allocation_Junction__c invall2 = TestObjecthelper.createInvoiceAllocationJunction(allocationObj3,invoiceObj1);
        invall2.Forecasting__c = forecastObj3.id;
        invall2.Amount__c = 50;
        insert invall2;
        }

    }
    
    static testMethod void testInvoice() {
        
        createData();
        System.runAs(userObj1){
        Test.startTest();
            
            CS_ScheduleBatchInvoiceReporting  objScheduler = new CS_ScheduleBatchInvoiceReporting();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test', sch, objScheduler); 
        Test.stopTest();
        }
        
    }

    static testMethod void testAccural() {
        
        createData();
        System.runAs(userObj1){
        AssignAccountId__c objAssignAccount = new AssignAccountId__c();
        objAssignAccount.Name = 'test';
        objAssignAccount.AccountNumber__c = 123;
        objAssignAccount.Account_Pattern__c = '123';
        insert objAssignAccount;
        Accrual__c acc =TestObjectHelper.createAccural(forecastObj1);
        acc.Amount__c = 7000;
        acc.Month_To_Accrue__c = system.today();
        acc.Reason__c = 'Test Reason';
        acc.Job__c = oppObj1.id;
        acc.Is_Engaged_with_Client__c = true;
        acc.IsPrice__c= true;
        acc.IsDeliverySer__c= true;
        acc.Status__c = 'Approved';
        insert acc;
        
        //create Accrual_Forecasting_Junction__c object
        Accrual_Forecasting_Junction__c   accFrcst = TestObjectHelper.createAccrualForecastJunction(acc,forecastObj1);
        accFrcst.Amount__c = forecastObj1.Amount__c;
        accFrcst.Forecasting__c = forecastObj1.id;
        accFrcst.Accrual__c = acc.id;
        insert accFrcst;
        
        Test.startTest();
            
            CS_ScheduleBatchAccrualReporting  objScheduler = new CS_ScheduleBatchAccrualReporting();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test', sch, objScheduler); 
        Test.stopTest();
        }
        
    }
    
    
    static testMethod void testCreditNote() {
        
        createData();
            System.runAs(userObj1){
            Credit_Note__c  objCreditNote = TestObjectHelper.createCreditNote(invoiceObj1);
            objCreditNote.Status__c= 'Approved';
            insert objCreditNote; 
            
        Test.startTest();
            
            CS_ScheduleBatchCreditNoteReporting  objScheduler = new CS_ScheduleBatchCreditNoteReporting();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test', sch, objScheduler); 
        Test.stopTest();
        }
        
    }
    
    
    static testMethod void testJournal() {
        
        createData();
        System.runAs(userObj1){
            List<Journal__c> lstJournal = new List<Journal__c>();
            Journal__c journalObj1 = new Journal__c();
            journalObj1.Allocation__c = allocationObj1.Id;
            journalObj1.Allocation_Value__c = -100;
            journalObj1.Transaction_Id__c= '1';
            journalObj1.Status__c = 'Raised';
            journalObj1.Invoice__c= invoiceObj1.id;
            journalObj1.Type__c= 'I-I';
            lstJournal.add(journalObj1);
            
            Journal__c journalObj2 = new Journal__c();
            journalObj2.Allocation__c = allocationObj2.Id;
            journalObj2.Allocation_Value__c = 100;
            journalObj2.Status__c = 'Raised';
            journalObj2.Transaction_Id__c= '1';
            journalObj2.Invoice__c= invoiceObj1.id;
            journalObj2.Type__c= 'I-I';
            lstJournal.add(journalObj2);
            
            Journal__c journalObj3 = new Journal__c();
            journalObj3.Allocation__c = allocationObj1.Id;
            journalObj3.Allocation_Value__c = -100;
            journalObj3.Transaction_Id__c= '1';
            journalObj3.Invoice__c= invoiceObj1.id;
            journalObj3.Status__c = 'Raised';
            journalObj3.Type__c= 'I-E';
            lstJournal.add(journalObj3);
            
            Journal__c journalObj4 = new Journal__c();
            journalObj4.Allocation__c = allocationObj3.Id;
            journalObj4.Allocation_Value__c = 100;
            journalObj4.Status__c = 'Raised';
            journalObj4.Transaction_Id__c= '1';
            journalObj4.Invoice__c= invoiceObj1.id;
            journalObj4.Type__c= 'I-E';
            lstJournal.add(journalObj4);
            
            Journal__c journalObj5 = new Journal__c();
            journalObj5.Allocation__c = allocationObj3.Id;
            journalObj5.Allocation_Value__c = -100;
            journalObj5.Transaction_Id__c= '2';
            journalObj5.Status__c = 'Raised';
            journalObj5.Invoice__c= invoiceObj2.id;
            journalObj5.Type__c= 'E-E';
            lstJournal.add(journalObj5);
            
            Journal__c journalObj6 = new Journal__c();
            journalObj6.Allocation__c = allocationObj3.Id;
            journalObj6.Allocation_Value__c = 100;
            journalObj6.Transaction_Id__c= '2';
            journalObj6.Status__c = 'Raised';
            journalObj6.Invoice__c= invoiceObj2.id;
            journalObj6.Type__c= 'E-E';
            lstJournal.add(journalObj6);
            
            Journal__c journalObj7 = new Journal__c();
            journalObj7.Allocation__c = allocationObj2.Id;
            journalObj7.Allocation_Value__c = 100;
            journalObj7.Transaction_Id__c= '3';
            journalObj7.Status__c = 'Cancelled';
            journalObj7.Cancelled_By_CreditNote__c = true;
            journalObj7.Reverse_Journal_created__c = false;
            journalObj7.Invoice__c= invoiceObj2.id;
            journalObj7.Type__c= 'E-I';
            lstJournal.add(journalObj7);
            
            
            Journal__c journalObj8 = new Journal__c();
            journalObj8.Allocation__c = allocationObj3.Id;
            journalObj8.Allocation_Value__c = -100;
            journalObj8.Transaction_Id__c= '3';
            journalObj8.Status__c = 'Cancelled';
            journalObj8.Cancelled_By_CreditNote__c = true;
            journalObj8.Reverse_Journal_created__c = false;
            journalObj8.Invoice__c= invoiceObj2.id;
            journalObj8.Type__c= 'E-I';
            lstJournal.add(journalObj8);
            
            insert lstJournal;
            
            
        Test.startTest();
            
            CS_ScheduleBatchJournalReporting  objScheduler = new CS_ScheduleBatchJournalReporting();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test', sch, objScheduler); 
            
            Journal__c journalObj9 = new Journal__c();
            journalObj9.Allocation__c = allocationObj3.Id;
            journalObj9.Allocation_Value__c = -100;
            journalObj9.Transaction_Id__c= '3';
            journalObj9.Status__c = 'Cancelled';
            journalObj9.Cancelled_By_CreditNote__c = true;
            journalObj9.Reverse_Journal_created__c = false;
            journalObj9.Invoice__c= invoiceObj2.id;
            journalObj9.Type__c= 'E-I';
            
            
            insert journalObj9;
            
            CS_ScheduleBatchJournalReporting  objScheduler1 = new CS_ScheduleBatchJournalReporting();
            String sch1 = '0 0 23 * * ?'; 
            system.schedule('Test 2', sch1, objScheduler1); 
            
        Test.stopTest();
        }
        
    }
    
}