/**
 * @author     Nidheesh
 * @date       Jan 4,2018
 * @Custom Exception Class to use in all Projects
 */
public with sharing class CustomException extends Exception {

}