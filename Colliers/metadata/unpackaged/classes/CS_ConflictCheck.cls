// ===========================================================================
// Component: CS_ConflictCheck
//    Author: Nidheesh N
// Copyright: 2018 by Dquotient
//   Purpose: Controller to for ConflictCheckPage.
// ===========================================================================
// ChangeLog: 2016-11-17 Nidheesh Initial version.
//            
// ===========================================================================
public without sharing class CS_ConflictCheck {
    public transient List<Opportunity> mainOpportunity {get;set;} // Parent Opportunity Details
    public transient List<SelectOption> otherPartySelection{get;set;} // List of OtherParty associated with the instructed company of parent job
    public transient List<SelectOption> companySearchOptions{get;set;} // List of values which were used in company filter picklist           
    public transient List<SelectOption> propertySearchOptions {get;set;} // List of properties associated with job        
    public transient List<SelectOption> instrDateSerachOptions {get;set;} // 'Instructed Since' picklist values     
    public transient List<SelectOption> propertyTypeSearchOptions {get;set;} // PropertyType picklist values (picklist values used in property tyep field in salesforce)     
    public transient List<SelectOption> radiusSearchOptions {get;set;} // Radius values used for search filter.
    public transient List<SelectOption> radioSelectOptions {get;set;} // Radio button select option values.
    public transient FilterWrapper filterObj {get;set;} // to store the filterValues
    public transient Map<Id,Map<Id,Set<Id>>> mapOfJobToProperties;
    // Default Constructor.
    public CS_ConflictCheck(){
         
    }
    
    /**
    *  Method Name: getConflicts
    *  Description: Method to search for conflicts and create a list.
    *  Param:  None
    *  Return: None
    */ 
    
    public void getConflicts(){
        // Get the Opportunity record Id from URL
        String oppId = ApexPages.currentPage().getParameters().get('id');
        System.debug('Opportunity id------>'+oppId);
        if(oppId != null && String.isNotBlank(oppId)){
            if(oppId instanceOf Id){
                // Get all details of Opportunity along with details of property childs and OtherPart childs.
                mainOpportunity = new List<Opportunity>();
                mainOpportunity = new CS_ConflictCheckServiceCls().getOpportunityWithChilds(oppId);
                if(mainOpportunity != null && !mainOpportunity.isEmpty()){
                    // call the function to set the filter picklist values of page.
                    setFilterValues(mainOpportunity);
                    // Build query to get conflicted jobs.
                    getConflictedJobs(mainOpportunity[0]);
                }else{
                    throw new CustomException('CS_ConflictCheck.getConflicts: Unable to find the Opportunity Reocrds related to this id');
                }
            }
        }else{
            throw new CustomException('CS_ConflictCheck.getConflicts: Must provide in Opportunity Id in URL');
        }
    }
    
    /**
    *  Method Name: setFilterValues
    *  Description: Method to set the filter values in page.
    *  Param:  None
    *  Return: None
    */ 
    public void setFilterValues(List<Opportunity> mainJob){
        // Initialize the selectOption values
        companySearchOptions = new List<SelectOption>();
        instrDateSerachOptions = new List<SelectOption>();
        propertySearchOptions = new List<SelectOption>();
        propertyTypeSearchOptions = new List<SelectOption>();
        radiusSearchOptions = new List<SelectOption>();
        // get the radius picklist values
        List<String> radiusValues = Label.ConflictMonthValues.split(CoreConstants.DELIMITER_COMMA);
        List<String> instrCompanyOptions = Label.ConflictComanyFilterOptions.split(CoreConstants.DELIMITER_COMMA);
        // create instructing month selectOption list
        if(!radiusValues.isEmpty()){
            for(String objStr :radiusValues){
                instrDateSerachOptions.add(new SelectOption(objStr,objStr+ConflictCheckConstantCls.CONFLICT_MONTH_CONSTANT));
            }
        }
        // create company filter selectOption list
        if(!instrCompanyOptions.isEmpty()){
            for(String objString :instrCompanyOptions){
                companySearchOptions.add(new SelectOption(objString,objString));
            }
        }
        // Create property search option picklist
        if(mainJob != null && !mainJob.isEmpty() && mainJob[0].Job_Property_Junction__r != null && !mainJob[0].Job_Property_Junction__r.isEmpty()){
            integer i = 0;
            if(!mainJob[0].Job_Property_Junction__r.isEmpty()){
                for(Job_Property_Junction__c jobPro :mainJob[0].Job_Property_Junction__r){
                    if(i<98){
                        propertySearchOptions.add(new SelectOption(jobPro.Property__r.Id,jobPro.Property__r.Name));
                        i++;
                    }
                }
                propertySearchOptions.add(new SelectOption(ConflictCheckConstantCls.CONFLICT_PICKLIST_ALL,ConflictCheckConstantCls.CONFLICT_PICKLIST_ALL));
                propertySearchOptions.add(new SelectOption(ConflictCheckConstantCls.CONFLICT_PICKLIST_NO_PROPERTY,ConflictCheckConstantCls.CONFLICT_PICKLIST_NO_PROPERTY));
            }
        }
        else{
            propertySearchOptions.add(new SelectOption(ConflictCheckConstantCls.CONFLICT_PICKLIST_NONE,ConflictCheckConstantCls.CONFLICT_PICKLIST_NONE,true));
        }
        // Create Property Type pick list options
        Schema.DescribeFieldResult fieldResult = Property__c.Property_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        if(!ple.isEmpty()){
            for( Schema.PicklistEntry f : ple)
            {
                propertyTypeSearchOptions.add(new SelectOption(f.getLabel(), f.getValue()));
            }
            propertyTypeSearchOptions.add(new SelectOption(ConflictCheckConstantCls.CONFLICT_PICKLIST_ALL,ConflictCheckConstantCls.CONFLICT_PICKLIST_ALL));            
        }else{
             
              propertyTypeSearchOptions.add(new SelectOption(ConflictCheckConstantCls.CONFLICT_PICKLIST_NONE,ConflictCheckConstantCls.CONFLICT_PICKLIST_NONE,true));
        }
        // Create radius picklist options
        if(!ConflictCheckConstantCls.CONFLICT_RADIUS_VALUE_MAP.isEmpty()){
            for(String strObj :ConflictCheckConstantCls.CONFLICT_RADIUS_VALUE_MAP.Keyset()){
                radiusSearchOptions.add(new SelectOption(strObj,ConflictCheckConstantCls.CONFLICT_RADIUS_VALUE_MAP.get(strObj)));
            }
        }
        System.debug('radiusSearchOptions-------->'+radiusSearchOptions);
    }
    
    /**
    *  Method Name: getConflictedJobs
    *  Description: Method to get conflicted Jobs.
    *  Param:  Opportunity
    *  Return: List<Opportunity>
    */ 
    public void getConflictedJobs(Opportunity mainJob){/*
        Id UserId = UserInfo.getUserId();
        filterObj = new FilterWrapper();
        CS_ConflictCheckServiceCls con = new CS_ConflictCheckServiceCls();
        User_ConflictCheck_Info__c userInfoFilter = con.getLastFilterValues(UserId,mainJob.Id);
        if(userInfoFilter != null && userInfoFilter.Id != null){
            if(userInfoFilter.Instructed_Date_Range__c != null)
                filterObj.instrMonthValue = String.valueOf(userInfoFilter.Instructed_Date_Range__c);
            else
                filterObj.instrMonthValue='24';
            if(userInfoFilter.Property_Info__c != null)
                filterObj.propertyValue = String.valueOf(userInfoFilter.Property_Info__c);
            else
                filterObj.propertyValue = 'All';
            if(userInfoFilter.Radius__c != null)
                filterObj.radiusValue = String.valueOf(userInfoFilter.Radius__c);
            else
                filterObj.radiusValue ='0';
            if(userInfoFilter.Company_Filter_Value__c != null)
                filterObj.companyValue = String.valueOf(userInfoFilter.Company_Filter_Value__c);
            else
                filterObj.companyValue = 'None';
            /*if(userInfoFilter.OtherPartyId__c != null)
                filterObj.otherPartyValue = String.valueOf(userInfoFilter.OtherPartyId__c);
            else
                filterObj.otherPartyValue = 'None';
            if(userInfoFilter.Property_Type_Filter_Value__c != null)
                filterObj.propertyTypeValue = String.valueOf(userInfoFilter.Property_Type_Filter_Value__c);
            else
                filterObj.propertyTypeValue = 'All'; 
        }else{
            filterObj.instrMonthValue='24';
            filterObj.propertyValue = 'All';
            filterObj.radiusValue ='0';
            filterObj.companyValue = 'None';
            filterObj.otherPartyValue = 'None';
            filterObj.propertyTypeValue = 'All';
        }
        buildquery(filterObj,mainJob);
        return null;
    */}
    /**
    *  Method Name: buildquery
    *  Description: build query string to get the conflicted records.
    *  Param:  Opportunity
    *  Return: List<Opportunity>
    */ 
    public String buildquery(FilterWrapper filterObj,Opportunity jobDetails){
        if(jobDetails != null && jobDetails.Id != null){
            mapOfJobToProperties = new Map<Id,Map<Id,Set<Id>>> ();
            // initialize query string
            String queryString = 'Select id,Name,';
            // get all the jobproperties in the radius level --- getJobswithinDistance
            mapOfJobToProperties = getJobsPropertiesMap(filterObj,jobDetails);
            
        }
        return null;
    }
    
    /**
    *  Method Name: getJobswithinDistance
    *  Description: Method to get the Jobs within the distance
    *  Param:  FilterWrapper , main job details
    *  Return: Set<Id> ----Set of jobIds
    */ 
    public  Map<Id,Map<Id,Set<Id>>> getJobsPropertiesMap(FilterWrapper filterObj,Opportunity jobDetails){
         Map<Id,Map<Id,Set<Id>>> jobToPropertiesMap;
        if(jobDetails != null && jobDetails.Id != null){
            if(!jobDetails.Job_Property_Junction__r.isEmpty() && filterObj.propertyValue != 'No Property'){
                
            }
        }
        return jobToPropertiesMap;
    }
    /**
     * Description: WrapperClass to store the filterValues
    **/
    public class FilterWrapper{
        public transient String radiusValue {get;set;}
        public transient String propertyValue {get;set;}
        public transient String companyValue {get;set;}
        public transient String instrMonthValue {get;set;}
        public transient String otherPartyValue {get;set;}
        public transient String propertyTypeValue {get;set;}
    }
    
}