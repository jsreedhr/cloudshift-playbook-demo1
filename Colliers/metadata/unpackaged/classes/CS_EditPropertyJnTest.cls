@isTest
private class CS_EditPropertyJnTest {
   
    Public static Account a;
    static Contact c;
    static opportunity opp;
    static Property__c prpty;
   
    
     static testmethod void setupData(){
        //create account
        a = TestObjectHelper.createAccount();
        insert a ;
        
        prpty=TestObjectHelper.createProperty();
        prpty.Street_No__c ='Park Lane';
        prpty.Post_Code__c ='W1K 3DD';
        prpty.Address_Validation_Status__c = 'Verified';
        prpty.Country__c='United Kingdom';
        prpty.Geolocation__Latitude__s=51.51;
        prpty.Geolocation__Longitude__s=-0.15;
        insert prpty;
        
        
   
        
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
        
        opp = [Select id from opportunity Limit 1];
        
      
      
    }
    private static testMethod void test1() {
         setupData();
        opp = [Select id from opportunity Limit 1];
       
        Test.setCurrentPageReference(new PageReference('Page.CS_PropertyJnView'));

        System.currentPageReference().getParameters().put('id', opp.id);
        System.currentPageReference().getParameters().put('Selids', prpty.id);
        
        Test.startTest();
        
        CS_EditProproprtyJunction ctrl = new CS_EditProproprtyJunction();  
        ctrl.SelectedProperty();
        ctrl.SaveRecords();
        
       /* CS_EditInvoiceController.booleanCostWrapper cstWrap= new CS_EditInvoiceController.booleanCostWrapper(true,Cost);
        CS_EditInvoiceController.booleanAllocationeWrapper allocWrap  = new CS_EditInvoiceController.booleanAllocationeWrapper(true,alloc,100.00,20000.00);
        CS_EditInvoiceController.CostCategoryWrapper cstCategory = new CS_EditInvoiceController.CostCategoryWrapper(true,'test',10.00,5.00,70.00,'testRecord');
        CS_EditInvoiceController.booleanInvoiceWrapper invWrap = new CS_EditInvoiceController.booleanInvoiceWrapper(true,po);*/
        
       
        Test.stopTest();


    }
     private static testMethod void test2() {
         setupData();
        opp = [Select id from opportunity Limit 1];
       
        Test.setCurrentPageReference(new PageReference('Page.CS_PropertyJnView'));

        System.currentPageReference().getParameters().put('id', a.id);
        System.currentPageReference().getParameters().put('Selids', prpty.id);
        
        Test.startTest();
        
        CS_EditProproprtyJunction ctrl = new CS_EditProproprtyJunction(); 
        //ctrl.SlctedIds=prpty.id;
        ctrl.SelectedProperty();
        ctrl.SaveRecords();
        
       /* CS_EditInvoiceController.booleanCostWrapper cstWrap= new CS_EditInvoiceController.booleanCostWrapper(true,Cost);
        CS_EditInvoiceController.booleanAllocationeWrapper allocWrap  = new CS_EditInvoiceController.booleanAllocationeWrapper(true,alloc,100.00,20000.00);
        CS_EditInvoiceController.CostCategoryWrapper cstCategory = new CS_EditInvoiceController.CostCategoryWrapper(true,'test',10.00,5.00,70.00,'testRecord');
        CS_EditInvoiceController.booleanInvoiceWrapper invWrap = new CS_EditInvoiceController.booleanInvoiceWrapper(true,po);*/
        
       
        Test.stopTest();


    }
}