/**
 *  Class Name: test_csallocationController 
 *  Description: Test class for the controller for allocationPage
 *  Company: dQuotient
 *  CreatedDate: 13/10/2016 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nidheesh               13/10/2016                  Orginal Version          
 *
 */
@isTest
private class test_csallocationController {

 @testsetup
    static void setupData(){
        //create account
        Account a = TestObjectHelper.createAccount();
        insert a;
        Schema.DescribeFieldResult fieldResult = Coding_Structure__c.Department__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        Schema.DescribeFieldResult fieldResultofffice = Coding_Structure__c.Office__c.getDescribe();
        List<Schema.PicklistEntry> pleoffice = fieldResultofffice.getPicklistValues();
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
        System.debug('opps---->'+opps);
        //create realted Opportunity
        List <Contact> cons = TestObjectHelper.createMultipleContact(a, 10);
        integer loopNo = 1;
        for(Contact objContact: cons){
            objContact.Email = 'test'+loopNo+'@test.com';
        }

        insert cons;
        System.debug('cons---->'+cons);
        Staff__c staffObj =  new Staff__c();
       staffObj.Name = 'svxct111affdummy71';
       staffObj.Email__c = 'stvcxa144423ff@gmail.com';
       //staffObj.RecordTypeId=recrdid;
       staffObj.active__c = true;
        staffObj.External_ID_ADsPath__c='saddfsds';
        staffObj.HR_No__c='233wasdds12343';
         
       
       List <Staff__c> staffList = new  List <Staff__c>();
      staffList.add(staffObj);
      upsert  staffObj HR_No__c;
        
        Registered_Entity__c regentObj = new Registered_Entity__c();
        regentObj.Name = 'regTest';
        regentObj.Parent_Company__c = a.Id;
        insert regentObj;
        
        Work_Type__c workObj = new Work_Type__c();
        workObj.Name = 'worktype';
        workObj.Active__c = true;
        insert workObj;
        Job_Amount__c amount = new Job_Amount__c();
        amount.Name = 'jobamount1';
        amount.Amount__c = 7500.00;
        insert amount;
         amount = new Job_Amount__c();
        amount.Name = 'jobamount2';
        amount.Amount__c = 7500.00;
        insert amount;
        
        Coding_Structure__c codestructObj = new Coding_Structure__c();
        if(!ple.isEmpty()){
            codestructObj.Department__c = ple[0].getValue();    
        }
        if(!pleoffice.isEmpty()){
            codestructObj.Office__c = pleoffice[0].getValue();    
        }
        //
        //codestructObj.Department__c = 'DepartMent';
        codestructObj.Work_Type__c = workObj.Id;
        codestructObj.Staff__c = staffObj.Id;
        codestructObj.Status__c = 'Active';
        insert codestructObj;
        opportunity x = [Select id from opportunity Limit 1];
        x.Coding_Structure__c = codestructObj.Id;
        x.Amount = 1000;
        update x;
        Allocation__c allocObj = new Allocation__c();
        allocObj.Job__c = x.Id;
        allocObj.Allocation_Amount__c = 1000;
        allocobj.Assigned_To__c = staffObj.Id;
        allocObj.complete__c = false;
        allocObj.Status__c = 'Forecasted';
        allocobj.Department_Allocation__c = 'Deptallo1';
        allocObj.Office__c = 'Officeallo1';
        allocobj.workType_Allocation__c = 'workallo';
        insert allocObj;
        /*Allocation__c allocObject = new Allocation__c();
        allocObject.Job__c = x.Id;
        allocObject.Assigned_To__c = staffObj.Id;
        allocObject.Allocation_Amount__c = 1000;
        insert allocObject;*/
        Allocation__c allocObjExt = new Allocation__c();
        allocObjExt.Job__c = x.Id;
        allocObjExt.Assigned_To__c = staffObj.Id;
        allocObjExt.externalCompany__c = a.Id;
        allocObjExt.externalContact__c = cons[0].id;
        allocObjExt.Allocation_Amount__c = 1000;
        allocObjExt.complete__c = false;
         allocObjExt.Status__c = 'Forecasted';
         allocobj.Department_Allocation__c = 'Deptallo1';
        allocObj.Office__c = 'Officeallo1';
        allocobj.workType_Allocation__c = 'workallo';
        insert allocObjExt; 
        Forecasting__c forecastobj = new Forecasting__c();
        forecastobj.Amount__c = 1000;
        forecastobj.CS_Forecast_Date__c=System.today();
        forecastobj.Allocation__c = allocObj.Id;
        insert forecastobj;
        Forecasting__c forecastobject = new Forecasting__c();
        forecastobject.Amount__c = 1000;
        forecastobject.CS_Forecast_Date__c=System.today();
        forecastobject.Allocation__c = allocObjExt.Id;
        insert forecastobject;
    }
    static testMethod void testCS_allocationControllerForOpps(){
        System.debug('opp test---->');
        opportunity a = [Select id,AccountId from opportunity Limit 1];
        Staff__c staffobject = [Select id from Staff__c Limit 1];
        Test.startTest();
        PageReference pf = new PageReference('/apex/CS_allocationPage?id='+a.Id);
        Test.setCurrentPage(pf);
        cs_allocationController con = new cs_allocationController();
        
        ApexPages.currentPage().getParameters().put('recordIndex','1' );
        ApexPages.currentPage().getParameters().put('recordnumber','1' );
        con.autoPopulatePicklist();
        ApexPages.currentPage().getParameters().put('staffId',String.valueOf(staffobject.Id));
        ApexPages.currentPage().getParameters().put('recordvalue','1' );
        ApexPages.currentPage().getParameters().put('accountId',String.valueOf(a.AccountId));
        con.deleteMethod();
        con.addNewexternal();
        con.addNew();
        //ApexPages.currentPage().getParameters().put('recordnumber','1' );
        ApexPages.currentPage().getParameters().put('recordIndex','1' );
        ApexPages.currentPage().getParameters().put('recordnumber','1' );
        ApexPages.currentPage().getParameters().put('staffId',String.valueOf(staffobject.Id));
        ApexPages.currentPage().getParameters().put('recordvalue','1' );
        ApexPages.currentPage().getParameters().put('accountId',String.valueOf(a.AccountId));
        con.indexOfDept = 1;
        con.indexOfOffice = 1;
        con.autoPopulatePicklist();
        con.autoPopulatePicklistDept();
        con.autoPopulatePicklistOffc();
        ApexPages.currentPage().getParameters().put('amounttoupdate','1000000' );
        con.updateOpp();
        //con.strSObjectId = a.id;
        con.save();
        con.deleteMethodext();
        con.autoPopulatecontact();
        ApexPages.currentPage().getParameters().put('percentchecked','true' );
        con.save();
        Test.stopTest();       
    }
    static testMethod void testCS_allocationControllerForOppsforid(){
        System.debug('opp test---->');
        opportunity a = [Select id from opportunity Limit 1];
        Test.startTest();
        PageReference pf = new PageReference('/apex/CS_allocationPage?id=1234');
        Test.setCurrentPage(pf);
        
        cs_allocationController con = new cs_allocationController();    
        Test.stopTest();      
    }
    static testMethod void testCS_allocationControllerf0rsave(){
        System.debug('opp test---->');
       
      //  setupData();
        opportunity a = [Select id,AccountId,Manager__c from opportunity Limit 1];
        list<Allocation__c > alocationlist = new list<Allocation__c >();
        alocationlist = [SELECT id,Allocation_Amount__c,Assigned_To__c,externalCompany__c FROM  Allocation__c  WHERE job__c=:a.id];
        if(!alocationlist.isEmpty()){
            for(Allocation__c objalc: alocationlist){
                if(objalc.Allocation_Amount__c == null && objalc.Allocation_Amount__c<0){
                  objalc.Allocation_Amount__c = 100;    
                }
                if(objalc.Assigned_To__c == null && string.isBlank(objalc.Assigned_To__c)){
                   objalc.Assigned_To__c = a.id;   
                }
            }
            update alocationlist;
        }
       
        Test.startTest();
        PageReference pf = new PageReference('/apex/CS_allocationPage?id='+a.id);
        Test.setCurrentPage(pf);
        cs_allocationController con1 = new cs_allocationController();
       // system.debug('wrapper+++'+con1.wrapeerlist);
        if(!con1.wrapeerlist.isEmpty()){
          con1.wrapeerlist[0].sixMonthdata.put(4,'100');
            con1.wrapeerlist[0].forecastdata.put(14,'100');
            
        }
        if(!con1.wrapeerlistexternal.isEmpty()){
            con1.wrapeerlistexternal[0].forecastdatasix.put(3,'10');
            con1.wrapeerlistexternal[0].forecastdataexternal.put(13,'10');
        }
       //con1.meassgecheck = true;
        con1.save(); 
        cs_allocationController con = new cs_allocationController();  
         con.autoPopulatecompany(); 
        Test.stopTest();      
    }
}