public class SObjectWrapper{
    
        public Sobject objSObject{get;set;}
        public integer rowNo{get;set;}

        public SObjectWrapper(Sobject objSObjectInt, integer no){
            objSObject = objSObjectInt;
            rowNo = no;
        }
}