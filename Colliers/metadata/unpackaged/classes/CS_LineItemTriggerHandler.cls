/**
 *  Class Name: CS_LineItemTriggerHandler
 *  Description: This is a Trigger Handler Class for trigger on Invoice_Line_Item__c
 *  Company: dQuotient
 *  CreatedDate:6/02/2017 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Anand              16/02/2017            Orginal Version
 *
 */
public class CS_LineItemTriggerHandler {
    
    
    /**
     *  Method Name: updateVATAmount  
     *  Description: Method to update VAT Amount
     *  Param:  Trigger.oldMap,  Trigger.newMap
     *  Return: None
    */
    public static void updateVATAmount(map<id, Invoice_Line_Item__c> mapOldInvoiceLineItem, map<id, Invoice_Line_Item__c> mapNewInvoiceLineItem){
        
        List<Invoice_Line_Item__c> lstInvoicLineItemToUpdate = new List<Invoice_Line_Item__c>();
        for(Invoice_Line_Item__c objInvoiceLineItem: mapNewInvoiceLineItem.values()){
            if(objInvoiceLineItem.VAT_Amount__c != mapOldInvoiceLineItem.get(objInvoiceLineItem.id).VAT_Amount__c){
                lstInvoicLineItemToUpdate.add(objInvoiceLineItem);
            }
            
        }
        calculateVATAmount(lstInvoicLineItemToUpdate);
    }



   /**
     *  Method Name: calculateVATAmount  
     *  Description: Method to update VAT Amount
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void calculateVATAmount(List<Invoice_Line_Item__c> lstInvoicLineItem){
        
        set<id> setInvoiceIds = new set<id>();
        for(Invoice_Line_Item__c objInvoiceLineItem: lstInvoicLineItem){
            setInvoiceIds.add(objInvoiceLineItem.Invoice__c);
        }
        
        if(!setInvoiceIds.isEmpty()){
            List<Invoice__c> lstInvoice = new List<Invoice__c>();
            set<String> setCurrencyCode = new set<String>();
            map<id,String> mapInvoiceISOCode = new map<id,String>();
            map<String, Decimal> mapCodeConversionRate = new map<String, Decimal>();
            map<String, Decimal> mapCodeVAT = new map<String, Decimal>();
            lstInvoice = [Select id, name,
                                    is_International__c,
                                    Invoice_Currency__c,
                                    VAT__c
                                    From
                                    Invoice__c where id in: setInvoiceIds and is_International__c = true];
            
            for(Invoice__c objInvoice: lstInvoice){
                if(!String.isBlank(objInvoice.Invoice_Currency__c)){
                    setCurrencyCode.add(objInvoice.Invoice_Currency__c);
                    mapInvoiceISOCode.put(objInvoice.id,objInvoice.Invoice_Currency__c);
                    if(objInvoice.VAT__c != null){
                        mapCodeVAT.put(objInvoice.id,objInvoice.VAT__c);
                    }
                }   
            }
            if(!setCurrencyCode.isEmpty()){
                List<CurrencyType> lstCurrencyType = new List<CurrencyType>();
                lstCurrencyType = [Select id,DecimalPlaces, 
                                            ConversionRate,
                                            IsoCode 
                                            From 
                                            CurrencyType 
                                            where IsoCode in :setCurrencyCode];
                                            
            
            
                for(CurrencyType objCurrency: lstCurrencyType){
                    if(objCurrency.ConversionRate != null){
                        mapCodeConversionRate.put(objCurrency.IsoCode, objCurrency.ConversionRate);
                    }
                }
            
            }
            
            for(Invoice_Line_Item__c objInvoiceLineItem: lstInvoicLineItem){
                
                if(mapInvoiceISOCode.containsKey(objInvoiceLineItem.Invoice__c)){
                    String isoCode = mapInvoiceISOCode.get(objInvoiceLineItem.Invoice__c);
                    decimal conversionRate;
                    decimal vatAmount = 0.0;
                    decimal vatpercent = 0.0;
                    decimal internationalVATAmount = 0.0;
                    if(mapCodeConversionRate.containsKey(isoCode)){
                        conversionRate = mapCodeConversionRate.get(isoCode);
                        
                        if(objInvoiceLineItem.Type__c == 'Cost'){
                            if(objInvoiceLineItem.VAT_Amount_Prec__c != null){
                                // System.debug('-----VAT Precision->+objInvoiceLineItem.VAT_Amount_Prec__c);
                                vatAmount = objInvoiceLineItem.VAT_Amount_Prec__c;
                                internationalVATAmount = vatAmount*conversionRate;
                                System.debug('-----VAT Precision->'+internationalVATAmount);
                                objInvoiceLineItem.International_VAT_Amount__c = internationalVATAmount;
                            }else{
                                objInvoiceLineItem.International_VAT_Amount__c = 0.0;
                            }
                        }else if(objInvoiceLineItem.Type__c == 'Fee'){
                            decimal internationalAmount = 0.0;
                            if(mapCodeVAT.containsKey(objInvoiceLineItem.Invoice__c)){
                                vatpercent = mapCodeVAT.get(objInvoiceLineItem.Invoice__c);
                            }
                            if(objInvoiceLineItem.Amount_ex_VAT__c != null){
                                internationalAmount = objInvoiceLineItem.Amount_ex_VAT__c;
                                internationalVATAmount = internationalAmount*vatpercent/100;
                                objInvoiceLineItem.International_VAT_Amount__c = internationalVATAmount;
                            }else{
                                objInvoiceLineItem.International_VAT_Amount__c = 0.0;
                            }
                            
                        }
                        
                    }
                }
           
            }
        }
    }
 
}