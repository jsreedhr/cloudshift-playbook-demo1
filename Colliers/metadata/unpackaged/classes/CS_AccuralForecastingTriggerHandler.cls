/**
 *  Class Name: CS_AccuralForecastingTriggerHandler
 *  Description: This is a Trigger Handler Class for trigger on Accrual_Forecasting_Junction__c
 *  Company: dQuotient
 *  CreatedDate:27/01/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Anand              27/01/2017            Orginal Version
 *
 */
public class CS_AccuralForecastingTriggerHandler {
 
    /**
     *  Method Name: updateAccuralStatus  
     *  Description: Method to update Accural Status
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void updateAccuralStatus(List<Accrual_Forecasting_Junction__c> lstAccuralForecasting){
        
        set<id> setAccuralIds = new set<id>();
        for(Accrual_Forecasting_Junction__c objAccural: lstAccuralForecasting){
            setAccuralIds.add(objAccural.Accrual__c);
        }
        map<id, Accrual__c> mapIdAccural = new map<id, Accrual__c>();
        if(!setAccuralIds.isEmpty()){
            mapIdAccural = new map<id, Accrual__c>([Select id, Status__c From Accrual__c where id in: setAccuralIds]);
        }
        
        for(Accrual_Forecasting_Junction__c objAccural: lstAccuralForecasting){
            
            if(mapIdAccural.containsKey(objAccural.Accrual__c)){
                if(mapIdAccural.get(objAccural.Accrual__c).Status__c != null){
                    objAccural.Accural_Status__c = mapIdAccural.get(objAccural.Accrual__c).Status__c;
                }
            }
        }
    }
 
 
}