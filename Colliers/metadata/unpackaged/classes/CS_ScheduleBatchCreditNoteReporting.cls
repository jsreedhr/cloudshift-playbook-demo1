global class CS_ScheduleBatchCreditNoteReporting implements Schedulable  {
    global void execute(SchedulableContext SC) {
        CS_BatchCreditNoteReporting objBatch = new CS_BatchCreditNoteReporting();
        database.executeBatch(objBatch, 100);
    }
}