/**
*  Class Name: SharingEngineConfigurationManager
*  Description: Helper class for querying and managing status of Sharing rules (on/off regular/batch)
* 
*  Tests: SharingEngineConfigurationManagerTest
*
*  Company: CloudShift
*  CreatedDate: 29/06/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  ----------------------------------------------------------- 
*  Kaspars Rezgalis		28/06/2018					Created
*
*/
public class SharingEngineConfigurationManager{
	private static Map<String, SharingEngineConfigurationManager>instances = new Map<String, SharingEngineConfigurationManager>();

	public static SharingEngineConfigurationManager getInstance(String sObjectName){
        SharingEngineConfigurationManager instance;

        if(instances.containsKey(sObjectName)){
            instance = instances.get(sObjectName);
        }else{
            instance = new SharingEngineConfigurationManager(sObjectName);
            instances.put(sObjectName, instance);
        }
        return instance;
    }

    private final SharingEngine_Status__c sharingEngineStatusForSObject;

	private SharingEngineConfigurationManager(String sObjectName){
        sharingEngineStatusForSObject = SharingEngine_Status__c.getInstance(sObjectName);

        if(sharingEngineStatusForSObject==null){
            sharingEngineStatusForSObject = new SharingEngine_Status__c(
                Name = sObjectName
            );
        }
	}

    public Boolean isBatchSharingAlreadyRunning(){
        return sharingEngineStatusForSObject.Batch_Processing_in_Progress__c;
    }

    public void setBatchSharingIsRunning_ON(){
        sharingEngineStatusForSObject.Batch_Processing_in_Progress__c = true;
    }

    public void setBatchSharingIsRunning_OFF(){
        sharingEngineStatusForSObject.Batch_Processing_in_Progress__c = false;
    }

    public void commitConfigurationChanges(){
        upsert sharingEngineStatusForSObject;
    }
}