@isTest
private class contactAddressControllerTest{
   
    Public static Account a;
    static Contact c;
    static opportunity opp;
    static Address__c addr;
    static Contact_Address_Junction__c accAddrJn;
    static Address__c addrr = new Address__c();
    
     static testmethod void setupData(){
        //create account
        a = TestObjectHelper.createAccount();
        insert a ;
        Schema.DescribeFieldResult fieldResult = Address__c.Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        c = TestObjectHelper.createContact(a);
        c.Email ='test1@test1.com';
        insert c;
        
        addr = TestObjectHelper.createAddress(a);
        if(!ple.isEmpty()){
            addr.Country__c =ple[0].getValue();
        }
        insert addr;
        
        addrr = TestObjectHelper.createAddress(a);
        addrr.Flat_Number__c='#45';
        addrr.Building__c = 'buildin';
        addrr.Estate__c = 'estate';
        addrr.Area__c= 'area';
        addrr.Street_Number__c='56';
        addrr.Street__c = 'Street';
        addrr.Town__c = 'town';
        addrr.Postcode__c = '12erts45';
        if(!ple.isEmpty()){
            addrr.Country__c =ple[0].getValue();
        }
        //insert addr;
        
        accAddrJn = TestObjectHelper.createContactAddressJunction(c,addr);
        insert accAddrJn;
              
    }
    
    private static testMethod void testRec() {
        setupData();

        List<Address__c> addrLst = new List<Address__c>();
        addrLst.add(addr);
        
        List<contactAddressController.booleanContactJnWrapper> jnWrper = new List<contactAddressController.booleanContactJnWrapper>();
        PageReference testPage = new pageReference('/apex/contactAddress');
        testPage.getParameters().put('id', c.id);

        test.setCurrentPage(testPage);   

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(addrLst);
       
        Test.startTest();
        
        contactAddressController ctrl = new contactAddressController(); 
        ctrl.cons = sc;

        contactAddressController.booleanContactWrapper wrapp= new contactAddressController.booleanContactWrapper(true,addr);
        contactAddressController.booleanContactJnWrapper jnwrapp= new contactAddressController.booleanContactJnWrapper(addr.id,'building','bc23pr','street','town',accAddrJn.id);
        jnWrper.add(jnwrapp);
        
        ctrl.ListbooleanContactJnWrapper = jnWrper;
        system.debug('addrr  ------->'+addrr);
        ctrl.newAddress = addrr;
        system.debug('ctrl.newAddress ------ >'+ctrl.newAddress);
        
        system.debug('ctrl.newAddress.Street_Number__c ------ >'+ctrl.newAddress.Street_Number__c);
        system.debug('ctrl.newAddress.Flat_Number__c------ >'+ctrl.newAddress.Flat_Number__c);
        ctrl.addError = 'errorMessage';
        ctrl.Ids = a.id;
        ctrl.IdtoDelete = '0';
        ctrl.IsSelectAll = true;
        ctrl.isInsrt = true;
        
        
        ctrl.reDirect();
        ctrl.getNewCategories();
        ctrl.getCategorieskeypreix();
        ctrl.saveAddress();
        ctrl.deleteSelectedRow();
        ctrl.SelectedProperty();
        ctrl.selectAllProperty();
        ctrl.first();
        ctrl.firstfilter();
        ctrl.last();
        ctrl.next();
        ctrl.previous();
        ctrl.cancelJobs();
        ctrl.closeModal();
        
        Test.stopTest();

    }
    
}