@isTest(SeeAllData=false)
public class TestBatchUpdateForecastingAmount{
    static Account a;
    static Contact c;
    static opportunity opp;
    static opportunity oppr;
    static Forecasting__c frcst;
    static Forecasting__c frcst1;
    static Allocation__c alloc;
    static Staff__c stf;
    static List<Forecasting__c> frcstLst;
    static List<Allocation__c> allocLst;
    static Job_Amount__c jobAmt;
    
   // @testsetup
    static testmethod void setupData(){
        //create account
        a = TestObjectHelper.createAccount();
        insert a ;
        
        //create contact
        c = TestObjectHelper.createContact(a);
        c.Email ='test1@test1.com';
        insert c;
        
        //create Opportunity 1
        opp = TestObjectHelper.createOpportunity(a);
        insert opp;
        
        //Create Opportunity Record
        oppr = TestObjectHelper.createOpportunity(a);
        Insert oppr;
        
        //create Staff
        stf = TestObjectHelper.createStaff();
        stf.Active__c = true;
        stf.HR_No__c = 'ABCD';
        insert stf;
        
        //create job Amount
        jobAmt = TestObjectHelper.createJobAmount();
        insert jobAmt;
        
        //create Allocation
        alloc = TestObjectHelper.createAllocation(oppr,stf);
        insert alloc;
        
        //create Forecast 1
        frcst = TestObjectHelper.createForecast(alloc);
        frcst.CS_Forecast_Date__c = system.today().addDays(-30);
        frcst.Amount__c = 3000.0;
        insert frcst;
        
        //create Forecast 2
        frcst1 = TestObjectHelper.createForecast(alloc);
        frcst1.CS_Forecast_Date__c = system.today();
        frcst1.Amount__c = 3000.0;
        insert frcst1;
        
        //create Forecast List
        frcstLst = TestObjectHelper.createMultipleForecasting(alloc,10);
        insert frcstLst;
        
        //create Alloction List
        //allocLst = TestObjectHelper.createMultipleAllocation(opp,stf,10);
        //insert allocLst;
    }
    
    static testmethod void TestBatchUpdateForecastingAmount(){
        setupData();
        Test.startTest();
        BatchUpdateForecastingAmount updtFrcstAmt= new BatchUpdateForecastingAmount();
        Database.executeBatch(updtFrcstAmt);

        Test.stopTest();  
    }

}