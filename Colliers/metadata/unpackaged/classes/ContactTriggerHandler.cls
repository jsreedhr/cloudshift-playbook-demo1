public class ContactTriggerHandler {
    public Static Boolean isAfterInsert = true;
    public static void insertContactUltimateParentRelation(List<contact> conList){
        List<AccountContactRelation> acrList = new List<AccountContactRelation>();
        List<Contact> contactList = new List<Contact>();
        contactList = [select id,name,accountId,account.ultimateParentId__c,account.ParentId from contact where id in:conList];
        Set<Id> setAccIds = new Set<Id>();
        for (Contact con : contactList){
            if(con.AccountId != null && String.isNotBlank(con.AccountId))
            {
                setAccIds.add(con.AccountId);
            }
        }   
        List<Account> listParentAccounts = [Select Id,ParentId,Parent.ParentId,Parent.Parent.ParentId,Parent.Parent.Parent.ParentId,Parent.Parent.Parent.Parent.ParentId from Account where Id In :setAccIds];
        
        // Create map of Id => parentId and Id => UltimateParentId
        Map<Id,Id> mapOfParentIds = new Map<Id,Id>();
        Map<Id,Id> mapOfUltimateParentIds = new Map<Id,Id>();
        if(!listParentAccounts.isEmpty())
        {
            for(Account acc : listParentAccounts)
            {
                if(acc.ParentId != null && String.isNotBlank(acc.ParentId))
                {
                    mapOfParentIds.put(acc.Id,acc.ParentId);
                }
                if(acc.Parent.Parent.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.Parent.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.Parent.Parent.Parent.ParentId);
                }
                else if(acc.Parent.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.Parent.Parent.ParentId);
                }
                else if(acc.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.Parent.ParentId);
                }
                else if(acc.Parent.ParentId != null && String.isNotBlank(acc.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.ParentId);
                }
                else if(acc.ParentId != null && String.isNotBlank(acc.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.ParentId);
                }
            }
        }
            
        for(Contact con : contactList){
            if(con.account.ParentId != null ){
                if(con.AccountId != mapOfUltimateParentIds.get(con.accountId))
                {
                    AccountContactRelation acr = new AccountContactRelation();
                    acr.AccountId = mapOfUltimateParentIds.get(con.accountId);
                    acr.ContactId = con.id;
                    acrList.add(acr);
                    System.debug('acr--'+acr);
                }
                if( con.account.ParentId != null && con.AccountId != con.account.ParentId && mapOfUltimateParentIds.get(con.accountId) !=  con.account.ParentId)
                {
                    AccountContactRelation acr = new AccountContactRelation();
                    acr.AccountId =  con.account.ParentId;
                    acr.ContactId = con.id;
                    acrList.add(acr);
                    System.debug('acr--'+acr);
                }
            }
        }
        if(acrList!=NULL && !acrList.isEmpty()){
            insert acrList;
        }
    }

}