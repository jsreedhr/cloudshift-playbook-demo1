/**
 *  Class Name: CS_PurchaseOrderInvoiceTriggerHandler
 *  Description: This is a Trigger Handler Class for trigger on Purchase_Order_Invoice_Junction__c
 *  Company: dQuotient
 *  CreatedDate:27/01/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Anand              27/01/2017            Orginal Version
 *
 */
public class CS_PurchaseOrderInvoiceTriggerHandler {
 
    /**
     *  Method Name: updateInvoiceStatus  
     *  Description: Method to update Invoice Status
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void updateInvoiceStatus(List<Purchase_Order_Invoice_Junction__c> lstPurchaseOrder){
        
        set<id> setInvoiceIds = new set<id>();
        for(Purchase_Order_Invoice_Junction__c objPurchaseOrderInvoice: lstPurchaseOrder){
            setInvoiceIds.add(objPurchaseOrderInvoice.Invoice__c);
        }
        map<id, Invoice__c> mapIdInvoice = new map<id, Invoice__c>();
        if(!setInvoiceIds.isEmpty()){
            mapIdInvoice = new map<id, Invoice__c>([Select id, Status__c From Invoice__c where id in: setInvoiceIds]);
        }
        
        for(Purchase_Order_Invoice_Junction__c objPurchaseOrderInvoice: lstPurchaseOrder){
            
            if(mapIdInvoice.containsKey(objPurchaseOrderInvoice.Invoice__c)){
                if(mapIdInvoice.get(objPurchaseOrderInvoice.Invoice__c).Status__c != null){
                    objPurchaseOrderInvoice.Invoice_Status__c = mapIdInvoice.get(objPurchaseOrderInvoice.Invoice__c).Status__c;
                }
            }
        }
    }
    
    /**
     *  Method Name: insertPOAmount  
     *  Description: Method to update PO Amount
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void insertPOAmount(List<Purchase_Order_Invoice_Junction__c> lstPurchaseOrderInvoice){    
        
        set<id> setInvoiceIds = new set<id>();
        set<id> setPurchaseIds = new set<id>();
        for(Purchase_Order_Invoice_Junction__c objPurchaseOrderInvoice: lstPurchaseOrderInvoice){
            setInvoiceIds.add(objPurchaseOrderInvoice.Invoice__c);
            setPurchaseIds.add(objPurchaseOrderInvoice.Purchase_Order__c);
        }
        map<id, Invoice__c> mapIdInvoice = new map<id, Invoice__c>();
        map<id, Decimal> mapIdInvoiceBalance = new map<id, Decimal>();
        map<id, Purchase_Order__c> mapIdPurchaseOrder = new map<id, Purchase_Order__c>();
        map<id, decimal> mapPurchaseOrderBalanceAmount = new map<id, decimal>();
        if(!setInvoiceIds.isEmpty()){
            mapIdInvoice = new map<id, Invoice__c>([Select id, Name,
                                                            Total_Fee__c,
                                                            Status__c,   
                                                            Opportunity__r.AccountId,
                                                            Opportunity__r.Account.PO_Required__c,
                                                            Balance_PO_to_be_Paid__c,
                                                            PO_Amount_Paid__c
                                                            From
                                                            Invoice__c 
                                                            where Opportunity__r.AccountId != null 
                                                            And ( Status__c = 'Paid' OR  Status__c = 'Printed'
                                                            OR  Status__c = 'Outstanding' OR  Status__c = 'Partial Paid'
                                                            OR  Status__c = 'Outstanding' OR  Status__c = 'Approved'
                                                            OR  Status__c = 'Cancelled' OR  Status__c = 'Partial' )
                                                            AND id in: setInvoiceIds]);
                                                            
            for(Invoice__c objInvoice : mapIdInvoice.values()){
                
                mapIdInvoiceBalance.put(objInvoice.id, objInvoice.Balance_PO_to_be_Paid__c);
            }
                                                            
            
        }
        
        if(!setPurchaseIds.isEmpty()){
            
            mapIdPurchaseOrder = new map<id, Purchase_Order__c>([Select id, Name,
                                                                        Amount__c,
                                                                        BalanceAmount__c,
                                                                        Used_Amount_Rollup__c,
                                                                        PO_Status__c
                                                                        From 
                                                                        Purchase_Order__c 
                                                                        where id in: setPurchaseIds 
                                                                        AND (PO_Status__c = 'Original' OR  PO_Status__c = 'Orginal'
                                                                        OR PO_Status__c = 'Amended - Approved')]);
        
            for(Purchase_Order__c objPurchaseOrder : mapIdPurchaseOrder.values()){
                mapPurchaseOrderBalanceAmount.put(objPurchaseOrder.id, objPurchaseOrder.BalanceAmount__c);
            }
        
        }
        for(Purchase_Order_Invoice_Junction__c objPurchaseOrderInvoice: lstPurchaseOrderInvoice){
            Invoice__c objInvoice = new Invoice__c();
            Purchase_Order__c objPurchaseOrder = new Purchase_Order__c();
            if(mapIdInvoiceBalance.containsKey(objPurchaseOrderInvoice.Invoice__c) && mapIdPurchaseOrder.containsKey(objPurchaseOrderInvoice.Purchase_Order__c)){
                // objInvoice = mapIdInvoice.get(objPurchaseOrderInvoice.Invoice__c);
                objPurchaseOrder = mapIdPurchaseOrder.get(objPurchaseOrderInvoice.Purchase_Order__c);
                decimal balanceToPay = 0.0;
                decimal balanceToPayAfterCal = 0.0;
                decimal balanceAvailable = 0.0;
                System.debug('---Balance-----'+objInvoice.Balance_PO_to_be_Paid__c);
                if(mapIdInvoiceBalance.get(objPurchaseOrderInvoice.Invoice__c) > 0){
                    System.debug('-InsideBalance-----'+mapIdInvoiceBalance.get(objPurchaseOrderInvoice.Invoice__c));
                    balanceToPay = mapIdInvoiceBalance.get(objPurchaseOrderInvoice.Invoice__c);
                    balanceToPayAfterCal = balanceToPay;
                    balanceAvailable = mapPurchaseOrderBalanceAmount.get(objPurchaseOrderInvoice.Purchase_Order__c);
                    if(balanceAvailable >= balanceToPay){
                        objPurchaseOrderInvoice.Amount_Paid__c = balanceToPay;
                        balanceToPayAfterCal = 0.0;                     
                    }else{
                        // objPurchaseOrderInvoice.addError('The PO entered is not sufficient to cover Net Fees. Please increase the amount.');
                        if(balanceAvailable > 0){
                            objPurchaseOrderInvoice.Amount_Paid__c = balanceAvailable;
                            balanceToPayAfterCal = balanceToPayAfterCal - balanceAvailable;
                        }
                    }
                    mapIdInvoiceBalance.put(objPurchaseOrderInvoice.Invoice__c, balanceToPayAfterCal);
                }
            
            }
        }
    }
    
     /**
     *  Method Name: deletePOAmount  
     *  Description: Method to update PO Amount
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void deletePOAmount(List<Purchase_Order_Invoice_Junction__c> lstPurchaseOrderInvoice){ 

        set<id> setInvoiceIds = new set<id>();
        set<id> setPurchaseIds = new set<id>();
        map<id, Invoice__c> mapIdInvoice = new map<id, Invoice__c>();
        map<id, Decimal> mapInvoicePoAmountPaid = new map<id, Decimal>();
        map<id, Decimal> mapInvoicePoAmountPaidAndAvailable = new map<id, Decimal>();
        map<id, List<Purchase_Order_Invoice_Junction__c>> mapIdListPurchaseOrderInvoice = new map<id, List<Purchase_Order_Invoice_Junction__c>> ();
        for(Purchase_Order_Invoice_Junction__c objPurchaseOrderInvoice: lstPurchaseOrderInvoice){
            setInvoiceIds.add(objPurchaseOrderInvoice.Invoice__c);
            setPurchaseIds.add(objPurchaseOrderInvoice.Purchase_Order__c);
        }
        
        
        if(!setInvoiceIds.isEmpty()){
            mapIdInvoice = new map<id, Invoice__c>([Select id, Name,
                                                            Total_Fee__c,
                                                            Status__c,   
                                                            Opportunity__r.AccountId,
                                                            Opportunity__r.Account.PO_Required__c,
                                                            Balance_PO_to_be_Paid__c,
                                                            PO_Amount_Paid__c
                                                            From
                                                            Invoice__c 
                                                            where Opportunity__r.AccountId != null 
                                                            And ( Status__c = 'Paid' OR  Status__c = 'Printed'
                                                            OR  Status__c = 'Outstanding' OR  Status__c = 'Partial Paid'
                                                            OR  Status__c = 'Outstanding' OR  Status__c = 'Approved'
                                                            OR  Status__c = 'Cancelled' OR  Status__c = 'Partial' )
                                                            AND id in: setInvoiceIds
                                                            AND Total_Fee__c != null]);
                                                            
            
        }
        
        if(!setPurchaseIds.isEmpty() && !mapIdInvoice.keyset().isEmpty()){
            
            List<Purchase_Order_Invoice_Junction__c> lstPurchaseOrderInvoiceOther = new List<Purchase_Order_Invoice_Junction__c>();
                                                                        
            lstPurchaseOrderInvoiceOther = [Select id, name,
                                                    Amount_Paid__c,
                                                    Purchase_Order__c,
                                                    CreatedDate,
                                                    Invoice__c,
                                                    Purchase_Order__r.PO_Status__c,
                                                    Purchase_Order__r.BalanceAmount__c
                                                    From 
                                                    Purchase_Order_Invoice_Junction__c
                                                    where Invoice__c in : mapIdInvoice.keyset() 
                                                    AND Purchase_Order__c not in: setPurchaseIds
                                                    AND (Purchase_Order__r.PO_Status__c = 'Original' OR  Purchase_Order__r.PO_Status__c = 'Orginal'
                                                    OR Purchase_Order__r.PO_Status__c = 'Amended - Approved') order by CreatedDate];
        
            for(Purchase_Order_Invoice_Junction__c objPurchaseOrderInvoice : lstPurchaseOrderInvoiceOther ){
                
                
                if(mapIdListPurchaseOrderInvoice.containsKey(objPurchaseOrderInvoice.Invoice__c)){
                    mapIdListPurchaseOrderInvoice.get(objPurchaseOrderInvoice.Invoice__c).add(objPurchaseOrderInvoice);
                }else{
                    mapIdListPurchaseOrderInvoice.put(objPurchaseOrderInvoice.Invoice__c, new List<Purchase_Order_Invoice_Junction__c>{objPurchaseOrderInvoice});
                }
                decimal amountAvailableAndAdded = 0.0;
                decimal amountPaid = 0.0;
                if(objPurchaseOrderInvoice.Purchase_Order__r.BalanceAmount__c != null){
                    amountAvailableAndAdded = amountAvailableAndAdded+objPurchaseOrderInvoice.Purchase_Order__r.BalanceAmount__c;
                }
                
                if(objPurchaseOrderInvoice.Amount_Paid__c != null){
                    amountAvailableAndAdded = amountAvailableAndAdded+objPurchaseOrderInvoice.Amount_Paid__c;
                    amountPaid = amountPaid+objPurchaseOrderInvoice.Amount_Paid__c;
                    
                }
                
                
                if(mapInvoicePoAmountPaidAndAvailable.containsKey(objPurchaseOrderInvoice.Invoice__c)){
                    decimal POAmountPaidAndAvailable = mapInvoicePoAmountPaidAndAvailable.get(objPurchaseOrderInvoice.Invoice__c);
                    POAmountPaidAndAvailable = POAmountPaidAndAvailable + amountAvailableAndAdded;
                    mapInvoicePoAmountPaidAndAvailable.put(objPurchaseOrderInvoice.Invoice__c, POAmountPaidAndAvailable);
                }else{
                    mapInvoicePoAmountPaidAndAvailable.put(objPurchaseOrderInvoice.Invoice__c, amountAvailableAndAdded);
                }   
                
                if(mapInvoicePOAmountPaid.containsKey(objPurchaseOrderInvoice.Invoice__c)){
                    decimal POAmountPaid = mapInvoicePOAmountPaid.get(objPurchaseOrderInvoice.Invoice__c);
                    POAmountPaid = POAmountPaid + amountPaid;
                    mapInvoicePOAmountPaid.put(objPurchaseOrderInvoice.Invoice__c, POAmountPaid);
                }else{
                    mapInvoicePOAmountPaid.put(objPurchaseOrderInvoice.Invoice__c, amountPaid);
                }
                    
            }
        
        }
        List<Purchase_Order_Invoice_Junction__c> lstPurchaseOrderInvoiceOtherUpdate = new List<Purchase_Order_Invoice_Junction__c>();
        for(Purchase_Order_Invoice_Junction__c objPurchaseOrderInvoice: lstPurchaseOrderInvoice){
            
            if(mapIdInvoice.containsKey(objPurchaseOrderInvoice.Invoice__c)){
                
                decimal totalFee = mapIdInvoice.get(objPurchaseOrderInvoice.Invoice__c).Total_Fee__c;
                decimal POAlreadyAvailableAndPaid = 0.0;
                decimal POAlreadyPaid = 0.0;
                if(mapInvoicePoAmountPaidAndAvailable.containsKey(objPurchaseOrderInvoice.Invoice__c)){
                    POAlreadyAvailableAndPaid = mapInvoicePoAmountPaidAndAvailable.get(objPurchaseOrderInvoice.Invoice__c);
                }
                if(mapInvoicePOAmountPaid.containsKey(objPurchaseOrderInvoice.Invoice__c)){
                    POAlreadyPaid = mapInvoicePOAmountPaid.get(objPurchaseOrderInvoice.Invoice__c);
                }
                if(POAlreadyAvailableAndPaid < totalFee){
                    objPurchaseOrderInvoice.addError('You cannot delete this Purchase Order. The PO Amount will fall below the Net Fee');
                }else{
                    if(POAlreadyPaid < totalFee){
                        decimal amountToBePaid = totalFee - POAlreadyPaid;
                        if(mapIdListPurchaseOrderInvoice.containsKey(objPurchaseOrderInvoice.Invoice__c)){
                            for(Purchase_Order_Invoice_Junction__c objPurchaseOrderInvoiceOther: mapIdListPurchaseOrderInvoice.get(objPurchaseOrderInvoice.Invoice__c)){
                                decimal amountAlreadyPaid = 0.0;
                                if(objPurchaseOrderInvoiceOther.Amount_Paid__c != null){
                                    amountAlreadyPaid = objPurchaseOrderInvoiceOther.Amount_Paid__c;
                                }
                                if(objPurchaseOrderInvoiceOther.Purchase_Order__r.BalanceAmount__c >=  amountToBePaid){
                                    objPurchaseOrderInvoiceOther.Amount_Paid__c = amountAlreadyPaid + amountToBePaid;
                                    lstPurchaseOrderInvoiceOtherUpdate.add(objPurchaseOrderInvoiceOther);
                                    break;
                                }else{
                                    objPurchaseOrderInvoiceOther.Amount_Paid__c = amountAlreadyPaid + objPurchaseOrderInvoiceOther.Purchase_Order__r.BalanceAmount__c;
                                    amountToBePaid = amountToBePaid - objPurchaseOrderInvoiceOther.Purchase_Order__r.BalanceAmount__c;
                                    lstPurchaseOrderInvoiceOtherUpdate.add(objPurchaseOrderInvoiceOther);
                                }
                            }
                            
                        }
                        
                    }
                }
                
            }
        }
        
        if(!lstPurchaseOrderInvoiceOtherUpdate.isEmpty()){
            update lstPurchaseOrderInvoiceOtherUpdate;
        }
    }
 
}