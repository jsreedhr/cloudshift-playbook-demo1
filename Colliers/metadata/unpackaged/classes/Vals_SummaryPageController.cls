public with sharing class Vals_SummaryPageController
{
    public Inspection__c inspectionObj{get;set;}
    public list<Surrounding__c> surroundingInspectionList{get; set;}
    public list<External_Inspection__c> externalInspectionList{get;set;}
    public list<Internal_Inspection__c> internalInspectionList {get;set;}
    public String inspectionId{get;set;}
    public Boolean hasSurrounding{get;set;}
    public Boolean hasExternal{get;set;}
    public List<ExternalWrapper> ExternalWrapperList {get;set;}
    public List<SurroudningWrapper> SurroudningWrapperList {get;set;}
    public Vals_SummaryPageController()
    {
        surroundingInspectionList = New list<Surrounding__c>();
        externalInspectionList = New list<External_Inspection__c>();
        internalInspectionList = New list<Internal_Inspection__c>();
        Map<id,List<Internal_Inspection__c>> externalInternalMap = New Map<id,List<Internal_Inspection__c>>();
        Map<id,List<Internal_Inspection__c>> internalInspectionMap = New Map<id,List<Internal_Inspection__c>>();
        ExternalWrapperList = New List<ExternalWrapper>();
        SurroudningWrapperList = New List<SurroudningWrapper>();
        hasExternal = false;
        hasSurrounding=false;
        
        inspectionId = ApexPages.currentpage().getparameters().get('id');
        if(inspectionId!=NULL)
        {
            inspectionObj =  [Select id,Name,Date_Time_of_Inspection__c,Inspection_Type_del__c,Job_Name__c,
                              Job_Number__c,Property_Address__c,Site_Contact__c,Site_Mobile__c,Inspecting_Surveyor__c ,
                              Client__c,Special_Requirements__c,Count_of_External_Inspection__c,No_of_Internal_Inspections__c,
                              Site_Email__c,Site_Landline__c,Is_Development__c,Is_Development_Land_Subtype__c,Mixed_Use__c,
                              Mixed_Use_Subtype__c,Measurement_Required__c
                              from Inspection__c 
                              where ID=:inspectionId LIMIT 1];
            if(inspectionObj.id!=NULL)
            {
                surroundingInspectionList=[Select id,Name,Neighboring_Occupiers__c,Other_Marketing_Boards__c,
                                           Accessibility__c,Is_Established_Area__c, Is_Site_Level__c,Site_Level_Subtype__c,
                                           Site_Level_To__c,Is_Site_Secure__c,Security_Type__c,Japanese_Knotweed__c,Fire_Risk_Assessment_in_Place__c,
                                           Asbestos_Management_Plan__c,General_Enquiries__c,Site_Access__c,Has_Roof_Access__c,Has_Access_to_Vault__c,
                                           Other__c,Rail_track__c,Substation__c,Lake__c,River__c,Large_Trees__c,Overhead_Power_Cables__c,
                                           Gas_Containers__c,Expansion_Land__c,Other_1__c,Other_2__c,Other_3__c,Weather__c,
                                           Inspection_Limitations__c,Occupancy__c,Occupier_Type__c,Location__c,Situation__c
                                           from Surrounding__c
                                           where Inspection__c=:inspectionId LIMIT 999];
                
                if(surroundingInspectionList.size()>0)
                {
                    hasSurrounding=true;
                    Set<Id> surroundinginspectionIdSet = New Set<Id>();
                    
                    for(Surrounding__c surObj : surroundingInspectionList)
                    {
                        SurroudningWrapperList.add(New SurroudningWrapper(surObj));
                        if( surObj.Neighboring_Occupiers__c!=NULL && surObj.Neighboring_Occupiers__c.length()>1)
                            surObj.Neighboring_Occupiers__c= toTitleCase(surObj.Neighboring_Occupiers__c);
                        if( surObj.Other_Marketing_Boards__c!=NULL && surObj.Other_Marketing_Boards__c.length()>1)
                            surObj.Other_Marketing_Boards__c= toTitleCase(surObj.Other_Marketing_Boards__c);
                        if( surObj.Accessibility__c!=NULL && surObj.Accessibility__c.length()>1)
                            surObj.Accessibility__c= toTitleCase(surObj.Accessibility__c);
                        if( surObj.Site_Level_Subtype__c!=NULL && surObj.Site_Level_Subtype__c.length()>1)
                            surObj.Site_Level_Subtype__c= toTitleCase(surObj.Site_Level_Subtype__c);
                        if( surObj.Site_Level_To__c!=NULL && surObj.Site_Level_To__c.length()>1)
                            surObj.Site_Level_To__c= toTitleCase(surObj.Site_Level_To__c);
                        if( surObj.Security_Type__c!=NULL && surObj.Security_Type__c.length()>1)
                            surObj.Security_Type__c= toTitleCase(surObj.Security_Type__c);
                        if( surObj.Fire_Risk_Assessment_in_Place__c!=NULL && surObj.Fire_Risk_Assessment_in_Place__c.length()>1)
                            surObj.Fire_Risk_Assessment_in_Place__c= toTitleCase(surObj.Fire_Risk_Assessment_in_Place__c);
                        if( surObj.Asbestos_Management_Plan__c!=NULL && surObj.Asbestos_Management_Plan__c.length()>1)
                            surObj.Asbestos_Management_Plan__c= toTitleCase(surObj.Asbestos_Management_Plan__c);
                        if( surObj.General_Enquiries__c!=NULL && surObj.General_Enquiries__c.length()>1)
                            surObj.General_Enquiries__c= toTitleCase(surObj.General_Enquiries__c);
                        if( surObj.Site_Access__c!=NULL && surObj.Site_Access__c.length()>1)
                            surObj.Site_Access__c= toTitleCase(surObj.Site_Access__c);
                        if( surObj.Other__c!=NULL && surObj.Other__c.length()>1)
                            surObj.Other__c= toTitleCase(surObj.Other__c);
                        if( surObj.Other_1__c!=NULL && surObj.Other_1__c.length()>1)
                            surObj.Other_1__c= toTitleCase(surObj.Other_1__c);
                        if( surObj.Other_2__c!=NULL && surObj.Other_2__c.length()>1)
                            surObj.Other_2__c= toTitleCase(surObj.Other_2__c);
                        if( surObj.Other_3__c!=NULL && surObj.Other_3__c.length()>1)
                            surObj.Other_3__c= toTitleCase(surObj.Other_3__c);
                        if( surObj.Weather__c!=NULL && surObj.Weather__c.length()>1)
                            surObj.Weather__c= toTitleCase(surObj.Weather__c);
                        if( surObj.Inspection_Limitations__c!=NULL && surObj.Inspection_Limitations__c.length()>1)
                            surObj.Inspection_Limitations__c= toTitleCase(surObj.Inspection_Limitations__c);
                        if( surObj.Occupancy__c!=NULL && surObj.Occupancy__c.length()>1)
                            surObj.Occupancy__c= toTitleCase(surObj.Occupancy__c);
                        if( surObj.Occupier_Type__c!=NULL && surObj.Occupier_Type__c.length()>1)
                            surObj.Occupier_Type__c= toTitleCase(surObj.Occupier_Type__c);
                        if( surObj.Location__c!=NULL && surObj.Location__c.length()>1)
                            surObj.Location__c= toTitleCase(surObj.Location__c);
                        if( surObj.Situation__c!=NULL && surObj.Situation__c.length()>1)
                            surObj.Situation__c= toTitleCase(surObj.Situation__c);
                    }
                    
                }
                
                externalInspectionList = [Select id,Name,Count_of_Internal_Inspection__c,No_parking_spaces_RepValue__c,Property_Notes__c,
                                          Extension_Notes__c,Other_Building_Construction__c,Century__c,Decade__c,
                                          Construction_Year__c,The_Front_Elevation_is_RepValue__c,Other_Front_Elevation__c,
                                          Other_Elevations_are_RepValue__c,Other_Other_Elevations__c,The_Roof_is_RepValue__c,
                                          Other_Roof__c,Covered_in_RepValue__c,Other_Covered_In__c,The_windows_are_Rep_Value__c, Window_Defects__c,
                                          Frames_are_RepValue__c,Other_Frames__c,Building_Construction_RepValue__c,Other_Staircases__c,
                                          Parking_Type__c,Parking_type_RepValue__c,Other_Parking__c,Parking_surface_RepValue__c,No_Parking_Spaces__c,
                                          Other_Parking_Surface__c,Loading_Bays__c,Other_Loading_Bays__c,External_Areas__c,
                                          External_Areas_multi__c,External_Decoration_is__c,External_Wall_Defects__c,
                                          Other_Defects_Wall__c,Roof_Defects__c,Other_Defects_Roof__c,Staircases_RepValue__c,
                                          Other_Defects_Windows__c,Floor_Defects__c,Other_Defects_Floor__c, Other_Windows__c
                                          from External_Inspection__c 
                                          where Inspection__c=:inspectionId LIMIT 999];
                if(externalInspectionList.size()>0)
                { 
                    hasExternal = true;
                    Set<Id> externalinspectionIdSet = New Set<Id>();
                    for(External_Inspection__c e : externalInspectionList)
                    {   if(e.The_Front_Elevation_is_RepValue__c!=NULL && e.The_Front_Elevation_is_RepValue__c.length()>1)
                        e.The_Front_Elevation_is_RepValue__c= toTitleCase(e.The_Front_Elevation_is_RepValue__c);
                     if( e.Building_Construction_RepValue__c!=NULL && e.Building_Construction_RepValue__c.length()>1)
                         e.Building_Construction_RepValue__c= toTitleCase(e.Building_Construction_RepValue__c);
                     if( e.External_Areas_multi__c!=NULL && e.External_Areas_multi__c.length()>1)
                         e.External_Areas_multi__c= toTitleCase(e.External_Areas_multi__c);
                     if( e.Floor_Defects__c!=NULL && e.Floor_Defects__c.length()>1)
                         e.Floor_Defects__c= toTitleCase(e.Floor_Defects__c);
                     if( e.Roof_Defects__c!=NULL && e.Roof_Defects__c.length()>1)
                         e.Roof_Defects__c= toTitleCase(e.Roof_Defects__c);
                     if( e.External_Wall_Defects__c!=NULL && e.External_Wall_Defects__c.length()>1)
                         e.External_Wall_Defects__c= toTitleCase(e.External_Wall_Defects__c);
                     if( e.External_Decoration_is__c!=NULL && e.External_Decoration_is__c.length()>1)
                         e.External_Decoration_is__c= toTitleCase(e.External_Decoration_is__c);
                     if(e.Other_Building_Construction__c!=NULL && e.Other_Building_Construction__c.length()>1)
                         e.Other_Building_Construction__c= toTitleCase(e.Other_Building_Construction__c);
                     if(e.Other_Front_Elevation__c!=NULL && e.Other_Front_Elevation__c.length()>1)
                         e.Other_Front_Elevation__c= toTitleCase(e.Other_Front_Elevation__c);
                     if(e.Other_Elevations_are_RepValue__c!=NULL && e.Other_Elevations_are_RepValue__c.length()>1)
                         e.Other_Elevations_are_RepValue__c= toTitleCase(e.Other_Elevations_are_RepValue__c);
                     if(e.Other_Other_Elevations__c!=NULL && e.Other_Other_Elevations__c.length()>1)
                         e.Other_Other_Elevations__c= toTitleCase(e.Other_Other_Elevations__c);
                     if( e.The_Roof_is_RepValue__c!=NULL && e.The_Roof_is_RepValue__c.length()>1 )
                         e.The_Roof_is_RepValue__c= toTitleCase(e.The_Roof_is_RepValue__c);
                     if( e.Staircases_RepValue__c!=NULL && e.Staircases_RepValue__c.length()>1)
                         e.Staircases_RepValue__c= toTitleCase(e.Staircases_RepValue__c);
                     if( e.Other_Roof__c!=NULL && e.Other_Roof__c.length()>1)
                         e.Other_Roof__c= toTitleCase(e.Other_Roof__c);
                     if( e.Covered_in_RepValue__c!=NULL && e.Covered_in_RepValue__c.length()>1)
                         e.Covered_in_RepValue__c= toTitleCase(e.Covered_in_RepValue__c);
                     if( e.Other_Covered_In__c!=NULL && e.Other_Covered_In__c.length()>1)
                         e.Other_Covered_In__c= toTitleCase(e.Other_Covered_In__c);
                     if(e.The_windows_are_Rep_Value__c!=NULL && e.The_windows_are_Rep_Value__c.length()>1  )
                         e.The_windows_are_Rep_Value__c= toTitleCase(e.The_windows_are_Rep_Value__c);
                    if(e.Window_Defects__c!=NULL && e.Window_Defects__c.length()>1  )
                         e.Window_Defects__c= toTitleCase(e.Window_Defects__c);
                     if(e.Frames_are_RepValue__c!=NULL && e.Frames_are_RepValue__c.length()>1 )
                         e.Frames_are_RepValue__c= toTitleCase(e.Frames_are_RepValue__c);
                     if( e.Other_Frames__c!=NULL && e.Other_Frames__c.length()>1)
                         e.Other_Frames__c= toTitleCase(e.Other_Frames__c);
                     if( e.Other_Staircases__c!=NULL && e.Other_Staircases__c.length()>1)
                         e.Other_Staircases__c= toTitleCase(e.Other_Staircases__c);
                     if( e.Parking_Type__c!=NULL && e.Parking_Type__c.length()>1 )
                         e.Parking_Type__c= toTitleCase(e.Parking_Type__c);
                     if( e.Parking_type_RepValue__c!=NULL && e.Parking_type_RepValue__c.length()>1 )
                         e.Parking_type_RepValue__c= toTitleCase(e.Parking_type_RepValue__c);
                     if( e.Other_Parking__c!=NULL && e.Other_Parking__c.length()>1)
                         e.Other_Parking__c= toTitleCase(e.Other_Parking__c);
                     if( e.Parking_surface_RepValue__c!=NULL && e.Parking_surface_RepValue__c.length()>1 )
                         e.Parking_surface_RepValue__c= toTitleCase(e.Parking_surface_RepValue__c);
                     if( e.Other_Parking_Surface__c!=NULL && e.Other_Parking_Surface__c.length()>1)
                         e.Other_Parking_Surface__c= toTitleCase(e.Other_Parking_Surface__c);
                     if( e.Loading_Bays__c!=NULL && e.Loading_Bays__c.length()>1)
                         e.Loading_Bays__c= toTitleCase(e.Loading_Bays__c);
                     if( e.Other_Loading_Bays__c!=NULL && e.Other_Loading_Bays__c.length()>1)
                         e.Other_Loading_Bays__c= toTitleCase(e.Other_Loading_Bays__c);
                     if( e.Other_Defects_Wall__c!=NULL && e.Other_Defects_Wall__c.length()>1)
                         e.Other_Defects_Wall__c= toTitleCase(e.Other_Defects_Wall__c);
                     if( e.Other_Defects_Roof__c!=NULL && e.Other_Defects_Roof__c.length()>1)
                         e.Other_Defects_Roof__c= toTitleCase(e.Other_Defects_Roof__c);
                     if( e.The_windows_are_Rep_Value__c!=NULL && e.The_windows_are_Rep_Value__c.length()>1 )
                         e.The_windows_are_Rep_Value__c= toTitleCase(e.The_windows_are_Rep_Value__c);
                     if( e.Other_Defects_Windows__c!=NULL && e.Other_Defects_Windows__c.length()>1)
                         e.Other_Defects_Windows__c= toTitleCase(e.Other_Defects_Windows__c);
                     if( e.Other_Windows__c!=NULL && e.Other_Windows__c.length()>1)
                         e.Other_Windows__c= toTitleCase(e.Other_Windows__c);
                     if( e.Other_Defects_Floor__c!=NULL && e.Other_Defects_Floor__c.length()>1)
                         e.Other_Defects_Floor__c= toTitleCase(e.Other_Defects_Floor__c);
                     if( e.Construction_Year__c!=NULL && e.Construction_Year__c.length()>1)
                         e.Construction_Year__c= toTitleCase(e.Construction_Year__c);
                     if(e.No_parking_spaces_RepValue__c!=NULL && e.No_parking_spaces_RepValue__c.length()>1)
                     {
                           e.Parking_type_RepValue__c= toTitleCase(e.No_parking_spaces_RepValue__c);
                     }
                     externalinspectionIdSet.add(e.id);
                    }
                    internalInspectionList = [Select id,name,External_Inspection__r.Name,Alarm_Fire_Smoke__c,Alarm_Intruder__c,Ancillary_Staircases_RepValue__c,
                                              Bathroom_Specification__c,Capacity__c,Ceiling_Defects__c,Clear_eaves_height_m__c,Concierge__c,Configuration__c,
                                              Construction_RepValue__c,Covering_RepValue__c,Decoration__c,Electricity__c,External_Inspection__c,Floor_Ceiling_Height__c,
                                              Floor_Defects__c,Services_Main__c,Gym__c,Has_the_tenant_s_made_improvements__c,Hazardous_Materials__c,Heating_RepValue__c,
                                              How_Many_Bathrooms__c,Internal_Wall_Defects__c,Kitchen_Specification__c,Light_Fixing_RepValue__c,Lighting_Type_RepValue__c,
                                              Main_Staircases_RepValue__c,No_Of_Person_Lifts__c,Number_Of_Lifts__c,Office_Type__c,Other_Ceilings__c,
                                              Other_InternaWalls__c,Other_LightFixings__c,Other_LightType__c,Other_Floor_Costruction__c,Rooflights_Other__c,
                                              Other_Floor_Covering__c,Other_Main_Staircase__c,Other_Ancillary_Staircase__c,Other_Heating__c,Other_Services__c,
                                              Other_Office_Type__c,Other_Defects_Ceiling__c,Other_Defects_Int_Walls__c,Other_Defects_Floors__c,Power_Distribution_RepValue__c,
                                              Roof_Lights_Wholly_Mainly__c,Specification__c,Swimming_Pool__c,The_Ceilings_are_RepValue__c,The_Internal_Walls_are_RepValue__c
                                              //Gas__c,Hot_Water__c,Mains_Drainage__c,Water__c,Name,Air_Conditioning__c,
                                              from Internal_Inspection__c
                                              where External_Inspection__c IN :externalinspectionIdSet LIMIT 999];
                    if(internalInspectionList!=NULL && internalInspectionList.size()>0)
                    {
                        for(Internal_Inspection__c intObj : internalInspectionList)
                        {   if( intObj.The_Ceilings_are_RepValue__c!=NULL && intObj.The_Ceilings_are_RepValue__c.length()>1 )
                            intObj.The_Ceilings_are_RepValue__c= toTitleCase(intObj.The_Ceilings_are_RepValue__c);
                         if( intObj.Other_Ceilings__c!=NULL && intObj.Other_Ceilings__c.length()>1 )
                             intObj.Other_Ceilings__c= toTitleCase(intObj.Other_Ceilings__c);
                         if( intObj.Has_the_tenant_s_made_improvements__c!=NULL && intObj.Has_the_tenant_s_made_improvements__c.length()>1 )
                             intObj.Has_the_tenant_s_made_improvements__c= toTitleCase(intObj.Has_the_tenant_s_made_improvements__c);
                         if( intObj.Configuration__c!=NULL && intObj.Configuration__c.length()>1 )
                             intObj.Configuration__c= toTitleCase(intObj.Configuration__c);
                         if( intObj.Office_Type__c!=NULL && intObj.Office_Type__c.length()>1 )
                         {
                            // intObj.Office_Type__c= toTitleCase(intObj.Office_Type__c);
                             List<String> elems = intObj.Office_Type__c.split(' ');
                               String rep_name = '';
                                for (String x : elems)
                                {
                                    rep_name += x.substring(0,1).toUpperCase()+x.substring(1,x.length()) + ' ';
                                }
                                intObj.Office_Type__c=rep_name;
                         }
                         if( intObj.Other_Office_Type__c!=NULL && intObj.Other_Office_Type__c.length()>1 )
                             intObj.Other_Office_Type__c= toTitleCase(intObj.Other_Office_Type__c);
                         if( intObj.Other_Defects_Floors__c!=NULL && intObj.Other_Defects_Floors__c.length()>1 )
                             intObj.Other_Defects_Floors__c= toTitleCase(intObj.Other_Defects_Floors__c);
                         if( intObj.Floor_Defects__c!=NULL && intObj.Floor_Defects__c.length()>1 )
                             intObj.Floor_Defects__c= toTitleCase(intObj.Floor_Defects__c);
                         if( intObj.Other_Defects_Ceiling__c!=NULL && intObj.Other_Defects_Ceiling__c.length()>1 )
                             intObj.Other_Defects_Ceiling__c= toTitleCase(intObj.Other_Defects_Ceiling__c);
                         if( intObj.Internal_Wall_Defects__c!=NULL && intObj.Internal_Wall_Defects__c.length()>1 )
                             intObj.Internal_Wall_Defects__c= toTitleCase(intObj.Internal_Wall_Defects__c);
                         if( intObj.Other_Defects_Int_Walls__c!=NULL && intObj.Other_Defects_Int_Walls__c.length()>1 )
                             intObj.Other_Defects_Int_Walls__c= toTitleCase(intObj.Other_Defects_Int_Walls__c);
                         if( intObj.Decoration__c!=NULL && intObj.Decoration__c.length()>1 )
                             intObj.Decoration__c= toTitleCase(intObj.Decoration__c);
                         if( intObj.Specification__c!=NULL && intObj.Specification__c.length()>1 )
                             intObj.Specification__c= toTitleCase(intObj.Specification__c);
                         if( intObj.Floor_Defects__c!=NULL && intObj.Floor_Defects__c.length()>1)
                             intObj.Floor_Defects__c= toTitleCase(intObj.Floor_Defects__c);
                         if(intObj.Light_Fixing_RepValue__c!=NULL && intObj.Light_Fixing_RepValue__c.length()>1 )
                             intObj.Light_Fixing_RepValue__c= toTitleCase(intObj.Light_Fixing_RepValue__c);
                         if( intObj.Other_LightFixings__c!=NULL && intObj.Other_LightFixings__c.length()>1 )
                             intObj.Other_LightFixings__c= toTitleCase(intObj.Other_LightFixings__c);
                         if(intObj.Lighting_Type_RepValue__c!=NULL && intObj.Lighting_Type_RepValue__c.length()>1 )
                             intObj.Lighting_Type_RepValue__c= toTitleCase(intObj.Lighting_Type_RepValue__c);
                         if( intObj.Other_LightType__c!=NULL && intObj.Other_LightType__c.length()>1 )
                             intObj.Other_LightType__c= toTitleCase(intObj.Other_LightType__c);
                         if(intObj.The_Internal_Walls_are_RepValue__c!=NULL && intObj.The_Internal_Walls_are_RepValue__c.length()>1)
                             intObj.The_Internal_Walls_are_RepValue__c= toTitleCase(intObj.The_Internal_Walls_are_RepValue__c);
                         if( intObj.Other_InternaWalls__c!=NULL && intObj.Other_InternaWalls__c.length()>1 )
                             intObj.Other_InternaWalls__c= toTitleCase(intObj.Other_InternaWalls__c);
                         if( intObj.Construction_RepValue__c!=NULL && intObj.Construction_RepValue__c.length()>1 )
                             intObj.Construction_RepValue__c= toTitleCase(intObj.Construction_RepValue__c);
                         if( intObj.Other_Floor_Costruction__c!=NULL && intObj.Other_Floor_Costruction__c.length()>1 )
                             intObj.Other_Floor_Costruction__c= toTitleCase(intObj.Other_Floor_Costruction__c);
                         if( intObj.Covering_RepValue__c!=NULL && intObj.Covering_RepValue__c.length()>1 )
                             intObj.Covering_RepValue__c= toTitleCase(intObj.Covering_RepValue__c);
                         if(intObj.Power_Distribution_RepValue__c!=NULL && intObj.Power_Distribution_RepValue__c.length()>1 )
                             intObj.Power_Distribution_RepValue__c= toTitleCase(intObj.Power_Distribution_RepValue__c);
                         if( intObj.Other_Floor_Covering__c!=NULL && intObj.Other_Floor_Covering__c.length()>1 )
                             intObj.Other_Floor_Covering__c= toTitleCase(intObj.Other_Floor_Covering__c);
                         if( intObj.Main_Staircases_RepValue__c!=NULL && intObj.Main_Staircases_RepValue__c.length()>1 )
                             intObj.Main_Staircases_RepValue__c= toTitleCase(intObj.Main_Staircases_RepValue__c);  
                         if(intObj.Ancillary_Staircases_RepValue__c!=NULL && intObj.Ancillary_Staircases_RepValue__c.length()>1 )
                             intObj.Ancillary_Staircases_RepValue__c= toTitleCase(intObj.Ancillary_Staircases_RepValue__c);
                         if( intObj.Other_Ancillary_Staircase__c!=NULL && intObj.Other_Ancillary_Staircase__c.length()>1 )
                             intObj.Other_Ancillary_Staircase__c= toTitleCase(intObj.Other_Ancillary_Staircase__c);
                         if(intObj.Bathroom_Specification__c!=NULL && intObj.Bathroom_Specification__c.length()>1 )
                             intObj.Bathroom_Specification__c= toTitleCase(intObj.Bathroom_Specification__c);
                         if(intObj.Kitchen_Specification__c!=NULL && intObj.Kitchen_Specification__c.length()>1  )
                             intObj.Kitchen_Specification__c= toTitleCase(intObj.Kitchen_Specification__c);
                         if( intObj.Other_Heating__c!=NULL && intObj.Other_Heating__c.length()>1 )
                             intObj.Other_Heating__c= toTitleCase(intObj.Other_Heating__c);
                         if( intObj.Heating_RepValue__c!=NULL && intObj.Heating_RepValue__c.length()>1 )
                             intObj.Heating_RepValue__c= toTitleCase(intObj.Heating_RepValue__c);
                         
                         system.debug('internalInspectionMap111---------->'+internalInspectionMap);
                         if(intObj.External_Inspection__c!=NULL)
                         {system.debug('internalInspectionMap22    ---------->'+internalInspectionMap);
                          if(internalInspectionMap.containsKey(intObj.External_Inspection__c))
                          {
                              internalInspectionMap.get(intObj.External_Inspection__c).add(intObj);
                          }
                          else 
                          {
                              List<Internal_Inspection__c> intList = New List<Internal_Inspection__c>();
                              intList.add(intObj);
                              internalInspectionMap.put(intObj.External_Inspection__c,intList);
                          }   
                         }
                        }
                    }
                    for(External_Inspection__c extObj : externalInspectionList)
                    {
                        if(internalInspectionMap.containsKey(extObj.id))
                            ExternalWrapperList.add(New ExternalWrapper(extObj,internalInspectionMap.get(extObj.id),true));
                        else
                            ExternalWrapperList.add(New ExternalWrapper(extObj,false));
                    }
                    
                    
                    
                }
            }
        }
    }
    public Class ExternalWrapper
    {
        public External_Inspection__c extObj{get;set;}
        public list<Internal_Inspection__c> intInspectionList{get;set;}
        public Boolean hasInternal{get;set;}
        public ExternalWrapper(External_Inspection__c extObj,Boolean hasInternal)
        {
            this.extObj = extObj;
            this.hasInternal = hasInternal;
        }
        public ExternalWrapper(External_Inspection__c extObj, List<Internal_Inspection__c> intInspectionList,Boolean hasInternal)
        {
            this.extObj = extObj;
            this.intInspectionList = intInspectionList;
            this.hasInternal = hasInternal;
        }
    }
    
    public Class SurroudningWrapper
    {
        public Surrounding__c surObj{get;set;}
        public SurroudningWrapper(Surrounding__c surObj)
        {
            this.surObj = surObj;
        }
        
    }
    
    public String toTitleCase(String phrase){
        String newPhrase = phrase.toLowerCase();
        newPhrase = newPhrase.substring(0,1).toUpperCase()+ newPhrase.substring(1);
        return newPhrase;
    }
    
}