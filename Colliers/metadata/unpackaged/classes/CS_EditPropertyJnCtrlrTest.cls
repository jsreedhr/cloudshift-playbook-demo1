@isTest
private class CS_EditPropertyJnCtrlrTest {
   
    Public static Account a;
    static Contact c;
    static opportunity opp;
    static Property__c property;
    static Company_Property_Affiliation__c CmpnyPrptyJn;
    static Job_Property_Junction__c JobPrptyJn;
   
    
     static testmethod void setupData(){
        //create account
        a = TestObjectHelper.createAccount();
        insert a ;
        
        property=TestObjectHelper.createProperty();
        property.Street_No__c ='Park Lane';
        property.Address_Validation_Status__c = 'Verified';
        property.Post_Code__c ='W1K 3DD';
        property.Country__c='United Kingdom';
        property.Geolocation__Latitude__s=51.51;
        property.Geolocation__Longitude__s=-0.15;
        insert property;
        
        
   
        
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
        CmpnyPrptyJn= TestObjectHelper.CmpnyPrptyJn(a);
        insert CmpnyPrptyJn;
        
        JobPrptyJn=  TestObjectHelper.JobPrptyJn(opps[0],property);
        insert JobPrptyJn;
        opp = [Select id from opportunity Limit 1];
        
      
    }
    private static testMethod void test1() {
         setupData();
        opp = [Select id from opportunity Limit 1];
       
        Test.setCurrentPageReference(new PageReference('Page.CS_PropertyJnView'));

        System.currentPageReference().getParameters().put('id', opp.id);
        System.currentPageReference().getParameters().put('Selids', property.id);
        
        Test.startTest();
        
        CS_EditProproprtyJunctionController ctrl = new CS_EditProproprtyJunctionController();  
        ctrl.getCategorieskeypreix();
         ctrl.getCategories();
        ctrl.getNewCategories();
        ctrl.SelectedProperty();
        //ctrl.SlctedIds=prpty.id;
        //ctrl.SelectedProperty();
        ctrl.SaveRecords();
       // system.assertequals(0,ctrl.JobPrptyJn.size());
        
       /* CS_EditInvoiceController.booleanCostWrapper cstWrap= new CS_EditInvoiceController.booleanCostWrapper(true,Cost);
        CS_EditInvoiceController.booleanAllocationeWrapper allocWrap  = new CS_EditInvoiceController.booleanAllocationeWrapper(true,alloc,100.00,20000.00);
        CS_EditInvoiceController.CostCategoryWrapper cstCategory = new CS_EditInvoiceController.CostCategoryWrapper(true,'test',10.00,5.00,70.00,'testRecord');
        CS_EditInvoiceController.booleanInvoiceWrapper invWrap = new CS_EditInvoiceController.booleanInvoiceWrapper(true,po);*/
        
       
        Test.stopTest();


    }
     private static testMethod void test2() {
         setupData();
        opp = [Select id from opportunity Limit 1];
       
        Test.setCurrentPageReference(new PageReference('Page.CS_PropertyJnView'));

        System.currentPageReference().getParameters().put('id', a.id);
        System.currentPageReference().getParameters().put('Selids', property.id);
        
        Test.startTest();
        
        CS_EditProproprtyJunctionController ctrl = new CS_EditProproprtyJunctionController(); 
        ctrl.getCategorieskeypreix();
        ctrl.getCategories();
        ctrl.getNewCategories();
       
        ctrl.SelectedProperty();
        ctrl.IdtoDelete = '0';
        ctrl.deleteSelectedRow();
        //ctrl.SlctedIds=prpty.id;
        //ctrl.SelectedProperty();
        ctrl.SaveRecords();
        
       /* CS_EditInvoiceController.booleanCostWrapper cstWrap= new CS_EditInvoiceController.booleanCostWrapper(true,Cost);
        CS_EditInvoiceController.booleanAllocationeWrapper allocWrap  = new CS_EditInvoiceController.booleanAllocationeWrapper(true,alloc,100.00,20000.00);
        CS_EditInvoiceController.CostCategoryWrapper cstCategory = new CS_EditInvoiceController.CostCategoryWrapper(true,'test',10.00,5.00,70.00,'testRecord');
        CS_EditInvoiceController.booleanInvoiceWrapper invWrap = new CS_EditInvoiceController.booleanInvoiceWrapper(true,po);*/
        
       
        Test.stopTest();


    }
     private static testMethod void test3() {
         setupData();
        opp = [Select id from opportunity Limit 1];
       
        Test.setCurrentPageReference(new PageReference('Page.CS_PropertyJnView'));

        System.currentPageReference().getParameters().put('id', a.id);
        System.currentPageReference().getParameters().put('Selids', property.id);
        
        Test.startTest();
        
        CS_EditProproprtyJunctionController ctrl = new CS_EditProproprtyJunctionController(); 
        ctrl.getCategorieskeypreix();
        ctrl.getCategories();
         ctrl.selectAllProperty();
       ctrl.first();
       ctrl.Last();
       ctrl.Previous();
       ctrl.Next();
        //ctrl.deleteSelectedRow();
       ctrl.firstfilter();
        //ctrl.hasPrevious();
        
        delete JobPrptyJn;
       
        Test.stopTest();


    }
}