/**
 *  Class Name: Test_codingStructureTriggerHandler 
 *  Description: Test class for the codingStructureTriggerHandler
 *  Company: dQuotient
 *  CreatedDate: 24/10/2016 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nidheesh               24/10/2016                  Orginal Version          
 *  Kaspars Rezgalis       30/07/2018                  Some setup data (e.g. AssignId__c) to be created as part of createData() and method updated not to be @testSetup (which has sissues with unique IDs)  
 *
 */
@isTest
private class Test_cs_contactRetrieval {

   
        static Account accountObj;
  static Opportunity oppObj;
  static opportunity oppcareof;
  static Contact contactObj;
  static Invoice__c invoiceObj;
  static User userObj;
  static Staff__c staffObj;
  static Allocation__c allocationObj;
  static Property__c propertyObj;
  static Purchase_Order__c purOrderObj;
  static Job_Property_Junction__c jobPropObj;
  static Forecasting__c forecastObj;
  static Disbursements__c disbursementObj;
  static MLR__c mlrObj;
  static Address__c addressObj;
  static Account_Address_Junction__c accountAddressJuncObj;
  static Purchase_Order_Invoice_Junction__c purOrderInvJuncObj;
 //  @testsetup
  private static void createData()
  {
    AssignId__c assignId = TestObjectHelper.createAssignId();
    assignId.AssignNo__c = 1;
    AssignAccountId__c assignAccId = TestObjectHelper.createAssignAccCode();
    colliers_Vat_Number__c vatNum = (colliers_Vat_Number__c)SmartFactory.createSObject('colliers_Vat_Number__c',false);
    currency_Symbol__c curr= (currency_Symbol__c)SmartFactory.createSObject('currency_Symbol__c',false);

    Database.Insert(new List<SObject>{assignId, assignAccId, vatNum, curr}, true);

    accountObj = TestObjectHelper.createAccount();
    accountObj.PO_Required__c = 'Yes';
    accountobj.Company_Status__c = 'Active';
      insert accountobj;
      System.assert(accountObj.id!=NULL);
      
      contactObj = TestObjectHelper.createContact(accountObj);
      contactobj.lastName='Tes6t';
      contactobj.Email ='test1@tes6t1.com';
      insert contactobj;
      System.assert(contactobj.id!=NULL);
      
    //   AccountContactRelation acctcontrel = new AccountContactRelation();
    //   acctcontrel.Accountid = accountObj.id;
    //   acctcontrel.ContactId = contactObj.id;
    //   insert acctcontrel;
      
      propertyObj = TestObjecthelper.createProperty();
      propertyObj=TestObjectHelper.createProperty();
        propertyObj.Street_No__c ='Park Lane';
        propertyObj.Post_Code__c ='W1K 3DD';
        propertyObj.Country__c = 'United Kingdom';
        propertyObj.Geolocation__Latitude__s=51.51;
        propertyObj.Geolocation__Longitude__s=-0.15;
      insert propertyObj;
        AssignId__c objAssign = new AssignId__c();
         objAssign.Name = 'test';
         objAssign.Invoice_Pattern__c = 'pattern';
         objAssign.AssignNo__c = 123;
         insert objAssign;
       AssignAccountId__c objAssignAccount = new AssignAccountId__c();
        objAssignAccount.Name = 'test';
        objAssignAccount.AccountNumber__c = 123;
        objAssignAccount.Account_Pattern__c = '123';
        insert objAssignAccount;
      System.assert(propertyObj.id!=NULL);
      addressObj = TestObjecthelper.createAddress(accountObj);
      addressObj.Country__c = 'United Kingdom';
      insert addressObj;
      System.assert(addressObj.id!=NULL);
      
      oppObj = TestObjectHelper.createOpportunity(accountObj);
      oppObj.Invoicing_Company2__c = accountObj.id;
      oppObj.Invoicing_Address__c = addressObj.id;
      oppObj.Engagement_Letter__c = true;
      insert oppObj;
      System.assert(oppObj.id!=NULL);
      
       Care_Of__c careofobj = TestObjectHelper.createCareOf(accountObj,contactObj);
       careofObj.Colliers_Int__c = true;
       insert careofobj;
       
      
       oppcareof = TestObjectHelper.createOpportunity(accountObj);
      oppcareof.Invoicing_Care_Of__c = careofobj.id;
      oppcareof.Invoicing_Address__c = addressObj.id;
      oppcareof.Engagement_Letter__c = true;
      insert oppcareof;
      
      jobPropObj = TestObjecthelper.JobPrptyJn(oppObj,propertyObj);
      insert jobPropObj;
      System.assert(jobPropObj.id!=NULL);
     

  }

  
    static testMethod void testCS_codingStructureTriggerHandler(){
        createData();
        test.starttest();
        ContanctRetrievalController cont1 = new ContanctRetrievalController();
        ApexPages.currentPage().getParameters().put('id',accountObj.id);
        cont1.accntid = accountObj.id;
        cont1.searchparam='instructingcnt';
        ApexPages.currentPage().getParameters().put('isCareof','no');
        
        //ApexPages.currentPage().putParameters(contactObj.id);
        cont1.BuildQuery();
        List<contact> contlist = cont1.getCurrentAccountList();
        cont1.SortToggle();
        
         ContanctRetrievalController cont3 = new ContanctRetrievalController();
        ApexPages.currentPage().getParameters().put('id',accountObj.id);
        cont3.accntid = accountObj.id;
        cont3.searchparam='instructingcnt';
        ApexPages.currentPage().getParameters().put('isCareof','yes');
        
        //ApexPages.currentPage().putParameters(contactObj.id);
        cont3.BuildQuery();
        
          ContanctRetrievalController cont2 = new ContanctRetrievalController();
        ApexPages.currentPage().getParameters().put('searchbox','instructingcnt');
        cont2.accntid = accountObj.id;
        ApexPages.currentPage().getParameters().put('isCareof','no');
         cont2.BuildQuery();
        
}
}