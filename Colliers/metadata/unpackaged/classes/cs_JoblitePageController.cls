/**
 *  Class Name: cs_JoblitePageController
 *  Description: Class to implement create and edit jobs(recordType == joblite)
 *  Company: dQuotient
 *  CreatedDate:26/12/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Nidheesh             26/12/2016               Orginal Version
 *
 */
 public without sharing class cs_JoblitePageController{
    public String oppId{get;set;}
    public string sitename{get;set;}
    public String oathcode{get;set;}
    private String accId{get;set;}
    private Id jobrecord{get;set;}
    private Id jobLiteREcord{get;set;}
    public Opportunity jobDetails{get;set;}
    private string recordtypeofjob{get;set;}
    public List<SelectOption> staffDepLst{get;set;}
    public List<SelectOption> staffOfcLst{get;set;}
    public List<SelectOption> staffWorkTypeLst{get;set;}
    public List<Other_Party__c> otherpartylist{get;set;}
    public List<String> errMsgLst {get;set;}
    public Boolean showErrMsg{get;set;}
    public String errorMessage {get;set;}
    public set<String> ofcSet;
    public Set<String> deptSet;
    public set<String> wrkTypSet;
    public Boolean authorized{get;set;}
    public Boolean guestuser{get;set;}
    public boolean isDisabledTab{get;set;}
    
    // Constructor
    public cs_JoblitePageController() {
        isDisabledTab = false;
        sitename = Label.siteAddress;
        Boolean authorized = true;
        List<UserLogin__c> lstofuserlogin = new List<UserLogin__c>();
        List<Account> lstAccount = new List<Account>();
        otherpartylist =new List<Other_Party__c>();
        guestuser = false;
        oppId = ApexPages.currentPage().getParameters().get('id');
        oathcode = ApexPages.currentPage().getParameters().get('auth');
        string jobrecordvalue= Label.job_instructed_recordLabel; 
        string jobLiteREcordvalue = Label.job_lite_recordLabel;
        jobrecord = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(jobrecordvalue).getRecordTypeId();
        jobLiteREcord = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(jobLiteREcordvalue).getRecordTypeId();
         if(UserInfo.getUserType()!='Standard')
        {
            if(ApexPages.currentPage().getParameters().containsKey('auth')){
            oathcode = ApexPages.currentPage().getParameters().get('auth');
            system.debug('Object msg'+oathcode);
            if(!string.isBlank(oathcode) && oathcode!= null){
                guestuser = true;
                lstofuserlogin = [select id,Name,Auth_Code__c from UserLogin__c where Auth_Code__c = : oathcode];
                if(!lstofuserlogin.isEmpty()){
                    authorized = true;
                }else if(lstofuserlogin.isEmpty()){
                    authorized = false;
                }
            }else{
                authorized = false;
            }
        }else{
            authorized = true;
        }
        }else{
            authorized = true;
        }
        
        if(authorized){
            if(!string.isBlank(oppId)){
                
                isDisabledTab = true;
                initCompaniesContacts();
                getStaffDept();
                getStaffDeptOffice();
                getStaffDeptOfcWorkType();
            }else{
                guestuser = false;
                jobDetails = new Opportunity();  
                
                staffDepLst= new List<SelectOption>();
                staffDepLst.add(new SelectOption('','-- None --'));
                
                staffOfcLst= new List<SelectOption>();
                staffOfcLst.add(new SelectOption('','-- None --'));
                
                staffWorkTypeLst= new List<SelectOption>();
                staffWorkTypeLst.add(new SelectOption('','-- None --'));
                
                if(ApexPages.currentPage().getParameters().containsKey('AccId')){
                    accId = ApexPages.currentPage().getParameters().get('AccId');
                    if(!String.isBlank(accId) && (accId InstanceOf Id)){
                        lstAccount = [Select id, name From Account where id =:accId limit 1];
                        if(!lstAccount.isEmpty()){
                            jobDetails.AccountId = lstAccount[0].id;

                        }
                    }
                }
            }
        }else{
            // redirect();
        }
    }
    
    public pageReference redirectToconflict(){
        PageReference OpporunityPageredirectconflict = new PageReference('/CS_conflictcheck_clone?auth='+oathcode +'&id='+oppId);
        OpporunityPageredirectconflict.setRedirect(true);
        return OpporunityPageredirectconflict;
    }
    public pageReference redirect(){
        PageReference OpporunityPageredirect = new PageReference(sitename+'/CS_SiteLoginPage' );
        OpporunityPageredirect.setRedirect(true);
        return OpporunityPageredirect;
    }
    
    /**
     *  Method Name: save
     *  Description: Method to update the job Details
     *  Param:  None
     *  Return: None
    */    
    public pageReference save(){
        //System.debug('Object msg'+otherpartylist);
        //otherpartylist.clear();
        string nameofaccount = apexpages.currentPage().getParameters().get('accountname');
        List<Account> lstofaccount = new List<Account>();
        if(nameofaccount != null && string.isNotBlank(nameofaccount)){
            lstofaccount = [select id,name from account where Name =:nameofaccount AND Company_Status__c != 'Archived'];
        }
        
        string recordtypename = Label.accountRecordType;
        id recordtypeofacc = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordtypename).getRecordTypeId();
        List<Job_Property_Junction__c> newjobProp = new List<Job_Property_Junction__c>();
        Boolean isInsrt = true;
        List<Coding_Structure__c> codingStrLst = new List<Coding_Structure__c>();
        Opportunity opp = new Opportunity();
        
        errMsgLst = new List<String>();
            if(string.isNotBlank(oppId)){
                opp.Id = jobDetails.Id;
            }
            if(!string.isBlank(jobDetails.AccountId)){    
                opp.AccountId = jobDetails.AccountId;
            }
            else if(!lstofaccount.isEmpty()){
                opp.AccountId = lstofaccount[0].id;
            }else{
                if(!string.isBLANK(jobDetails.Client_company_name__c)){
                }else{
                    
                    isInsrt = false;
                    showErrMsg = true;
                    errMsgLst.add('Please Fill the Instructing company Or Company Name Free Text');
                }
            }
            if(!string.isBLANK(jobDetails.Client_company_name__c)){
                Map<string ,Account> nameToAcount = new Map<String,Account>();
                
                opp.Client_company_name__c = jobDetails.Client_company_name__c;
                string defaultcompany = 'Default Company';
                List<Account> objaccount = [SELECT Id, Name FROM Account WHERE (Name=:jobDetails.Client_company_name__c OR Name=:defaultcompany) AND RecordTypeId = :recordtypeofacc AND (Company_Status__c = 'Active' OR Company_Status__c = 'Pending')];               
                if(!objaccount.isEmpty()){
                    for(Account obj:objaccount){
                        if(obj.Name == jobDetails.Client_company_name__c){
                            nameToAcount.put(jobDetails.Client_company_name__c,obj);
                        }else if(obj.Name == defaultcompany){
                            nameToAcount.put(defaultcompany,obj);
                        }
                    }
                     if(nameToAcount.containsKey(jobDetails.Client_company_name__c)){    
                          if(opp.AccountId == null){
                            opp.AccountId = nameToAcount.get(jobDetails.Client_company_name__c).Id ;  
                          }
                    }else if(nameToAcount.containsKey(defaultcompany)){
                        if(opp.AccountId == null){
                            opp.AccountId = nameToAcount.get(defaultcompany).Id ;  
                        }
                    }
                }
                if(opp.AccountId == null){
                    opp.AccountId = Label.Default_Company;
                }
                if(objaccount.isEmpty()){
                    string accid = Label.Default_Company;
                    if(string.isNotBlank(accid)){
                        opp.AccountId =accid;
                    }else{
                        System.debug('checking save');
                        isInsrt = false;
                        showErrMsg = true;
                        errMsgLst.add('Default account has been removed, please contact the system administrator.');
                    }
                    
                }
               
                 
            }
            if(!lstofaccount.isEmpty()){
                opp.AccountId = lstofaccount[0].id;
            }
            if(!string.isBlank(jobDetails.Name)){
                opp.Name=jobDetails.Name; 
                
            }else{
                opp.Name = 'X';
               /* isInsrt = false;
                showErrMsg = true;
                errMsgLst.add('Please Enter The Job Name.');*/
            }
            
            if(!string.isBlank(jobDetails.Client_contact_name__c)){
                opp.Client_contact_name__c = jobDetails.Client_contact_name__c;    
            }else {
                isInsrt = false;
                showErrMsg = true;
                errMsgLst.add('Please Fill the Contact Name.');
            }
            if(!string.isBLANK(jobDetails.Manager__c)){
                opp.Manager__c = jobDetails.Manager__c;
            }else{
                isInsrt = false;
                showErrMsg = true;
                errMsgLst.add('Please Select the Job Manager.');
                
            }
            if(!string.isBLANK(jobDetails.Department__c)){
                opp.Department__c = jobDetails.Department__c;
            }else{
                isInsrt = false;
                showErrMsg = true;
                errMsgLst.add('Please Select Job Manager Department.');
            }
            if(!string.isBLANK(jobDetails.Work_Type__c)){
                opp.Work_Type__c = jobDetails.Work_Type__c;
            }else{
                isInsrt = false;
                showErrMsg = true;
                errMsgLst.add('Please Select the Job Manager Work Type');
            }
            if(!string.isBLANK(jobDetails.Office__c)){
                opp.Office__c = jobDetails.Office__c;
                
                codingStrLst = [SELECT id,name,Department__c,Office__c,Work_Type__c,Staff__c,Work_Type__r.Name from Coding_Structure__c where Staff__c=:jobDetails.Manager__c AND Department__c=:jobDetails.Department__c AND Office__c=:jobDetails.Office__c AND Work_Type__r.Name=:jobDetails.Work_Type__c limit 1]; 
                if(codingStrLst.size()>0){
                    System.debug('checking save');
                    opp.Coding_Structure__c = codingStrLst[0].id;
                }
            }else{
                isInsrt = false;
                showErrMsg = true;
                errMsgLst.add('Please Select the Job Manager Office.');
            }
            if(isInsrt == true){
                opp.StageName = 'Job Lite';
                if(!string.isBLANK(jobDetails.Property_Address__c)){
                    opp.Property_Address__c = jobDetails.Property_Address__c;
                }
                opp.CloseDate = System.today();
                opp.RecordTypeId = jobLiteREcord;
            }
            if(isInsrt == true){
                boolean valuesave = false;
                set<Id> propertyids = new set<Id>();
                System.debug('checking save');
                try{
                    upsert opp ;
                    valuesave = true;
                    System.debug('checking save');
                }catch(Exception e){
                    string value = string.valueOf(e);
                    isInsrt = false;
                    showErrMsg = true;
                    string labelvalue = Label.ErrorMessage;
                    if(value.contains(labelvalue)){
                        errMsgLst.add('Value does not exist or does not match filter criteria.: [Manager__c]');
                    }else{
                        errMsgLst.add(''+e.getmessage());
                    }
                    
                    
                    System.debug('checking save');
                    
                    valuesave = false;
                }
                if(valuesave){
                if(!string.isBLANK(oppId)){
                    /*if(!jobDetails.Job_Property_Junction__r.isEmpty()){
                        for(Job_Property_Junction__c  obj : jobDetails.Job_Property_Junction__r){
                            if(!string.isBLANK(obj.Property__c)){
                                propertyids.add(obj.Property__c);
                            }
                        }   
                    }
                    if(!string.isBLANK(jobDetails.Property_Address__c)){
                        if(!propertyids.isEmpty()){
                            if(!propertyids.contains(jobDetails.Property_Address__c)){
                                Job_Property_Junction__c newobjpro = new Job_Property_Junction__c();
                                newobjpro.Property__c = jobDetails.Property_Address__c;
                                newobjpro.Job__c = oppId;
                                newjobProp.add(newobjpro);
                            }
                        }else{
                            Job_Property_Junction__c newobjpro = new Job_Property_Junction__c();
                            newobjpro.Property__c = jobDetails.Property_Address__c;
                            newobjpro.Job__c = oppId;
                            newjobProp.add(newobjpro);
                        }
                    }
                    if(!newjobProp.isEmpty()){
                        upsert newjobProp;
                    }*/
                   /* otherpartylist = new List<Other_Party__c>();
                    for(Integer i=0;i<3;i++){
                        Other_Party__c newobj = new Other_Party__c();
                        otherpartylist.add(newobj);
                    }*/
                    PageReference OpporunityPageCancel = new PageReference('/apex/cs_MainJobLitePage?id='+oppId +'&auth='+oathcode);
                    OpporunityPageCancel.setRedirect(true);
                    return OpporunityPageCancel;
                }else{
                  
                        List<Opportunity> oppobject = new List<Opportunity>();
                         oppobject = [SELECT Id,Name,Property_Address__c,Job_Number__c,(SELECT Id,Property__c,Job__c FROM Job_Property_Junction__r ) FROM Opportunity Where id=:opp.Id  ];
                     
                    if(!oppobject.isEmpty()){
                    //     
                    // if(!oppobject[0].Job_Property_Junction__r.isEmpty()){
                    //     for(Job_Property_Junction__c  obj : oppobject[0].Job_Property_Junction__r){
                    //         if(!string.isBLANK(obj.Property__c)){
                    //             propertyids.add(obj.Property__c);
                    //         }
                    //     }   
                    // }
                    // if(!string.isBLANK(oppobject[0].Property_Address__c)){
                    //     if(!propertyids.isEmpty()){
                    //         if(!propertyids.contains(oppobject[0].Property_Address__c)){
                    //             Job_Property_Junction__c newobjpro = new Job_Property_Junction__c();
                    //             newobjpro.Property__c = oppobject[0].Property_Address__c;
                    //             newobjpro.Job__c = oppobject[0].Id;
                    //             newjobProp.add(newobjpro);
                    //         }
                    //     }else{
                    //         Job_Property_Junction__c newobjpro = new Job_Property_Junction__c();
                    //         newobjpro.Property__c = oppobject[0].Property_Address__c;
                    //         newobjpro.Job__c = oppobject[0].Id;
                    //         newjobProp.add(newobjpro);
                    //     }
                        
                    // }
                    // if(!newjobProp.isEmpty()){
                    //     upsert newjobProp;
                    // }
                    PageReference OpporunityPageCancel = new PageReference('/apex/cs_MainJobLitePage?id='+oppobject[0].id+'&auth='+oathcode );
                    OpporunityPageCancel.setRedirect(true);
                    return OpporunityPageCancel;
                    }
                }
            }
            }else{
                System.debug('checking save');
                system.debug('errMsgLst'+errMsgLst);
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Record Cannot be Saved.'));  
                //errorMessage = '';
            }
        
        return  null;
    }
     /**
     *  Method Name: initCompaniesContacts
     *  Description: Method to get the Job Details
     *  Param:  None
     *  Return: None
    */
    public void initCompaniesContacts(){
        oppId = ApexPages.currentPage().getParameters().get('id');
        if(!string.isBlank(oppId)){ 
            showErrMsg = false;
            otherpartylist = new List<Other_Party__c>();
            jobDetails = [SELECT id,name,Account.Name,StageName,Job_Number__c,Account.id,Company_Name_System_Lookup__c,Amount,Property_Address__c,CloseDate,Client_contact_name__c,Client_company_name__c,Manager__c,Department__c,Manager__r.Name,Work_Type__c,Office__c,RecordTypeId,(SELECT Id,Property__c,Job__c FROM Job_Property_Junction__r ),(select id,Company__c,Company_Role__c from Other_Parties__r) from Opportunity where id=:oppId];   
            if(jobDetails != null){
                if(!jobDetails.Other_Parties__r.isEmpty()){
                    if(jobDetails.Other_Parties__r.size()>=3){
                        for(Integer i=0;i<3;i++){
                            otherpartylist.add(jobDetails.Other_Parties__r[i]);
                        }
                    }else{
                        otherpartylist.addAll(jobDetails.Other_Parties__r);
                        Integer x= 3 -jobDetails.Other_Parties__r.size();
                        for(Integer i=0;i<x;i++){
                            Other_Party__c newobj = new Other_Party__c();
                            otherpartylist.add(newobj);
                        }
                    }
                }else{
                    for(Integer i=0;i<3;i++){
                        Other_Party__c newobj = new Other_Party__c();
                        otherpartylist.add(newobj);
                    }
                }
            }
        }
    }
     /**
     *  Method Name: initCompaniesContacts
     *  Description: Method to get the Job Details
     *  Param:  None
     *  Return: None
    */
    public pageReference ConvertToJob(){
        oppId = ApexPages.currentPage().getParameters().get('id');
        if(!string.isBlank(oppId)){ 
            List<worktype_Job_Related_to__c> lstofworkTypeMapping = new List<worktype_Job_Related_to__c>();
            errMsgLst = new List<String>();
            Opportunity opp = [SELECT Id,StageName,Job_Number__c,Work_Type__c, AccountId, Account.Company_Status__c FROM Opportunity WHERE Id =:oppId Limit 1];
            if(opp.Work_Type__c != null && string.isNotBlank(opp.Work_Type__c)){
                lstofworkTypeMapping = [SELECT Id,Job_related_field__c,worktype_Name__c FROM worktype_Job_Related_to__c WHERE worktype_Name__c =: opp.Work_Type__c ];
                if(!lstofworkTypeMapping.isEmpty()){
                    opp.Relates_To__c = lstofworkTypeMapping[0].Job_related_field__c;
                }
            
            }
            if(opp.AccountId != null && opp.Account.Company_Status__c ==  'Active'){
                opp.StageName = 'Instructed';
                opp.RecordTypeId = jobrecord;
                upsert opp;
                
                PageReference OpporunityPageCancel = new PageReference('/apex/CS_ManageMyJobPage?id='+oppId +'&auth='+oathcode);
                OpporunityPageCancel.setRedirect(true);
                return OpporunityPageCancel;  
            }else{
                // isInsrt = false;
                showErrMsg = true;
                errMsgLst.add('You cannot convert to a job on a Company with Company Status as Pending.');
                return null;
            }
            
        }else if(string.isBlank(oppId)){
            Boolean isInsrt = true;
            List<Coding_Structure__c> codingStrLst = new List<Coding_Structure__c>();
            Opportunity opp = new Opportunity();
        
            errMsgLst = new List<String>();
            if(!string.isNotBlank(jobDetails.Company_Name_System_Lookup__c)){    
                opp.Company_Name_System_Lookup__c = jobDetails.Company_Name_System_Lookup__c;
            }else{
                if(!string.isBLANK(jobDetails.Client_company_name__c)){
                }else{
                    
                    isInsrt = false;
                    showErrMsg = true;
                    errMsgLst.add('Please Fill the Company Name Lookup Or Company Name Free Text');
                }
            }
            if(!string.isBLANK(jobDetails.Client_company_name__c)){
                Map<string ,Account> nameToAcount = new Map<String,Account>();
                opp.StageName = 'Job Lite';
                opp.Client_company_name__c = jobDetails.Client_company_name__c;
                string defaultcompany = 'Default Company';
                List<Account> objaccount = [SELECT Id, Name FROM Account WHERE Name=:jobDetails.Client_company_name__c OR Name=:defaultcompany ];               
                if(!objaccount.isEmpty()){
                    for(Account obj:objaccount){
                        if(obj.Name == jobDetails.Client_company_name__c){
                            nameToAcount.put(jobDetails.Client_company_name__c,obj);
                        }else if(obj.Name == defaultcompany){
                            nameToAcount.put(defaultcompany,obj);
                        }
                    }
                }
                if(nameToAcount.containsKey(jobDetails.Client_company_name__c)){                    
                    opp.AccountId = nameToAcount.get(jobDetails.Client_company_name__c).Id ;                    
                }else if(nameToAcount.containsKey(defaultcompany)){
                    opp.AccountId = nameToAcount.get(defaultcompany).Id ;  
                }       
            }
            if(!string.isBlank(jobDetails.Name)){
                opp.Name=jobDetails.Name; 
            
            }else{
                opp.Name = 'X';
               /* isInsrt = false;
                showErrMsg = true;
                errMsgLst.add('Please Enter The Job Name.');*/
            }           
            
            if(!string.isBlank(jobDetails.Client_contact_name__c)){
                opp.Client_contact_name__c = jobDetails.Client_contact_name__c;    
            }else {
                isInsrt = false;
                showErrMsg = true;
                errMsgLst.add('Please Fill the Contact Name.');
            }
            if(!string.isBLANK(jobDetails.Manager__c)){
                opp.Manager__c = jobDetails.Manager__c;
            }else{
                isInsrt = false;
                showErrMsg = true;
                errMsgLst.add('Please Select the Job Manager.');
                
            }
            if(!string.isBLANK(jobDetails.Department__c)){
                opp.Department__c = jobDetails.Department__c;
            }else{
                isInsrt = false;
                showErrMsg = true;
                errMsgLst.add('Please Select Job Manager Department.');
            }
            if(!string.isBLANK(jobDetails.Work_Type__c)){
                opp.Work_Type__c = jobDetails.Work_Type__c;
            }else{
                isInsrt = false;
                showErrMsg = true;
                errMsgLst.add('Please Select the Job Manager Work Type');
            }
            if(!string.isBLANK(jobDetails.Office__c)){
                opp.Office__c = jobDetails.Office__c;
                
                codingStrLst = [SELECT id,name,Department__c,Office__c,Work_Type__c,Staff__c,Work_Type__r.Name from Coding_Structure__c where Staff__c=:jobDetails.Manager__c AND Department__c=:jobDetails.Department__c AND Office__c=:jobDetails.Office__c AND Work_Type__r.Name=:jobDetails.Work_Type__c limit 1]; 
                if(codingStrLst.size()>0){
                    System.debug('checking save');
                    opp.Coding_Structure__c = codingStrLst[0].id;
                }
            }else{
                isInsrt = false;
                showErrMsg = true;
                errMsgLst.add('Please Select the Job Manager Office.');
            }
            if(isInsrt == true){
                if(!string.isBLANK(jobDetails.Property_Address__c)){
                    opp.Property_Address__c = jobDetails.Property_Address__c;                                       
                }
                opp.CloseDate = System.today();
                opp.RecordTypeId = jobrecord;
            }
            if(isInsrt == true){
                System.debug('checking save');
                upsert opp ;
                
                 Opportunity oppobject = [SELECT Id,Name,Job_Number__c,Company_Name_System_Lookup__c FROM Opportunity Where Manager__c =:opp.Manager__c AND Coding_Structure__c =: opp.Coding_Structure__c AND Work_Type__c =: opp.Work_Type__c AND Department__c =: opp.Department__c AND Office__c =: opp.Office__c AND Name=: opp.Name AND Client_contact_name__c =: opp.Client_contact_name__c AND  Client_company_name__c =: opp.Client_company_name__c AND CreatedById  =: UserInfo.getUserId() AND Property_Address__c =:opp.Property_Address__c ORDER BY createddate desc LIMIT 1  ];
                 PageReference OpporunityPageCancel = new PageReference('/apex/CS_ManageMyJobPage?id='+oppobject.id );
                 OpporunityPageCancel.setRedirect(true);
                 return OpporunityPageCancel;
                
            }else{
                
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Record Cannot be Saved.'));  
                //errorMessage = '';
            }
        
        return  null;
        }
        return  null;
    }
    /**
     *  Method Name: getStaffDept
     *  Description: Method to get the Job Manager Details(Staff)
     *  Param:  None
     *  Return: None
    */
    public void getStaffDept(){
        List<Coding_Structure__c> codingStrLst = new List<Coding_Structure__c>();
        staffDepLst = new List<SelectOption>();
        staffDepLst.add(new SelectOption('','-- None --'));
        deptSet = new Set<String>();
        id staffId;
        //jobDetails.Department__c=null;
         
        if(!string.isBlank(jobDetails.Manager__c)){
            Staff__c stfDetails = [SELECT id,Office__c,Department__c,Coding_Structure__c from staff__c where id=:jobDetails.Manager__c];

            codingStrLst = [SELECT id,name,Department__c,Office__c,Work_Type__c,Staff__c,Work_Type__r.Name from Coding_Structure__c where Staff__c=:jobDetails.Manager__c AND  Status__c = 'Active' AND Work_Type__r.Active__c = true ];
            
            for(Coding_Structure__c cs :codingStrLst ){
                if(!string.isBlank(cs.Department__c) && !deptSet.contains(cs.Department__c)){
                    staffDepLst.add(new SelectOption(cs.Department__c,cs.Department__c)); 
                    deptSet.add(cs.Department__c);
                }
            }
            if(staffId !=stfDetails.id){
                staffOfcLst = new List<SelectOption>();
                staffWorkTypeLst = new List<SelectOption>();
            }
            if(deptSet!=null&&deptSet.size() == 1){
                staffId = stfDetails.id;
                jobDetails.Department__c = codingStrLst[0].Department__c;
                getStaffDeptOffice();
                
                if(ofcSet!=null&&ofcSet.size() == 1){
                    jobDetails.Office__c = codingStrLst[0].Office__c;  
                    //getStaffDeptOfcWorkType();
                }
               
                
            }
            
        }

    }
    
    /**
     *  Method Name: getStaffDeptOffice
     *  Description: Method to get the Job Manager Details(Staff)
     *  Param:  None
     *  Return: None
    */
    public void getStaffDeptOffice(){
        List<Coding_Structure__c> codingStrLst = new List<Coding_Structure__c>();
        staffOfcLst = new List<SelectOption>();
        staffOfcLst.add(new SelectOption('','-- None --'));
        ofcSet = new Set<String>();
        
        if(!string.isBlank(jobDetails.Manager__c)){
            Staff__c stfDetails = [SELECT id,Office__c,Department__c,Coding_Structure__c from staff__c where id=:jobDetails.Manager__c];  
            if(!string.isBlank(jobDetails.Department__c) ){
                codingStrLst = [SELECT id,name,Department__c,Office__c,Work_Type__c,Staff__c,Work_Type__r.Name,Work_Type__r.id from Coding_Structure__c where Staff__c=:jobDetails.Manager__c AND Department__c=:jobDetails.Department__c AND  Status__c = 'Active' AND Work_Type__r.Active__c = true];
                for(Coding_Structure__c cs :codingStrLst){
                    if(!string.isBlank(cs.Office__c) && !ofcSet.contains(cs.Office__c)){
                        staffOfcLst.add(new SelectOption(cs.Office__c,cs.Office__c));
                        ofcSet.add(cs.Office__c);
                    }   
                    
                }
                if(ofcSet.size() == 1){
                    jobDetails.Office__c = codingStrLst[0].Office__c;  
                    getStaffDeptOfcWorkType();
                

                }
                
            }
            
        }
        
    }
    
    /**
     *  Method Name: getStaffDeptOfcWorkType
     *  Description: Method to get the Job Manager Details(Staff)
     *  Param:  None
     *  Return: None
    */
    public void getStaffDeptOfcWorkType(){
        List<Coding_Structure__c> codingStrLst = new List<Coding_Structure__c>();
        staffWorkTypeLst = new List<SelectOption>();
        staffWorkTypeLst.add(new SelectOption('','-- None --'));
        wrkTypSet = new Set<String>();
         
        if(!string.isBlank(jobDetails.Manager__c) ){
            if(!string.isBlank(jobDetails.Department__c) && !string.isBlank(jobDetails.Office__c)){
                codingStrLst = [SELECT id,name,Department__c,Office__c,Work_Type__c,Staff__c,Work_Type__r.Name from Coding_Structure__c where Staff__c=:jobDetails.Manager__c AND Department__c=:jobDetails.Department__c AND Office__c=:jobDetails.Office__c AND  Status__c = 'Active' AND Work_Type__r.Active__c = true];
                if(codingStrLst!=null&&codingStrLst.size()==1){
                    jobDetails.Work_Type__c = codingStrLst[0].Work_Type__r.Name;
                }
                
                for(Coding_Structure__c cs :codingStrLst){
                    if(!string.isBlank(cs.Work_Type__r.Name) && !wrkTypSet.contains(cs.Work_Type__r.Name)){
                        staffWorkTypeLst.add(new SelectOption(cs.Work_Type__r.Name,cs.Work_Type__r.Name));
                        wrkTypSet.add(cs.Work_Type__r.Name);
                    }    
                }
               
             
            }         
        }
        
    }
     /**
    *   Method Name: closeModal
    *   Description: method to close Modal
    *   Param: None
    *   Return: None
    */
    public void closeModal(){
        errorMessage = '';
        showErrMsg = false;
    }
    
 }