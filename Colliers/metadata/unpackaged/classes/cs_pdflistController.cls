public class cs_pdflistController
{

    public cs_pdflistController(ApexPages.StandardController controller) {

    }

    ApexPages.StandardSetController setCon; 
    public string firstId {get;set;} 
    public string NilehrNo {get;set;}
    public Integer sizeofwrapper {get;set;}
    public Map<Id,Id> invoiceToJob {get;set;}
    public List<pdfwrapper> lstwrapper {get;set;}
    public List<Invoice__c> lstToupdate = new List<Invoice__c>();
    public cs_pdflistController(ApexPages.StandardSetController controller)
    {   NilehrNo = Label.Nile_Hr_No; 
        lstwrapper = new  List<pdfwrapper> ();
        Map<string,set<Invoice_PDF_Value__c>> currencytoids= new Map<string,set<Invoice_PDF_Value__c>>();
        Map<id,set<string>> idTodepartment = new Map<id,set<string>>();
        Map<string,Invoice_PDF_Value__c> mapofinvpdf = new Map<string,Invoice_PDF_Value__c>();
        List<Invoice_PDF_Value__c> lstofInvPdf = new List<Invoice_PDF_Value__c>();
        set<Id> IdofInvoice = new set<id>();
        set<Id> companyIds = new set<id>();
        List<id> lstofids = new List<Id>();
        List<currency_Symbol__c> currencyobj = new List<currency_Symbol__c>();
        List<Account_Address_Junction__c> accountAdeslist = new List<Account_Address_Junction__c>();
        Map<Id,Map<id,string>> addresscompanyMap = new Map<Id,Map<id,string>>();
        Map<string,string> currencynameTosymbol = new Map<string,string>();
        set<string> lstofdepartment = new set<string>();
        set<string> setofcurrency = new set<string>();
        Map<id,Invoice_PDF_Value__c> hr_To_PDFmap  = new  Map<id,Invoice_PDF_Value__c>();
        firstId = '';
        set<id> ManagerIds = new set<id>();
        setCon = controller;
        List<Invoice__c> Invlist = new List<Invoice__c>();
        string colliervatlabel = Label.colliers_Vat_number;
        List<colliers_Vat_Number__c> listofcolliersvat = new List<colliers_Vat_Number__c>();
        
        If(colliervatlabel != null && string.isNotBlank(colliervatlabel)){
            listofcolliersvat = [SELECT Type_Of_Vat_Number__c,Vat_Value__c FROM colliers_Vat_Number__c WHERE Type_Of_Vat_Number__c =:colliervatlabel];
        }
        string depCodeList;
        for ( Invoice__c Inv : (Invoice__c[])setCon.getSelected() )
        {
               lstofids.add(Inv.Id);               
        }
        Invlist = [select Id,Total_PDF_Amount__c,Care_Of_Information__r.client__c,Care_Of_Information__r.client__r.name,Opportunity__r.Invoicing_Address_Formula__c,Care_Of_Information__r.Care_of_Company_Name_Full__c,Time_Stamp_remittance__c,Invoice_contact_Name__c,Invoice_Company_Name__c,Address__c, Invoice_Currency__c,Opportunity__r.Department_Cost_Centre_Code__c, Total_Amount_Inc_Vat__c,Description__c,Total_cost_Ex_Vat__c,Total_Fee_Ex_Vat__c ,Opportunity__r.Manager__r.HR_No__c,Date_of_Invoice__c,Opportunity__r.Department_Integer_Id__c,Opportunity__r.Coding_Structure__r.Department_Cost_Code_Centre__c,Opportunity__r.Manager__c,Invoice_Address__c,Company__r.name,Care_Of_Information__r.name,Invoice_contact__r.name,Opportunity__r.Invoicing_Company__c,OriginalInvoicePrinted__c,Final_Invoice__c ,Total_Fee__c ,Total_cost__c,Total_amount__c,VAT_Amount__c, Company__c,Opportunity__c,CurrencyIsoCode,PO_Number__r.name,(select id, name, Purchase_Order__c, Purchase_Order__r.Name from Purchase_Order_Invoice_Junctions__r),Opportunity__r.Job_Number__c,Total_Internaional_Vat_Amount__c,Opportunity__r.Invoicing_Company2__r.MLR_Number__c,Opportunity__r.Invoicing_Company2__c,Opportunity__r.Invoicing_Company2__r.Building_Name__c,Opportunity__r.Invoicing_Company2__r.Street__c,Opportunity__r.Invoicing_Company2__r.Post_Code__c,Foreign_Country__c,Opportunity__r.Invoicing_Company2__r.Client_VAT_Number__c,Opportunity__r.Invoicing_Company2__r.Floor_No__c,Opportunity__r.Invoicing_Company2__r.Street_No__c,Opportunity__r.Invoicing_Company2__r.Town__c,Opportunity__r.Invoicing_Company2__r.country__c,Invoice_Wording__c,Opportunity__r.name,Opportunity__r.Invoicing_Company2__r.name,Opportunity__r.Manager__r.Coding_Structure__r.Department__c,Opportunity__r.id,Assigned_Invoice_Number__c,File_Reference__c,Vat_Reg_No__c ,Opportunity__r.Manager__r.id,Opportunity__r.Invoicing_Company__r.Address__r.Town__c,Opportunity__r.Invoicing_Company__r.Address__r.Building__c,Opportunity__r.Invoicing_Company__r.Address__r.country__c,Opportunity__r.Invoicing_Company__r.Address__r.Street__c , name,Contact__c, Contact__r.name, FE_Write_Of_Created_Date__c, is_International__c, Payable_By__c,status__c,VAT__c, createdDate,(select id,fee__c,type__c,Cost_Type__c,International_Amount__c,Amount_ex_VAT__c,VAT_Amount__c,Description__c,International_VAT_Amount_Formula__c from Invoice_Line_Items__r where Type__c='Fee' Or Type__c='Cost') from invoice__c where id IN:lstofids ];
        if(!Invlist.isEmpty()){
            for(Invoice__c Inv :Invlist){
                if(Inv.Opportunity__r.Manager__c != null){
                    IdofInvoice.add(Inv.Opportunity__r.Manager__c);
                }
                if(Inv.Opportunity__r.Coding_Structure__r.Department_Cost_Code_Centre__c != null && string.isNotBlank(Inv.Opportunity__r.Coding_Structure__r.Department_Cost_Code_Centre__c)){
                    lstofdepartment.add(Inv.Opportunity__r.Coding_Structure__r.Department_Cost_Code_Centre__c);
                    setofcurrency.add(Inv.Invoice_Currency__c);
                }
                if(Inv.Company__c != null){
                    companyIds.add(Inv.Company__c);
                }else if(Inv.Care_Of_Information__r.client__c != null && string.isNotBlank(Inv.Care_Of_Information__r.client__c)){
                    companyIds.add(Inv.Care_Of_Information__r.client__c);
                }
            }
        }
        // Creating Map of address code and companyIds
        accountAdeslist = [SELECT Account_ID__c,Id,Address__c,Name,Account__c FROM Account_Address_Junction__c WHERE Account__c IN: companyIds];
        for(Account_Address_Junction__c objacntadrs :accountAdeslist ){
            if(objacntadrs.Account__c != null && objacntadrs.Account_ID__c != null){
                if(addresscompanyMap.containsKey(objacntadrs.Account__c)){
                    if(objacntadrs.Address__c != null && objacntadrs.Address__c != null){
                        if(!addresscompanyMap.get(objacntadrs.Account__c).containsKey(objacntadrs.Address__c)){
                            addresscompanyMap.get(objacntadrs.Account__c).put(objacntadrs.Address__c,objacntadrs.Account_ID__c);
                        }
                    }
                }else{
                    if(objacntadrs.Address__c != null && objacntadrs.Address__c != null){
                        Map<id,string> mapofAddressAccountId = new Map<id,string>();
                        mapofAddressAccountId.put(objacntadrs.Address__c,objacntadrs.Account_ID__c);
                        addresscompanyMap.put(objacntadrs.Account__c, mapofAddressAccountId);
                    }
                }
                
            }
        }
        
        
        //CREATING MAP FOR CURRENCY NAME TO CURRENCY SYMBOL
        currencyobj = [SELECT Id,currencyName__c,currencySymbol__c FROM currency_Symbol__c];
        if(!currencyobj.isEmpty()){
            for(currency_Symbol__c currobj :currencyobj ){
                currencynameTosymbol.put(currobj.currencyName__c,currobj.currencySymbol__c);
            }
        }
        if(lstofdepartment != null && !lstofdepartment.isEmpty()){
            List<string> deptList = new List<String>();
            deptList.addAll(lstofdepartment);
            depCodeList=string.join(deptList,';');
        }
        // creating payment footer and header
        lstofInvPdf = [select id,Cheque_Footer__c,Company__c,  External_Id_JM_UK__c,Footer__c, Currency__c,Remittance__c,Payment_Footer__c,HrNo__c,Header_Details__c,  Dept_Cost_Centre__c, Company_Name__c from Invoice_PDF_Value__c where  (Dept_Cost_Centre__c Includes( :depCodeList) AND Currency__c IN:setofcurrency) OR HrNo__c IN:IdofInvoice] ;    
        
        if(!lstofInvPdf.isEmpty()){
            for(Invoice_PDF_Value__c objinf: lstofInvPdf){
                mapofinvpdf.put(objinf.id, objinf);
                if(objinf.HrNo__c != null && string.isNotBlank(objinf.HrNo__c)){
                    if(!hr_To_PDFmap.containsKey(objinf.HrNo__c)) {
                        hr_To_PDFmap.put(objinf.HrNo__c,objinf);
                    }
                }
                if(objinf.Currency__c != null && string.isNotBlank(objinf.Currency__c)){
                    if(currencytoids.containsKey(objinf.Currency__c)){
                        currencytoids.get(objinf.Currency__c).add(objinf);
                    }else{
                        set<Invoice_PDF_Value__c> newset = new set<Invoice_PDF_Value__c>();
                        newset.add(objinf);
                        currencytoids.put(objinf.Currency__c,newset);
                    }
                    if(objinf.Dept_Cost_Centre__c != null && string.isNotBlank(objinf.Dept_Cost_Centre__c)){
                        if(idTodepartment.containsKey(objinf.id)){
                            set<string> department = new set<string>();
                            if(objinf.Dept_Cost_Centre__c != null){
                                department.addAll(objinf.Dept_Cost_Centre__c.split(';'));
                            }
                            
                            idTodepartment.get(objinf.id).addAll(department);
                        }else{
                            set<string> setdepartment = new set<string>();
                            if(objinf.Dept_Cost_Centre__c != null){
                                setdepartment.addAll(objinf.Dept_Cost_Centre__c.split(';'));
                            }
                            idTodepartment.put(objinf.id,setdepartment);
                        }
                    }
                }
            }
        }
        if(!Invlist.isEmpty()){
            Integer row = 1;
            for(Invoice__c invobj :Invlist ){
                string invpdfid = '';
                pdfwrapper wrapperobj = new pdfwrapper();
                wrapperobj.index = row;
                wrapperobj.invobj = invobj;
                if(invobj.Company__c != null &&  String.isNotBlank(invobj.Company__c)){
                    if(addresscompanyMap.containsKey(invobj.Company__c)){
                        if(addresscompanyMap.get(invobj.Company__c).containsKey(invobj.Address__c)){
                            wrapperobj.Accountcode = addresscompanyMap.get(invobj.Company__c).get(invobj.Address__c);
                        }
                        
                    }
                }else if(invobj.Care_Of_Information__c != null && String.isNotBlank(invobj.Care_Of_Information__c)){
                    if(addresscompanyMap.containsKey(invobj.Care_Of_Information__r.client__c)){
                        if(addresscompanyMap.get(invobj.Care_Of_Information__r.client__c).containsKey(invobj.Address__c)){
                            wrapperobj.Accountcode = addresscompanyMap.get(invobj.Care_Of_Information__r.client__c).get(invobj.Address__c);
                        }
                        
                    }
                }
                
                wrapperobj.todaysdate = Date.today();
                List<string> Remitence= new List<string> ();
                wrapperobj.FooterMsg='Supply outside the scope of UK VAT under s.7A UK VATA 1994 and Article 44 of EC Directive 2006/112';
                if(invobj.VAT__c== null){
                    invobj.VAT__c=0;
                }
                wrapperobj.companyname ='';
                wrapperobj.contactName ='';
                wrapperobj.showcareof = false;
                if(invobj.Invoice_Company_Name__c!= null && string.isNotBlank(invobj.Invoice_Company_Name__c)){
                    wrapperobj.companyname = invobj.Invoice_Company_Name__c;
                }
                else{
                    if (invobj.Company__r.name!= null && string.isNotBlank(invobj.Company__r.name)){
                        wrapperobj.companyname = invobj.Company__r.name;
                    }else if(invobj.Care_Of_Information__r.Care_of_Company_Name_Full__c   != null && string.isNotBlank(invobj.Care_Of_Information__r.Care_of_Company_Name_Full__c)){
                        wrapperobj.companyname = invobj.Care_Of_Information__r.Care_of_Company_Name_Full__c;
                        if(invobj.Care_Of_Information__r.client__r.name != null && string.isNotBlank(invobj.Care_Of_Information__r.client__r.name)){
                            wrapperobj.careofClient = invobj.Care_Of_Information__r.client__r.name;
                            wrapperobj.showcareof = true;
                        }
                        
                    }
                }
                if(invobj.Invoice_contact_Name__c!= null && string.isNotBlank(invobj.Invoice_contact_Name__c)){
                    wrapperobj.contactName = invobj.Invoice_contact_Name__c;
                }
                else{
                    if (invobj.Invoice_contact__r.name!= null && string.isNotBlank(invobj.Invoice_contact__r.name)){
                        wrapperobj.contactName = invobj.Invoice_contact__r.name;
                    }
                }
                List<string> addresslist = new List<String>();
                if(invobj.Invoice_Address__c != null && string.isNotBlank(invobj.Invoice_Address__c)){
                    addresslist = invobj.Invoice_Address__c.split(',');
                    wrapperobj.invoiceAddress = addresslist;
                }else if(invobj.Opportunity__r.Invoicing_Address_Formula__c != null && string.isNotBlank(invobj.Opportunity__r.Invoicing_Address_Formula__c)){
                    addresslist = invobj.Opportunity__r.Invoicing_Address_Formula__c.split(',');
                    wrapperobj.invoiceAddress = addresslist;
                }
                if(invobj.Invoice_Currency__c != null){
                    if(currencynameTosymbol.containsKey(invobj.Invoice_Currency__c)){
                        wrapperobj.currencysymbiol = currencynameTosymbol.get(invobj.Invoice_Currency__c);
                    }
                }else{
                    wrapperobj.currencysymbiol = currencynameTosymbol.get('GBP');
                }
                Invoice_PDF_Value__c invpdf = new Invoice_PDF_Value__c();
                if(hr_To_PDFmap.containsKey(invobj.Opportunity__r.Manager__c)){
                    invpdfid = hr_To_PDFmap.get(invobj.Opportunity__r.Manager__c).Id; 
                }
                else if(invobj.Invoice_Currency__c != null && string.isNotBlank(invobj.Invoice_Currency__c)){
                    if(currencytoids.containsKey(invobj.Invoice_Currency__c)){
                        for(Invoice_PDF_Value__c objinvpdf :currencytoids.get(invobj.Invoice_Currency__c)){
                            if(idTodepartment.containsKey(objinvpdf.Id)){
                                if(invobj.Opportunity__r.Department_Cost_Centre_Code__c != null && string.isNotBlank(invobj.Opportunity__r.Department_Cost_Centre_Code__c)){
                                    if(idTodepartment.get(objinvpdf.Id).contains(invobj.Opportunity__r.Department_Cost_Centre_Code__c)){
                                        
                                        invpdfid = objinvpdf.Id;
                                        break;
                                    } 
                                }
                            }
                        }
                    }
                }
               
                /*system.debug('invoice pdf values'+invpdf.Id);
                if(invobj.Opportunity__r.Manager__r.HR_No__c == NilehrNo){
                    string RemitanceBlank=Label.Default_Nile_Remittance;
                    wrapperobj.Remitence=RemitanceBlank.split('  ');
                    wrapperobj.Paymentfoot= Label.Default_Paymentfoot;
                    wrapperobj.Footer = Label.Default_Nile_Footer;
                }
                else{*/
                    if(invpdfid != null && string.isNotBlank(invpdfid)){
                        system.debug('invoice pdf true');
                        
                        if(mapofinvpdf.get(invpdfid).Remittance__c != null && mapofinvpdf.get(invpdfid).Remittance__c != ''){
                            wrapperobj.isCapital=mapofinvpdf.get(invpdfid).Remittance__c.contains('COLLIERS CAPITAL');
                            if(mapofinvpdf.get(invpdfid).Company_Name__c != null && String.isNotBlank(mapofinvpdf.get(invpdfid).Company_Name__c) && mapofinvpdf.get(invpdfid).Company_Name__c.contains('NILE MANAGEMENT')){
                                wrapperobj.isnilemanage = true; 
                            } 
                            string RemitanceBlank=Label.Default_Remittance;
                
                            if(invobj.Time_Stamp_remittance__c != null && String.isNotBlank(invobj.Time_Stamp_remittance__c)){
                                 wrapperobj.Remitence=invobj.Time_Stamp_remittance__c.split(',');
                                 system.debug('LoggingLevel logLevel invoice---'+invobj.Time_Stamp_remittance__c);
                            }
                            else if(mapofinvpdf.get(invpdfid).Remittance__c != null && mapofinvpdf.get(invpdfid).Remittance__c!=''){
                                wrapperobj.Remitence=mapofinvpdf.get(invpdfid).Remittance__c.split(',');
                                system.debug('LoggingLevel logLevel invpdf---'+mapofinvpdf.get(invpdfid).Remittance__c);
                            }
                            else{
                                wrapperobj.Remitence=RemitanceBlank.split('  ');
                            }
                            if(mapofinvpdf.get(invpdfid).Payment_Footer__c != null && mapofinvpdf.get(invpdfid).Payment_Footer__c != ''){
                                wrapperobj.Paymentfoot=mapofinvpdf.get(invpdfid).Payment_Footer__c;
                            }
                            else if(mapofinvpdf.get(invpdfid).Payment_Footer__c==null || mapofinvpdf.get(invpdfid).Payment_Footer__c==''){
                                wrapperobj.Paymentfoot= Label.Default_Paymentfoot;
                            
                            }
                            if(mapofinvpdf.get(invpdfid).Footer__c != null && mapofinvpdf.get(invpdfid).Footer__c != ''){
                                wrapperobj.Footer=mapofinvpdf.get(invpdfid).Footer__c;
                                //System.debug('checking footer value---'+invPdf.Footer__c);
                            }
                            else if(mapofinvpdf.get(invpdfid).Footer__c==null || mapofinvpdf.get(invpdfid).Footer__c==''){
                                wrapperobj.Footer=Label.Default_Footer;
                                System.debug('checking footer value---'+Label.Default_Footer);
                            
                            }
                        }
                    }
                    else{
                        system.debug('invoice pdf flase');
                        string RemitanceBlank=Label.Default_Remittance;
                        wrapperobj.Remitence=RemitanceBlank.split('  ');
                        wrapperobj.Paymentfoot=Label.Default_Paymentfoot;
                        wrapperobj.Footer=Label.Default_Footer;
                        
                    }
                if(!listofcolliersvat.isEmpty()){
                    wrapperobj.Footer += ' , VAT Reg No: '+listofcolliersvat[0].Vat_Value__c+' ';
                }
                wrapperobj.IsDraftOrApproved = null;
                if(invobj.status__c== 'Approved'){
                    if(invobj.OriginalInvoicePrinted__c!= true){
                        //invobj.OriginalInvoicePrinted__c=true;
                        wrapperobj.IsDraftOrApproved='';// changed for d-0161 ORIGINAL INVOICE'; 
                        lstToupdate.add(invobj);
                    }
                    
                }
                if(invobj.OriginalInvoicePrinted__c== true && wrapperobj.IsDraftOrApproved!='FINAL' && invobj.status__c!= 'Draft' && invobj.status__c!= 'Printed'){
                    wrapperobj.IsDraftOrApproved='CERTIFIED COPY';
                }
                else if(invobj.status__c== 'Draft'){
                    wrapperobj.IsDraftOrApproved='DRAFT INVOICE';
                }
                string pordid = '';
                for(Purchase_Order_Invoice_Junction__c PoJn: invobj.Purchase_Order_Invoice_Junctions__r){
                 
                    if(pordid==''){
                        wrapperobj.PoRef= PoJn.Purchase_Order__r.Name;
                    }
                    else{
                        wrapperobj.PoRef=pordid+','+PoJn.Purchase_Order__r.Name;
                    }
                
                }
                List<Invoice_Line_Item__c>listofFee = new List<Invoice_Line_Item__c>();
                List<Invoice_Line_Item__c>listofCost = new List<Invoice_Line_Item__c>();
                if(!invobj.Invoice_Line_Items__r.isEmpty()){
                    for(Invoice_Line_Item__c cst: invobj.Invoice_Line_Items__r){
                        if( cst.type__c=='Cost'){
                            listofCost.add(cst);
                        }
                        else if(cst.type__c=='Fee'){
                            listofFee.add(cst); 
                        }
                    
                    }
                }
                wrapperobj.FeeList = listofFee;
                wrapperobj.CostList = listofCost;
                wrapperobj.totalamount = 0.00;
                /* if(invobj.Total_cost_Ex_Vat__c != null){
                    wrapperobj.totalamount = wrapperobj.totalamount + invobj.Total_cost_Ex_Vat__c;
                }
                if(invobj.Total_Vat_Amount__c != null){
                    wrapperobj.totalamount = wrapperobj.totalamount + invobj.Total_Vat_Amount__c;
                }
                if(invobj.Total_Fee_Ex_Vat__c != null){
                    wrapperobj.totalamount = wrapperobj.totalamount + invobj.Total_Fee_Ex_Vat__c;
                } */
                System.debug('footer'+wrapperobj.Footer);
                lstwrapper.add(wrapperobj); 
                row++;
            }
            
        }
        
        sizeofwrapper = lstwrapper.size();
       
    }
    //update invoice with OriginalInvoicePrinted__c values
    public void updateinvoice(){
        try{
            if(!lstToupdate.isEmpty()){
                update lstToupdate;
            }
        
        } catch (Exception e) {
            ApexPages.addMessages(e);
        }
        
    }
    public class pdfwrapper{
        public List<string> Remitence{get;set;}
        public List<string> invoiceAddress{get;set;}
        public string companyname{get;set;}
        public string contactName{get;set;}
        public string careofClient{get;set;}
        public Boolean isCapital{get;set;}
        public Boolean showcareof{get;set;}
        public Boolean isnilemanage{get;set;}
        public Decimal totalamount{get;set;}
        public date todaysdate{get;set;}
        public string IsDraftOrApproved{get;set;}
        public Invoice__c invobj {get;set;}
        public string Accountcode{get;set;}
        public string Paymentfoot{get;set;}
        public string Footer{get;set;}
        public string FooterMsg{get;set;}
        public Integer index{get;set;}
        public string PoRef{get;set;}
        public string currencysymbiol{get;set;}
        public List<Invoice_Line_Item__c>FeeList{get;set;}
        public List<Invoice_Line_Item__c>CostList{get;set;}
        
        public pdfwrapper(){

        }
    }

}