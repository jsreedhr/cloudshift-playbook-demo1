/**
*  Class Name: SharingRuleTriggerBatchCtrl
*  Description: Apex controller for Visualforce page SharingRuleTriggerBatch which 
*  is used to review sharing rules (custom functionality) batch job status or start those
*
*  Tests available in SharingRuleTriggerBatchCtrlTest
*
*  Company: CloudShift
*  CreatedDate: 11/07/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  ----------------------------------------------------------- 
*  Kaspars Rezgalis		11/07/2018					Created
*
*/

public class SharingRuleTriggerBatchCtrl{
	
	public transient String whichSObject{get;set;}

	public SharingRuleTriggerBatchCtrl(){
		
	}

	public void triggerBatch(){
		if(String.isBlank(whichSObject)){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, SharingEngineBatch.ERROR_NO_SOBJECT));
			return;
		}

		if(!ConfigurationManager.getInstance().isSharingOn() || SharingEngineConfigurationManager.getInstance(whichSObject).isBatchSharingAlreadyRunning()){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, SharingEngineBatch.ERROR_SHARING_IS_OFF));
			return;
		}				
		
		SharingRuleTriggerBatchCtrl.dispatchSharingRulesRecalculation(new Set<String>{whichSObject});
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, SharingEngineBatch.SUCCESS_BATCH_QUEUED));
	}

	public Boolean getCurrentSharingConfigIsOn(){
		return ConfigurationManager.getInstance().isSharingOn();
	}

	public List<SharingEngine_Status__c>getCurrentEngineStatuses(){
		List<SharingEngine_Status__c>retValues = new List<SharingEngine_Status__c>();
		Map<String, SharingEngine_Status__c>allSettings = SharingEngine_Status__c.getAll();

		for(PicklistEntry picklistValue : Sharing_Rule__c.Object__c.getDescribe().getPicklistValues()){
            String sObjectName = picklistValue.getValue();

            if(allSettings.containsKey(sObjectName)){
            	retValues.add(allSettings.get(sObjectName));
            }else{
            	retValues.add(new SharingEngine_Status__c(Name = sObjectName, Batch_Processing_in_Progress__c = false));
            }
        }

		return retValues;
	}

	public String getCurrentSharingConfigObjectPrefixForLink(){
		return Schema.getGlobalDescribe().get('Bypass_Configuration__c').getDescribe().getKeyPrefix();
	}

	public String getSharingRuleObjectPrefixForLink(){
		return Schema.getGlobalDescribe().get('Sharing_Rule__c').getDescribe().getKeyPrefix();
	}

	public String getErrorLogObjectPrefixForLink(){
		return Schema.getGlobalDescribe().get('Error_Log__c').getDescribe().getKeyPrefix();
	}

	public List<SelectOption> getSharingRuleWhichObjectOptions() {
        List<SelectOption> whichObjects = new List<SelectOption>();
        whichObjects.add(new SelectOption('', '--None--'));
        for (PicklistEntry picklistValue : Sharing_Rule__c.Object__c.getDescribe().getPicklistValues()){
            whichObjects.add(new SelectOption(picklistValue.getValue(), picklistValue.getValue()));
        }
        return whichObjects;
    }

    public static void dispatchSharingRulesRecalculation(Set<String>sObjectNamesForBatchProcessing){
		for(String sObjectName : sObjectNamesForBatchProcessing){
		//checking whether batch is already running (or sharing is on/off) was performed in trigger; another check (+ throwing an error) is done in batch itself
			SharingEngineBatch batch = new SharingEngineBatch(sObjectName);
			Database.executeBatch(batch, 50);
		}
	}
}