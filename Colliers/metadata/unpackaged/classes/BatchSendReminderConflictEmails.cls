/* Name : BatchSendReminderConflictEmails
 * Created By : Jyoti H/28-11-2016
 * Description : Class to send the Conflict Reminder Emails daily
 */

global class BatchSendReminderConflictEmails implements Database.Batchable<sObject>{

   public List<String> emailList;
   public List<String> reminderEmailList;
   
   public map<id,Conflict__c> conflictMap;
   public map<id,Staff__c> staffUsrMap;
   public set<Id> cId;
     
   List<Conflict__c> updateConflictList {get;set;}
  
   global Database.QueryLocator start(Database.BatchableContext BC){
       return Database.getQueryLocator([SELECT id,name,Conflict__c,Job_1__r.Manager__r.User__c,Job_1__r.Manager__r.Name,Job_1__c,Job_2__c,Job_1__r.Manager__c,Job_2__r.Manager__c,Job_1__r.Name,Job_2__r.Name,Job_1__r.Job_Manager_Email__c,Job_2__r.Job_Manager_Email__c,Reminder_Sent_Date__c,Potential_Conflict_resolved__c from Conflict__c where conflicMailSend__c =true AND Potential_Conflict_resolved__c = false  limit 10000]);
   } 
	   global void execute(Database.BatchableContext BC, List<Conflict__c> scope){
			cId = new Set<Id>();
				  
			updateConflictList = new List<Conflict__c>();
			
			conflictMap = new map<id,Conflict__c>();

			List<Conflict_Reminder_days__c> cfRem = Conflict_Reminder_days__c.getall().values();
			Integer remDays = cfRem[0].No_of_Days__c.intValue();
			remDays = remDays* -1;
			
			for(Conflict__c cf:scope){
				Conflict__c cfRec = new Conflict__c();
				
				if(cf.Reminder_Sent_Date__c != null){
				    System.debug('Object msg'+cf.Job_1__r.Manager__r);
				    Integer dategap = cf.Reminder_Sent_Date__c.daysBetween(Date.today());
				    if(dategap >=0){
    					if(!cId.contains(cf.Id)){
    						system.debug('remCId---->'+cId);
    
    						conflictMap.put(cf.id,cf); 
    						cId.add(cf.Id);
    						cfRec.Id = cf.Id;
    						cfRec.Reminder_Sent_Date__c = date.today();
    						updateConflictList.add(cfRec);
    					}
				    }
				}  
					 
			}
	   
			if(cId.size()>0){
				sendmail(cId);
			}
			if(updateConflictList.size()>0){
				update updateConflictList;
			}
	 
	   }

	   global void finish(Database.BatchableContext BC){
			  
	   }

	   
	   public void sendmail(set<Id> remIdSet)
		{
			List<Messaging.SingleEmailMessage> sendEmailList = new List<Messaging.SingleEmailMessage>();
			
			//Prefetch email Template id
			EmailTemplate et = [SELECT id FROM EmailTemplate WHERE developerName = 'Conflict_Reminder_Email'];
			Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

        List<String> emailLst = new List<String>();
        for(Id i:remIdSet){
            string whoId = conflictMap.get(i).Job_1__r.Manager__r.User__c ;
            string watId = i;
            String templateId =et.id ;
     
            Messaging.SingleEmailMessage msgDetails = Messaging.renderStoredEmailTemplate(templateId,whoId,watId);
            
            //String mailTextBody = msgDetails.getPlainTextBody();
            String mailHtmlBody = msgDetails.getHTMLBody();
            String mailSubject = msgDetails.getSubject();
            if(!string.isblank(conflictMap.get(i).Job_1__r.Job_Manager_Email__c)){
            emailLst.add(conflictMap.get(i).Job_1__r.Job_Manager_Email__c);
            }
            if(!string.isblank(conflictMap.get(i).Job_2__r.Job_Manager_Email__c)){
            emailLst.add(conflictMap.get(i).Job_2__r.Job_Manager_Email__c);
            }
            system.debug('emailLst--->'+emailLst);
            email.setToAddresses(emailLst);
           // email.setTargetObjectId(i);
            email.setTemplateId(et.id);
            email.setSaveAsActivity(false);
            email.setHtmlBody(mailHtmlBody);
            //email.setPlainTextBody(mailTextBody);
            sendEmailList.add(email);
        }
        
        if(sendEmailList.size()>0){
            if(!Test.isRunningTest()) {
                Messaging.sendEmail(sendEmailList);
            }
        }
    }  
    
}