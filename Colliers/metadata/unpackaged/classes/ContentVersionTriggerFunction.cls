/**
*  Class Name: ContentVersionTriggerFunction
*  Description: This is a Trigger handler for  ContentVersion Trigger.
*  @Company:       dQuotient
*  CreatedDate:    27/11/2017
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------
*  Swetha	     
*/
public class ContentVersionTriggerFunction {
    
    //To replace the special char in file name being posted/uploaded.
    public static void RemoveSpecialCharFromFileName(List<ContentVersion> feedItems)
    {
        String fileName;
        String modifiedFileName;
          string s2;
          string s3;
              
        for(ContentVersion cv : feedItems){
            fileName = cv.Title;
            modifiedFileName = cv.Title;
            System.debug('original File Name--'+fileName);
            String regex = '[:]';
            Pattern regexPattern = Pattern.compile(regex);
            Matcher regexMatcher = regexPattern.matcher(fileName);
            if(regexMatcher.find()) {
              //  modifiedFileName = FileName.replaceAll(regex, '-');
                  modifiedFileName = FileName.replaceAll(regex, ' ');
                //November-27-2017-16:37:34-IST-+0530.jpg
                 s2 = modifiedFileName.substringBefore('-+');
				 s3 = modifiedFileName.substringAfter('.');
            }
            cv.Title = s2+'.'+s3;
            System.debug('replaced--'+modifiedFileName);
            
        }
    }
}