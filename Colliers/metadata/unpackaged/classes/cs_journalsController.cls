/* Class: cs_journalsController
 * Created By : Nidheesh N/04-10-2016
 * Purpose : Journal Tab Functionality
 */
 
public with sharing class cs_journalsController{

    public List<Invoice__c> InvoiceList{get;set;}
    public List<Journal__c> journalList{get;set;}
    public Invoice__c InvoiceDetails{get;set;}
    public boolean isButton{get;set;}
    public boolean isactivejournal{get;set;}
    public Opportunity oppobject {get;set;}
    public Integer allocationSize {get;set;}
    public ErrorHandling ObjModal{get;set;}
    /*public Credit_Note__c creditNotesDetails{get;set;}*/
    public List<journalLstWrapper> lstofJournalWrapper{get;set;}
    public List<invoiceLstWrapper> lstInvoiceDetails{get;set;}
    public invoiceLstWrapper invoiceDetailsWrap{get;set;}
    /*public Map<String,CreditDetailsWrapper> MapCreditDetail{get;set;}*/
    public String oppId{get;set;}
    public Boolean stagestatus{get;set;}
    public integer detailWrpId{get;set;}
    public String jobManager {get;set;}
    public String financecode {get;set;}
    /*public List<Credit_Note__c> creditNotesLst {get;set;}*/
    public String invoiceId{get;set;}
    
    //Error Messages String
    public string isIntr{get;set;}
        
    public cs_journalsController() {
        isactivejournal = true;
        initialJournal();    
    }
    public void initialJournal(){
        isButton = false;
        isactivejournal = true;
        if(ApexPages.currentPage().getParameters().containsKey('id')) {
            oppId = ApexPages.currentPage().getParameters().get('id');
            if (!String.isBlank(oppId)) {
                oppId = String.escapeSingleQuotes(oppId);
                if(oppId InstanceOf Id){
                    oppobject = new Opportunity();
                    oppobject = [SELECT Id, AccountId,isClosed,Account.Name, Name,Relates_To__c,StageName,Amount,Manager__c,Manager__r.Name,Coding_Structure__r.Status__c,Job_Number__c FROM Opportunity WHERE Id = : oppId];
                    if(oppobject.Coding_Structure__r.Status__c != null && string.isNotBlank(oppobject.Coding_Structure__r.Status__c)){
                        if(oppobject.Coding_Structure__r.Status__c == 'Active'){
                            isactivejournal = true;
                        }else{
                            isactivejournal = false;
                        }
                        
                    }else{
                        isactivejournal = false;
                    }
                    fetchInvoiceList();
                    
                    jobManager='';
                    
                }else{
                   // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.InvalidParamterError));
                }
                
            }else{
               // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.InvalidParamterError));
            }
        }else{
           // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.InvalidParamterError));
        }
        //oppId='0068E000007n4PT';
       // fetchCreditNotesList();
       
    }
    //Method to get the List of Invoice associted with the Job   
    public void  fetchInvoiceList(){
        lstofJournalWrapper = new List<journalLstWrapper>();
        journalList = new List<Journal__c>();
        isIntr = '';
        financecode = '';
        string jb_mngr = '';
        ObjModal=new ErrorHandling();
        Integer numCN =0;
        Id invId ;
        set<Id> invIdSet ;
        stagestatus = false;
        lstInvoiceDetails = new List<invoiceLstWrapper>();
        InvoiceList = new List<Invoice__c>();
        invoiceDetailsWrap = new invoiceLstWrapper();
        invIdSet =new Set<Id>();
        // logic to add the count of credit notes
            Map<Id, Integer> CNcountMap= new Map<Id, Integer>();
            for (AggregateResult aggRes : [
                  SELECT COUNT(id)cn,Invoice__c,Invoice__r.Opportunity__c
                  FROM Credit_Note__c where Invoice__r.Opportunity__c=:oppId 
                  GROUP BY Invoice__c,Invoice__r.Opportunity__c
            ]) {
                invId = (Id) aggRes.get('Invoice__c');
                numCN = (Integer) aggRes.get('cn');
                CNcountMap.put(invId, numCN);
                invIdSet.add(invId);
            }
            
            system.debug('CNcountMap---->'+CNcountMap);
        //,
        
        InvoiceList = [select id,Opportunity__c,Amount_Inc_VAT__c,Total_cost__c,Total_Fee__c,Date_of_Invoice__c,Assigned_Invoice_Number__c,Opportunity__r.StageName,Name,Total__c,Job_Title__c,Manager__r.name,Opportunity__r.name,Opportunity__r.Job_Number__c,Opportunity__r.isClosed,Opportunity__r.Account.Name,Company__r.Name,Status__c,Net_Fee__c,Net_Fees__c,is_International__c,(select id,name,Allocation__c,Invoice__c,Allocation__r.workType_Allocation__c,Allocation__r.Assigned_To__r.name,Amount__c,Allocation__r.Allocation_Amount__c,Allocation__r.Department_Allocation__c,Allocation__r.name from Invoice_Allocation_Junctions__r ) from Invoice__c where Opportunity__c=:oppId AND (status__c='Approved' OR status__c='Paid'  OR  status__c = 'Printed' OR Status__c = 'Partial' OR Status__c = 'Partial Paid')  AND id NOT IN:invIdSet ORDER BY CreatedDate desc limit 100]; 
        integer counter=0;
        if(InvoiceList != null && InvoiceList.size()>0){
            if(InvoiceList[0].Opportunity__r.StageName =='Closed Won' && InvoiceList[0].Opportunity__r.StageName == 'Closed Lost'){
                stagestatus = true;
            }else{
                stagestatus = false;
            }
            if(InvoiceList[0].Opportunity__r.isClosed){
                stagestatus = true;
            }
            for(Invoice__c invObj :InvoiceList){
                counter++;
                 
                 if(invObj ==InvoiceList.get(0)){
                     invoiceId = invObj.id;
                     lstInvoiceDetails.add(new invoiceLstWrapper(counter,true,invObj));

                 }else{
                     lstInvoiceDetails.add(new invoiceLstWrapper(counter,false,invObj));
                     
                 } 
            }
            if(lstInvoiceDetails.size()>0 ){
            isButton = true;
            invoiceDetailsWrap = lstInvoiceDetails.get(0);
            journaldata();
            }
            
            system.debug('invoiceDetailsWrap --->'+invoiceDetailsWrap );
            
            system.debug('lstInvoiceDetails--->'+lstInvoiceDetails);
        }
    }
    
    public PageReference cancel(){
       PageReference OpporunityPageCancel = new PageReference('/'+oppId );
       OpporunityPageCancel.setRedirect(true);
       return OpporunityPageCancel;
       return null; 
    }   
    
    //method to pre populate the credit note detail fields
    public void fetchjournalDetails(){
       /* if(!lstofJournalWrapper.IsEmpty()){
            lstofJournalWrapper.clear();
        }*/
        lstofJournalWrapper = new List<journalLstWrapper>();
        string inv_id;
        map<id,Decimal> mapjournAllocation = new map<id,Decimal>();
        set<id> IdofAllocation = new set<Id>();
        if(lstInvoiceDetails != null && !lstInvoiceDetails.Isempty()){
            for(invoiceLstWrapper obj :lstInvoiceDetails){
                if(obj.wrapperId== detailWrpId){
                    obj.isChecked =true;
                    invoiceDetailsWrap= obj;
                    inv_id = obj.invoice.id;
                }
                else{
                     obj.isChecked= false;
                }
            }
        }
        if(invoiceDetailsWrap != null){
            if(invoiceDetailsWrap.invoice != null){
                journaldata();
            }
        }
    }
    //Method to fetch Journal Data
    public void journaldata(){
        map<id,Decimal> mapjournAllocation = new map<id,Decimal>();
        map<id,Decimal> mapjournalInvoice = new map<id,Decimal>();
        map<id,Invoice_Allocation_Junction__c> mapIdlInvoice = new map<id,Invoice_Allocation_Junction__c>();
        set<id> IdofAllocation = new set<Id>();
        set<id> IdofAllocationininoice = new set<Id>();
        List<Allocation__c> lstofAllocation = new List<Allocation__c>();
        if(invoiceDetailsWrap != null){
            if(invoiceDetailsWrap.invoice.Invoice_Allocation_Junctions__r != null && invoiceDetailsWrap.invoice.Invoice_Allocation_Junctions__r.size()>0){
                for(Invoice_Allocation_Junction__c objInvoAlloc : invoiceDetailsWrap.invoice.Invoice_Allocation_Junctions__r){
                    IdofAllocationininoice.add(objInvoAlloc.Allocation__c);
                    mapIdlInvoice.put(objInvoAlloc.Allocation__c,objInvoAlloc);
                }
            }
        }
        lstofAllocation = [SELECT Id,Department_Allocation__c,Allocation_Amount__c,externalCompany__c,workType_Allocation__c,Assigned_To__r.name,externalCompany__r.Name,externalContact__c,externalContact__r.Name FROM Allocation__c WHERE (Job__c=:oppId AND Complete__c = false) OR Id IN:IdofAllocationininoice];
        if(invoiceDetailsWrap != null){
            if(invoiceDetailsWrap.invoice.Invoice_Allocation_Junctions__r != null && invoiceDetailsWrap.invoice.Invoice_Allocation_Junctions__r.size()>0){
                for(Invoice_Allocation_Junction__c objInvoAlloc : invoiceDetailsWrap.invoice.Invoice_Allocation_Junctions__r){
                    IdofAllocation.add(objInvoAlloc.Allocation__c);
                    mapIdlInvoice.put(objInvoAlloc.Allocation__c,objInvoAlloc);
                }
            }
        }
        
        if(!lstofAllocation.isEmpty()){
            for(Allocation__c objalloc : lstofAllocation){
                if(!IdofAllocation.contains(objalloc.Id)){
                    IdofAllocation.add(objalloc.Id);
                }
            }
        }
        /*journalList = [SELECT Id,Allocation_Value__c,Allocation__c FROM Journal__c WHERE Allocation__c IN :IdofAllocation];*/
        if(IdofAllocation != null && IdofAllocation.size()>0){
            journalList = [SELECT Id,Allocation_Value__c,Allocation__c,Invoice__c,Allocation__r.Department_Allocation__c FROM Journal__c WHERE Allocation__c IN :IdofAllocation AND Invoice__c =: invoiceDetailsWrap.invoice.Id];
              
            for(Id objId : IdofAllocation){
                Decimal amountJournal =0;
                if(journalList != null && journalList.size()>0){
                    for(Journal__c objJournal : journalList){
                        if(objJournal.Allocation__c == objId && objJournal.Allocation__c != null){
                            if(objJournal.Allocation_Value__c != null && objJournal.Allocation_Value__c != 0.00){
                                amountJournal = amountJournal + objJournal.Allocation_Value__c;
                            }else{
                                amountJournal = amountJournal + 0;
                            }
                        }
                    }
                    
                }
                mapjournAllocation.put(objId,amountJournal);
            }
        }
        Integer rowNo = 1;
        /*if(invoiceDetailsWrap.invoice.Invoice_Allocation_Junctions__r != null && invoiceDetailsWrap.invoice.Invoice_Allocation_Junctions__r.size()>0 ){
        for(Invoice_Allocation_Junction__c objInvoAlloc : invoiceDetailsWrap.invoice.Invoice_Allocation_Junctions__r){
            journalLstWrapper wrapperObj = new journalLstWrapper();
            wrapperObj.invoiceAlocationObj = objInvoAlloc;
            wrapperObj.wrapperId = rowNo;
            wrapperObj.journalAmount = mapjournAllocation.get(objInvoAlloc.Allocation__c);
            if(objInvoAlloc.Allocation__r.Allocation_Amount__c != null){
                wrapperObj.existingjournalAmount = objInvoAlloc.Allocation__r.Allocation_Amount__c + (mapjournAllocation.get(objInvoAlloc.Allocation__c));
            }else{
                wrapperObj.existingjournalAmount = 0;
            }
            wrapperObj.journalDeduct = '0';
            wrapperObj.journalAllocate = '0';
            lstofJournalWrapper.add(wrapperObj);
            rowNo++;
        }
        }*/
        
        // creating Journal wrapper
        
        if(!lstofAllocation.isEmpty()){
            for(Allocation__c objectallo : lstofAllocation){
                 journalLstWrapper wrapperObj = new journalLstWrapper();
                 wrapperObj.invoiceAlocationObj = objectallo;
                 wrapperObj.wrapperId = rowNo;
                 if(mapjournAllocation.containsKey(objectallo.Id)){
                     wrapperObj.journalAmount = mapjournAllocation.get(objectallo.Id);
                 }else{
                      wrapperObj.journalAmount = 0.00;
                 }
                if(mapIdlInvoice.containsKey(objectallo.Id)){
                    wrapperObj.inovoiceamount = mapIdlInvoice.get(objectallo.Id).Amount__c;
                }else{
                    wrapperObj.inovoiceamount = 0.00;
                }
                // if(objectallo.Allocation_Amount__c != null){
                    // if(mapjournAllocation.containsKey(objectallo.Id)){
                        // wrapperObj.existingjournalAmount = objectallo.Allocation_Amount__c + (mapjournAllocation.get(objectallo.Id));
                    // }else{
                        // wrapperObj.existingjournalAmount = objectallo.Allocation_Amount__c;
                    // }
                    
                // }else{
                    // wrapperObj.existingjournalAmount = 0;
                // }
                if(wrapperObj.inovoiceamount==null)
                wrapperObj.inovoiceamount=0.0;
                  if(wrapperObj.journalAmount==null)
                wrapperObj.journalAmount=0.0;
                
                
                
                wrapperObj.existingjournalAmount = wrapperObj.inovoiceamount+wrapperObj.journalAmount;
                if(objectallo.externalCompany__c != null){
                    wrapperObj.nameofRecord = ''+objectallo.externalCompany__r.Name+'-'+objectallo.externalContact__r.Name+'-- External';
                    wrapperObj.intorext = 'Ext';
                }else{
                    wrapperObj.nameofRecord = ''+objectallo.Assigned_To__r.name+'-'+objectallo.Department_Allocation__c+'-'+objectallo.workType_Allocation__c+'--Internal';
                    wrapperObj.intorext = 'Int';
                }
                if(invoiceDetailsWrap != null){
                    wrapperObj.invid = invoiceDetailsWrap.invoice.Id;
                }
                wrapperObj.journalDeduct = '0';
                wrapperObj.journalAllocate = '0';
                lstofJournalWrapper.add(wrapperObj);
                rowNo++;
            }
        }
        allocationSize = 0;
        if(lstofJournalWrapper.size()>0)
            allocationSize = allocationSize +lstofJournalWrapper.size();
        system.debug('--journalList'+lstofJournalWrapper);
        
        
            
    }
    
    // method to save the Journal record   
    public void saveJournal(){
        List<Journal__c> lstOfJournalupsert = new List<Journal__c>();
        if(lstofJournalWrapper != null&&(!lstofJournalWrapper.Isempty())){
            system.debug('CNcountMap---->'+financecode);
                if(!string.isBlank(financecode) && Integer.valueOf(financecode)>0){
                    Journal__c journalInternal = new Journal__c();
                    Journal__c journalExternal = new Journal__c();
                    Journal__c journalExtToInt = new Journal__c();
                    Journal__c journalIntToExt = new Journal__c();
                    String mainallocationId='';
                    mainallocationId = lstofJournalWrapper[Integer.valueOf(financecode)-1].invoiceAlocationObj.Id;
                    boolean isexternal = false;
                    if(lstofJournalWrapper[Integer.valueOf(financecode)-1].invoiceAlocationObj.externalCompany__c != null && string.isNotBlank(lstofJournalWrapper[Integer.valueOf(financecode)-1].invoiceAlocationObj.externalCompany__c)){
                        isexternal = true;
                    }
                    if(isexternal){
                        journalExternal.Allocation__c = mainallocationId;
                        journalExternal.Invoice__c = invoiceDetailsWrap.invoice.Id; 
                        journalExternal.Finance_Code__c = '7510';
                        journalExternal.Type__c = 'E-E';
                        journalExternal.Month_to_Journal__c = System.today();
                        journalExternal.Status__c = 'Raised';
                        journalExternal.Allocation_Value__c = 0.00;
                        journalExtToInt.Allocation__c = mainallocationId;
                        journalExtToInt.Invoice__c = invoiceDetailsWrap.invoice.Id;
                        journalExtToInt.Finance_Code__c = '7510';
                        journalExtToInt.Type__c = 'E-I';
                        journalExtToInt.Month_to_Journal__c = System.today();
                        journalExtToInt.Status__c = 'Raised';
                        journalExtToInt.Allocation_Value__c = 0.00;
                    }else{
                        journalInternal.Allocation__c = mainallocationId;
                        journalInternal.Invoice__c = invoiceDetailsWrap.invoice.Id;
                        journalInternal.Finance_Code__c = '1010';
                        journalInternal.Type__c = 'I-I';
                        journalInternal.Month_to_Journal__c = System.today();
                        journalInternal.Status__c = 'Raised';
                        journalInternal.Allocation_Value__c = 0.00;
                        journalIntToExt.Allocation__c = mainallocationId;
                        journalIntToExt.Invoice__c = invoiceDetailsWrap.invoice.Id;
                        journalIntToExt.Finance_Code__c = '1022';
                        journalIntToExt.Type__c = 'I-E';
                        journalIntToExt.Month_to_Journal__c = System.today();
                        journalIntToExt.Status__c = 'Raised';
                        journalIntToExt.Allocation_Value__c = 0.00;
                    }
                /*for(journalLstWrapper obj :lstofJournalWrapper){
                    
                    
                    if(String.isNotBlank(obj.journalDeduct)){
                        if(Decimal.valueOf(obj.journalDeduct.remove(',')) != null && Decimal.valueOf(obj.journalDeduct.remove(','))>0.00){
                            
                            /*objectJournal.Allocation__c = obj.invoiceAlocationObj.Id;
                            objectJournal.Invoice__c = obj.invid;
                            objectJournal.Allocation_Value__c = -1*Decimal.valueOf(obj.journalDeduct.remove(','));
                            objectJournal.Month_to_Journal__c = System.today();
                            objectJournal.Status__c = 'Awaiting Approval';
                            if(!string.isBlank(financecode) && Integer.valueOf(financecode)>0){                            
                                objectJournal.Finance_Code__c = financecode;
                            }
                            lstOfJournalupsert.add(objectJournal);
                        }
                    }
                }*/
                for(journalLstWrapper obj :lstofJournalWrapper){
                    
                   
                    if(String.isNotBlank(obj.journalAllocate)){
                        if(Decimal.valueOf(obj.journalAllocate.remove(',')) != null && Decimal.valueOf(obj.journalAllocate.remove(','))>0.00){
                            if(isexternal){
                                if((obj.invoiceAlocationObj.externalCompany__c != null && string.isNotBlank(obj.invoiceAlocationObj.externalCompany__c)) && isexternal){
                                    journalExternal.Allocation_Value__c = journalExternal.Allocation_Value__c +  (-1*Decimal.valueOf(obj.journalAllocate.remove(',')));
                                    Journal__c objectJournal = new Journal__c();
                                    objectJournal.Allocation__c = obj.invoiceAlocationObj.Id;
                                    objectJournal.Invoice__c = obj.invid;
                                    objectJournal.Allocation_Value__c = Decimal.valueOf(obj.journalAllocate.remove(','));                                   
                                    objectJournal.Finance_Code__c = '7510';
                                    objectJournal.Type__c = 'E-E';
                                    objectJournal.Month_to_Journal__c = System.today();
                                    objectJournal.Status__c = 'Raised';
                                    lstOfJournalupsert.add(objectJournal);
                                }
                                else{
                                    journalExtToInt.Allocation_Value__c = journalExtToInt.Allocation_Value__c +  (-1*Decimal.valueOf(obj.journalAllocate.remove(',')));
                                    Journal__c objectJournal = new Journal__c();
                                    objectJournal.Allocation__c = obj.invoiceAlocationObj.Id;
                                    objectJournal.Invoice__c = obj.invid;
                                    objectJournal.Type__c = 'E-I';
                                    objectJournal.Allocation_Value__c = Decimal.valueOf(obj.journalAllocate.remove(','));                                   
                                    objectJournal.Finance_Code__c = '1022';
                                    objectJournal.Month_to_Journal__c = System.today();
                                    objectJournal.Status__c = 'Raised';
                                    lstOfJournalupsert.add(objectJournal);
                                }
                            }else{
                                if(!(obj.invoiceAlocationObj.externalCompany__c != null && string.isNotBlank(obj.invoiceAlocationObj.externalCompany__c)) && !isexternal){
                                    journalInternal.Allocation_Value__c = journalInternal.Allocation_Value__c +  (-1*Decimal.valueOf(obj.journalAllocate.remove(',')));
                                    Journal__c objectJournal = new Journal__c();
                                    objectJournal.Allocation__c = obj.invoiceAlocationObj.Id;
                                    objectJournal.Invoice__c = obj.invid;
                                    objectJournal.Type__c = 'I-I';
                                    objectJournal.Allocation_Value__c = Decimal.valueOf(obj.journalAllocate.remove(','));                                   
                                    objectJournal.Finance_Code__c = '1010';
                                    objectJournal.Month_to_Journal__c = System.today();
                                    objectJournal.Status__c = 'Raised';
                                    lstOfJournalupsert.add(objectJournal);
                                }
                                else if(obj.invoiceAlocationObj.externalCompany__c != null && string.isNotBlank(obj.invoiceAlocationObj.externalCompany__c)){
                                    journalIntToExt.Allocation_Value__c = journalIntToExt.Allocation_Value__c +  (-1*Decimal.valueOf(obj.journalAllocate.remove(',')));
                                    Journal__c objectJournal = new Journal__c();
                                    objectJournal.Allocation__c = obj.invoiceAlocationObj.Id;
                                    objectJournal.Invoice__c = obj.invid;
                                    objectJournal.Type__c = 'I-E';
                                    objectJournal.Allocation_Value__c = Decimal.valueOf(obj.journalAllocate.remove(','));                                   
                                    objectJournal.Finance_Code__c = '7510';
                                    objectJournal.Month_to_Journal__c = System.today();
                                    objectJournal.Status__c = 'Raised';
                                    lstOfJournalupsert.add(objectJournal);
                                }
                            }
                            /*Journal__c objectJournal = new Journal__c();
                            objectJournal.Allocation__c = obj.invoiceAlocationObj.Id;
                            objectJournal.Invoice__c = obj.invid;
                            objectJournal.Allocation_Value__c = Decimal.valueOf(obj.journalAllocate.remove(','));
                            if(isexternal){ 
                                objectJournal.Finance_Code__c = '1022';
                            }
                            objectJournal.Month_to_Journal__c = System.today();
                            objectJournal.Status__c = 'Awaiting Approval';
                            if( )
                            Journal__c objectJournal = new Journal__c();
                            objectJournal.Allocation__c = mainallocationId;
                            objectJournal.Invoice__c = obj.invid;
                            objectJournal.Allocation_Value__c =  -1*Decimal.valueOf(obj.journalAllocate.remove(',')); 
                            objectJournal.Month_to_Journal__c = System.today();
                            objectJournal.Status__c = 'Awaiting Approval'; 
                            lstOfJournalupsert.add(objectJournal);*/
                        }
                    }
                }
                if(isexternal){
                    if(journalExternal.Allocation_Value__c != 0.00){
                        lstOfJournalupsert.add(journalExternal);
                    }
                    if(journalExtToInt.Allocation_Value__c != 0.00){
                        lstOfJournalupsert.add(journalExtToInt);
                    }
                    
                }else{
                    if(journalInternal.Allocation_Value__c != 0.00){
                        lstOfJournalupsert.add(journalInternal);
                    }
                    if(journalIntToExt.Allocation_Value__c != 0.00){
                        lstOfJournalupsert.add(journalIntToExt);
                    }
                    
                }
                if(isactivejournal){
                    
                    try{
                        Journal_Transaction_Num__c objJournalTransactionNum  = new Journal_Transaction_Num__c();
                        objJournalTransactionNum = Journal_Transaction_Num__c.getOrgDefaults();
                        String Patternval;
                        if(objJournalTransactionNum != null){
                            
                            Patternval = objJournalTransactionNum.Pattern__c;
                            String transactionNum = string.valueof(integer.valueof(objJournalTransactionNum.Transaction_Num__c));
                            Patternval = Patternval.substring(0,Patternval.length()-transactionNum.Length())+transactionNum;
                            Patternval = Patternval.replaceall('X','0');
                            objJournalTransactionNum.Transaction_Num__c= objJournalTransactionNum.Transaction_Num__c+1;
                            update objJournalTransactionNum;
                            if(!String.isBlank(Patternval)){
                                for(Journal__c objJournal : lstOfJournalupsert){
                                    objJournal.Transaction_Id__c = Patternval;
                                    
                                }
                            }
                        }
                        
                        if(!lstOfJournalupsert.isEmpty()){
                            insert lstOfJournalupsert;
                            lstofJournalWrapper.clear();
                            /*ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Record Created Successfully.Thank you!'));*/
                            ObjModal=new ErrorHandling(true,'Journal Record Saved Successfully.','Success'); 
                            journaldata();
                        }else{
                           /* ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No data available for creating Journal Data'));*/
                            ObjModal.ErrorHandling(true,'No data available for creating Journal Data','Error',true);
                        }
                       
                    }catch(Exception e){
                         //ApexPages.addMessages(e);
                         ObjModal=new ErrorHandling(true,e.getMessage(),'Error');
                    }
                }else{
                    ObjModal.ErrorHandling(true,'Job Manager’s Coding Structure is Inactive – Please contact the System Administrator to change the job manager on the job','Error',true);
                }
            }
        }
        else{
                    /*ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No data available for creating Journal Data'));*/
                    ObjModal.ErrorHandling(true,'No data available for creating Journal Data','Error',true);
                }
    }
    
    
    public void close_ErrorModal(){
        ObjModal=new ErrorHandling();
    }
    public class journalLstWrapper{
        public Allocation__c invoiceAlocationObj{get;set;}
        public integer wrapperId{get;set;}
        Public Decimal journalAmount{get;set;}
        Public Decimal existingjournalAmount{get;set;}
        Public Decimal inovoiceamount{get;set;}
        Public string journalDeduct{get;set;}
        Public string journalAllocate{get;set;}
        public string invid{get;set;}
        public string nameofRecord{get;set;}
        public string intorext{get;set;}
        public journalLstWrapper(){}
        
    }
    public class invoiceLstWrapper{
        public Boolean isChecked {get;set;}
        public Invoice__c invoice{get;set;}
        public integer wrapperId{get;set;}
        public invoiceLstWrapper(integer wrap,Boolean ck,invoice__c inv) {
            isChecked = ck;
            invoice =inv;    
            wrapperId=wrap;   
        }
        public invoiceLstWrapper(){}
        
    }
    public class ErrorHandling{
        public boolean displayerror{get;set;}
        public string Errormsg{get;set;}
        public string Title{get;set;}
        Public List<string> ErrorList{get;set;}
        public boolean hasmore{get;set;}
        public ErrorHandling(boolean displayerror, string Errormsg,string Title){
            this.displayerror =displayerror;
            this.Errormsg=Errormsg;
            this.Title=Title;
            this.hasmore=false;
            ErrorList = new List<string>();
        }
        public  void ErrorHandling(boolean displayerror, string Errormsg,string Title,boolean hasmore){
            this.displayerror =displayerror;
            ErrorList.add(Errormsg);
            this.Title=Title;
            this.hasmore=hasmore;
        }
        public ErrorHandling(){
            this.displayerror =false;
            this.Errormsg='';
            ErrorList = new List<string>();
        }
    }
    
}