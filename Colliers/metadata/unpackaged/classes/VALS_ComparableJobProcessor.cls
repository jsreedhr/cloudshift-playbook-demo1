// Utility class for performing VALS - Comparables Call Out 
// Developer: Asif Muhammad (AM^2K) - dQuotient;
public with sharing class VALS_ComparableJobProcessor 
{
    public String validationString;
    public Boolean validationError;
    public Boolean isMarketRent;
    public String jsonData;
    public list<String> propertyTypesString;
    public VALS_ComparableJobProcessor(Job_Property_Junction__c jobPropObj) 
    {
        Opportunity oppObj = New Opportunity();
        Property__c propObj = New Property__c();
        Staff__c jobManagerObj = New Staff__c();
        User userObj = New User();

        //Wrapper Declarations 
        LocationWrapper locationWrapperObj;
        PropertyWrapper propertyWrapperObj;

        validationString = 'Call out cannot proceed, ';
        validationError = false;
        propertyTypesString = New list<String>();
        propertyTypesString.add('');

        Id jobId = jobPropObj.Job__c;
        Id propId = jobPropObj.Property__c;
        system.debug('jobId----->' +jobId);
        if(jobId!=NULL && propId!=NULL)
        {
            try{
                oppObj = [Select id,Name,Work_Type__c,Job_Number__c,Manager__c,Job_Manager_HR_Number__c,CloseDate,Date_Instructed__c
                      from Opportunity
                      where ID =: jobId LIMIT 1];
            }catch(Exception e){
                System.debug('Error is ' +e);
            }

            System.Debug('Work Type: '+oppObj.Work_Type__c);
            System.Debug(oppObj.Date_Instructed__c);

            propObj = [Select id,Vals_Property_Type__c,Property_Type__c,Sq_Feet__c,Full_Street__c,Street__c,Estate__c,Area__c,Town__c,County__c,
                       Post_Code__c,Name,Geolocation__c
                       from Property__c
                       where ID =: propId LIMIT 1];
        }

        if(oppObj.ID!=NULL && propObj.ID!=NULL)
        {
            if(oppObj.Manager__c!=NULL)
            {
                jobManagerObj = [Select id,Email__c,User__c,FIRST_NAME__c,SURNAME__c from Staff__c where id =: oppObj.Manager__c LIMIT 1];

                if(jobManagerObj.User__c!=NULL)
                {
                    userObj = [Select id,Department,Phone from User where id =: jobManagerObj.User__c LIMIT 1];
                }
            }
            checkForWorktype(oppObj);
            if(isMarketRent!=NULL)
            {
                //Formatting Latitude and Longitude to String
                String latLng = '';
                if(propObj.Geolocation__c!=NULL)
                    latLng = propObj.Geolocation__c.getLatitude()+','+propObj.Geolocation__c.getLongitude();
                String jobManager = '';
                String jobManagerEmail = '';
                String jobManagerDept = '';
                String jobManagerMobile = '';
                if(jobManagerObj!=NULL && jobManagerObj.FIRST_NAME__c!=NULL && jobManagerObj.SURNAME__c!=NULL)
                    jobManager = jobManagerObj.FIRST_NAME__c + ' ' + jobManagerObj.SURNAME__c;
                if(jobManagerObj!=NULL && jobManagerObj.Email__c!=NULL)
                    jobManagerEmail = jobManagerObj.Email__c;
                if(userObj!=NULL && userObj.Department!=NULL)
                    jobManagerDept = userObj.Department;
                if(userObj!=NULL && userObj.Phone!=NULL)
                    jobManagerMobile = userObj.Phone;
                if(PropObj.Property_Type__c!=NULL)
                    propertyTypesString = PropObj.Property_Type__c.split(';');
                else
                    propertyTypesString = NULL; 

                locationWrapperObj = New LocationWrapper (propObj.Full_Street__c,propObj.Street__c,
                                                          propObj.Estate__c,propObj.Area__c,
                                                          propObj.Town__c,propObj.County__c,
                                                          propObj.Post_Code__c,String.valueOf(PropObj.id),
                                                          propObj.Name,latLng);
                
                //propertyWrapperObj = New PropertyWrapper (propObj.Sq_Feet__c);
                propertyWrapperObj = New PropertyWrapper (5.00);

                //ISO 9601 Formatting
                String closeDate = oppObj.CloseDate.year() + '-' + oppObj.CloseDate.month() + '-' + oppObj.CloseDate.day();

                if(isMarketRent==true)
                {
                    MarketRentExtensionsWrapper mrExtObj = new MarketRentExtensionsWrapper (0.0,0.0);

                    Date dt;

                    MarketRentWrapper mrObj = New MarketRentWrapper (oppObj.Name,'comparable-market-rent',
                                                                     oppObj.Id,propertyTypesString,
                                                                     'Salesforce',String.ValueOf(oppObj.Job_Manager_HR_Number__c),
                                                                      mrExtObj,propertyWrapperObj,locationWrapperObj,oppObj.Date_Instructed__c,
                                                                      jobManager,
                                                                      jobManagerEmail,jobManagerDept,jobManagerMobile);
                    validateMarketRent(mrObj);
                    list<MarketRentWrapper> marketRentWrapperList = New List<MarketRentWrapper>();
                    marketRentWrapperList.add(mrObj);
                    jsonData = JSON.serialize(marketRentWrapperList);
                }
                else
                {
                    MarketValueExtensionsWrapper mvExtObj = New MarketValueExtensionsWrapper (0.0,0.0);

                    MarketValueWrapper mvObj = New MarketValueWrapper (oppObj.Name,'comparable-market-valuation',
                                                                       oppObj.Id,propertyTypesString,
                                                                       'Salesforce',String.valueOf(oppObj.Job_Manager_HR_NUmber__c),
                                                                       mvExtObj,propertyWrapperObj,locationWrapperObj,oppObj.Date_Instructed__c,
                                                                       jobManager,jobManagerEmail,jobManagerDept,jobManagerMobile);
                    validateMarketValue(mvObj);
                    list<MarketValueWrapper> marketValueWrapperList = New List<MarketValueWrapper>();
                    marketValueWrapperList.add(mvObj);
                    jsonData = JSON.serialize(marketValueWrapperList);
                }
                System.debug('Le JSON: '+jsonData);
                if(jsonData!=NULL && validationError==false)
                    doCallOut(jsonData,jobPropObj.ID);
            }
            else
            {
                validationString+='Could not identify Market Type, ';
                validationError=true;
            }
        }
        System.Debug('Do we have a Validation Problem? '+validationError);
    }
    public void validateMarketValue(MarketValueWrapper mvObj)
    {
        if(!(mvObj.type!=NULL && mvObj.source!=NULL &&
             mvObj.propertyTypes!=NULL && mvObj.propertyTypes.size()>0 && mvObj.contactEmployeeReference!=NULL && mvObj.propertyExtensions!=NULL &&
             mvObj.property!=NULL && mvObj.location!=NULL && mvObj.jobManager!=NULL && mvObj.jobManagerEmail!=NULL &&
             mvObj.jobManagerDept!=NULL && mvObj.jobManagerMobile!=NULL))
        {
            validationError = true;
            validationString+='Required fields are Missing!!';
        }
        else if(!(mvObj.location.addressLine1!=NULL && mvObj.location.externalReference!=NULL))
        {
            validationError = true;
            validationString+='Required fields are Missing!!';
        }
        else if(!(mvObj.propertyExtensions!=NULL))
        {
            validationError = true;
            validationString+='Required fields are Missing!!';
        }
    }
    public void validateMarketRent(MarketRentWrapper mrObj)
    {
        if(!(mrObj.type!=NULL && mrObj.source!=NULL &&
             mrObj.propertyTypes!=NULL && mrObj.propertyTypes.size()>0 && mrObj.contactEmployeeReference!=NULL && mrObj.propertyExtensions!=NULL &&
             mrObj.property!=NULL && mrObj.location!=NULL && mrObj.jobManager!=NULL && mrObj.jobManagerEmail!=NULL &&
             mrObj.jobManagerDept!=NULL && mrObj.jobManagerMobile!=NULL))
        {
            validationError = true;
            validationString+='Required fields are Missing!';
        }
        else if(!(mrObj.location.addressLine1!=NULL && mrObj.location.externalReference!=NULL))
        {
            validationError = true;
            validationString+='Required fields are Missing!';
        }
        else if(!(mrObj.propertyExtensions!=NULL))
        {
            validationError = true;
            validationString+='Required fields are Missing!';
        }

    }
    @future(callout= true)
    public static void doCallOut(String jsonData,ID jobPropId)
    {
        Map<string,ShareDo_Credentials__c> shareDo = ShareDo_Credentials__c.getAll();
        
        //Encoding Api Key for authorization
        String apiKey = EncodingUtil.base64Encode(Blob.valueOf(shareDo.get('Sharedo Comparable').API_Key__c));
        //String authorizationHeader = 'bearer '+apiKey; 
        //String authorizationHeader = 'bearer '+  'ZGM3NjU4NzYtYjA4YS00MjJkLTgzMmUtNzYzZGI3ODRhOTUy';
        //Colliers UAT:
        String authorizationHeader = 'bearer ' + apikey;

        String username = 'EU\\svc_sharedoAPI';
        String password = shareDo.get('Sharedo Comparable').ShareDo_Password__c;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        System.Debug('Le Uname and Pwd: '+username + ':' + password);

        String authorizationHeaderBasic = 'BASIC ' +EncodingUtil.base64Encode(headerValue);

        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        req.setHeader('Authorization', authorizationHeaderBasic);
        req.setHeader('X-Authorization', authorizationHeader);
        req.setBody(jsonData);
        System.Debug('Le Request: '+req);
        System.Debug('Le X-Authorization: '+req.getHeader('X-Authorization'));
        System.Debug('Le Authorization: '+req.getHeader('Authorization'));
        req.setEndpoint(shareDo.get('Sharedo Comparable').shareDoUrl__c);
        System.debug('You shall not pass: ' + shareDo.get('Sharedo Comparable').shareDoUrl__c);

        try
        {   Http http = new Http();
            HTTPResponse res = http.send(req);
            System.debug('ShareDo Response:' +res.getStatusCode()+'-- ' + res.getBody().substringAfter('errorMessage'));
            if(res.getStatusCode()==201 || res.getStatusCode()==200)
            {
                Job_Property_Junction__c jobPropObj =[Select id,Sent_to_ShareDo__c from Job_Property_Junction__c
                                                      where ID=:jobPropId LIMIT 1];
                if(jobPropObj.id!=NULL)
                {
                    jobPropObj.Sent_to_ShareDo__c=true;
                    try{
                    update jobPropObj;
                    }catch(DmlException e)
                    {
                        System.Debug('Error on Updating Job Property Junction');
                    }
                }   
            }   
        }catch(Exception e)
        {
            System.Debug('An exception has been encountered: '+e);  
        }
    }
    public void checkForWorktype(Opportunity oppObj)
    {
        Map<string,MarketRent__c> marketRent = MarketRent__c.getAll();
        Map<string,MarketValue__c> marketValue = MarketValue__c.getAll();
        System.Debug('Job Worktype: '+oppObj.Work_Type__c);
        if(oppObj.Work_Type__c!=NULL)
        {
            System.Debug('Job Work Type: '+oppObj.Work_Type__c);
            for(MarketRent__c mrObj : marketRent.values())
            {
                if(oppObj.Work_Type__c==mrObj.Worktype_Name__c)
                    isMarketRent = true;
            }
            for(MarketValue__c mvObj : marketValue.values())
            {
                if(oppObj.Work_Type__c==mvObj.Worktype_Name__c)
                    isMarketRent = false;
            }
        }
        else
        {
            validationError = true;
            validationString+='Missing Required fields-->Property.Work_Type__c, ';
            //isMarketRent = false;
        }
    }

    //A stream of Wrapper classes follow........
    public class MarketValueWrapper
    {
        String title;
        String type;
        //String name;
        String reference;
        list<String> propertyTypes;
        String source;
        String contactEmployeeReference;
        MarketValueExtensionsWrapper propertyExtensions;
        PropertyWrapper property;
        LocationWrapper location;
        Date dateInstructed;
        String jobManager;
        String jobManagerEmail;
        String jobManagerDept;
        String jobManagerMobile;
        public MarketValueWrapper(String title, String type,String reference,
                                  list<String> propertyTypes, String source, String contactEmployeeReference,
                                  MarketValueExtensionsWrapper propertyExtensions, PropertyWrapper property,
                                  LocationWrapper location,Date dateInstructed, String jobManager,
                                  String jobManagerEmail, String jobManagerDept, String jobManagerMobile)
        {
            if(title!=NULL)
                this.title = title;
            if(type!=NULL)
                this.type = type;
            if(reference!=NULL)
                this.reference = reference;
            if(propertyTypes!=NULL && propertyTypes.size()>0)
                this.propertyTypes = propertyTypes;
            if(source!=NULL)
                this.source = source;
            if(contactEmployeeReference!=NULL)
                this.contactEmployeeReference = contactEmployeeReference;
            if(propertyExtensions!=NULL)
                this.propertyExtensions = propertyExtensions;
            if(property!=NULL)
                this.property = property;
            if(location!=NULL)
                this.location = location;
            if(dateInstructed!=NULL)
                this.dateInstructed = dateInstructed;
            if(jobManager!=NULL)
                this.jobManager = jobManager;
            if(jobManagerEmail!=NULL)
                this.jobManagerEmail = jobManagerEmail;
            if(jobManagerDept!=NULL)
                this.jobManagerDept = jobManagerDept;
            if(jobManagerMobile!=NULL)
                this.jobManagerMobile = jobManagerMobile;
        }
    }
    public class MarketRentWrapper
    {
        String title;
        String type;
        //String name;
        String reference;
        list<String> propertyTypes;
        String source;
        String contactEmployeeReference;
        MarketRentExtensionsWrapper propertyExtensions;
        PropertyWrapper property;
        LocationWrapper location;
        Date dateInstructed;
        String jobManager;
        String jobManagerEmail;
        String jobManagerDept;
        String jobManagerMobile;
        public MarketRentWrapper(String title, String type,String reference,
                                 list<String> propertyTypes, String source, String contactEmployeeReference,
                                 MarketRentExtensionsWrapper propertyExtensions, PropertyWrapper property,
                                 LocationWrapper location,Date dateInstructed, String jobManager,
                                 String jobManagerEmail, String jobManagerDept, String jobManagerMobile)
        {
            if(title!=NULL)
                this.title = title;
            if(type!=NULL)
                this.type = type;
            if(reference!=NULL)
                this.reference = reference;
            if(propertyTypes!=NULL && propertyTypes.size()>0)
                this.propertyTypes = propertyTypes;
            if(source!=NULL)
                this.source = source;
            if(contactEmployeeReference!=NULL)
                this.contactEmployeeReference = contactEmployeeReference;
            if(propertyExtensions!=NULL)
                this.propertyExtensions = propertyExtensions;
            if(property!=NULL)
                this.property = property;
            if(location!=NULL)
                this.location = location;
            if(dateInstructed!=NULL)
                this.dateInstructed = dateInstructed;
            if(jobManager!=NULL)
                this.jobManager = jobManager;
            if(jobManagerEmail!=NULL)
                this.jobManagerEmail = jobManagerEmail;
            if(jobManagerDept!=NULL)
                this.jobManagerDept = jobManagerDept;
            if(jobManagerMobile!=NULL)
                this.jobManagerMobile = jobManagerMobile;
        }

    }
    public class MarketValueExtensionsWrapper
    {
        Decimal purchasePrice;
        Decimal capitalValuePsf;
        public marketValueExtensionsWrapper(Decimal purchasePrice, Decimal capitalValuePsf) 
        {
            if(purchasePrice!=NULL)
                this.purchasePrice = purchasePrice;
            if(capitalValuePsf!=NULL)
                this.capitalValuePsf = capitalValuePsf;
        }
    }
    public class MarketRentExtensionsWrapper
    {
        Decimal netYield;
        Decimal rentPerSqf;
        public MarketRentExtensionsWrapper(Decimal netYield, Decimal rentPerSqf) 
        {
            if(netYield!=NULL)
                this.netYield = netYield;
            if(rentPerSqf!=NULL)
                this.rentPerSqf = rentPerSqf;
        }
    }
    public class PropertyWrapper
    {
        Decimal size;
        public propertyWrapper(Decimal size)
        {
            if(size!=NULL)
                this.size = size;
        }
    }
    public class LocationWrapper
    {
        String addressline1;
        String addressLine2;
        String addressLine3;
        String addressLine4;
        String town;
        String county;
        String postCode;
        String externalReference;
        String name;
        String latLng;
        public LocationWrapper(String addressLine1, String addressLine2,
                               String addressLine3, String addressLine4,
                               String town, String county, String postCode,
                               String externalReference, String name,
                               String latLng)
        {
            if(addressLine1!=NULL)
                this.addressline1 = addressLine1;
            if(addressLine2!=NULL)  
                this.addressLine2 = addressLine2;
            if(addressLine3!=NULL)
                this.addressLine3 = addressLine3;
            if(addressLine4!=NULL)
                this.addressLine4 = addressLine4;
            if(town!=NULL)
                this.town = town;
            if(county!=NULL)
                this.county = county;
            if(postCode!=NULL)
                this.postCode = postCode;
            if(externalReference!=NULL)
                this.externalReference = externalReference;
            if(name!=NULL)
                this.name = name;
            if(latLng!=NULL)
                this.latLng = latLng;
        }
    }
}