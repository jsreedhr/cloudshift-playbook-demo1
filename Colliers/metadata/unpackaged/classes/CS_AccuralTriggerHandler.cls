/**
 *  Class Name: CS_AccuralTriggerHandler  
 *  Description: This is a Trigger Handler Class for trigger on Accrual__c
 *  Company: dQuotient
 *  CreatedDate:02/11/2016 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Anand              02/11/2016                 Orginal Version
 *
 */
public class CS_AccuralTriggerHandler {
    
    public static Schema.DescribeFieldResult fieldResult; 
    public static List<Schema.PicklistEntry> lstPickList;
    public static String statusAwaitingApproval;
    public static String statusApproval;
    public static String statusReversedbyUser;
    public static String statusReversedbyInvoice;
    public static String statusReversedbyJM;
    public static String statusRejected;
    public static String statusAccuralReverse;
    
    
    
    public CS_AccuralTriggerHandler(){
        
        fieldResult = Accrual__c.Status__c.getDescribe();
        lstPickList = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry objPickList : lstPickList){
            
            if(objPickList.getValue() == Label.AccuralApprovedStatus){
                statusApproval = objPickList.getLabel();
            }
          
            if(objPickList.getValue() == Label.AccuralAwaitingApprovedStatus){
                statusAwaitingApproval = objPickList.getLabel();
            }
            
            if(objPickList.getValue() == 'Reversed By User'){
                statusReversedbyUser = objPickList.getLabel();
            }
            
            if(objPickList.getValue() == 'Reversed By Invoice'){
                statusReversedbyInvoice = objPickList.getLabel();
            }
            
            if(objPickList.getValue() == 'Reversed By User JM'){
                statusReversedbyJM = objPickList.getLabel();
            }
            
            if(objPickList.getValue() == 'Rejected'){
                statusRejected = objPickList.getLabel();
            }
            
            if(objPickList.getValue() == 'Accrual - Reversal'){
                statusAccuralReverse = objPickList.getLabel();
            }
            
            
        } 
    
        System.debug(statusApproval + '   '+ statusAwaitingApproval);
    }
    
    /**
     *  Method Name: submitAccuralForApproval  
     *  Description: Method to submit accurals for approvals on insert
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void insertcodingStructure(List<Accrual__c> lstAccural){
        
        for (Accrual__c objAccural: lstAccural){
            
            // 
            if(objAccural.Status__c == statusAwaitingApproval && objAccural.Amount__c != null && objAccural.Amount__c > 0){
                
                // create the new approval request to submit
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                
                // Setting the Request Parameters
                req.setComments('Submitted for approval. Please approve.');
                req.setObjectId(objAccural.Id);
                
                // submit the approval request for processing
                Approval.ProcessResult result = Approval.process(req);
                
                // display if the reqeust was successful
                System.debug('Submitted for approval successfully: '+result.isSuccess());
            
            }
        }
        
    }
    
    /**
     *  Method Name: insertReverseAccuralCreation 
     *  Description: Method to create Reverse Accural on create
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void insertReverseAccuralCreation(List<Accrual__c> lstAccural){
        
        map<id, Accrual__c> mapIdAccuralToUpdate = new map<id, Accrual__c>();
        for (Accrual__c objAccural: lstAccural){
            if(objAccural.Status__c == statusReversedbyUser || objAccural.Status__c == statusReversedbyInvoice || objAccural.Status__c == statusReversedbyJM){
                mapIdAccuralToUpdate.put(objAccural.id, objAccural);
            }
        }
        createReversalAccural(mapIdAccuralToUpdate);
        
    }
    
    
    /**
     *  Method Name: updateReverseAccuralCreation 
     *  Description: Method to create Reverse Accural on Update
     *  Param:  Trigger.newMap, Trigger.oldMap
     *  Return: None
    */
    public static void updateReverseAccuralCreation(map<id, Accrual__c> mapOldAccural, map<id, Accrual__c> mapNewAccural){
        
        map<id, Accrual__c> mapIdAccuralToUpdate = new map<id, Accrual__c>();
        for (Accrual__c objAccural: mapNewAccural.values()){
            if(mapOldAccural.get(objAccural.id).Status__c != objAccural.Status__c && (objAccural.Status__c == statusReversedbyUser || objAccural.Status__c == statusReversedbyInvoice || objAccural.Status__c == statusReversedbyJM)){
                mapIdAccuralToUpdate.put(objAccural.id, objAccural);
            }
        }
        createReversalAccural(mapIdAccuralToUpdate);
        
    }
    
    /**
     *  Method Name: createReversalAccural  
     *  Description: Method to create Reverse Accural record
     *  Param:  map<id, Accrual__c>
     *  Return: None
    */
    public static void createReversalAccural(map<id, Accrual__c> mapIdAccural){
        
        if(!mapIdAccural.keyset().isEmpty()){
            
            List<Accrual__c> lstReverseAccural = new List<Accrual__c>();
            
            set<id> setJobIds = new set<id>();
            Id idStaff;
            List<Staff__c> lstStaff = new List<Staff__c>();
            lstStaff= [SELECT id,name,User__c from Staff__c where User__c =:UserInfo.getUserId() Limit 1];
            if(!lstStaff.isEmpty()){
                idStaff = lstStaff[0].id;
            }
            for(Accrual__c objAccural:mapIdAccural.values()){
                
                Accrual__c objNewAccural = new Accrual__c();
                objNewAccural.Reversed_Date__c = date.today();    
                objNewAccural.Reversed_By__c = idStaff;
                objNewAccural.Status__c= statusAccuralReverse;
                objNewAccural.Job__c = objAccural.Job__c;
                objNewAccural.Reversed_From__c = objAccural.id;
                lstReverseAccural.add(objNewAccural);
                
                setJobIds.add(objAccural.Job__c);
            }
            
            try{
                insert lstReverseAccural;
                map<id,id> mapAccuralReverseAccural = new map<id,id>();
                for(Accrual__c objRevAccural:lstReverseAccural){
                    mapAccuralReverseAccural.put(objRevAccural.Reversed_From__c, objRevAccural.id);
                }
                set<id> allocationids = new set<id>();
                List<Accrual_Forecasting_Junction__c> lstForecastingJunction = new List<Accrual_Forecasting_Junction__c>();
                List<Accrual_Forecasting_Junction__c> lstForecastingJunctiontoInsert = new List<Accrual_Forecasting_Junction__c>();
                lstForeCastingJunction = [Select id, name, 
                                                Accrual__c,
                                                Amount__c,
                                                Forecasting__c,
                                                Forecasting__r.Allocation__c
                                                From 
                                                Accrual_Forecasting_Junction__c
                                                where 
                                                Accrual__c in: mapIdAccural.keyset() and Forecasting__c != null and Amount__c != null];
                                                
                
                List<Forecasting__c> lstForecasting = new List<Forecasting__c>();
                 List<Forecasting__c> lstForecastingnew = new List<Forecasting__c>();
                List<Forecasting__c> lstForecastinginsert = new List<Forecasting__c>();
                Map<id,Forecasting__c> mapJobForecasting = new Map<id,Forecasting__c>();
                Map<id,Forecasting__c> mapJobForecastingnew = new Map<id,Forecasting__c>();
                Map<id,Accrual_Forecasting_Junction__c> mapJobForecastingcurrentMont = new Map<id,Accrual_Forecasting_Junction__c>();
                lstForecasting = [Select id, name,
                                            Allocation__c,
                                            Allocation__r.Job__c
                                            From 
                                            Forecasting__c
                                            where CS_Forecast_Date__c = THIS_MONTH and Allocation__r.Job__c in: setJobIds];
                                            
                for(Forecasting__c objForecasting: lstForecasting){
                    mapJobForecasting.put(objForecasting.Allocation__c, objForecasting);
                }
                
                for(Accrual_Forecasting_Junction__c objForecastJunction: lstForecastingJunction){
                    
                    Accrual_Forecasting_Junction__c objReveralForecastJunction = new Accrual_Forecasting_Junction__c();
                    if(mapJobForecasting.containsKey(objForecastJunction.Forecasting__r.Allocation__c)){
                        System.debug('---Here----');
                        objReveralForecastJunction.Accural_Status__c = statusAccuralReverse;
                        objReveralForecastJunction.Amount__c = -1*objForecastJunction.Amount__c;
                        objReveralForecastJunction.Forecasting__c = mapJobForecasting.get(objForecastJunction.Forecasting__r.Allocation__c).id;
                        objReveralForecastJunction.Accrual__c = mapAccuralReverseAccural.get(objForecastJunction.Accrual__c);
                        lstForecastingJunctiontoInsert.add(objReveralForecastJunction);
                    }else{
                        Forecasting__c objForecasting = new Forecasting__c();
                        objForecasting.Amount__c = 0.00;
                        objForecasting.Allocation__c = objForecastJunction.Forecasting__r.Allocation__c;
                        allocationids.add(objForecastJunction.Forecasting__r.Allocation__c);
                        objForecasting.CS_Forecast_Date__c = system.today();
                        lstForecastinginsert.add(objForecasting);
                        mapJobForecastingcurrentMont.put(objForecastJunction.Forecasting__r.Allocation__c,objReveralForecastJunction);
                        /*objReveralForecastJunction.Accural_Status__c = statusAccuralReverse;
                        objReveralForecastJunction.Amount__c = -1*objForecastJunction.Amount__c;
                        objReveralForecastJunction.Forecasting__c =objForecasting.id;
                        objReveralForecastJunction.Accrual__c = mapAccuralReverseAccural.get(objForecastJunction.Accrual__c);
                        lstForecastingJunctiontoInsert.add(objReveralForecastJunction);*/
                    }
                    
                    
                }
                System.debug('---ForecastJunction----'+lstForecastingJunctiontoInsert);
                insert lstForecastinginsert;
                lstForecastingnew = [Select id, name,
                                            Allocation__c,
                                            Allocation__r.Job__c
                                            From 
                                            Forecasting__c
                                            where CS_Forecast_Date__c = THIS_MONTH and Allocation__c in: allocationids];
                for(Forecasting__c objForecasting: lstForecastingnew){
                    mapJobForecastingnew.put(objForecasting.Allocation__c, objForecasting);
                }
                for(Accrual_Forecasting_Junction__c objForecastJunction: lstForecastingJunction){
                    System.debug('---ForecastJunctionnew----');
                    Accrual_Forecasting_Junction__c objReveralForecastJunction = new Accrual_Forecasting_Junction__c();
                    if(mapJobForecastingcurrentMont.containsKey(objForecastJunction.Forecasting__r.Allocation__c)){
                        objReveralForecastJunction.Accural_Status__c = statusAccuralReverse;
                        objReveralForecastJunction.Amount__c = -1*objForecastJunction.Amount__c;
                        objReveralForecastJunction.Forecasting__c =mapJobForecastingnew.get(objForecastJunction.Forecasting__r.Allocation__c).id;
                        objReveralForecastJunction.Accrual__c = mapAccuralReverseAccural.get(objForecastJunction.Accrual__c);
                        lstForecastingJunctiontoInsert.add(objReveralForecastJunction); 
                        System.debug('---ForecastJunctionnew----'+objReveralForecastJunction);
                    }
                }
                checkRecursiveForecast.run = true;
                insert lstForecastingJunctiontoInsert;
                map<id, Accrual__c> mapIDAccuralReverse = new map<id, Accrual__c>();
                for(Accrual__c objAccural : lstReverseAccural){
                    
                    mapIDAccuralReverse.put(objAccural.id, objAccural);
                }
                
                calculateAccuralStatus(mapIDAccuralReverse);
            }catch(exception e){
                System.debug('---->'+e.getMessage());
            }
        }
        
    }
    
    /**
     *  Method Name: insertAccuralStatus
     *  Description: Method to create Reverse Accural on create
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void insertAccuralStatus(List<Accrual__c> lstAccural){
        
        map<id, Accrual__c> mapIdAccuralToUpdate = new map<id, Accrual__c>();
        for (Accrual__c objAccural: lstAccural){
            mapIdAccuralToUpdate.put(objAccural.id, objAccural);
        }
        calculateAccuralStatus(mapIdAccuralToUpdate);
        
    }
    
    
    /**
     *  Method Name: updateAccuralStatus 
     *  Description: Method to create Reverse Accural on Update
     *  Param:  Trigger.newMap, Trigger.oldMap
     *  Return: None
    */
    public static void updateAccuralStatus(map<id, Accrual__c> mapOldAccural, map<id, Accrual__c> mapNewAccural){
        
        map<id, Accrual__c> mapIdAccuralToUpdate = new map<id, Accrual__c>();
        for (Accrual__c objAccural: mapNewAccural.values()){
            if(mapOldAccural.get(objAccural.id).Status__c != objAccural.Status__c){
                mapIdAccuralToUpdate.put(objAccural.id, objAccural);
            }
        }
        calculateAccuralStatus(mapIdAccuralToUpdate);
        
    }
    
    /**
    *  Method Name: calculateAccuralStatus
    *  Description: Method to update the Invoice Forecasting Junction Status
    *  Param:  map<id, Invoice__c>
    *  Return: None
    */
    public static void calculateAccuralStatus(map < id, Accrual__c > mapIdAccural) {
        
        if (!mapIdAccural.keyset().isEmpty()) {
            
            List<Accrual_Forecasting_Junction__c> lstAccuralForecasting = new List<Accrual_Forecasting_Junction__c>();
            List<Accrual_Forecasting_Junction__c> lstAccuralForecastingToUpdate = new List<Accrual_Forecasting_Junction__c>();
            
            lstAccuralForecasting = [Select id, name, 
                                            Accrual__c, 
                                            Accural_Status__c,
                                            Forecasting__c                                          
                                            From 
                                            Accrual_Forecasting_Junction__c 
                                            where  
                                            Accrual__c in: mapIdAccural.keyset()];
                                            
            for(Accrual_Forecasting_Junction__c objAccuralForecasting: lstAccuralForecasting){
                objAccuralForecasting.Accural_Status__c = mapIdAccural.get(objAccuralForecasting.Accrual__c).Status__c;
                lstAccuralForecastingToUpdate.add(objAccuralForecasting);
            }
            
            try {
                if(!lstAccuralForecastingToUpdate.isEmpty()){
                    update lstAccuralForecastingToUpdate;
                }
            } catch (exception e) {
                System.debug('---->' + e.getMessage());
            }
        }
    }
    
    
    /**
     *  Method Name: updateApprovalProcess 
     *  Description: Method to update Approval Process
     *  Param:  Trigger.newMap, Trigger.oldMap
     *  Return: None
    */
    public static void updateApprovalProcess(map<id, Accrual__c> mapOldAccural, map<id, Accrual__c> mapNewAccural){
        
        set<id> setAccuralIds = new set<id>();
        for (Accrual__c objAccural: mapNewAccural.values()){
            if((mapOldAccural.get(objAccural.id).Status__c != objAccural.Status__c) &&  (mapOldAccural.get(objAccural.id).Status__c == statusAwaitingApproval)  && (objAccural.Status__c == statusRejected)){
                setAccuralIds.add(objAccural.id);
            }
        }
        recallApprovalProcess(setAccuralIds);
        
    }
    /**
     *  Method Name: recallApprovalProcess  
     *  Description: Method to update Forecast Amount on Accural Approval
     *  Param:  set<id>
     *  Return: None
    */
    public static void recallApprovalProcess(set<id> setAccuralIds){
        
        if(!setAccuralIds.isEmpty()){
            
            List<ProcessInstance> lstProcessInstance = new List<ProcessInstance>();
            set<id> setProcessInstanceId = new set<id>();
            lstProcessInstance = [Select id, 
                                        Status, 
                                        TargetObjectId
                                        FROM   
                                        ProcessInstance
                                        WHERE TargetObjectId in :setAccuralIds 
                                        AND Status='Pending'];
                                        
            for(ProcessInstance objProcessInstance: lstProcessInstance){
                setProcessInstanceId.add(objProcessInstance.id);
            }
            
            if(!setProcessInstanceId.isEmpty()){
                List<ProcessInstanceWorkitem > lstProcessInstanceWorkitem = new List<ProcessInstanceWorkitem >();
                lstProcessInstanceWorkitem =  [SELECT Id,
                                                    ProcessInstanceId
                                                    FROM ProcessInstanceWorkitem
                                                    where ProcessInstanceId in :setProcessInstanceId];
                
                List<Approval.ProcessWorkItemRequest> lstApprovalProcessRequest = new List<Approval.ProcessWorkItemRequest>();
                for(ProcessInstanceWorkitem objProcessInstanceWorkitem : lstProcessInstanceWorkitem){
                    Approval.ProcessWorkItemRequest req = new Approval.ProcessWorkItemRequest();
                    req.setWorkItemId(objProcessInstanceWorkitem.id);
                    req.setAction('Removed'); 
                    req.setComments('Status is changed');
                    lstApprovalProcessRequest.add(req);
                }
                
                if(!lstApprovalProcessRequest.isEmpty()){
                    List<Approval.ProcessResult> result =  new List<Approval.ProcessResult>();
                    result = Approval.process(lstApprovalProcessRequest);
                }
            }
        }
    }
        
    /**
     *  Method Name: calculateForecastAmount  
     *  Description: Method to update Forecast Amount on Accural Approval
     *  Param:  set<id>
     *  Return: None
    */
    public static void calculateForecastAmount(set<id> setAccuralIds){
        
        // if(!setAccuralIds.isEmpty()){
            // List<Accrual_Forecasting_Junction__c> lstForeCastingJunction = new List<Accrual_Forecasting_Junction__c>();
            // set<id> setForecastIds = new set<id>();
            // lstForeCastingJunction = [Select id, name, 
                                                // Accrual__c,
                                                // Amount__c,
                                                // Forecasting__c
                                                // From 
                                                // Accrual_Forecasting_Junction__c
                                                // where 
                                                // Accrual__c in: setAccuralIds and Forecasting__c != null and Amount__c != null];
                                    
            // for(Accrual_Forecasting_Junction__c objForecastJunction: lstForeCastingJunction){
                // setForecastIds.add(objForecastJunction.Forecasting__c);
            // }
            // if(!setForecastIds.isEmpty()){
                // Map<id, Forecasting__c> mapIdForecast = new Map<id, Forecasting__c>();
                // Map<id, Forecasting__c> mapIdForecastToUpdate = new Map<id, Forecasting__c>();
                // mapIdForecast =  new Map<id, Forecasting__c>([Select id, name,
                                                                        // Accrued_Amount__c,
                                                                        // Amount__c
                                                                        // From 
                                                                        // Forecasting__c
                                                                        // where 
                                                                        // id in: setForecastIds]);
                
                    
                // for(Accrual_Forecasting_Junction__c objForecastJunction: lstForeCastingJunction){
                    
                    // Forecasting__c objForecast = new Forecasting__c();
                    // decimal accuredAmount;
                    // decimal totalAmount;
                    // if(mapIdForecastToUpdate.containsKey(objForecastJunction.Forecasting__c)){
                        // objForecast = mapIdForecastToUpdate.get(objForecastJunction.Forecasting__c);
                        // if(objForecast.Accrued_Amount__c != null){
                            // accuredAmount = objForecast.Accrued_Amount__c;
                        // }else{
                            // accuredAmount = 0;
                        // }
                        // if(objForecast.Amount__c != null){
                            // totalAmount = objForecast.Amount__c;
                        // }else{
                            // totalAmount = 0;
                        // }
                        // objForecast.Accrued_Amount__c = accuredAmount + objForecastJunction.Amount__c;
                        // objForecast.Amount__c = totalAmount - objForecastJunction.Amount__c;
                        // mapIdForecastToUpdate.put(objForecastJunction.Forecasting__c, objForecast);
                    // }else{
                        // objForecast = mapIdForecast.get(objForecastJunction.Forecasting__c);
                        // if(objForecast.Accrued_Amount__c != null){
                            // accuredAmount = objForecast.Accrued_Amount__c;
                        // }else{
                            // accuredAmount = 0;
                        // }
                        // if(objForecast.Amount__c != null){
                            // totalAmount = objForecast.Amount__c;
                        // }else{
                            // totalAmount = 0;
                        // }
                        // objForecast.Accrued_Amount__c = accuredAmount + objForecastJunction.Amount__c;
                        // objForecast.Amount__c = totalAmount - objForecastJunction.Amount__c;
                        // mapIdForecastToUpdate.put(objForecastJunction.Forecasting__c, objForecast);
                    // }
                    
                // }
                
                // try{
                    // update mapIdForecastToUpdate.values();
                // }catch(exception e){
                    // System.debug('---->'+e.getMessage());
                // }
                                                        
            // }
        // }
    }
     /**
    * class Name:createCustomSetting.
    * functinality : To update the value of account_Id in account address junction based on the invoicing address and company of accrual.
    * author : Nidheesh
    **/
    public Static void createCustomSetting(List<Accrual__c> lstAccural){
       List<Account_Address_Junction__c> lstaccAdd = new List<Account_Address_Junction__c>();
       List<Account_Address_Junction__c> updatelist = new List<Account_Address_Junction__c>();
       AssignAccountId__c assignAcc = new AssignAccountId__c();
       set<id> lstofaccuraIds = new set<id>();
       Map<id,id> mapofaccrutojob = new Map<id,id>();
       Map<id,Opportunity> mapofIdtojob = new Map<id,Opportunity>();
       Map<id,id> mapofjobtocareof = new Map<id,id>();
       set<id> lstofjobIds = new set<id>();
       set<id> lstofaccountids = new set<id>();
       set<id> lstofaddressids = new set<id>();
       List<Care_Of__c> lstoffcareofs = new List<Care_Of__c>();
       List<Accrual__c> lstofaccural = new List<Accrual__c>();
       List<Opportunity> lstofopps = new List<Opportunity>();
       List<Opportunity> updateopps = new List<Opportunity>();
       Map<id,map<id,Account_Address_Junction__c>> lstofmap = new  Map<id,map<id,Account_Address_Junction__c>>();
       if(!lstAccural.isEmpty()){
           for(Accrual__c objacr: lstAccural){
               lstofaccuraIds.add(objacr.id);
               if(objacr.Job__c != null && string.isNotBlank(objacr.Job__c)){
                   mapofaccrutojob.put(objacr.id,objacr.Job__c);
               }
               
           }
       }
       lstofopps = [Select Id,Account_id__c, Invoicing_Care_Of__c from Opportunity where id IN : mapofaccrutojob.values()];
       
       if(!lstofopps.isEmpty()){
           for(Opportunity oppobj : lstofopps){
               mapofIdtojob.put(oppobj.Id,oppobj);
           }
       }
       lstofaccural = [SELECT Id,name,Job__c,Job__r.Invoicing_Address__c,job__r.Invoicing_Care_Of__c,job__r.Invoicing_Company2__c,job__r.Invoicing_Care_Of__r.Client__c from Accrual__c where id IN:lstofaccuraIds];
       
       if(!lstofaccural.isEmpty()){
           for(Accrual__c objacral : lstofaccural){
               if(objacral.Job__r.Invoicing_Address__c != null && string.isNotBlank(objacral.Job__r.Invoicing_Address__c)){
                   lstofaddressids.add(objacral.Job__r.Invoicing_Address__c);
               }else{
                   
               }
               if(objacral.job__r.Invoicing_Company2__c != null && string.isNotBlank(objacral.Job__r.Invoicing_Company2__c)){
                   lstofaccountids.add(objacral.job__r.Invoicing_Company2__c);
               }else if(objacral.job__r.Invoicing_Care_Of__c != null && string.isNotBlank(objacral.Job__r.Invoicing_Care_Of__c)){
                   
                       //lstofaccountids.add(objacral.job__r.Invoicing_Care_Of__r.Client__c);
                       mapofjobtocareof.put(objacral.job__c,objacral.job__r.Invoicing_Care_Of__c);
                   
               }
           }
       }
       lstoffcareofs = [SELECT id,Account_ID__c from Care_Of__c where id IN:mapofjobtocareof.values() ];
       Map<Id,Care_Of__c> maptocare =new Map<Id,Care_Of__c>();
       if(!lstoffcareofs.isEmpty()){
           for(Care_Of__c careobj : lstoffcareofs){
               maptocare.put(careobj.Id,careobj);
           }
       }
       if(!lstofopps.isEmpty()){
           for(Opportunity oppobj : lstofopps){
                mapofIdtojob.put(oppobj.Id,oppobj);
                if(oppobj.Invoicing_Care_Of__c != null && string.isNotBlank(oppobj.Invoicing_Care_Of__c)){
                    if(mapofjobtocareof.containsKey(oppobj.Id)){
                        if(maptocare.containsKey(mapofjobtocareof.get(oppobj.Id))){
                            if(maptocare.get(mapofjobtocareof.get(oppobj.Id)).Account_ID__c != null && string.isNotBlank(maptocare.get(mapofjobtocareof.get(oppobj.Id)).Account_ID__c)){
                                oppobj.Account_id__c = maptocare.get(mapofjobtocareof.get(oppobj.Id)).Account_ID__c;
                                updateopps.add(oppobj);
                            }
                        }
                    }
                }
           }
       }
       lstaccAdd = [SELECT id,name,Account__c,Account_ID__c,Address__c from Account_Address_Junction__c where Address__c IN:lstofaddressids AND Account__c IN:lstofaccountids ];
        if(!lstaccAdd.isEmpty()){
            for(Account_Address_Junction__c objaccadd : lstaccAdd){
                if(objaccadd.Account__c != null && string.isNotBlank(objaccadd.Account__c)){
                    if(lstofmap.containsKey(objaccadd.Account__c)){
                        if(objaccadd.Address__c != null && string.isNotBlank(objaccadd.Address__c)){
                            if(lstofmap.get(objaccadd.Account__c).containsKey(objaccadd.Address__c)){
                                
                            }else{
                                lstofmap.get(objaccadd.Account__c).put(objaccadd.Address__c,objaccadd);
                            }
                        }
                    }else{
                        map<id,Account_Address_Junction__c> addTorecord = new map<id,Account_Address_Junction__c>();
                        if(objaccadd.Address__c != null && string.isNotBlank(objaccadd.Address__c)){
                            addTorecord.put(objaccadd.Address__c,objaccadd);
                            lstofmap.put(objaccadd.Account__c,addTorecord);
                        }
                    }
                }
            }
        }
        AssignAccountId__c assignAccRec = [SELECT id,AccountNumber__c,Account_Pattern__c from AssignAccountId__c limit 1];
        Decimal account_id_number = assignAccRec.AccountNumber__c;
         //AssignAccountId__c assignAccRec ;
        if(!lstofaccural.isEmpty()){
            for(Accrual__c objac : lstofaccural){
                String Pattern;
                String accCode;
                string ac;
                if(objac.Job__r.Invoicing_Company2__c != null && string.isNotBlank(objac.Job__r.Invoicing_Company2__c) && lstofmap.containsKey(objac.Job__r.Invoicing_Company2__c)){
                    if(objac.Job__r.Invoicing_Address__c != null && string.isNotBlank(objac.Job__r.Invoicing_Address__c)){
                        if(lstofmap.get(objac.Job__r.Invoicing_Company2__c).containsKey(objac.Job__r.Invoicing_Address__c)){
                            Account_Address_Junction__c accAddRec = lstofmap.get(objac.Job__r.Invoicing_Company2__c).get(objac.Job__r.Invoicing_Address__c);
                            if(string.isBlank(accAddRec.Account_ID__c)){
                                account_id_number = account_id_number +1;
                                Pattern = assignAccRec.Account_Pattern__c;
                                assignAccRec.AccountNumber__c = assignAccRec.AccountNumber__c;
                                accCode = string.valueof(integer.valueof(account_id_number));
                                Pattern = Pattern.replaceall('X','0');
                                Pattern = Pattern.substring(0,Pattern.length()-accCode.Length())+accCode;
                                ac = 'ACC - ';
                                accAddRec.Account_ID__c = ac+Pattern;
                                if(mapofaccrutojob.containsKey(objac.Id)){
                                    if(mapofaccrutojob.get(objac.Id) != null){
                                        if(mapofIdtojob.containsKey(mapofaccrutojob.get(objac.Id))){
                                            mapofIdtojob.get(mapofaccrutojob.get(objac.Id)).Account_id__c = ac+Pattern;
                                            updateopps.add(mapofIdtojob.get(mapofaccrutojob.get(objac.Id)));
                                        }
                                    }
                                }
                                updatelist.add(accAddRec);
                            }
                            else if(string.isNotBlank(accAddRec.Account_ID__c)){
                                if(mapofaccrutojob.containsKey(objac.Id)){
                                    if(mapofaccrutojob.get(objac.Id) != null){
                                        if(mapofIdtojob.containsKey(mapofaccrutojob.get(objac.Id))){
                                            mapofIdtojob.get(mapofaccrutojob.get(objac.Id)).Account_id__c = accAddRec.Account_ID__c;
                                            updateopps.add(mapofIdtojob.get(mapofaccrutojob.get(objac.Id)));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        assignAcc.id= assignAccRec.id;
        assignAcc.AccountNumber__c = account_id_number;
        update assignAcc;
        if(!updatelist.isEmpty()){
            
                upsert updatelist;
            
        }
        if(!updateopps.isEmpty()){
            
                upsert updateopps;
            
        }
        
        
   
    
    }
}