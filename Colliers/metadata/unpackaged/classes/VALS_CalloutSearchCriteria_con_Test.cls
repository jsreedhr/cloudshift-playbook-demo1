@isTest
private class VALS_CalloutSearchCriteria_con_Test {
static Property__c propertyObj;
static Search_Criteria__c searCrit;

  static testmethod void createData()
  {
    propertyObj=TestObjectHelper.createProperty();
    propertyObj.Street_No__c ='Park Lane';
    propertyObj.Post_Code__c ='W1K 3DD';
    propertyObj.Country__c = 'United Kingdom';
    propertyObj.Geolocation__Latitude__s=51.51;
    propertyObj.Geolocation__Longitude__s=-0.15;
    propertyObj.Override_Experian_validation__c=true;
    insert propertyObj;
    System.assert(propertyObj.id!=NULL);
    
    searCrit = new Search_Criteria__c();
    searCrit.Name='Test SearchCriteria';
    searCrit.property__C=propertyObj.id;
    insert searCrit; 
    
    ShareDo_Credentials__c Customsetting = new ShareDo_Credentials__c();
    Customsetting.Name='Sharedo Search Criteria';
    Customsetting.API_Key__c='e53aa9e3-2ca2-43db-89bb-f4f985c4f5e1';
    Customsetting.shareDoUrl__c='test';
    insert Customsetting;   
  }
  
  
  static testmethod void doCalloutTest()
  {
    createData();

    PageReference pageRef = Page.VALS_ShareDoCalloutSearchCriteria;
    pageRef.getParameters().put('SearchCriteriaId',searCrit.id);
    Test.setCurrentPage(pageRef);

    VALS_ShareDoCalloutSearchCriteria_con cont= new VALS_ShareDoCalloutSearchCriteria_con ();
    cont.doCallout();
  }



}