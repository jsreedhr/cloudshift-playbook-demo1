@isTest 
public class TestBatchUpdateFeeIncomeStagingTable 
{
    static testMethod void UpdateFeeIncomeStagingTable(){ 
        
        Job_Amount__c jm = new Job_Amount__c();
        jm.name= 'jobAmount';
        jm.Amount__c = 20000;
        insert jm;
        
        AssignAccountId__c objAssignAccount = new AssignAccountId__c();
        objAssignAccount.Name = 'test';
        objAssignAccount.AccountNumber__c = 123;
        objAssignAccount.Account_Pattern__c = '123';
        insert objAssignAccount;
        
        Account acc1 = new Account();
        acc1.name = 'testaccount1';
        acc1.Company_Classification__c = 'Developer;Lender;Occupier;Owner;Service Provider';
        insert acc1;
        
        Account acc = new Account();
        acc.name='test acc';
        insert acc;
        
        Account acc2 = new Account();
        acc2.name = 'testaccount2';
        acc2.parentid = acc1.id;
        acc2.Company_Classification__c ='Developer';
        acc2.Geographical_Client__c ='Local';
        insert acc2;
        
        Account acc3 = new Account();
        acc3.name = 'test acc3';
        acc3.parentid=acc.id;
        acc3.Company_Classification__c ='Lender';
        acc2.Geographical_Client__c ='Local';
        insert acc3;
        
        Opportunity opp1= new Opportunity();
        opp1.Name= 'test opp1';
        opp1.Accountid = acc2.id;
        opp1.StageName ='Instructed';
        opp1.CloseDate = system.today().addDays(8);
        insert opp1;
        
        Opportunity opp2= new Opportunity();
        opp2.Name= 'test opp2';
        opp2.Accountid = acc3.id;
        opp2.StageName ='Instructed';
        opp2.CloseDate = system.today().addDays(7);
        insert opp2;
        
        Opportunity opp3= new Opportunity();
        opp3.Name= 'test opp2';
        opp3.Accountid = acc2.id;
        opp3.StageName ='Instructed';
        opp3.CloseDate = system.today().addDays(9);
        
        insert opp3;
        
        Invoice__c inv1 = new Invoice__c();
        inv1.Opportunity__c = opp1.id;
        insert inv1;
        
        Accrual__c acrl1 = new Accrual__c();
        acrl1.Job__c=opp2.id;
        insert acrl1;
        
        Allocation__c alloc1 = new Allocation__c();
        alloc1.Job__c = opp3.id;
        insert alloc1;
        
        Fee_Income_Staging_Table__c fist = new Fee_Income_Staging_Table__c();
        fist.Accrual__c=acrl1.id;
        fist.Invoice__c = inv1.id;
        fist.Allocation__c =alloc1.id;
        insert fist;
    
    
        
        
        
        
        Test.startTest();

            BatchUpdateFeeIncomeStagingTable obj = new BatchUpdateFeeIncomeStagingTable();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
}