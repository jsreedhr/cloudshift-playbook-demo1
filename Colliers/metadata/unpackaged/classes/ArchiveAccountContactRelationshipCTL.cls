public class ArchiveAccountContactRelationshipCTL{
    public id AcctConRelID;
    public ArchiveAccountContactRelationshipCTL(ApexPages.StandardController controller){
        AcctConRelID = ApexPages.currentPage().getParameters().get('id');
    }
    public pagereference archiveRelationship(){
        AccountContactRelation objrel = [select id,accountId,StartDate,EndDate,Department__c,Title__c,IsActive,ContactId,IsDirect,Roles,External_ID_CM_UK__c,
        Floor__c,Position__c,Receive_Email__c,Receive_Hard_Copy__c,Suite__c from AccountContactRelation where id=:AcctConRelID];
        if(objrel != null){
            Previous_Company__c objPrevCompany = new Previous_Company__c();
            if(objrel.accountId != null)
                objPrevCompany.Company__c = objrel.accountId;
            if(objrel.ContactId != null)        
                objPrevCompany.Contact__c = objrel.ContactId;
            if(objrel.Position__c != null && objrel.Position__c != '')
                objPrevCompany.Position__c = objrel.Position__c;
            if(objrel.External_ID_CM_UK__c != null && objrel.External_ID_CM_UK__c != '')
                objPrevCompany.External_ID_CM_UK__c = objrel.External_ID_CM_UK__c;
            if(objrel.Roles != null && objrel.Roles != '')
                objPrevCompany.Roles__c = objrel.Roles;
            if(objrel.Floor__c != null && objrel.Floor__c != '')
                objPrevCompany.Floor__c = objrel.Floor__c;
            if(objrel.Suite__c != null && objrel.Suite__c != '')
                objPrevCompany.Suite__c = objrel.Suite__c ;
            if(objrel.Department__c != null && objrel.Department__c != '')
                objPrevCompany.Department__c = objrel.Department__c;
            if(objrel.Title__c != null && objrel.Title__c != '')
                objPrevCompany.Title__c = objrel.Title__c;
            objPrevCompany.IsActive__c = objrel.IsActive;
            objPrevCompany.IsDirect__c = objrel.IsDirect;
            objPrevCompany.Receive_Email__c = objrel.Receive_Email__c;
            objPrevCompany.Receive_Hard_Copy__c = objrel.Receive_Hard_Copy__c;
            objPrevCompany.Start_Date__c = objrel.StartDate; 
            objPrevCompany.End_Date__c = objrel.EndDate;
            Database.SaveResult dbsr = Database.insert(objPrevCompany,true);
            if(dbsr.isSuccess()){                
                delete objrel;
                return new PageReference('/'+objPrevCompany.Contact__c);
            }
        }
        return null;
    }
}