global class autoCompleteController 
{
    private static Map<String, Schema.SObjectType> gd =Schema.getGlobalDescribe();
    @RemoteAction
    global static string findSObjects(string obj, string qry, string addFields, string profilename,string filtermanager,string allocation) 
    {
        /* More than one field can be passed in the addFields parameter
           Split it into an array for later use */
        List<String> fieldList=new List<String>();
        if (addFields != '')  
        fieldList = addFields.split(',');
        
       
        Schema.SObjectType sot = gd.get(obj);
        if (sot == null) 
        {
            return null;
        }
        
        /* Creating the filter text */
        String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
        
        /* Begin building the dynamic soql query */
        String soql = 'SELECT Name';
        if(allocation != '' && allocation == 'True'){
            soql += ' ,(select Department__c ,Office__c ,Work_Type__c  from Coding_Structures__r WHERE Department__c != null AND Office__c != null AND Work_Type__c!= null )';
        }
        
        /* If any additional field was passed, adding it to the soql */
        if (fieldList.size()>0) 
        {
            for (String s : fieldList) 
            {
                soql += ', ' + s;
            }
        }
        
        /* Adding the object and filter by name to the soql */
        
        soql += ' from ' + obj + ' where name' + filter;
        
        if(profilename!='')
        {
            //profile name and the System Administrator are allowed
            soql += ' and Profile.Name like \'%' + String.escapeSingleQuotes(profilename) + '%\'';
            system.debug('Profile:'+profilename+' and SOQL:'+soql);
        }
        if(filtermanager != '' && filtermanager == 'Active'){
            soql += ' AND Active__c = TRUE';
        }
        else if(filtermanager != '' && filtermanager == 'Companystatus'){
            string active = 'Active';
            soql += ' AND Company_Status__c =: active';
        }
        
        /* for allocation surveyor checking for proper coding structue*/
        
        
        
        /* Adding the filter for additional fields to the soql */
        if (fieldList != null) 
        {
            for (String s : fieldList) 
            {
                soql += ' or ' + s + filter;
            }
        }
        
        soql += ' order by Name limit 10';
        
        system.debug('Qry: '+soql);
        
             List<Staff__c> stafflist = new List<Staff__c>();
        
            List<sObject> L = new List<sObject>();
        
        
        try 
        {
            if(allocation != '' && allocation == 'True'){
            
                stafflist = Database.query(soql);
            }else{
                L = Database.query(soql);
            
            }
            
            /*if(allocation != '' && allocation == 'True'){
                stafflist = Database.query(soql);
                for(Integer j = 0; j < stafflist.size(); j++){
                   if(stafflist[j].Coding_Structures.isEmpty()){
                        stafflist.remove(j);
                        
                   }
                }
            }*/
            if(allocation != '' && allocation == 'True'){
                L.clear();
                for(Staff__c objstaff : stafflist){
                    
                    if(!objstaff.Coding_Structures__r.isEmpty()){
                        L.add(objstaff);
                    }
                }
            }
            system.debug('Qryvalue: '+L);
        }
        catch (QueryException e) 
        {
            system.debug('Query Exception:'+e.getMessage());
            return null;
        }
        
            String jsonbody = JSON.serialize(L).escapeXML();  
            return jsonbody; //L;
        
        
   }
}