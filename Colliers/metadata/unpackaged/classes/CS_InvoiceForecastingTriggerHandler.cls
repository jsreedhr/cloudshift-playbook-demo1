/**
 *  Class Name: CS_InvoiceForecastingTriggerHandler
 *  Description: This is a Trigger Handler Class for trigger on Invoice_Allocation_Junction__c
 *  Company: dQuotient
 *  CreatedDate:27/01/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Anand              27/01/2017            Orginal Version
 *
 */
public class CS_InvoiceForecastingTriggerHandler {
 
    /**
     *  Method Name: updateInvoiceStatus  
     *  Description: Method to update Invoice Status
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void updateInvoiceStatus(List<Invoice_Allocation_Junction__c> lstInvoiceForecasting){
        
        set<id> setInvoiceIds = new set<id>();
        for(Invoice_Allocation_Junction__c objInvoice: lstInvoiceForecasting){
            setInvoiceIds.add(objInvoice.Invoice__c);
        }
        map<id, Invoice__c> mapIdInvoice = new map<id, Invoice__c>();
        if(!setInvoiceIds.isEmpty()){
            mapIdInvoice = new map<id, Invoice__c>([Select id, Status__c From Invoice__c where id in: setInvoiceIds]);
        }
        
        for(Invoice_Allocation_Junction__c objInvoice: lstInvoiceForecasting){
            
            if(mapIdInvoice.containsKey(objInvoice.Invoice__c)){
                if(mapIdInvoice.get(objInvoice.Invoice__c).Status__c != null){
                    objInvoice.Invoice_Status_Rollup__c = mapIdInvoice.get(objInvoice.Invoice__c).Status__c;
                }
            }
        }
    }
 
 
}