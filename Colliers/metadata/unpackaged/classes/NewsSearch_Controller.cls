public class NewsSearch_Controller{
    public Account acc{get;set;}
    public String newsBody{get;set;}
    public string searchValue{get;set;}
    public string strconame = '';
    public List<newsArticles> lstNewsArticles{get;set;}
    public list<string> lstcursorvalue = new list<string>();
    public string concatcursor;
    public integer i = 0;
    public string cursorvalue;
    public String strsearchnews;
    public boolean prevbutton{get;set;}
    public boolean nextbutton{get;set;}
    
    public NewsSearch_Controller(ApexPages.StandardController controller){
    lstNewsArticles = new list<newsArticles>();
    acc = [SELECT Id,Name,News_Feed__c FROM Account WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    searchValue = acc.Name;
    lstcursorvalue.add('*');
    concatcursor = '*';
    cursorvalue = lstcursorvalue[i];
    if(acc.News_Feed__c != null && acc.News_Feed__c != ''){
        strconame = acc.News_Feed__c.replaceAll('\\s','+');
        strconame = '%22'+strconame+'%22';
    }
    searchNews();
    }
    
    public void searchNews(){
        lstNewsArticles = new list<newsArticles>();
        HttpResponse response = null;
        HttpRequest req = new HttpRequest();
        if(strsearchnews != searchValue.replaceAll('\\s','+')){
            i = 0;
            lstcursorvalue = new list<string>();
            lstcursorvalue.add('*');
            concatcursor = '*';
            cursorvalue = lstcursorvalue[i];
        }
        strsearchnews = searchValue.replaceAll('\\s','+');
        req.setEndpoint('https://api.newsapi.aylien.com/api/v1/stories?text=%22'+strsearchnews+'%22'+strconame+'&published_at.start=NOW-365DAYS&published_at.end=NOW&categories.id%5B%5D=IAB21&categories.taxonomy=iab-qag&language=en&source.locations.country%5B%5D=GB&cursor='+cursorvalue+'&per_page='+System.Label.NewsPerPage);
        req.setMethod('GET');
        req.setheader('X-AYLIEN-NewsAPI-Application-ID',System.Label.AylienNewsAPIID);
        req.setheader('X-AYLIEN-NewsAPI-Application-Key',System.Label.AylienNewsAPIKey);
        Http http = new Http();
        response = http.send(req);        
        newsBody = response.getBody();          
        JSONParser parser = JSON.createParser(response.getBody());
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                while (parser.nextToken() != null) {
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        newsArticles newArt = (newsArticles)parser.readValueAs(newsArticles.class);
                        if(newArt.published_at != null && newArt.published_at != '')
                            newArt.published_at = newArt.published_at.substring(0,10);
                        if(newArt.title != null && newArt.title != '' && newArt.title.length()>100)
                            newArt.title = newArt.title.substring(0,100)+'...';
                        lstNewsArticles.add(newArt);
                        String s = JSON.serialize(newArt);
                    }
                    if(parser.getText() == 'next_page_cursor'){
                        parser.nextToken();
                        if(!concatcursor.contains(parser.getText())){
                            lstcursorvalue.add(parser.getText());
                            concatcursor += ';'+parser.getText();
                        }
                    }
                }
            }
        }
        if(lstNewsArticles.size()==0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No more news available');
            ApexPages.addMessage(myMsg);    
        }
        if(cursorvalue == '*'){
            prevbutton = false;
        }
        else{
            prevbutton = true;
        }
        if(lstNewsArticles.size() < Integer.ValueOf(System.Label.NewsPerPage)){
            nextbutton = false;
        }
        else{
        nextbutton = true;
        } 
    }
    
    public void nextNews(){
    i++;
    cursorvalue = lstcursorvalue[i];
    searchNews();
    }
    
    public void prevNews(){
    i--;
    cursorvalue = lstcursorvalue[i];
    searchNews();
    }
    
    public class newsArticles{
        public string title{get;set;}    
        public Links_X links{get;set;}    
        public Source source{get;set;} 
        public string published_at{get;set;}        
    }
    
    public class Links_X {
        public String permalink {get;set;}
        public Links_X(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'permalink') {
                            permalink = parser.getText();
                        }
                    }
                }
            }
        }
    }
    
    public class Source {
        public String name {get;set;}
        public Source(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'name') {
                            name = parser.getText();
                        }
                    }
                }
            }
        }
    }
}