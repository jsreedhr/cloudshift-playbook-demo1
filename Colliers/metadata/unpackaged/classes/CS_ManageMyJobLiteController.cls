/*  Class Name: CS_ManageMyJobLiteController 
 *  Description: This class is a controller for ManagemyjobLite_Page
 *  Company: dQuotient
 *  CreatedDate: 02/03/2017 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nidheesh               02/03/2017                  Orginal Version
 *
 */
public with sharing class CS_ManageMyJobLiteController{ 
  
    public opportunity jobObject{get;set;} 
    public string recordId{get;set;}
    public string authId{get;set;}  
    public Boolean isDisabled{get;set;}
    public boolean isManageMyJob{get;set;}
    public boolean authorized{get;set;}
    public boolean isCompany{get;set;}
    public boolean isProperty{get;set;}
    public boolean isAllocation{get;set;}
    public boolean isCost{get;set;}
    public boolean isAccural{get;set;}
    public boolean isInvoice{get;set;}
    public boolean isCreditNote{get;set;}
    public boolean isJournal{get;set;}
    public boolean isEditMode{get;set;}
     Public boolean guestUser {get;set;}
    
    public CS_ManageMyJobLiteController(){ 
        if(ApexPages.currentPage().getParameters().containsKey('edit')){
            isEditMode = true;
        } 
        else{
            isEditMode=false;    
        }
          if(UserInfo.getUserType()!='Standard')
         guestUser=true;
         else
         guestUser=false;
        isDisabled = true;
        isCompany=true;
        isProperty=false;
        isAllocation=false;
        isCost=false;
        isAccural=false;
        isInvoice=false;
        isCreditNote=false;
        isJournal=false;
        List<Opportunity> lstOpp = new List<Opportunity>();
        jobObject=new opportunity();
       
        
            if(ApexPages.currentPage().getParameters().containsKey('id')){
                // isDisabled = false;
                // isDisabled = false;
                recordId = ApexPages.currentPage().getParameters().get('id');
                    if(!String.isBlank(recordId)){
                        recordId = String.escapeSingleQuotes(recordId);
                            if(recordId InstanceOf Id){
                                Id id1 = UserInfo.getProfileId();
                                System.debug('******************Profile ID' +id1);
                                lstOpp=[SELECT Manager__c,
                                                    isClosed, 
                                                    Job_Number__c,
                                                    Name,
                                                    Department__c,
                                                    Work_Type__c,
                                                    Office__c,
                                                    Account.Name,
                                                    AccountId,
                                                    CloseDate,
                                                    Amount,
                                                    Invoicing_Company2__c,
                                                    Invoicing_Address__c,
                                                    Invoicing_Care_Of__c,
                                                    Invoice_Contact__c,
                                                    InstructingCompanyAddress__c,
                                                    Instructing_Contact__c,
                                                    Instructing_Company_Role__c,
                                                    Source_Of_Instruction__c,
                                                    Date_Instructed__c,
                                                    Coding_Structure__c,
                                                    Office_Referrer__c,
                                                    Colliers_Referrer__c,
                                                    Client_Referral__c,
                                                    Manager__r.Name 
                                                    FROM Opportunity 
                                                    WHERE ID =:recordId Limit 1];

                                if(!lstOpp.isEmpty()){
                                    jobObject = lstOpp[0];
                                    
                                    if( !String.isBLANK(jobObject.Name) &&  jobObject.Manager__c != null &&
                                        !string.isBLANK(jobObject.Department__c) && !string.isBLANK(jobObject.Work_Type__c) &&
                                        jobObject.Coding_Structure__c != null && !string.isBLANK(jobObject.Office__c)){
                                                
                                            isDisabled = false;
                                            // if(jobObject.Source_Of_Instruction__c != null && (jobObject.Source_Of_Instruction__c.equalsignoreCase('Referral from Colliers overseas (inc. Belfast)')) && (string.isBlank(jobObject.Office_Referrer__c) || (jobObject.Office_Referrer__c == null && jobObject.Office_Referrer__c.equalsignoreCase('--None--')))){
                                            //     isDisabled = true;
                                            // }
                                            
                                            // if(jobObject.Source_Of_Instruction__c != null && (jobObject.Source_Of_Instruction__c.equalsignoreCase('Referral from Colliers UK')) && (jobObject.Colliers_Referrer__c == null )) {
                                            //     isDisabled = true;
                                            // }
                                            
                                            // if(jobObject.Source_Of_Instruction__c != null && (jobObject.Source_Of_Instruction__c.equalsignoreCase('Client - referred by another client') ) && (jobObject.Client_Referral__c == null)) {
                                            //     isDisabled = true;
                                            // }
                                            
                                        }
                                    
                                }
                                // if(string.isBLANK(jobObject.AccountId) ){
                                    // isDisabled = true;    
                                    
                                // }else if(!string.isBLANK(jobObject.AccountId) ){
                                    // if(!string.isBLANK(jobObject.Invoicing_Company2__c) && string.isBLANK(jobObject.Invoicing_Care_Of__c)){
                                        // isDisabled = false;     
                                    // }else if(string.isBLANK(jobObject.Invoicing_Company2__c) && !string.isBLANK(jobObject.Invoicing_Care_Of__c)){
                                        // isDisabled = false;      
                                    // }else{
                                        // isDisabled = true;      
                                    // }
                                       
                                // }else{
                                    // isDisabled = false;    
                                // }                                      
                            }else{
                               // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.InvalidParamterError));
                            }
                    }else{
                     //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.InvalidParamterError));
                    }
            }else{
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.InvalidParamterError));
            }
        }
        
        
    
    public pageReference redirect(){
        if(UserInfo.getUserType()!='Standard')
        {
         List<UserLogin__c> lstofuserlogin = new List<UserLogin__c>();
         if(ApexPages.currentPage().getParameters().containsKey('auth')){
            authId = ApexPages.currentPage().getParameters().get('auth');
            if(authId!=null && !string.isBlank(authId)){
                lstofuserlogin = [select id,Name,Auth_Code__c from UserLogin__c where Auth_Code__c = : authId];
                if(!lstofuserlogin.isEmpty()){
                    authorized = true;
                }else if(lstofuserlogin.isEmpty()){
                    
                    authorized = false;
                          PageReference OpporunityPageredirect = new PageReference('https://preprod-colliers-preprod.cs80.force.com/colliers' );
        OpporunityPageredirect.setRedirect(true);
        return OpporunityPageredirect;
                }
            }else{
                authorized = false;
                      PageReference OpporunityPageredirect = new PageReference('https://preprod-colliers-preprod.cs80.force.com/colliers' );
        OpporunityPageredirect.setRedirect(true);
        return OpporunityPageredirect;
            }
        }else{
            authorized = false;
               PageReference OpporunityPageredirect = new PageReference('https://preprod-colliers-preprod.cs80.force.com/colliers' );
        OpporunityPageredirect.setRedirect(true);
        return OpporunityPageredirect;
        }
        }
  return null;
    }
    public PageReference cancel(){
       PageReference OpporunityPageCancel = new PageReference('/'+recordId);
       OpporunityPageCancel.setRedirect(true);
       return OpporunityPageCancel ; 
    }
    
    // public void renderMainCost(){
        // isCost = true;
    // }
    public void renderMainCompany(){
        isCompany = true;
    }
    public void renderMainProperty(){
        //CS_EditProproprtyJunctionControllerJM objCS_EditProproprtyJunctionControllerJM = new CS_EditProproprtyJunctionControllerJM(jobObject);
        isProperty = true;
    }
    public void renderMainAllocation(){
        isAllocation = true;
    }
    
    public void renderMainCost(){
        isCost = true;
    }
    
    public void renderMainAccural(){
        isAccural = true;
    }
    public void renderMainInvoice(){
        isInvoice = true;
    }
    
    public void renderMainCreditNote(){
        isCreditNote = true;
    }
    public void renderMainJournal(){
        isJournal = true;
    }
}