@isTest(SeeAllData=false)
public class TestCS_AccuralsController {
    static Account a;
    static Account b;
    static Contact c;
    static opportunity opp;
    Static Opportunity oppr;
    Static Opportunity oppobj;
    static Invoice__c inv;
    static Invoice__c inv1;
    static Allocation__c alloc;
    static Forecasting__c frcst;
    static List<Forecasting__c> lstFrcst;
    static Staff__c stf;
    static User usr;
    static Coding_Structure__c codestructObj;
    static Staff__c stf1;
    static Accrual__c acc;
    static Accrual__c acc1;
    static Accrual_Forecasting_Junction__c accFrcst;
    static List<Accrual_Forecasting_Junction__c> accFrcstLst;
   
   // @testsetup
    static testmethod void setupData(){
        //create account
        a = TestObjectHelper.createAccount();
        a.Company_Status__c = 'Active';
        insert a ;
        b = TestObjectHelper.createAccount();
        b.Company_Status__c = 'Archived';
        insert b ;
        Schema.DescribeFieldResult fieldResult = Coding_Structure__c.Department__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        Schema.DescribeFieldResult fieldResultofffice = Coding_Structure__c.Office__c.getDescribe();
        List<Schema.PicklistEntry> pleoffice = fieldResultofffice.getPicklistValues();
        stf = TestObjectHelper.createStaff();
        stf.Active__c = true;
        stf.HR_No__c = 'ABCD';
        insert stf;
        Work_Type__c workObj = new Work_Type__c();
        workObj.Name = 'worktype';
        workObj.Active__c = true;
        insert workObj;
        codestructObj = new Coding_Structure__c();
        if(!ple.isEmpty()){
            codestructObj.Department__c = ple[0].getValue();    
        }
        if(!pleoffice.isEmpty()){
            codestructObj.Office__c = pleoffice[0].getValue();    
        }
        //
        //codestructObj.Department__c = 'DepartMent';
        codestructObj.Work_Type__c = workObj.Id;
        codestructObj.Staff__c = stf.Id;
        codestructObj.Status__c = 'Active';
        insert codestructObj;
        //create contact
        c = TestObjectHelper.createContact(a);
        c.Email ='test1@test1.com';
        insert c;
        
        //Create Opportunity Record
        oppr = TestObjectHelper.createOpportunity(a);
        oppr.Coding_Structure__c = codestructObj.Id;
        oppr.Invoicing_Company2__c = a.Id;
        Insert oppr;
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
        
        opp = [Select id from opportunity Limit 1];
        // create the opportubnity without active records
        oppobj = TestObjectHelper.createOpportunity(a);
        oppobj.Invoicing_Company2__c = b.Id;
        Insert oppobj;
        //create realted Opportunity
        List <Contact> cons = TestObjectHelper.createMultipleContact(a, 10);
        integer loopNo = 1;
        for(Contact objContact: cons){
            objContact.Email = 'test'+loopNo+'@test.com';
        }
        insert cons;
        
        //create Staff
        
        
        //create a custom setting Job Amount
        Job_Amount__c jm = new Job_Amount__c();
        jm.name= 'jobAmount';
        jm.Amount__c = 20000;
        insert jm;
        
        //create Allocation
        alloc = TestObjectHelper.createAllocation(oppr,stf);
        insert alloc;
        
        //create Forcasting
        frcst = TestObjectHelper.createForecast(alloc);
        frcst.Amount__c = 5000;
        frcst.CS_Forecast_Date__c = system.today();
        insert frcst;  
        
        //create List Forecastings
        lstFrcst = TestObjectHelper.createMultipleForecasting(alloc,10);
        insert lstFrcst;
        
        Date firstDayOfMonth = System.today().toStartOfMonth();
        Date lastDayOfMonth = firstDayOfMonth.addDays(Date.daysInMonth(firstDayOfMonth.year(), firstDayOfMonth.month()) - 1);
        
        AssignAccountId__c objAssignAccount = new AssignAccountId__c();
        objAssignAccount.Name = 'test';
        objAssignAccount.AccountNumber__c = 123;
        objAssignAccount.Account_Pattern__c = '123';
        insert objAssignAccount;
        //create Accural
        acc =TestObjectHelper.createAccural(frcst);
        acc.Amount__c = 7000;
        acc.Month_To_Accrue__c = lastDayOfMonth.addDays(1);
        acc.Reason__c = 'Test Reason';
        acc.Job__c = oppr.id;
        acc.Is_Engaged_with_Client__c = true;
        acc.IsPrice__c= true;
        acc.IsDeliverySer__c= true;
        insert acc;
        
        //create Accrual_Forecasting_Junction__c object
        accFrcst = TestObjectHelper.createAccrualForecastJunction(acc,frcst);
        accFrcst.Amount__c = frcst.Amount__c;
        accFrcst.Forecasting__c = frcst.id;
        accFrcst.Accrual__c = acc.id;
        insert accFrcst;
        
        //create Accrual_Forecasting_Junction__c object
        accFrcstLst =TestObjectHelper.createMultipleAccFrcstJunction(acc,frcst,10);
        insert accFrcstLst;
        
        //usr = [SELECT id,name,email from User where email='jyoti.h@dquotient.com'];
        
        //create Staff
        stf1 = TestObjectHelper.createStaff();
        stf1.Name='Jyoti Hiremath';
        stf1.Email__c= 'jyoti.hiremath@dquotient.com';
        stf1.User__c =UserInfo.getUserId(); 
        stf1.HR_No__c = 'ABCD2';
        insert stf1;
        
        acc1 = new Accrual__c();
       
        acc1= (Accrual__c)SmartFactory.createSObject('Accrual__c',false);
        acc1.Status__c = 'Awaiting Approval';
        acc1.Amount__c = 7000;
        acc1.Month_To_Accrue__c = system.today().addMonths(1).toStartOfMonth();
        acc1.Reason__c = 'Test Reason';
        acc1.Is_Engaged_with_Client__c = true;
        acc1.IsPrice__c= true;
        acc1.Job__c = oppr.id;
        acc1.IsDeliverySer__c= true;
        insert acc1;
        
        acc1.Reversed__c = true;
        acc1.Reversed_By__c = stf1.id;
        update acc1;
 
    }
    
    static testmethod void TestCS_AccuralsController(){
        setupData();
        // opp = [Select id from opportunity Limit 1];
        
        Test.startTest();
        PageReference pf = new PageReference('/apex/cs_Accurals?id='+oppr.Id);
        Test.setCurrentPage(pf);
        cs_accuralsController ctrl = new cs_accuralsController();
        ctrl.detailWrpId=1;
        ctrl.reason = 'Test Record';
        ctrl.errorMsg_reason='';
        ctrl.errorMsg_month ='';
        ctrl.accrualId = acc.Id;
        ctrl.recordId=alloc.Id;
        ctrl.insrt =true;
        
        ctrl.accuredDate = date.today();
        cs_accuralsController.AllocationDetailsWrapper wrapp= new cs_accuralsController.AllocationDetailsWrapper(true,alloc,1);
        ctrl.accural = new Accrual__c();
        ctrl.accural.Month_To_Accrue__c = system.today().addMonths(1).toStartOfMonth();
        ctrl.accural.Is_Engaged_with_Client__c = true;
        ctrl.accural.IsPrice__c= true;
        ctrl.accural.Job__c = oppr.id;
        ctrl.accural.IsDeliverySer__c= true;
        ctrl.acc =  new Accrual__c();
        ctrl.acc.Job__c = oppr.id;
        
        ctrl.AllocationWrapperLst = new List<cs_accuralsController.AllocationDetailsWrapper>();
        ctrl.AllocationWrapperLst.add(wrapp);
        ctrl.save();
        ctrl.checkRecord();
        ctrl.getItems();
        ctrl.reverseAccrual();
        ctrl.cancel();
        ctrl.close_ErrorModal();
        ctrl.validateAccural();
        
        
        //ctrl.fetchAllocationList();
 
        Test.stopTest();  
    }
    static testmethod void TestCS_AccuralsControlleractive(){
        setupData();
        // opp = [Select id from opportunity Limit 1];
        
        Test.startTest();
        PageReference pf = new PageReference('/apex/cs_Accurals?id='+oppobj.Id);
        Test.setCurrentPage(pf);
        cs_accuralsController ctrl = new cs_accuralsController();
        ctrl.detailWrpId=1;
        ctrl.reason = 'Test Record';
        ctrl.errorMsg_reason='';
        ctrl.errorMsg_month ='';
        ctrl.accrualId = acc.Id;
        ctrl.recordId=alloc.Id;
        ctrl.insrt =true;
        
        ctrl.accuredDate = date.today();
        cs_accuralsController.AllocationDetailsWrapper wrapp= new cs_accuralsController.AllocationDetailsWrapper(true,alloc,1);
        ctrl.accural = new Accrual__c();
        ctrl.accural.Month_To_Accrue__c = system.today().addMonths(1).toStartOfMonth();
        ctrl.accural.Is_Engaged_with_Client__c = true;
        ctrl.accural.IsPrice__c= true;
        ctrl.accural.Job__c = oppr.id;
        ctrl.accural.IsDeliverySer__c= true;
        ctrl.acc =  new Accrual__c();
        ctrl.acc.Job__c = oppr.id;
        
        ctrl.AllocationWrapperLst = new List<cs_accuralsController.AllocationDetailsWrapper>();
        ctrl.AllocationWrapperLst.add(wrapp);
        ctrl.save();
        ctrl.checkRecord();
        ctrl.getItems();
        ctrl.reverseAccrual();
        ctrl.cancel();
        ctrl.close_ErrorModal();
        ctrl.validateAccural();
        
        
        //ctrl.fetchAllocationList();
 
        Test.stopTest();  
    }
    static testmethod void testFetchAllocationList(){
        setupData();
        opp = [Select id from opportunity Limit 1];
        List<Allocation__c> allocLst = new List<Allocation__c>();
        set<id> accIds = new set<id>();
        
        Map<id,Accrual__c> accMap = new Map<id,Accrual__c>();
        accMap.put(acc.id,acc);
        accIds.add(acc.id);
        
        Integer monthSelected = system.today().month();
        allocLst =[SELECT id,name,workType_Allocation__c,Office__c,Department_Allocation__c,Complete__c,Job__r.Account.Name,Assigned_To__r.name,Assigned_To__r.Office__c,AccruedAmount__c,(Select id,name,CS_Assigned_To__c,Amount__c,CS_Forecast_Date__c from Forecastings__r where CALENDAR_MONTH(CS_Forecast_Date__c)=:monthSelected AND Amount__c>:0 limit 1) from Allocation__c where Job__c=:opp.id AND externalCompany__c=Null AND Complete__c =: false];
        
        CS_AccuralTriggerHandler.createReversalAccural(accMap);
        CS_AccuralTriggerHandler.calculateForecastAmount(accIds);
        CS_AccuralTriggerHandler.recallApprovalProcess(accIds);

    }
    

}