@isTest
private class ConfigurationManagerTest {
	@isTest
    private static void singletonTest() {
        ConfigurationManager initialInstance = ConfigurationManager.getInstance();
        ConfigurationManager secondaryInstance = ConfigurationManager.getInstance();

        System.assert(
            initialInstance === secondaryInstance, 
            'The get instance method should always return the same instance of the ConfigurationManager'
        );
    }

    @isTest
    private static void isSharingOffTest_NoSetting() {
        System.assert(
            ConfigurationManager.getInstance().isSharingOn(),
            'The sharing should always be off by default, ie when a bypass configuration record does not exist in the system'
        );
    }

    @isTest
    private static void toggleSharingTest() {
        ConfigurationManager congigurationManager = ConfigurationManager.getInstance();
        congigurationManager.turnSharingOff();
        System.assert(
            congigurationManager.isSharingOff(),
            'Sharing should have truned off after the off method is called'
        );
        congigurationManager.turnSharingOn();
        System.assert(
            congigurationManager.isSharingOn(),
            'Sharing should have truned On after the On method is called'
        );
    }

    @isTest
    private static void commitConfigurationChangesTest_WithoutExistingSetting() {
        System.assertEquals(
            0, [SELECT COUNT() FROM Bypass_Configuration__c],
            'There should be no Bypass_Configuration__c records in the system before the save method is called'
        );
        ConfigurationManager congigurationManager = ConfigurationManager.getInstance();
        congigurationManager.commitConfigurationChanges();
        System.assertEquals(
            1, [SELECT COUNT() FROM Bypass_Configuration__c],
            'There should be 1 Bypass_Configuration__c records after the bypass method is called'
        );
    }

    @isTest
    private static void commitConfigurationChangesTest_WithExistingSetting() {
        Bypass_Configuration__c configurationSetting = new Bypass_Configuration__c(
            Is_Sharing_Off__c = true
        );
        insert configurationSetting;
        ConfigurationManager congigurationManager = ConfigurationManager.getInstance();
        System.assert(
            congigurationManager.isSharingOff(),
            'Sharing should be truned off when a Bypass_Configuration__c is created with it turned off'
        );
        congigurationManager.commitConfigurationChanges();
        configurationSetting = [
            SELECT Is_Sharing_Off__c 
            FROM Bypass_Configuration__c 
            WHERE Id = :configurationSetting.Id
        ];
        System.assert(
            configurationSetting.Is_Sharing_Off__c, 
            'The Bypass_Configuration__c setting should have been comitted with sharing off'
        );
    }
}