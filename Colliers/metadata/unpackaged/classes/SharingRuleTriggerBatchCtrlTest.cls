/**
*  Class Name: SharingRuleTriggerBatchCtrlTest
*  Description: Test class for SharingRuleTriggerBatchCtrl which is a controller for Visualforce page which 
*  is used to review sharing rules (custom functionality) batch job status or start those
*
*  Company: CloudShift
*  CreatedDate: 11/07/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  ----------------------------------------------------------- 
*  Kaspars Rezgalis		11/07/2018					Created
*
*/

@isTest(seeAllData=false)
private class SharingRuleTriggerBatchCtrlTest{
	private static Map<String, SObject> setupSharedTestData(){
		Map<String, SObject> retValues = new Map<String, SObject>();

		AssignId__c assignId = TestObjectHelper.createAssignId();
		assignId.AssignNo__c = 1;
		AssignAccountId__c assignAccId = TestObjectHelper.createAssignAccCode();
		Job_Amount__c jobAmount = TestObjectHelper.createJobAmount();
		colliers_Vat_Number__c vatNum = (colliers_Vat_Number__c)SmartFactory.createSObject('colliers_Vat_Number__c',false);
		currency_Symbol__c curr= (currency_Symbol__c)SmartFactory.createSObject('currency_Symbol__c',false);

		Bypass_Configuration__c setting = new Bypass_Configuration__c(
			Is_Sharing_Off__c = true,
			SetupOwnerId = UserInfo.getUserId()
		);

		Group testGroup = new Group(
			Name = 'testGroup'
		);

		Database.Insert(new List<SObject>{assignId, assignAccId, jobAmount, vatNum, curr, setting, testGroup}, true);
		retValues.put('Group', testGroup);

		Account a = TestObjectHelper.createAccount();
		a.Name = 'SharingTestAccount';
		Database.Insert(a, true);
		retValues.put('Account', a);

		Opportunity o = TestObjectHelper.createOpportunity(a);
		o.Name = 'opp1';
		Database.Insert(o, true);
		retValues.put('Opportunity', o);

		Sharing_Rule__c s = new Sharing_Rule__c(
			Name = 'OpportunityTestRule',
			Object__c = 'Opportunity',
			Description__c = 'Apex TEST rule',
			Access_Level__c = SharingEngine.SHARING_ACCESS_EDIT,
			Logic__c = SharingEngine.SHARING_LOGIC_ANY,
			Shared_With__c = testGroup.Id
		);
		Database.Insert(s, true);
		retValues.put('Sharing_Rule__c', s);

		Sharing_Condition__c sc = new Sharing_Condition__c(
			Sharing_Rule__c = s.Id,
			Field_API_Name__c = 'AccountId',
			Operator__c = SharingEngine.SHARING_CONDITION_EQUALS,
			Value__c = a.Id,
			Index__c = 1
		);
		Database.Insert(sc, true);
		retValues.put('Sharing_Condition__c', sc);

		s.Change_Status__c = SharingEngine.SHARING_RULE_CHANGE_STATUS_COMPLETED;
		Database.Update(s, true);

		return retValues;
	}

	@isTest
	private static void test_errors(){
		Map<String, SObject> testData = setupSharedTestData();

		SharingRuleTriggerBatchCtrl extension = new SharingRuleTriggerBatchCtrl();
		System.assertEquals(0, ApexPages.getMessages().size(), 'There should be no page messages when loading page. Actual: ' + ApexPages.getMessages());
		System.assertEquals(false, extension.getCurrentSharingConfigIsOn(), 'Sharing is turned off and correct value should be returned by method getCurrentSharingConfigIsOn().');

		for(SharingEngine_Status__c s : extension.getCurrentEngineStatuses()){
			System.assertEquals(false, s.Batch_Processing_in_Progress__c, 'SharingEngine_Status__c custom settings do not exist, so are created on the fly; in such case flag is not set that batch is running.');
		}

		Test.setCurrentPage(Page.SharingRuleTriggerBatch);
		Test.startTest();
			extension.triggerBatch();
			Assert.pageMessageExists(SharingEngineBatch.ERROR_NO_SOBJECT);

			extension.whichSObject = 'Opportunity';
			extension.triggerBatch();
			Assert.pageMessageExists(SharingEngineBatch.ERROR_SHARING_IS_OFF);

			ConfigurationManager configManager = ConfigurationManager.getInstance();
			configManager.turnSharingOn();
			configManager.commitConfigurationChanges();

			System.assertEquals(true, extension.getCurrentSharingConfigIsOn(), 'Sharing is turned on now and correct value should be returned by method getCurrentSharingConfigIsOn().');

			SharingEngineConfigurationManager sharingSettingForOpp = SharingEngineConfigurationManager.getInstance(extension.whichSObject);
        	sharingSettingForOpp.setBatchSharingIsRunning_ON();
        	sharingSettingForOpp.commitConfigurationChanges();

			extension.triggerBatch();
			Assert.pageMessageExists(SharingEngineBatch.ERROR_SHARING_IS_OFF);

			for(SharingEngine_Status__c s : extension.getCurrentEngineStatuses()){
				if(s.Name==extension.whichSObject){
					System.assertEquals(true, s.Batch_Processing_in_Progress__c, 'SharingEngine_Status__c custom settings for Opporutnity exist now and flag should be set that batch is running.');
					break;
				}
			}

		Test.stopTest();
	}

	@isTest
	private static void test_constantOptions(){
		SharingRuleTriggerBatchCtrl extension = new SharingRuleTriggerBatchCtrl();
		System.assertEquals(0, ApexPages.getMessages().size(), 'There should be no page messages when loading page. Actual: ' + ApexPages.getMessages());

		Test.setCurrentPage(Page.SharingRuleTriggerBatch);

		Test.startTest();
			System.assert(String.isNotBlank(extension.getCurrentSharingConfigObjectPrefixForLink()), 'Controller should at all times return prefix for Bypass_Configuration__c custom setting.');
			System.assert(String.isNotBlank(extension.getSharingRuleObjectPrefixForLink()), 'Controller should at all times return prefix for Sharing_Rule__c custom object.');
			System.assert(String.isNotBlank(extension.getErrorLogObjectPrefixForLink()), 'Controller should at all times return prefix for Error_Log__c custom object.');
			System.assertNotEquals(0, extension.getSharingRuleWhichObjectOptions().size(), 'There should be some Object__c picklist options available on Sharing_Rule__c at all times.');
		Test.stopTest();
	}

	@isTest
	private static void test_triggerBatchForOpportunity(){
		Map<String, SObject> testData = setupSharedTestData();

		Opportunity o = (Opportunity)testData.get('Opportunity');
		Sharing_Rule__c s = (Sharing_Rule__c)testData.get('Sharing_Rule__c');

		String sObjectNameForTest = 'Opportunity';

		SharingRuleTriggerBatchCtrl extension = new SharingRuleTriggerBatchCtrl();
		System.assertEquals(0, ApexPages.getMessages().size(), 'There should be no page messages when loading page. Actual: ' + ApexPages.getMessages());

		ConfigurationManager configManager = ConfigurationManager.getInstance();
		configManager.turnSharingOn();
		configManager.commitConfigurationChanges();

		SharingEngineConfigurationManager sharingSettingForOpp = SharingEngineConfigurationManager.getInstance(sObjectNameForTest);
        sharingSettingForOpp.setBatchSharingIsRunning_OFF();
        sharingSettingForOpp.commitConfigurationChanges();

		Test.setCurrentPage(Page.SharingRuleTriggerBatch);

		Test.startTest();
			extension.whichSObject = sObjectNameForTest;
			extension.triggerBatch();
			Assert.pageMessageExists(SharingEngineBatch.SUCCESS_BATCH_QUEUED);
		Test.stopTest();

		List<Error_Log__c>errors = new List<Error_Log__c>([SELECT Details__c FROM Error_Log__c WHERE Log_Type__c='Sharing Error']);
		System.assertEquals(0, errors.size(), 'There should be no error log entries for sharing error. Actual: '+errors);

		List<OpportunityShare>sh = new List<OpportunityShare>([SELECT Id, OpportunityId FROM OpportunityShare WHERE RowCause=:SharingEngine.SHARING_ROW_CAUSE_STD_OBJECTS AND UserOrGroupId=:s.Shared_With__c AND OpportunityId=:o.Id]);
		System.assertEquals(1, sh.size(), 'There should be one manual share record - custom sharing is enabled and it applies only to one opportunity (all conditions should be matched = AccountId and CloseDate).');
		System.assertEquals(o.Id, sh[0].OpportunityId, 'OpportunityId for the only share record should match to second opportunity which has close date within next 100 days.');
	}
}