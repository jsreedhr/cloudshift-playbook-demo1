/**
*  Class Name: ErrorLogsHelper
*  Description: Helper class to facilitate logging of any errors to custom object Error_Log__c.
*
* USE PATTERNS

	1. Collect errors within relevant piece of code and pass for processing only by the end of code execution; commit error log records to DB at the end:
		List<String>errorMessagesForLog = new List<String>();
		[...]
		errorMessagesForLog.add('my own error');
		[...]
		ErrorLogsHelper errorLogging = ErrorLogsHelper.getInstance();
		errorLogging.addListToErrorLog(errorMessagesForLog);
		errorLogging.setErrorLogType('Sharing Error'); //set this only if default value "System Error" should be overwritten (Log_Type__c on Error_log__c)
		errorLogging.saveErrorLogs();

	2. Instantiate class in the beginning and add errors whenever those are encountered; commit error log records to DB at the end:
		ErrorLogsHelper errorLogging = ErrorLogsHelper.getInstance();
		[...]
		errorLogging.addStringToErrorLog('my own error');
		[...]
		try{
		
		}catch(Exception e){
			errorLogging.addExceptionToErrorLog(e);
		}
		[...]
		errorLogging.saveErrorLogs();

*
* Tests available in ErrorLogsHelperTest
*
*  Company: CloudShift
*  CreatedDate: 18/07/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  ----------------------------------------------------------- 
*  Kaspars Rezgalis		18/07/2018					Created
*
*/

public class ErrorLogsHelper{
	@testVisible private String errorLogType = 'System Error';
	private List<String>errors = new List<String>();
	private static final ErrorLogsHelper instance = new ErrorLogsHelper();
    
    public static ErrorLogsHelper getInstance(){
        return instance;
    }

    private ErrorLogsHelper(){
    
    }

	public void setErrorLogType(String errorLogType){
		this.errorLogType = errorLogType;
	}

    public void addExceptionToErrorLog(Exception e){
		String combinedError = 'Error: ' + e.getMessage() + ' : ' + e.getStackTraceString();
		errors.add(combinedError);
	}

	public void addStringToErrorLog(String error){
		errors.add(error);
	}

	public void addListToErrorLog(List<String>errorsList){
		errors.addAll(errorsList);
	}

	public void saveErrorLogs(){
		if(!errors.isEmpty()){
			String combinedError = String.join(errors, '\n');
			Error_Log__c errorRecord = new Error_Log__c(
				Log_Type__c = errorLogType,
				Details__c = (combinedError.length()>10000 ? '[...]' + combinedError.substring(combinedError.length()-9000, combinedError.length()) + '\n' : combinedError)
				//if error is too long, take only last lines
			);

			Database.Insert(errorRecord, false);
			errors.clear();
		}
    }
}