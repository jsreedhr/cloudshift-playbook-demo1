@isTest
private class Test_D0450{

    @isTest
    private static void testD0450_test(){
        User userObj;
        Account account;
        Contact contact;
        Opportunity opportunity;
        Invoice__c invoice;
        Invoice_Allocation_Junction__c invAllocationJn;
        Allocation__c allocation;
        Disbursements__c cost;
        List<Invoice_Line_Item__c> invoiceLineItems;

        User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       // Insert account as current user
        System.runAs (currentUser) {
            // create user
            userObj = TestObjectHelper.createAdminUser(true);
            userobj.UserName ='Test_123@dquotient.com';
            insert userObj;
            System.assert(userObj.id!=NULL);
        }
        //account;
        account = new Account();
        account.Name = 'Some Name';
        account.Town__c = 'acc town';
        account.Street_No__c ='#12';
        account.Street__c ='Street';
        account.Post_Code__c = '2345';
        account.Country__c ='UK';
        account.Company_Status__c = 'Active';
        insert account;

        //contact
        contact = (Contact)SmartFactory.createSObject('Contact', false);
        contact.AccountId = account.Id;
        contact.lastName='Test';
        contact.Email ='test1@test1.com';
        insert contact;


        //Address__c
        Address__c address = new Address__c();
        address = (Address__c)SmartFactory.createSObject('Address__c', false);
        address.Address_Type__c = 'Primary';
        address.Area__c = '500';
        address.Building__c = 'XYZ Apartment';
        address.Country__c = 'Netherlands';
        address.Flat_Number__c = '301';
        address.Head_Office_Address__c = true;
        address.Postcode__c = '123 456';
        address.Street__c = 'Abc';
        address.Street_Number__c = '13A';
        address.Town__c = 'some town';

        address.Estate__c = 'estate';
        address.Flat_Number__c = '#12';
        address.Postcode__c = '2345';
        address.Country__c = 'United Kingdom';
        insert address;

        //Staff__c
        Staff__c staff = new Staff__c();
        staff.name='Jaredresrstt8SDhHHZSHxzM';
        staff.Email__c='resrstt8SDhHHZSHxzM.ZMSDLM@gmail.com';
        staff.active__c = true;
        staff.HR_No__c = String.valueOf(Math.round(Math.random()*1000) + Math.random()*345);
        staff.User__c = userInfo.getUserId();
        insert staff;

        //Work_Type__c
        Work_Type__c workType = new Work_Type__c();
        workType.Name = 'worktype';
        workType.Active__c = true;
        insert workType;

        Coding_Structure__c codingStructure = new Coding_Structure__c();
        codingStructure.Work_Type__c = workType.Id;
        codingStructure.Staff__c = staff.Id;
        codingStructure.Status__c = 'Active';
        codingStructure.Department__c = 'International Properties';
        codingStructure.Office__c = 'Amsterdam';
        insert codingStructure;


        //Contact Address Jn
        Contact_Address_Junction__c contactAddressJn = new Contact_Address_Junction__c();
        contactAddressJn.Address__c = address.Id;
        contactAddressJn.Contact__c = contact.Id;
        insert contactAddressJn;


        //account Address Jn
        Account_Address_Junction__c accountAddressJn = new Account_Address_Junction__c();
        accountAddressJn.Address__c = address.Id;
        accountAddressJn.Account__c = account.Id;
        insert accountAddressJn;


        //Opportunity__c
        Date today = Date.today();
        SmartFactory.IncludedFields.put('Opportunity', new Set<String> { 'Sales_Region__c' });
        opportunity = (Opportunity)SmartFactory.createSObject('Opportunity',false);
        opportunity.Name = 'test' + Math.random()*1000;
        opportunity.Amount = 2000;
        opportunity.Date_Instructed__c = today;
        opportunity.CloseDate = today+30;
        opportunity.Source_Of_Instruction__c = 'Client - new/own initiative';
        opportunity.Manager__c = staff.Id;
        opportunity.Department__c = 'International Properties';
        opportunity.Office__c = 'Amsterdam';
        opportunity.Work_Type__c = 'worktype';

        opportunity.AccountId = account.Id;
        opportunity.Instrcting_Contact_name__c = contact.Id;
        opportunity.InstructingCompanyAddress__c = address.Id;
        opportunity.Instructing_Company_Role__c = 'Landlord';
        opportunity.Instructing_Contact__c = contact.Id;

        opportunity.Engagement_Letter__c = true;
        opportunity.Engagement_Letter_Sent_Date__c = Date.today();
        opportunity.Property_Area__c = 1000;
        opportunity.Property_Area_UOM__c = 'sqm';
        insert opportunity;

        //Care_Of__c
        Care_Of__c careOf = new Care_Of__c();
        careOf= (Care_Of__c)SmartFactory.createSObject('Care_Of__c', false);
        careOf.Client__c = account.id;
        careOf.Contact__c = contact.id;
        careOf.Colliers_Int__c = true;
        insert careOf;


        //Allocation__c
        allocation = (Allocation__c)SmartFactory.createSObject('Allocation__c',false);
        allocation.job__c= opportunity.id;
        allocation.Main_Allocation__c = true;
        allocation.Department_Allocation__c = 'International Properties';
        allocation.Assigned_To__c=staff.id;
        allocation.complete__c = false;
        insert allocation;

        //not creating purchase order
        //Invoice__c
        invoice = new Invoice__c();
        invoice = (Invoice__c)SmartFactory.createSObject('Invoice__c',false);
        invoice.Opportunity__c= opportunity.id;
        invoice.contact__c=contact.id;
        invoice.is_International__c=true;
        invoice.status__c ='Draft';
        invoice.Invoice_Wording__c = 'testWord';
        insert invoice;


        //Invoice_Line_Item__c
        invoiceLineItems = new List<Invoice_Line_Item__c>();
        Invoice_Line_Item__c invoiceLineItem1 = new Invoice_Line_Item__c();
        invoiceLineItem1 = (Invoice_Line_Item__c)SmartFactory.createSObject('Invoice_Line_Item__c',false);
        invoiceLineItem1.Invoice__c = invoice.id;
        invoiceLineItem1.type__c = 'Fee';
        invoiceLineItem1.Amount_ex_VAT__c = 1000;
        invoiceLineItem1.VAT_Amount__c = 200;
        invoiceLineItem1.International_Amount__c = 1000;
        invoiceLineItems.add(invoiceLineItem1);

        Invoice_Line_Item__c invoiceLineItem2 = new Invoice_Line_Item__c();
        invoiceLineItem2 = (Invoice_Line_Item__c)SmartFactory.createSObject('Invoice_Line_Item__c',false);
        invoiceLineItem2.Invoice__c = invoice.id;
        invoiceLineItem2.type__c = 'Cost';
        invoiceLineItem2.Amount_ex_VAT__c = 500;
        invoiceLineItem2.VAT_Amount__c = 100;
        invoiceLineItem2.International_Amount__c = 500;
        invoiceLineItems.add(invoiceLineItem2);

        insert invoiceLineItems;

        //Cost__c
        cost = new Disbursements__c();
        cost = (Disbursements__c)SmartFactory.createSObject('Disbursements__c',false);
        cost.Job__c = opportunity.id;
        cost.InvoiceId__c = invoice.Id;
        cost.Purchase_Cost__c = 1000;
        cost.Recharge_Cost__c = 1000;
        cost.Category__c ='Cost Flow Through (Residential only)';
        cost.Created_For__c = staff.id;
        cost.Sub_Category__c='Cost Flow Through (RESIDENTIAL ONLY) - With VAT';
        cost.Purchase_Date__c  = System.today();
        cost.Description__c = 'Tsts';
        cost.Invoice_Details__c = 'Tsts';
        insert cost;


        //Forecasting__c
        Forecasting__c forecasting = new Forecasting__c();
        forecasting = (Forecasting__c)SmartFactory.createSObject('Forecasting__c',false);
        forecasting.Allocation__c =allocation.id;
        forecasting.CS_Forecast_Date__c = system.today().toStartOfMonth();
        //amount must be equal to Invoice line items total
        forecasting.Amount__c=1800.0;
        insert forecasting;

        //Invoice Cost Junction
        Invoice_Cost_Junction__c invoiceCostJn = new Invoice_Cost_Junction__c();
        invoiceCostJn = (Invoice_Cost_Junction__c)SmartFactory.createSObject('Invoice_Cost_Junction__c',false);
        invoiceCostJn.Disbursement__c = cost.id;
        invoiceCostJn.Invoice__c = invoice.id;
        insert invoiceCostJn;

        //Invoice Allocation Jn
        Invoice_Allocation_Junction__c invoiceAllocationJn = new Invoice_Allocation_Junction__c();
        invoiceAllocationJn = (Invoice_Allocation_Junction__c)SmartFactory.createSObject('Invoice_Allocation_Junction__c',false);
        invoiceAllocationJn.Allocation__c= allocation.id;
        invoiceAllocationJn.Invoice__c=invoice.id;
        invoiceAllocationJn.Forecasting__c = forecasting.id;
        insert invoiceAllocationJn;


        pageReference pr = Page.CS_ManageMyJobPage;
        pr.getParameters().put('id',opportunity.id);
        Test.setCurrentPage(pr);
        cs_companiesContactsController controller1 = new cs_companiesContactsController();
        controller1.changeInv = 'YES';
        controller1.jobDetails.Instructing_Contact__c = contact.Id;
        controller1.renderInvoiceDetails();

        controller1.save();

        Invoice_Allocation_Junction__c invAllocJn = [Select Id From Invoice_Allocation_Junction__c Where Invoice__c =: invoice.Id AND Allocation__r.Main_Allocation__c = TRUE];
        System.assertNotEquals(null, invAllocJn);

        Opportunity opp = [Select Id, StageName From Opportunity Where Id =: opportunity.Id];
        System.assertNotEquals(null,opp.StageName);

        pr = Page.CS_JobInvoiceViewClone;
        pr.getParameters().put('id', opportunity.Id);
        CS_JobInvoiceController controller2 = new CS_JobInvoiceController();
        controller2.refreshAll();
        controller2.initInvoice();
        controller2.selectedId = invoice.Id;
        controller2.selectedInvoiceDetails();
        System.assertEquals(true, controller2.showPDFTab);

        
        controller2.feesList[0].PDF_Amount__c = 10000;
        controller2.feesList[0].PDF_VAT__c = 2000;
        controller2.costsList[0].PDF_Amount__c = 4000;
        controller2.costsList[0].PDF_VAT__c = 400;
        controller2.pdfAmountTotal = controller2.pdfInvoice.Total_PDF_Amount__c -1000;

        controller2.saveAll();
        controller2.updatePDFValues();
        System.assertEquals(TRUE,controller2.pdfTabFailed);

        // look for cost of 'Fee' & 'Cost' invoice line item above
        //pdfAmountTotal should match sum of all invoice line items
        controller2.feesList[0].PDF_Amount__c = 1300;
        controller2.feesList[0].PDF_VAT__c = 0;
        controller2.costsList[0].PDF_Amount__c = 200;
        controller2.costsList[0].PDF_VAT__c = 0;
        controller2.pdfAmountTotal = controller2.pdfInvoice.Total_PDF_Amount__c;

        controller2.saveAll();
        controller2.updatePDFValues();
        System.assertEquals(TRUE,controller2.pdfTabSuccess);
        
    }
}