@RestResource(urlMapping='/ShareDo/v1')
global with sharing Class VALS_ShareDoInbound
{
    public Blob responseBody{get;set;}
    @HttpPost
    global static void createRecord()
    {

        Boolean inspectionExists = false;
        Boolean validationError = false;
        String valdationErrorMessage = '';
        RestRequest request = RestContext.request;
        mainWrapper mainWrapperObj = (mainWrapper)JSON.deserialize(request.requestbody.tostring(), mainWrapper.Class);
        System.Debug('------>x'+request.requestbody.toString());
        //String jsonsts='{"@t":"2017-06-20T17:39:36.7520029Z","@mt":"SalesforceInspectionService:SendModel model - {@model}","@m":"SalesforceInspectionService:SendModel model - {\r\n \"_typeTag\": \"SalesforceInspectionOutboundRequest\",\r\n \"jobRef\": \"0060E000003ePl0QAE\",\r\n \"propertyRef\": \"a030E0000005WNpQAM\",\r\n \"requirements\": \"\",\r\n \"inspectionType\": \"Drive by\",\r\n \"inspectionName\": \"Inspection Required\",\r\n \"measurementRequired\": \"Check\",\r\n \"siteContact\": \"Richard Heath\",\r\n \"siteMobile\": null,\r\n \"siteLandline\": null,\r\n \"siteEmail\": null,\r\n \"Participants\": [\r\n {\r\n \"_typeTag\": \"SalesforceInspectionParticipantOutboundRequest\",\r\n \"Role\": \"inspection-contact\",\r\n \"HRreference\": \"0030E000007ERejQAG\"\r\n },\r\n {\r\n \"_typeTag\": \"SalesforceInspectionParticipantOutboundRequest\",\r\n \"Role\": \"inspection-surveyor\",\r\n \"HRreference\": \"71049\"\r\n }\r\n ]\r\n}","@i":"5f9450ed","model":{"_typeTag":"SalesforceInspectionOutboundRequest","jobRef":"0060E000003ePl0QAE","propertyRef":"a030E0000005WNpQAM","requirements":"","inspectionType":"Drive by","inspectionName":"Inspection Required","measurementRequired":"Check","siteContact":"Richard Heath","siteMobile":null,"siteLandline":null,"siteEmail":null,"Participants":[{"_typeTag":"SalesforceInspectionParticipantOutboundRequest","Role":"inspection-contact","HRreference":"0030E000007ERejQAG"},{"_typeTag":"SalesforceInspectionParticipantOutboundRequest","Role":"inspection-surveyor","HRreference":"71049"}]},"Meta":null,"SessionId":"c121ecca-e39a-43c2-9a57-54308ca7f4f7","CorrelationId":"49410768-11b7-46e7-89aa-6a4dceb65f0a","UserId":null,"UserName":"","_AppName":"Sharedo-Web"}';
        //mainWrapper mainWrapperObj = (mainWrapper)JSON.deserialize(jsonsts, mainWrapper.Class);
        System.debug('----->main'+mainWrapperObj);
        if(mainWrapperObj!=NULL)
        {
            Inspection__c inspectionObj = new Inspection__c();
            if(mainWrapperObj.JobRef!=NULL)
            {
               inspectionObj.Job__c=mainWrapperObj.JobRef;
               Opportunity jobObj = [Select id,Account.Name,job_number__c from Opportunity where ID=:mainWrapperObj.JobRef LIMIT 1];
               if(jobObj!=NULL && jobObj.Account.Name!=NULL)
                inspectionObj.Client__c = jobObj.Account.Name;
               if(jobObj!=NULL && jobObj.Job_Number__c!=NULL)
                inspectionObj.Job_Number__c = jobObj.Job_Number__c;

            }
            else
            {
                validationError = true;
                valdationErrorMessage+='Job Id is missing,';
            }
            if(mainWrapperObj.PropertyRef!=NULL)
                inspectionObj.Property__c=mainWrapperObj.PropertyRef;
            else
            {
                validationError = true;
                valdationErrorMessage+='Property ID is missing';
            }
            if(mainWrapperObj.Inspectionname!=NULL)
            {
                inspectionObj.Name=mainWrapperObj.Inspectionname.left(80);
                //Need Clarity
                //inspectionObj.Full_Name__c=mainWrapperObj.InspectionName;
            }
            if(mainWrapperObj.Sitecontact!=NULL)
                inspectionObj.Site_Contact__c=mainWrapperObj.Sitecontact;
            if(mainWrapperObj.Sitemobile!=NULL)
                inspectionObj.Site_Mobile__c=mainWrapperObj.Sitemobile;
            if(mainWrapperObj.Sitelandline!=NULL)
                inspectionObj.Site_Landline__c=mainWrapperObj.Sitelandline;
            if(mainWrapperObj.Siteemail!=NULL)
                inspectionObj.Site_email__c=mainWrapperObj.Siteemail;

            if(mainWrapperObj.Requirements!=NULL)
                inspectionObj.Special_Requirements__c=mainWrapperObj.Requirements;
            if(mainWrapperObj.Measurementrequired!=NULL)
                inspectionObj.Measurement_Required__c=mainWrapperObj.Measurementrequired;
            if(mainWrapperObj.InspectionType!=NULL)
                inspectionObj.Inspection_Type_del__c=mainWrapperObj.InspectionType;

            if(mainWrapperObj.participants!=NULL && mainWrapperObj.participants.size()>0)
            {
                for(participantWrapper partObj : mainWrapperObj.participants)
                {
                    if(partObj.role!=NULL && partObj.role.containsIgnoreCase('surveyor') && partObj.HRreference!=NULL)
                    {
                        Staff__c staffObj = [Select id,Full_Name__c,User__c from Staff__c where HR_No__c=:partObj.HRreference LIMIT 1];
                        if(staffObj!=NULL && staffObj.Full_Name__c!=NULL)
                            inspectionObj.Inspecting_Surveyor__c = staffObj.Full_Name__c.Left(18);
                        if(staffObj!=NULL && staffObj.User__c!=NULL)
                            inspectionObj.OwnerId = staffObj.User__c;
                    }
                    System.Debug(mainWrapperObj.participants);
                }
            }

            if(inspectionObj.Property__c!=NULL)
            {
                Property__c propertyObj = new Property__c();
                propertyObj = [Select id,Area__c,Estate__c,Building_Name__c,Suite_Unit__c,Floor_No__c,
                               Street_No__c,Street__c,Town__c,County__c,Country__c,Post_Code__c
                               from Property__c
                               where id=:inspectionObj.Property__c LIMIT 1];
                System.debug('Property Object'+propertyObj);
                
            }
            try{
                if(validationError==false)
                {
                    list<Inspection__c> inspectionList = [Select id from Inspection__c 
                                                          where Job__c =:inspectionObj.Job__c
                                                          AND Property__c =:inspectionObj.Property__c 
                                                          AND Job__c != NULL
                                                          AND Property__c != NULL LIMIT 999];
                    if(inspectionList!=NULL && inspectionList.size()>0)
                        inspectionExists = true;
                    else
                        insert inspectionObj;

                    System.Debug('Does an inspection exist?:'+inspectionExists);
                }
                
            }catch(DmlException e){
                System.debug('Error on Inspection Object insert (@HttpPost');
            }
            System.debug('Inspection Object:'+inspectionObj);
            if(inspectionObj.id!=NULL)
            {
                if(mainWrapperObj.participants!=NULL && mainWrapperObj.participants.size()>0)
                {
                    list<Inspection_Participant__c> inspecParticipantList = new list<Inspection_Participant__c>();
                    map<String,id> staffIdMap = new map<String,id>();
                    set<String> HrReferenceSet = new Set<String>();
                    list<Inspection_Participant__c> inspecParticipantInsertList = new list<Inspection_Participant__c>();
                    // ---------Need to remove query inside for loop-------------
                    for(participantWrapper pw : mainWrapperObj.participants)
                    {
                        if(pw.HRreference!=NULL )
                            HrReferenceSet.add(pw.HRreference);
                        else
                        {
                            validationError = true; 
                            valdationErrorMessage+='HR Referene code is missing';
                        }
                    }
                    if(HrReferenceSet.size()>0)
                    {
                        list<Staff__c> staffList = new list<Staff__c>();
                        staffList = [Select id,HR_No__c from Staff__c where HR_No__c IN :HrReferenceSet ];
                        System.Debug('Hr Set:'+staffList);
                        if(staffList.size()>0)
                        {
                            for(Staff__c staffObj : staffList)
                                staffIdMap.put(staffObj.HR_No__c,staffObj.id);
                        }
                    }
                    for(participantWrapper pw : mainWrapperObj.participants)
                    {
                        Inspection_Participant__C inspectPartObj = new Inspection_Participant__c();
                        inspectPartObj.Inspection__c = inspectionObj.id;
                        if(pw.HRreference!=NULL)
                        {
                            if(staffIdMap.containskey(pw.HRreference))
                             {
                                inspectPartObj.Staff__c = staffIdMap.get(pw.HRreference);
                             }   
                            //Ask about roles Field on Inpection Participant
                        }
                        inspecParticipantInsertList.add(inspectPartObj);
                        System.Debug(inspecParticipantInsertList);
                    } 
                    try{
                        insert inspecParticipantInsertList;
                    }catch(DmlException e)
                    {
                        System.Debug('Error on Inspection Participant Insert (@HttpPost');
                    }
                } 
            }
            RestContext.response.addHeader('Content-Type', 'application/json');
            if(inspectionObj.id!=NULL)
            {
                // RestContext.response.responseBody = Blob.valueOf('Inspection Inserted Succesfully, InspectionID:'+inspectionObj.id);
                successWrapper successWrapperObj = New successWrapper(inspectionObj.ID);
                String succesObj = JSON.serialize(successWrapperObj);
                RestContext.response.responseBody = Blob.valueOf(succesObj);
                RestContext.response.statusCode = 200;
                System.Debug('Inspection Inserted Succesfully, InspectionID:'+inspectionObj.id);
            }
            else
            {
                String restBody = 'Operation Failed: ';
                if(valdationErrorMessage!='' && valdationErrorMessage!=NULL)
                    restBody+=valdationErrorMessage;
                if(inspectionExists)
                    restBody+=' An inspection with the given Property and Job reference already exists.';
                RestContext.response.responseBody = Blob.valueOf(restBody);
                RestContext.response.statusCode = 400;
                System.Debug(valdationErrorMessage);
            }
        }
    }
    @HttpPut
    global static void updateRecord()
    {
        RestRequest request = RestContext.request;
        mainWrapper mainWrapperObj = (mainWrapper)JSON.deserialize(request.requestbody.tostring(), mainWrapper.Class);
        String responseMessage = 'Begin';
        if(mainWrapperObj!=NULL && mainWrapperObj.InspectionId!=NULL)
        {
            Inspection__c inspectionObj = new Inspection__c();
            if(mainWrapperObj.JobRef!=NULL)
                inspectionObj.Job__c=mainWrapperObj.JobRef;
            if(mainWrapperObj.PropertyRef!=NULL)
                inspectionObj.Property__c=mainWrapperObj.PropertyRef;
            if(mainWrapperObj.Inspectionname!=NULL)
            {
                inspectionObj.Name=mainWrapperObj.Inspectionname.left(80);
               // inspectionObj.Full_Name__c=mainWrapperObj.InspectionName;
            }
            if(mainWrapperObj.InspectionId!=NULL)
                inspectionObj.Id=mainWrapperObj.InspectionId;
            if(mainWrapperObj.Sitecontact!=NULL)
                inspectionObj.Site_Contact__c=mainWrapperObj.Sitecontact;
            if(mainWrapperObj.Sitemobile!=NULL)
                inspectionObj.Site_Mobile__c=mainWrapperObj.Sitemobile;
            if(mainWrapperObj.Sitelandline!=NULL)
                inspectionObj.Site_Landline__c=mainWrapperObj.Sitelandline;
            if(mainWrapperObj.Siteemail!=NULL)
                inspectionObj.Site_email__c=mainWrapperObj.Siteemail;
            
            if(mainWrapperObj.Measurementrequired!=NULL)
                inspectionObj.Measurement_Required__c=mainWrapperObj.Measurementrequired;

            if(inspectionObj.Property__c!=NULL)
            {
                Property__c propertyObj = new Property__c();
                propertyObj = [Select id,Area__c,Estate__c,Building_Name__c,Suite_Unit__c,Floor_No__c,
                               Street_No__c,Street__c,Town__c,County__c,Country__c,Post_Code__c
                               from Property__c
                               where id=:inspectionObj.Property__c LIMIT 1];
                
            }
            try{
                update inspectionObj;
            }catch(DmlException e){
                responseMessage = 'Operation Failed';
                System.debug('Error on Inspection Object insert (@HttpPut');
            }
            if(inspectionObj.id!=NULL)
            {
                responseMessage = 'Inspection Updated Succesfully, InspectionID:'+inspectionObj.id;
                if(mainWrapperObj.participants!=NULL && mainWrapperObj.participants.size()>0)
                {
                    list<Inspection_Participant__c> inspecParticipantDeleteList = new list<Inspection_Participant__c>();
                    inspecParticipantDeleteList = [Select id from Inspection_Participant__c where Inspection__c =:inspectionObj.id LIMIT 999];
                    delete inspecParticipantDeleteList;
                    map<String,id> staffIdMap = new map<String,id>();
                    set<String> HrReferenceSet = new Set<String>();
                    list<Inspection_Participant__c> inspecParticipantInsertList = new list<Inspection_Participant__c>();
                    // ---------Need to remove query inside for loop-------------
                    for(participantWrapper pw : mainWrapperObj.participants)
                    {
                        if(pw.person!=NULL && pw.person.HRreference!=NULL)
                            HrReferenceSet.add(pw.person.HRreference); 
                    }
                    if(HrReferenceSet.size()>0)
                    {
                        list<Staff__c> staffList = new list<Staff__c>();
                        staffList = [Select id,HR_No__c from Staff__c where HR_No__c IN :HrReferenceSet LIMIT 1];
                        if(staffList.size()>0)
                        {
                            for(Staff__c staffObj : staffList)
                                staffIdMap.put(staffObj.HR_No__c,staffObj.id);
                        }
                    }
                    for(participantWrapper pw : mainWrapperObj.participants)
                    {
                        Inspection_Participant__C inspectPartObj = new Inspection_Participant__c();
                        inspectPartObj.Inspection__c = inspectionObj.id;
                        if(pw.person!=NULL && pw.person.HRreference!=NULL)
                        {
                            if(staffIdMap.containskey(pw.person.HRreference))
                                inspectPartObj.Staff__c = staffIdMap.get(pw.person.HRreference);
                            //Ask about roles Field on Inpection Participant
                        }
                        inspecParticipantInsertList.add(inspectPartObj);
                    } 
                    try{
                        insert inspecParticipantInsertList;
                    }catch(DmlException e){
                        System.Debug('Error on Inspection Participant Insert (@HttpPost');
                    }
                } 
            }
            RestContext.response.addHeader('Content-Type', 'application/json');
            if(responseMessage.containsIgnoreCase('Succesfully'))
            {
                RestContext.response.responseBody = Blob.valueOf(responseMessage);
                RestContext.response.statusCode = 200;
            }
            else
            {
                RestContext.response.responseBody = Blob.valueOf(responseMessage);
                RestContext.response.statusCode = 400;
            }
        }
    }
    global class mainWrapper
    {
        String JobRef;
        String PropertyRef;
        String InspectionName;
        String InspectionId;
        String Sitecontact;
        String Sitemobile;
        String Sitelandline;
        String Siteemail;
        String Requirements;
        String InspectionType;
        String Measurementrequired;
        list<participantWrapper> participants;
        global mainWrapper (String JobRef, String PropertyRef,
                            String InspectionName, String Sitecontact,
                            String Sitemobile,String Sitelandline,
                            String Siteemail,String Requirements,
                            String InspectionType,String Measurementrequired,
                            list<participantWrapper> participants)
        {
            if(JobRef!=NULL)
                this.JobRef = JobRef;
            if(PropertyRef!=NULL)
                this.PropertyRef = PropertyRef;
            if(Inspectionname!=NULL)
                this.Inspectionname = Inspectionname;
            if(Sitecontact!=NULL)
                this.Sitecontact = Sitecontact;
            if(Sitemobile!=NULL)
                this.Sitemobile = Sitemobile;
            if(Sitelandline!=NULL)
                this.Sitelandline = Sitelandline;
            if(Siteemail!=NULL)
                this.Siteemail = Siteemail;
            if(Requirements!=NULL)
                this.Requirements = Requirements;
            if(InspectionType!=NULL)
                this.InspectionType = InspectionType;
            if(Measurementrequired!=NULL)
                this.Measurementrequired = Measurementrequired;
            if(participants!=NULL && participants.size()>0)
                this.participants = participants;
        }
        global mainWrapper (String JobRef, String PropertyRef,
                            String InspectionName,String InspectionId, String Sitecontact,
                            String Sitemobile,String Sitelandline,
                            String Siteemail,String Requirements,
                            String InspectionType,String Measurementrequired,
                            list<participantWrapper> participants)
        {
            if(JobRef!=NULL)
                this.JobRef = JobRef;
            if(PropertyRef!=NULL)
                this.PropertyRef = PropertyRef;
            if(Inspectionname!=NULL)
                this.Inspectionname = Inspectionname;
            if(Sitecontact!=NULL)
                this.Sitecontact = Sitecontact;
            if(Sitemobile!=NULL)
                this.Sitemobile = Sitemobile;
            if(Sitelandline!=NULL)
                this.Sitelandline = Sitelandline;
            if(Siteemail!=NULL)
                this.Siteemail = Siteemail;
            if(Requirements!=NULL)
                this.Requirements = Requirements;
            if(InspectionType!=NULL)
                this.InspectionType = InspectionType;
            if(Measurementrequired!=NULL)
                this.Measurementrequired = Measurementrequired;
            if(participants!=NULL && participants.size()>0)
                this.participants = participants;
        }
    }
    global class participantWrapper
    {
        String role;
        String HRreference;
        personWrapper person;
        global participantWrapper(String role,String HRreference,personWrapper person)
        {
            if(role!=NULL)
                this.role = role;
            if(HRreference!=NULL)
                this.HRreference = HrReference;
            if(person!=NULL)
                this.person = person;
        }
    }
    global class personWrapper
    {
        String HRreference;
        global personWrapper(String HRreference)
        {
            if(HRreference!=NULL)
                this.HRreference = HRreference; 
        }

    }
    public class successWrapper
    {
        Public String inspectionId;
        public successWrapper(Id idObj)
        {
            if(idObj!=NULL)
                this.inspectionId = idObj;
        }
    }


}