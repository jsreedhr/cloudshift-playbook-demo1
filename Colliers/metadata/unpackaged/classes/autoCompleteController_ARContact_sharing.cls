global with sharing class autoCompleteController_ARContact_sharing
{
    @RemoteAction
    global static string findSObjects(string obj, string qry, string addFields, string profilename,string filterobject ,string filterfield,string accountid) 
    {
        /* More than one field can be passed in the addFields parameter
           Split it into an array for later use */
        List<String> fieldList=new List<String>();
        List<string> lstOfidstoFiler = new List<string>();
        
        if(filterobject != '' && string.isNotBlank(filterobject)){
            if(filterobject == 'AccountContactRelation'  ){
                List<AccountContactRelation> filteobjectlist = new List<AccountContactRelation>();
                if(filterfield != '' && string.isNotBlank(filterfield)){
                    if(filterfield == 'Contact' ){
                        string querystring = 'SELECT Id,ContactId FROM AccountContactRelation where AccountId =\'' +accountid+'\'';
                        filteobjectlist = Database.query(querystring);
                        if(!filteobjectlist.isEmpty()){
                            for(AccountContactRelation objacc : filteobjectlist){
                                if(objacc.ContactId != null  ){
                                    system.debug('idof contact'+objacc.ContactId);
                                    lstOfidstoFiler.add(objacc.ContactId);
                                }
                            }
                        }
                    }
                }
            }
        
        }
        
        
        if (addFields != '')  
        fieldList = addFields.split(',');
        
        /* Check whether the object passed is valid */
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sot = gd.get(obj);
        if (sot == null) 
        {
            return null;
        }
        
        /* Creating the filter text */
        String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
        //filter = 'AND Id IN:lstOfidstoFiler '
        /* Begin building the dynamic soql query */
        String soql = 'SELECT Name';
        
        /* If any additional field was passed, adding it to the soql */
        if (fieldList.size()>0) 
        {
            for (String s : fieldList) 
            {
                //soql += ', ' + s;
            }
        }
        
        /* Adding the object and filter by name to the soql */
        soql += ' from ' + obj + ' where name' + filter;
        if(!lstOfidstoFiler.isEmpty()){
            soql += 'and Id IN:lstOfidstoFiler and Contact_Status__c = \'Active\' ';
        }
        if(profilename!='')
        {
            //profile name and the System Administrator are allowed
            soql += ' and Profile.Name like \'%' + String.escapeSingleQuotes(profilename) + '%\'';
            system.debug('Profile:'+profilename+' and SOQL:'+soql);
        }
        
        
        if (fieldList != null) 
        {
            for (String s : fieldList) 
            {
                soql += ' or ' + s + filter;
            }
        }
        
        soql += ' order by Name limit 20';
        
        system.debug('Qry: '+soql);
        
        List<sObject> L = new List<sObject>();
        try 
        {   if(!lstOfidstoFiler.isEmpty()){
                L = Database.query(soql);
                system.debug('Query success'+L);
            }
        }
         catch (QueryException e) 
        {
            system.debug('Query Exception:'+e.getMessage());
            return null;
        }
        
        String jsonbody = JSON.serialize(L).escapeXML();  
        return jsonbody; //L;
   }
}