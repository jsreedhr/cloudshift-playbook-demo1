/**
 *  Class Name: cs_forecastTriggerHandler  
 *  Description: This is a Trigger Handler Class for trigger on Forecasting__c
 *  Company: dQuotient
 *  CreatedDate:26/10/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Nidheesh              26/10/2016                 Orginal Version
 *
 */
public class cs_forecastTriggerHandler {
    /**
     *  Method Name: beforeinsert 
     *  Description: Method to check wheather the there is only one forecast per month in every allocation before inserting forecast.'
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void beforeinsert(List<Forecasting__c> lstofNewforecast){
        set<Id> LstofAllocIds = new set<Id>();
        Map<Id,Allocation__c> mapofAllocation = new Map<Id,Allocation__c>();
        if(!lstofNewforecast.isEmpty()){
            for(Forecasting__c objfrct :lstofNewforecast ){
                if(objfrct.Allocation__c!= null){
                    LstofAllocIds.add(objfrct.Allocation__c);
                }    
            }
        }
        
        List<Allocation__c> lstofAllocation = new List<Allocation__c>();
        lstofAllocation = [SELECT Id,Name,(SELECT Id,CS_Forecast_Date__c,Allocation__c FROM Forecastings__r) FROM Allocation__c WHERE Id IN:LstofAllocIds];
        if(!lstofAllocation.isEmpty()){
            for(Allocation__c allocobj :lstofAllocation){
                mapofAllocation.put(allocobj.Id,allocobj);       
            }
        }
        System.debug('data'+mapofAllocation);
        if(!lstofNewforecast.isEmpty()){
            for(Forecasting__c objfc : lstofNewforecast){
                if(objfc.Allocation__c != null){
                    if(mapofAllocation.containsKey(objfc.Allocation__c)){
                        for(Forecasting__c obj: mapofAllocation.get(objfc.Allocation__c).Forecastings__r){
                            if(obj.CS_Forecast_Date__c != null && objfc.CS_Forecast_Date__c != null){
                                if((obj.CS_Forecast_Date__c.month()==objfc.CS_Forecast_Date__c.month()) && (obj.CS_Forecast_Date__c.year()==objfc.CS_Forecast_Date__c.year())){
                                  //  objfc.CS_Forecast_Date__c.addError('There can be only one Forecast for one month in Every Allocation');
                                }   
                            }
                        }
                    }
                }
            }    
        }
    }
     /**
     *  Method Name: beforeupdate 
     *  Description: Method to check wheather the there is only one forecast per month in every allocation before updating forecast.'
     *  Param:  Trigger.newMap,Trigger.OldMap.
     *  Return: None
    */
    Public static void beforeupdate(Map<Id,Forecasting__c> mapofNewforecast,Map<Id,Forecasting__c> mapofOldforecast){
        List<Forecasting__c> lsttoupdate = new List<Forecasting__c>();
        if(!mapofNewforecast.isEmpty()){
            for(Id objId :mapofNewforecast.keySet()){
                if(mapofNewforecast.get(objId).CS_Forecast_Date__c != mapofOldforecast.get(objId).CS_Forecast_Date__c){
                    lsttoupdate.add(mapofNewforecast.get(objId));  
                }    
            } 
        }
        beforeinsert(lsttoupdate);
    }
    
        /**
    *  Method Name: updateForecastAmount
    *  Description: Method to create Forecast Amount on Update
    *  Param:  Trigger.newMap, Trigger.oldMap
    *  Return: None
    */
    public static void updateForecastAmount(map < id, Forecasting__c > mapOldForecasting, map < id, Forecasting__c > mapNewForecasting) {

        map < id, Forecasting__c > mapIdInvoiceToUpdate = new map < id, Forecasting__c > ();
        for (Forecasting__c objForeCasting: mapNewForecasting.values()) {
            
            if (mapOldForecasting.get(objForeCasting.id).Invoiced_Amount__c != objForeCasting.Invoiced_Amount__c)  {
                
                decimal invoiceAmount = 0;
                decimal invoiceAmountPrevious = 0;
                decimal amountInvoice = 0;
                
                if(mapOldForecasting.get(objForeCasting.id).Invoiced_Amount__c  != null){
                    invoiceAmountPrevious = mapOldForecasting.get(objForeCasting.id).Invoiced_Amount__c ;
                }
                
                if(objForeCasting.Invoiced_Amount__c != null){
                    invoiceAmount = objForeCasting.Invoiced_Amount__c;
                }
                if(objForeCasting.Amount__c != null){
                    amountInvoice = objForeCasting.Amount__c;
                }
                amountInvoice = amountInvoice+invoiceAmountPrevious-invoiceAmount;
                objForeCasting.Amount__c = amountInvoice;
            } 

            if (mapOldForecasting.get(objForeCasting.id).Accured_Amount__c != objForeCasting.Accured_Amount__c)  {
                
                decimal accuralAmount = 0;
                decimal accuralAmountPrevious = 0;
                decimal amountAccural = 0;
                
                if(mapOldForecasting.get(objForeCasting.id).Accured_Amount__c  != null){
                    accuralAmountPrevious = mapOldForecasting.get(objForeCasting.id).Accured_Amount__c ;
                }
                
                if(objForeCasting.Accured_Amount__c != null){
                    accuralAmount = objForeCasting.Accured_Amount__c;
                }
                if(objForeCasting.Amount__c != null){
                    amountAccural = objForeCasting.Amount__c;
                }
                amountAccural = amountAccural+accuralAmountPrevious-accuralAmount;
                objForeCasting.Amount__c = amountAccural;
            }
            
            
        }
       // calculateForecastAmount(mapIdInvoiceToUpdate);
    }

    /**
    *  Method Name: insertForecastAmount 
    *  Description: Method to create Forecast Amount on insert
    *  Param:  Trigger.new
    *  Return: None
    */
    // public static void insertForecastAmount(List < Forecasting__c > lstForecasting) {

    //     map < id, Forecasting__c > mapIdInvoiceToUpdate = new map < id, Forecasting__c > ();
    //     for (Forecasting__c objForeCasting: lstForecasting) {
    //         if (objForeCasting.Invoiced_Amount__c != null && objForeCasting.Invoiced_Amount__c != 0) {
    //             decimal invoiceAmount = objForeCasting.Invoiced_Amount__c;
    //             decimal amountInvoice = 0;
    //             if(objForeCasting.Amount__c != null){
    //                 amountInvoice = objForeCasting.Amount__c;
    //             }
    //             amountInvoice = amountInvoice-invoiceAmount;
    //             objForeCasting.Amount__c = amountInvoice;
    //         }
            
    //         if (objForeCasting.Accured_Amount__c != null && objForeCasting.Accured_Amount__c != 0) {
    //             decimal accuredAmount = objForeCasting.Accured_Amount__c;
    //             decimal amountAccural = 0;
    //             if(objForeCasting.Amount__c != null){
    //                 amountAccural = objForeCasting.Amount__c;
    //             }
    //             amountAccural = amountAccural-accuredAmount;
    //             objForeCasting.Amount__c = amountAccural;
    //         }
    //     }
    //   //  calculateForecastAmount(mapIdInvoiceToUpdate);
    // }
    
}