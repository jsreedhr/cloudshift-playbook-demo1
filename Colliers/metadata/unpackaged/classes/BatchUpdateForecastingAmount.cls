/**
 *  Class Name: BatchUpdateForecastingAmount
 *  Description: Class to update the forecast amount of the current month
 *  Company: dQuotient
 *  CreatedDate:24-10-2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Jyoti             24-10-2016                 Orginal Version
 * 
 */
global class BatchUpdateForecastingAmount implements Database.Batchable<sObject>, Database.Stateful{
    public Database.SaveResult[] lstForecastUpdateDML;
    public Database.SaveResult[] lstForecastInsertDML;
    public List<Forecasting__c> lstForecastingUpdate;
    public List<Forecasting__c> lstForecastingInsert;
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        date firstofCurrentMonth = system.today().toStartOfMonth();
        date firstoflastMonth = system.today().toStartofMonth().addDays(-1).toStartOfMonth();
        
        String query='SELECT id,name,Amount__c,Invoiced_Amount__c,Allocation__c, Allocation__r.Job__c, Allocation__r.Job__r.isClosed,  Job__c,CS_Forecast_Date__c from Forecasting__c where CS_Forecast_Date__c < :firstofCurrentMonth  and CS_Forecast_Date__c >= :firstoflastMonth and Amount__c > 0 and Allocation__r.Job__r.isClosed != true order by CS_Forecast_Date__c Desc limit 50000';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Forecasting__c> scope){
        
        date firstofCurrentMonth = system.today().toStartOfMonth();
        integer monthSelected = system.today().month();   
        integer yearSelected =  System.Today().year();
        List<Allocation__c> lstAllocation = new List<Allocation__c>();
        map<id, List<Forecasting__c>> mapIdForecasting = new map<id, List<Forecasting__c>>();
        map<id, Forecasting__c> mapIdCurrentForecasting =  new map<id, Forecasting__c>();
        set<id> setAllocationIds = new set<id>();
        lstForecastingUpdate = new List<Forecasting__c>(); 
        lstForecastingInsert = new List<Forecasting__c>(); 
        System.debug('--Scope--->'+scope);
        
        for(Forecasting__c objForecasting: scope){
            
            setAllocationIds.add(objForecasting.Allocation__c);
            if(mapIdForecasting.containskey(objForecasting.Allocation__c)){
                mapIdForecasting.get(objForecasting.Allocation__c).add(objForecasting);
            }else{
                mapIdForecasting.put(objForecasting.Allocation__c, new List<Forecasting__c>{objForecasting});
            }
            
        }
        
        lstAllocation = [SELECT id,name,
                                Job__c, 
                                (Select 
                                id,name,
                                CS_Assigned_To__c,
                                Amount__c,
                                Allocation__c,
                                CS_Forecast_Date__c 
                                from 
                                Forecastings__r 
                                where 
                                CALENDAR_MONTH(CS_Forecast_Date__c)=:monthSelected 
                                AND CALENDAR_YEAR(CS_Forecast_Date__c)=:yearSelected  
                                Limit 1)
                                from Allocation__c 
                                where id in :setAllocationIds];
                                
        for(Allocation__c objAllocation : lstAllocation){
            
            if(!objAllocation.Forecastings__r.isEmpty()){
                mapIdCurrentForecasting.put(objAllocation.id,objAllocation.Forecastings__r[0]);
            }
        }
        System.debug('-mapIdCurrentForecasting---'+mapIdCurrentForecasting);
        for(Allocation__c objAllocation : lstAllocation){
            
            List<Forecasting__c> lstForecasting = new List<Forecasting__c>();
            lstForecasting = mapIdForecasting.get(objAllocation.id);
            
            Forecasting__c objForecasting = new Forecasting__c();
            decimal forecastAmount = 0.0;
            decimal extForecastAmount = 0.0;
            if(mapIdCurrentForecasting.containskey(objAllocation.id)){
                objForecasting = mapIdCurrentForecasting.get(objAllocation.id);
                objForecasting.Previous_Forecast_Amount__c =  objForecasting.Amount__c;
                extForecastAmount= objForecasting.Amount__c;
            }else{
                objForecasting.Allocation__c = objAllocation.id;
                objForecasting.CS_Forecast_Date__c = firstofCurrentMonth;
            }
            
            for(Forecasting__c objForecastingExt: lstForecasting){
                decimal forecastAmountExt = objForecastingExt.Amount__c;
                forecastAmount = forecastAmount+forecastAmountExt;
                objForecastingExt.Previous_Forecast_Amount__c =  objForecastingExt.Amount__c;
                objForecastingExt.Amount__c = 0;
                lstForecastingUpdate.add(objForecastingExt);
            }
            
            extForecastAmount = extForecastAmount+forecastAmount;
            objForecasting.Amount__c = extForecastAmount;
            if(objForecasting.id != null){
                lstForecastingUpdate.add(objForecasting);
            }else{
                lstForecastingInsert.add(objForecasting);
            }
        }
                                
        // try{
            // if(!lstForecastingUpdate.isEmpty()){
                // System.debug('-TheListtoUpdate---'+lstForecastingUpdate);
                // update lstForecastingUpdate;
                // checkRecursiveForecast.run = true;
            // }
            
            // if(!lstForecastingInsert.isEmpty()){
                // System.debug('-TheListtoUpdate---'+lstForecastingUpdate);
                // insert lstForecastingInsert;
            // }
        // }catch(exception e){
            // System.debug('----'+e.getMessage());
        // }
        checkRecursiveForecast.run = true;
        checkRecursiveAllocation.run = true;
        opportunityAllocTriggerHandler.checkrecursiveopp = true;
        
        if(!lstForecastingUpdate.isEmpty()){                       
           lstForecastUpdateDML = Database.update(lstForecastingUpdate, false);
            
        }
        checkRecursiveForecast.run = true;
        checkRecursiveAllocation.run = true;
        opportunityAllocTriggerHandler.checkrecursiveopp = true;
        if(!lstForecastingInsert.isEmpty()){
            lstForecastInsertDML = Database.insert(lstForecastingInsert, false);
            
        }
        
        List<Batch_exception__c> lstofbatchexception = new List<Batch_exception__c>();
        if(lstForecastUpdateDML != null && !lstForecastUpdateDML.isEmpty()){
            for(Integer i=0;i<lstForecastUpdateDML.size();i++){
                if(!lstForecastUpdateDML[i].isSuccess()){//only look at failures, or NOT Successes
                    Batch_exception__c newobject = new Batch_exception__c();
                    
                    newobject.Object_Name__c = 'Forecasting';
                    newobject.Record_Id__c = lstForecastingUpdate[i].Id;
                    newobject.Amount__c = lstForecastingUpdate[i].Amount__c;
                    newobject.Record_Name__c = lstForecastingUpdate[i].Name;
                
                    lstofbatchexception.add(newobject);
                }
                
            }
        }
        
        if(lstForecastInsertDML != null && !lstForecastInsertDML.isEmpty()){
            for(Integer i=0;i<lstForecastInsertDML.size();i++){
                if(!lstForecastInsertDML[i].isSuccess()){//only look at failures, or NOT Successes
                    Batch_exception__c newobject = new Batch_exception__c();
                    newobject.Object_Name__c = 'Forecasting';
                    newobject.Record_Id__c = lstForecastingInsert[i].Id;
                    newobject.Amount__c = lstForecastingInsert[i].Amount__c;
                    newobject.Record_Name__c = lstForecastingInsert[i].Name;
                    lstofbatchexception.add(newobject);
                }
                
            }
        }
        
        if(!lstofbatchexception.isEmpty()){
            try{
                insert lstofbatchexception;
            }catch(System.DmlException e){
                System.debug('---->'+e.getMessage());
            }
        }
        
        
        
    }
    global void finish(Database.BatchableContext BC){
        
      
        
    }
    
}