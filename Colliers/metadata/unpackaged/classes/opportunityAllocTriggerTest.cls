@isTest
private class opportunityAllocTriggerTest {
    
    
    static Invoice__c inv;
    static Invoice__c inv1;
    static List<Invoice__c> invList;
    static Allocation__c alloc;
    static Forecasting__c frcst;
    static Accrual__c acc;
    static Account a;
    static Account b;
    static Contact c;
    static opportunity opp;
    static opportunity oppr;
    static Opportunity opp1;
    public static opportunity careOfopty;
    public static Address__c addr;
    static List<Opportunity> opps1;
    static Disbursements__c Cost;
    static Staff__c stf;
    static Staff__c stf1;
    public static Contact invc;
    public static Account_Address_Junction__c accAddrJunc;
    static Invoice_Allocation_Junction__c invAll;
    static Invoice_Cost_Junction__c invCost;
    static Purchase_Order__c po;
    static AssignId__c asignId;
    static Invoice_Line_Item__c invLineItem;
    static List<Invoice_Line_Item__c> lstInvLineItem;
    static Purchase_Order_Invoice_Junction__c poInv;
    static List<Purchase_Order_Invoice_Junction__c> LstPoInv;
    static List<Invoice_Allocation_Junction__c> LstInvAll;
    static List<Purchase_Order__c> LstPo;
    static Invoice__c inve;
    static Invoice_PDF_Value__c invcPdf;
    static Property__c property;
    static Job_Property_Junction__c jobProp;
    static Job_Property_Junction__c jobProp1;
    static currency_Symbol__c currencyVal;
    static List<Invoice__c> invtlist;
    static Set<Id> StfIDs;
    static Coding_Structure__c cdst;
    static Work_Type__c wrktyp;
    static worktype_Job_Related_to__c wkJobRel;
    public static Care_Of__c careOf;
    public static Address__c invaddr;
    
     static testmethod void setupData(){
         //create currency custom setting
         currencyVal=new currency_Symbol__c(Name='Heyyaeh',currencyName__c='GBP');
         insert currencyVal;
         
        //create account
        a = TestObjectHelper.createAccount();
        a.Company_Status__c = 'Active';
        insert a ;
        //create account
        b = TestObjectHelper.createAccount();
        b.Company_Status__c = 'Active';
        insert b ;
        //create contact
        c = TestObjectHelper.createContact(a);
        c.Email ='test1@test1.com';
        insert c;
        
        careOf = TestObjectHelper.createCareOf(a,c);
        careOf.Colliers_Int__c = true;
        careOf.Reporting_Client__c = a.id;
        insert careOf;
        //create realted Opportunity
        stf = TestObjectHelper.createStaff();
        stf.Active__c = true;
        insert stf;
         //create worktype
        Work_Type__c workObj = new Work_Type__c();
        workObj.Name = 'Valuation - Redbook';
        workObj.Active__c = true;
        insert workObj;
        
        //create coding structure
        cdst = new Coding_Structure__c();
        cdst.Staff__c = stf.Id;
        cdst.Work_Type__c = workObj.Id;
        cdst.Department__c = 'Accurates';
        cdst.Office__c = 'London - West End';
        cdst.Status__c = 'Active';
        cdst.Department_Cost_Code_Centre__c = '620';
        insert cdst;
        
        //create worktype_Job_Related_to__c
        wkJobRel=new worktype_Job_Related_to__c(Name='rel wk',worktype_Name__c=workObj.Name);
        insert wkJobRel;
        
        
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunitys(a,stf, 10);
        insert opps;
   
        
        opp = [Select id from opportunity Limit 1];
        
         //create allocation
        alloc=TestObjectHelper.createAllocation(opp,stf);
        alloc.Coding_Structure__c = cdst.Id;
        alloc.Department_Allocation__c = 'Accurates';
        alloc.Office__c = 'London - West End';
        alloc.workType_Allocation__c=workObj.Name;
        alloc.Main_Allocation__c=true;
        insert alloc;
        
        invc = TestObjectHelper.createContact(a);
        invc.Email ='test2@test1.com';
        insert invc;
        //create forecast
        frcst=new Forecasting__c(Allocation__c=alloc.Id,Amount__c=100);
        //insert frcst;
       
        //create realted Opportunity
        List <Contact> cons = TestObjectHelper.createMultipleContact(a, 10);
        integer loopNo = 1;
        for(Contact objContact: cons){
            objContact.Email = 'test'+loopNo+'@test.com';
        }

        insert cons;
        property=TestObjectHelper.createProperty();
        property.Street_No__c ='Park Lane';
        property.Post_Code__c ='W1K 3DD';
        property.Country__c='United Kingdom';
        property.Geolocation__Latitude__s=51.51;
        property.Geolocation__Longitude__s=-0.15;
        insert property;
        
        //create invoice
        Inve = TestObjecthelper.createInvoice(opp,c);
        Inve.Status__c='Draft';
        Inve.Final_Invoice__c=true;
        insert Inve;
        
        invcPdf= TestObjectHelper.createInvoicePDFValue(stf);
        insert invcPdf; 
       //
        Schema.DescribeFieldResult fieldResult = Address__c.Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        addr = TestObjectHelper.createAddress(a);
        if(!ple.isEmpty()){
            addr.Country__c =ple[0].getValue();
        }
        insert addr;
        invaddr = TestObjectHelper.createAddress(a);
        if(!ple.isEmpty()){
            invaddr.Country__c =ple[0].getValue();
        }
        insert invaddr;
        accAddrJunc = TestObjectHelper.createAccountAddressJunction(a,addr);
        insert accAddrJunc;
        
       jobProp=TestObjectHelper.JobPrptyJn(opps[0],property);
       insert jobProp;
        jobProp1=TestObjectHelper.JobPrptyJn(opps[1],property);
       insert jobProp1;
        
        invList=new List<Invoice__c>();
        inv = TestObjectHelper.createInvoice(opps[0],c); 
        inv.Invoice_Address__c='This address is being given';
        invList.add(inv);
        inv1 = TestObjectHelper.createInvoice(opps[1],c); 
        inv1.Invoice_Address__c='This address is being given';
        invList.add(inv1);
         insert  invList;
        
        invLineItem = TestObjectHelper.createInvoiceLineItem(inv);
        insert invLineItem;
        System.assertNotEquals(invLineItem.id , null);
    }
    private static testMethod void test1() {
        
        setupData();
        Test.startTest();
        stf1=[select Id from Staff__c Limit 1];
        StfIDs=new Set<ID>();
        StfIDs.add(stf1.ID);
        opp1 = [Select id, Manager__c,Invoicing_Company2__c,Coding_Structure__r.Department__c,Amount,Coding_Structure__r.Office__c,Coding_Structure__r.Work_Type__r.name from Opportunity Limit 1];
        opp1.Name='new Opportunity';
        
        opps1=new List<opportunity>([Select id, Manager__c,Coding_Structure__r.Department__c,Amount, Coding_Structure__r.Office__c,Coding_Structure__r.Work_Type__r.name from Opportunity Limit 10]);
        //opportunityAllocTriggerHandler oppAllObj=new opportunityAllocTriggerHandler();
        opportunityAllocTriggerHandler.creatmainalloc(StfIDs,opps1,false);
        //opportunityAllocTriggerHandler.createmainforjoblite(StfIDs,opps1,true);
        opportunityAllocTriggerHandler.creatmainalloc(StfIDs,opps1,true);
        opportunityAllocTriggerHandler.checkrecursiveopp = true;
        opp1.Invoicing_Company2__c = b.Id;
        update opp1;
        //opportunityAllocTriggerHandler.createmainforjoblite(StfIDs,opps1,true);
        Test.stopTest();
    }
    private static testMethod void test2(){
        setupData();
        stf1=[select Id from Staff__c Limit 1];
        StfIDs=new Set<ID>();
        StfIDs.add(stf1.ID);
        oppr = TestObjectHelper.createOpportunity(a);
        oppr.AccountId = a.id;
        oppr.Name = 'Test';
        oppr.Date_Instructed__c = date.today();
        oppr.CloseDate = system.today().addDays(7);
        oppr.Amount = 8000;
        oppr.Instructing_Contact__c = c.id;
        oppr.InstructingCompanyAddress__c =addr.id;
        oppr.Invoicing_Company2__c =a.id;
        oppr.Invoice_Contact__c = c.id;
        oppr.Invoicing_Address__c = addr.id;
        oppr.Manager__c = stf.id;
        oppr.Coding_Structure__c = cdst.Id;
        oppr.Department__c = 'Accurates';
        oppr.Work_Type__c = 'Valuation - Redbook';
        oppr.Office__c = 'London - West End';
        oppr.Instructing_Company_Role__c ='Landlord';
        Test.startTest();
        system.debug('===========oppr====='+oppr);
        opportunityAllocTriggerHandler.checkrecursiveopp = true;
        insert oppr;
        oppr.Amount = 9000;
        opportunityAllocTriggerHandler.checkrecursiveopp = true;
        update oppr;
        List<Opportunity> lstofopp = new List<Opportunity>();
        lstofopp.add(oppr);
        opportunityAllocTriggerHandler oppAllObj=new opportunityAllocTriggerHandler();
        oppAllObj.updateaccoutID(lstofopp);
        oppAllObj.createmainforjoblite(StfIDs,lstofopp,true);
        careOfopty= TestObjectHelper.createOpportunity(a);
        careOfopty.AccountId = a.id;
        careOfopty.Name = 'Test';
        careOfopty.Coding_Structure__c = cdst.Id;
        careOfopty.CloseDate = system.today().addDays(7);
        careOfopty.Amount = 8000;
        careOfopty.Instructing_Contact__c = c.id;
        careOfopty.InstructingCompanyAddress__c =addr.id;
        careOfopty.Invoicing_Care_Of__c=careOf.id;
        careOfopty.Invoice_Contact__c = invc.id;
        careOfopty.Invoicing_Address__c = invaddr.id;
        careOfopty.Manager__c = stf.id;
        careOfopty.Department__c = 'Accurates';
        careOfopty.Work_Type__c = 'Valuation - Redbook';
        careOfopty.Office__c = 'London - West End';
        careOfopty.Instructing_Company_Role__c ='Landlord';
        
        insert careOfopty;
        Allocation__c alloc1 = new Allocation__c();
        alloc1=TestObjectHelper.createAllocation(careOfopty,stf);
        //alloc.Main_Allocation__c=true;
        insert alloc1;
        lstofopp = new List<Opportunity>();
        lstofopp.add(careOfopty);
        //opportunityAllocTriggerHandler oppAllObj=new opportunityAllocTriggerHandler();
        oppAllObj.updateaccoutID(lstofopp);
        //oppAllObj.creatmainalloc(StfIDs,lstofopp,true);
        Test.stopTest();
    }
    private static testMethod void test3(){
        setupData();
        stf1=[select Id from Staff__c Limit 1];
        StfIDs=new Set<ID>();
        StfIDs.add(stf1.ID);
        oppr = TestObjectHelper.createOpportunity(a);
        oppr.AccountId = a.id;
        oppr.Name = 'Test';
        oppr.Date_Instructed__c = date.today();
        oppr.CloseDate = system.today().addDays(7);
        oppr.Amount = 8000;
        oppr.Instructing_Contact__c = c.id;
        oppr.InstructingCompanyAddress__c =addr.id;
        oppr.Invoicing_Care_Of__c =careOf.id;
        oppr.Invoice_Contact__c = c.id;
        oppr.Invoicing_Address__c = addr.id;
        oppr.Manager__c = stf.id;
        oppr.Coding_Structure__c = cdst.Id;
        oppr.Department__c = 'Accurates';
        oppr.Work_Type__c = 'Valuation - Redbook';
        oppr.Office__c = 'London - West End';
        oppr.Instructing_Company_Role__c ='Landlord';
        Test.startTest();
        system.debug('===========oppr====='+oppr);
        opportunityAllocTriggerHandler.checkrecursiveopp = true;
        insert oppr;
        oppr.Amount = 9000;
        opportunityAllocTriggerHandler.checkrecursiveopp = true;
        update oppr;
        List<Opportunity> lstofopp = new List<Opportunity>();
        lstofopp.add(oppr);
        opportunityAllocTriggerHandler oppAllObj=new opportunityAllocTriggerHandler();
        oppAllObj.updateaccoutID(lstofopp);
        oppAllObj.createmainforjoblite(StfIDs,lstofopp,true);
        careOfopty= TestObjectHelper.createOpportunity(a);
        careOfopty.AccountId = a.id;
        careOfopty.Name = 'Test';
        careOfopty.Coding_Structure__c = cdst.Id;
        careOfopty.CloseDate = system.today().addDays(7);
        careOfopty.Amount = 8000;
        careOfopty.Instructing_Contact__c = c.id;
        careOfopty.InstructingCompanyAddress__c =addr.id;
        careOfopty.Invoicing_Care_Of__c=careOf.id;
        careOfopty.Invoice_Contact__c = invc.id;
        careOfopty.Invoicing_Address__c = invaddr.id;
        careOfopty.Manager__c = stf.id;
        careOfopty.Department__c = 'Accurates';
        careOfopty.Work_Type__c = 'Valuation - Redbook';
        careOfopty.Office__c = 'London - West End';
        careOfopty.Instructing_Company_Role__c ='Landlord';
        
        insert careOfopty;
        Allocation__c alloc1 = new Allocation__c();
        alloc1=TestObjectHelper.createAllocation(careOfopty,stf);
        //alloc.Main_Allocation__c=true;
        insert alloc1;
        lstofopp = new List<Opportunity>();
        lstofopp.add(careOfopty);
        //opportunityAllocTriggerHandler oppAllObj=new opportunityAllocTriggerHandler();
        oppAllObj.updateaccoutID(lstofopp);
        //oppAllObj.creatmainalloc(StfIDs,lstofopp,true);
        Test.stopTest();
    }
}