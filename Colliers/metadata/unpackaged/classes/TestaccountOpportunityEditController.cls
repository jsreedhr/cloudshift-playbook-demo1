@isTest 
public class TestaccountOpportunityEditController {
    @testsetup
    static void setupData(){
        //Create Custom setting data for opportunity
        ObjectFieldSetMapping__c ofs = new ObjectFieldSetMapping__c();
        ofs.Name = 'AccParent_OppChildFR';
        ofs.Object_API_Name__c = 'Opportunity';
        ofs.Field_Set_Name__c = 'OppFieldSetFR';
        ofs.ParentRecordType__c='';
        ofs.ParentObjectAPIName__c='Account';
        ofs.RecordType__c='';
        insert ofs;
        
         
        ObjectFieldSetMapping__c ofs2 = new ObjectFieldSetMapping__c();
        ofs2.Name = 'Opportunity';
        ofs2.Object_API_Name__c = 'Opportunity';
         ofs2.Field_Set_Name__c = 'OppFieldSetFR';
        ofs2.ParentObjectAPIName__c='';
        ofs2.RecordType__c='';
        insert ofs2;
        
        ObjectFieldSetMapping__c ofs3 = new ObjectFieldSetMapping__c();
        ofs3.Name = 'Opportunity1';
        ofs3.Object_API_Name__c = 'Opportunity_Service_Line__c';
         ofs3.Field_Set_Name__c = 'OppServiceLineFieldSetUK';
        ofs3.ParentObjectAPIName__c='Opportunity';
        ofs3.RecordType__c='';
        insert ofs3;
        
        ObjectRelatedListMapping__c ors=new ObjectRelatedListMapping__c();
        ors.Name='OpportunityUK';
        ors.ObjectAPIName__c='Opportunity';
        ors.RecordType__c='';
        ors.ChildObjectsString__c='Opportunity_Service_Line__c;opportunitycontactrole';
        insert ors;
        
        //create account
        Account a = TestObjectHelper.createAccount();
        insert a;
        
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
        System.debug('opps---->'+opps);
        //create realted Opportunity
        List <Contact> cons = TestObjectHelper.createMultipleContact(a, 10);
        integer loopNo = 1;
        for(Contact objContact: cons){
            objContact.Email = 'test'+loopNo+'@test.com';
        }

        insert cons;
        System.debug('cons---->'+cons);
    }
    
    static testMethod void testEditOverrideListPafeForOpps(){
        System.debug('opp test---->');
        Account a = [Select id,Opportunity_Name__c,Opportunity_Created_Date__c from Account Limit 1];
        system.debug('----->accccvalue'+a);
        DateTime dT = a.Opportunity_Created_Date__c;
        Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());

        Test.startTest();
        PageReference pf = new PageReference('apex/accountOpportunityEditPage?accid='+a.id+'&opp3='+a.Opportunity_Name__c+'&opp9=27/09/2016&opp11=Email Sent');
        Test.setCurrentPage(pf);
        accountOpportunityEditController con = new accountOpportunityEditController();
        con.selectedRelation='Opportunities';
        con.addRow();
        
        
    }
    static testMethod void testRelatedListPafeForContacts(){
        System.debug('con test---->');
        account a = [Select id,Opportunity_Name__c,Opportunity_Created_Date__c from Account Limit 1];
        DateTime dT = a.Opportunity_Created_Date__c;
        Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
        Test.startTest();
                PageReference pf1 = new PageReference('apex/accountOpportunityEditPage?accid='+a.id+'123&opp3='+a.Opportunity_Name__c+'&opp9=27/09/2016&opp11=Email Sent');

        PageReference pf = new PageReference('apex/accountOpportunityEditPage?accid='+a.id+'&opp3='+a.Opportunity_Name__c+'&opp9=27/09/2016&opp11=Email Sent');
        Test.setCurrentPage(pf);
        accountOpportunityEditController con = new accountOpportunityEditController();
        con.selectedRelation='OpportunityContactRoles';
        ApexPages.currentPage().getParameters().put('wrapperId','1' );
        con.primBool=true;
        con.selectPrimaryContact();
        //con.primBool=true;
        con.addRow();
        con.selectedRelation='Opportunity_Service_Lines__r';
        
         con.addRow();
         con.cancel();
         con.selectedRelation='Opportunity_Service_Lines__r';
          con.addRow();
         
        ApexPages.currentPage().getParameters().put('sobjectName','Opportunity_Service_Lines__r' );
        ApexPages.currentPage().getParameters().put('recordIndex','1' );
        con.deleteMethod();
        con.cancel();
       
         
        con.save();
        Test.stopTest();
        
    }
    static testMethod void testEditOverrideListPafeForOpps1(){
        System.debug('opp test---->');
        Account a = [Select id,Opportunity_Name__c,Opportunity_Created_Date__c from Account Limit 1];
        DateTime dT = a.Opportunity_Created_Date__c;
        Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
        Test.startTest();
                        PageReference pf1 = new PageReference('apex/accountOpportunityEditPage?accid= &opp9=27/09/2016&opp11=Email Sent');

                        PageReference pf2 = new PageReference('apex/accountOpportunityEditPage?accid=12345678&opp3='+a.Opportunity_Name__c+'&opp9=27/09/2016&opp11=Email Sent');

        PageReference pf = new PageReference('apex/accountOpportunityEditPage?accid='+a.id+'&opp3='+a.Opportunity_Name__c+'&opp9=27/09/2016&opp11=Email Sent');
        Test.setCurrentPage(pf);
        accountOpportunityEditController con = new accountOpportunityEditController();
        con.selectedRelation='Opportunities';
        con.addRow();
          con.save();
        Test.stopTest();
          
        
        
    }

    
    
}