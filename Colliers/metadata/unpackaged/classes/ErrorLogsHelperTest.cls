/**
*  Class Name: ErrorLogsHelperTest
*  Description: Test class for ErrorLogsHelper helper class that facilitates logging of any errors to custom object Error_Log__c.

*  Company: CloudShift
*  CreatedDate: 18/07/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  ----------------------------------------------------------- 
*  Kaspars Rezgalis		18/07/2018					Created
*
*/
@isTest(seeAllData=false)
private class ErrorLogsHelperTest{

	@isTest
	private static void singletonTest(){
		ErrorLogsHelper initialInstance = ErrorLogsHelper.getInstance();
		ErrorLogsHelper secondaryInstance = ErrorLogsHelper.getInstance();

		System.assert(
			initialInstance === secondaryInstance, 
			'The get instance method should always return the same instance of the ErrorLogsHelper'
		);
	}

	@isTest 
	private static void test_ListOfErrors(){
		List<String>errorMessagesForLog = new List<String>();
		errorMessagesForLog.add('CustomError1');
		errorMessagesForLog.add('Apex error 2');
		errorMessagesForLog.add('&*^% just custom error to assert multiple lines are added');

		Test.startTest();
			ErrorLogsHelper.getInstance().addListToErrorLog(errorMessagesForLog);
			ErrorLogsHelper.getInstance().setErrorLogType('Sharing Error');
			ErrorLogsHelper.getInstance().saveErrorLogs();
		Test.stopTest();

		List<Error_Log__c>errors = new List<Error_Log__c>([SELECT Details__c FROM Error_Log__c WHERE Log_Type__c='Sharing Error']);
		System.assertEquals(1, errors.size(), 'There should be one error log entry. Actual: ' + errors);
		for(String err : errorMessagesForLog){
			System.assert(errors[0].Details__c.contains(err), 'Error from the initial list should be added to error log record. Expected: ' + err + ' Actual: ' + errors[0].Details__c);
		}
	}

	@isTest 
	private static void test_StringErrors(){

		Test.startTest();
			ErrorLogsHelper errorLogging = ErrorLogsHelper.getInstance();
			errorLogging.addStringToErrorLog('My custom error');
			errorLogging.saveErrorLogs();
		Test.stopTest();

		List<Error_Log__c>errors = new List<Error_Log__c>([SELECT Details__c, Log_Type__c FROM Error_Log__c]);
		System.assertEquals(1, errors.size(), 'There should be one error log entry. Actual: ' + errors);
		System.assertEquals(errorLogging.errorLogType, errors[0].Log_Type__c, 'Log type for error entry should be set to default one (System Error).');
	}

	@isTest 
	private static void test_ExceptionToLog(){
		String expectedError = '';
		Test.startTest();
			try{
				Database.Insert(new Account(), true);
			}catch(Exception e){
				expectedError = e.getMessage();
				ErrorLogsHelper errorLogging = ErrorLogsHelper.getInstance();
				errorLogging.addExceptionToErrorLog(e);
				errorLogging.saveErrorLogs();
			}
		Test.stopTest();

		List<Error_Log__c>errors = new List<Error_Log__c>([SELECT Details__c, Log_Type__c FROM Error_Log__c]);
		System.assertEquals(1, errors.size(), 'There should be one error log entry. Actual: ' + errors);
		System.assert(errors[0].Details__c.contains(expectedError), 'Error log entry should contain exception message. Actual: ' + errors[0].Details__c);
	}
}