public class AccountContactRelationTriggerHandler {
   public static  List<AccountContactRelation> acrList = new List<AccountContactRelation>();
    
    public static void insertContactUltimateParentRelation(List<AccountContactRelation> listAcr){
        Set<id> acIdSet = new Set<id>();
        Set<id> conIdSet = new Set<id>();
        map<id,set<id>> mapAcr = new map<id,set<id>>();

        for(AccountContactRelation acr : listAcr){
            if(acr.ContactId != null && String.isNotBlank(acr.ContactId)){
                conIdSet.add(acr.ContactId);
            }
            if(acr.accountId != null && String.isNotBlank(acr.accountId)){
                acIdSet.add(acr.accountId);
            }
        }
        List<Contact> ContactList = [select id,name,accountid,account.parentid,(select contactid,AccountId from AccountContactRelations) from Contact where id =: conIdSet ];  
       // List<Contact> ContactList1 = [select id,name,accountid,account.parentid,(select contactid,AccountId from AccountContactRelations) from Contact where accountId =: acIdSet ];  
        
        
        for(Contact con : ContactList){
                               
                for(AccountContactRelation ac : con.AccountContactRelations){
                    if(mapAcr.containskey(ac.contactid)){
                        mapAcr.get(ac.contactid).add(ac.AccountId);
                    }
                    else{
                        mapAcr.put(ac.contactid, new set<Id>{ac.accountId});
                    }
                }
        }
        
        for(AccountContactRelation acr : listAcr){
            if(mapAcr.containsKey(acr.contactid)){
                mapAcr.get(acr.contactid).add(acr.accountid);
            }
            else{
                mapAcr.put(acr.contactid, new set<Id>{acr.accountId});
            }
        }
                        
        List<Account> listParentAccounts = [Select Id, name, ParentId,Parent.ParentId,Parent.Parent.ParentId,Parent.Parent.Parent.ParentId,Parent.Parent.Parent.Parent.ParentId from Account where Id In :acIdSet];
        system.debug('======listParentAccounts====='+listParentAccounts);
        // Create map of Id => parentId and Id => UltimateParentId
        Map<Id,Id> mapOfParentIds = new Map<Id,Id>();
        Map<Id,Id> mapOfUltimateParentIds = new Map<Id,Id>();
        if(!listParentAccounts.isEmpty())
        {
            for(Account acc : listParentAccounts)
            {
                system.debug('======acc====='+acc);
                system.debug('======acc====='+acc.ParentId); 
                if(acc.ParentId != null && String.isNotBlank(acc.ParentId))
                {
                    mapOfParentIds.put(acc.Id,acc.ParentId);
                    
                }
                if(acc.Parent.Parent.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.Parent.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.Parent.Parent.Parent.ParentId);
                }
                else if(acc.Parent.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.Parent.Parent.ParentId);
                }
                else if(acc.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.Parent.ParentId);
                }
                else if(acc.Parent.ParentId != null && String.isNotBlank(acc.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.ParentId);
                }
               
            }
        }
            
        for(AccountContactRelation acr : listAcr){
            system.debug('here====='+mapAcr.get(acr.contactid).contains(mapOfParentIds.get(acr.accountid)));
            system.debug('parent===='+mapOfParentIds.get(acr.accountid));
            
                if( mapOfParentIds.containsKey(acr.accountid) && !mapAcr.get(acr.contactid).contains(mapOfParentIds.get(acr.accountid)))
                {   system.debug('enter here1===>');

                    AccountContactRelation ac = new AccountContactRelation();
                    ac.AccountId =  mapOfParentIds.get(acr.accountid);
                    ac.ContactId = acr.contactid;
                    acrList.add(ac);
                    System.debug('ac--'+ac);
                }
                if( mapOfUltimateParentIds.containsKey(acr.accountId)&& !mapAcr.get(acr.contactid).contains(mapOfUltimateParentIds.get(acr.accountid)))
                {   system.debug('enter here2===>');
                    AccountContactRelation ac = new AccountContactRelation();
                    ac.AccountId = mapOfUltimateParentIds.get(acr.accountId);
                    ac.ContactId = acr.contactid;
                    acrList.add(ac);
                    System.debug('ac=='+ac);
                }
                
        }
        
            if(acrList!=NULL && !acrList.isEmpty()){
                insert acrList;
            }
        
        
        
        
        
    }
    
    public static boolean firstRun = true;
    public static boolean isFirstRun(){
        if(firstRun){
            firstRun = false;
            return true;
            
        }else{
            return firstRun;
        }
    }
    

}