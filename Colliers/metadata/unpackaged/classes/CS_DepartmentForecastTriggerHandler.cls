/**
 *  Class Name: CS_DepartmentForecastTriggerHandler
 *  Description: This is a Trigger Handler Class for trigger on Department_Forecast__c
 *  Company: dQuotient
 *  CreatedDate:01/02/2017 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Anand                01/02/2017                 Orginal Version
 *
 */
public class CS_DepartmentForecastTriggerHandler {

     
    /**
     *  Method Name: insertCompanyCostCentre
     *  Description: Method to update the Company and Cost Centre
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void insertCompanyCostCentre(List<Department_Forecast__c> lstDepartmentForecast){
        
        List<Department_Forecast__c> lstDepartmentForecastNew = new List<Department_Forecast__c>();
        
        for(Department_Forecast__c objDepartmentForecast: lstDepartmentForecast){
            
            if(!String.isBlank(objDepartmentForecast.Department__c)){
                lstDepartmentForecastNew.add(objDepartmentForecast);
            }
        }
        
        calculateCompanyCostCentre(lstDepartmentForecastNew);
    }
    
    /**
     *  Method Name: updateCompanyCostCentre
     *  Description: Method to update the Company and Cost Centre
     *  Param:  Trigger.oldMap, Trigger.newMap
     *  Return: None
    */
    public static void updateCompanyCostCentre(map<id,Department_Forecast__c> mapOldDepartmentForecast, map<id,Department_Forecast__c> mapNewDepartmentForecast){
        
        List<Department_Forecast__c> lstDepartmentForecastNew = new List<Department_Forecast__c>();
        
        for(Department_Forecast__c objDepartmentForecast: mapNewDepartmentForecast.values()){
            
            if(objDepartmentForecast.Department__c != mapOldDepartmentForecast.get(objDepartmentForecast.id).Department__c){
                lstDepartmentForecastNew.add(objDepartmentForecast);
            }
        }
        
        calculateCompanyCostCentre(lstDepartmentForecastNew);
    }
    
    /**
     *  Method Name: calculateCompanyCostCentre 
     *  Description: Method to update the Company and Cost Centre
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void calculateCompanyCostCentre(List<Department_Forecast__c> lstDepartmentForecast){
        
        map<String, String> mapDeptCompany = new map<String, String>();
        map<String, String> mapDeptCostCentre = new map<String, String>();
        List<Department__c> lstDepartment = new List<Department__c>();
        lstDepartment = Department__c.getall().values();
        
        for(Department__c objDept: lstDepartment){
            
            if(!String.isBlank(objDept.Department__c)){
                if(!String.isBlank(objDept.Company__c)){
                    mapDeptCompany.put(objDept.Department__c, objDept.Company__c);
                }
                
                if(!String.isBlank(objDept.Cost_Centre__c)){
                    mapDeptCostCentre.put(objDept.Department__c, objDept.Cost_Centre__c);
                }
            }
        }
        
        for(Department_Forecast__c objDepartmentForecast: lstDepartmentForecast){
            
            if(!String.isBlank(objDepartmentForecast.Department__c)){
                
                if(mapDeptCostCentre.containsKey(objDepartmentForecast.Department__c)){
                    String costCentre = mapDeptCostCentre.get(objDepartmentForecast.Department__c);
                    objDepartmentForecast.Department_Cost_Centre_Code__c = costCentre;
                }else{
                    objDepartmentForecast.Department_Cost_Centre_Code__c = null;
                }
                
                if(mapDeptCompany.containsKey(objDepartmentForecast.Department__c)){
                    String company = mapDeptCompany.get(objDepartmentForecast.Department__c);
                    objDepartmentForecast.Company__c = company;
                }else{
                    objDepartmentForecast.Company__c = null;
                }
                
            }
            
        }
        
    }
    
}