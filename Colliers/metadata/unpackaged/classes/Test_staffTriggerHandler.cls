/**
 *  Class Name: Test_staffTriggerHandler 
 *  Description: Test class for the cs_staffTriggerHandler
 *  Company: dQuotient
 *  CreatedDate: 29/03/2017 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nidheesh               29/03/2017                  Orginal Version          
 *
 */
@isTest
private class Test_staffTriggerHandler {
    static Account accountObj;
    static Opportunity oppObj;
    static Allocation__c allocObj;
    static Forecasting__c forecastObj;
    static Staff__c staffObj;
    static Coding_Structure__c codestructObj;
    static Coding_Structure__c codestructObjnew;
    
    static void createData(){
        Schema.DescribeFieldResult fieldResult = Coding_Structure__c.Department__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        Schema.DescribeFieldResult fieldResultofffice = Coding_Structure__c.Office__c.getDescribe();
        List<Schema.PicklistEntry> pleoffice = fieldResultofffice.getPicklistValues();
        staffobj = TestObjectHelper.createStaff();
        if(!ple.isEmpty()){
            staffobj.Department__c =  ple[0].getValue();    
        }
        if(!pleoffice.isEmpty()){
            staffobj.Office__c = pleoffice[0].getValue();    
        }
        
        staffobj.active__c = true;
        insert staffobj;
        
        Work_Type__c workObj = new Work_Type__c();
        workObj.Name = 'worktype';
        workObj.Active__c = true;
        insert workObj;
        codestructObj = new Coding_Structure__c();
        if(!ple.isEmpty()){
            codestructObj.Department__c = ple[0].getValue();    
        }
        if(!pleoffice.isEmpty()){
            codestructObj.Office__c = pleoffice[0].getValue();    
        }
        //
        //codestructObj.Department__c = 'DepartMent';
        codestructObj.Work_Type__c = workObj.Id;
        codestructObj.Staff__c = staffobj.Id;
        codestructObj.Status__c = 'Active';
        insert codestructObj;
        codestructObjnew = new Coding_Structure__c();
        if(!ple.isEmpty()){
            codestructObjnew.Department__c = ple[0].getValue();    
        }
        if(!pleoffice.isEmpty()){
            codestructObjnew.Office__c = pleoffice[0].getValue();    
        }
        //
        //codestructObj.Department__c = 'DepartMent';
        codestructObjnew.Work_Type__c = workObj.Id;
        codestructObjnew.Staff__c = staffobj.Id;
        codestructObjnew.Status__c = 'Active';
        insert codestructObjnew;
        Department__c deptcustom = new Department__c();
        deptcustom.Name = 'test';
        deptcustom.Company__c = '2600';
        if(!ple.isEmpty()){
            deptcustom.Department__c = ple[0].getValue();    
        }
        deptcustom.Cost_Centre__c ='620';
        insert deptcustom;
        
        
  }
  @isTest
  static void testmethod1()
  {
    
    createData();
    staffobj.Name = 'test1';
    staffobj.Active__c = false;
    try{
        update staffobj;
    }catch (DMLException e) {
        System.assert(e.getMessage().contains('There are still active Coding Structures associated with this Staff'));
    }
    
  }
  
}