global class BatchUpdateValidMLR implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'select id,Invoicing_Company2__c,Invoicing_Care_Of__c from opportunity where Invoicing_Company2__c != null or Invoicing_Care_Of__c != null ';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Opportunity> scope)
    {   
        Set<id> invoiceIdSet = new set<id>();
        Set<id> invoiceCareOfSet = new Set<id>();
        
        for(opportunity op : scope){
            if(op.Invoicing_Company2__c != null){
                
                invoiceIdSet.add(op.Invoicing_Company2__c); 
                
            }
            else if(op.Invoicing_Care_Of__c != null){
                invoiceCareOfSet.add(op.Invoicing_Care_Of__c);
            }
        }
        List<account> accList = new list<account>();
        List<Care_Of__c> careOfList =new list<Care_Of__c>();
        accList= [select id, MLR_Id__c, Valid_MLR__c,MLR_Id__r.MLR_Set_Date__c,MLR_Id__r.Expiry_Date__c  
                 from account where id =: invoiceIdSet ];
        careOfList= [select id, Reporting_Client__c,Reporting_Client__r.MLR_Id__c, Reporting_Client__r.MLR_Id__r.MLR_Set_Date__c,
                    Reporting_Client__r.MLR_Id__r.Expiry_Date__c,Reporting_Client__r.Valid_MLR__c  
                    from care_of__c where id =: invoiceCareOfSet ];
        map<id,care_of__c> careOfMap = new map<id, care_of__c>(careOfList);
        map <id, Account> acMap = new map<id,account>(accList);
        for(opportunity op : scope){
            if(op.Invoicing_Company2__c != null && acMap.containsKey(op.Invoicing_Company2__c)){
                
                 Account objAcc = acMap.get(op.Invoicing_Company2__c);
                op.Valid_MLR__c = objAcc.Valid_MLR__c;
                op.MLR_Set_Date__c =objAcc.MLR_Id__r.MLR_Set_Date__c;
                op.MLR_Expiry_Date__c = objAcc.MLR_Id__r.Expiry_Date__c;
				
            }
            else if(op.Invoicing_Care_Of__c != null && careOfMap.containsKey(op.Invoicing_Care_Of__c)){
                care_of__c objCareOf = careOfMap.get(op.Invoicing_Care_Of__c);
                op.Valid_MLR__c = objCareOf.Reporting_Client__r.Valid_MLR__c;
                op.MLR_Set_Date__c = objCareOf.Reporting_Client__r.MLR_Id__r.MLR_Set_Date__c;
                op.MLR_Expiry_Date__c = objCareOf.Reporting_Client__r.MLR_Id__r.Expiry_Date__c;
            }
            
        }
        
        update scope;
      
    }  
    global void finish(Database.BatchableContext BC)
    {
        
    }
}