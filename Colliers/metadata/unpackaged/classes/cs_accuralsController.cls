/* Class: cs_accuralsController
 * Created By : Jyoti Hiremath/10-10-2016
 * Purpose : Accurals Tab Functionality
 */
 
public with sharing class cs_accuralsController{

    public List<Allocation__c> AllocationList{get;set;} 
    public String oppId{get;set;}
    public Integer monthSelected{get;set;}
    public Integer yearSelected{get;set;}
    public integer detailWrpId{get;set;}
    public string recordId {get;set;}
    public String reason{get;set;}
    public Date accuredDate{get;set;}
    public Boolean insrt {get;set;}
    public ErrorHandling ObjModal{get;set;}
    public boolean accuralModal{get;set;}
    public Opportunity oppObject{get;set;}
    integer counter;
    public List<AllocationDetailsWrapper> AllocationWrapperLst {get;set;}
    public List<Accrual__c> AccrualHistoryLst {get;set;}
    Public AllocationDetailsWrapper AllocationWrapper{get;set;}
    
    //String to display the Error Messages
    public String errorMsg_reason{get;set;}
    public String errorMsg_month{get;set;}
    Public Accrual__c accural{get;set;}
    public boolean isactiveacrual{get;set;}
    public string accrualId{get;set;}
    
    public boolean stagestatus{get;set;}
    public boolean displayBtn{get;set;}
    public Accrual__c acc;
    // Constructor
    public cs_accuralsController() {
        initAccurals();
        isactiveacrual = true;
    }
    
    public void initAccurals(){
        acc = new Accrual__c();
        isactiveacrual = true;
        accuralModal =false;
        ObjModal=new ErrorHandling();
        AllocationWrapperLst = new List<AllocationDetailsWrapper>();
        AccrualHistoryLst = new List<Accrual__c>();
        AllocationWrapper = new AllocationDetailsWrapper();
        accural = new Accrual__c();
        displayBtn = false;
        oppId = ApexPages.currentPage().getParameters().get('id');
        //oppId='0068E000009DUhe';
        monthSelected = system.today().month();   
        yearSelected =System.Today().year();
        fetchAllocationList();
        //system.debug('AllocationWrapperLst  --->'+AllocationWrapperLst);
        Date firstDayOfMonth = System.today().toStartOfMonth();
        Date lastDayOfMonth = firstDayOfMonth.addDays(Date.daysInMonth(firstDayOfMonth.year(), firstDayOfMonth.month()) - 1);
        accural.Month_To_Accrue__c = lastDayOfMonth.addDays(1);
        oppObject = new Opportunity();
        oppObject = [SELECT Id, AccountId,isClosed,Account.Name, Name,Relates_To__c,StageName,Amount,Manager__c,Manager__r.Name,Coding_Structure__r.Status__c,Job_Number__c FROM Opportunity WHERE Id = : oppId];
        if(oppObject!=null){
            if(oppObject.Coding_Structure__r.Status__c != null && string.isNotBlank(oppObject.Coding_Structure__r.Status__c)){
                if(oppObject.Coding_Structure__r.Status__c == 'Active'){
                    isactiveacrual = true;
                }else{
                    isactiveacrual = false;
                }
                
            }else{
                isactiveacrual = false;
            }
            
        }else{
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.InvalidParamterError));
        }
        AccrualHistoryLst = [SELECT id,name,Amount__c,Status__c,Reversed__c ,Accrual_Amount__c from Accrual__c where Job__c=:oppId AND Amount__c>0];
      
    }
    
    /* Name: fetchAllocationList
     * Return Type : void
     * Description: fetch the List of Allocation associated for Opp and add to the wrapper
     */ 
    public void fetchAllocationList(){
        
        AllocationWrapperLst.clear();
        try{
            Opportunity jb = [SELECT id,Status__c,name,StageName, isClosed from Opportunity where id=:oppId ];
            if(jb.isClosed){
                  stagestatus = true;
                  //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'This Job is Closed.')); 
              }else{
                  
                   stagestatus = false;
              }
                    AllocationList = [SELECT id,name,workType_Allocation__c,Office__c,Department_Allocation__c,Complete__c,Job__r.Account.Name,Assigned_To__r.name,Assigned_To__r.Office__c,AccruedAmount__c,(Select id,name,CS_Assigned_To__c,Amount__c,CS_Forecast_Date__c from Forecastings__r where CALENDAR_MONTH(CS_Forecast_Date__c)=:monthSelected AND CALENDAR_YEAR(CS_Forecast_Date__c)=:yearSelected  AND Amount__c>:0 limit 1) from Allocation__c where Job__c=:oppId AND externalCompany__c=Null AND Complete__c =: false];
                    counter=0;
                    if(AllocationList.size()>0){
                        for(Allocation__c invObj :AllocationList){
                            if(invObj.Forecastings__r.size()>0){
                                counter++;
                                if(invObj ==AllocationList.get(0)){
                                   recordId = invObj.id;
                                   AllocationWrapperLst.add(new AllocationDetailsWrapper(true,invObj,counter));
                                }else{
                                   AllocationWrapperLst.add(new AllocationDetailsWrapper(true,invObj,counter));
                                } 
                            }
                         }
                     }else{
                     
                     }

                        
        }catch(exception e){
            //System.debug('Total Number of SOQL Queries allowed in this apex code context: ' +  Limits.getLimitQueries());
            ObjModal=new ErrorHandling(true,e.getMessage(),'Error'); 
        }
        
    }
    public void close_ErrorModal(){
        ObjModal=new ErrorHandling();
    }
    
    /* Name: checkRecord (Not Used Now)
     * Return Type : Void
     * Description: uncheck all the checkboxes if not selected by user
     */ 
    public void checkRecord(){
        try{
            for(AllocationDetailsWrapper obj :AllocationWrapperLst){
                if(obj.wrapperId== detailWrpId){
                    obj.isChecked =true;
                    recordId = obj.alloc.id;
                }
                else{
                     obj.isChecked= false;
                }
            }

        }catch(Exception e){
            ObjModal=new ErrorHandling(true,e.getMessage(),'Error'); 
                
        }  
    }
    
    public void validateAccural(){
        acc = new Accrual__c();
        insrt = true;
        Decimal amt = 0.0;
       
       
        List<Accrual__c> accrualLst = new List<Accrual__c>();
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        try{
            
            lstOpportunity = [SELECT id,Status__c,
                                            name,
                                            Invoicing_Company2__c, 
                                            Invoicing_Company2__r.Company_Status__c,
                                            Invoicing_Care_Of__c,
                                            Invoicing_Care_Of__r.Client__c,
                                            Invoicing_Care_Of__r.Client__r.Company_Status__c,
                                            isClosed,
                                            Coding_Structure__r.Status__c
                                            from 
                                            Opportunity 
                                            where id=:oppId Limit 1 ];
            
            accrualLst = [SELECT id,name,Amount__c,Job__c, Date_To_Bill__c, Accrual_Amount__c, Reversed__c from Accrual__c where Reversed__c=:false AND (Status__c='Approved' OR Status__c='Awaiting Approval') AND Amount__c>:0 AND Job__c=:oppId];
            system.debug('accrualLst-------------------------->'+accrualLst);
            boolean isErrorActive = false;
            if(!lstOpportunity.isEmpty()){
                if(isactiveacrual){
                    Opportunity objOpportunity = new Opportunity();
                    objOpportunity = lstOpportunity[0];
                    if(objOpportunity.Invoicing_Company2__c != null){
                        if(objOpportunity.Invoicing_Company2__r.Company_Status__c != 'Active'){
                            ObjModal.ErrorHandling(true,'Invoicing Company is not Active','Error',true);
                            insrt = false;
                            isErrorActive = true;
                        }
                    }else if(objOpportunity.Invoicing_Care_Of__c != null && objOpportunity.Invoicing_Care_Of__r.Client__c != null){
                        if(objOpportunity.Invoicing_Care_Of__r.Client__r.Company_Status__c != 'Active'){
                            ObjModal.ErrorHandling(true,'Care of Company is not Active','Error',true);
                            insrt = false;
                            isErrorActive = true;
                        }
                        
                    }else if(objOpportunity.Coding_Structure__r.Status__c != null && String.isNotBlank(objOpportunity.Coding_Structure__r.Status__c)){
                        if(objOpportunity.Coding_Structure__r.Status__c != 'Active'){
                            ObjModal.ErrorHandling(true,'Job Manager’s Coding Structure is Inactive – Please contact the System Administrator to change the job manager on the job','Error',true);
                            insrt = false;
                            isErrorActive = true;
                        }
                        ObjModal.ErrorHandling(true,'Job Manager’s Coding Structure is Inactive – Please contact the System Administrator to change the job manager on the job','Error',true);
                        insrt = false;
                        isErrorActive = true;
                    }
                }else{
                    ObjModal.ErrorHandling(true,'Job Manager’s Coding Structure is Inactive – Please contact the System Administrator to change the job manager on the job','Error',true);
                    insrt = false;
                    isErrorActive = true;
                }
                
            }
            if(!isErrorActive){
                if(accrualLst.size()>0){
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Accrual Record already exists for this job.')); 
                   // ObjModal=new ErrorHandling(true,e.getMessage(),'Error'); 
                    ObjModal.ErrorHandling(true,'Accrual Record already exists for this job.','Error',true);
                    insrt = false; 
                }else{
                    if(AllocationWrapperLst.size()>0){ 
                    for(AllocationDetailsWrapper obj : AllocationWrapperLst){
                        if(obj.isChecked== true){
                         amt = amt + obj.Forecast.Amount__c;
                        }
                        
                        if(!String.isBlank(accural.Reason__c)){
                           acc.Reason__c =accural.Reason__c;
                           errorMsg_reason = '';
                         }else{
                            errorMsg_reason ='Reason Is Required';
                            ObjModal.ErrorHandling(true,'Reason of Accural Is Required.','Error',true); 
                           insrt = false;
                         }
                         if(accural.Month_To_Accrue__c!=NULL){
                            acc.Month_To_Accrue__c= accural.Month_To_Accrue__c;
                            errorMsg_month='';
                            if(accural.Month_To_Accrue__c < system.today().addMonths(1).toStartOfMonth()){
                                errorMsg_month ='Date to bill cannot be earlier than the 1st of next month.'; 
                                ObjModal.ErrorHandling(true,'Date to bill cannot be earlier than the 1st of next month.','Error',true); 
                                insrt = false;
                            }else{
                                acc.Date_Accrued__c = accural.Month_To_Accrue__c;
                                errorMsg_month='';
                             }
                         }else{
                            errorMsg_month ='Date is required'; 
                            ObjModal.ErrorHandling(true,'Date is required','Error',true); 
                            insrt = false;
                         }
                         acc.Date_Requested__c = system.today();
                         acc.Job__c =String.valueOf(obj.alloc.Job__r.id).substring(0, 15);
                         acc.Status__c='Awaiting Approval';
                         acc.Amount__c = amt;
                         
                    }
                    }else{
                        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No Allocation Record to create a accrual.'));
                        ObjModal.ErrorHandling(true,'No Allocation Record to create a accrual.','Error',true); 
                        insrt = false;
                    }
                    if(insrt == true){
                        accuralModal = true;
                    }
                }
            }
        }catch(Exception e){
            ObjModal=new ErrorHandling(true,e.getMessage(),'Error'); 
        }
        
        
    }
    
    /* Name: save
     * Return Type : Void
     * Description: Save the Accural Record
     */ 
    public void save(){
        try{
             List<Accrual_Forecasting_Junction__c> insrtJunctionLst = new List<Accrual_Forecasting_Junction__c>();
            if(insrt == true){
                    if(accural.Is_Engaged_with_Client__c && accural.IsPrice__c && accural.IsDeliverySer__c){
                        acc.Is_Engaged_with_Client__c=accural.Is_Engaged_with_Client__c;
                        acc.IsPrice__c=accural.IsPrice__c;
                        acc.IsDeliverySer__c=accural.IsDeliverySer__c;
                        insert acc;
                        for(AllocationDetailsWrapper obj : AllocationWrapperLst){
                        Accrual_Forecasting_Junction__c accFrcst = new Accrual_Forecasting_Junction__c();
                        
                            if(obj.isChecked== true){
                               accFrcst.Accrual__c = acc.id;
                               accFrcst.Forecasting__c =obj.Forecast.id;
                               accFrcst.Amount__c = obj.Forecast.Amount__c; 
                                    
                               insrtJunctionLst.add(accFrcst);
                            }
                       
                        }
                        insert insrtJunctionLst;
                        accuralModal = false;
                        ObjModal=new ErrorHandling(true,'Accrual Record Saved Successfully.','Success'); 
                        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'Accrual Record Saved Successfully.'));
                        AccrualHistoryLst = [SELECT id,name,Amount__c,Reversed__c ,Status__c,Accrual_Amount__c from Accrual__c where Job__c=:oppId AND Amount__c>0];
                   
                     system.debug('AccrualHistoryLst-------------->'+AccrualHistoryLst);
                   }else{
                       ObjModal.ErrorHandling(true,'Please ensure you have required documentation in place prior to proceeding','Error',true); 
                   }
            }else{
                accuralModal = false;
            }
        }catch(Exception e){
            ObjModal=new ErrorHandling(true,e.getMessage(),'Error'); 
        }

    }
    
    /* Name: reverseAccrual
     * Return Type : pageReference
     * Description : Redirect to the ManageMyJob
     */ 
    public void reverseAccrual(){
       accural = new Accrual__c();
       accural.Id=accrualId;
       accural.Status__c = 'Reversed By User JM';
       accural.Reversed__c=true;
       update accural;
    }
    
    
    /* Name: cancel
     * Return Type : pageReference
     * Description : Redirect to the ManageMyJob
     */ 
    public PageReference cancel(){
       /*PageReference OpporunityPageCancel = new PageReference('/'+oppId );
       OpporunityPageCancel.setRedirect(true);
       return OpporunityPageCancel;*/
       initAccurals();
       accural = new Accrual__c();
       accural.Reason__c = '';
       errorMsg_reason = '';
       return null;
    }
    
    // Name : AllocationDetailsWrapper
    public class AllocationDetailsWrapper{
        public Boolean isChecked {get;set;}
        public Allocation__c alloc{get;set;}
        public integer wrapperId{get;set;}
        public Forecasting__c Forecast {get;set;}
    
        public AllocationDetailsWrapper(Boolean ck,Allocation__c all,Integer wrap) {
            isChecked = ck;
            alloc =all;    
            wrapperId=wrap;  

            if(all.Forecastings__r!=null&&all.Forecastings__r.size()>0)
                Forecast = all.Forecastings__r[0];
            else{
                Forecast = new Forecasting__c(); 
            }
        }
        //Wrapper Class Constructor
        public AllocationDetailsWrapper(){}
        
    }
    
   /**
     *  Method Name: getItems
     *  Description: Method to populate the Radio Button
     *  Param:  None
     *  Return: List<SelectOption>
    */ 
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('true','YES'));
        options.add(new SelectOption('false','NO'));
        return options;
    }
    
    public class ErrorHandling{
        public boolean displayerror{get;set;}
        public string Errormsg{get;set;}
        public string Title{get;set;}
        Public List<string> ErrorList{get;set;}
        Public set<string> Errorset{get;set;}
        public boolean hasmore{get;set;}
        public ErrorHandling(boolean displayerror, string Errormsg,string Title){
            this.displayerror =displayerror;
            this.Errormsg=Errormsg;
            this.Title=Title;
            this.hasmore=false;
            ErrorList = new List<string>();
            Errorset = new set<string>();
        }
        public  void ErrorHandling(boolean displayerror, string Errormsg,string Title,boolean hasmore){
            this.displayerror =displayerror;
            if(!Errorset.contains(Errormsg)){
                 ErrorList.add(Errormsg);
                 Errorset.add(Errormsg);
            }
          
            this.Title=Title;
            this.hasmore=hasmore;
        }
        public ErrorHandling(){
            this.displayerror =false;
            this.Errormsg='';
            ErrorList = new List<string>();
            Errorset = new set<String>();
        }
    }
}