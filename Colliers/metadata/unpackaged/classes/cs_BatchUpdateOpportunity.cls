global class cs_BatchUpdateOpportunity implements Database.Batchable<SObject>, Database.Stateful{
       
    
    List<Opportunity> listRecords = new List<Opportunity>();
   
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'select id,name,Job__c,Job__r.MLR_Required__c,Job__r.StageName from allocation__c where Coding_Structure__r.Work_Type__r.MLR__c = true and Job__r.amount > 7500 AND Job__r.MLR_Required__c = false  limit 5000000';
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<SObject> scope){
        set<id> jobIds = new set<Id>();
        for(Allocation__c obj : (Allocation__c[]) scope){
           if(obj.Job__r.MLR_Required__c == false ) {
                jobIds.add(obj.Job__c);
                
           }
          listRecords =   [select id,name from Opportunity where id IN:jobIds LIMIT 200];
     
            
         }
          if(!listRecords.isEmpty())
            {
              update listRecords;
            }
    
    }
    
    global void finish(Database.BatchableContext BC){
        system.debug('list to be deleted size  :: '+listRecords.size());
       
    }
}