@isTest
public with sharing class VALS_ShareDoCalloutController_Test
{
    @isTest(seealldata=True)
    private static void testMethod1()
    {
         RecordType  rec1= [Select id,name from RecordType where id='012240000006tgP'];
        system.debug('rec1--->'+rec1);
        Property__c propobj = New Property__c ();
        propobj.RecordTypeId = rec1.id;
        propobj.Name = 'Test Inspection';
        propobj.Property_Type__c = 'Bars';
        propobj.Geolocation__Latitude__s=51.51;
         propobj.Geolocation__Longitude__s=-0.15;
        propobj.Override_Experian_validation__c = TRUE;
        propobj.Property_Type__c = 'Bank';
        propobj.Town__c = 'tyu';
        insert propobj;
         system.debug('propobj--->'+propobj);
    	System.assert(propobj.id!=NULL);
        
      account accountObj = new account();    
      accountObj = TestObjectHelper.createAccount();
      accountObj.PO_Required__c = 'Yes';
      accountObj.Client_VAT_Number__c='123456';
      accountObj.Fax = '56893948920';
      accountObj.Phone = '57698900023';
      accountObj.Name = 'lkj';
     
      insert accountobj;
      System.assert(accountObj.id!=NULL);
         
      Property__c propertyObj2 = new Property__c();   
      propertyObj2=TestObjectHelper.createProperty();
      propertyObj2.Street_No__c ='Park Lane 2';
      propertyObj2.Post_Code__c ='W1K 3DD';
      propertyObj2.Country__c = 'United Kingdom';
      propertyObj2.Geolocation__Latitude__s=51.51;
      propertyObj2.Geolocation__Longitude__s=-0.15;
      propertyObj2.Override_Experian_validation__c=true;
      insert propertyObj2;
      System.assert(propertyObj2.id!=NULL);
      
      Address__c addressObj = new Address__c();
      addressObj = TestObjecthelper.createAddress(accountObj);
      addressObj.Country__c = 'United Kingdom';
      insert addressObj;
      System.assert(addressObj.id!=NULL);

      WorkType__c CustSetting2 = new WorkType__c();
      Custsetting2.name='TestworkType';
      insert CustSetting2;
      
      PropertyType__c Custsetting= new PropertyType__c();
      Custsetting.name='Test';
      insert CustSetting;
        
        Contact contactObj = new Contact(); 
      contactObj = TestObjecthelper.createContact(accountObj);
      contactObj.Email='Cersei@kingslanding.com';
      contactobj.LastName='Lannister';
      contactObj.HomePhone='12345678';
      contactObj.MobilePhone='12345678';
      contactObj.Phone='12345678';
      contactObj.Fax='12345678';
      insert contactObj;
      System.assert(contactObj.id!=NULL);
        
      Staff__c staff = new Staff__c();
      staff.Name = 'abc';
      insert staff; 
      System.assert(staff.id!=NULL);  
      
       opportunity oppObj = new opportunity();    
      oppObj = TestObjectHelper.createOpportunity(accountObj);
      oppObj.Sent_to_ShareDo__c=True;
      oppObj.StageName='Instructed';
      oppObj.Work_Type__C='TestworkType';
      oppObj.AccountId = accountObj.id;
      oppObj.InstructingCompanyAddress__c = addressObj.id;
      oppObj.Property_Address__c = propertyObj2.id;
      oppObj.Instructing_Contact__c = contactObj.id;
      oppObj.Manager__c = staff.id;
      oppObj.Work_Category__c = 'Residential';
      oppObj.Work_Purpose__c = 'NPL';
      oppObj.Description = 'for test';
      oppObj.Date_Instructed__c = system.today();
      insert oppObj;
      System.assert(oppObj.id!=NULL);
        
          
        Job_Property_Junction__c junction = new Job_Property_Junction__c();        
         junction.Job__c = oppobj.id;
         junction.Property__c = propertyObj2.id;
         insert junction;
        System.assert(junction.id!=NULL);
        
        Job_Property_Junction__c jun = new Job_Property_Junction__c();        
         jun.Job__c = oppobj.id;
         jun.Property__c = propertyObj2.id;
         insert jun;
        System.assert(jun.id!=NULL);
        
         PageReference pageRef = Page.Vals_sharedoPopUp;
    pageRef.getParameters().put('oppid',oppObj.id);
    Test.setCurrentPage(pageRef);

    VALS_ShareDoCalloutController cont= new VALS_ShareDoCalloutController ();
    cont.doCallout();
    }
    
    private static void testMethod2()
   {
         RecordType  rec2= [Select id,name from RecordType where id='012240000006tgP'];
        system.debug('rec2--->'+rec2);
        Property__c propobj1 = New Property__c ();
        propobj1.RecordTypeId = rec2.id;
        propobj1.Name = 'Test Inspection';
        propobj1.Property_Type__c = 'Bars';
        propobj1.Geolocation__Latitude__s=51.51;
         propobj1.Geolocation__Longitude__s=-0.15;
        propobj1.Override_Experian_validation__c = TRUE;
        propobj1.Property_Type__c = 'Bank';
        propobj1.Town__c = 'tyu';
        insert propobj1;
         system.debug('propobj1--->'+propobj1);
    	System.assert(propobj1.id!=NULL);
        
      account accountObj1 = new account();    
      accountObj1 = TestObjectHelper.createAccount();
      accountObj1.PO_Required__c = 'Yes';
      accountObj1.Client_VAT_Number__c='123486';
      accountObj1.Fax = '56893948920';
      accountObj1.Phone = '57698900023';
      accountObj1.Name = 'lkj';
     
      insert accountobj1;
      System.assert(accountObj1.id!=NULL);
         
      Property__c propertyObj3 = new Property__c();   
      propertyObj3=TestObjectHelper.createProperty();
      propertyObj3.Street_No__c ='Park Lane 2';
      propertyObj3.Post_Code__c ='W1K 3DD';
      propertyObj3.Country__c = 'United Kingdom';
      propertyObj3.Geolocation__Latitude__s=51.51;
      propertyObj3.Geolocation__Longitude__s=-0.15;
      propertyObj3.Override_Experian_validation__c=true;
      insert propertyObj3;
      System.assert(propertyObj3.id!=NULL);
      
      Address__c addressObj1 = new Address__c();
      addressObj1 = TestObjecthelper.createAddress(accountObj1);
      addressObj1.Country__c = 'United Kingdom';
      insert addressObj1;
      System.assert(addressObj1.id!=NULL);

      WorkType__c CustSetting5 = new WorkType__c();
      Custsetting5.name='Testwork';
      insert CustSetting5;
      
      PropertyType__c Custsetting1= new PropertyType__c();
      Custsetting1.name='Testxsc';
      insert CustSetting1;
        
        Contact contactObj1 = new Contact(); 
      contactObj1 = TestObjecthelper.createContact(accountObj1);
      contactObj1.Email='Cersei@kingslanding.com';
      contactobj1.LastName='Lannister';
      contactObj1.HomePhone='12345678';
      contactObj1.MobilePhone='12345678';
      contactObj1.Phone='12345678';
      contactObj1.Fax='12345678';
      insert contactObj1;
      System.assert(contactObj1.id!=NULL);
        
      Staff__c staff1 = new Staff__c();
      staff1.Name = 'abc';
      insert staff1; 
      System.assert(staff1.id!=NULL);  
      
            
        opportunity oppObj1 = new opportunity();    
      oppObj1 = TestObjectHelper.createOpportunity(accountObj1);
      oppObj1.Sent_to_ShareDo__c=True;
      oppObj1.StageName='Instructed';
      oppObj1.Work_Type__C='TestworkType';
      oppObj1.AccountId = accountObj1.id;
      oppObj1.InstructingCompanyAddress__c = addressObj1.id;
      oppObj1.Property_Address__c = propertyObj3.id;
      oppObj1.Instructing_Contact__c = contactObj1.id;
      oppObj1.Manager__c = staff1.id;
      oppObj1.Work_Category__c = 'Residential';
      oppObj1.Work_Purpose__c = 'NPL';
      oppObj1.Description = 'for test';
      oppObj1.Date_Instructed__c = system.today();
      insert oppObj1;
      System.assert(oppObj1.id!=NULL);
      
        Job_Property_Junction__c junction1 = new Job_Property_Junction__c();        
         junction1.Job__c = oppobj1.id;
         junction1.Property__c = propertyObj3.id;
         insert junction1;
        System.assert(junction1.id!=NULL);
        
        
        PageReference pageRef = Page.Vals_sharedoPopUp;
    pageRef.getParameters().put('oppid',oppObj1.id);
    Test.setCurrentPage(pageRef);

    VALS_ShareDoCalloutController cont1= new VALS_ShareDoCalloutController ();
    cont1.doCallout();
       
    }
   
}