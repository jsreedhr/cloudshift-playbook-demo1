/**
 *  Class Name: CS_BatchCreditNoteReporting
 *  Description: Class to update the fields of Fee Income Staging Table with Credit Note 
 *  Company: dQuotient
 *  CreatedDate:24-10-2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Anand             23-03-2016                 Orginal Version
 * 
 */
global class CS_BatchCreditNoteReporting implements Database.Batchable<sObject>, Database.Stateful{
    
    
    public CS_BatchCreditNoteReporting(){
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){

        String query = 'SELECT id,name, Invoice__c, Invoice__r.No_of_Records_with_No_Fee_Income_Credit__c, Status__c From Credit_Note__c where Status__c = \'Approved\' and Invoice__c != null and Invoice__r.No_of_Records_with_No_Fee_Income_Credit__c > 0 order by CreatedDate asc limit 50000000';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<Credit_Note__c> scope){
        
        List<Fee_Income_Staging_Table__c> lstFeeIncomeStagingTable = new List<Fee_Income_Staging_Table__c>();
        set<id> setInvoiceAllocationIds = new set<id>();
        set<id> setInvoiceds = new set<id>();
        List<Invoice_Allocation_Junction__c> lstInvoiceAllocationExisting = new List<Invoice_Allocation_Junction__c>();
        Map<id,Fee_Income_Staging_Table__c> mapIdFeeIncomeStagingTableExisting = new Map<id, Fee_Income_Staging_Table__c>();
        Map<id,Decimal> mapIdSubContractor= new Map<id, Decimal>();
        Map<id,Decimal> mapIdNetFee = new Map<id, Decimal>();
        Map<id,Decimal> mapIdJMAllocation = new Map<id, Decimal>();
        Map<id,Decimal> mapIdOtherAllocation = new Map<id, Decimal>();
        Map<id,id> mapInvoiceCreditNote = new Map<id, id>();
        
        
        // Population of the Client
        for(Credit_Note__c objCreditNote : scope){
            setInvoiceds.add(objCreditNote.Invoice__c);
            mapInvoiceCreditNote.put(objCreditNote.Invoice__c, objCreditNote.id);
        }
        
        lstInvoiceAllocationExisting = [Select id, name, 
                                            Allocation__c, 
                                            Invoice__c,
                                            Invoice__r.Opportunity__c, 
                                            Invoice__r.Opportunity__r.Manager__c,
                                            Invoice__r.Opportunity__r.AccountId,
                                            Invoice__r.Opportunity__r.Invoicing_Company2__c, 
                                            Invoice__r.Opportunity__r.Invoicing_Care_Of__c, 
                                            Invoice__r.Opportunity__r.Invoicing_Care_Of__r.Reporting_Client__c, 
                                            Invoice__r.Assigned_Invoice_Number__c,
                                            Allocation__r.externalCompany__c, 
                                            Allocation__r.Assigned_To__c,Amount__c,
                                            Forecasting__c, Forecasting__r.Allocation__c,  
                                            Forecasting__r.Allocation__r.externalCompany__c, 
                                            Forecasting__r.Allocation__r.Assigned_To__c, 
                                            Staging_Table_Record_Credit_Note__c,
                                            (Select id, name,
                                            Invoice_Allocation_Junction__c,
                                            Type__c
                                            From
                                            Fee_Income_Staging_Table__r
                                            where Type__c = 'Credit Note')
                                            From 
                                            Invoice_Allocation_Junction__c
                                            where  Invoice__c in :setInvoiceds
                                            Limit 10000];
        
           
        for(Invoice_Allocation_Junction__c objInvoiceAllocation : lstInvoiceAllocationExisting){
            decimal amountTemp = 0.0;
            id idJM = null;
            if(objInvoiceAllocation.Invoice__r.Opportunity__r.Manager__c != null){
                idJM = objInvoiceAllocation.Invoice__r.Opportunity__r.Manager__c;
            }
            if(objInvoiceAllocation.Amount__c != null){
                amountTemp = objInvoiceAllocation.Amount__c;
            }
            
            if(objInvoiceAllocation.Forecasting__r.Allocation__r.externalCompany__c != null){
                // Sub Less Contractor
                if(mapIdSubContractor.containsKey(objInvoiceAllocation.Invoice__c)){
                    decimal subcontractor = mapIdSubContractor.get(objInvoiceAllocation.Invoice__c);
                    subcontractor = subcontractor+amountTemp;
                    mapIdSubContractor.put(objInvoiceAllocation.Invoice__c, subcontractor);
                }else{
                    mapIdSubContractor.put(objInvoiceAllocation.Invoice__c, amountTemp);
                    
                }
                
            }else if(objInvoiceAllocation.Forecasting__r.Allocation__r.Assigned_To__c != null){
                // Net Fee
                
                if(mapIdNetFee.containsKey(objInvoiceAllocation.Invoice__c)){
                    decimal netFee = mapIdNetFee.get(objInvoiceAllocation.Invoice__c);
                    netFee = netFee+amountTemp;
                    mapIdNetFee.put(objInvoiceAllocation.Invoice__c, netFee);
                }else{
                    mapIdNetFee.put(objInvoiceAllocation.Invoice__c, amountTemp);
                    
                }
                
                if(objInvoiceAllocation.Forecasting__r.Allocation__r.Assigned_To__c == idJM){
                    // JM Allocation
                    if(mapIdJMAllocation.containsKey(objInvoiceAllocation.Invoice__c)){
                        decimal JMAllocation = mapIdJMAllocation.get(objInvoiceAllocation.Invoice__c);
                        JMAllocation = JMAllocation+amountTemp;
                        mapIdJMAllocation.put(objInvoiceAllocation.Invoice__c, JMAllocation);
                    }else{
                        mapIdJMAllocation.put(objInvoiceAllocation.Invoice__c, amountTemp);
                        
                    }
                }else{
                    // Non JM Allocation
                    if(mapIdOtherAllocation.containsKey(objInvoiceAllocation.Invoice__c)){
                        decimal otherAllocation = mapIdOtherAllocation.get(objInvoiceAllocation.Invoice__c);
                        otherAllocation = otherAllocation+amountTemp;
                        mapIdOtherAllocation.put(objInvoiceAllocation.Invoice__c, otherAllocation);
                    }else{
                        mapIdOtherAllocation.put(objInvoiceAllocation.Invoice__c, amountTemp);
                        
                    }
                }
            }
            
        }
        
        Set<Id> opportunityAccountIdSet = new Set<Id>();
        for(Invoice_Allocation_Junction__c objInvoiceAllocation : lstInvoiceAllocationExisting){
            
            
            System.debug('----'+objInvoiceAllocation.Staging_Table_Record_Credit_Note__c);
            System.debug('----'+objInvoiceAllocation.Fee_Income_Staging_Table__r.isEmpty());
            if(!(objInvoiceAllocation.Staging_Table_Record_Credit_Note__c) && objInvoiceAllocation.Fee_Income_Staging_Table__r.isEmpty()){
                
                decimal subContractor = 0.0;
                decimal netFee = 0.0;
                decimal JMAllocation = 0.0;
                decimal OtherAllocation = 0.0;
                id idJM = null;
                id idInvoicingCompany = null;
                id opportunityAccountId = null;
                String invoiceNum = '';
                
                if(!String.isBlank(objInvoiceAllocation.Invoice__r.Assigned_Invoice_Number__c)){
                    invoiceNum = objInvoiceAllocation.Invoice__r.Assigned_Invoice_Number__c;
                }
                
                if(objInvoiceAllocation.Invoice__r.Opportunity__r.Manager__c != null){
                    idJM = objInvoiceAllocation.Invoice__r.Opportunity__r.Manager__c;
                }
                
                if(objInvoiceAllocation.Invoice__r.Opportunity__r.Invoicing_Company2__c != null){
                    idInvoicingCompany = objInvoiceAllocation.Invoice__r.Opportunity__r.Invoicing_Company2__c;
                }else if(objInvoiceAllocation.Invoice__r.Opportunity__r.Invoicing_Care_Of__c != null && objInvoiceAllocation.Invoice__r.Opportunity__r.Invoicing_Care_Of__r.Reporting_Client__c != null){
                    idInvoicingCompany = objInvoiceAllocation.Invoice__r.Opportunity__r.Invoicing_Care_Of__r.Reporting_Client__c;
                    
                }
                // Adding instructing client
                if(objInvoiceAllocation.Invoice__r.Opportunity__r.AccountId != null){
                    opportunityAccountId = objInvoiceAllocation.Invoice__r.Opportunity__r.AccountId;
                    if(!opportunityAccountIdSet.contains(opportunityAccountId)){
                        opportunityAccountIdSet.add(opportunityAccountId);
                    }
                }
                
                if(mapIdSubContractor.containsKey(objInvoiceAllocation.Invoice__c)){
                    subContractor = mapIdSubContractor.get(objInvoiceAllocation.Invoice__c);
                }
                if(mapIdNetFee.containsKey(objInvoiceAllocation.Invoice__c)){
                    netFee = mapIdNetFee.get(objInvoiceAllocation.Invoice__c);
                }
                if(mapIdJMAllocation.containsKey(objInvoiceAllocation.Invoice__c)){
                    JMAllocation = mapIdJMAllocation.get(objInvoiceAllocation.Invoice__c);
                }
                if(mapIdOtherAllocation.containsKey(objInvoiceAllocation.Invoice__c)){
                    OtherAllocation = mapIdOtherAllocation.get(objInvoiceAllocation.Invoice__c);
                }
                Fee_Income_Staging_Table__c objFeeIncomeStagingTable = new Fee_Income_Staging_Table__c();
                
                // Fee Income Master Details
                objFeeIncomeStagingTable.Allocation__c = objInvoiceAllocation.Forecasting__r.Allocation__c;
                objFeeIncomeStagingTable.Client__c = idInvoicingCompany;
                //Value for the Instructing Client
                objFeeIncomeStagingTable.Instructing_Client__c = opportunityAccountId;
                objFeeIncomeStagingTable.Forecasting__c =  objInvoiceAllocation.Forecasting__c;
                objFeeIncomeStagingTable.Invoice__c = objInvoiceAllocation.Invoice__c;
                objFeeIncomeStagingTable.Credit_Note__c = mapInvoiceCreditNote.get(objInvoiceAllocation.Invoice__c);
                objFeeIncomeStagingTable.Job_Manager__c = idJM;
                objFeeIncomeStagingTable.Type__c = 'Credit Note';
                objFeeIncomeStagingTable.Invoice_Allocation_Junction__c = objInvoiceAllocation.id;
                objFeeIncomeStagingTable.Notes__c = invoiceNum;
                
                // Amount Calculations
                decimal amountTemp = 0.0;
                if(objInvoiceAllocation.Amount__c != null){
                    amountTemp = objInvoiceAllocation.Amount__c;
                }
                objFeeIncomeStagingTable.Allocation_Net__c = -(amountTemp);
                objFeeIncomeStagingTable.Less_Sub_Contractor__c = subContractor;
                objFeeIncomeStagingTable.Net_Fees__c = -(netFee);
                objFeeIncomeStagingTable.Job_Manager_Allocation__c = -(JMAllocation);
                objFeeIncomeStagingTable.Other_Allocations__c = OtherAllocation;
                
                if(objInvoiceAllocation.Forecasting__r.Allocation__r.externalCompany__c != null){
                    objFeeIncomeStagingTable.External_Company__c = objInvoiceAllocation.Forecasting__r.Allocation__r.externalCompany__c;
                }else if(objInvoiceAllocation.Forecasting__r.Allocation__r.Assigned_To__c != null){
                    objFeeIncomeStagingTable.Fee_Allocated_To__c = objInvoiceAllocation.Forecasting__r.Allocation__r.Assigned_To__c;
                }
                
                
                lstFeeIncomeStagingTable.add(objFeeIncomeStagingTable);
            
            }
            
        }
        // Adding instructing client fields
        Map<Id, Account> accountMap = new Map<Id, Account>([Select Id, Ultimate_Parent_Name__c, Ultimate_Parent__c, Company_Classification__c, Geographical_Client__c, Ultimate_Parent_Sector__c, Ultimate_Parent_Sub_Sector__c From Account Where Id IN :opportunityAccountIdSet]);
        system.debug('accountMap----->'+accountMap);
        if(!lstFeeIncomeStagingTable.isEmpty()){
            for(Fee_Income_Staging_Table__c objFeeIncomeStagingTable :lstFeeIncomeStagingTable)
            {
                if(objFeeIncomeStagingTable.Instructing_Client__c != null)
                {
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Name__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Name__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Name__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent__c != null) {
                        String textString= accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent__c;
                        Integer int1 = textString.indexOf('/');
                        objFeeIncomeStagingTable.Ultimate_Instructing_ParentId__c = textString.substring(int1 + 1,int1 + 16);
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sector__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Sector__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sector__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Geographical_Client__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Geo_Class__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Geographical_Client__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sub_Sector__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Sub_Sector__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sub_Sector__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c = null;
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Developer' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Developer,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Lender' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Lender,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Occupier' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Occupier,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Owner' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Owner,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Service Provider' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Service Provider';
                        }
                        
                    }
                }
            }
        }
        if(!lstFeeIncomeStagingTable.isEmpty()){
            Database.SaveResult[] lstDatabaseSaveResultInsert = Database.insert(lstFeeIncomeStagingTable, false);
            
            integer index = 0;
            List<Invoice_Allocation_Junction__c> lstInvoiceAllocation = new List<Invoice_Allocation_Junction__c>();
            for (Database.SaveResult objDatabaseSaveResult : lstDatabaseSaveResultInsert) {
                if (objDatabaseSaveResult.isSuccess() && lstFeeIncomeStagingTable[index] != null 
                    && lstFeeIncomeStagingTable[index].Invoice_Allocation_Junction__c != null) {
                    
                    Invoice_Allocation_Junction__c objInvoiceAllocation = new Invoice_Allocation_Junction__c();
                    objInvoiceAllocation.id = lstFeeIncomeStagingTable[index].Invoice_Allocation_Junction__c ;
                    objInvoiceAllocation.Staging_Table_Record_Credit_Note__c = true;
                    lstInvoiceAllocation.add(objInvoiceAllocation);
                }
                
                index = index+1;
            }
            
            if(!lstInvoiceAllocation.isEmpty()){
                Database.SaveResult[] lstDatabaseSaveResultUpdate = Database.update(lstInvoiceAllocation, false);
            }
        }
        
        
    }
    global void finish(Database.BatchableContext BC){
        
    }
    
}