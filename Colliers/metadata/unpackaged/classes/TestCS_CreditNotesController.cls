@isTest(SeeAllData=false)
public class TestCS_CreditNotesController {
    static Account a;
    static Contact c;
    static opportunity opp;
    static Invoice__c inv;
    static Credit_Note__c cd;
    static Credit_Note__c cdNt;
    static List<Invoice__c> lstInv;
    static Property__c property;
    static Job_Property_Junction__c jobProp;
    static Job_Property_Junction__c jobProp1;
     static User userObj;
    static Staff__c staffObj;
    static Allocation__c allocationObj;
    static Property__c propertyObj;
    static Purchase_Order__c purOrderObj;
    static Job_Property_Junction__c jobPropObj;
    static Forecasting__c forecastObj;
    static Forecasting__c forecastObj1;
    static Disbursements__c disbursementObj;

   // @testsetup
    static testmethod void setupData(){
        //create account
        a = TestObjectHelper.createAccount();
        insert a ;
        
        //create contact
        c = TestObjectHelper.createContact(a);
        c.Email ='test1@test1.com';
        insert c;
        User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       // Insert account as current user
        System.runAs (currentUser) {
			// create user
			userObj = TestObjectHelper.createAdminUser(true);
			userobj.UserName ='Test_123@dquotient.com';
			insert userObj;
			System.assert(userObj.id!=NULL);
        }
        /*
        userObj = TestObjectHelper.createAdminUser(true);
        userobj.UserName ='Test_123@dquotient.com';
        insert userObj;
        System.assert(userObj.id!=NULL);
		*/
        staffObj = TestObjecthelper.createStaff();
        staffObj.User__c = userObj.id;
        staffObj.Active__c = true;
        insert staffObj;
        Work_Type__c workObj = new Work_Type__c();
        workObj.Name = 'Valuation - Redbook';
        workObj.Active__c = true;
        insert workObj;
        Coding_Structure__c codobj = new Coding_Structure__c();
        codobj.Staff__c = staffObj.Id;
        codobj.Work_Type__c = workObj.Id;
        codobj.Department__c = 'Accurates';
        codobj.Office__c = 'London - West End';
        codobj.Status__c = 'Active';
        codobj.Department_Cost_Code_Centre__c = '620';
        insert codobj;
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        for(Opportunity objopp:opps){
            objopp.Coding_Structure__c = codobj.Id;
        }
        insert opps;
        
        //create realted property
        property=TestObjectHelper.createProperty();
        property.Street_No__c ='Park Lane';
        property.Post_Code__c ='W1K 3DD';
        property.Country__c='United Kingdom';
        property.Geolocation__Latitude__s=51.51;
        property.Geolocation__Longitude__s=-0.15;
        insert property;
        
        //create realted Opportunity
        List <Contact> cons = TestObjectHelper.createMultipleContact(a, 10);
        integer loopNo = 1;
        for(Contact objContact: cons){
            objContact.Email = 'test'+loopNo+'@test.com';
        }
        insert cons;
        //Create Invoice data for opportunity
        opp = [Select id from opportunity Limit 1];
        
        //create realted job-Property-Junction
        jobProp=TestObjectHelper.JobPrptyJn(opps[0],property);
        insert jobProp;
        jobProp1=TestObjectHelper.JobPrptyJn(opps[1],property);
        insert jobProp1;
        
        
        
        System.assert(staffObj.id!=NULL);
        allocationObj = TestObjectHelper.createAllocation(opp,staffObj);
        insert allocationObj;
        System.assert(allocationObj.id!=NULL);
        purOrderObj = TestObjecthelper.createPurchaseOrder(opp);
        purOrderObj.PO_Status__c = 'Original';
        purOrderObj.Amount__c =1000;
        purOrderObj.Used_Amount__c=500;
        insert purOrderObj;
        System.assert(purOrderObj.id!=NULL);
        forecastObj = TestObjecthelper.createForecast(allocationObj);
        forecastObj.CS_Forecast_Date__c = system.today().toStartOfMonth();
        insert forecastObj;
        forecastObj1 = TestObjecthelper.createForecast(allocationObj);
        forecastObj1.CS_Forecast_Date__c = system.today().addMonths(-1);
        forecastObj1.Amount__c = 50;
        insert forecastObj1;
        System.assert(forecastObj.id!=NULL);
        
        disbursementObj = TestObjecthelper.createDisbursement(opp);
        disbursementObj.Purchase_Cost__c = 1000;
        disbursementObj.Recharge_Cost__c = 1000;
        disbursementObj.Category__c ='Motor Car';
        disbursementObj.Created_For__c = staffObj.id;
        disbursementObj.Sub_Category__c='Damage';
        disbursementObj.Purchase_Date__c  = System.today();
        disbursementObj.Description__c = 'Tsts';
        insert disbursementObj;
        System.assert(disbursementObj.id!=NULL);
        
        
        
        inv = TestObjectHelper.createInvoice(opp,c); 
        insert inv;
        Invoice_Allocation_Junction__c invallo = new Invoice_Allocation_Junction__c();
        invallo.Allocation__c = allocationObj.Id;
        invallo.Forecasting__c = forecastObj1.Id;
        invallo.Invoice__c = inv.Id;
        invallo.Amount__c = 50;
        insert invallo;
        lstInv = TestObjectHelper.createMultipleInvoice(opp,c,10);
        insert lstInv;
        
        cd = TestObjectHelper.createCreditNote(inv);
        insert cd;    
        
        cdNt = new Credit_Note__c();
        cdNt.id = cd.id;
        cdNt.status__c = 'Approved';
        update cdNt;
        
    }  
        
    static testmethod void TestCreditNotesController(){
        setupData();
        opp = [Select id from opportunity Limit 1];
        
        List<Credit_Note__c> cLst = new List<Credit_Note__c>();
                
        Test.startTest();
        PageReference pf = new PageReference('/apex/cs_CreditNotes?id='+opp.Id);
        Test.setCurrentPage(pf);
        cs_creditNotesController ctrl = new cs_creditNotesController();
        cLst.add(cd);
        ctrl.initCreditNotes();
        ctrl.creditNotesLst = cLst;
        ctrl.detailWrpId =1;
        ctrl.errorMsg_cancelReasons = '';
        ctrl.errorMsg_newAmt ='';
        ctrl.errorMsg_reasonAmt='';
        ctrl.reissue=true;
        ctrl.creditNotesDetails=cd;
      //  ctrl.ErrorHandling(true,'yih','hd',false);
        ctrl.GenerateCreditNotePDf();
        ctrl.fetchInvoiceList();
        
        ctrl.cancel();
        cs_creditNotesController.ErrorHandling erorrhandle = new cs_creditNotesController.ErrorHandling(true,'yih','hd');
        cs_creditNotesController.CreditDetailsWrapper wrapp= new cs_creditNotesController.CreditDetailsWrapper(1,true,inv,cd,'test',2);
        ctrl.fetchInvoiceDetails();
        ctrl.save();
        ctrl.getItems();
        ctrl.fetchCreditNotesList();
        Test.stopTest();  
    }

 static testmethod void TestCreditNotestriggerhandler(){
        setupData();
        
                
        Test.startTest();
        PageReference pf = new PageReference('/apex/cs_CreditNotes?id='+opp.Id);
        Test.setCurrentPage(pf);
          cd = TestObjectHelper.createCreditNote(inv);
          checkRecursiveCreditNote.run = true;
        insert cd;    
        
        cdNt = new Credit_Note__c();
        cdNt.id = cd.id;
        cdNt.status__c = 'Approved';
        cdNt.Is_Invoice_Reissued__c = true;
        CS_CreditNoteTriggerHandler.isInsrt = false;
        checkRecursiveCreditNote.run = true;
       Invoice_Allocation_Junction__c invallo1 = TestObjecthelper.createInvoiceAllocationJunction(allocationObj,inv);
       invallo1.forecasting__c = forecastObj.id;
      Purchase_Order_Invoice_Junction__c poinv1 = TestObjecthelper.createPurchaseOrderInvoiceJunction( purOrderObj, inv);
      insert poinv1;
      Invoice_Cost_Junction__c incost1 = TestObjecthelper.createInvoiceCostJunction(disbursementObj, inv);
       insert invallo1;
       insert incost1;
      Invoice_Line_Item__c invlin = TestObjecthelper.createInvoiceLineItem(inv);
      insert invlin;
        update cdNt;
       Credit_Note__c  cd1 = TestObjectHelper.createCreditNote(inv);
           cd1.status__c = 'Rejected';
           checkRecursiveCreditNote.run = true;
        insert cd1;  
        
        
       
        Test.stopTest();  
    }
}