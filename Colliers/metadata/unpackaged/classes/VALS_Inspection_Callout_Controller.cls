public class VALS_Inspection_Callout_Controller {
public string jsonInspection;
public String displayMessage{get;set;}

String InsId;
public void doCallout()
{
Map<string,ShareDo_Credentials__c> shareDo = ShareDo_Credentials__c.getAll();

InsId = apexpages.currentpage().getparameters().get('oppid');
System.debug('InsId----->'+InsId);
String jsonData;
displayMessage = '';

InspectionWrapper InspWrapper;

Inspection__c  InsptObj = new Inspection__c();
list<External_Inspection__c> ExtIns = new list<External_Inspection__c>();
list<Internal_Inspection__c> IntIns = new list<Internal_Inspection__c>();
list<Surrounding__c> surIns = new list<Surrounding__c>();
List<Feeditem> feedItemList = New list<FeedItem>();
Map<Id,list<InspectionWrapper>> propertyInspectionMap = New Map<Id,list<InspectionWrapper>>();
List<Id> ExinsId = New List<Id>();
    List<Id> externalinspectionIdSet = New List<Id>();

List<Id> IntInsId = New List<Id>();
List<Id> imageIdList = New List<Id>();

InsptObj = [select id,name,Special_Requirements__c,Job_Name__c,Measurement_Required__c,Job__c,
        Job_Number__c,Inspecting_Surveyor__c,Property__c,Site_Contact__c,Site_Mobile__c,Site_Landline__c,
        Site_Email__c,Date_Time_of_Inspection__c,Is_Development__c,Is_Development_Land_Subtype__c,Mixed_Use__c,
        Mixed_Use_Subtype__c,Inspection_Type_Del__c,Count_of_external_inspection__c,No_of_internal_inspections__c,Sent_to_ShareDo__c,
          Property_address__c from Inspection__c  where id=:InsId LIMIT 1];

    
 if(InsptObj!=NULL && InsptObj.id!=NULL)
 {
   ExtIns =  [select id,Name,Inspection__c,Count_of_Internal_Inspection__c,Property_Notes__c,Extension_Notes__c,
                Is_Property_Extended__c,Wholly_Mainly__c,Partly__c,Other_Building_Construction__c,
                Other_Front_Elevation__c,Other_Other_Elevations__c  ,Other_Roof__c  ,Other_Covered_In__c,Other_Frames__c,Other_Staircases__c,Other_Parking__c,Other_Parking_Surface__c,
                Loading_Bays__c,Loading_Bays_RepValue__c,Other_Loading_Bays__c,Other_Defects_Wall__c,Other_Defects_Roof__c,Other_Defects_Windows__c,Other_Defects_Floor__c,Century__c,Decade__c,
                Wholly_MainlyFront__c,PartlyFront__c,Wholly_MainlyOtherEve__c,PartllyOtherEle__c,Wholly_MainlyRoof__c,PartllyRoof__c,
                Parking_Type__c,Parking_type_RepValue__c,No_Parking_Spaces__c,No_parking_spaces_RepValue__c,
                Wholly_MainlyCovered__c,PartlyConvered__c,Wholly_MainlyWindows__c,PartlyWindows__c,
                External_Areas__c, External_Area_Single_RepValue__c, External_Decoration_is__c,External_Wall_Defects__c,Floor_Defects__c,Roof_Defects__c,Window_Defects__c,
                External_Areas_multi__c, External_Area_Multi_RepValue__c, Wholly_MainlyFrames__c,PartlyFrames__c,Parking_Surface_Is__c,Parking_Surface_is_Partly__c,
                Wholly_MainlyStaircases__c,PartlyStaircases__c,Building_Construction_RepValue__c,Covered_in_RepValue__c,Frames_are_RepValue__c,Other_Elevations_are_RepValue__c,Parking_surface_RepValue__c,
                Staircases_RepValue__c,The_Front_Elevation_is_RepValue__c,The_Roof_is_RepValue__c,The_windows_are_Rep_Value__c,Construction_Year__c,Other_External_Areas__c,Other_Windows__c
                 from External_Inspection__c  where Inspection__c=:InsId ];
        
     surIns =    [select id,Accessibility__c,Asbestos_Management_Plan__c,Expansion_Land__c,Fire_Risk_Assessment_in_Place__c,Gas_Containers__c,General_Enquiries__c,Has_Access_to_Vault__c,
                    Has_Roof_Access__c,Inspection__c,Inspection_Limitations__c,Is_Established_Area__c,
                    Is_Site_Level__c,Is_Site_Secure__c,Japanese_Knotweed__c,Lake__c,Large_Trees__c,Neighboring_Occupiers__c,Occupancy__c,Occupier_Type__c,
                    Other__c,Other_1__c,Other_2__c,Other_3__c,Other_Marketing_Boards__c,Overhead_Power_Cables__c,Rail_track__c,River__c,Site_Access__c,
                    Site_Level_Subtype__c,Site_Level_To__c,Substation__c,Weather__c,Site_Level_RepValue__c,Location__c,Situation__c from Surrounding__c  where Inspection__c=:InsId];

            
            for(External_Inspection__c e : ExtIns)
            {   if(e.The_Front_Elevation_is_RepValue__c!=NULL && e.The_Front_Elevation_is_RepValue__c.length()>1)
                e.The_Front_Elevation_is_RepValue__c= toTitleCase(e.The_Front_Elevation_is_RepValue__c);
             if( e.Building_Construction_RepValue__c!=NULL && e.Building_Construction_RepValue__c.length()>1)
                 e.Building_Construction_RepValue__c= toTitleCase(e.Building_Construction_RepValue__c);
             if(e.Other_Building_Construction__c!=NULL && e.Other_Building_Construction__c.length()>1)
                 e.Other_Building_Construction__c= toTitleCase(e.Other_Building_Construction__c);
             if(e.Other_Front_Elevation__c!=NULL && e.Other_Front_Elevation__c.length()>1)
                 e.Other_Front_Elevation__c= toTitleCase(e.Other_Front_Elevation__c);
             if(e.Other_Elevations_are_RepValue__c!=NULL && e.Other_Elevations_are_RepValue__c.length()>1)
                 e.Other_Elevations_are_RepValue__c= toTitleCase(e.Other_Elevations_are_RepValue__c);
             if(e.Other_Other_Elevations__c!=NULL && e.Other_Other_Elevations__c.length()>1)
                 e.Other_Other_Elevations__c= toTitleCase(e.Other_Other_Elevations__c);
             if( e.The_Roof_is_RepValue__c!=NULL && e.The_Roof_is_RepValue__c.length()>1 )
                 e.The_Roof_is_RepValue__c= toTitleCase(e.The_Roof_is_RepValue__c);
             if( e.Staircases_RepValue__c!=NULL && e.Staircases_RepValue__c.length()>1)
                 e.Staircases_RepValue__c= toTitleCase(e.Staircases_RepValue__c);
             if( e.Other_Roof__c!=NULL && e.Other_Roof__c.length()>1)
                 e.Other_Roof__c= toTitleCase(e.Other_Roof__c);
             if( e.Covered_in_RepValue__c!=NULL && e.Covered_in_RepValue__c.length()>1)
                 e.Covered_in_RepValue__c= toTitleCase(e.Covered_in_RepValue__c);
             if( e.Other_Covered_In__c!=NULL && e.Other_Covered_In__c.length()>1)
                 e.Other_Covered_In__c= toTitleCase(e.Other_Covered_In__c);
             if(e.The_windows_are_Rep_Value__c!=NULL && e.The_windows_are_Rep_Value__c.length()>1  )
                 e.The_windows_are_Rep_Value__c= toTitleCase(e.The_windows_are_Rep_Value__c);
             if(e.Frames_are_RepValue__c!=NULL && e.Frames_are_RepValue__c.length()>1 )
                 e.Frames_are_RepValue__c= toTitleCase(e.Frames_are_RepValue__c);
             if( e.Other_Frames__c!=NULL && e.Other_Frames__c.length()>1)
                 e.Other_Frames__c= toTitleCase(e.Other_Frames__c);
             if( e.Other_Staircases__c!=NULL && e.Other_Staircases__c.length()>1)
                 e.Other_Staircases__c= toTitleCase(e.Other_Staircases__c);
             if( e.Parking_Type__c!=NULL && e.Parking_Type__c.length()>1 )
                 e.Parking_Type__c= toTitleCase(e.Parking_Type__c);
             if( e.Parking_type_RepValue__c!=NULL && e.Parking_type_RepValue__c.length()>1 )
                 e.Parking_type_RepValue__c= toTitleCase(e.Parking_type_RepValue__c);
             if( e.Other_Parking__c!=NULL && e.Other_Parking__c.length()>1)
                 e.Other_Parking__c= toTitleCase(e.Other_Parking__c);
             if( e.Parking_surface_RepValue__c!=NULL && e.Parking_surface_RepValue__c.length()>1 )
                 e.Parking_surface_RepValue__c= toTitleCase(e.Parking_surface_RepValue__c);
             if( e.Other_Parking_Surface__c!=NULL && e.Other_Parking_Surface__c.length()>1)
                 e.Other_Parking_Surface__c= toTitleCase(e.Other_Parking_Surface__c);
             if( e.Loading_Bays__c!=NULL && e.Loading_Bays__c.length()>1)
                 e.Loading_Bays__c= toTitleCase(e.Loading_Bays__c);
             if( e.Other_Loading_Bays__c!=NULL && e.Other_Loading_Bays__c.length()>1)
                 e.Other_Loading_Bays__c= toTitleCase(e.Other_Loading_Bays__c);
             if( e.Other_Defects_Wall__c!=NULL && e.Other_Defects_Wall__c.length()>1)
                 e.Other_Defects_Wall__c= toTitleCase(e.Other_Defects_Wall__c);
             if( e.Other_Defects_Roof__c!=NULL && e.Other_Defects_Roof__c.length()>1)
                 e.Other_Defects_Roof__c= toTitleCase(e.Other_Defects_Roof__c);
             if( e.The_windows_are_Rep_Value__c!=NULL && e.The_windows_are_Rep_Value__c.length()>1 )
                 e.The_windows_are_Rep_Value__c= toTitleCase(e.The_windows_are_Rep_Value__c);
             if( e.Other_Defects_Windows__c!=NULL && e.Other_Defects_Windows__c.length()>1)
                 e.Other_Defects_Windows__c= toTitleCase(e.Other_Defects_Windows__c);
             if( e.Other_Windows__c!=NULL && e.Other_Windows__c.length()>1)
                 e.Other_Windows__c= toTitleCase(e.Other_Windows__c);
             if( e.Other_Defects_Floor__c!=NULL && e.Other_Defects_Floor__c.length()>1)
                 e.Other_Defects_Floor__c= toTitleCase(e.Other_Defects_Floor__c);
             if( e.Construction_Year__c!=NULL && e.Construction_Year__c.length()>1)
                 e.Construction_Year__c= toTitleCase(e.Construction_Year__c);
             
              if( e.Wholly_MainlyFront__c!=NULL && e.Wholly_MainlyFront__c.length()>1)
                 e.Wholly_MainlyFront__c= toTitleCase(e.Wholly_MainlyFront__c);
				 
				   if( e.PartlyFront__c!=NULL && e.PartlyFront__c.length()>1)
                 e.PartlyFront__c= toTitleCase(e.PartlyFront__c);
				 
				   if( e.Wholly_MainlyOtherEve__c!=NULL && e.Wholly_MainlyOtherEve__c.length()>1)
                 e.Wholly_MainlyOtherEve__c= toTitleCase(e.Wholly_MainlyOtherEve__c);
                if( e.PartllyOtherEle__c!=NULL && e.PartllyOtherEle__c.length()>1)
                 e.PartllyOtherEle__c= toTitleCase(e.PartllyOtherEle__c);
				 
				 if( e.Wholly_MainlyRoof__c!=NULL && e.Wholly_MainlyRoof__c.length()>1)
                 e.Wholly_MainlyRoof__c= toTitleCase(e.Wholly_MainlyRoof__c);
				 if( e.PartllyRoof__c!=NULL && e.PartllyRoof__c.length()>1)
                 e.PartllyRoof__c= toTitleCase(e.PartllyRoof__c);
				 
				 if( e.Wholly_MainlyCovered__c!=NULL && e.Wholly_MainlyCovered__c.length()>1)
                 e.Wholly_MainlyCovered__c= toTitleCase(e.Wholly_MainlyCovered__c);
				 
				 if( e.PartlyConvered__c!=NULL && e.PartlyConvered__c.length()>1)
                 e.PartlyConvered__c= toTitleCase(e.PartlyConvered__c);
				 
				 if( e.Wholly_MainlyWindows__c!=NULL && e.Wholly_MainlyWindows__c.length()>1)
                 e.Wholly_MainlyWindows__c= toTitleCase(e.Wholly_MainlyWindows__c);
				 
				 if( e.PartlyWindows__c!=NULL && e.PartlyWindows__c.length()>1)
                 e.PartlyWindows__c= toTitleCase(e.PartlyWindows__c);
				 if( e.External_Decoration_is__c!=NULL && e.External_Decoration_is__c.length()>1)
                 e.External_Decoration_is__c= toTitleCase(e.External_Decoration_is__c);
				 
				 
				 if( e.External_Wall_Defects__c!=NULL && e.External_Wall_Defects__c.length()>1)
                 e.External_Wall_Defects__c= toTitleCase(e.External_Wall_Defects__c);
				 
				  if( e.Floor_Defects__c!=NULL && e.Floor_Defects__c.length()>1)
                 e.Floor_Defects__c= toTitleCase(e.Floor_Defects__c);
				 
				  if( e.Roof_Defects__c!=NULL && e.Roof_Defects__c.length()>1)
                 e.Roof_Defects__c= toTitleCase(e.Roof_Defects__c);
				 
				  if( e.Window_Defects__c!=NULL && e.Window_Defects__c.length()>1)
                 e.Window_Defects__c= toTitleCase(e.Window_Defects__c);
				 
				  if( e.External_Areas_multi__c!=NULL && e.External_Areas_multi__c.length()>1)
                 e.External_Areas_multi__c= toTitleCase(e.External_Areas_multi__c);
				 
				  if( e.Wholly_MainlyFrames__c!=NULL && e.Wholly_MainlyFrames__c.length()>1)
                 e.Wholly_MainlyFrames__c= toTitleCase(e.Wholly_MainlyFrames__c);
				 
				  if( e.PartlyFrames__c!=NULL && e.PartlyFrames__c.length()>1)
                 e.PartlyFrames__c= toTitleCase(e.PartlyFrames__c);
				 
				  if( e.Parking_Surface_Is__c!=NULL && e.Parking_Surface_Is__c.length()>1)
                 e.Parking_Surface_Is__c= toTitleCase(e.Parking_Surface_Is__c);
				 
				  if( e.Parking_Surface_is_Partly__c!=NULL && e.Parking_Surface_is_Partly__c.length()>1)
                 e.Parking_Surface_is_Partly__c= toTitleCase(e.Parking_Surface_is_Partly__c);	 
				 
				  if( e.Wholly_MainlyStaircases__c!=NULL && e.Wholly_MainlyStaircases__c.length()>1)
                 e.Wholly_MainlyStaircases__c= toTitleCase(e.Wholly_MainlyStaircases__c);	 
				 
				  if( e.PartlyStaircases__c!=NULL && e.PartlyStaircases__c.length()>1)
                 e.PartlyStaircases__c= toTitleCase(e.PartlyStaircases__c);	 
				 
				 
             
             externalinspectionIdSet.add(e.id);
            }
            
    if(ExtIns!=NULL && ExinsId.size()>0)
        system.debug('ExtIns----->'+ExtIns);
    {
       
           
        IntIns = [select id,Name,External_Inspection__c
        ,Wholly_Mainly__c,PartlyCeiling__c,Floor_Ceiling_Height__c,Configuration__c,Number_Of_Lifts__c,No_Of_Person_Lifts__c,Decoration__c,Hazardous_Materials__c,Electricity__c,
        Gas__c,Water__c,Mains_Drainage__c,Hot_Water__c,Wholly_MainlyInternal__c,PartlyInternal__c,
        Wholly_MainlyConstruction__c,PartlyConstruction__c,Wholly_MainlyCovering__c,PartlyCovering__c,Wholly_MainlyStair__c,PartlyStair__c
        ,Wholly_MainlyAncillaryStaircases__c,Ancillary_Staircases_Partly__c,
        Other_Ceilings__c, Rooflights_Other__c,Other_InternaWalls__c,Other_LightFixings__c,Other_LightType__c,Other_Floor_Costruction__c,Other_Floor_Covering__c,Other_Main_Staircase__c,
        Other_Ancillary_Staircase__c,Other_Heating__c,Other_Services__c,Other_Office_Type__c,Other_Defects_Ceiling__c,Other_Defects_Int_Walls__c,Other_Defects_Floors__c,
        Specification__c,Swimming_Pool__c,Lighting_type_Entirely_Mainly__c,Lighting_Type_Partly__c,LightPartlyFixing__c,How_Many_Bathrooms__c,
        Internal_Wall_Defects__c,Kitchen_Specification__c,Gym__c,Has_the_tenant_s_made_improvements__c,Floor_Defects__c,Bathroom_Specification__c,
        Capacity__c,Ceiling_Defects__c,Clear_eaves_height_m__c,Concierge__c,Services_Main__c,
        Air_Conditioning__c,Alarm_Fire_Smoke__c ,Alarm_Intruder__c,Office_Type__c,Light_Fixing_Wholly_Mainly__c,
        Roof_Lights_Wholly_Mainly__c,Wholly_MainlyHeating__c,PartlyHeating__c,Wholly_MainlyPowerDisribution__c,PartlyPowerDisribution__c,Ancillary_Staircases_RepValue__c,Construction_RepValue__c,
        Covering_RepValue__c, Heating_RepValue__c, Light_Fixing_RepValue__c, Lighting_Type_RepValue__c, Main_Staircases_RepValue__c,
        Power_Distribution_RepValue__c, The_Ceilings_are_RepValue__c, The_Internal_Walls_are_RepValue__c,Has_roof_lights__c,Other_Power_Distribution__c from Internal_Inspection__c where External_Inspection__c IN : externalinspectionIdSet];
        
        if(IntIns!=NULL && IntIns.size()>0)
        {
            for(Internal_Inspection__c intObj : IntIns)
            {
                
                    if( intObj.The_Ceilings_are_RepValue__c!=NULL && intObj.The_Ceilings_are_RepValue__c.length()>1 )
                    intObj.The_Ceilings_are_RepValue__c= toTitleCase(intObj.The_Ceilings_are_RepValue__c);
                 if( intObj.Other_Ceilings__c!=NULL && intObj.Other_Ceilings__c.length()>1 )
                     intObj.Other_Ceilings__c= toTitleCase(intObj.Other_Ceilings__c);
                 if(intObj.Light_Fixing_RepValue__c!=NULL && intObj.Light_Fixing_RepValue__c.length()>1 )
                     intObj.Light_Fixing_RepValue__c= toTitleCase(intObj.Light_Fixing_RepValue__c);
                 if( intObj.Other_LightFixings__c!=NULL && intObj.Other_LightFixings__c.length()>1 )
                     intObj.Other_LightFixings__c= toTitleCase(intObj.Other_LightFixings__c);
                 if(intObj.Lighting_Type_RepValue__c!=NULL && intObj.Lighting_Type_RepValue__c.length()>1 )
                     intObj.Lighting_Type_RepValue__c= toTitleCase(intObj.Lighting_Type_RepValue__c);
                 if( intObj.Other_LightType__c!=NULL && intObj.Other_LightType__c.length()>1 )
                     intObj.Other_LightType__c= toTitleCase(intObj.Other_LightType__c);
                 if(intObj.The_Internal_Walls_are_RepValue__c!=NULL && intObj.The_Internal_Walls_are_RepValue__c.length()>1)
                     intObj.The_Internal_Walls_are_RepValue__c= toTitleCase(intObj.The_Internal_Walls_are_RepValue__c);
                 if( intObj.Other_InternaWalls__c!=NULL && intObj.Other_InternaWalls__c.length()>1 )
                     intObj.Other_InternaWalls__c= toTitleCase(intObj.Other_InternaWalls__c);
                 if( intObj.Construction_RepValue__c!=NULL && intObj.Construction_RepValue__c.length()>1 )
                     intObj.Construction_RepValue__c= toTitleCase(intObj.Construction_RepValue__c);
                 if( intObj.Other_Floor_Costruction__c!=NULL && intObj.Other_Floor_Costruction__c.length()>1 )
                     intObj.Other_Floor_Costruction__c= toTitleCase(intObj.Other_Floor_Costruction__c);
                 if( intObj.Covering_RepValue__c!=NULL && intObj.Covering_RepValue__c.length()>1 )
                     intObj.Covering_RepValue__c= toTitleCase(intObj.Covering_RepValue__c);
                 if(intObj.Power_Distribution_RepValue__c!=NULL && intObj.Power_Distribution_RepValue__c.length()>1 )
                     intObj.Power_Distribution_RepValue__c= toTitleCase(intObj.Power_Distribution_RepValue__c);
                 if( intObj.Other_Floor_Covering__c!=NULL && intObj.Other_Floor_Covering__c.length()>1 )
                     intObj.Other_Floor_Covering__c= toTitleCase(intObj.Other_Floor_Covering__c);
                 if( intObj.Main_Staircases_RepValue__c!=NULL && intObj.Main_Staircases_RepValue__c.length()>1 )
                     intObj.Main_Staircases_RepValue__c= toTitleCase(intObj.Main_Staircases_RepValue__c);  
                 if(intObj.Ancillary_Staircases_RepValue__c!=NULL && intObj.Ancillary_Staircases_RepValue__c.length()>1 )
                     intObj.Ancillary_Staircases_RepValue__c= toTitleCase(intObj.Ancillary_Staircases_RepValue__c);
                 if( intObj.Other_Ancillary_Staircase__c!=NULL && intObj.Other_Ancillary_Staircase__c.length()>1 )
                     intObj.Other_Ancillary_Staircase__c= toTitleCase(intObj.Other_Ancillary_Staircase__c);
                 if(intObj.Bathroom_Specification__c!=NULL && intObj.Bathroom_Specification__c.length()>1 )
                     intObj.Bathroom_Specification__c= toTitleCase(intObj.Bathroom_Specification__c);
                 if(intObj.Kitchen_Specification__c!=NULL && intObj.Kitchen_Specification__c.length()>1  )
                     intObj.Kitchen_Specification__c= toTitleCase(intObj.Kitchen_Specification__c);
                 if( intObj.Other_Heating__c!=NULL && intObj.Other_Heating__c.length()>1 )
                     intObj.Other_Heating__c= toTitleCase(intObj.Other_Heating__c);
                 if( intObj.Heating_RepValue__c!=NULL && intObj.Heating_RepValue__c.length()>1 )
                     intObj.Heating_RepValue__c= toTitleCase(intObj.Heating_RepValue__c);
            
            
                IntInsId.add(intObj.Id);
            }
        }
    }
 system.debug('IntIns----->'+IntIns);
        
        feedItemList =[Select Id,ParentId,(SELECT RecordId, Title,type,Value FROM FeedAttachments),Type, 
                       Title, Body, CommentCount, LikeCount, LinkUrl, RelatedRecordId,parent.type From FeedItem where parentid =:InsId OR parentid IN :externalinspectionIdSet OR parentid IN :IntInsId];
        if(feedItemList!=NULL && feedItemList.size()>0)
        {
          for(Feeditem fi : feedItemList)
          {
            imageIdList.add(fi.id);
          }
          InspWrapper = New InspectionWrapper(InsptObj,ExtIns,IntIns,surIns,imageIdList);
        }
        else
          InspWrapper = New InspectionWrapper(InsptObj,ExtIns,IntIns,surIns);
          
if(InsptObj.Property__c!=NULL && InsptObj.Job__c!=NULL)
{
  jsonInspection=json.serialize(InspWrapper);
  system.debug('Le JSON: '+jsonInspection);
  
  //Encoding Api Key for authorization
  String apiKey = EncodingUtil.base64Encode(Blob.valueOf(shareDo.get('Sharedo Inspection').API_Key__c));
 // String authorizationHeader = 'bearer '+'ZTE0MWRmN2ItYjRlNy00NThjLWE3YTctMGYyNjc4MDNmY2M4';
  String authorizationHeader = 'bearer '+apiKey;
 
  //prod
  //'bearer '+'YWE1ZGZmY2EtMzgzZC00NmY5LThlMmQtNjY0NGVmMzQ5OTIx';
  //Changed to VNext
  //String authorizationHeader = 'bearer '+  'ZGM3NjU4NzYtYjA4YS00MjJkLTgzMmUtNzYzZGI3ODRhOTUy';

  
  String username = shareDo.get('Sharedo Inspection').ShareDo_Username__c;
  //String username = 'EU\\svc_sharedoAPI';
  String password = shareDo.get('Sharedo Inspection').ShareDo_Password__c;
  Blob headerValue = Blob.valueOf(username + ':' + password);
  String authorizationHeaderBasic = 'BASIC '/*EncodingUtil.base64Encode(headerValue)*/+'RVVcc3ZjX3NoYXJlZG9BUEk6U0ZQc2gxMjMh';

  Http http1 = new Http(); 
  HttpRequest req1 = new HttpRequest();
  req1.setHeader('Content-Type', 'application/json');
  req1.setHeader('Accept','application/json');
  req1.setHeader('Authorization', authorizationHeaderBasic);
  req1.setHeader('X-Authorization', authorizationHeader);
  if(InsptObj.Sent_to_ShareDo__c!=NULL && InsptObj.Sent_to_ShareDo__c==true)
    req1.setMethod('PUT');
  else  
    req1.setMethod('POST');
  req1.setEndpoint(shareDo.get('Sharedo Inspection').shareDoUrl__c+InsptObj.Job__c+'/salesforce/'+InsptObj.Property__c);
  req1.setBody(jsonInspection);
  System.debug('jsonInspection----->'+jsonInspection);
  req1.setTimeout(60000);
  req1.setHeader('Content-Length',String.valueof(req1.getBody().length()));
  
  System.Debug('Inspection EndPoint: '+shareDo.get('Sharedo Inspection').shareDoUrl__c+InsptObj.Job__c+'/salesforce'+InsptObj.Property__c);

  try
  {
    HTTPResponse res = http1.send(req1);
    
    Vals__c log   = new Vals__c();
    log.Json_Inspection__c = jsonInspection;
    insert log ;
    
    System.debug('log---------->' +log);
    System.debug('---'+res.getBody());
    if(res.getBody() != NULL){
      displayMessage = res.getBody()+' Status Code:'+res.getStatusCode();
      if(res.getStatusCode()==200 && InsptObj.Sent_to_ShareDo__c==false)
      {
        InsptObj.Sent_to_ShareDo__c=true;
        try{
          update InsptObj;
        }catch(Exception e1)
        {
          System.Debug('Error on Inspection Update: '+e1);
        }
      }
    }

  }catch(Exception e)
  {
    displayMessage = String.valueOf(e);
  }
}
else
{
  displayMessage = 'Cannot Perform call out, Inspection does not have a valid Property/Job linked to it';
}
}

}     
                  


public class InspectionWrapper
{
Public Inspection__c inspection;
Public List<External_Inspection__c> externalInspections;
Public List<Internal_Inspection__c> internalInspections;
Public List<Surrounding__c> surroundings;
Public List<Id> imageIdList;

public InspectionWrapper(Inspection__c insObj,List<External_Inspection__c> extInspList,List<Internal_Inspection__c>  intinspObj,List<Surrounding__c> surroundings)
{
if(insObj!=NULL)
    this.inspection = insObj;
if(extInspList!=Null && extInspList.size()>0)
    this.externalInspections = extInspList;
if(intinspObj!=NULL && intinspObj.size()>0)
    this.internalInspections = intinspObj;
if(surroundings!=NULL && surroundings.size()>0)
    this.surroundings = surroundings;
   
}
public InspectionWrapper(Inspection__c insObj,List<External_Inspection__c> extInspList,List<Internal_Inspection__c>  intinspObj,List<Surrounding__c> surroundings,List<ID> imageIdList )
{
if(insObj!=NULL)
    this.inspection = insObj;
if(extInspList!=Null && extInspList.size()>0)
    this.externalInspections = extInspList;
if(intinspObj!=NULL && intinspObj.size()>0)
    this.internalInspections = intinspObj;
if(surroundings!=NULL && surroundings.size()>0)
    this.surroundings = surroundings;
if(imageIdList!=NULL && imageIdList.size()>0)
  this.imageIdList = imageIdList;
}
}

public String toTitleCase(String phrase){
String newPhrase = phrase.toLowerCase();
newPhrase = newPhrase.substring(0,1).toUpperCase()+ newPhrase.substring(1);
return newPhrase;
}


}