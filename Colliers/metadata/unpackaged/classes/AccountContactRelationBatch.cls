global class AccountContactRelationBatch implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'select id,contactid,AccountId,account.ParentId,account.ultimateParentId__c from AccountContactRelation  where account.ParentId != null ';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<accountcontactrelation> scope)
    {   
        map<id,set<id>> mapAcr =new map<id,set<id>>();
        list<accountcontactrelation> acrList =new list<accountcontactrelation>();
        Set<Id> setAccIds = new Set<Id>();
		Set<Id> setConIds = new Set<Id>();
        for (accountcontactrelation ac : scope){
            if(ac.AccountId != null && String.isNotBlank(ac.AccountId))
            {
                setAccIds.add(ac.AccountId);
            }
			if(ac.contactid != null && String.isNotBlank(ac.contactId)){
				setConIds.add(ac.contactId);
			}
            
            
            
        }
        list<accountcontactrelation> acrListNew =[select id,contactid,AccountId,account.ParentId,account.ultimateParentId__c from AccountContactRelation  where contactId =: setConIds];
		for(accountcontactrelation ac : acrListNew){
			if(mapAcr.containskey(ac.contactid)){
                    mapAcr.get(ac.contactid).add(ac.AccountId);
            }
			else{
				mapAcr.put(ac.contactid, new set<Id>{ac.accountId});
            }
		}
		
		system.debug('=====mapacr'+mapAcr);
        List<Account> listParentAccounts = [Select Id,ParentId,Parent.ParentId,Parent.Parent.ParentId,Parent.Parent.Parent.ParentId,Parent.Parent.Parent.Parent.ParentId from Account where Id In :setAccIds];
        
        // Create map of Id => parentId and Id => UltimateParentId
        Map<Id,Id> mapOfParentIds = new Map<Id,Id>();
        Map<Id,Id> mapOfUltimateParentIds = new Map<Id,Id>();
        if(!listParentAccounts.isEmpty())
        {
            for(Account acc : listParentAccounts)
            {
                if(acc.ParentId != null && String.isNotBlank(acc.ParentId))
                {
                    mapOfParentIds.put(acc.Id,acc.ParentId);
                }
                if(acc.Parent.Parent.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.Parent.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.Parent.Parent.Parent.ParentId);
                }
                else if(acc.Parent.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.Parent.Parent.ParentId);
                }
                else if(acc.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.Parent.ParentId);
                }
                else if(acc.Parent.ParentId != null && String.isNotBlank(acc.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.ParentId);
                }
                else if(acc.ParentId != null && String.isNotBlank(acc.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.ParentId);
                }
            }
        }
        
        for(accountcontactrelation ac : scope){
				
			system.debug('map2===='+mapAcr.get(ac.contactid).contains(mapOfUltimateParentIds.get(ac.accountId)));	
         
			if(ac.Account.parentid != null){
                
                 if(ac.account.parentid != null && mapOfParentIds.containskey(ac.accountId) && !mapAcr.get(ac.contactid).contains(ac.account.parentid) && ac.account.parentId != mapOfUltimateParentIds.get(ac.accountId)){            
                    // Creating Account relation to Parent.
                    AccountContactRelation acr = new AccountContactRelation();
                    acr.AccountId =  ac.account.ParentId;
                    acr.ContactId = ac.contactid;
					system.debug('====ac2==='+acr);
                    acrList.add(acr);
                    mapAcr.get(ac.contactid).add(ac.Account.parentid);
                }
                
                if(mapOfUltimateParentIds.containskey(ac.accountId) && !mapAcr.get(ac.contactid).contains(mapOfUltimateParentIds.get(ac.accountId)))
                {
                    // Creating Account relation to ultimate parent.
                    AccountContactRelation acr = new AccountContactRelation();
                    acr.AccountId = mapOfUltimateParentIds.get(ac.accountId);
                    acr.ContactId = ac.contactid;
					system.debug('====ac==='+acr);
                    acrList.add(acr);
                    // Add the new ultimate ParentId to Map
                    mapAcr.get(ac.contactid).add(mapOfUltimateParentIds.get(ac.accountId));
                    
                }
				system.debug('map===='+mapAcr.get(ac.contactid).contains(ac.account.parentid));
           
               
                     
            }
        }
            
            
        

        if(acrList!=NULL && !acrList.isEmpty()){
            insert acrList;
        }
    }  
    global void finish(Database.BatchableContext BC)
    {
        
    }
}