/*
*  Class Name: CS_CostTriggerHandler  
*  Description: This is a Trigger Handler Class for trigger on Cost
*  Company: dQuotient
*  CreatedDate:23/12/2016 
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------  
*  Anand              25/01/2017                 Orginal Version
*  
*/
public with sharing class CS_CostTriggerHandler 
 {

    /**
    *  Method Name: setVatandInvoiceId
    *  Description: Method to ssetvat and invoice Id for invoiced costs.
    *  Param:  Trigger.oldmap, trigger.newmap
    *  Return: None
    */
    
   public static void setVatandInvoiceId(map < id, Disbursements__c > mapOldCost, map < id, Disbursements__c > mapNewCost)
     {
         set<id>costIdToUpdate = new set<id>();
         for(Disbursements__c objcost:mapNewCost.values())
         {
           if (mapOldCost.get(objcost.id).Status__c != objcost.Status__c && objcost.Status__c == 'Invoiced') 
           {
                costIdToUpdate.add(objcost.id);
            }
         }
         
         list<Invoice_Cost_Junction__c > lstinvoicecost = [select invoice__c, invoice__r.vat__c,Disbursement__c  from Invoice_Cost_Junction__c where Disbursement__c in:costIdToUpdate];
        
        map<id, Invoice_Cost_Junction__c> mapIdInvoicecost = new map<id, Invoice_Cost_Junction__c>();
        for(Invoice_Cost_Junction__c objInvoicecost: lstinvoiceCost)
        
        {
            mapIdInvoicecost.put(objInvoicecost.Disbursement__c,objInvoicecost);
        }
        
        
        for(Disbursements__c objcost: mapNewCost.values())
        {
            
            if(mapIdInvoicecost.containsKey(objcost.id))
            {
                if(mapIdInvoicecost.get(objcost.id).invoice__r.vat__c != null)
                {
                    if(objcost.VAT_Applicable__c == true)
                    objcost.vat_amount__c =(mapIdInvoicecost.get(objcost.id).invoice__r.vat__c/100)*objcost.Recharge_Cost__c;
                    if(objcost.VAT_Applicable__c == false)
                    objcost.vat_amount__c = 0;
                }
                
                if(mapIdInvoicecost.get(objcost.id).invoice__c != null)
                    objCost.invoiceid__c = mapIdInvoicecost.get(objcost.id).invoice__c;
                
            }
        }
    }         
   
 }