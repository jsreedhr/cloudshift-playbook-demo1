@isTest
private class Test_CS_otherpartyPageController{
    static Account accountObj;
    static Account accountObjother;
    static Opportunity oppObj;
    
    static testmethod void setupData(){
        //create account
        accountObj = TestObjectHelper.createAccount();
        accountObj.name = 'test';
        insert accountObj ;
        accountObjother = TestObjectHelper.createAccount();
        accountObjother.name = 'test Other Party';
        insert accountObjother ;
        oppObj = TestObjectHelper.createOpportunity(accountObj);
        insert oppObj;
        Other_Party__c otherparty = new Other_Party__c();
        otherparty.Company__c = accountObjother.Id;
        otherparty.Job__c = oppObj.Id;
        insert otherparty;
    }
   
    private static testMethod void test1() {
        setupData();
        Test.setCurrentPageReference(new PageReference('Page.OtherPartyPage'));
        System.currentPageReference().getParameters().put('id',oppObj.Id);
        
        
        
        Test.startTest();
        CS_otherpartyPageController cntr= new CS_otherpartyPageController();
        cntr.save();
        cntr.closeModal();
        Test.stopTest();


    }
}