public class opportunityAllocTriggerHandler {
    //private List<Opportunity> lstofOpportunity{get;set;}
    //private set<Id> setofstaffId{get;set;}
    public static boolean checkrecursiveopp  = true;
    public opportunityAllocTriggerHandler(){

    }

    public void updateinvoicingAddress(Map<Id,Opportunity> mapOldOpportunity, Map<Id,Opportunity> mapNewOpportunity){

        set<id> setOppIds = new set<id>();
        for(Opportunity objOpportunity: mapNewOpportunity.values()){

            if((objOpportunity.Instructing_Contact__c != mapOldOpportunity.get(objOpportunity.id).Instructing_Contact__c) ||
                    (objOpportunity.Invoicing_Company2__c != mapOldOpportunity.get(objOpportunity.id).Invoicing_Company2__c) ||
                    (objOpportunity.Invoicing_Care_Of__c != mapOldOpportunity.get(objOpportunity.id).Invoicing_Care_Of__c) ||
                    (objOpportunity.Invoice_Contact__c != mapOldOpportunity.get(objOpportunity.id).Invoice_Contact__c) ||
                    (objOpportunity.Invoicing_Address__c != mapOldOpportunity.get(objOpportunity.id).Invoicing_Address__c)){

                setOppIds.add(objOpportunity.id);

            }
        }

        if(!setOppIds.isEmpty()){

            List<Opportunity> lstOpportunity = new List<Opportunity>();
            List<Invoice__c> lstInvoiceToUpdate = new List<Invoice__c>();
            lstOpportunity = [Select id, name,
            Instructing_Contact__c,
            Invoicing_Company2__c,
            Invoicing_Care_Of__c,
            Invoice_Contact__c,
            Invoicing_Address_Formula__c,
            Invoicing_Address__c,
            (Select id,Name,
            Contact__c,
            Company__c,
            Care_Of_Information__c,
            Invoice_contact__c,
            Invoice_Address__c,
            Address__c
            From Invoice_History__r
            where Status__c = 'Draft')
            From
            Opportunity where id in: setOppIds];


            for(Opportunity objOpportunity : lstOpportunity){

                List<Invoice__c> lstInvoice = new List<Invoice__c>();
                if(!objOpportunity.Invoice_History__r.isEmpty()){

                    lstInvoice.addall(objOpportunity.Invoice_History__r);
                    for(Invoice__c objInvoice : lstInvoice){

                        objInvoice.Contact__c = objOpportunity.Instructing_Contact__c;
                        objInvoice.Company__c =objOpportunity.Invoicing_Company2__c;
                        objInvoice.Care_Of_Information__c=objOpportunity.Invoicing_Care_Of__c;
                        objInvoice.Invoice_contact__c = objOpportunity.Invoice_Contact__c;
                        objInvoice.Invoice_Address__c =  objOpportunity.Invoicing_Address_Formula__c;
                        objInvoice.Address__c =objOpportunity.Invoicing_Address__c;
                    }
                    lstInvoiceToUpdate.addall(lstInvoice);
                }
            }

            try{
                if(!lstInvoiceToUpdate.isEmpty()){
                    update lstInvoiceToUpdate;
                }
            }catch(exception e){
                System.debug('----->'+e.getMessage());
            }
        }

    }



    public void insertfunction(Map<Id,Opportunity> jobnewMap){
        
        List<Opportunity> lstofOpportunity = new List<Opportunity>();
        List<Opportunity> opplst = new List<Opportunity>();
        set<Id> setofstaffId = new set<Id>();
        for(Opportunity oppobj : jobnewMap.values()){
            if(oppobj.Manager__c != null && string.isNotBlank(oppobj.Manager__c)){
                lstofOpportunity.add(oppobj);
                setofstaffId.add(oppobj.Manager__c);
            }
        }
        boolean insertcheck = true;
        opplst = [SELECT Id,Manager__c,Amount,Coding_Structure__r.Department__c,Coding_Structure__r.Office__c,Coding_Structure__r.Work_Type__r.name,(SELECT Id,Main_Allocation__c,Allocation_Amount__c,Assigned_To__c,Job__c,Department_Allocation__c,Office__c,workType_Allocation__c FROM Allocations__r ORDER BY Allocation_Amount__c DESC NULLS LAST) FROM Opportunity WHERE Id IN:lstofOpportunity];
        creatmainalloc(setofstaffId,opplst,insertcheck);
    }
    public void updatefunction(Map<Id,Opportunity> joboldMapupdate,Map<Id,Opportunity> jobnewMapupdate){
        List<Opportunity> lstofOpportunityupdate = new List<Opportunity>();
        List<Opportunity> opplst = new List<Opportunity>();
        List<Opportunity> opplstforjob = new List<Opportunity>();
        List<Opportunity> jobtojoblite = new List<Opportunity>();
        set<Id> setofstaffIdupdate = new set<Id>();
        set<Id> setofstaffIdupdateforjobs = new set<Id>();
        for(Id objId : jobnewMapupdate.Keyset()){
            if((jobnewMapupdate.get(objId).Manager__c != joboldMapupdate.get(objId).Manager__c || jobnewMapupdate.get(objId).Coding_Structure__c != joboldMapupdate.get(objId).Coding_Structure__c) /*&& (!jobnewMapupdate.get(objId).Allocations__r.isEmpty())*/){
                lstofOpportunityupdate.add(jobnewMapupdate.get(objId));
                setofstaffIdupdate.add(jobnewMapupdate.get(objId).Manager__c);
            }
            if(jobnewMapupdate.get(objId).Allocations__r.isEmpty() && jobnewMapupdate.get(objId).Amount>0){
                jobtojoblite.add(jobnewMapupdate.get(objId));
                setofstaffIdupdateforjobs.add(jobnewMapupdate.get(objId).Manager__c);
            }
        }
        system.debug('Object msg lstofids'+lstofOpportunityupdate);
        opplst = [SELECT Id,Manager__c,Amount,Coding_Structure__c,Coding_Structure__r.Department__c,Coding_Structure__r.Office__c,Coding_Structure__r.Work_Type__r.name,(SELECT Id,Main_Allocation__c,Allocation_Amount__c,Assigned_To__c,Job__c,Department_Allocation__c,Coding_Structure__c,Office__c,workType_Allocation__c FROM Allocations__r ORDER BY Allocation_Amount__c DESC NULLS LAST) FROM Opportunity WHERE Id IN:lstofOpportunityupdate];
        opplstforjob = [SELECT Id,Manager__c,Amount,Coding_Structure__r.Department__c,Coding_Structure__r.Office__c,Coding_Structure__r.Work_Type__r.name,(SELECT Id,Main_Allocation__c,Allocation_Amount__c,Assigned_To__c,Job__c,Department_Allocation__c,Office__c,workType_Allocation__c FROM Allocations__r ORDER BY Allocation_Amount__c DESC NULLS LAST) FROM Opportunity WHERE Id IN:jobtojoblite];
        boolean updatecheck = false;
        boolean insertTojob = true;
        creatmainalloc(setofstaffIdupdate,opplst,updatecheck);
        createmainforjoblite(setofstaffIdupdateforjobs,opplstforjob,insertTojob);
    }




    // Logic to update the JM Assistant with Job Owner
    /* public void UpdateAssistantJobManager(Map<Id,Opportunity> Triggerold,Map<Id,Opportunity> TriggerNew){
        set<id> setJM = new set<id>();
        set<id> SetOwner = new set<id>();
        Map<id,id> MapOwnerStaff ;
        Map<id,id> MapJMOW;

        for(Opportunity Opp:TriggerNew.values() ){
            setJM.add(Opp.Manager__c);
            SetOwner.add(Opp.Ownerid);
        }
        MapOwnerStaff = GenerateMapOppOwnerStaffID(SetOwner);
        system.debug(MapOwnerStaff);
        MapJMOW = GenerateMapJobManagerOldOwner(TriggerOld, TriggerNew, Trigger.isinsert);
        system.debug(MapJMOW);
        List<Staff__c> JMList = new List<Staff__c>();
        for(Staff__c stff: [Select id, ASSISTANT__c from  Staff__c where id in:setJM and ASSISTANT__c=null]){
            system.debug(stff);
            system.debug(stff);
            system.debug(MapJMOW.containskey(stff.id));
            system.debug(MapOwnerStaff.containskey(MapJMOW.get(stff.id)));
            if(MapJMOW!=null&&MapOwnerStaff!=null&& MapJMOW.containskey(stff.id)&&MapOwnerStaff.containskey(MapJMOW.get(stff.id))){
                stff.ASSISTANT__c = MapOwnerStaff.get(MapJMOW.get(stff.id));
                JMList.add(stff);
            }
        }

        update JMList;
    }

    // Create a Owner and Staff ID
    public Map<id,id> GenerateMapOppOwnerStaffID (set<id> SetOwner ){
            List<Staff__c> OwnerList  = [Select id, User__c from  staff__c where User__c in:SetOwner ];
            Map<id,id> MapOwnerStaff = new Map<id,id>();
            for(Staff__c stff: OwnerList){
                MapOwnerStaff.put(stff.user__c, stff.id);
            }
        return MapOwnerStaff;
    }*/

    // Create a Owner and Staff ID
    public Map<id,id> GenerateMapJMAssistant (set<id> SetJM ){
        List<Staff__c> OwnerList  = [Select id,ASSISTANT__c, ASSISTANT__r.User__c,User__c from  staff__c where id in:SetJM ];
        Map<id,id> MapOwnerStaff = new Map<id,id>();
        for(Staff__c stff: OwnerList){
            if(stff.ASSISTANT__c!=null)
            MapOwnerStaff.put(stff.id, stff.ASSISTANT__r.User__c);
        }
        return MapOwnerStaff;
    }


    // Create a Map of Changed Job Manager and Owner ID
    /*public Map<id,id> GenerateMapJobManagerOldOwner (Map<Id,Opportunity> Triggerold,Map<Id,Opportunity> TriggerNew,boolean insertflag){
            Map<id,id> MapJMOW = new Map<id,id>();

            for(Opportunity Opp: TriggerNew.values()){
                if(insertflag||Opp.Manager__c!=Triggerold.get(Opp.id).Manager__c){
                    MapJMOW.put(Opp.Manager__c, Opp.Ownerid);
                }

            }
            return MapJMOW;
    }*/

    // Logic to update Opportunity Owner Based on Job Manager
    public void UpdateOppOWner(Map<Id,Opportunity> Triggerold,Map<Id,Opportunity> TriggerNew){
        set<id> setJM = new set<id>();
        set<id> SetOwner = new set<id>();
        Map<id,id> MapJMAssistant ;
        Map<id,id> MapJMOW;
        //commented by HA due to null pointer exception
        if(TriggerNew!=null){
            for(Opportunity Opp:TriggerNew.values() ){
                if((Trigger.isinsert&&Opp.Manager__c!=null)||(Trigger.isupdate&&Triggerold.get(opp.id).Manager__c!=Opp.Manager__c&&Opp.Manager__c!=null)){
                    setJM.add(Opp.Manager__c);
                }

            }
            MapJMAssistant = GenerateMapJMAssistant(setJM);
            for(Opportunity Opp: Triggernew.values()){
                if(Opp.Manager__c!=null&&MapJMAssistant.containskey(Opp.Manager__c)&&MapJMAssistant.get(Opp.Manager__c)!=null){
                    Opp.ownerid = MapJMAssistant.get(Opp.Manager__c);
                }
            }
        }
    }

    // Logic to update Opportunity Owner Based on Job Manager
    public void updateFinalInvoice(Map<Id,Opportunity> mapIdOldOpportunity,Map<Id,Opportunity> mapNewOpportunity){

        set<id> setOppIds = new set<id>();
        for(Opportunity objOpportunity : mapNewOpportunity.values()){

            if((objOpportunity.StageName != mapIdOldOpportunity.get(objOpportunity.id).StageName) && mapIdOldOpportunity.get(objOpportunity.id).isClosed == true){
                setOppIds.add(objOpportunity.id);
            }

        }

        if(!setOppIds.isEmpty()){

            List<Invoice__c> lstInvoice = new List<Invoice__c>();
            lstInvoice = [Select id, name,
            Status__c,
            Opportunity__c,
            disableinvoice__c,
            Final_Invoice__c
            From
            Invoice__c
            where
            Opportunity__c in: setOppIds
            and (Final_Invoice__c = true Or
            Status__c = 'Draft')];


            for(Invoice__c objInvoice: lstInvoice){
                if(objInvoice.Final_Invoice__c = true){
                    objInvoice.Final_Invoice__c = false;
                }
                if(objInvoice.Status__c.equalsIgnoreCase('Draft')){
                    objInvoice.disableinvoice__c = false;
                }
            }

            try{
                if(!lstInvoice.isEmpty()){
                    update lstInvoice;
                }
            }catch(exception e){
                System.debug('---->'+e.getMessage());
            }
        }

    }
    public void updateaccoutID(List<Opportunity> opplist){
        List<Account_Address_Junction__c > lstofaccrecds = new List<Account_Address_Junction__c >();
        //List<Opportunty> lstofaccrecds = new List<Opportunty >();
        set<id> address = new set<id>();
        set<id> careofs = new set<id>();
        set<id> addresscareof = new set<id>();
        List<Care_Of__c> lstoffcareofs = new List<Care_Of__c>();
        set<id> accounts = new set<id>();
        set<string> oppids = new set<string>();
        Map<Id,Map<Id,Account_Address_Junction__c>> mapofaccRecd = new Map<Id,Map<Id,Account_Address_Junction__c>>();
        if(!opplist.isEmpty()){
            for(Opportunity obj :opplist){
                if(obj.Invoicing_Address__c != null && string.isNotBlank(obj.Invoicing_Address__c)){
                    address.add(obj.Invoicing_Address__c);
                }
                if(obj.Invoicing_Care_Of__c != null && string.isNotBlank(obj.Invoicing_Care_Of__c)){
                    addresscareof.add(obj.Id);
                    careofs.add(obj.Invoicing_Care_Of__c);
                    if(obj.Invoicing_Care_Of__r.Account_ID__c != null){
                        obj.Account_id__c = obj.Invoicing_Care_Of__r.Account_ID__c;
                    }

                }
                if(obj.Invoicing_Company2__c != null && string.isNotBlank(obj.Invoicing_Company2__c)){
                    accounts.add(obj.Invoicing_Company2__c);
                }
            }
        }
        lstoffcareofs = [SELECT id,Account_ID__c from Care_Of__c where id IN:careofs ];
        Map<Id,Care_Of__c> maptocare =new Map<Id,Care_Of__c>();
        if(!lstoffcareofs.isEmpty()){
            for(Care_Of__c careobj : lstoffcareofs){
                maptocare.put(careobj.Id,careobj);
            }
        }
        system.debug('Object msg'+accounts);
        system.debug('Object msg'+address);
        lstofaccrecds =[SELECT id,name,Account__c,Account_ID__c,Address__c from Account_Address_Junction__c where Address__c IN:address AND Account__c IN: accounts];
        system.debug('Object msg'+lstofaccrecds);
        if(!lstofaccrecds.isEmpty()){
            for(Account_Address_Junction__c objacc : lstofaccrecds){
                if(mapofaccRecd.containskey(objacc.Account__c)){
                    if(mapofaccRecd.get(objacc.Account__c).containskey(objacc.Address__c)){

                    }else{
                        mapofaccRecd.get(objacc.Account__c).put(objacc.Address__c,objacc);
                    }
                }else{
                    map<id,Account_Address_Junction__c> addressmap = new map<id,Account_Address_Junction__c>();
                    if(objacc.Address__c != null && string.isNotBlank(objacc.Address__c)){
                        addressmap.put(objacc.Address__c,objacc);
                        if(objacc.Account__c != null && string.isNotBlank(objacc.Account__c)){
                            mapofaccRecd.put(objacc.Account__c,addressmap);
                        }
                    }

                }
            }
        }
        if(!opplist.isEmpty()){
            for(Opportunity obj :opplist){
                //system.debug('failed'+addresscareof);
                if(!addresscareof.contains(obj.Id)){
                    system.debug('failed');
                    if(obj.Invoicing_Company2__c != null && string.isNotBlank(obj.Invoicing_Company2__c)){
                        system.debug('failed');
                        if(mapofaccRecd.containskey(obj.Invoicing_Company2__c)){
                            system.debug('failed');
                            if(obj.Invoicing_Address__c != null && string.isNotBlank(obj.Invoicing_Address__c)){
                                system.debug('failed');
                                if(mapofaccRecd.get(obj.Invoicing_Company2__c).containskey(obj.Invoicing_Address__c)){
                                    system.debug('failed');
                                    if(string.isNotBlank(mapofaccRecd.get(obj.Invoicing_Company2__c).get(obj.Invoicing_Address__c).Account_ID__c )){
                                        obj.Account_id__c = mapofaccRecd.get(obj.Invoicing_Company2__c).get(obj.Invoicing_Address__c).Account_ID__c ;
                                        system.debug('success change account ID field'+mapofaccRecd.get(obj.Invoicing_Company2__c).get(obj.Invoicing_Address__c).Account_ID__c);
                                    }
                                }
                            }
                        }
                    }
                }else if(addresscareof.contains(obj.Id)){
                    if(maptocare.containsKey(obj.Invoicing_Care_Of__c)){
                        system.debug('success'+maptocare.get(obj.Invoicing_Care_Of__c).Account_ID__c);
                        obj.Account_id__c = maptocare.get(obj.Invoicing_Care_Of__c).Account_ID__c;
                    }
                }
            }
        }
    }
    public static void creatmainalloc(Set<Id> lstidstaff,List<Opportunity> opplst,boolean insertcheck){

        List<Allocation__c> lsttoUpsert = new List<Allocation__c>();
        List<Allocation__c> listofallocation = new List<Allocation__c>();
        List<Forecasting__c> forecastlist = new List<Forecasting__c>();




        if(insertcheck){
            if(!opplst.isEmpty()){
                for(Opportunity objopp : opplst){

                    if(objopp.Allocations__r.isEmpty()){
                        Allocation__c newaloc = new Allocation__c();
                        newaloc.Main_Allocation__c = true;
                        newaloc.Allocation_Amount__c = objopp.Amount;
                        newaloc.Job__c = objopp.Id;
                        newaloc.Assigned_To__c =  objopp.Manager__c;
                        if(objopp.Coding_Structure__c != null){
                            newaloc.Department_Allocation__c = objopp.Coding_Structure__r.Department__c;
                            newaloc.Office__c = objopp.Coding_Structure__r.Office__c;
                            newaloc.workType_Allocation__c=objopp.Coding_Structure__r.Work_Type__r.name;
                        }
                        if(objopp.Amount != null && objopp.Amount>0.00){
                            lsttoUpsert.add(newaloc);
                        }
                    }
                }
            }
        }
        else if(!insertcheck){
            system.debug('Object msg entered to the method' );
            if(!opplst.isEmpty()){
                for(Opportunity oppobject: opplst){
                    List<Allocation__c> lsttosort = new List<Allocation__c>();
                    if(!oppobject.Allocations__r.isEmpty()){
                        system.debug('Object msg allocation not empty' );
                        for(Allocation__c allocobj : oppobject.Allocations__r){
                            if(allocobj.Main_Allocation__c == true && allocobj.Coding_Structure__c != oppobject.Coding_Structure__c ){
                                allocobj.Main_Allocation__c = false;
                                lsttoUpsert.add(allocobj);
                            }
                            if(allocobj.Assigned_To__c != null && allocobj.Coding_Structure__c != null && oppobject.Manager__c != null && oppobject.Coding_Structure__c != null){
                                if(allocobj.Assigned_To__c == oppobject.Manager__c ){

                                    if(allocobj.Coding_Structure__c == oppobject.Coding_Structure__c ){
                                        lsttosort.add(allocobj);
                                    }
                                }
                            }
                        }
                        system.debug('Object msg allocation not empty'+lsttosort );
                    }
                    if(!lsttosort.isEmpty()){
                        lsttosort[0].Main_Allocation__c = true;
                        if(oppobject.Coding_Structure__c != null){
                            lsttosort[0].Department_Allocation__c = oppobject.Coding_Structure__r.Department__c;
                            lsttosort[0].Office__c = oppobject.Coding_Structure__r.Office__c;
                            lsttosort[0].workType_Allocation__c=oppobject.Coding_Structure__r.Work_Type__r.name;
                        }
                        lsttoUpsert.add(lsttosort.get(0));
                    }else{
                        Allocation__c newalocation = new Allocation__c();

                        newalocation.Main_Allocation__c = true;
                        newalocation.Allocation_Amount__c = 0;
                        newalocation.Job__c = oppobject.Id;
                        newalocation.Assigned_To__c =  oppobject.Manager__c;
                        if(oppobject.Coding_Structure__c != null){
                            newalocation.Department_Allocation__c = oppobject.Coding_Structure__r.Department__c;
                            newalocation.Office__c = oppobject.Coding_Structure__r.Office__c;
                            newalocation.workType_Allocation__c=oppobject.Coding_Structure__r.Work_Type__r.name;
                        }
                        lsttoUpsert.add(newalocation);
                    }

                }
            }
        }
        try{
            if(!lsttoUpsert.isEmpty()){
                upsert lsttoUpsert;
            }
        }catch(Exception e){
            ApexPages.addMessages(e);
        }


        if(insertcheck){
            listofallocation = [SELECT Id,Allocation_Amount__c,job__r.CloseDate,job__r.Amount,job__c,Main_Allocation__c,(SELECT Id, Amount__c, CS_Forecast_Date__c FROM Forecastings__r) FROM Allocation__c WHERE job__c IN:opplst AND Main_Allocation__c = true ];
            for(Allocation__c objalloc :listofallocation ){
                if(objalloc.Forecastings__r.isEmpty()){
                    Forecasting__c newforecast = new Forecasting__c();
                    if(objalloc.job__r.CloseDate != null){
                        newforecast.CS_Forecast_Date__c = Date.newinstance(objalloc.job__r.CloseDate.year(), objalloc.job__r.CloseDate.month(), objalloc.job__r.CloseDate.day());
                    }else{
                        newforecast.CS_Forecast_Date__c = system.today();
                    }
                    if(objalloc.Allocation_Amount__c != null){
                        newforecast.Amount__c = objalloc.job__r.Amount;
                    }
                    newforecast.Allocation__c = objalloc.id;
                    forecastlist.add(newforecast);
                }
            }
            try{
                if(!forecastlist.isEmpty()){
                    upsert forecastlist;
                }
            }catch(Exception e){
                ApexPages.addMessages(e);
            }
        }

    }
    public void createmainforjoblite(Set<Id> lstidstaff,List<Opportunity> opplst,boolean insertcheck){
        List<Allocation__c> lsttoUpsert = new List<Allocation__c>();
        List<Forecasting__c> forecastlist = new List<Forecasting__c>();
        List<Allocation__c> listofallocation = new List<Allocation__c>();
        if(insertcheck){
            if(!opplst.isEmpty()){
                for(Opportunity objopp : opplst){

                    if(objopp.Allocations__r.isEmpty() && objopp.Amount>0){
                        Allocation__c newaloc = new Allocation__c();
                        newaloc.Main_Allocation__c = true;
                        newaloc.Allocation_Amount__c = objopp.Amount;
                        newaloc.Job__c = objopp.Id;
                        newaloc.Assigned_To__c =  objopp.Manager__c;
                        if(objopp.Coding_Structure__c != null){
                            newaloc.Department_Allocation__c = objopp.Coding_Structure__r.Department__c;
                            newaloc.Office__c = objopp.Coding_Structure__r.Office__c;
                            newaloc.workType_Allocation__c=objopp.Coding_Structure__r.Work_Type__r.name;
                        }
                        if(objopp.Amount != null && objopp.Amount>0.00){
                            lsttoUpsert.add(newaloc);
                        }
                    }
                }
            }
            try{
                if(!lsttoUpsert.isEmpty()){
                    upsert lsttoUpsert;
                }
            }catch(Exception e){
                System.debug('----->'+e.getMessage());
            }
            listofallocation = [SELECT Id,Allocation_Amount__c,job__r.CloseDate,job__r.Amount,job__c,Main_Allocation__c,(SELECT Id, Amount__c, CS_Forecast_Date__c FROM Forecastings__r) FROM Allocation__c WHERE job__c IN:opplst AND Main_Allocation__c = true ];
            for(Allocation__c objalloc :listofallocation ){
                if(objalloc.Forecastings__r.isEmpty()){
                    Forecasting__c newforecast = new Forecasting__c();
                    if(objalloc.job__r.CloseDate != null){
                        newforecast.CS_Forecast_Date__c = Date.newinstance(objalloc.job__r.CloseDate.year(), objalloc.job__r.CloseDate.month(), objalloc.job__r.CloseDate.day());
                    }else{
                        newforecast.CS_Forecast_Date__c = system.today();
                    }
                    if(objalloc.Allocation_Amount__c != null){
                        newforecast.Amount__c = objalloc.job__r.Amount;
                    }
                    newforecast.Allocation__c = objalloc.id;
                    forecastlist.add(newforecast);
                }
            }
            try{
                if(!forecastlist.isEmpty()){
                    upsert forecastlist;
                }
            }catch(Exception e){
                System.debug('------>'+E.getMessage());
            }
        }
    }
    /* public void checkMLR(Map<Id,Opportunity> jobnewMap){
        set<Id> oppIdSet = new set<Id>();
        List<Allocation__c> allocList = new List<Allocation__c>();
        List<Work_Type__c> wrkTypLst = new List<Work_Type__c>();
        Map<string,Boolean> wkMLRmap = new Map<string,Boolean>();
        List<Opportunity> updateOppList = new List<Opportunity>();
        List<Account> updateAccList = new List<Account>();
        set<Id> accIdSet = new set<Id>();


        for(Opportunity opp :jobnewMap.values()){
            oppIdSet.add(opp.id);
        }
        system.debug('oppIdSet--->'+oppIdSet);
        allocList = [SELECT id,name,Job__c,workType_Allocation__c,Job__r.Amount,job__r.AccountId from Allocation__c where job__c IN:oppIdSet];
        wrkTypLst = [SELECT id,name,MLR__c from Work_Type__c];

        for(Work_Type__c wt : wrkTypLst){
            wkMLRmap.put(wt.name,wt.MLR__c);
        }
        system.debug('wkMLRmap    --->'+wkMLRmap);
        oppIdSet.clear();
        List<MLR__c> mlrLst = [select id,name from MLR__c where name='Test MLR' limit 1];
        system.debug('oppIdSet Claered--->'+oppIdSet);
        set<Id> nonMLRset = new set<Id>();
        for(Opportunity opp :jobnewMap.values()){
            for(Allocation__c alloc :allocList){
                //Account acc = new Account();
                system.debug('alloc.Job__r.Amount---->'+alloc.Job__r.Amount);
                system.debug('wkMLRmap.get(alloc.workType_Allocation__c)---->'+wkMLRmap.get(alloc.workType_Allocation__c));

                List<Job_Amount__c> cfRem = Job_Amount__c.getall().values();
                //  edited by Nidheesh because it is causing error in allocation test class (List index out of bounds: 0 )
                Decimal jobAmt = 0.00;
                if(!cfRem.isEmpty()){
                    jobAmt = jobAmt +cfRem[0].Amount__c;
                }
                system.debug('jobAmt --->'+jobAmt );
                if(alloc.Job__r.Amount > jobAmt && !string.isblank(alloc.workType_Allocation__c) && wkMLRmap.get(alloc.workType_Allocation__c) == true){
                    opp.id=alloc.job__c;
                    opp.MLR_Required__c = true;

                    system.debug('oppIdSet-----1111--->'+oppIdSet);
                    system.debug('alloc.Job__c--->'+alloc.Job__c);
                    if(!oppIdSet.contains(alloc.Job__c)){
                        updateOppList.add(opp);
                        oppIdSet.add(alloc.Job__c);
                    }
                    system.debug('updateOppList----->'+updateOppList);

                }else if(alloc.Job__r.Amount < jobAmt || (!string.isblank(alloc.workType_Allocation__c) && wkMLRmap.get(alloc.workType_Allocation__c) == false)){
                    opp.id=alloc.job__c;
                    //opp.MLR_Required__c = false;

                }else{

                }
            }
            system.debug('oppIdSet--------------->'+oppIdSet);
            system.debug('updateOppList--------------->'+updateOppList);
        }

    } */
    public void checkMLR(Map<Id,Opportunity> jobnewMap){
        system.debug('updateOppList--------------->'+jobnewMap.size());
        set<Id> oppIdSet = new set<Id>();
        List<Opportunity> lstoffOpps = new List<Opportunity>();
        List<Allocation__c> allocList = new List<Allocation__c>();
        List<Work_Type__c> wrkTypLst = new List<Work_Type__c>();
        Map<string,Boolean> wkMLRmap = new Map<string,Boolean>();
        List<Opportunity> updateOppList = new List<Opportunity>();
        List<Account> updateAccList = new List<Account>();
        set<Id> accIdSet = new set<Id>();
        if(!jobnewMap.isEmpty()){
            lstoffOpps = [Select id,name,MLR_Required__c,Amount,(select id,name,workType_Allocation__c from Allocations__r) FROM Opportunity where id IN:jobnewMap.keyset()];
        }



        for(Opportunity opp :jobnewMap.values()){
            oppIdSet.add(opp.id);
        }
        system.debug('oppIdSet--->'+oppIdSet);
        allocList = [SELECT id,name,Job__c,workType_Allocation__c,Job__r.Amount,job__r.AccountId from Allocation__c where job__c IN:oppIdSet];
        wrkTypLst = [SELECT id,name,MLR__c from Work_Type__c where MLR__c = true];

        for(Work_Type__c wt : wrkTypLst){
            wkMLRmap.put(wt.name,wt.MLR__c);
        }
        system.debug('wkMLRmap    --->'+wkMLRmap);
        oppIdSet.clear();
        set<Id> nonMLRset = new set<Id>();
        List<Job_Amount__c> cfRem = Job_Amount__c.getall().values();
        Decimal jobAmt = 0.00;
        if(!cfRem.isEmpty()){
            jobAmt = jobAmt +cfRem[0].Amount__c;
        }
        for(Opportunity opp :lstoffOpps){
            if(opp.Amount != null && opp.Amount>jobAmt){
                if(!opp.Allocations__r.isEmpty()){
                    for(Allocation__c objall : opp.Allocations__r){
                        if(wkMLRmap.containskey(objall.workType_Allocation__c)){
                            if(jobnewMap.containskey(opp.Id)){
                                jobnewMap.get(opp.Id).MLR_Required__c = true;
                                break;
                            }
                        }else{
                            if(jobnewMap.containskey(opp.Id)){
                                jobnewMap.get(opp.Id).MLR_Required__c = false;
                            }
                        }
                    }
                }else{
                    if(jobnewMap.containskey(opp.Id)){
                        jobnewMap.get(opp.Id).MLR_Required__c = false;
                    }
                }
            }else{
                if(jobnewMap.containskey(opp.Id)){
                    jobnewMap.get(opp.Id).MLR_Required__c = false;
                }
            }
        }

    }
    
    public static void insertValidMLR(List<opportunity> listOpp)
    {
        Set<id> invoiceIdSet = new set<id>();
        Set<id> invoiceCareOfSet = new Set<id>();
        
        for(opportunity op : listOpp){
            system.debug('===========op====='+op);
            if(op.Invoicing_Company2__c != null){
                
                invoiceIdSet.add(op.Invoicing_Company2__c); 
                
            }
            else if(op.Invoicing_Care_Of__c != null){
                invoiceCareOfSet.add(op.Invoicing_Care_Of__c);
            }
        }
        
        
        List<account> accList = new list<account>();
        List<Care_Of__c> careOfList =new list<Care_Of__c>();
        accList= [select id, MLR_Id__c, Valid_MLR__c,MLR_Id__r.MLR_Set_Date__c,MLR_Id__r.Expiry_Date__c  
                 from account where id =: invoiceIdSet ];
        careOfList= [select id, Reporting_Client__c,Reporting_Client__r.MLR_Id__c, Reporting_Client__r.MLR_Id__r.MLR_Set_Date__c,
                    Reporting_Client__r.MLR_Id__r.Expiry_Date__c,Reporting_Client__r.Valid_MLR__c  
                    from care_of__c where id =: invoiceCareOfSet ];
        map<id,care_of__c> careOfMap = new map<id, care_of__c>(careOfList);
        map <id, Account> acMap = new map<id,account>(accList);
        for(opportunity op : listOpp){
            if(op.Invoicing_Company2__c != null && acMap.containsKey(op.Invoicing_Company2__c)){
                
                 Account objAcc = acMap.get(op.Invoicing_Company2__c);
                op.Valid_MLR__c = objAcc.Valid_MLR__c;
                op.MLR_Set_Date__c =objAcc.MLR_Id__r.MLR_Set_Date__c;
                op.MLR_Expiry_Date__c = objAcc.MLR_Id__r.Expiry_Date__c;
            }
            else if(op.Invoicing_Care_Of__c != null && careOfMap.containsKey(op.Invoicing_Care_Of__c)){
                care_of__c objCareOf = careOfMap.get(op.Invoicing_Care_Of__c);
                op.Valid_MLR__c = objCareOf.Reporting_Client__r.Valid_MLR__c;
                op.MLR_Set_Date__c = objCareOf.Reporting_Client__r.MLR_Id__r.MLR_Set_Date__c;
                op.MLR_Expiry_Date__c = objCareOf.Reporting_Client__r.MLR_Id__r.Expiry_Date__c;
            }
            
        }
    }
    
        public static void updateValidMLR(List<opportunity> oppList,Map<id,opportunity> oppMap){
        Set<id> invoiceIdSet = new set<id>();
        Set<id> invoiceCareOfSet = new Set<id>();
            for(opportunity op : oppList){
                if(op.Invoicing_Company2__c != null){
                    
                    invoiceIdSet.add(op.Invoicing_Company2__c); 
                    
                }
                else if(op.Invoicing_Care_Of__c != null){
                    invoiceCareOfSet.add(op.Invoicing_Care_Of__c);
                }
            
            }
            
            List<account> accList = new list<account>();
            List<Care_Of__c> careOfList =new list<Care_Of__c>();
            accList= [select id, MLR_Id__c, Valid_MLR__c,MLR_Id__r.MLR_Set_Date__c,MLR_Id__r.Expiry_Date__c  
                     from account where id =: invoiceIdSet ];
            careOfList= [select id, Reporting_Client__c,Reporting_Client__r.MLR_Id__c, Reporting_Client__r.MLR_Id__r.MLR_Set_Date__c,
                        Reporting_Client__r.MLR_Id__r.Expiry_Date__c,Reporting_Client__r.Valid_MLR__c  
                        from care_of__c where id =: invoiceCareOfSet ];
            map<id,care_of__c> careOfMap = new map<id, care_of__c>(careOfList);
            map <id, Account> acMap = new map<id,account>(accList);
            
            for(opportunity op : oppList){
                if(oppMap.get(op.id).Invoicing_Company2__c != op.Invoicing_Company2__c && op.Invoicing_Company2__c != null && acMap.containsKey(op.Invoicing_Company2__c)){
                        Account objAcc = acMap.get(op.Invoicing_Company2__c);
                        op.Valid_MLR__c = objAcc.Valid_MLR__c;
                        op.MLR_Set_Date__c =objAcc.MLR_Id__r.MLR_Set_Date__c;
                        op.MLR_Expiry_Date__c = objAcc.MLR_Id__r.Expiry_Date__c;
                    
                    
                }
                else if(oppMap.get(op.id).Invoicing_Care_Of__c != op.Invoicing_Care_Of__c && op.Invoicing_Care_Of__c != null && careOfMap.containsKey(op.Invoicing_Care_Of__c)){
                        care_of__c objCareOf = careOfMap.get(op.Invoicing_Care_Of__c);
                        op.Valid_MLR__c = objCareOf.Reporting_Client__r.Valid_MLR__c;
                        op.MLR_Set_Date__c = objCareOf.Reporting_Client__r.MLR_Id__r.MLR_Set_Date__c;
                        op.MLR_Expiry_Date__c = objCareOf.Reporting_Client__r.MLR_Id__r.Expiry_Date__c;
                    
                }
                
            }
        }
            
        }