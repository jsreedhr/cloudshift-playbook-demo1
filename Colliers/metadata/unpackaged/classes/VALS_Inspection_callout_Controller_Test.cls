@isTest
public class VALS_Inspection_callout_Controller_Test
{
   
    @isTest(seealldata=True)
    private static void Method1()
    {
      Property__c propertyObj3 = new Property__c();   
      propertyObj3=TestObjectHelper.createProperty();
      propertyObj3.Street_No__c ='Park Lane 2';
      propertyObj3.Post_Code__c ='W1K 3DD';
      propertyObj3.Country__c = 'United Kingdom';
      propertyObj3.Geolocation__Latitude__s=51.51;
      propertyObj3.Geolocation__Longitude__s=-0.15;
      propertyObj3.Override_Experian_validation__c=true;
      insert propertyObj3;
      System.assert(propertyObj3.id!=NULL);
        
        opportunity opp = new opportunity();
        opp.stagename='Negotiation';
        opp.closedate=system.today();
        opp.name='TestData';
        insert opp;
        
        
    	Inspection__c inspObj = New Inspection__c();
    	inspObj.Name = 'Test Inspection';
        inspobj.Property__c = propertyObj3.id;
        inspobj.job__c=opp.id;
    	insert inspObj;
    	System.assert(inspObj.id!=NULL);
        
    	External_Inspection__c extInspObj = New External_Inspection__c();
    	extInspObj.Inspection__c = inspObj.id;
    	insert extInspObj;
    	System.assert(extInspObj.id!=NULL);
        
        Surrounding__c surobj = new Surrounding__c();
        Surobj.Inspection__c = inspObj.id;
        insert surobj;
    	System.assert(surobj.id!=NULL);

    	Internal_Inspection__c intInspObj = New Internal_Inspection__c();
    	intInspObj.Inspection__c = inspObj.id;
    	intInspObj.External_Inspection__c = extInspObj.id;
    	insert intInspObj;
    	System.assert(intInspObj.id!=NULL);
        
       Feeditem fi = new Feeditem();
        fi.body='testData';
        fi.parentid=extInspObj.id;
        insert fi;
        
         
        PageReference pageRef = Page.Vals_sharedoPopUpForInspection;
        pageRef.getParameters().put('oppid',inspObj.id);
        Test.setCurrentPage(pageRef);
    
        VALS_Inspection_callout_Controller cont= new VALS_Inspection_callout_Controller ();
        cont.doCallout();
    }
    
    @isTest(seealldata=True)
    private static void Method2()
    {
         Property__c propertyObj3 = new Property__c();   
      propertyObj3=TestObjectHelper.createProperty();
      propertyObj3.Street_No__c ='Park Lane 2';
      propertyObj3.Post_Code__c ='W1K 3DD';
      propertyObj3.Country__c = 'United Kingdom';
      propertyObj3.Geolocation__Latitude__s=51.51;
      propertyObj3.Geolocation__Longitude__s=-0.15;
      propertyObj3.Override_Experian_validation__c=true;
      insert propertyObj3;
      System.assert(propertyObj3.id!=NULL);
        
        opportunity opp = new opportunity();
        opp.stagename='Negotiation';
        opp.closedate=system.today();
        opp.name='TestData';
        insert opp;
        
        
    	Inspection__c inspObj = New Inspection__c();
    	inspObj.Name = 'Test Inspection';
        inspobj.Property__c = propertyObj3.id;
        inspobj.job__c=opp.id;
    	insert inspObj;
    	System.assert(inspObj.id!=NULL);
        
    	External_Inspection__c extInspObj = New External_Inspection__c();
    	extInspObj.Inspection__c = inspObj.id;
    	insert extInspObj;
    	System.assert(extInspObj.id!=NULL);
        
        Surrounding__c surobj = new Surrounding__c();
        Surobj.Inspection__c = inspObj.id;
        insert surobj;
    	System.assert(surobj.id!=NULL);

    	Internal_Inspection__c intInspObj = New Internal_Inspection__c();
    	intInspObj.Inspection__c = inspObj.id;
    	intInspObj.External_Inspection__c = extInspObj.id;
    	insert intInspObj;
    	System.assert(intInspObj.id!=NULL);
    	
    	PageReference pageRef = Page.Vals_sharedoPopUpForInspection;
        pageRef.getParameters().put('oppid',inspObj.id);
        Test.setCurrentPage(pageRef);
    
        VALS_Inspection_callout_Controller cont= new VALS_Inspection_callout_Controller ();
        cont.doCallout();
    }
    
}