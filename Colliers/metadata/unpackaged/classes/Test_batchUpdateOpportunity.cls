@isTest
private class Test_batchUpdateOpportunity {
   
    Public static Account a;
    static List <Opportunity> opps;
    static testmethod void setupData(){
        //create account
        a = TestObjectHelper.createAccount();
        insert a ;
        opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
    }
    private static testMethod void test1() {
        setupData();
        Test.startTest();
        cs_BatchUpdateOpportunity objBatch = new cs_BatchUpdateOpportunity();
        database.executeBatch(objBatch,200);
        Test.stopTest();
    }
}