/**
 *  Class Name: TestUtilClass
 *  Description: Test Class for UtilClass
 *  Company: dQuotient
 *  CreatedDate: 19/08/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Aparna G                19/08/2016              
 *
 */

@isTest
private class TestUtilClass {
    
    /**
     *  Method Name:    TestFindingObjectGivenRecordId 
     *  Description:    Test for findOjectFromId method in UtilClass 
     *  Param:  -
     *  Return: void
     */
    @isTest
    static void TestFindingObjectGivenRecordId(){
        //creating accounts using TestAccountData class
        List<Account> accts=TestAccountData.createAccountsWithOpps(1,2);
        //using findOjectFromId from UtilClass 
        Schema.sObjectType objt=UtilClass.findOjectFromId(accts[0].Id);
        System.assertEquals('Account',String.valueOf(objt));
    }
    
    /**
     *  Method Name:    TestFindingObjectGivenNullId
     *  Description:    Test for findOjectFromId when null parameters are passed
     *  Param:  -
     *  Return: void
     */
    @isTest 
    static void TestFindingObjectGivenNullId(){
        //creating accounts using TestAccountData class
        List<Account> accts=TestAccountData.createAccountsWithOpps(1,2);
        //using findOjectFromId from UtilClass and passing null parameters
        Schema.sObjectType objt=UtilClass.findOjectFromId(null);
        System.assertEquals(false,objt!=null);
    }
    
    /**
     *  Method Name:    TestFindChildsFromGivenObjName 
     *  Description:    Test for findChildsFromParent method in UtilClass 
     *  Param:  -
     *  Return: void
     */ 
    @isTest 
    static void TestFindChildsFromGivenObjName(){
        //creating accounts using TestAccountData class
        List<Account> accts=TestAccountData.createAccountsWithOpps(1,2);
        //using findChildsFromParent from UtilClass 
        list<Schema.ChildRelationship>  childList=UtilClass.findChildsFromParent(Schema.getGlobalDescribe().get( 'Account' ));
        System.debug(childList);
        System.assertEquals(true,childList!=null);
    }
    
    /**
     *  Method Name:    TestFindChildsFromNullobjName
     *  Description:    Test for findChildsFromParent when null parameters are passed
     *  Param:  -
     *  Return: void
     */  
    @isTest 
    static void TestFindChildsFromNullobjName(){
        //creating accounts using TestAccountData class
        List<Account> accts=TestAccountData.createAccountsWithOpps(1,2);
        //using findChildsFromParent from UtilClass when null parameters are passed
        list<Schema.ChildRelationship>  childList=UtilClass.findChildsFromParent(Schema.getGlobalDescribe().get( '' ));
        System.assertEquals(false,childList!=null);
    }
    
    /**
     *  Method Name:    TestFindChildRelGivenTwoObjName 
     *  Description:    Test for findChildRelationship method in UtilClass 
     *  Param:  -
     *  Return: void
     */
    @isTest 
    static void TestFindChildRelGivenTwoObjName(){
        //creating accounts using TestAccountData class
        List<Account> accts=TestAccountData.createAccountsWithOpps(1,2);
        //using findChildRelationship from UtilClass
        Schema.ChildRelationship  childRel=UtilClass.findChildRelationship(Schema.getGlobalDescribe().get( 'Account' ),Schema.getGlobalDescribe().get( 'Opportunity' ));
        System.assertEquals(true,childRel!=null);
    }
    
    /**
     *  Method Name:    TestFindChildRelGivenParntName
     *  Description:    Test for findChildRelationship method when only parent name is passed 
     *  Param:  -
     *  Return: void
     */
    @isTest 
    static void TestFindChildRelGivenParntName(){
        //creating accounts using TestAccountData class
        List<Account> accts=TestAccountData.createAccountsWithOpps(1,2);
        //using findChildRelationship from UtilClass and only passing parent name
        Schema.ChildRelationship childRel=UtilClass.findChildRelationship(Schema.getGlobalDescribe().get( 'Account' ),Schema.getGlobalDescribe().get( '' ));
        System.assertEquals(false,childRel!=null);
    }
    
    /**
     *  Method Name:    TestFindChildRelGivenParntName
     *  Description:    Test for findChildRelationship method when only child name is passed 
     *  Param:  -
     *  Return: void
     */ 
    @isTest 
    static void TestFindChildRelGivenChildName(){
        //creating accounts using TestAccountData class
        List<Account> accts=TestAccountData.createAccountsWithOpps(1,2);
        //using findChildRelationship from UtilClass and only passing child name
        Schema.ChildRelationship  childRel=UtilClass.findChildRelationship(Schema.getGlobalDescribe().get( '' ),Schema.getGlobalDescribe().get( 'Opportunity' ));
        System.assertEquals(false,childRel!=null);
    }
    
    /**
     *  Method Name:    TestFindChildRelGivenParntName
     *  Description:    Test for findChildRelationship method when null values are passed 
     *  Param:  -
     *  Return: void
     */
    @isTest 
    static void TestFindChildRelGivenNullName(){
        //creating accounts using TestAccountData class
        List<Account> accts=TestAccountData.createAccountsWithOpps(1,2);
        Schema.ChildRelationship  childRel=UtilClass.findChildRelationship(Schema.getGlobalDescribe().get( '' ),Schema.getGlobalDescribe().get( '' ));
        //using findChildRelationship from UtilClass and passing null values
        System.assertEquals(false,childRel!=null);
    }
    
    /**
     *  Method Name:    TestFindFieldsGivenObjName
     *  Description:    Test for findRequiredFields method in UtilClass
     *  Param:  -
     *  Return: void
     */
    @isTest 
    static void TestFindFieldsGivenObjName(){
        //creating accounts using TestAccountData class
        List<Account> accts=TestAccountData.createAccountsWithOpps(1,2);
        //using findRequiredFields from UtilClass 
        list<Schema.SObjectField>  fieldList=UtilClass.findRequiredFields(Schema.getGlobalDescribe().get( 'Account' ));     
    }
    
    /**
     *  Method Name:    TestFindFieldsGivenObjName
     *  Description:    Test for findRequiredFields when null parameters are passed
     *  Param:  -
     *  Return: void
     */
    @isTest 
    static void TestFindFieldsGivenNullName(){
        //creating accounts using TestAccountData class
        List<Account> accts=TestAccountData.createAccountsWithOpps(1,2);
        //using findRequiredFields from UtilClass and passing null parameters
        list<Schema.SObjectField>  fieldList=UtilClass.findRequiredFields(Schema.getGlobalDescribe().get( '' ));        
    }   
    /**
     *  Method Name:    TestReadFieldSetGivenObjName
     *  Description:    Test for ReadFieldSet method in UtilClass
     *  Param:  -
     *  Return: void
     */
    @isTest 
    static void TestReadFieldSetGivenObjName(){
        //creating accounts using TestAccountData class
        List<Account> accts=TestAccountData.createAccountsWithOpps(1,2);
        //using findRequiredFields from UtilClass 
        list<Schema.FieldSetMember>  fieldList=UtilClass.readFieldSet('AccountFieldsetForTest', 'Account');
    }
    /**
     *  Method Name:    TestgetFieldSetNameFromCustomSettingGivenObjName
     *  Description:    Test for getFieldSetNameFromCustomSetting method in UtilClass
     *  Param:  -
     *  Return: void
     */
    @isTest 
    static void TestgetFieldSetNameFromCustomSettingGivenObjName(){
        //creating accounts using TestAccountData class
        List<Account> accts=TestAccountData.createAccountsWithOpps(1,2);
        //using findRequiredFields from UtilClass 
        String  result=UtilClass.getFieldSetNameFromCustomSetting('Account');
    }
    /**
     *  Method Name:    TestgetFieldSetNameFromCustomSettingGivenObjName
     *  Description:    Test for getFieldSetNameFromCustomSetting method in UtilClass
     *  Param:  -
     *  Return: void
     */
    @isTest 
    static void TestgetFieldSetNameFromCustomSettingGivenNullName(){
        //creating accounts using TestAccountData class
        List<Account> accts=TestAccountData.createAccountsWithOpps(1,2);
        //using findRequiredFields from UtilClass 
        String  result=UtilClass.getFieldSetNameFromCustomSetting(NULL);
    }
}