/**
*  Class Name: SharingEngineConfigurationManagerTest
*  Description: Test class for helper class SharingEngineConfigurationManager
*  which is used for querying and managing status of Sharing rules (on/off regular/batch)
* 
*  Company: CloudShift
*  CreatedDate: 29/06/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  ----------------------------------------------------------- 
*  Kaspars Rezgalis		28/06/2018					Created
*
*/
@isTest
private class SharingEngineConfigurationManagerTest{
    @isTest
    private static void singletonTest() {
        SharingEngineConfigurationManager initialInstance = SharingEngineConfigurationManager.getInstance('Opportunity');
        SharingEngineConfigurationManager secondaryInstance = SharingEngineConfigurationManager.getInstance('Opportunity');

        System.assert(
            initialInstance === secondaryInstance, 
            'The get instance method should always return the same instance of the SharingEngineConfigurationManager. Actual initialInstance: ' + initialInstance + ', secondaryInstance: ' + secondaryInstance
        );
    }

    @isTest
    private static void isSharingOffTest_NoSetting() {
        String sObjectNameForTest = 'Opportunity';
        SharingEngine_Status__c tmp_assert = SharingEngine_Status__c.getInstance(sObjectNameForTest);
        System.assertEquals(null, tmp_assert, 'Custom sharing rules setting for ' + sObjectNameForTest + ' should be null - it was not created manually and getInstance() method has not been called yet.');

        System.assert(
            !SharingEngineConfigurationManager.getInstance(sObjectNameForTest).isBatchSharingAlreadyRunning(),
            'Batch sharing should always be not running by default, i.e. when a settings do not exist yet in the system.'
        );
    }

    @isTest
    private static void toggleSharingTest(){
    	String sObjectNameForTest = 'Opportunity';

        SharingEngineConfigurationManager sharingSettingForOpp = SharingEngineConfigurationManager.getInstance(sObjectNameForTest);

        sharingSettingForOpp.setBatchSharingIsRunning_ON();
        System.assert(
            sharingSettingForOpp.isBatchSharingAlreadyRunning(),
            'Batch sharing flag should be set to TRUE after setBatchSharingIsRunning_ON() method is called'
        );
        sharingSettingForOpp.setBatchSharingIsRunning_OFF();
        System.assert(
            !sharingSettingForOpp.isBatchSharingAlreadyRunning(),
            'Batch sharing flag should be set to FALSE after setBatchSharingIsRunning_OFF() method is called'
        );
    }

    @isTest
    private static void commitConfigurationTest(){
    	String sObjectNameForTest = 'Opportunity';

        SharingEngineConfigurationManager sharingSettingForOpp = SharingEngineConfigurationManager.getInstance(sObjectNameForTest);
        sharingSettingForOpp.setBatchSharingIsRunning_ON();
        sharingSettingForOpp.commitConfigurationChanges();

        SharingEngine_Status__c tmp_assert = SharingEngine_Status__c.getInstance(sObjectNameForTest);

        System.assertEquals(true, tmp_assert.Batch_Processing_in_Progress__c, 'Flag that batch sharing is currently running should be set for given sObject.');
    }

    @isTest
    private static void testMultipleSObjects(){
    	String sObjectNameForTest1 = 'Opportunity';
    	String sObjectNameForTest2 = 'Allocation__c';

        SharingEngine_Status__c tmp_assert = SharingEngine_Status__c.getInstance(sObjectNameForTest1);
        System.assertEquals(null, tmp_assert, 'Custom sharing rules setting for ' + sObjectNameForTest1 + ' should be null - it was not created manually and getInstance() method has not been called yet.');

        tmp_assert = SharingEngine_Status__c.getInstance(sObjectNameForTest2);
        System.assertEquals(null, tmp_assert, 'Custom sharing rules setting for ' + sObjectNameForTest2 + ' should be null - it was not created manually and getInstance() method has not been called yet.');

        SharingEngineConfigurationManager sharingSettingForOpp = SharingEngineConfigurationManager.getInstance(sObjectNameForTest1);
        SharingEngineConfigurationManager sharingSettingForAlloc = SharingEngineConfigurationManager.getInstance(sObjectNameForTest2);
        sharingSettingForOpp.setBatchSharingIsRunning_OFF();
        sharingSettingForOpp.commitConfigurationChanges();
        sharingSettingForAlloc.setBatchSharingIsRunning_ON();
        sharingSettingForAlloc.commitConfigurationChanges();

        tmp_assert = SharingEngine_Status__c.getInstance(sObjectNameForTest1);
        System.assertEquals(false, tmp_assert.Batch_Processing_in_Progress__c, 'Flag that batch sharing is running for ' + sObjectNameForTest1 + ' should not be set (processing not in progress).');
        tmp_assert = SharingEngine_Status__c.getInstance(sObjectNameForTest2);
        System.assertEquals(true, tmp_assert.Batch_Processing_in_Progress__c, 'Flag that batch sharing is running for ' + sObjectNameForTest2 + ' should be set (processing in progress).');
    }
}