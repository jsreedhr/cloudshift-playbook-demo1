@isTest
public class VALS_InternInspecTrigger_Test
{
    @isTest
    public static void TestMethod1()
    {
        Inspection__c inspObj = New Inspection__c();
        inspObj.Name = 'Test Inspection';
        insert inspObj;
        System.assert(inspObj.id!=NULL);

        External_Inspection__c extInspObj = New External_Inspection__c();
        extInspObj.Inspection__c = inspObj.id;
        insert extInspObj;
        System.assert(extInspObj.id!=NULL);

        Internal_Inspection__c intInspObj = New Internal_Inspection__c();
        intInspObj.Inspection__c = inspObj.id;
        intInspObj.External_Inspection__c = extInspObj.id;
        insert intInspObj;
        System.assert(intInspObj.id!=NULL);

        intInspObj.Wholly_MainlyAncillaryStaircases__c = 'Metal';
        intInspObj.Wholly_MainlyConstruction__c = 'Concrete';
        intInspObj.Wholly_MainlyCovering__c = 'Carpeted';
        intInspObj.Wholly_MainlyHeating__c = 'Electric radiators';
        intInspObj.Light_Fixing_Wholly_Mainly__c = 'Suspended';
        intInspObj.Lighting_type_Entirely_Mainly__c = 'Spot';
        intInspObj.Wholly_MainlyStair__c = 'Metal';
        intInspObj.Wholly_MainlyPowerDisribution__c = '3 phase';
        intInspObj.Wholly_Mainly__c = 'Tiled';
        intInspObj.Wholly_MainlyInternal__c = 'Brick';
        update intInspObj;

        intInspObj.Ancillary_Staircases_Partly__c = 'Metal';
        intInspObj.PartlyConstruction__c = 'Concrete';
        intInspObj.PartlyCovering__c = 'Carpeted';
        intInspObj.PartlyHeating__c = 'Temporary heating';
        intInspObj.LightPartlyFixing__c = 'Suspended';
        intInspObj.Lighting_Type_Partly__c = 'Spot';
        intInspObj.PartlyStair__c = 'Metal';
        intInspObj.PartlyPowerDisribution__c = 'Raised Floor';
        intInspObj.PartlyCeiling__c = 'Tiled';
        intInspObj.PartlyInternal__c = 'Brick';
        update intInspObj;

        intInspObj.Wholly_MainlyAncillaryStaircases__c = NULL;
        intInspObj.Wholly_MainlyConstruction__c = NULL;
        intInspObj.Wholly_MainlyCovering__c = NULL;
        intInspObj.Wholly_MainlyHeating__c = NULL;
        intInspObj.Light_Fixing_Wholly_Mainly__c = NULL;
        intInspObj.Lighting_type_Entirely_Mainly__c = NULL;
        intInspObj.Wholly_MainlyStair__c = NULL;
        intInspObj.Wholly_MainlyPowerDisribution__c = NULL;
        intInspObj.Wholly_Mainly__c = NULL;
        intInspObj.Wholly_MainlyInternal__c = NULL;
        update intInspObj;

    }
}