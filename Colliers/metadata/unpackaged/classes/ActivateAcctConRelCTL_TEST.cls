@isTest
private class ActivateAcctConRelCTL_TEST{
    public static testmethod void activateacctconrelation(){
    Previous_Company__c objprevcom = new Previous_Company__c();
    Account acc = new Account();
    acc.Name = 'Test Account';
    insert acc;
    
    Account acca = new Account();
    acca.Name = 'Test Account a';
    insert acca;
    
    Contact con = new Contact();
    con.LastName = 'Test Contact';
    con.AccountId = acc.id;
    insert con;
    
    objprevcom.Company__c = acca.id;
    objprevcom.Contact__c = con.id;
    insert objprevcom;
    
    ApexPages.currentPage().getParameters().put('id',String.valueOf(objprevcom.Id));
    ApexPages.StandardController StdCtl = new ApexPages.StandardController(objprevcom);
    ActivateAccountContactRelationshipCTL methodins = new ActivateAccountContactRelationshipCTL(StdCtl);
    methodins.activateRelationship();
    }
}