/**
 *  Class Name: Test_codingStructureTriggerHandler 
 *  Description: Test class for the codingStructureTriggerHandler
 *  Company: dQuotient
 *  CreatedDate: 24/10/2016 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nidheesh               24/10/2016                  Orginal Version          
 *
 */
@isTest
private class Test_codingStructureTriggerHandler {

    @testsetup
    static void setupData(){
        Staff__c staffObj =  new Staff__c();
        staffObj.Name = 'staffdummy';
        staffObj.HR_No__c = String.valueOf(Math.round(Math.random()*1000));
        staffObj.Email__c = 'staff@gmail.com';
        insert staffObj;
        Work_Type__c workObj = new Work_Type__c();
        workObj.Name = 'worktype';
        workObj.Department__c  = 'Residential - Development';
        insert workObj;
        Work_Type__c workObjnew = new Work_Type__c();
        workObjnew.Name = 'worktypenew';
        workObjnew.Department__c  = 'Residential - Development';
        insert workObjnew;
        Work_Type__c workObjtest = new Work_Type__c();
        workObjtest.Name = 'worktypetest';
        workObjtest.Department__c  = 'Hotels Valuation';
        insert workObjtest;    
    }
    static testMethod void testCS_codingStructureTriggerHandler(){
        Staff__c staffobject = [Select id from Staff__c Limit 1];
        Coding_Structure__c codestructObj = new Coding_Structure__c();
        codestructObj.Office__c = 'Bristol';
        codestructObj.Work_Type__c = null;
        codestructObj.Status__c = 'Active';
        codestructObj.Department__c = 'Residential - Development';
        codestructObj.Staff__c = staffobject.Id;
        Test.startTest();
        cs_codingStructureTriggerHandler.run = true;
        insert codestructObj;
        //Create Disbursements__c data for opportunity
        
        Test.stopTest();     
    }
    static testmethod void Test_worktypeName(){
        Staff__c staffobject = [Select id from Staff__c Limit 1];
        Coding_Structure__c codestructObj = new Coding_Structure__c();
        codestructObj.Office__c = 'Bristol';
        codestructObj.Department__c = 'Hotels Valuation';
        codestructObj.Staff__c = staffobject.Id;
        codestructObj.Status__c = 'Active';
        Test.startTest();
        cs_codingStructureTriggerHandler.run = true;
        insert codestructObj;
        cs_codingStructureTriggerHandler.run = true;
                codestructObj.Department__c = 'Residential - Development';
                cs_codingStructureTriggerHandler.run = true;
                codestructObj.Status__c = 'Inactive';
                update codestructObj;
                  codestructObj.Department__c = 'Hotels Valuation';
                  cs_codingStructureTriggerHandler.run = true;
                update codestructObj;
      

        //Create Disbursements__c data for opportunity
        Coding_Structure__c codeobject = [Select id,Work_Type__r.Name from Coding_Structure__c Limit 1];
        //System.assertEquals('worktypetest',codeobject.Work_Type__r.Name);
        Test.stopTest();    
    }
    
}