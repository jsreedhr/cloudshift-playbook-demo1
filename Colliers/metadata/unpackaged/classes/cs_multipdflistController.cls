public class cs_multipdflistController
{

    public cs_multipdflistController(ApexPages.StandardController controller) {

    }

    ApexPages.StandardSetController setCon; 
    public string firstId {get;set;}
    public Integer sizeofwrapper {get;set;}
    public Map<Id,Id> invoiceToJob {get;set;}
    public List<Invoice__c> Invlist {get;set;}
    public Map<Id,Id> invoiceToattach{get;set;}
    public List<id> lstofids {get;set;}
    
    public cs_multipdflistController(ApexPages.StandardSetController controller)
    {   
        
        Map<Id,Invoice_PDF_Value__c> managertopdfvalue = new Map<Id,Invoice_PDF_Value__c>();
        Map<string,Invoice_PDF_Value__c> departtopdfvalue = new Map<string,Invoice_PDF_Value__c>();
        List<Invoice_PDF_Value__c> lstofInvPdf = new List<Invoice_PDF_Value__c>();
        set<Id> IdofInvoice = new set<id>();
        lstofids = new List<Id>();
        
        List<currency_Symbol__c> currencyobj = new List<currency_Symbol__c>();
        Map<string,string> currencynameTosymbol = new Map<string,string>();
        set<string> lstofdepartment = new set<string>();
        invoiceToattach = new Map<Id,Id>();
        firstId = '';
        setCon = controller;
        Invlist = new List<Invoice__c>();
        for ( Invoice__c Inv : (Invoice__c[])setCon.getSelected() )
        {
               lstofids.add(Inv.Id);               
        }
        Invlist = [select Id, Total_Amount_Inc_Vat__c, Invoice_Currency__c,Opportunity__r.Department_Cost_Centre_Code__c,Printed_Date__c,Date_of_Invoice__c,Amount_Inc_VAT__c,Opportunity__r.Coding_Structure__r.Department_Cost_Code_Centre__c,Opportunity__r.Manager__c,Opportunity__r.Invoicing_Company__c,OriginalInvoicePrinted__c,Final_Invoice__c ,Total_Fee__c ,Total_cost__c,Total_amount__c,VAT_Amount__c, Company__c,Opportunity__c,CurrencyIsoCode,PO_Number__r.name,(select name from Purchase_Order_Invoice_Junctions__r),Opportunity__r.Job_Number__c,Opportunity__r.Invoicing_Company2__r.MLR_Number__c,Opportunity__r.Invoicing_Company2__c,Opportunity__r.Invoicing_Company2__r.Building_Name__c,Opportunity__r.Invoicing_Company2__r.Street__c,Opportunity__r.Invoicing_Company2__r.Post_Code__c,Opportunity__r.Invoicing_Company2__r.Floor_No__c,Opportunity__r.Invoicing_Company2__r.Street_No__c,Opportunity__r.Invoicing_Company2__r.Town__c,Opportunity__r.Invoicing_Company2__r.country__c,Invoice_Wording__c,Opportunity__r.name,Opportunity__r.Invoicing_Company2__r.name,Opportunity__r.Manager__r.Coding_Structure__r.Department__c,Opportunity__r.id,Assigned_Invoice_Number__c,File_Reference__c,Vat_Reg_No__c ,Opportunity__r.Manager__r.id,Opportunity__r.Invoicing_Company__r.Address__r.Town__c,Opportunity__r.Invoicing_Company__r.Address__r.Building__c,Opportunity__r.Invoicing_Company__r.Address__r.country__c,Opportunity__r.Invoicing_Company__r.Address__r.Street__c , name, Company__r.name, Contact__c, Contact__r.name, FE_Write_Of_Created_Date__c, is_International__c, Payable_By__c,status__c,VAT__c, createdDate,(select id,fee__c,type__c,Cost_Type__c,Amount_ex_VAT__c,VAT_Amount__c,Description__c from Invoice_Line_Items__r where Type__c='Fee' Or Type__c='Cost') from invoice__c where id IN:lstofids ];
        
        
       
    }
    public void saveattch(){
        List<Invoice__c> lstToupdate = new List<Invoice__c>();
        List<Attachment> lstToupdateattach = new List<Attachment>();
        if(!Invlist.isEmpty()){
            for(Invoice__c objInvoiceSelected : Invlist){
                PageReference pdf = Page.CS_InvoicePDFView;
                pdf.getParameters().put('invid',objInvoiceSelected.id);
                objInvoiceSelected.Printed_Date__c= System.today();
                Attachment attach = new Attachment();

                // the contents of the attachment from the pdf
                Blob body;
                String original ='Copy';
                string IsDraftOrApproved= null;
                if(objInvoiceSelected.status__c=='Approved'){
                    if(objInvoiceSelected.OriginalInvoicePrinted__c!= true){               
                        objInvoiceSelected.OriginalInvoicePrinted__c=true;
                        IsDraftOrApproved='FINAL';
                        lstToupdate.add(objInvoiceSelected);
                        
                    }
                    
                }
                if(objInvoiceSelected.OriginalInvoicePrinted__c== true && IsDraftOrApproved!='FINAL' && objInvoiceSelected.status__c!= 'Draft'){
                        IsDraftOrApproved='COPY';
                }
                else if(objInvoiceSelected.status__c== 'Draft'){
                    IsDraftOrApproved='DRAFT';
                }
                original=IsDraftOrApproved;
                pdf.getParameters().put('Copy',Original);
                try {

                    // returns the output of the page as a PDF
                    body = pdf.getContentAsPDF();
                    system.debug('body should be fine');

                    // need to pass unit test -- current bug    
                } catch (VisualforceException e) {
                    system.debug('in the catch block');
                    body = Blob.valueOf('Error');
                }
                attach.Body = body;
                if(objInvoiceSelected.Assigned_Invoice_Number__c== null){
                    attach.Name =IsDraftOrApproved+'_'+objInvoiceSelected.Opportunity__r.Job_Number__c +'_'+ objInvoiceSelected.Opportunity__r.name  + '.pdf';
                }
                else{
                    attach.Name =IsDraftOrApproved+'_'+objInvoiceSelected.Opportunity__r.Job_Number__c +'_'+ objInvoiceSelected.Assigned_Invoice_Number__c +'_'+ objInvoiceSelected.Opportunity__r.name  + '.pdf';
                }
                attach.IsPrivate = false;
                // attach the pdf to the account
                attach.ParentId = objInvoiceSelected.id;
                lstToupdateattach.add(attach);
            }
        }
        try{
            if(!lstToupdateattach.isEmpty()){
                insert lstToupdateattach;
            }
            if(!lstToupdate.isEmpty()){
                update lstToupdate;
            }
            
        } catch (Exception e) {
            ApexPages.addMessages(e);
        }
        List<Attachment> lstTodisplay = new List<Attachment>();
        lstTodisplay = [SELECT name,Id,ParentID FROM Attachment WHERE ParentID IN: lstofids ];
        if(!lstTodisplay.isEmpty()){
            for(Attachment objattach :lstTodisplay ){
                invoiceToattach.put(objattach.ParentID,objattach.Id);
            }
        }
    }
    /*public class pdfwrapper{
        public List<string> Remitence{get;set;}
        public Boolean isCapital{get;set;}
        public date todaysdate{get;set;}
        public string IsDraftOrApproved{get;set;}
        public Invoice__c invobj {get;set;}
        public string Paymentfoot{get;set;}
        public string Footer{get;set;}
        public string FooterMsg{get;set;}
        public Integer index{get;set;}
        public string PoRef{get;set;}
        public string currencysymbiol{get;set;}
        public List<Invoice_Line_Item__c>FeeList{get;set;}
        public List<Invoice_Line_Item__c>CostList{get;set;}
        
        public pdfwrapper(){

        }
    }*/

}