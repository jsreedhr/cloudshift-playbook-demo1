/**
* Author : Nideesh N
* Date : 01-03-2018
* Description : Extension for wkw banner
* Name : WKWBannerExtension
**/
public with sharing class WKWBannerExtension {
    // private contact id
    private final String contactId;
    // standard controller constructor
    public WKWBannerExtension(ApexPages.StandardController stdController){
        this.contactId = stdController.getId();
    }
    
    public Boolean getWkW(){
        Contact cont = [select id,(select id from Who_Knows_Who__r) from contact where id=:contactId];
        if(cont != null && cont.Who_Knows_Who__r != null && !cont.Who_Knows_Who__r.isEmpty()){
            return false;
        }else{
            return true;
        }
    }
}