@isTest 
private class Test_allocationTriggerHandler
{
  static Account accountObj;
  static Opportunity oppObj;
  static Allocation__c allocObj;
  static Forecasting__c forecastObj;
  static Staff__c staffObj;
  static Coding_Structure__c codestructObj;
  static Coding_Structure__c codestructObjnew;
  static void createData()
  {
    accountObj = TestObjectHelper.createAccount();
    insert accountObj;
    System.assert(accountObj.id!=NULL);
    Schema.DescribeFieldResult fieldResult = Coding_Structure__c.Department__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    Schema.DescribeFieldResult fieldResultofffice = Coding_Structure__c.Office__c.getDescribe();
    List<Schema.PicklistEntry> pleoffice = fieldResultofffice.getPicklistValues();
    oppObj = TestObjectHelper.createOpportunity(accountObj);
    insert oppObj;
    System.assert(accountObj.id!=NULL);
    staffobj = TestObjectHelper.createStaff();
    staffobj.active__c = true;
    insert staffobj;
    Work_Type__c workObj = new Work_Type__c();
    workObj.Name = 'worktype';
    workObj.Active__c = true;
    insert workObj;
    codestructObj = new Coding_Structure__c();
    if(!ple.isEmpty()){
        codestructObj.Department__c = ple[0].getValue();    
    }
    if(!pleoffice.isEmpty()){
        codestructObj.Office__c = pleoffice[0].getValue();    
    }
    //
    //codestructObj.Department__c = 'DepartMent';
    codestructObj.Work_Type__c = workObj.Id;
    codestructObj.Staff__c = staffobj.Id;
    codestructObj.Status__c = 'Active';
    insert codestructObj;
    codestructObjnew = new Coding_Structure__c();
    if(!ple.isEmpty()){
        codestructObjnew.Department__c = ple[0].getValue();    
    }
    if(!pleoffice.isEmpty()){
        codestructObjnew.Office__c = pleoffice[0].getValue();    
    }
    //
    //codestructObj.Department__c = 'DepartMent';
    codestructObjnew.Work_Type__c = workObj.Id;
    codestructObjnew.Staff__c = staffobj.Id;
    codestructObjnew.Status__c = 'Active';
    insert codestructObjnew;
    System.assert(staffobj.id!=NULL);
    allocObj = TestObjectHelper.createAllocation(oppObj,staffobj);
    allocObj.Allocation_Amount__c=100;
    if(!ple.isEmpty()){
        allocobj.Department_Allocation__c = ple[0].getValue();    
    }
    if(!pleoffice.isEmpty()){
        allocobj.Office__c = pleoffice[0].getValue();    
    }
    allocobj.workType_Allocation__c = workObj.Name;
    //allocObj.Job__c = oppObj.Id;
    //allocObj.Assigned_To__c = staffobj.Id;
    //allocObj.Coding_Structure__c = codestructObj.Id;
    insert allocObj;
    checkRecursiveAllocation.run = true;
    allocObj.Complete__c=true;
   
    update allocObj;
    System.assert(allocObj.id!=NULL);
    forecastObj = TestObjectHelper.createForecast(allocObj);
    forecastObj.Amount__c=100;
    //forecastObj.Accrued_Amount__c=100;
    //forecastObj.Invoiced_Amount__c=100;
    insert forecastObj;
    System.assert(forecastObj.id!=NULL);


  }
  @isTest
  static void testmethod1()
  {
    createData();
    Opportunity oppCheck = [Select id,Forecasted_Fee_Total__c,Original_Fee_Total__c,
                Total_Accrued_Amount__c,Total_Invoice_Amount__c,(select Total_Amount__c from allocations__r where id =:allocObj.id LIMIT 1)
                from Opportunity
                where ID =:oppObj.id LIMIT 1];
    System.assertEquals(100,oppCheck.allocations__r[0].Total_Amount__c);
    //System.assertEquals(100,oppCheck.Original_Fee_Total__c);
    //System.assertEquals(100,oppCheck.Total_Accrued_Amount__c);
   // System.assertEquals(100,oppCheck.Total_Invoice_Amount__c);

    allocObj.Allocation_Amount__c=200;
    update allocObj;
    oppCheck = [Select id,Original_Fee_Total__c
          from Opportunity
          where ID =:oppObj.id LIMIT 1];
    //System.assertEquals(200,oppCheck.Original_Fee_Total__c);

    delete allocObj;
    oppCheck = [Select id,Original_Fee_Total__c
          from Opportunity
          where ID =:oppObj.id LIMIT 1];
    //System.assertEquals(0,oppCheck.Original_Fee_Total__c);

    undelete allocObj;
    oppCheck = [Select id,Original_Fee_Total__c
          from Opportunity
          where ID =:oppObj.id LIMIT 1];
   // System.assertEquals(200,oppCheck.Original_Fee_Total__c);

  }
  @isTest
  static void testmethod2()
  {
    createData();
    Opportunity oppCheck = [Select id,Forecasted_Fee_Total__c,Original_Fee_Total__c,
                Total_Accrued_Amount__c,Total_Invoice_Amount__c,(select Total_Amount__c,Complete__c from allocations__r where id =:allocObj.id LIMIT 1)
                from Opportunity
                where ID =:oppObj.id LIMIT 1];
    //System.assertEquals(100,oppCheck.allocations__r[0].Total_Amount__c);
    //System.assertEquals(100,oppCheck.Original_Fee_Total__c);
    //System.assertEquals(100,oppCheck.Total_Accrued_Amount__c);
   // System.assertEquals(100,oppCheck.Total_Invoice_Amount__c);

    allocObj.Complete__c=true;
    update allocObj;
    oppCheck = [Select id,Original_Fee_Total__c
          from Opportunity
          where ID =:oppObj.id LIMIT 1];
    //System.assertEquals(200,oppCheck.Original_Fee_Total__c);

    delete allocObj;
    oppCheck = [Select id,Original_Fee_Total__c
          from Opportunity
          where ID =:oppObj.id LIMIT 1];
    //System.assertEquals(0,oppCheck.Original_Fee_Total__c);

    undelete allocObj;
    oppCheck = [Select id,Original_Fee_Total__c
          from Opportunity
          where ID =:oppObj.id LIMIT 1];
   // System.assertEquals(200,oppCheck.Original_Fee_Total__c);

  }

  @isTest(seeAllData=false) 
  private static void test_AllocationSharing(){
    //full tests for custom sharing functionality are located in SharingEngineTest; these are just basic tests for trigger coverage
    ConfigurationManager configManager = ConfigurationManager.getInstance();
    configManager.turnSharingOff();
    configManager.commitConfigurationChanges();

    Group testGroup = new Group(
      Name = 'testGroup'
    );
    Database.Insert(testGroup, true);

    Account a = TestObjectHelper.createAccount();
    Database.Insert(a, true);

    Opportunity o = TestObjectHelper.createOpportunity(a);
    Database.Insert(o, true);

    Staff__c staff = TestObjectHelper.createStaff();
    staff.active__c = true;
    Database.Insert(staff, true);

    Allocation__c allocationObj = TestObjectHelper.createAllocation(o, staff);
    allocationObj.externalCompany__c = a.Id;
    Database.Insert(allocationObj, true);

    Sharing_Rule__c s = new Sharing_Rule__c(
      Name = 'AllocationTestRule',
      Object__c = 'Allocation__c',
      Description__c = 'Apex TEST rule',
      Access_Level__c = SharingEngine.SHARING_ACCESS_EDIT,
      Logic__c = SharingEngine.SHARING_LOGIC_ANY,
      Shared_With__c = testGroup.Id
    );
    Database.Insert(s, true);

    Sharing_Condition__c sc = new Sharing_Condition__c(
      Sharing_Rule__c = s.Id,
      Field_API_Name__c = 'externalCompany__c',
      Operator__c = SharingEngine.SHARING_CONDITION_EQUALS,
      Value__c = a.Id,
      Index__c = 1
    );
    Database.Insert(sc, true);

    s.Change_Status__c = SharingEngine.SHARING_RULE_CHANGE_STATUS_COMPLETED;
    Database.Update(s, true);

    configManager.turnSharingOn();
    configManager.commitConfigurationChanges();

    Test.startTest();
      allocationObj.Allocation_Amount__c = 10000;
      Database.Update(allocationObj, true);
    Test.stopTest();

    List<Error_Log__c>errors = new List<Error_Log__c>([SELECT Details__c FROM Error_Log__c WHERE Log_Type__c='Sharing Error']);
    System.assertEquals(0, errors.size(), 'There should be no error log entries. Actual: '+errors);

    List<Allocation__Share>sh = [SELECT Id FROM Allocation__Share WHERE RowCause=:SharingEngine.SHARING_ROW_CAUSE_CUSTOM_OBJECTS AND UserOrGroupId=:testGroup.Id AND ParentId=:allocationObj.Id];
    System.assertEquals(1, sh.size(), 'Number of manual share records for given allocation should be 1 - one record was inserted via custom sharing functionality.');
  }
}