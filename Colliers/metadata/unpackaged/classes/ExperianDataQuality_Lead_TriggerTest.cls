@isTest
public with sharing class ExperianDataQuality_Lead_TriggerTest {
	public static testmethod void InsertLeadTest() {
    	Lead lead = new Lead(Company='QAS Test Lead', LastName='Smith', Street = 'Hamilton Ave', Country = 'US',
                             State = 'NY', PostalCode = '10606', City = 'White Plains');
		insert lead;        
    }
}