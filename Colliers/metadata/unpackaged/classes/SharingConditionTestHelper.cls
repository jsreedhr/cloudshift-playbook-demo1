/**
 *  Class Name: SharingConditionTestHelper
 *  Description: this helper class will be responsable for creating sharing Condition sobjects, 
 *  it will default any required fields with constants
 *  Company: CloudShift
 *  CreatedDate: 29/05/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Gavin Palmer             29/05/2018                 Orginal Version
 *
 */
@isTest
public class SharingConditionTestHelper {

    public static String SHARING_RULE_ID;
    public static String FIELD_API_NAME = 'Name';
    public static String OPERATOR = SharingEngine.SHARING_CONDITION_EQUALS;
    public static String VALUE = 'gavin';

    public static Sharing_Condition__c getSharingCondition() {
        return new Sharing_Condition__c(
            Sharing_Rule__c = SHARING_RULE_ID,
            Field_API_Name__c = FIELD_API_NAME,
            Operator__c = OPERATOR,
            Value__c = VALUE
        );
    }

    public static Sharing_Condition__c insertSharingCondition() {
        Sharing_Condition__c sharingConditionToInsert = getSharingCondition();
        sharingConditionToInsert.Index__c = 1;
        insert sharingConditionToInsert;
        return sharingConditionToInsert;
    }

    public static List<Sharing_Condition__c> getMultipleSharingConditions(Integer numberToCreate) {
        List<Sharing_Condition__c> sharingConditionsToReturn = new List<Sharing_Condition__c>();
        String originalValue = VALUE;
        for (Integer currentIndex = 0; currentIndex < numberToCreate; currentIndex++) {
            VALUE = originalValue + currentIndex;
            Sharing_Condition__c sharingConditionToReturn = getSharingCondition();
            sharingConditionToReturn.Index__c = currentIndex+1;
            sharingConditionsToReturn.add(sharingConditionToReturn);
        }
        return sharingConditionsToReturn;
    }

    public static List<Sharing_Condition__c> insertMultipleSharingConditions(Integer numberToInsert) {
        List<Sharing_Condition__c> sharingConditionsToInsert = getMultipleSharingConditions(numberToInsert);
        insert sharingConditionsToInsert;
        return sharingConditionsToInsert;
    }
}