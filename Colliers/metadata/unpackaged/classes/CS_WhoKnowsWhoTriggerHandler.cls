/**
*  Class Name: CS_WhoKnowsWhoTriggerHandler  
*  Description: This is a Trigger Handler Class for trigger on Who_knows_who
*  Company: dQuotient
*  CreatedDate:23/12/2016 
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------  
*  Anand              23/12/2016                 Orginal Version
*  Asif               28/12/2016                 Added Methods for delete,Undelete
*/
public class CS_WhoKnowsWhoTriggerHandler {

    /**
    *  Method Name: insertFollowContactForUser 
    *  Description: Method for the Staff to follow associated Contact Record
    *  Param:  Trigger.new
    *  Return: None
    */
    public static void insertFollowContactForUser(List<Who_Knows_Who__c> lstWhoKnowsWho) {
        
        List<Who_Knows_Who__c> lstWhoKnowsWhoUpdate = new List<Who_Knows_Who__c>();
        set<id> setWhoKnowsWho = new set<id>();
        set<id> setIdContactsFollow = new set<id>();
        for (Who_Knows_Who__c objWhoKnowsWho: lstWhoKnowsWho) {
            
            if(objWhoKnowsWho.Sync_with_Outlook__c){
                setWhoKnowsWho.add(objWhoKnowsWho.id);
                lstWhoKnowsWhoUpdate.add(objWhoKnowsWho);
                setIdContactsFollow.add(objWhoKnowsWho.Contact__c);
            }
        }
        if(!setWhoKnowsWho.isEmpty()){
            map<id, Who_Knows_Who__c> mapIdStaff = new map<id, Who_Knows_Who__c>([Select id,Staff_User_Id__c
                                                                            From
                                                                            Who_Knows_Who__c 
                                                                            where id in: setWhoKnowsWho]);
    
            List<EntitySubscription> lstEntitySubscriptionCurrent = new List<EntitySubscription>();
            map<id,set<id>> mapParentSubscriber = new map<id,set<id>>();
            
            lstEntitySubscriptionCurrent = [Select id, SubscriberId,
                                                    ParentId
                                                    From
                                                    EntitySubscription
                                                    where ParentId in: setIdContactsFollow];
            
            for(EntitySubscription objEntitySubscriptionCurrent: lstEntitySubscriptionCurrent){
                
                if(mapParentSubscriber.containsKey(objEntitySubscriptionCurrent.ParentId)){
                    mapParentSubscriber.get(objEntitySubscriptionCurrent.ParentId).add(objEntitySubscriptionCurrent.SubscriberId);
                }else{
                    mapParentSubscriber.put(objEntitySubscriptionCurrent.ParentId, new set<id>{objEntitySubscriptionCurrent.SubscriberId});
                }
                
            }
                                                                            
            List<EntitySubscription> lstEntitySubscription = new List<EntitySubscription>();
            
            for (Who_Knows_Who__c objWhoKnowsWhoUpdate: lstWhoKnowsWhoUpdate) {
                if(mapParentSubscriber.containsKey(objWhoKnowsWhoUpdate.Contact__c) && !mapParentSubscriber.get(objWhoKnowsWhoUpdate.Contact__c).contains(objWhoKnowsWhoUpdate.Staff_User_Id__c)){
                    if(mapIdStaff.containsKey(objWhoKnowsWhoUpdate.id) && !String.isBlank(mapIdStaff.get(objWhoKnowsWhoUpdate.id).Staff_User_Id__c)){
                        EntitySubscription objEntitySubscription = new EntitySubscription();
                        objEntitySubscription.SubscriberId = mapIdStaff.get(objWhoKnowsWhoUpdate.id).Staff_User_Id__c;
                        objEntitySubscription.ParentId = objWhoKnowsWhoUpdate.Contact__c;
                        lstEntitySubscription.add(objEntitySubscription);
                    }
                }else if(!mapParentSubscriber.containsKey(objWhoKnowsWhoUpdate.Contact__c)){
                    if(mapIdStaff.containsKey(objWhoKnowsWhoUpdate.id) && !String.isBlank(mapIdStaff.get(objWhoKnowsWhoUpdate.id).Staff_User_Id__c)){
                        EntitySubscription objEntitySubscription = new EntitySubscription();
                        objEntitySubscription.SubscriberId = mapIdStaff.get(objWhoKnowsWhoUpdate.id).Staff_User_Id__c;
                        objEntitySubscription.ParentId = objWhoKnowsWhoUpdate.Contact__c;
                        lstEntitySubscription.add(objEntitySubscription);
                    }
                }
            }
            
            try{
                if(!lstEntitySubscription.isEmpty()){
                    insert lstEntitySubscription;
                }
            }catch(exception e){
                System.debug('---->'+e.getMessage());
            }
        
        }   
    }
    
    
    /**
    *  Method Name: updateFollowContactForUser 
    *  Description: Method for the Staff to follow associated Contact Record
    *  Param:  Trigger.new
    *  Return: None
    */
    public static void updateFollowContactForUser(map<id, Who_Knows_Who__c > mapOldWhoKnowsWho, map<id, Who_Knows_Who__c> mapNewWhoKnowsWho) {
        
        List<Who_Knows_Who__c> lstWhoKnowsWhoToFollow = new List<Who_Knows_Who__c>();
        map<id,set<id>> mapContactStaffToUnFollow = new map<id,set<id>>();
        set<id> setIdContactsFollow = new set<id>();
        for(Who_Knows_Who__c objWhoKnowsWho: mapNewWhoKnowsWho.values()){
            
            if((objWhoKnowsWho.Sync_with_Outlook__c != mapOldWhoKnowsWho.get(objWhoKnowsWho.id).Sync_with_Outlook__c) && !String.isBlank(objWhoKnowsWho.Staff_User_Id__c)){
                
                if(objWhoKnowsWho.Sync_with_Outlook__c){
                    lstWhoKnowsWhoToFollow.add(objWhoKnowsWho);
                    setIdContactsFollow.add(objWhoKnowsWho.Contact__c);
                }else{
                    if(mapContactStaffToUnFollow.containsKey(objWhoKnowsWho.Contact__c)){
                        mapContactStaffToUnFollow.get(objWhoKnowsWho.Contact__c).add(objWhoKnowsWho.Staff_User_Id__c);
                    }else{
                        mapContactStaffToUnFollow.put(objWhoKnowsWho.Contact__c, new set<id>{objWhoKnowsWho.Staff_User_Id__c});
                    }
                }
            }
        }
        
        if(!mapContactStaffToUnFollow.isEmpty()){
            List<EntitySubscription> lstEntitySubscriptionTotal = new List<EntitySubscription>();
            List<EntitySubscription> lstEntitySubscriptionDel = new List<EntitySubscription>();
            lstEntitySubscriptionTotal = [Select id, SubscriberId,
                                                    ParentId
                                                    From
                                                    EntitySubscription
                                                    where ParentId in: mapContactStaffToUnFollow.keyset()];
                                                    
            for(EntitySubscription objEntitySubscriptionTotal: lstEntitySubscriptionTotal){
                
                if(mapContactStaffToUnFollow.get(objEntitySubscriptionTotal.ParentId).contains(objEntitySubscriptionTotal.SubscriberId)){
                    lstEntitySubscriptionDel.add(objEntitySubscriptionTotal);
                }
            }
            try{
                if(!lstEntitySubscriptionDel.isEmpty()){
                    delete lstEntitySubscriptionDel;
                }
            }catch(exception e){
                System.debug('---->'+e.getMessage());
            }
        }
        
        if(!lstWhoKnowsWhoToFollow.isEmpty()){
            List<EntitySubscription> lstEntitySubscriptionCurrent = new List<EntitySubscription>();
            map<id,set<id>> mapParentSubscriber = new map<id,set<id>>();
            
            lstEntitySubscriptionCurrent = [Select id, SubscriberId,
                                                    ParentId
                                                    From
                                                    EntitySubscription
                                                    where ParentId in: setIdContactsFollow];
            
            for(EntitySubscription objEntitySubscriptionCurrent: lstEntitySubscriptionCurrent){
                
                if(mapParentSubscriber.containsKey(objEntitySubscriptionCurrent.ParentId)){
                    mapParentSubscriber.get(objEntitySubscriptionCurrent.ParentId).add(objEntitySubscriptionCurrent.SubscriberId);
                }else{
                    mapParentSubscriber.put(objEntitySubscriptionCurrent.ParentId, new set<id>{objEntitySubscriptionCurrent.SubscriberId});
                }
                
            }
            
            
            
            List<EntitySubscription> lstEntitySubscriptionToFollow = new List<EntitySubscription>();
            for(Who_Knows_Who__c objWhoKnowsWhoToFollow: lstWhoKnowsWhoToFollow){
                
                if(mapParentSubscriber.containsKey(objWhoKnowsWhoToFollow.Contact__c) && !mapParentSubscriber.get(objWhoKnowsWhoToFollow.Contact__c).contains(objWhoKnowsWhoToFollow.Staff_User_Id__c)){
                    EntitySubscription objEntitySubscriptionToFollow = new EntitySubscription();
                    objEntitySubscriptionToFollow.SubscriberId = objWhoKnowsWhoToFollow.Staff_User_Id__c;
                    objEntitySubscriptionToFollow.ParentId = objWhoKnowsWhoToFollow.Contact__c;
                    lstEntitySubscriptionToFollow.add(objEntitySubscriptionToFollow);
                }else if(!mapParentSubscriber.containsKey(objWhoKnowsWhoToFollow.Contact__c)){
                    EntitySubscription objEntitySubscriptionToFollow = new EntitySubscription();
                    objEntitySubscriptionToFollow.SubscriberId = objWhoKnowsWhoToFollow.Staff_User_Id__c;
                    objEntitySubscriptionToFollow.ParentId = objWhoKnowsWhoToFollow.Contact__c;
                    lstEntitySubscriptionToFollow.add(objEntitySubscriptionToFollow);
                }
            }
            
            try{
                if(!lstEntitySubscriptionToFollow.isEmpty()){
                    insert lstEntitySubscriptionToFollow;
                }
            }catch(exception e){
                System.debug('---->'+e.getMessage());
            }
        }
    }

     /**
    *  Method Name: deleteFollowContactForUser 
    *  Description: Method for the Staff to unfollow associated Contact Record
    *  Param:  Trigger.old
    *  Return: None
    */
    public static void deleteFollowContactForUser(map<id, Who_Knows_Who__c > mapOldWhoKnowsWho)
    {
        map<id,set<id>> mapContactStaffToUnFollow = new map<id,set<id>>();
        for(Who_Knows_Who__c objWhoKnowsWho : mapOldWhoKnowsWho.values())
        {
            if(objWhoKnowsWho.Sync_with_Outlook__c)
            {
                if(mapContactStaffToUnFollow.containsKey(objWhoKnowsWho.Contact__c))
                    mapContactStaffToUnFollow.get(objWhoKnowsWho.Contact__c).add(objWhoKnowsWho.Staff_User_Id__c);
                else
                    mapContactStaffToUnFollow.put(objWhoKnowsWho.Contact__c, new set<id>{objWhoKnowsWho.Staff_User_Id__c});
            }
        }
        if(!mapContactStaffToUnFollow.isEmpty())
        {
            List<EntitySubscription> lstEntitySubscriptionTotal = new List<EntitySubscription>();
            List<EntitySubscription> lstEntitySubscriptionDel = new List<EntitySubscription>();
            lstEntitySubscriptionTotal = [Select id, SubscriberId,
                                                    ParentId
                                                    From
                                                    EntitySubscription
                                                    where ParentId in: mapContactStaffToUnFollow.keyset()];
                                                    
            for(EntitySubscription objEntitySubscriptionTotal: lstEntitySubscriptionTotal)
            {
                if(mapContactStaffToUnFollow.get(objEntitySubscriptionTotal.ParentId).contains(objEntitySubscriptionTotal.SubscriberId))
                    lstEntitySubscriptionDel.add(objEntitySubscriptionTotal);
            }
            try{
                if(!lstEntitySubscriptionDel.isEmpty()){
                    delete lstEntitySubscriptionDel;
                }
            }catch(exception e)
            {
                System.debug('---->'+e.getMessage());
            }
        }
    }
     /**
    *  Method Name: undeleteFollowContactForUser 
    *  Description: Method for the Staff to follow associated Contact Record
    *  Param:  Trigger.old
    *  Return: None
    */
    public static void undeleteFollowContactForUser(List<Who_Knows_Who__c> lstWhoKnowsWho)
    {
        List<Who_Knows_Who__c> lstWhoKnowsWhoUpdate = new List<Who_Knows_Who__c>();
        set<id> setWhoKnowsWho = new set<id>();
        set<id> setIdContactsFollow = new set<id>();
        for (Who_Knows_Who__c objWhoKnowsWho: lstWhoKnowsWho) {
            
            if(objWhoKnowsWho.Sync_with_Outlook__c){
                setWhoKnowsWho.add(objWhoKnowsWho.id);
                lstWhoKnowsWhoUpdate.add(objWhoKnowsWho);
                setIdContactsFollow.add(objWhoKnowsWho.Contact__c);
            }
        }
        if(!setWhoKnowsWho.isEmpty()){
            map<id, Who_Knows_Who__c> mapIdStaff = new map<id, Who_Knows_Who__c>([Select id,Staff_User_Id__c
                                                                            From
                                                                            Who_Knows_Who__c 
                                                                            where id in: setWhoKnowsWho]);
            
            List<EntitySubscription> lstEntitySubscriptionCurrent = new List<EntitySubscription>();
            map<id,set<id>> mapParentSubscriber = new map<id,set<id>>();
            
            lstEntitySubscriptionCurrent = [Select id, SubscriberId,
                                                    ParentId
                                                    From
                                                    EntitySubscription
                                                    where ParentId in: setIdContactsFollow];
            
            for(EntitySubscription objEntitySubscriptionCurrent: lstEntitySubscriptionCurrent){
                
                if(mapParentSubscriber.containsKey(objEntitySubscriptionCurrent.ParentId)){
                    mapParentSubscriber.get(objEntitySubscriptionCurrent.ParentId).add(objEntitySubscriptionCurrent.SubscriberId);
                }else{
                    mapParentSubscriber.put(objEntitySubscriptionCurrent.ParentId, new set<id>{objEntitySubscriptionCurrent.SubscriberId});
                }
                
            }
                                                                            
            List<EntitySubscription> lstEntitySubscription = new List<EntitySubscription>();
            
            for (Who_Knows_Who__c objWhoKnowsWhoUpdate: lstWhoKnowsWhoUpdate) {
                if(mapParentSubscriber.containsKey(objWhoKnowsWhoUpdate.Contact__c) && !mapParentSubscriber.get(objWhoKnowsWhoUpdate.Contact__c).contains(objWhoKnowsWhoUpdate.Staff_User_Id__c)){
                    if(mapIdStaff.containsKey(objWhoKnowsWhoUpdate.id) && !String.isBlank(mapIdStaff.get(objWhoKnowsWhoUpdate.id).Staff_User_Id__c)){
                        EntitySubscription objEntitySubscription = new EntitySubscription();
                        objEntitySubscription.SubscriberId = mapIdStaff.get(objWhoKnowsWhoUpdate.id).Staff_User_Id__c;
                        objEntitySubscription.ParentId = objWhoKnowsWhoUpdate.Contact__c;
                        lstEntitySubscription.add(objEntitySubscription);
                    }
                }else if(!mapParentSubscriber.containsKey(objWhoKnowsWhoUpdate.Contact__c)){
                    if(mapIdStaff.containsKey(objWhoKnowsWhoUpdate.id) && !String.isBlank(mapIdStaff.get(objWhoKnowsWhoUpdate.id).Staff_User_Id__c)){
                        EntitySubscription objEntitySubscription = new EntitySubscription();
                        objEntitySubscription.SubscriberId = mapIdStaff.get(objWhoKnowsWhoUpdate.id).Staff_User_Id__c;
                        objEntitySubscription.ParentId = objWhoKnowsWhoUpdate.Contact__c;
                        lstEntitySubscription.add(objEntitySubscription);
                    }
                }
            }
            
            try{
                if(!lstEntitySubscription.isEmpty()){
                    insert lstEntitySubscription;
                }
            }catch(exception e){
                System.debug('---->'+e.getMessage());
            }
        
        }        
    }
}