Public  with sharing class uniquestringHelperclass{
 
 
 public static  set<string>uniquestring;
 public static void oninsert(List<allocation__c> alloList){
 
    list<allocation__c> existingAllo = new list<allocation__c>();
    list<Coding_Structure__c> lstofcodstruct = new list<Coding_Structure__c>();
    Map<id,list<allocation__c>> jobMap = new Map<id,list<allocation__c>>();
    Map<string,Id> StringtoId = new Map<string,Id>();
    Set<id> ExistingAlloc = new Set<id> ();
    set<id> jobid = new set<id>();
    set<id> surveyorId = new set<id>();
    set<id> setAssignTo = new set<id>();
    set<string> codstructstring = new set<string>();
    //if(uniquestring==null){
        uniquestring = new set<string>();
    //}
    for(Allocation__c objaloc : alloList){
        if(objaloc.Assigned_To__c != null && string.isNotBlank(objaloc.Assigned_To__c)){
            surveyorId.add(objaloc.Assigned_To__c);
        }
    }
    lstofcodstruct = [SELECT Id,Department__c,Office__c,Work_Type__c,Staff__c,Work_Type__r.Name FROM Coding_Structure__c WHERE Staff__c IN:surveyorId];
    if(!lstofcodstruct.isEmpty()){
        for(Coding_Structure__c obj :lstofcodstruct){
            String mapkey = generateUniquecodingString(obj);
            system.debug('codekey'+mapkey);
            if(string.isNotBlank(mapkey)){
                StringtoId.put(mapkey,obj.Id);
            }           
        }
    }
 
     for(allocation__c a: alloList){
        if(a.externalCompany__c == null){
            string uniallostring = generateUniqueString(a);
            system.debug('allokey'+uniallostring);
            if(string.isNotBlank(uniallostring)){
                system.debug('allokey'+uniallostring);
                if(StringtoId.containsKey(uniallostring)){
                    system.debug('allokey'+uniallostring);
                    a.Coding_Structure__c = StringtoId.get(uniallostring);
                }
            }
              if(uniquestring.contains(uniallostring)){
                  a.adderror('Allocation Record already exists for this Surveyor, Department, Office and WorkType');
              }
              else
                 uniquestring.add(uniallostring);
             
             jobId.add(a.job__c);
             if(a.Assigned_To__c != null){
                setAssignTo.add(a.Assigned_To__c);
             }
         
        }
        if(a.id!=null){
            ExistingAlloc.add(a.id);
        }
     }
    if(!ExistingAlloc.isEmpty()){
        existingAllo = [select allocationstring__c, job__c from allocation__c where job__c in :jobid and externalCompany__c = null and id NOT IN: ExistingAlloc AND Complete__c = false and Assigned_To__c in:setAssignTo and Assigned_To__c != null ];
    }
    List <allocation__c> jobMapList;
    for(allocation__c a :existingAllo ){
    
        if (jobMap != null && !jobMap.isEmpty() && jobMap.containsKey(a.job__c)){
              jobMap.get(a.job__c).add(a);
        
        } 
        else{
              jobMapList = new List <allocation__c> ();
              jobMapList.add(a);
              jobMap.put(a.job__c, jobMapList );
        }
    
    }
    String allostring;
    for(allocation__c a: alloList){
        if(a.externalCompany__c == null){
            allostring = generateUniqueString(a);
            system.debug('checkstring'+allostring );
            if(jobMap.get(a.job__c)!=null){
                for(allocation__c allo : jobMap.get(a.job__c)){
                    system.debug('checkstring -----'+allo.allocationstring__c);
                       
                
                    if(allo.allocationstring__c.equals(allostring)){
                        a.adderror('Allocation Record already exists');
                    }
                }
            }
        }
        
    
 
    }

}
    // Logic to generate the Unique Comparisson string
    public static string generateUniqueString(allocation__c a){
        string assignedid = string.valueof(a.Assigned_To__c);
        string allostring;
        if(!String.isBlank(assignedid)){
            assignedid = assignedid.substring(0,15);
            allostring = assignedid+'-' ;           
        }else{
            allostring ='-';
        }
        
        if(!string.isblank(a.Department_Allocation__c)){
            allostring+=a.Department_Allocation__c+'-';
            
        }
        if(!string.isblank(a.Office__c)){
           allostring+=a.Office__c+'-';
            
        }
         if(!string.isblank(a.workType_Allocation__c )){
              allostring+=a.workType_Allocation__c ;
             
         }
         return allostring;
    }
    public static string generateUniquecodingString(Coding_Structure__c cod){
        string assignedid = string.valueof(cod.Staff__c); 
        assignedid = assignedid.substring(0,15);
        string codestring = assignedid+'-' ;
        if(!string.isblank(cod.Department__c)){
            codestring+=cod.Department__c+'-';
            
        }
        if(!string.isblank(cod.Office__c)){
           codestring+=cod.Office__c+'-';
            
        }
         if(!string.isblank(cod.Work_Type__r.Name )){
              codestring+=cod.Work_Type__r.Name ;
             
         }
         return codestring;
    }


public static void onupdate(List<allocation__c> alloList){
 
  list<allocation__c> existingAllo = new list<allocation__c>();
  Map<id,list<allocation__c>> jobMap = new Map<id,list<allocation__c>>();
  set<id> jobid = new set<id>();
  if(uniquestring==null)
        uniquestring = new set<string>();

 
     for(allocation__c a: alloList)
         if(a.externalCompany__c == null){
         {    
            string uniallostring = generateUniqueString(a);
              if(uniquestring.contains(uniallostring)){
                  a.adderror('Allocation Record already exists');
              }
              else{
                uniquestring.add(uniallostring);
                jobId.add(a.job__c); 
              }
             
         }
        }
    }
}