/**
*  Class Name: SharingRuleTriggerHandlerTest
*  Description: Test class for SharingRuleTriggerHandler which handles insert/update/delete operations for Sharing_Rule__c object
*  
*  Company: CloudShift
*  CreatedDate: 02/07/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  ----------------------------------------------------------- 
*  Kaspars Rezgalis		02/07/2018					Created
*
*/
@isTest(seeAllData=false)
private class SharingRuleTriggerHandlerTest{
	private static Map<String, SObject> setupSharedTestData(){
		Map<String, SObject> retValues = new Map<String, SObject>();

		AssignId__c assignId = TestObjectHelper.createAssignId();
		assignId.AssignNo__c = 1;
		AssignAccountId__c assignAccId = TestObjectHelper.createAssignAccCode();
		Job_Amount__c jobAmount = TestObjectHelper.createJobAmount();
		colliers_Vat_Number__c vatNum = (colliers_Vat_Number__c)SmartFactory.createSObject('colliers_Vat_Number__c',false);
		currency_Symbol__c curr= (currency_Symbol__c)SmartFactory.createSObject('currency_Symbol__c',false);

		Bypass_Configuration__c setting = new Bypass_Configuration__c(
			Is_Sharing_Off__c = false,
			SetupOwnerId = UserInfo.getUserId()
		);

		Group testGroup = new Group(
			Name = 'testGroup'
		);

		Database.Insert(new List<SObject>{assignId, assignAccId, jobAmount, vatNum, curr, setting, testGroup}, true);
		retValues.put('Group', testGroup);

		Account a = TestObjectHelper.createAccount();
		a.Name = 'SharingTestAccount';
		Database.Insert(a, true);
		retValues.put('Account', a);

		Opportunity o = TestObjectHelper.createOpportunity(a);
		o.Name = 'opp1';
		Database.Insert(o, true);
		retValues.put('Opportunity', o);

		return retValues;
	}

	@isTest 
	private static void test_errorCheckingorCustomLogicRules(){
		Map<String, SObject> testData = setupSharedTestData();
		Group testGroup = (Group)testData.get('Group');
		Account a = (Account)testData.get('Account');
		Opportunity o = (Opportunity)testData.get('Opportunity');

		Sharing_Rule__c s = new Sharing_Rule__c(
			Name = 'OpportunityTestRule',
			Object__c = 'Opportunity',
			Description__c = 'Apex TEST rule',
			Access_Level__c = SharingEngine.SHARING_ACCESS_EDIT,
			Logic__c = SharingEngine.SHARING_LOGIC_CUSTOM,
			Shared_With__c = testGroup.Id
		);
		Database.Insert(s, true);

		Sharing_Condition__c sc1 = new Sharing_Condition__c(
			Sharing_Rule__c = s.Id,
			Field_API_Name__c = 'AccountId',
			Operator__c = SharingEngine.SHARING_CONDITION_EQUALS,
			Value__c = a.Id,
			Index__c = 1
		);

		Sharing_Condition__c sc2 = new Sharing_Condition__c(
			Sharing_Rule__c = s.Id,
			Field_API_Name__c = 'Id',
			Operator__c = SharingEngine.SHARING_CONDITION_EQUALS,
			Value__c = o.Id,
			Index__c = 2
		);

		Sharing_Condition__c sc3 = new Sharing_Condition__c(
			Sharing_Rule__c = s.Id,
			Field_API_Name__c = 'Name',
			Operator__c = SharingEngine.SHARING_CONDITION_EQUALS,
			Value__c = o.Name,
			Index__c = 3
		);

		Sharing_Condition__c sc4 = new Sharing_Condition__c(
			Sharing_Rule__c = s.Id,
			Field_API_Name__c = 'Name',
			Operator__c = SharingEngine.SHARING_CONDITION_EQUALS,
			Value__c = o.Name,
			Index__c = 4
		);
		Database.Insert(new List<Sharing_Condition__c>{sc1, sc2, sc3, sc4});

		Test.startTest();
			s.Custom_Logic__c = '1 OR 2';
			s.Change_Status__c = SharingEngine.SHARING_RULE_CHANGE_STATUS_COMPLETED;

			Boolean errorTriggered = false;
			try{
				Database.Update(s, true);
			}catch(Exception e){
				System.assert(e.getMessage().contains(SharingRuleTriggerHandler.ERROR_UNUSED_INDEX_IN_CONDITION_RECORDS), 'An error should be thrown - custom logic condition text does not refrence index "3", which exists in sharing criteria records. Actual: ' + e.getMessage());
				errorTriggered = true;
			}
			System.assert(errorTriggered, 'Error should have been triggered - custom logic condition text does not refrence index "3", which exists in sharing criteria records.');

			s.Custom_Logic__c = '1 OR 2 OR 3 OR 4 OR 5';
			errorTriggered = false;

			try{
				Database.Update(s, true);
			}catch(Exception e){
				System.assert(e.getMessage().contains(SharingRuleTriggerHandler.ERROR_INVALID_INDEX_IN_CONDITION_TEXT), 'An error should be thrown - custom logic condition text contains index "5", yet sharing criteria record with such index does not exist. Actual: ' + e.getMessage());
				errorTriggered = true;
			}
			System.assert(errorTriggered, 'Error should have been triggered - custom logic condition text contains index "5", yet sharing criteria record with such index does not exist.');

			s.Custom_Logic__c = '(1 AND 2) OR (3 AND 4)';
			Database.Update(s, true);
			SharingRuleTriggerBatchCtrl.dispatchSharingRulesRecalculation(new Set<String>{s.Object__c});

		Test.stopTest();

		List<Error_Log__c>errors = new List<Error_Log__c>([SELECT Details__c FROM Error_Log__c WHERE Log_Type__c='Sharing Error']);
		System.assertEquals(0, errors.size(), 'There should be no error log entries for sharing error. Actual: '+errors);


		List<OpportunityShare>sh = new List<OpportunityShare>([SELECT Id, OpportunityId FROM OpportunityShare 
									WHERE RowCause=:SharingEngine.SHARING_ROW_CAUSE_STD_OBJECTS AND UserOrGroupId=:testGroup.Id 
									AND OpportunityId=:o.Id]);
		System.assertEquals(1, sh.size(), 'There should be 1 manual share records for the only test opportunity.');
	}
}