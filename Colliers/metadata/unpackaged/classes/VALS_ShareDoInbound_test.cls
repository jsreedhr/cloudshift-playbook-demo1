@isTest
public with sharing class VALS_ShareDoInbound_test
{ 
@isTest
    static void test_Method_doPost() 
    {  
        Profile p = [select id from profile where name='Standard User'];

        User u = new User(alias = 'test123', email='test123@noemailTEST.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id, country='United States',
            timezonesidkey='America/Los_Angeles', username='test123@noemailTEST.com');
        insert u;
        
        System.runAs(u){
        Property__c propertyObj3 = new Property__c();   
      propertyObj3=TestObjectHelper.createProperty();
      propertyObj3.Street_No__c ='Park Lane 2';
      propertyObj3.Post_Code__c ='W1K 3DD';
      propertyObj3.Country__c = 'United Kingdom';
      propertyObj3.Geolocation__Latitude__s=51.51;
      propertyObj3.Geolocation__Longitude__s=-0.15;
      propertyObj3.Override_Experian_validation__c=true;
      insert propertyObj3;
         system.debug('propertyObj3---->'+propertyObj3);
      System.assert(propertyObj3.id!=NULL);
        
         account accountObj = new account();    
      accountObj = TestObjectHelper.createAccount();
      accountObj.PO_Required__c = 'Yes';
      accountObj.Client_VAT_Number__c='123456';
      accountObj.Fax = '56893948920';
      accountObj.Phone = '57698900023';
      accountObj.Name = 'lkj';
     
      insert accountobj;
         system.debug('accountObj---->'+accountObj);
      System.assert(accountObj.id!=NULL);
        
      Address__c addressObj = new Address__c();
      addressObj = TestObjecthelper.createAddress(accountObj);
      addressObj.Country__c = 'United Kingdom';
      insert addressObj;
         system.debug('addressObj---->'+addressObj);
      System.assert(addressObj.id!=NULL);

      WorkType__c CustSetting2 = new WorkType__c();
      Custsetting2.name='TestworkType';
         system.debug('CustSetting2---->'+CustSetting2);
      insert CustSetting2;
      
      PropertyType__c Custsetting= new PropertyType__c();
      Custsetting.name='Test';
         system.debug('Custsetting---->'+Custsetting);
      insert CustSetting;
        
        Contact contactObj = new Contact(); 
      //contactObj = TestObjecthelper.createContact(accountObj);
      contactObj.Email='Cersei@kingslandingTEST.com';
      contactobj.LastName='Lannister';
      contactObj.HomePhone='12345678';
      contactObj.MobilePhone='12345678';
      contactObj.Phone='12345678';
      contactObj.Fax='12345678';
      insert contactObj;
         system.debug('contactObj---->'+contactObj);
      System.assert(contactObj.id!=NULL);
        
      Staff__c staff = new Staff__c();
      staff.Name = 'abc';
        staff.User__c = u.id;
        staff.FIRST_NAME__c = 'test full';
        staff.HR_No__c = 'test';
      insert staff; 
         system.debug('staff---->'+staff);
      System.assert(staff.id!=NULL);  
        
       opportunity oppObj = new opportunity();
      oppObj = TestObjectHelper.createOpportunity(accountObj);
      oppObj.Sent_to_ShareDo__c=True;
      oppObj.StageName='Instructed';
      oppObj.Work_Type__C='TestworkType';
      oppObj.AccountId = accountObj.id;
      oppObj.InstructingCompanyAddress__c = addressObj.id;
      oppObj.Property_Address__c = propertyObj3.id;
      oppObj.Instructing_Contact__c = contactObj.id;
      oppObj.Manager__c = staff.id;
     // oppObj.Work_Category__c = 'Commercial';
     // oppObj.Work_Purpose__c = 'NPL';
      oppObj.Description = 'for test';
      oppObj.Date_Instructed__c = system.today();
      oppObj.Job_Number__c = 'B31637';
      insert oppObj;
        system.debug('oppObj---->'+oppObj);
      System.assert(oppObj.id!=NULL);
        
        
    	Inspection__c inspObj = New Inspection__c();
    	inspObj.Name = 'Test Inspection';
        //inspobj.Property__c = propertyObj3.id;
        inspobj.job__c=oppObj.id;
    	insert inspObj;
         system.debug('inspObj---->'+inspObj);
    	System.assert(inspObj.id!=NULL);
        
    	External_Inspection__c extInspObj = New External_Inspection__c();
    	extInspObj.Inspection__c = inspObj.id;
    	insert extInspObj;
         system.debug('extInspObj---->'+extInspObj);
    	System.assert(extInspObj.id!=NULL);
        
        Surrounding__c surobj = new Surrounding__c();
        Surobj.Inspection__c = inspObj.id;
        insert surobj;
         system.debug('surobj---->'+surobj);
    	System.assert(surobj.id!=NULL);

    	Internal_Inspection__c intInspObj = New Internal_Inspection__c();
    	intInspObj.Inspection__c = inspObj.id;
    	intInspObj.External_Inspection__c = extInspObj.id;
    	insert intInspObj;
         system.debug('intInspObj---->'+intInspObj);
    	System.assert(intInspObj.id!=NULL);
        
       Feeditem fi = new Feeditem();
        fi.body='testData';
        fi.parentid=extInspObj.id;
         system.debug('fi---->'+fi);
        insert fi;
   
    RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/ShareDo/v1';
req.requestBody = Blob.valueOf('{"jobRef":"'+oppObj.id+'","propertyRef":"'+propertyObj3.id+'","requirements":"","inspectionType":"Drive by","inspectionId":"'+inspObj.id+'","inspectionName":"Inspection Required","measurementRequired":"Check","siteContact":"Richard Heath","siteMobile":"9066879509","siteLandline":"080041616161","siteEmail":"pratyusha.kodali@dquotient.com","Participants":[{"_typeTag":"SalesforceInspectionParticipantOutboundRequest","Role":"inspection-contact","HRreference":"test"},{"_typeTag":"SalesforceInspectionParticipantOutboundRequest","Role":"inspection-surveyor","HRreference":"test"}]}');

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        try
        {
        VALS_ShareDoInbound.createRecord();
        }
        catch(Exception e) 
        {
            system.debug(e.getMessage());
        }
        try
        {
        VALS_ShareDoInbound.updateRecord();
        }
        catch(Exception e) 
        {
            system.debug(e.getMessage());
        }
        string test = '12345';
        VALS_ShareDoInbound.personWrapper person = New VALS_ShareDoInbound.personWrapper(test);
       VALS_ShareDoInbound.participantWrapper part = new VALS_ShareDoInbound.participantWrapper(test,test,Person);
        list<VALS_ShareDoInbound.participantWrapper> listpart = new list<VALS_ShareDoInbound.participantWrapper>(); 
       listpart.add(part);
       VALS_ShareDoInbound.mainWrapper testmain =  New VALS_ShareDoInbound.mainWrapper (test,test,test,test,test,test,test,test,test,test,listpart);
       testmain = new  VALS_ShareDoInbound.mainWrapper (test,test,test,test,test,test,test,test,test,test,test,listpart);
        VALS_ShareDoInbound.successWrapper success = New VALS_ShareDoInbound.successWrapper(inspObj.id);
    }
  }
}