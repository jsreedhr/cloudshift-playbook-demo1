@isTest
private class ArchiveAcctConRelCTL_TEST{
    public static testmethod void archiveacctconrelation(){
    AccountContactRelation objacr = new AccountContactRelation();
    Account acc = new Account();
    acc.Name = 'Test Account';
    insert acc;
    
    Account acca = new Account();
    acca.Name = 'Test Account a';
    insert acca;
    
    Contact con = new Contact();
    con.LastName = 'Test Contact';
    con.AccountId = acc.id;
    insert con;
    
    objacr.accountid = acca.id;
    objacr.contactid = con.id;
    insert objacr;
    
    ApexPages.currentPage().getParameters().put('id',String.valueOf(objacr.Id));
    ApexPages.StandardController StdCtl = new ApexPages.StandardController(objacr);
    ArchiveAccountContactRelationshipCTL methodins = new ArchiveAccountContactRelationshipCTL(StdCtl);
    methodins.archiveRelationship();
    }
}