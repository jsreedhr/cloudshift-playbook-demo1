/**
 *  Class Name: CS_CreditNoteTriggerHandler  
 *  Description: This is a Trigger Handler Class for trigger on Credit_Note__c
 *  Company: dQuotient
 *  CreatedDate:30/11/2016 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  JYOTI              30/11/2016                 Orginal Version
 *
 */
public class CS_CreditNoteTriggerHandler {
    
    /**
     *  Method Name: submitCreditNoteForApproval  
     *  Description: Method to submit credit note for approvals on insert
     *  Param:  Trigger.new
     *  Return: None
    */
    
    public static Boolean isInsrt = false;
        
    public static void submitCreditNoteForApproval(List<Credit_Note__c> lstCreditNote){
        
        for (Credit_Note__c objCN: lstCreditNote){
            system.debug('objCN.Status__c--->'+objCN.Status__c);
            // 
            if(objCN.Status__c == 'Awaiting Approval'){
                
                // create the new approval request to submit
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                
                // Setting the Request Parameters
                req.setComments('Submitted for approval. Please approve.');
                req.setObjectId(objCN.Id);
                
                // submit the approval request for processing
                Approval.ProcessResult result = Approval.process(req);
                
                // display if the reqeust was successful
                System.debug('Submitted for approval successfully: '+result.isSuccess());
            
            }
        }
        
    }
    
    public static void updateRejectedReason(map<id,Credit_Note__c> mapOldCreditNote, map<id,Credit_Note__c> mapNewCreditNote){
        
        List<Credit_Note__c> lstNewCreditNoteToUpdate = new List<Credit_Note__c>();
        system.debug('---The Map-->'+mapNewCreditNote);
        for(Credit_Note__c objCreditNoteNew: mapNewCreditNote.values()){
            if((objCreditNoteNew.Status__c != mapOldCreditNote.get(objCreditNoteNew.id).Status__c) &&  objCreditNoteNew.Status__c.equalsIgnoreCase('Rejected')){
                lstNewCreditNoteToUpdate.add(objCreditNoteNew);
            }
        }
        
        calculateRejectedReason(lstNewCreditNoteToUpdate);
    }
    
    public static void insertRejectedReason(List<Credit_Note__c> lstNewCreditNote){
        
        List<Credit_Note__c> lstNewCreditNoteToUpdate = new List<Credit_Note__c>();
        
        for(Credit_Note__c objCreditNoteNew: lstNewCreditNote){
            if(objCreditNoteNew.Status__c.equalsIgnoreCase('Rejected')){
                lstNewCreditNoteToUpdate.add(objCreditNoteNew);
            }
        }
        calculateRejectedReason(lstNewCreditNoteToUpdate);
    }
    
    public static void calculateRejectedReason(List<Credit_Note__c> lstCreditNote){
        
        if(!lstCreditNote.isEmpty()){   
            
            set<id> setCreditNoteIds = new set<id>();
            for(Credit_Note__c objCreditNote: lstCreditNote){
                setCreditNoteIds.add(objCreditNote.id);
            }
            
            List<ProcessInstance> lstProcessInstance = new List<ProcessInstance>();
            lstProcessInstance = [Select id, 
                                    Status, 
                                    TargetObjectId,
                                    (SELECT Id, 
                                    StepStatus, 
                                    Comments 
                                    FROM Steps ORDER BY CreatedDate DESC)
                                    FROM   
                                    ProcessInstance
                                    WHERE TargetObjectId in :setCreditNoteIds 
                                    AND Status='Rejected']; 
                                                                
                                                                
            map<id, ProcessInstance> mapIdProcessInstance = new map<id, ProcessInstance>();
            for(ProcessInstance objProcessInstance : lstProcessInstance){
                mapIdProcessInstance.put(objProcessInstance.TargetObjectId, objProcessInstance);
            }
            
            for(Credit_Note__c objCreditNote: lstCreditNote){
                
                if(mapIdProcessInstance.containsKey(objCreditNote.id) && !mapIdProcessInstance.get(objCreditNote.id).Steps.isEmpty()){
                    ProcessInstanceStep objProcessInstanceStep = new ProcessInstanceStep();
                    objProcessInstanceStep =  mapIdProcessInstance.get(objCreditNote.id).Steps[0];
                    System.debug('---The Comments-->'+objProcessInstanceStep.Comments);
                    if(!String.isBlank(objProcessInstanceStep.Comments)){
                        objCreditNote.Rejected_Reason__c = objProcessInstanceStep.Comments;
                    }
                    
                    // for(ProcessInstanceStep objProcessInstanceStep: mapIdProcessInstance.get(objCreditNote.id).Steps){
                        // objCreditNote.Rejected_Reason__c += objProcessInstanceStep.Comments+ ' ';
                    // }
                }
                
            }
    
        }
        
    }
    
    
    public static void updateInvoiceStatus(List<Credit_Note__c> lstCreditNote){
        List<Invoice__c> invUpdateList = new List<Invoice__c>();    
        List<Invoice__c> invInsrtList = new List<Invoice__c>(); 
        List<Invoice__c> invList = new List<Invoice__c>();
        List<Forecasting__c> forecastList = new List<Forecasting__c>();
        List<Forecasting__c> forecastListupsert = new List<Forecasting__c>();
        List<Invoice_Allocation_Junction__c> InvoiceforecastList = new List<Invoice_Allocation_Junction__c>();
        set<id> invIds = new set<Id>();
        set<id> forecastids = new set<Id>();
        set<id> allocationIds = new set<Id>();
        Map<Id,List<Invoice_Allocation_Junction__c>> MapofInvIdToforecastjunction = new Map<Id,List<Invoice_Allocation_Junction__c>>();
        Map<Id,Forecasting__c> Mapofalloc_To_forecast = new Map<Id,Forecasting__c>();
        Map<Id,Forecasting__c> Mapofforecast = new Map<Id,Forecasting__c>();
        
        set<Id> updtSet = new set<Id>();
        set<Id> insrtSet = new set<Id>();
     
        for (Credit_Note__c objCN: lstCreditNote){
            Invoice__c inv = new Invoice__c();
            
            if(objCN.Status__c == 'Approved'){
                inv.id = objCN.Invoice__c;
                invIds.add(objCN.Invoice__c);
                inv.Status__c = 'Cancelled';
                inv.Credit_Note_Approved_Date__c = objCN.Approved_Date__c;
                invUpdateList.add(inv);
            }
            
        }
        if(invUpdateList.size()>0){
            update invUpdateList;
            checkRecursiveInvoice.run = true;
        }
        // Method to return the forecast amount of invoice allocation junction to current month forecast
        if(!invIds.isEmpty()){
            InvoiceforecastList = [select id,name,Allocation__c,Amount__c,Forecasting__c,ForeCastDate__c,Invoice__c from Invoice_Allocation_Junction__c where Invoice__c IN:invIds AND Forecasting__r.CS_Forecast_Date__c< THIS_MONTH ];
        } 
        if(!InvoiceforecastList.isEmpty()){
            for(Invoice_Allocation_Junction__c objinvjunction : InvoiceforecastList ){
                if(objinvjunction.Allocation__c != null && String.isNotBlank(objinvjunction.Allocation__c)){
                    allocationIds.add(objinvjunction.Allocation__c);
                }
                if(objinvjunction.Forecasting__c != null && String.isNotBlank(objinvjunction.Forecasting__c)){
                    forecastids.add(objinvjunction.Forecasting__c);
                }
                
                if(MapofInvIdToforecastjunction.containsKey(objinvjunction.Invoice__c)){
                    MapofInvIdToforecastjunction.get(objinvjunction.Invoice__c).add(objinvjunction);
                }else{
                    List<Invoice_Allocation_Junction__c> listforInvoice = new List<Invoice_Allocation_Junction__c>();
                    listforInvoice.add(objinvjunction);
                    MapofInvIdToforecastjunction.put(objinvjunction.Invoice__c,listforInvoice);
                }
                
            }
        
            if(!allocationIds.isEmpty()){
                forecastList = [SELECT Id,name,Amount__c,Allocation__c,CS_Forecast_Date__c from Forecasting__c WHERE (Allocation__c IN:allocationIds AND CS_Forecast_Date__c = THIS_MONTH) OR (id IN:forecastids AND CS_Forecast_Date__c != THIS_MONTH) ];
            }
            if(!forecastList.isEmpty()){
                for(Forecasting__c objforecast:forecastList){
                    if(objforecast.CS_Forecast_Date__c.month()==Date.Today().month() && objforecast.CS_Forecast_Date__c.year()==Date.Today().year()){
                        if(objforecast.Allocation__c != null && String.isNotBlank(objforecast.Allocation__c)){
                            if(Mapofalloc_To_forecast.containsKey(objforecast.Allocation__c)){
                                
                            }else{
                                Mapofalloc_To_forecast.put(objforecast.Allocation__c,objforecast);
                            }
                        }
                    }else{
                        if(objforecast.Allocation__c != null && String.isNotBlank(objforecast.Allocation__c)){
                            Mapofforecast.put(objforecast.id,objforecast);
                        }
                        
                    }
                }
            }
            for(Invoice_Allocation_Junction__c objinvjunction2 : InvoiceforecastList ){
                if(objinvjunction2.Allocation__c != null && String.isNotBlank(objinvjunction2.Allocation__c)){
                    if(Mapofalloc_To_forecast.containsKey(objinvjunction2.Allocation__c)){
                        if(Mapofalloc_To_forecast.get(objinvjunction2.Allocation__c).Amount__c != null && objinvjunction2.Amount__c != null){
                             Mapofalloc_To_forecast.get(objinvjunction2.Allocation__c).Amount__c = Mapofalloc_To_forecast.get(objinvjunction2.Allocation__c).Amount__c + objinvjunction2.Amount__c;
                        }
                    }else{
                        Forecasting__c newforecast = new Forecasting__c();
                        newforecast.Allocation__c = objinvjunction2.Allocation__c;
                        newforecast.CS_Forecast_Date__c = Date.today();
                        newforecast.Amount__c = 0.00;
                        if(objinvjunction2.Amount__c != null){
                            newforecast.Amount__c = newforecast.Amount__c + objinvjunction2.Amount__c;
                        }
                        Mapofalloc_To_forecast.put(objinvjunction2.Allocation__c,newforecast);
                    }
                }
               if(objinvjunction2.Forecasting__c != null && String.isNotBlank(objinvjunction2.Forecasting__c)){
                   if(Mapofforecast.containsKey(objinvjunction2.Forecasting__c)){
                       if(Mapofforecast.get(objinvjunction2.Forecasting__c).Amount__c != null && objinvjunction2.Amount__c != null){
                           Mapofforecast.get(objinvjunction2.Forecasting__c).Amount__c = Mapofforecast.get(objinvjunction2.Forecasting__c).Amount__c - objinvjunction2.Amount__c;
                       }
                       
                   }
               }
                
            }
        }
        if(!Mapofforecast.values().isEmpty()){
            forecastListupsert.addAll(Mapofforecast.values());
        }
        if(!Mapofalloc_To_forecast.values().isEmpty()){
            forecastListupsert.addAll(Mapofalloc_To_forecast.values());
        }
        if(!forecastListupsert.isEmpty()){
            try{
                upsert forecastListupsert;
            }catch(Exception e){
                System.debug('error----'+e);
            }
        }
    }
    
    public static void insertInvoiceClone(List<Credit_Note__c> lstCreditNote){
        List<Invoice__c> invUpdateList = new List<Invoice__c>();    
        List<Invoice__c> invInsrtList = new List<Invoice__c>(); 
        List<Invoice__c> invList = new List<Invoice__c>();    
        set<Id> updtSet = new set<Id>();
        set<Id> insrtSet = new set<Id>();
        
     
        for (Credit_Note__c objCN: lstCreditNote){
            Invoice__c inv = new Invoice__c();
            
            if(objCN.Status__c == 'Approved'){
                inv.id = objCN.Invoice__c;
                inv.Status__c = 'Cancelled';
                if(!updtSet.contains(objCN.Invoice__c)){
                    invUpdateList.add(inv);
                }
                if(objCN.Is_Invoice_Reissued__c==true){
                    
                    insrtSet.add(objCN.Invoice__c);
                }
            }
            
        }
        
        DescribeSObjectResult describeResult = Invoice__c.getSObjectType().getDescribe();
        system.debug('describeResult  ------------------->'+describeResult );
        List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );
        system.debug('fieldNames ------------------->'+fieldNames );
        String query =
          ' SELECT ' +
              String.join( fieldNames, ',' ) +
          ' FROM ' +
              describeResult.getName() +
          ' WHERE ' +
              ' id IN:insrtSet '
        ;
        
        system.debug('query ------------------->'+query );
        // return generic list of sobjects or typecast to expected type
        invList = Database.query( query );
        system.debug('invList--------------------->'+invList);
        insrtSet.clear();
        if(invList.size()>0){
            
            for(Invoice__c invs:invList){
                Invoice__c invss = new Invoice__c();
                invss  = invs.clone(false,true);
                invss.Assigned_Invoice_Number__c = null;
                invss.Date_of_Invoice__c = null;
                invss.disableinvoice__c = false;
                invss.OriginalInvoicePrinted__c = false;
                invss.Status__c= 'Draft';
                invss.Contact__c = invs.Contact__c;
                system.debug('invss ----------------->'+invss );
                if(!insrtSet.contains(invs.id)){
                    invInsrtList.add(invss);
                }
                insrtSet.add(invs.id);
            }
        }
        try{
            system.debug('invInsrtList----------------->'+invInsrtList);
            system.debug('insrtSet--->'+insrtSet);
            if(invInsrtList.size()>0){
                if(!isInsrt){
                    isInsrt=true;
                    checkRecursiveInvoice.run = true;
                    insert invInsrtList;
                }
                map<id, List<Invoice_Allocation_Junction__c>> mapInvoiceForecatingJuntion = new map<id, List<Invoice_Allocation_Junction__c>>();
                List<Invoice_Allocation_Junction__c> lstInvoiceForecating = new List<Invoice_Allocation_Junction__c>();
                 lstInvoiceForecating = [Select id,name,
                                                Amount__c,
                                                Forecasting__c,
                                                Invoice__c
                                                From
                                                Invoice_Allocation_Junction__c
                                                where Invoice__c in: insrtSet];
                                                                                    
                for(Invoice_Allocation_Junction__c objInvoiceForcasting : lstInvoiceForecating ){
                    
                    if(mapInvoiceForecatingJuntion.containsKey(objInvoiceForcasting.Invoice__c)){
                        mapInvoiceForecatingJuntion.get(objInvoiceForcasting.Invoice__c).add(objInvoiceForcasting);
                    }else{
                        mapInvoiceForecatingJuntion.put(objInvoiceForcasting.Invoice__c, new List<Invoice_Allocation_Junction__c>{objInvoiceForcasting});
                    }
                }
                set<id> setCostIds = new set<id>();
                map<id, List<Invoice_Cost_Junction__c>> mapInvoiceCostJuntion = new map<id, List<Invoice_Cost_Junction__c>>();
                List<Invoice_Cost_Junction__c> lstInvoiceCost = new List<Invoice_Cost_Junction__c>();
                lstInvoiceCost = [Select id,name,
                                        Amount__c,
                                        Disbursement__c,
                                        Invoice__c
                                        From
                                        Invoice_Cost_Junction__c
                                        where Invoice__c in: insrtSet];
                                                                                    
                for(Invoice_Cost_Junction__c objInvoiceCost : lstInvoiceCost ){
                    
                    setCostIds.add(objInvoiceCost.Disbursement__c);
                    if(mapInvoiceCostJuntion.containsKey(objInvoiceCost.Invoice__c)){
                        mapInvoiceCostJuntion.get(objInvoiceCost.Invoice__c).add(objInvoiceCost);
                    }else{
                        mapInvoiceCostJuntion.put(objInvoiceCost.Invoice__c, new List<Invoice_Cost_Junction__c>{objInvoiceCost});
                    }
                }
                
                
                map<id, List<Purchase_Order_Invoice_Junction__c>> mapInvoicePurchaseOrderJunction = new map<id, List<Purchase_Order_Invoice_Junction__c>>();
                List<Purchase_Order_Invoice_Junction__c> lstPurchaseOrderJunction = new List<Purchase_Order_Invoice_Junction__c>();
                lstPurchaseOrderJunction = [Select id,name,
                                                    Invoice__c,
                                                    Purchase_Order__c
                                                    From
                                                    Purchase_Order_Invoice_Junction__c
                                                    where Invoice__c in: insrtSet];
                                                                                    
                for(Purchase_Order_Invoice_Junction__c objInvoicePurchaseOrder : lstPurchaseOrderJunction ){
                    
                    if(mapInvoicePurchaseOrderJunction.containsKey(objInvoicePurchaseOrder.Invoice__c)){
                        mapInvoicePurchaseOrderJunction.get(objInvoicePurchaseOrder.Invoice__c).add(objInvoicePurchaseOrder);
                    }else{
                        mapInvoicePurchaseOrderJunction.put(objInvoicePurchaseOrder.Invoice__c, new List<Purchase_Order_Invoice_Junction__c>{objInvoicePurchaseOrder});
                    }
                }
                
                map<id, List<Invoice_Line_Item__c>> mapInvoiceLineItem = new map<id, List<Invoice_Line_Item__c>>();
                List<Invoice_Line_Item__c> lstInvoiceLineItem = new List<Invoice_Line_Item__c>();
                lstInvoiceLineItem = [Select id,name,
                                            Amount_ex_VAT__c,
                                            Amount_Inc_Vat__c,
                                            Cost_Line_Category__c,
                                            Cost_Type__c,
                                            Description__c,
                                            External_Id_JM_UK__c,
                                            Fee__c,
                                            International_Amount__c,
                                            Amount_ex_VAT_Formula__c,
                                            Invoice__c,
                                            IsFromAllocation__c,
                                            Sequence_Number__c,
                                            Type__c,
                                            VAT_Amount__c
                                            From
                                            Invoice_Line_Item__c
                                            where Invoice__c in: insrtSet];
                                                                                    
                for(Invoice_Line_Item__c objInvoiceLineItem : lstInvoiceLineItem ){
                    
                    if(mapInvoiceLineItem.containsKey(objInvoiceLineItem.Invoice__c)){
                        mapInvoiceLineItem.get(objInvoiceLineItem.Invoice__c).add(objInvoiceLineItem);
                    }else{
                        mapInvoiceLineItem.put(objInvoiceLineItem.Invoice__c, new List<Invoice_Line_Item__c>{objInvoiceLineItem});
                    }
                }
                
                List<Invoice_Cost_Junction__c> lstInvoiceCostNew = new List<Invoice_Cost_Junction__c>();
                List<Invoice_Allocation_Junction__c> lstInvoiceForecatingNew = new List<Invoice_Allocation_Junction__c>();
                List<Purchase_Order_Invoice_Junction__c> lstPurchaseOrderJunctionNew = new List<Purchase_Order_Invoice_Junction__c>();
                List<Invoice_Line_Item__c> lstInvoiceLineItemNew = new List<Invoice_Line_Item__c>();
                integer indexOF = 0;
                for(Invoice__c objInvoice:invList){
                    
                    if(invInsrtList[indexOF] != null){
                        // Creating Cost Junctions
                        if(mapInvoiceCostJuntion.containsKey(objInvoice.id)){
                            
                            for(Invoice_Cost_Junction__c objInvoiceCost: mapInvoiceCostJuntion.get(objInvoice.id)){
                                
                                Invoice_Cost_Junction__c objInvoiceCostNew = new Invoice_Cost_Junction__c();
                                objInvoiceCostNew = objInvoiceCost.clone();
                                objInvoiceCostNew.Invoice__c = invInsrtList[indexOF].id;
                                lstInvoiceCostNew.add(objInvoiceCostNew);
                            }
                            
                        }
                        
                        if(mapInvoiceForecatingJuntion.containsKey(objInvoice.id)){
                            // Creating Forecasting Junctions
                            for(Invoice_Allocation_Junction__c objInvoiceForecast: mapInvoiceForecatingJuntion.get(objInvoice.id)){
                                
                                Invoice_Allocation_Junction__c objInvoiceForecastNew = new Invoice_Allocation_Junction__c();
                                objInvoiceForecastNew = objInvoiceForecast.clone();
                                objInvoiceForecastNew.Invoice__c = invInsrtList[indexOF].id;
                                lstInvoiceForecatingNew.add(objInvoiceForecastNew);
                            }
                        }
                        
                        
                        if(mapInvoicePurchaseOrderJunction.containsKey(objInvoice.id)){
                            // Creating Forecasting Junctions
                            for(Purchase_Order_Invoice_Junction__c objInvoicePurchaseOrder: mapInvoicePurchaseOrderJunction.get(objInvoice.id)){
                                
                                Purchase_Order_Invoice_Junction__c objInvoicePurchaseOrderNew = new Purchase_Order_Invoice_Junction__c();
                                objInvoicePurchaseOrderNew = objInvoicePurchaseOrder.clone();
                                objInvoicePurchaseOrderNew.Invoice__c = invInsrtList[indexOF].id;
                                lstPurchaseOrderJunctionNew.add(objInvoicePurchaseOrderNew);
                            }
                        }
                        
                        
                        if(mapInvoiceLineItem.containsKey(objInvoice.id)){
                            // Creating Forecasting Junctions
                            for(Invoice_Line_Item__c objInvoiceLineItem: mapInvoiceLineItem.get(objInvoice.id)){
                                
                                Invoice_Line_Item__c objInvoiceLineItemNew = new Invoice_Line_Item__c();
                                objInvoiceLineItemNew = objInvoiceLineItem.clone();
                                objInvoiceLineItemNew.Invoice__c = invInsrtList[indexOF].id;
                                lstInvoiceLineItemNew.add(objInvoiceLineItemNew);
                            }
                        }
                        
                        
                        
                    }
                }
                checkRecursiveInvoiceCost.run = true;
                if(!lstInvoiceCostNew.isEmpty()){
                    insert lstInvoiceCostNew;
                }
                
                // Updating of Cost Status
                List<Disbursements__c> lstCosts = new List<Disbursements__c>();
                for(id idCost :setCostIds){
                    Disbursements__c objCost = new Disbursements__c();
                    objCost.id = idCost;
                    objCost.Status__c = 'Included on Draft';
                    lstCosts.add(objCost);
                }
                
                if(!lstCosts.isEmpty()){
                    update lstCosts;
                }
                checkRecursiveInvoice.run = true;
                if(!lstInvoiceForecatingNew.isEmpty()){
                    insert lstInvoiceForecatingNew;
                }
                
                if(!lstPurchaseOrderJunctionNew.isEmpty()){
                    insert lstPurchaseOrderJunctionNew;
                }
                checkRecursiveInvoice.run = true;
                if(!lstInvoiceLineItemNew.isEmpty()){
                    insert lstInvoiceLineItemNew;
                }
                
            }
        }catch(exception e){
            System.debug('------'+e.getMessage());
        }
     
    }
    

}