/**
 * @author     Nidheesh N
 * @date       Jan 4 2018
 * @decription Base selector class
 */
public abstract class BaseSelector {
  /*****************************************************************************************************************************
   *
   * Constants
   *
   *****************************************************************************************************************************/
  public final String SOQL = 'SELECT {0} FROM {1}';
  public final String SOQL_WHERE = ' WHERE {0}';
  public final String SOQL_GROUPBY = ' GROUP BY {0}';
  public final String SOQL_GROUPBY_ROLLUP = ' GROUP BY ROLLUP({0})';
  public final String SOQL_ORDERBY = ' ORDER BY {0}';
  public final String SOQL_DILIMITER_AND = ' AND ';
  public final String SOQL_DILIMITER_OR = ' OR ';

  // Maximum number of fields allowed in the GROUP BY ROLLUP field list
  public final Integer GROUPBY_ROLLUP_MAX_FIELDS = 3;
  
  /*****************************************************************************************************************************
   *
   * Constructor
   *
   *****************************************************************************************************************************/
  public BaseSelector() {}

  /*****************************************************************************************************************************
   *
   * Public Virtual Methods
   *
   *****************************************************************************************************************************/

  /*****************************************************************************************************************************
   *
   * Public Methods
   *
   *****************************************************************************************************************************/

  /*
   * SOQL Builder method that builds SOQL SELECT Command from object name, field list, AND where clause list, and order by list
   *
   * @param  objectName      Name of the object from which records are to be selected
   * @param  fieldList      A list fields to be selected
   * @param  whereClauseList    A list of 'where' clauses that will be joined together by 'AND' or 'OR'
   * @param  isAndClause      Indicate if AND or OR clause
   * @param  orderByList      A list of order by fields
   */
  public String soqlBuilder(String objectName, List<String> fieldList, List<String> whereClauseList, Boolean isAndClause, 
    List<String> orderByList) {
    return soqlBuilder(objectName, fieldList, whereClauseList, isAndClause, orderByList, -1);
  }

  /*
   * SOQL Builder method that builds SOQL SELECT Command from object name, field list, AND where clause list, and order by list
   *
   * @param  objectName      Name of the object from which records are to be selected
   * @param  fieldList      A list fields to be selected
   * @param  whereClauseList    A list of 'where' clauses that will be joined together by 'AND' or 'OR'
   * @param  isAndClause      Indicate if AND or OR clause
   * @param  orderByList      A list of order by fields
   * @param  objectLimit      Maximum number of objects to return (if less than 1, then return all selected objects)
   */
  public String soqlBuilder(String objectName, List<String> fieldList, List<String> whereClauseList, Boolean isAndClause, 
    List<String> orderByList, Integer objectLimit) {
    return soqlBuilder(objectName, fieldList, whereClauseList, isAndClause, null, false, orderByList, objectLimit);
  }

  /*
   * SOQL Builder method that builds SOQL SELECT Command from object name, field list, AND where clause list, and order by list
   *
   * @param  objectName      Name of the object from which records are to be selected
   * @param  fieldList      A list fields to be selected
   * @param  whereClauseList    A list of 'where' clauses that will be joined together by 'AND' or 'OR'
   * @param  isAndClause      Indicate if AND or OR clause
   * @param  groupByList      A list of group by clauses
   * @param  isRollup      Indicate if the Group By should perform RollUp or not
   * @param  orderByList      A list of order by fields
   * @param  objectLimit      Maximum number of objects to return (if less than 1, then return all selected objects)
   */
  public String soqlBuilder(String objectName, List<String> fieldList, List<String> whereClauseList, Boolean isAndClause, 
    List<String> groupByList, Boolean isRollup, List<String> orderByList, Integer objectLimit) {
    // Validate
    if (String.isEmpty(objectName)) { 
      throw new CustomException('BaseSelector.soqlBuilder: An object name must be specified');
    }
    if (fieldList == null || fieldList.isEmpty()) {
      throw new CustomException('BaseSelector.soqlBuilder: Field list must not be empty');
    }
    if (groupByList != null && isRollup && groupByList.size() > GROUPBY_ROLLUP_MAX_FIELDS) {
      throw new CustomException('BaseSelector.soqlBuilder: GROUP BY ROLLUP field list cannot be more than ' + 
        GROUPBY_ROLLUP_MAX_FIELDS + '.');
    }

    // Build SOQL command
    String soqlCmd = String.format(SOQL, new List<String>{String.join(fieldList, CoreConstants.DELIMITER_COMMA), objectName});
    if (whereClauseList != null && !whereClauseList.isEmpty()) soqlCmd += String.format(SOQL_WHERE, new List<String>{
      String.join(whereClauseList, isAndClause ? SOQL_DILIMITER_AND : SOQL_DILIMITER_OR)});
    if (groupByList != null && !groupByList.isEmpty()) {
      if (isRollup) soqlCmd += String.format(SOQL_GROUPBY_ROLLUP, new List<String>{String.join(groupByList, CoreConstants.DELIMITER_COMMA)});
      else soqlCmd += String.format(SOQL_GROUPBY, new List<String>{String.join(groupByList, CoreConstants.DELIMITER_COMMA)});
    }
    if (orderByList != null && !orderByList.isEmpty()) soqlCmd += String.format(SOQL_ORDERBY, new List<String>{
      String.join(orderByList, CoreConstants.DELIMITER_COMMA)});
    if (objectLimit > 0) soqlCmd += ' LIMIT ' + String.valueOf(objectLimit).replace(CoreConstants.DELIMITER_COMMA, CoreConstants.BLANK_STRING);

    return soqlCmd;
  }
}