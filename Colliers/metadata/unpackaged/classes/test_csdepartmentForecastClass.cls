/**
 *  Class Name: test_csdepartmentForecastClass 
 *  Description: Test class for the controller of journalPage
 *  Company: dQuotient
 *  CreatedDate: 13/10/2016 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  ----------------------------------------------------------- 
 *  Nidheesh               17/10/2016                  Orginal Version          
 *
 */
@isTest
private class test_csdepartmentForecastClass {

 @testsetup
    static void setupData(){
        //create account
        Account a = TestObjectHelper.createAccount();
        insert a;
        
        Schema.DescribeFieldResult fieldResult = Coding_Structure__c.Department__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        Schema.DescribeFieldResult fieldResulthead = Staff__c.headOfDepartment__c.getDescribe();
        List<Schema.PicklistEntry> plehead = fieldResult.getPicklistValues();
        Schema.DescribeFieldResult fieldResultofffice = Coding_Structure__c.Office__c.getDescribe();
        List<Schema.PicklistEntry> pleoffice = fieldResultofffice.getPicklistValues();
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
        System.debug('opps---->'+opps);
        //create realted Opportunity
        List <Contact> cons = TestObjectHelper.createMultipleContact(a, 10);
        integer loopNo = 1;
        for(Contact objContact: cons){
            objContact.Email = 'test'+loopNo+'@test.com';
        }
        insert cons;
        System.debug('cons---->'+cons);
        Staff__c staffObj =  new Staff__c();
        staffObj.Email__c = 'stvcxa14576423ff@gmail.com';
        staffObj.HR_No__c = String.valueOf(Math.round(Math.random()*1000));
        staffObj.Name = 'staffdummy';
        
        if(!ple.isEmpty()){
            staffObj.headOfDepartment__c = 'Accurates';
        }
        
        staffObj.Email__c = 'staff@gmail.com';
        staffobj.active__c = true;
        insert staffObj;
        
        
        //Create Disbursements__c data for opportunity
        opportunity x = [Select id from opportunity Limit 1];
        Contact b = [Select id from Contact Limit 1];
        Allocation__c allocObj = new Allocation__c();
        allocObj.Job__c = x.Id;
        if(!ple.isEmpty()){
            allocObj.Department_Allocation__c = 'Accurates';
        }
        system.debug('LoggingLevel logLevel'+ple[0].getValue());
        if(!pleoffice.isEmpty()){
            allocObj.Office__c =  pleoffice[0].getValue();
        }
        allocObj.Allocation_Amount__c = 10000;
        allocobj.Assigned_To__c = staffObj.Id;
        insert allocObj;
        Allocation__c allocObject = new Allocation__c();
        allocObject.Job__c = x.Id;
        system.debug('LoggingLevel logLevel'+ple.size());
        if(!ple.isEmpty() && ple.size()>4){
            allocObject.Department_Allocation__c = 'Accurates';
            system.debug('LoggingLevel logLevel'+ple[0].getValue());
        }else if(!ple.isEmpty()){
            allocObject.Department_Allocation__c = 'Accurates';
        }
        if(!pleoffice.isEmpty()&& pleoffice.size()>4){
            allocObject.Office__c =  pleoffice[3].getValue();
        }
        allocObject.Assigned_To__c = staffObj.Id;
        allocObject.Allocation_Amount__c = 1000;
        insert allocObject;
        Forecasting__c forcastobj = new Forecasting__c();
        forcastobj.Allocation__c = allocObject.Id;
        forcastobj.Amount__c = 100;
        forcastobj.CS_Forecast_Date__c = System.today();
        insert forcastobj;
        Forecasting__c forcastobject = new Forecasting__c();
        forcastobject.Allocation__c = allocObj.Id;
        forcastobject.Amount__c = 100;
        forcastobject.CS_Forecast_Date__c = System.today().addMonths(1);
        insert forcastobject;
        Forecasting__c forcastobjectFuture = new Forecasting__c();
        forcastobjectFuture.Allocation__c = allocObj.Id;
        forcastobjectFuture.Amount__c = 100;
        forcastobjectFuture.CS_Forecast_Date__c = System.today().addMonths(2);
        insert forcastobjectFuture;
        
    }
    static testMethod void testCS_allocationControllerForOpps(){
        System.debug('opp test---->');
        Schema.DescribeFieldResult fieldResult = Coding_Structure__c.Department__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        Schema.DescribeFieldResult fieldResultofffice = Coding_Structure__c.Office__c.getDescribe();
        List<Schema.PicklistEntry> pleoffice = fieldResultofffice.getPicklistValues();
        opportunity a = [Select id from opportunity Limit 1];
        Staff__c staffobject = [Select id from Staff__c Limit 1];
        Department_Forecast__c  newobj = new Department_Forecast__c ();
        newobj.Month_1_Name__c = system.today();
        if(!ple.isEmpty()){
            newobj.Department__c = 'Accurates';
        }
        if(!pleoffice.isEmpty()){
            newobj.officename__c =  pleoffice[0].getValue();
        }
        newobj.Adjusted_Month_1_Amount__c = 10;
        newobj.Adjusted_Month_2_Amount__c = 10;
        newobj.Adjusted_Month_3_Amount__c =10;
        insert newobj;
        Department__c deptcustom = new Department__c();
        deptcustom.Name = 'test';
        deptcustom.Company__c = '2600';
        if(!ple.isEmpty()){
            deptcustom.Department__c = 'Accurates';    
        }
        deptcustom.Cost_Centre__c ='620';
        insert deptcustom;
        User objuser = TestObjectHelper.createAdminUser(true);
        objuser.username = 'test@gmat.com';
        insert objuser;
        staffobject.user__c = objuser.Id;
        staffobject.active__C = true;
        upsert staffobject;
        system.runAs(objuser){
        Test.startTest();
            PageReference pf = new PageReference('/apex/cs_departForecastPage');
            Test.setCurrentPage(pf);
            cs_departmentForecastClass con = new cs_departmentForecastClass();
            con.initialSave();
            con.savemethod();
            con.Exportmethod();
            PageReference pf1 = new PageReference('/apex/cs_departForecastPage');
            Test.setCurrentPage(pf1);
            con.savemethod();
            Test.stopTest();
        }
    }
    
    static testMethod void testCS_allocationControllerForOpportunity(){
        System.debug('opp test---->');
        opportunity a = [Select id from opportunity Limit 1];
        Staff__c staffobject = [Select id from Staff__c Limit 1];
        
        User objuser = TestObjectHelper.createAdminUser(true);
        objuser.username = 'test@gmat.com';
        insert objuser;
        staffobject.user__c = objuser.Id;
        upsert staffobject;
        system.runAs(objuser){
        Test.startTest();
            PageReference pf = new PageReference('/apex/cs_departForecastPage');
            Test.setCurrentPage(pf);
            cs_departmentForecastClass con = new cs_departmentForecastClass();
            con.initialSave();
            con.savemethod();
            con.Exportmethod();
            PageReference pf1 = new PageReference('/apex/cs_departForecastPage');
            Test.setCurrentPage(pf1);
            con.savemethod();
            Test.stopTest();
        }
    }
}