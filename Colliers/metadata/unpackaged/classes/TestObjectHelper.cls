@isTest(SeeAllData=false)
public class TestObjectHelper {
    private static integer index = 25;
    /**
    * @description Creates the user object with the system administrator profile.
    * @param Boolean A flag to indicate whether to set the currently logged in user's email as the email for the newly created user.
    * @return User The newly created user object.
    */
    public static User createAdminUser(Boolean useCurrentUsersEmail) {
        User user = (User)SmartFactory.createSObject('User', false);
        user.Username = 'te2345st@test.com';
        if(useCurrentUsersEmail == true)
          user.Email = [SELECT Email FROM User WHERE Id = :UserInfo.getUserId()].Email;
        user.ProfileId = [SELECT Id FROM Profile WHERE name = 'System Administrator' LIMIT 1].Id;
        user.Email = 'test'+String.valueOf(Math.round(Math.random()*1000))+'@test.com';
        user.External_Id_Email__c  = String.valueOf(Math.round(Math.random()*1000));
        return user;
    }
    
    /**
    * @description creates the user object with the given profile.
    * @param Boolean A flag to indicate whether to set the currently logged in user's email as the email for the newly created user.
    * @return User The newly created user object.
    */
    public static User createUserAs(String profileName, Boolean useCurrentUsersEmail) {
        //double x= math.random();
        index= index+6000;
        system.debug('Called from account trigger test method');
        User user = (User)SmartFactory.createSObject('User', false);
        user.Username = 'VVV' + index++ + '+com@phds.com';
        if(useCurrentUsersEmail == true)
          user.Email = [SELECT Email FROM User WHERE Id = :UserInfo.getUserId()].Email;
        user.ProfileId = [SELECT Id FROM Profile WHERE name = :profileName LIMIT 1].Id;
        user.Email = 'test'+String.valueOf(Math.round(Math.random()*1000))+'@test.com';
        user.External_Id_Email__c  = String.valueOf(Math.round(Math.random()*1000));
        return user;
    }
    
    /* Object creation methods for the 'Account' object. */
     
    /**
    * @description creates account object
    * @return Account The newly created account object.
    */
    public static Account createAccount() {
        Account account = (Account)SmartFactory.createSObject('Account', false);
        account.Town__c = 'acc town';
        account.Street_No__c ='#12';
        account.Street__c ='Street';
        account.Post_Code__c = '2345';
        account.Country__c ='UK';
        return account;
    }
    
    /**
    * @description creates Care OF object
    * @return Care Of object.
    */
    public static Care_Of__c createCareOf(Account a,Contact c) {
        Care_Of__c careOf= (Care_Of__c)SmartFactory.createSObject('Care_Of__c', false);
        careOf.Client__c = a.id;
        careOf.Contact__c = c.id;
        return careOf;
    }
    
    public static Property__c createProperty() {
        Property__c property = (Property__c)SmartFactory.createSObject('Property__c', false);
        property.Address_Validation_Status__c = 'Verified';
        return property;
    }
     public static Company_Property_Affiliation__c CmpnyPrptyJn(Account Acc ) {
        Company_Property_Affiliation__c propertyJob = (Company_Property_Affiliation__c)SmartFactory.createSObject('Company_Property_Affiliation__c', false);
        propertyJob.Company__c=Acc.id;
        return propertyJob;
    }
    
    public static Job_Property_Junction__c JobPrptyJn(Opportunity Opp, property__c propertys ) {
        Job_Property_Junction__c propertyJob = (Job_Property_Junction__c)SmartFactory.createSObject('Job_Property_Junction__c', false);
        propertyJob.Job__c=Opp.id;
        propertyJob.Property__c=propertys.id;
        return propertyJob;
    }
    
    
    public static Event  createEvent(contact c) {
        Event evnts = (Event)SmartFactory.createSObject('Event', false);
       // propertyJob.Job__c=Opp.id;
         evnts.IsAllDayEvent = true;
         evnts.ActivityDate = Date.today(); 
         evnts.whoid=c.id;
        // evnts.whatId =c.id;
        //propertyJob.Property__c=propertys.id;
        return evnts;
    }
    
    /**
    * @description creates multiple account object
    * @return List<Account> The newly created account object.
    */
    public static List<Account> createMultipleAccounts(Integer no) {
        List<Account> accounts = new List<Account>();
        for(Integer i = 0; i<no ; i++){
            Account account = (Account)SmartFactory.createSObject('Account', false);
            accounts.add(account);
        }
        return accounts;
    }

    /* Object creation methods for the 'Contact' object. */
 
    /**
    * @description creates the contact object and associates to an account.
    * @return Contact The newly created contact object.
    */
    public static Contact createContact(Account accRec) {
        Contact conRec = (Contact)SmartFactory.createSObject('Contact', false);
        conRec.AccountId = accRec.Id;
        return conRec;
    }
    
    /**
    * @description creates multiple contact object and associates to an account.
    * @return Contact The newly created contact object.
    */
    public static List<Contact> createMultipleContact(Account accRec,Integer no) {
        List<Contact> cons = new List<Contact>();
        for(Integer i = 0; i<no; i++){ 
            Contact conRec = (Contact)SmartFactory.createSObject('Contact', false);
            conRec.AccountId = accRec.Id;
            cons.add(conRec);
        }
        return cons;
    }
    
    /**
    * @description creates the opportunity object and associates to an account.
    * @return Opportunity The newly created contact object.
    */
    public static Opportunity createOpportunity(Account accRec){
        // the first parameter is the object name.
        SmartFactory.IncludedFields.put('Opportunity', new Set<String> { 'Sales_Region__c' });
        Opportunity opty = (Opportunity)SmartFactory.createSObject('Opportunity',false);
        opty.AccountId = accRec.Id;
        opty.Engagement_Letter__c = true;
        opty.Engagement_Letter_Sent_Date__c=date.today();
        return opty;
    }
    
    
    /**
    * @description creates multiple the opportunity object and associates to an account.
    * @return Opportunity The newly created contact object.
    */
    public static List<Opportunity> createMultipleOpportunity(Account accRec,Integer no){
        // the first parameter is the object name.
        List<Opportunity> opps = new List<OpporTunity>();
        for(Integer i = 0; i<no; i++){
            Opportunity opty = (Opportunity)SmartFactory.createSObject('Opportunity',false);
            opty.AccountId = accRec.Id;
            opty.Engagement_Letter__c = true;
            opty.Engagement_Letter_Sent_Date__c=date.today();
            opps.add(opty);
        }
        return opps;
    }
    
    public static List<Opportunity> createMultipleOpportunitys(Account accRec,staff__c stf,Integer no){
        // the first parameter is the object name.
        List<Opportunity> opps = new List<OpporTunity>();
        for(Integer i = 0; i<no; i++){
            Opportunity opty = (Opportunity)SmartFactory.createSObject('Opportunity',false);
            opty.AccountId = accRec.Id;
            opty.Engagement_Letter__c = true;
            opty.Engagement_Letter_Sent_Date__c=date.today();
            opty.Manager__c=stf.id;
            opps.add(opty);
        }
        return opps;
    }
    
    //Methods Added by : Jyoti/18-10-2016
    
    /**
    * @description creates the Staff object 
    * @return Staff object.
    */
    public static Staff__c createStaff(){
        // the first parameter is the object name.
        Staff__c stf= new Staff__c();//(Staff__c)SmartFactory.createSObject('Staff__c',false);
        stf.name='Jaredresrstt8SDhHHZSHxzM';
        stf.Email__c='resrstt8SDhHHZSHxzM.ZMSDLM@gmail.com';
        stf.active__c = true;
        stf.HR_No__c = String.valueOf(Math.round(Math.random()*1000));
        return stf;
    }
    
    Public static EventRelation CreateEventRel(event Evnt,Contact con){
        EventRelation er =  (EventRelation)SmartFactory.createSObject('EventRelation',false);
        er.EventId = Evnt.id;
        er.RelationId = con.id;
        return er;

    }
    
    /**
    * @description creates the Allocation object 
    * @return Allocation object.
    */
    public static Allocation__c createAllocation(Opportunity opp,Staff__c stf){
        // the first parameter is the object name.
        Allocation__c alloc= (Allocation__c)SmartFactory.createSObject('Allocation__c',false);
        alloc.job__c= opp.id;
        alloc.Assigned_To__c=stf.id;
        return alloc;
    }
    
    /**
    * @description creates multiple the Invoice Line Items object.
    * @return Invoice Line Item List
    */
    public static List<Allocation__c> createMultipleAllocation(Opportunity opp,Staff__c stf,Integer no){
        // the first parameter is the object name.
        List<Allocation__c> allocs= new List<Allocation__c>();
        for(Integer i = 0; i<no; i++){
            Allocation__c alloc = (Allocation__c)SmartFactory.createSObject('Allocation__c',false);
            alloc.job__c= opp.id;
            alloc.Assigned_To__c=stf.id;
            allocs.add(alloc);
        }
        return allocs;
    }
    
    /**
    * @description creates the Purchase Order object 
    * @return Purchase Order object.
    */
    public static Purchase_Order__c createPurchaseOrder(Opportunity opp){
        // the first parameter is the object name.
        Purchase_Order__c po= (Purchase_Order__c)SmartFactory.createSObject('Purchase_Order__c',false);
        po.job__c= opp.id;
        return po;
    }
    
    /**
    * @description creates multiple Invoice Allocation Junction object.
    * @return Invoice Allocation Junction List
    */
    public static List<Purchase_Order__c> createMultiPurchaseOrder(Opportunity opp,Integer no){
        // the first parameter is the object name.
        List<Purchase_Order__c> pos= new List<Purchase_Order__c>();
        for(Integer i = 0; i<no; i++){
            Purchase_Order__c po= (Purchase_Order__c)SmartFactory.createSObject('Purchase_Order__c',false);
            po.job__c= opp.id;
            pos.add(po);
        }
        return pos;
    }
    
    /**
    * @description creates the Invoice object 
    * @return Invoice object.
    */
    public static Invoice__c createInvoice(Opportunity opp,Contact c){
        // the first parameter is the object name.
        Invoice__c inv= (Invoice__c)SmartFactory.createSObject('Invoice__c',false);
        inv.Opportunity__c= opp.id;
        inv.contact__c=c.id;
        inv.status__c='Approved';
        inv.is_International__c=true;
        return inv;
    }
    
    /**
    * @description creates multiple the Invoice object.
    * @return Invoice List
    */
    public static List<Invoice__c> createMultipleInvoice(Opportunity opp,Contact c,Integer no){
        // the first parameter is the object name.
        List<Invoice__c> invs = new List<Invoice__c>();
        for(Integer i = 0; i<no; i++){
            Invoice__c inv = (Invoice__c)SmartFactory.createSObject('Invoice__c',false);
            inv.Opportunity__c= opp.id;
            inv.contact__c=c.id;
            inv.status__c='Approved';
            invs.add(inv);
        }
        return invs;
    }
  
    /**
    * @description creates the Invoice Allocation Junction object 
    * @return Invoice Allocation Junction object.
    */
    public static Invoice_Allocation_Junction__c createInvoiceAllocationJunction(Allocation__c all,Invoice__c inv){
        // the first parameter is the object name.
        Invoice_Allocation_Junction__c invAlloc= (Invoice_Allocation_Junction__c)SmartFactory.createSObject('Invoice_Allocation_Junction__c',false);
        invAlloc.Allocation__c= all.id;
        invAlloc.Invoice__c=inv.id;
        return invAlloc;
    }
    
    /**
    * @description creates multiple Invoice Allocation Junction object.
    * @return Invoice Allocation Junction List
    */
    public static List<Invoice_Allocation_Junction__c> createMultipleInvoiceAllocationJunction(Allocation__c all,Invoice__c inv,Integer no){
        // the first parameter is the object name.
        List<Invoice_Allocation_Junction__c> invs = new List<Invoice_Allocation_Junction__c>();
        for(Integer i = 0; i<no; i++){
            Invoice_Allocation_Junction__c invAlloc = (Invoice_Allocation_Junction__c)SmartFactory.createSObject('Invoice_Allocation_Junction__c',false);
            invAlloc.Allocation__c= all.id;
            invAlloc.Invoice__c=inv.id;
            invs.add(invAlloc);
        }
        return invs;
    }
  
    
    /**
    * @description creates the Credit Note object 
    * @return Credit Note object.
    */
    public static Credit_Note__c createCreditNote(Invoice__c inv){
        // the first parameter is the object name.
        Credit_Note__c cd= (Credit_Note__c)SmartFactory.createSObject('Credit_Note__c',false);
        cd.Invoice__c=inv.id;
        cd.Is_Invoice_Reissued__c =true;
        cd.New_Invoice_Amount__c = 1200;
        cd.Credit_Note_Date__c=date.today();
        cd.status__c='Awaiting Approval';
        cd.CN_Reason__c = 'Incorrect Amount';
        return cd;
    }
    
    /**
    * @description creates the Forecast object 
    * @return Forecast object.
    */
    public static Invoice_PDF_Value__c createInvoicePDFValue(staff__c stf){
        // the first parameter is the object name.
        Invoice_PDF_Value__c invpdf= (Invoice_PDF_Value__c)SmartFactory.createSObject('Invoice_PDF_Value__c',false);
        invpdf.HrNo__c= stf.id;
        invpdf.Remittance__c='test  tetstr';
       
        return invpdf;
    }
    
    
    /**
    * @description creates the Forecasting object 
    * @return Forecasting object.
    */
    public static Forecasting__c createForecast(Allocation__c alloc){
        // the first parameter is the object name.
        Forecasting__c frcst= (Forecasting__c)SmartFactory.createSObject('Forecasting__c',false);
        frcst.Allocation__c =alloc.id;
        frcst.Amount__c =15000;
        return frcst;
    }
    
    /**
    * @description creates multiple Forecasting object.
    * @return Invoice Allocation Junction List
    */
    public static List<Forecasting__c> createMultipleForecasting(Allocation__c all,Integer no){
        // the first parameter is the object name.
        List<Forecasting__c> frcsts = new List<Forecasting__c>();
        for(Integer i = 0; i<no; i++){
            Forecasting__c frcst = (Forecasting__c)SmartFactory.createSObject('Forecasting__c',false);
            frcst.Allocation__c= all.id;
            frcst.Amount__c=15000;
            frcsts.add(frcst);
        }
        return frcsts;
    }
    
    /**
    * @description creates the Accural object 
    * @return Accural object.
    */
    public static Accrual__c createAccural(Forecasting__c frcst){
        // the first parameter is the object name.
        Accrual__c acc= (Accrual__c)SmartFactory.createSObject('Accrual__c',false);
        acc.Status__c = 'Awaiting Approval';
        return acc;
    }
    
    /**
    * @description creates the Accrual Forecast Junction object 
    * @return Accrual Forecast Junction object.
    */
    public static Accrual_Forecasting_Junction__c createAccrualForecastJunction(Accrual__c acc,Forecasting__c frcst){
        // the first parameter is the object name.
        Accrual_Forecasting_Junction__c alloc= (Accrual_Forecasting_Junction__c)SmartFactory.createSObject('Accrual_Forecasting_Junction__c',false);
        alloc.Accrual__c = acc.id;
        alloc.Forecasting__c =frcst.id;
        return alloc;
    }
    
    
    /**
    * @description creates multiple Accrual Forecast Junction object.
    * @return IAccrual Forecast Junction List
    */
    public static List<Accrual_Forecasting_Junction__c> createMultipleAccFrcstJunction(Accrual__c acc,Forecasting__c frcst,Integer no){
        // the first parameter is the object name.
        List<Accrual_Forecasting_Junction__c> frcsts = new List<Accrual_Forecasting_Junction__c>();
        for(Integer i = 0; i<no; i++){
            Accrual_Forecasting_Junction__c frcstrec= (Accrual_Forecasting_Junction__c)SmartFactory.createSObject('Accrual_Forecasting_Junction__c',false);
            frcstrec.Accrual__c = acc.id;
            frcstrec.Forecasting__c =frcst.id;
            frcsts.add(frcstrec);
        }
        return frcsts;
    }
    
    /**
    * @description creates the Disbursement object 
    * @return Disbursement object.
    */
    public static Disbursements__c createDisbursement(Opportunity opp){
        // the first parameter is the object name.
        Disbursements__c disburs= (Disbursements__c)SmartFactory.createSObject('Disbursements__c',false);
        disburs.Job__c = opp.id;
        disburs.category__C = 'Motor';
        disburs.Sub_Category__c='Damage';
        
        return disburs;
    }
    
    /**
    * @description creates multiple the Disbursement object.
    * @return Disbursement List
    */
    public static List<Disbursements__c> createMultipleDisbursement(Opportunity opp,Integer no){
        // the first parameter is the object name.
        List<Disbursements__c> LstDsb= new List<Disbursements__c>();
        for(Integer i = 0; i<no; i++){
            Disbursements__c disburs= (Disbursements__c)SmartFactory.createSObject('Disbursements__c',false);
            disburs.Job__c = opp.id;
            LstDsb.add(disburs);
        }
        return LstDsb;
    }
    
    /**
    * @description creates the Invoice Cost Junction object 
    * @return  Invoice Cost Junction object.
    */
    public static Invoice_Cost_Junction__c createInvoiceCostJunction(Disbursements__c d,Invoice__c inv){
        // the first parameter is the object name.
        Invoice_Cost_Junction__c invCst= (Invoice_Cost_Junction__c)SmartFactory.createSObject('Invoice_Cost_Junction__c',false);
        invCst.Disbursement__c = d.id;
        invCst.Invoice__c = inv.id;
        return invCst;
    }
    
    /**
    * @description creates the Invoice Line Item object 
    * @return Invoice Line Item object.
    */
    public static Invoice_Line_Item__c createInvoiceLineItem(Invoice__c inv){
        // the first parameter is the object name.
        Invoice_Line_Item__c invLineItem= (Invoice_Line_Item__c)SmartFactory.createSObject('Invoice_Line_Item__c',false);
        invLineItem.Invoice__c = inv.id;
        invLineItem.type__c = 'Fee';
        return invLineItem;
    }
    
    /**
    * @description creates multiple the Invoice Line Items object.
    * @return Invoice Line Item List
    */
    public static List<Invoice_Line_Item__c> createMultipleInvoiceLineItems(Invoice__c inv,Integer no){
        // the first parameter is the object name.
        List<Invoice_Line_Item__c> invs = new List<Invoice_Line_Item__c>();
        for(Integer i = 0; i<no; i++){
            Invoice_Line_Item__c invLineItem= (Invoice_Line_Item__c)SmartFactory.createSObject('Invoice_Line_Item__c',false);
            invLineItem.Invoice__c = inv.id;
            invs.add(invLineItem);
        }
        return invs;
    }
    
    /**
    * @description creates the Assign Id Custom Setting Record
    * @return Assign Id Custom Setting object.
    */
    public static AssignId__c createAssignId(){
        // the first parameter is the object name.
        AssignId__c assignId= (AssignId__c)SmartFactory.createSObject('AssignId__c',false);
        assignId.Invoice_Pattern__c = '0000000000001';
        return assignId;
    }
    
    /**
    * @description creates the Purchase Order Invoice Junction Record
    * @return Purchase Order Invoice Junction object.
    */
    public static Purchase_Order_Invoice_Junction__c createPurchaseOrderInvoiceJunction(Purchase_Order__c po,Invoice__c inv){
        // the first parameter is the object name.
        Purchase_Order_Invoice_Junction__c poInvJun= (Purchase_Order_Invoice_Junction__c)SmartFactory.createSObject('Purchase_Order_Invoice_Junction__c',false);
        poInvJun.Invoice__c = inv.id;
        poInvJun.Purchase_Order__c = po.id;
        return poInvJun;
    }
    
     /**
    * @description creates multiple the Purchase Order Invoice Junction object.
    * @return Purchase Order Invoice Junction List
    */
    public static List<Purchase_Order_Invoice_Junction__c> createMultipleInvoiceLineItems(Purchase_Order__c po,Invoice__c inv,Integer no){
        // the first parameter is the object name.
        List<Purchase_Order_Invoice_Junction__c> pos = new List<Purchase_Order_Invoice_Junction__c>();
        for(Integer i = 0; i<no; i++){
            Purchase_Order_Invoice_Junction__c poItem= (Purchase_Order_Invoice_Junction__c)SmartFactory.createSObject('Purchase_Order_Invoice_Junction__c',false);
            poItem.Invoice__c = inv.id;
            poItem.Purchase_Order__c = po.id;
            pos.add(poItem);
        }
        return pos;
    }
    
    /**
    * @description Creates the Address Object
    * @return Address Object
    */
    public static Address__c createAddress(Account a) {
        Address__c addrs= (Address__c)SmartFactory.createSObject('Address__c', false);
        //addrs.Company__c =a.id;
        //addrs.Country__c = 'UK';
        addrs.Estate__c = 'estate';
        addrs.Flat_Number__c = '#12';
        addrs.Postcode__c = '2345';
        return addrs;
    }
    
    /**
    * @description Creates the Contact Address Junction Object
    * @return Contact Address Junction Object
    */
    public static Contact_Address_Junction__c createContactAddressJunction(Contact c,Address__c a) {
        Contact_Address_Junction__c conAddrs= (Contact_Address_Junction__c)SmartFactory.createSObject('Contact_Address_Junction__c', false);
        conAddrs.Address__c = a.id;
        conAddrs.Contact__c = c.id;
        return conAddrs;
    }
    
    /**
    * @description Creates the Account Address Junction Object
    * @return Contact Address Junction Object
    */
    public static Account_Address_Junction__c createAccountAddressJunction(Account ac,Address__c a) {
        Account_Address_Junction__c accAddrs= (Account_Address_Junction__c)SmartFactory.createSObject('Account_Address_Junction__c', false);
        accAddrs.Address__c = a.id;
        accAddrs.Account__c = ac.id;
        return accAddrs;
    }
    
    /**
    * @description creates the Assign Account Code Custom Setting Record
    * @return Assign Id Custom Setting object.
    */
    public static AssignAccountId__c createAssignAccCode(){
        // the first parameter is the object name.
        AssignAccountId__c assignId= (AssignAccountId__c)SmartFactory.createSObject('AssignAccountId__c',false);
        assignId.Account_Pattern__c = '000001';
        assignId.AccountNumber__c = 1;
        return assignId;
    }
    
    /**
    * @description creates the Conflict Record
    * @return Conflict object.
    */
    public static Conflict__c createConflict(Opportunity opp,Opportunity oppr){
        // the first parameter is the object name.
        Conflict__c conflict= (Conflict__c)SmartFactory.createSObject('Conflict__c',false);
        conflict.Job_1__c=opp.id;
        conflict.Job_2__c= oppr.id;
        conflict.Conflict__c='Yes';
        return conflict;
    }
    
    /**
    * @description creates the Conflict Reminder Days Record
    * @return Conflict Reminder Day Custom Setting object.
    */
    public static Conflict_Reminder_days__c createConflictReminder(){
        // the first parameter is the object name.
        Conflict_Reminder_days__c conRemDays= (Conflict_Reminder_days__c)SmartFactory.createSObject('Conflict_Reminder_days__c',false);
        conRemDays.Name = 'reminder days';
        conRemDays.No_of_Days__c =5;
        return conRemDays;
    }
    
    /**
    * @description creates the Job Amount
    * @return Job Amount Custom Setting object.
    */
    public static Job_Amount__c createJobAmount(){
        // the first parameter is the object name.
        Job_Amount__c jobAmt= (Job_Amount__c)SmartFactory.createSObject('Job_Amount__c',false);
        jobAmt.Name = 'jbAmount';
        jobAmt.Amount__c=8000.00;
        return jobAmt;
    }
    
    
    
}