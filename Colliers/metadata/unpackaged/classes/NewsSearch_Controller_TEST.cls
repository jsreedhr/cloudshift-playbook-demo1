@isTest
public class NewsSearch_Controller_TEST{
    public static testMethod void testNews(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.News_Feed__c = 'Test Feed';
        insert acc;
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            ApexPages.currentPage().getParameters().put('id',String.valueOf(acc.Id));
            ApexPages.StandardController StdCtl = new ApexPages.StandardController(acc);
            NewsSearch_Controller objNews = new NewsSearch_Controller(StdCtl);
            objNews.searchNews();
            objNews.NextNews();
            objNews.PrevNews();
        Test.stopTest();
    }
}