@isTest 
public class TestEditOverrideController {
  //  @testsetup
    static void setupData(){
        //Create Custom setting data for opportunity
        ObjectFieldSetMapping__c ofs = new ObjectFieldSetMapping__c();
        ofs.Name = 'AccParent_OppChildFR';
        ofs.Object_API_Name__c = 'Opportunity';
        ofs.Field_Set_Name__c = 'OppFieldSetFR';
        ofs.ParentRecordType__c='';
        ofs.ParentObjectAPIName__c='Account';
        ofs.RecordType__c='';
        insert ofs;
         
        ObjectFieldSetMapping__c ofs2 = new ObjectFieldSetMapping__c();
        ofs2.Name = 'Opportunity';
        ofs2.Object_API_Name__c = 'Opportunity';
         ofs2.Field_Set_Name__c = 'OppFieldSetUK';
        ofs2.ParentObjectAPIName__c='';
        ofs2.RecordType__c='UK Rating';
        insert ofs2;
        ObjectRelatedListMapping__c ors=new ObjectRelatedListMapping__c();
        ors.Name='OpportunityUK';
        ors.ObjectAPIName__c='Opportunity';
        ors.RecordType__c='UK Rating';
        ors.ChildObjectsString__c='Opportunity_Service_Line__c;opportunitycontactrole';
        insert ors;
        
        //create account
        Account a = TestObjectHelper.createAccount();
        insert a;
        
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 5);
        insert opps;
        System.debug('opps---->'+opps);
        //create realted Opportunity
        List <Contact> cons = TestObjectHelper.createMultipleContact(a, 5);
        integer loopNo = 1;
        for(Contact objContact: cons){
            objContact.Email = 'test'+loopNo+'@test.com';
        }
        insert cons;
        System.debug('cons---->'+cons);
    }
    
    static testMethod void testEditOverrideListPafeForOpps(){
        System.debug('opp test---->');
            setupData();  
            Account a = [Select id from Account Limit 1];
            Test.startTest();
            PageReference pf = new PageReference('/apex/EditOverride?id='+a.Id);
            Test.setCurrentPage(pf);
            try{  
          /*  EditOverrideController con = new EditOverrideController();
            con.selectedRelation='Opportunities';
            con.addRow();
            ApexPages.currentPage().getParameters().put('sobjectName','Opportunities' );
            ApexPages.currentPage().getParameters().put('recordIndex','1' );
            con.deleteMethod();
            con.cancel();*/
            }catch(exception e){
              }
        
            Test.stopTest();
       
    }
    static testMethod void testRelatedListPafeForContacts(){
        System.debug('con test---->');
        setupData();  
        Opportunity a = [Select id from Opportunity Limit 1];
        Test.startTest();
        PageReference pf = new PageReference('/apex/EditOverride?id='+a.Id);
        Test.setCurrentPage(pf);
        EditOverrideController con = new EditOverrideController();
        con.selectedRelation='OpportunityContactRoles';
        ApexPages.currentPage().getParameters().put('wrapperId','1' );
        con.selectPrimaryContact();
        
         con.addRow();
         con.cancel();
         
        con.save();
        Test.stopTest();
        
    }
    static testMethod void testEditOverrideListPafeForOpps1(){
        setupData();  
        System.debug('opp test---->');
        Account a = [Select id from Account Limit 1];
        Test.startTest();
        PageReference pf = new PageReference('/apex/EditOverride?id='+a.Id);
        Test.setCurrentPage(pf);
        // EditOverrideController con = new EditOverrideController();
        // con.selectedRelation='Opportunity';
        // con.addRow();
        // con.cancel();
        Test.stopTest();
          
        
        
    }
     static testMethod void testEditOverrideListApexErrors(){
         setupData();  
        System.debug('opp test---->');
        Account a = [Select id from Account Limit 1];
        ObjectFieldSetMapping__c ofc=[select id from ObjectFieldSetMapping__c where Name='AccParent_OppChildFR' limit 1];
        Test.startTest();
        PageReference pf = new PageReference('/apex/EditOverride?'+a.Id);
        Test.setCurrentPage(pf);
        
        EditOverrideController con = new EditOverrideController();
        
        ApexPages.currentPage().getParameters().put('sobjectName','Opportunities' );
        ApexPages.currentPage().getParameters().put('recordIndex','1' );
        try{
        con.deleteMethod();
        }catch(exception e){
            
        }
        PageReference pf2 = new PageReference('/apex/EditOverride?id= ');
        Test.setCurrentPage(pf2);
        EditOverrideController con2 = new EditOverrideController();
        PageReference pf3 = new PageReference('/apex/EditOverride?id=12345');
        Test.setCurrentPage(pf3);
        EditOverrideController con3 = new EditOverrideController();
        PageReference pf4 = new PageReference('/apex/EditOverride?id='+ofc.ID);
        Test.setCurrentPage(pf4);
        //EditOverrideController con4 = new EditOverrideController();
        con.selectedRelation='Opportunity';
        con.addRow();
        con.cancel();
        con.renderValue = true;
        try{
        con.save();
        }catch(exception e){
            
        }
        con.mapSobjectType = new map<String, SObjectType>();
        con.mapSobjectType.put('ABC', Opportunity.getsobjectType());
       con.selectedRelation='ABC';
       try{
       con.addRow();
       }catch(exception e){
            
        }
        PageReference pf5 = new PageReference('/apex/EditOverride?');
        Test.setCurrentPage(pf5);
        EditOverrideController con5 = new EditOverrideController();
        
        Test.stopTest();
    }

    
    
}