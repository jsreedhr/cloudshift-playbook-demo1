/**
*  Class Name: OpportunityTriggerHandlerTest
*  Description: Test class for trigger OpportunityTrigger and any Opportunity Apex classes

*  Company: CloudShift
*  CreatedDate: 30/05/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  ----------------------------------------------------------- 
*  Kaspars Rezgalis		30/05/2018					Created
*
*/

@isTest(seeAllData=false)
private class OpportunityTriggerHandlerTest{
	
	@isTest
	private static void test_OpportunitySharing(){
		//full tests for custom sharing functionality are located in SharingEngineTest; these are just basic tests for trigger coverage
		ConfigurationManager configManager = ConfigurationManager.getInstance();
		configManager.turnSharingOff();
		configManager.commitConfigurationChanges();

		Group testGroup = new Group(
			Name = 'testGroup'
		);
		Database.Insert(testGroup, true);

		Account a = TestObjectHelper.createAccount();
		Database.Insert(a, true);

		Opportunity o = TestObjectHelper.createOpportunity(a);
		Database.Insert(o, true);		

		Sharing_Rule__c s = new Sharing_Rule__c(
			Name = 'OppTestRule',
			Object__c = 'Opportunity',
			Description__c = 'Apex TEST rule',
			Access_Level__c = SharingEngine.SHARING_ACCESS_EDIT,
			Logic__c = SharingEngine.SHARING_LOGIC_ANY,
			Shared_With__c = testGroup.Id
		);
		Database.Insert(s, true);

		Sharing_Condition__c sc = new Sharing_Condition__c(
			Sharing_Rule__c = s.Id,
			Field_API_Name__c = 'AccountId',
			Operator__c = SharingEngine.SHARING_CONDITION_EQUALS,
			Value__c = a.Id,
      		Index__c = 1
		);
		Database.Insert(sc, true);

		s.Change_Status__c = SharingEngine.SHARING_RULE_CHANGE_STATUS_COMPLETED;
		Database.Update(s, true);

		configManager.turnSharingOn();
		configManager.commitConfigurationChanges();

		Test.startTest();
			o.Description = 'Updated';
			Database.Update(o, true);
		Test.stopTest();

		List<Error_Log__c>errors = new List<Error_Log__c>([SELECT Details__c FROM Error_Log__c WHERE Log_Type__c='Sharing Error']);
		System.assertEquals(0, errors.size(), 'There should be no error log entries. Actual: '+errors);

		Opportunity o_assert = [SELECT Sharing_Rows__c FROM Opportunity WHERE Id =:o.Id];
		List<OpportunityShare>sh = [SELECT Id FROM OpportunityShare WHERE RowCause=:SharingEngine.SHARING_ROW_CAUSE_STD_OBJECTS AND UserOrGroupId=:testGroup.Id AND OpportunityId=:o.Id];
		System.assertEquals(1, sh.size(), 'Number of manual share records for given opportunity should be 1 - one record was inserted via custom sharing functionality.');
		System.assert(String.isNotBlank(o_assert.Sharing_Rows__c), 'Sharing Rows field on opportunity should be updated to reflect sharing records inserted via custom sharing functionality.');
		System.assertEquals(sh[0].Id, o_assert.Sharing_Rows__c, 'Share record ID should match to record id stored on Opporunity Sharing_Rows__c.');
	}
}