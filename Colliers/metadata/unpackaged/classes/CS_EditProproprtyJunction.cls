/**
 *  Class Name:  RescheduleProjectRevenueController 
 *  Description: This is a class to Reschedule Project Revenue Records 
 *  Company: dQuotient
 *   CreatedDate: 18/08/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer      Modification Date      Comments
 *  -----------------------------------------------------------
 *  Hemant garg      19/08/2016            Orginal Version
 *
 *
 */
public with sharing class CS_EditProproprtyJunction{
    Public String Ids{get;set;}
    Public List<Property__c> ListProperty;
    Public List<booleanProproprtyWrapper> ListbooleanProproprtyWrapper{get;set;}
    Public List<booleanProproprtyJnWrapper> ListbooleanProproprtyJnWrapper{get;set;}
    public Map<id,Property__c> MapIdProprty{get;set;}
    Public string IdtoDelete{get;set;}
    public List<Job_Property_Junction__c> JobPrptyJn;
    Public string keyPrefix{get;set;}
    Public Boolean isError{get;set;}
    Public List<Company_Property_Affiliation__c> CompnyPrptyJn{get;set;}
    
    
    public CS_EditProproprtyJunction() {
        isError=false;
        Ids = apexpages.currentpage().getparameters().get('id');
       keyPrefix = String.valueOf(Ids).substring(0,3);
       //String keyPrefix = '001';
        
         

        ListbooleanProproprtyWrapper = new List<booleanProproprtyWrapper>(); 
        ListbooleanProproprtyJnWrapper= new List<booleanProproprtyJnWrapper>(); 
        CompnyPrptyJn= new List<Company_Property_Affiliation__c>(); 
        JobPrptyJn= new List<Job_Property_Junction__c>(); 
        
        MapIdProprty= new Map<id,Property__c>(); 
        ListProperty= [select id , name,Building_Name__c,Street__c,Post_Code__c,Town__c from Property__c];
        
        for(Property__c prpty:ListProperty){
            //ListbooleanProproprtyWrapper.add(new booleanProproprtyWrapper(false,prpty));
            MapIdProprty.put(prpty.id,prpty);
        }
        set<id> idSet= new set<id>();
        if(keyPrefix=='001'){
        CompnyPrptyJn=[select id ,Property__c,Property__r.id ,Property__r.Building_Name__c,Property__r.Street__c,Property__r.Post_Code__c, Property__r.Town__c from Company_Property_Affiliation__c where Company__c=:Ids Limit 1000];
        for(Company_Property_Affiliation__c CompntJn:CompnyPrptyJn){
            ListbooleanProproprtyJnWrapper.add(new booleanProproprtyJnWrapper(CompntJn.Property__r.id,CompntJn.Property__r.Building_Name__c,CompntJn.Property__r.Post_Code__c,CompntJn.Property__r.Street__c,CompntJn.Property__r.Town__c));
            idSet.add(CompntJn.Property__r.id);
        }
        }
        if(keyPrefix=='006'){
        JobPrptyJn=[select id ,Property__c,Property__r.id ,Property__r.Building_Name__c,Property__r.Street__c,Property__r.Post_Code__c, Property__r.Town__c from Job_Property_Junction__c where Job__c=:Ids Limit 1000];
        for(Job_Property_Junction__c CompntJn:JobPrptyJn){
            ListbooleanProproprtyJnWrapper.add(new booleanProproprtyJnWrapper(CompntJn.Property__r.id,CompntJn.Property__r.Building_Name__c,CompntJn.Property__r.Post_Code__c,CompntJn.Property__r.Street__c,CompntJn.Property__r.Town__c));
             idSet.add(CompntJn.Property__r.id); 
        }
        }
         for(Property__c prpty:ListProperty){
            if(idSet.contains(prpty.id)){ 
           
            }
            else{
                 ListbooleanProproprtyWrapper.add(new booleanProproprtyWrapper(false,prpty));
            }
        }
        
    }
    Public Void SelectedProperty(){
        string SlctedIds = ApexPages.currentPage().getParameters().get('Selids');
        system.debug('------ids'+SlctedIds);
        
          List<string> SelctedId = new List<string>();
        
       
          
        if(SlctedIds!=null && SlctedIds!=''){
            string.escapeSingleQuotes(SlctedIds);
            SelctedId = SlctedIds.split(',');
        }
        
        // ListbooleanProproprtyJnWrapper= new List<booleanProproprtyJnWrapper>();
        for(string idss:SelctedId){
              ListbooleanProproprtyJnWrapper.add(new booleanProproprtyJnWrapper(MapIdProprty.get(idss).id,MapIdProprty.get(idss).Building_Name__c,MapIdProprty.get(idss).Post_Code__c,MapIdProprty.get(idss).Street__c,MapIdProprty.get(idss).Town__c));
           for(Integer i=ListbooleanProproprtyWrapper.size()-1;i>=0;i--){
               if(string.valueof(ListbooleanProproprtyWrapper[i].Prprty.id) ==idss ){
                   ListbooleanProproprtyWrapper.remove(i);
               }
               
               
           } 
        }
        
        
        
    }
    Public void deleteSelectedRow(){
        system.debug('------idno'+IdtoDelete);
        system.debug('------id'+ListbooleanProproprtyJnWrapper[Integer.valueof(IdtoDelete)].Idss);
        
        ListbooleanProproprtyWrapper.add(new booleanProproprtyWrapper(false,MapIdProprty.get(ListbooleanProproprtyJnWrapper[Integer.valueof(IdtoDelete)].Idss)));
        ListbooleanProproprtyJnWrapper.remove(Integer.valueof(IdtoDelete));
        //ListbooleanProproprtyWrapper.add()
    }
     Public void SaveRecords(){
         try{
             isError=false;
        if(CompnyPrptyJn.size()>0){
        Delete CompnyPrptyJn;
        }
        if(JobPrptyJn.size()>0){
        Delete JobPrptyJn;
        }
        //ListbooleanProproprtyJnWrapper.remove(Integer.valueof(IdtoDelete));
        if(keyPrefix=='001'){
        List<Company_Property_Affiliation__c> ToUpdateProptyAccJn= new List<Company_Property_Affiliation__c>();
        for(booleanProproprtyJnWrapper CmpnyJn :ListbooleanProproprtyJnWrapper){
            Company_Property_Affiliation__c CpnyJn= new Company_Property_Affiliation__c();
            CpnyJn.Company__c=Ids;
            CpnyJn.Property__c=CmpnyJn.Idss;
            ToUpdateProptyAccJn.add(CpnyJn);
            
        }
        Insert ToUpdateProptyAccJn;
            
        }
        else if(keyPrefix=='006'){
             List<Job_Property_Junction__c> ToUpdateJobProptyAccJn= new List<Job_Property_Junction__c>();
        for(booleanProproprtyJnWrapper CmpnyJn :ListbooleanProproprtyJnWrapper){
            Job_Property_Junction__c CpnyJn= new Job_Property_Junction__c();
            CpnyJn.Job__c=Ids;
            CpnyJn.Property__c=CmpnyJn.Idss;
            ToUpdateJobProptyAccJn.add(CpnyJn);
            
        }
        Insert ToUpdateJobProptyAccJn;
        }
         }
         catch (Exception e) {
            ApexPages.addMessages(e);
           isError=true;
            return;
        }
    }
    
    
    public class booleanProproprtyWrapper{
        public Boolean isSelected{get;set;}
        public Property__c Prprty{get;set;}
       
        
        public booleanProproprtyWrapper(boolean isSelect, Property__c Prprtys ){
          Prprty = Prprtys;
          isSelected= isSelect;
         
          
          
           
        }
    }
    
    public class booleanProproprtyJnWrapper{
        //public Boolean isSelected{get;set;}
        public id Idss{get;set;}
        Public string Building{get;set;}
        Public string Postal{get;set;}
        Public string Street{get;set;}
        Public string Town{get;set;}
       
        
        public booleanProproprtyJnWrapper(id idsss ,string Buildings, string Postals,string Streets,string Towns ){
          Building = Buildings;
          Postal=Postals;
          Town=Towns;
          Street=Streets;
          Idss=Idsss;
          
          
         
          
          
           
        }
    }
}