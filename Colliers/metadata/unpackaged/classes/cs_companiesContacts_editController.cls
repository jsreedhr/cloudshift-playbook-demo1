/**
 *  Class Name: cs_companiesContactsController
 *  Description: Class to implement Companies and Contact tab Functionality
 *  Company: dQuotient
 *  CreatedDate:07/11/2016 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Jyoti             08/11/2016                 Orginal Version
 *
 */
 
public with sharing class cs_companiesContacts_editController{
    public String oppId{get;set;}
    public boolean isClosedJob{get;set;}
    public String sourceinstuct{get;set;}
    //private String accId{get;set;}
    public Opportunity jobDetails{get;set;}
    //public Boolean stagestatus{get;set;}
    public Account objCareOfClient{get;set;}
    public string isCareOf{get;set;} 
    //public String instructingAddress{get;set;}
    //public List<SelectOption> contactList{get;set;}
    public List<SelectOption> invAddressList {get;set;}
    public List<SelectOption> instructingAddressList{get;set;}
    public  List<AccountContactRelation> listContactRelation{get;set;}
    public  List<AccountContactRelation> listContactRelationInvoice{get;set;}
    //public List<SelectOption> staffDepLst{get;set;}
    //public List<SelectOption> staffOfcLst{get;set;}
    //public List<SelectOption> staffWorkTypeLst{get;set;}
    //public List<Contact> listContact{get;set;}
    public List<Contact> listInvContact{get;set;}
    
    //Public String insContactId {get;set;}
    //Public String invContactId {get;set;}
    Public String InstrContactAddress {get;set;}
    Public String InvContactAddress {get;set;}
    public boolean isDisabledTab{get;set;}
    //public String errorMessage {get;set;}
    
    //public List<String> errMsgLst {get;set;}
    
    public Boolean showErrMsg{get;set;}
    public string changeInv{get;set;} 
    public boolean pageload=false;
    public set<String> ofcSet;
    public Set<String> deptSet;
    public set<String> wrkTypSet;
        String invCompany;
        String invContact;
        String invAddress;
        String invCareOf;
        String instrContact;
    
    private Staff__c stfDetails;
    private map<String, List<Coding_Structure__c>> mapDeptCodingStructure;
    private map<String, List<Coding_Structure__c>> mapDeptOfficeCodingStructure;
    // private map<String, Coding_Structure__c> mapDeptOfficeWorkTypeCodingStructure;
    // Constructor
    public cs_companiesContacts_editController() {
        pageload=true;
        listContactRelation = new List<AccountContactRelation>();
        listContactRelationInvoice = new List<AccountContactRelation>();
        isClosedJob = false;
        stfDetails = new Staff__c();
        jobDetails = new Opportunity(); 
        objCareOfClient = new Account();
        isCareOf = 'NO';
        changeInv = 'NO';
        showErrMsg = false;
        List<Account> lstAccount = new List<Account>();
        mapDeptCodingStructure = new map<String, List<Coding_Structure__c>>();
        mapDeptOfficeCodingStructure = new map<String, List<Coding_Structure__c>>();
        // mapDeptOfficeWorkTypeCodingStructure = new map<String, List<Coding_Structure__c>>();
        oppId = ApexPages.currentPage().getParameters().get('id');
        
        if(!string.isBlank(oppId)){
            initCompaniesContacts();
           
            
        }/*else if(ApexPages.currentPage().getParameters().containsKey('AccId')){
            accId = ApexPages.currentPage().getParameters().get('AccId');
            if(!String.isBlank(accId) && (accId InstanceOf Id)){
                // lstAccount = [Select id, name From Account where id =:accId limit 1];
                // if(!lstAccount.isEmpty()){
                jobDetails.AccountId = accId;
                getJobContacts();
                // }
            }
        }*/else{
            
        }
        
        //updateisDisabled();
        pageload=false;
    }
    
    
    public pagereference gotoedit(){
        
        //Pagereference pg = new Pagereference('/apex/CS_ManageMyJobPage?id='+oppId+'&edit=yes');
        Pagereference pg = new Pagereference('/apex/CS_ManageMyJobPage?id='+oppId+'&edit=yes');
        return pg;
    }
    
    /**
     *  Method Name: initCompaniesContacts
     *  Description: Method to get the Job Details
     *  Param:  None
     *  Return: None
    */
    public void initCompaniesContacts(){
        listContactRelation = new List<AccountContactRelation>();
        listContactRelationInvoice = new List<AccountContactRelation>();
        objCareOfClient = new Account();
        oppId = ApexPages.currentPage().getParameters().get('id');
        if(!string.isBlank(oppId)){
            showErrMsg = false;
            List<Opportunity> lstOppTemp = new List<Opportunity>();
            lstOppTemp = [SELECT id,name,
                        Invoicing_Address_Formula__c, 
                        Instructing_Company_Address__c, 
                        Account.Name,   
                        Account.id,
                        Amount,isClosed, 
                        CloseDate,
                        Property_Area__c,
                        Property_Area_UOM__c,
                        Instructing_Company__c,
                        Colliers_Referrer__c,
                        Office_Referrer__c,
                        Client_Referral__c,
                        Source_Of_Instruction__c,
                        Invoice_Contact__r.name,
                        Date_Instructed__c,
                        Engagement_Letter__c,
                        InstructingCompanyAddress__c,
                        Manager__c,
                        Invoicing_Care_Of__r.Client__c,
                        Invoicing_Care_Of__r.Client__r.Name,
                        Department__c,
                        Work_Type__c,
                        Relates_To__c,
                        Office__c,
                        Instructing_Company_Role__c,
                        Referral_out_of_UK__c,
                        Instructing_Contact__c,
                        Instructing_Contact__r.name,
                        Invoicing_Company2__c,
                        Invoicing_Care_Of__c,
                        Invoice_Contact__c,
                        Invoicing_Address__c, 
                        CreatedDate 
                        from 
                        Opportunity 
                        where id=:oppId Limit 1];   
            
            if(!lstOppTemp.isEmpty()){
                jobDetails = lstOppTemp[0];
                
                instrContact = jobDetails.Instructing_Contact__c;
                invCompany =jobDetails.Invoicing_Company2__c;
                invCareOf = jobDetails.Invoicing_Care_Of__c;
                invContact =jobDetails.Invoice_Contact__c;
                invAddress =jobDetails.Invoicing_Address__c;
                if(jobDetails.isClosed){
                    isClosedJob = true;
                }
               
                InstrContactAddress = jobDetails.Instructing_Contact__c;
                InvContactAddress = jobDetails.Invoice_Contact__r.Name;
                
                
            }
        
       
           
            getJobContacts();
            //getInvoicingContact();
          //  getInvoicingContactAddress();
            //getStaffDept();
            //getStaffDeptOffice(); 
            //getStaffDeptOfcWorkType();
            getsourceofinstruct();
           // getInstructingAddress();
            
            if(jobDetails.Instructing_Contact__c != null && string.isNotBlank(jobDetails.Instructing_Contact__c)){
                InstrContactAddress = jobDetails.Instructing_Contact__r.Name;
            }else{
                InstrContactAddress = '';
            }
            if(jobDetails.Invoice_Contact__c != null && string.isNotBlank(jobDetails.Invoice_Contact__c)){
                InvContactAddress = jobDetails.Invoice_Contact__r.Name;
            }
            else{
                InvContactAddress = '';
            }
            
            
            if(!string.isBLANK(jobDetails.Invoicing_Care_Of__c)){
                    objCareOfClient.id = jobDetails.Invoicing_Care_Of__r.Client__c;
                    objCareOfClient.Name = jobDetails.Invoicing_Care_Of__r.Client__r.Name;
                    isCareOf = 'YES';    
                    changeInv = 'NO';
                }else if(jobDetails.Instructing_Contact__c==jobDetails.Invoice_Contact__c){
                     isCareOf = 'NO';    
                    changeInv = 'YES';
                }
                else{
                    isCareOf = 'NO';
                    changeInv = 'NO';
                    if(string.isBLANK(jobDetails.Invoicing_Company2__c)){
                        jobDetails.Invoicing_Company2__c = jobDetails.AccountId;   
                    }
                }
            
        }else{
            /*jobDetails = new Opportunity();  
            listContactRelation = new List<AccountContactRelation>();
            listContactRelationInvoice = new List<AccountContactRelation>();
            isCareOf = 'NO';
            changeInv = 'NO';
            showErrMsg = false;
            
            invAddressList= new List<SelectOption>();
            invAddressList.add(new SelectOption('','-- None --'));
            
            instructingAddressList= new List<SelectOption>();
            instructingAddressList.add(new SelectOption('','-- None --'));
            
            staffDepLst= new List<SelectOption>();
            staffDepLst.add(new SelectOption('','-- None --'));
            
            staffOfcLst= new List<SelectOption>();
            staffOfcLst.add(new SelectOption('','-- None --'));
            
            staffWorkTypeLst= new List<SelectOption>();
            staffWorkTypeLst.add(new SelectOption('','-- None --'));
            
            if(ApexPages.currentPage().getParameters().containsKey('AccId')){
                accId = ApexPages.currentPage().getParameters().get('AccId');
                if(!String.isBlank(accId) && (accId InstanceOf Id)){
                    // lstAccount = [Select id, name From Account where id =:accId limit 1];
                    // if(!lstAccount.isEmpty()){
                    jobDetails.AccountId = accId;
                    getJobContacts();
                    // }
                }
            }*/
        }
        
        //updateisDisabled();
        pageload=false;
    }
    
   
     /**
     *  Method Name: getsourceofinstruct
     *  Description: Method to get the source of instructer
     *  Param:  None
     *  Return: None
    */
    public void getsourceofinstruct(){
        sourceinstuct = '';
        if(jobDetails.Source_Of_Instruction__c != null && jobDetails.Source_Of_Instruction__c != ''){
            if(jobDetails.Source_Of_Instruction__c == 'Referral from Colliers UK'){
                sourceinstuct += 'collreffer';
            }
            else if(jobDetails.Source_Of_Instruction__c == 'Referral from Colliers overseas (inc. Belfast)'){
                sourceinstuct += 'officereffer';
            }
            else if( jobDetails.Source_Of_Instruction__c == 'Client - referred by another client' ){
                sourceinstuct += 'clientreffer';
            }else{
                sourceinstuct = '';
            }
        }
        system.debug('fetchsourceofinstruct'+sourceinstuct);
        
        
    }
   
    
    
    
    /**
     *  Method Name: getJobContacts
     *  Description: Method to get all the Contacts associated to the Instructing Company(Account)
     *  Param:  None
     *  Return: pageReference 
    */ 
    public pageReference getJobContacts(){
        Set<Id> cId = new Set<Id>();
        instructingAddressList = new List<SelectOption>();
        InstrContactAddress = '';
        if(jobDetails.Invoicing_Care_Of__c!=null){
            isCareOf='YES';
        }
       
        List<AccountContactRelation> accContactLst = new List<AccountContactRelation>();
        listContactRelation = new List<AccountContactRelation>();
    
       /* Account acc = new Account();
        
        if(!string.isBlank(jobDetails.AccountId)){
            listContactRelation = [SELECT id,Contact.Name,Contact.FirstName, Contact.LastName, ContactId,Account.Id from AccountContactRelation where Account.Id=:jobDetails.AccountId];
          


       }else{
            // Error Message No Comapny Selected
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No Instructing Company Selected,Please Select the Instructing Comapny.'));
        }
        */
      
        if(!pageload)
            changerenderInvoiceDetails();
        return null;
    }
    

    
    public void changeInvoiceDetails(){
    
    }
    
    /*public void createContactAddressJunction(){
        List<Contact_Address_Junction__c> ContactAddressLst = new List<Contact_Address_Junction__c>();
        List<Account_Address_Junction__c> AccountAddressLst = new List<Account_Address_Junction__c>();
        set<Id> AccaddressLst = new Set<Id>();
        set<Id> cLst = new set<Id>();
        set<Id> ConAddressLst = new Set<Id>();
        
        // Logic to CReate a Contact Address Junction For Instructing Company
        
        if(!string.isBlank(jobDetails.InstructingCompanyAddress__c) && !string.isBlank(jobDetails.Instructing_Contact__c) && !string.isBlank(jobDetails.AccountId)){
            AccountAddressLst = [SELECT id,Address__c,Account__c from Account_Address_Junction__c where Account__c=:jobDetails.AccountId ]; 
            for(Account_Address_Junction__c acc:AccountAddressLst){
                AccaddressLst.add(acc.Address__c);   
            }
           ContactAddressLst = [SELECT id,Address__c,Contact__c,Contact__r.AccountId from Contact_Address_Junction__c where Contact__c=:jobDetails.Instructing_Contact__c];
           for(Contact_Address_Junction__c con:ContactAddressLst){
                 ConAddressLst.add(con.Address__c); 
           }
       
           if(AccaddressLst.contains(jobDetails.InstructingCompanyAddress__c) && !ConAddressLst.contains(jobDetails.InstructingCompanyAddress__c)){
                Contact_Address_Junction__c cj = new Contact_Address_Junction__c();
                cj.Contact__c = jobDetails.Instructing_Contact__c;
                cj.Address__c = jobDetails.InstructingCompanyAddress__c;
                insert cj;
           }
            
        }
        
         // Logic to CReate a Contact Address Junction For Invoicing Company
        
        ContactAddressLst = new List<Contact_Address_Junction__c>();
        AccountAddressLst = new List<Account_Address_Junction__c>();
        AccaddressLst = new Set<Id>();
        cLst = new set<Id>();
        ConAddressLst = new Set<Id>();

        if(isCareOf=='NO'){
            
            if(!string.isBlank(jobDetails.Invoicing_Address__c) && !string.isBlank(jobDetails.Invoice_Contact__c) && !string.isBlank(jobDetails.Invoicing_Company2__c)){
                AccountAddressLst = [SELECT id,Address__c,Account__c from Account_Address_Junction__c where Account__c=:jobDetails.Invoicing_Company2__c ]; 
                for(Account_Address_Junction__c acc:AccountAddressLst){
                    AccaddressLst.add(acc.Address__c);   
                }
                
               ContactAddressLst = [SELECT id,Address__c,Contact__c,Contact__r.AccountId from Contact_Address_Junction__c where  Contact__c=:jobDetails.Invoice_Contact__c];
               for(Contact_Address_Junction__c con:ContactAddressLst){
                     ConAddressLst.add(con.Address__c); 
               }

               if(AccaddressLst.contains(jobDetails.Invoicing_Address__c) && !ConAddressLst.contains(jobDetails.Invoicing_Address__c)){
                    Contact_Address_Junction__c cj = new Contact_Address_Junction__c();
                    cj.Contact__c = jobDetails.Invoice_Contact__c;
                    cj.Address__c = jobDetails.Invoicing_Address__c;
                    insert cj;
               }
                
            }
        }else{
            if(!string.isBlank(jobDetails.Invoicing_Address__c) && !string.isBlank(jobDetails.Invoice_Contact__c) && !string.isBlank(jobDetails.Invoicing_Care_Of__c)){
                String AccId = [SELECT id,name,client__c from Care_Of__c where id=:jobDetails.Invoicing_Care_Of__c].client__c;
                AccountAddressLst = [SELECT id,Address__c,Account__c from Account_Address_Junction__c where Account__c=:AccId ]; 
                for(Account_Address_Junction__c acc:AccountAddressLst){
                    AccaddressLst.add(acc.Address__c);   
                }
                
               ContactAddressLst = [SELECT id,Address__c,Contact__c,Contact__r.AccountId from Contact_Address_Junction__c where  Contact__c=:jobDetails.Invoice_Contact__c];
               for(Contact_Address_Junction__c con:ContactAddressLst){
                     ConAddressLst.add(con.Address__c); 
               }
          
               if(AccaddressLst.contains(jobDetails.Invoicing_Address__c) && !ConAddressLst.contains(jobDetails.Invoicing_Address__c)){
                    Contact_Address_Junction__c cj = new Contact_Address_Junction__c();
                    cj.Contact__c = jobDetails.Invoice_Contact__c;
                    cj.Address__c = jobDetails.Invoicing_Address__c;
                    insert cj;
               }
                
            }
            
        }

    }
*/
    /**
     *  Method Name: checkCareOf
     *  Description: Method to check if CareOf is checked
     *  Param:  None
     *  Return: None
    */ 
    /*public void checkCareOf(){
        Id invId;
        objCareOfClient = new Account();
        if(isCareOf =='YES'){
               if(string.isBlank(invCareOf)){
                   resetInvPanel(); 
               }else{
                   getInvoicingContact();
                   if(!string.isBlank(invContact)){
                      InvContactAddress = invContact;
                   }
                 //  getInvoicingContactAddress();    
               }
               
               if(string.isBlank(jobDetails.Invoicing_Care_Of__c)){
                   resetInvPanel();
               }else{
                  getInvoicingContact();
                  if(!string.isBlank(jobDetails.Invoice_Contact__c)){
                      InvContactAddress = jobDetails.Invoice_Contact__r.Name;
                   }
                 //  getInvoicingContactAddress(); 
               }
               
        }else{
            system.debug('Not a care Of');
            if(changeInv == 'YES'){
                system.debug('Is Change Invoice--->');
                system.debug('invCompany--->'+invCompany);
                system.debug('jobDetails.Invoicing_Company2__c--->'+jobDetails.Invoicing_Company2__c);
               if(string.isBlank(invCompany)){
                   
                   if(string.isBlank(jobDetails.Invoicing_Company2__c)){
                    invAddressList = new List<SelectOption>();    
                    listContact= new List<Contact>();
                    InvContactAddress = ''; 
                   
                        if(!string.isBlank(jobDetails.AccountId)){
                            jobDetails.Invoicing_Company2__c = jobDetails.AccountId; 
                            jobDetails.Invoicing_Address__c = jobDetails.InstructingCompanyAddress__c;
                            jobDetails.Invoice_Contact__c = jobDetails.Instructing_Contact__c;
                            InvContactAddress = jobDetails.Invoice_Contact__r.Name;
                            invAddressList = instructingAddressList;
                        }
                    }else{
                        //IF Change invoice is NO
                        jobDetails.Invoicing_Company2__c = invId;   
                        resetInvPanel();
                    }   
                }else{
                    getInvoicingContact();
                    if(!string.isBlank(invContact)){
                      InvContactAddress = invContact;
                    }
                   // getInvoicingContactAddress(); 
                }
                 
            }else{
                if(changeInv == 'YES'){
                    if(!string.isBlank(jobDetails.AccountId)){
                        jobDetails.Invoicing_Company2__c = jobDetails.AccountId; 
                        getInvoicingContact();
                        if(!string.isBlank(jobDetails.Invoice_Contact__c)){
                            InvContactAddress = jobDetails.Invoice_Contact__r.Name;
                        }
                        //getInvoicingContactAddress(); 
                    }
                }else{
                    //IF Change invoice is NO
                    jobDetails.Invoicing_Company2__c = invId;   
                    //resetInvPanel();
                }
            }
               
        }
        //renderInvoiceDetails();
            
    }*/
    
    /**
     *  Method Name: getInvoicingContact
     *  Description: Method to get the Contacts associated with the Invoicing Company
     *  Param:  None
     *  Return: pageReference 
    */ 
    /*public pageReference getInvoicingContact(){
        Set<Id> cId = new Set<Id>();
        invAddressList= new List<SelectOption>();
        System.debug('--isCareOf---'+isCareOf);
        objCareOfClient = new Account();
        List<AccountContactRelation> accContactLst = new List<AccountContactRelation>();
        List<Care_Of__c> careOfLst = new List<Care_Of__c>();
        if(!string.isBlank(jobDetails.Invoicing_Company2__c) && isCareOf=='NO'){
            invAddressList= new List<SelectOption>();
            InvContactAddress='';
            // listContactRelationInvoice = new List<AccountContactRelation>();
            listInvContact = new List<Contact>();
            accContactLst = [SELECT id,Contact.Name,ContactId,Account.Id from AccountContactRelation where Account.Id=:jobDetails.Invoicing_Company2__c];
            if(accContactLst.size()>0){
                for(AccountContactRelation accRol :accContactLst){
                    cId.add(accRol.ContactId);
                }
                listInvContact = [SELECT id,name from Contact where Id IN:cId and Id != null];
            }else{
                // No Contact exist for the Company
                 //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No Contact Exist for the Invoicing Company Selected.'));
            }  
        }else if(!string.isBlank(jobDetails.Invoicing_Care_Of__c) && isCareOf=='YES'){
            invAddressList= new List<SelectOption>();
            InvContactAddress='';
            // listContactRelationInvoice = new List<AccountContactRelation>();
            listInvContact = new List<Contact>();
            careOfLst =[SELECT id,Name,Client__c, Client__r.Name, Contact__c,Contact__r.Id,Contact__r.Name from Care_Of__c where id=:jobDetails.Invoicing_Care_Of__c];  
            system.debug('careOfLst');
            if(careOfLst.size()>0){
                for(Care_Of__c co : careOfLst){
                    cId.add(co.Client__c);
                    if(jobDetails.Invoicing_Care_Of__c != null){
                        if(co.Client__c != null){
                            objCareOfClient.id =co.Client__c;
                            objCareOfClient.Name = co.Client__r.Name;
                        }
                    }
                    
                    system.debug('co.Client__c---->'+co.Client__c);
                }
                listInvContact = [SELECT id,name from Contact where AccountId IN:cId and AccountId != null];
            }else{
                // No care Of Contact exist for Company
                 //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No Contact Exist for the Care of Company Selected.'));
            }
        }else{
            // No Invoicing Company Selected 
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No Invoicing Company Selected.Please select the Invoicing Company.'));
        }
        //getInvoicingContactAddress();
        return null;
        
    }*/

  
    
    
    
   
    
    
    
    /**
     *  Method Name: getItems
     *  Description: Method to populate the Radio Button
     *  Param:  None
     *  Return: List<SelectOption>
    */ 
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('YES','YES'));
        options.add(new SelectOption('NO','NO'));
        return options;
    }
    
    /**
     *  Method Name: getInvItems
     *  Description: Method to populate the Radio Button
     *  Param:  None
     *  Return: List<SelectOption>
    */ 
    public List<SelectOption> getInvItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('YES','YES'));
        options.add(new SelectOption('NO','NO'));
        return options;
    }
    
    public void changerenderInvoiceDetails(){
        changeInv='NO';
        isCareOf='NO';
      
    } 
    
    /*public void renderInvoiceDetails(){
        jobDetails.Invoice_Contact__c=null;
        jobDetails.Invoicing_Company2__c=null;
        jobDetails.Invoicing_Care_Of__c=null;
        jobDetails.Invoicing_Address__c=null;
        invAddressList = new List<Selectoption>(); 
        InvContactAddress='';
      //  jobDetails.Instructing_Company_Role__c=null;
    system.debug('changeInv --->'+changeInv );
        Id InvId;
        if(changeInv == 'YES'){
            system.debug('-------YES---------------'); 
            if(isCareOf == 'NO'){
               if(string.isBlank(invCompany)){
                   resetInvPanel();
               }else{
              getInvoicingContact();
              if(!string.isBlank(invContact)){
                  InvContactAddress = [SELECT id,Name from contact where id=:invContact Limit 1].Name;
               }
             //   getInvoicingContactAddress(); 
               }
           
               if(string.isBlank(jobDetails.Invoicing_Company2__c)||String.isBlank(jobDetails.Invoice_Contact__c)){
               resetInvPanel();
               
               if(!string.isBlank(jobDetails.AccountId)){
                  if(jobDetails.AccountId!=null)jobDetails.Invoicing_Company2__c = jobDetails.AccountId; 
                  if(jobDetails.InstructingCompanyAddress__c!=null)jobDetails.Invoicing_Address__c = jobDetails.InstructingCompanyAddress__c;
                  if(jobDetails.Instructing_Contact__c!=null){
                      jobDetails.Invoice_Contact__c = jobDetails.Instructing_Contact__c;
                      list<contact> ConList = [SELECT id,Name from contact where id=:jobDetails.Invoice_Contact__c Limit 100];
                      if(ConList!=null&&ConList.size()>0){
                      InvContactAddress = ConList[0].Name;
                      invAddressList = instructingAddressList;
                      }
               }
               }
                }
            }else{
               if(string.isBlank(invCareOf)){
                   jobDetails.Invoicing_Company2__c = invId; 
                   resetInvPanel();
               }else{
               getInvoicingContact();
               if(!string.isBlank(invContact)){
                  InvContactAddress = [SELECT id,Name from contact where id=:invContact].Name;
               }
            //   getInvoicingContactAddress();    
               
               }
           
               if(string.isBlank(jobDetails.Invoicing_Care_Of__c)){
                   jobDetails.Invoicing_Company2__c = invId; 
                   resetInvPanel();    
               }else{
              getInvoicingContact();
              if(!string.isBlank(jobDetails.Invoice_Contact__c)){
                  InvContactAddress = [SELECT id,Name from contact where id=:jobDetails.Invoice_Contact__c].Name;
               }
             //  getInvoicingContactAddress(); 
               }
            
            }
        }else{
            // jobDetails.Invoicing_Company2__c = invId; 
            // resetInvPanel();
            if(  isCareOf == 'NO'){
                jobDetails.Invoicing_Company2__c = invId; 
                resetInvPanel();
            
            }else if((string.isBlank(invCareOf)) && isCareOf == 'YES'){
                jobDetails.Invoicing_Company2__c = invId;   
                resetInvPanel();
            }else{
                if(!string.isBlank(invCompany)){
                    jobDetails.Invoicing_Company2__c  = invCompany;
                    getInvoicingContact();
                    if(!string.isBlank(invContact)){
                      InvContactAddress = [SELECT id,Name from contact where id=:invContact].Name;
                    }
                //    getInvoicingContactAddress(); 
                }
            
            }
            
        }
    
    }*/
    
    public void resetInvPanel(){
        listInvContact = new List<contact>(); 
        InvContactAddress = '';
        invAddressList = new List<SelectOption>();
    }
    
    
     //public boolean isDisabled{get;set;}
     /*public void updateisDisabled(){     
        isDisabledTab = true;
        opportunity jobObject=new opportunity();        
        if(ApexPages.currentPage().getParameters().containsKey('id')){
            isDisabledTab = false;
            String recordId = ApexPages.currentPage().getParameters().get('id');
                if(!String.isBlank(recordId)){
                    recordId = String.escapeSingleQuotes(recordId);
                        if(recordId InstanceOf Id){                        
                           // jobObject=[SELECT Manager__c,Job_Number__c,Name,Account.Name,AccountId,Invoicing_Company2__c,Invoicing_Address__c,Invoicing_Care_Of__c,Invoice_Contact__c,Instructing_Contact__c,Manager__r.Name FROM opportunity WHERE ID=:recordId];   
                            if(string.isBLANK(jobDetails.AccountId) ){
                                isDisabledTab = true;    
                                
                            }else if(!string.isBLANK(jobDetails.AccountId) ){
                                if(!string.isBLANK(jobDetails.Invoicing_Company2__c) && string.isBLANK(jobDetails.Invoicing_Care_Of__c)){
                                    isDisabledTab = false;     
                                }else if(string.isBLANK(jobDetails.Invoicing_Company2__c) && !string.isBLANK(jobDetails.Invoicing_Care_Of__c)){
                                    isDisabledTab = false;      
                                }else{
                                    isDisabledTab = true;      
                                }
                                   
                            }else{
                                isDisabledTab = false;    
                            }                                      
                        }else{
                           // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.InvalidParamterError));
                        }
                }else{
                 //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.InvalidParamterError));
                }
        }else{
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.InvalidParamterError));
        }
        
        
    }*/
        
    
}