@isTest(seealldata=True)
public with sharing class VALS_ComparableJobProcessor_Test
{ 
    @isTest
    private static void testMethod1()
    {

      list<String> propertyTypesString = new list<String>();
      propertyTypesString.add('abc');
        
      account accountObj = new account ();
      accountObj = TestObjectHelper.createAccount();
      accountObj.PO_Required__c = 'Yes';
      accountObj.Client_VAT_Number__c='123456';
      insert accountobj;
      System.assert(accountObj.id!=NULL);
      
      Property__c propertyObj2 = new Property__c ();
      propertyObj2=TestObjectHelper.createProperty();
      propertyObj2.Street_No__c ='Park Lane 2';
      propertyObj2.Post_Code__c ='W1K 3DD';
      propertyObj2.Country__c = 'United Kingdom';
      propertyObj2.Geolocation__Latitude__s=51.51;
      propertyObj2.Geolocation__Longitude__s=-0.15;
      propertyObj2.Override_Experian_validation__c=true;
      insert propertyObj2;
      System.assert(propertyObj2.id!=NULL);
      
      Address__c addressObj = new Address__c ();
      addressObj = TestObjecthelper.createAddress(accountObj);
      addressObj.Country__c = 'United Kingdom';
      insert addressObj;
      System.assert(addressObj.id!=NULL);

      WorkType__c CustSetting2 = new WorkType__c();
      Custsetting2.name='TestworkType';
      insert CustSetting2;
      
      PropertyType__c Custsetting= new PropertyType__c();
      Custsetting.name='Test';
      insert CustSetting;
      
      opportunity oppObj = new opportunity ();    
      oppObj = TestObjectHelper.createOpportunity(accountObj);
      oppObj.Sent_to_ShareDo__c=True;
      oppObj.StageName='Instructed';
      oppObj.Work_Type__C='Acquisition - Leasehold';
      oppObj.AccountId = accountObj.id;
      oppObj.InstructingCompanyAddress__c = addressObj.id;
      oppObj.Property_Address__c = propertyObj2.id;
      oppObj.Instructing_Contact__c = oppObj.Instructing_Contact__c;
      insert oppObj;
      System.assert(oppObj.id!=NULL);
      
      contact contactObj = new contact ();
      contactObj = TestObjecthelper.createContact(accountObj);
      contactObj.Email='Cersei@kingslanding.com';
      contactobj.LastName='Lannister';
      contactObj.HomePhone='12345678';
      contactObj.MobilePhone='12345678';
      contactObj.Phone='12345678';
      contactObj.Fax='12345678';
      insert contactObj;
      System.assert(contactObj.id!=NULL);
      
      
      Date todaydate = Date.today();
      todaydate=todaydate.addDays(-1);
      
      
      Invoice__C inv=new Invoice__C();
      inv.Contact__c=contactObj.id;
      inv.Date_of_Invoice__c=todaydate;
      inv.Opportunity__C=oppObj.id;
      insert inv;
      
        
        Job_Property_Junction__c junction = new Job_Property_Junction__c();        
         junction.Job__c = oppobj.id;
         junction.Property__c = propertyObj2.id;
         insert junction;
        System.assert(junction.id!=NULL);
        
        Job_Property_Junction__c jun = new Job_Property_Junction__c();        
        jun.Job__c = oppobj.id;
         jun.Property__c = propertyObj2.id;
         insert jun;
       System.assert(jun.id!=NULL);
        
         ShareDo_Credentials__c sdc = new ShareDo_Credentials__c();
        sdc.Name = 'Sharedo Inspe';
        sdc.API_Key__c = 'e141df7b-b4e7-458c-a7a7-0f267803fcc8';
        sdc.ShareDo_Password__c = 'SFPsh123!';
        sdc.ShareDo_Username__c = 'EU\\svc_sharedoAPI';
        sdc.shareDoUrl__c = 'https://uatvals-express.colliersemea.com/api/inspections/';
        insert sdc;
        System.assert(sdc.id!=NULL);
        
        MarketValue__c mkv = new MarketValue__c();
        mkv.Name = 'Leasehold (Investment)';
        mkv.Worktype_Name__c = 'Acquisition - Leasehold';
         insert mkv;
        System.assert(mkv.id!=NULL);
        
        MarketRent__c  mkr = new MarketRent__c();
        mkr.Name = 'Leasehold (Occupational)';
        mkr.Worktype_Name__c = 'Acquisition - Leasehold';
        insert mkr;
         System.assert(mkr.id!=NULL);
        
      oppObj.StageName='Pitching';
      update oppObj;
        
      VALS_ComparableJobProcessor.doCallOut('testjsondate',junction.Id);
      VALS_ComparableJobProcessor cont1= new VALS_ComparableJobProcessor (junction);
        // VALS_ComparableJobProcessor cont1= new VALS_ComparableJobProcessor (propertyObj2.id ,oppObj.id);
   

    //  VALS_ComparableJobProcessor.MarketRentExtensionsWrapper mretw = new VALS_ComparableJobProcessor.MarketRentExtensionsWrapper('system.today()',12.1, 0.3);
     VALS_ComparableJobProcessor.MarketRentExtensionsWrapper mretw = new VALS_ComparableJobProcessor.MarketRentExtensionsWrapper(12.1, 0.3);
              VALS_ComparableJobProcessor.PropertyWrapper ptw = new VALS_ComparableJobProcessor.PropertyWrapper(10.2);
             VALS_ComparableJobProcessor.LocationWrapper ltw = new VALS_ComparableJobProcessor.LocationWrapper('myaddress','test2',
							   'addressLine3','addressLine4',
							   'town', 'county','postCode',
							   'externalReference','name',
							   'testData');
         VALS_ComparableJobProcessor.MarketValueExtensionsWrapper mvetw = new VALS_ComparableJobProcessor.MarketValueExtensionsWrapper( 12.1, 0.3);
        
       
	 VALS_ComparableJobProcessor.MarketValueWrapper mvtw = new VALS_ComparableJobProcessor.MarketValueWrapper('title','type','testdata',
								 propertyTypesString,'source','contactEmployeeReference' , mvetw, ptw,
								  ltw,system.today(),'testdata','testdata','testData','9999999999'); 

              VALS_ComparableJobProcessor.MarketRentWrapper mrtw = new VALS_ComparableJobProcessor.MarketRentWrapper('title','type','name',
								 propertyTypesString,'source','contactEmployeeReference',
								  mretw, ptw,
								  ltw,system.today(),'jobManager','jobmanageremail','jobmanagerdept','9899999999');
 
        
   
         }
}