/**
 *  Class Name: SharingRuleTestHelper
 *  Description: this helper class will be responsable for creating sharing rule sobjects, 
 *  it will default any required fields with constants
 *  Company: CloudShift
 *  CreatedDate: 29/05/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Gavin Palmer             29/05/2018                 Orginal Version
 *
 */
@isTest
public class SharingRuleTestHelper {

    public static String NAME = 'Test Rule';
    public static String DESCRIPTION = 'Test Description';
    public static String SHARED_WITH_ID = 'fakeid';
    public static String ACCESS_LEVEL = SharingEngine.SHARING_ACCESS_EDIT;
    public static String LOGIC = SharingEngine.SHARING_LOGIC_ALL;
    public static String OBJECT_NAME = 'Opportunity';
    public static String CHANGE_STATUS = SharingEngine.SHARING_RULE_CHANGE_STATUS_IN_PROGRESS;

	public static Sharing_Rule__c getSharingRule() {
        return new Sharing_Rule__c(
            Name = NAME,
            Description__c = DESCRIPTION,
            Shared_With__c = SHARED_WITH_ID,
            Access_Level__c = ACCESS_LEVEL,
            Logic__c = LOGIC,
            Object__c = OBJECT_NAME,
            Change_Status__c = CHANGE_STATUS
        );
    }

    public static Sharing_Rule__c insertSharingRule() {
        Sharing_Rule__c sharingRuleToInsert = getSharingRule();
        insert sharingRuleToInsert;
        return sharingRuleToInsert;
    }
}