@isTest
private class CS_InvoicePdfControllerTest {


	static Invoice__c inv;
	static Invoice__c inv1;
	public static Invoice__c inv2;
	public static Opportunity mainOpp;
	static Staff__c staf2;
	static List<Invoice__c> invList;
	static Allocation__c alloc;
	static Forecasting__c frcst;
	static Accrual__c acc;
	static Account a;
	static Contact c;
	static opportunity opp;
	static Disbursements__c Cost;
	static Staff__c stf;
	static Staff__c stfnile;
	static Invoice_Allocation_Junction__c invAll;
	static Invoice_Cost_Junction__c invCost;
	static Purchase_Order__c po;
	static AssignId__c asignId;
	static Invoice_Line_Item__c invLineItem;
	static List<Invoice_Line_Item__c> lstInvLineItem;
	static Purchase_Order_Invoice_Junction__c poInv;
	static List<Purchase_Order_Invoice_Junction__c> LstPoInv;
	static List<Invoice_Allocation_Junction__c> LstInvAll;
	static List<Purchase_Order__c> LstPo;
	static Invoice__c inve;
	static Invoice_PDF_Value__c invcPdf;
	static Property__c property;
	static Job_Property_Junction__c jobProp;
	static Job_Property_Junction__c jobProp1;
	static currency_Symbol__c currencyVal;
	static List<Invoice__c> invtlist;

	//variables added for D-0450
	static User userObj;
	static Account account;
	static Contact contact;
	static Opportunity opportunity;
	static Invoice__c invoice;
	static Invoice_Allocation_Junction__c invAllocationJn;
	static Allocation__c allocation;
	static Disbursements__c cost1;
	static List<Invoice_Line_Item__c> invoiceLineItems;
	Static Address__c address;
	Static Invoice_PDF_Value__c invoicePdf;
	Static Staff__c staff;
	Static Work_Type__c workType;
	Static Coding_Structure__c codingStructure;
	Static Contact_Address_Junction__c contactAddressJn;
	Static Account_Address_Junction__c accountAddressJn;
	Static Care_Of__c careOf;
	Static Invoice_Cost_Junction__c invoiceCostJn;
	Static Forecasting__c forecasting;
	Static Invoice_Allocation_Junction__c invoiceAllocationJn;


	static testmethod void setupData(){
		//create currency custom setting
		List<string> lstofcurrency = new List<string>();
		Schema.DescribeFieldResult fieldResult = Invoice__c.Invoice_Currency__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		if(!ple.isEmpty()){
			for( Schema.PicklistEntry f : ple){
				lstofcurrency.add(f.getValue());
			}
		}
		currencyVal=new currency_Symbol__c(Name='British Pound');
		if(!lstofcurrency.isEmpty()){
			currencyVal.currencyName__c = lstofcurrency.get(0);
		}else{
			currencyVal.currencyName__c = 'GBP';
		}
		insert currencyVal;
		currencyVal=new currency_Symbol__c(Name='EURO',currencyName__c='EUR');
		if(!lstofcurrency.isEmpty() && lstofcurrency.size()>1){
			currencyVal.currencyName__c = 'EUR';
		}
		insert currencyVal;

		//create account
		a = TestObjectHelper.createAccount();
		insert a ;

		//create contact
		c = TestObjectHelper.createContact(a);
		c.Email ='test1@test1.com';
		insert c;

		//create related Opportunity
		stf = TestObjectHelper.createStaff();
		stf.Active__c = true;
		stf.Name = Label.Nile_manager_pdf;
		stf.HR_No__c = 'ABCD';
		insert stf;
		
		staf2 = TestObjectHelper.createStaff();
        staf2.Email__c='resrstt8SZSHsdrfxzM.ZMSDLM@gmail.com';
        staf2.active__c = true;
		staf2.Active__c = true;
		staf2.Name = 'Test staff';
		staf2.HR_No__c = 'ABCDe';
		insert staf2;
		
		mainOpp = TestObjectHelper.createMultipleOpportunitys(a,staf2, 1)[0];
		insert mainOpp;
		/*stfnile = TestObjectHelper.createStaff();
		stf.Name = Label.Nile_manager_pdf;
		stfnile.Active__c = true;
		stfnile.HR_No__c = 'ABCD1';
		insert stfnile;*/
		List <Opportunity> opps = TestObjectHelper.createMultipleOpportunitys(a,stf, 10);
		insert opps;


		opp = [Select id from opportunity Limit 1];
		opp.Manager__c = stf.Id;
		opp.Invoicing_Company2__c = a.Id;
		update opp;

		//create related Opportunity
		List <Contact> cons = TestObjectHelper.createMultipleContact(a, 10);
		integer loopNo = 1;
		for(Contact objContact: cons){
			objContact.Email = 'test'+loopNo+'@test.com';
		}
		insert cons;
		property=TestObjectHelper.createProperty();
		property.Street_No__c ='Park Lane';
		property.Post_Code__c ='W1K 3DD';
		property.Country__c='United Kingdom';
		property.Geolocation__Latitude__s=51.51;
		property.Geolocation__Longitude__s=-0.15;
		insert property;

		//create Staff
		// Staff__c staffobj =  TestObjectHelper.createStaff();
		// insert staffobj;




		Coding_Structure__c codestructObj = new Coding_Structure__c();

		codestructObj.Staff__c = stf.Id;
		codestructObj.Department_Integer_Id__c = '620';
		codestructObj.Department_Cost_Code_Centre__c = '620';
		insert codestructObj;
		
		Coding_Structure__c codestructObj2 = new Coding_Structure__c();

		codestructObj2.Staff__c = stf.Id;
		codestructObj2.Department_Integer_Id__c = '618';
		codestructObj2.Department_Cost_Code_Centre__c = '618';
		insert codestructObj2;

		opp.Coding_Structure__c = codestructObj.id;
		update opp;
		mainOpp.Coding_Structure__c = codestructObj2.Id;
		update mainOpp;

		invcPdf = TestObjectHelper.createInvoicePDFValue(stf);
		invcPdf.Dept_Cost_Centre__c = '620';
		if(!lstofcurrency.isEmpty()){
			invcPdf.Currency__c = lstofcurrency.get(0);
		}else{
			invcPdf.Currency__c = 'GBP';
		}
		//invcPdf.Currency__c = 'GBP';
		insert invcPdf;

		Invoice_PDF_Value__c invcPdf1 = TestObjectHelper.createInvoicePDFValue(stf);
		invcPdf1.Dept_Cost_Centre__c = '620';
		if(!lstofcurrency.isEmpty()  && lstofcurrency.size()>1){
			invcPdf1.Currency__c = lstofcurrency.get(1);
		}else{
			invcPdf1.Currency__c = 'EUR';
		}
		//invcPdf1.Currency__c = 'EUR';
		insert invcPdf1;
		
		Invoice_PDF_Value__c invcPdf2 = TestObjectHelper.createInvoicePDFValue(stf);
		invcPdf2.Dept_Cost_Centre__c = '618';
		invcPdf2.Currency__c = 'GBP';
		//invcPdf1.Currency__c = 'EUR';
		insert invcPdf2;

		/*Invoice_PDF_Value__c invcPdf2 = TestObjectHelper.createInvoicePDFValue(stfnile);
		invcPdf1.HrNo__c = stfnile.Id;
		invcPdf2.Dept_Cost_Centre__c = '620';
		if(!lstofcurrency.isEmpty()  && lstofcurrency.size()>1){
			invcPdf2.Currency__c = lstofcurrency.get(1);
		}else{
			invcPdf2.Currency__c = 'GBP';
		}
		//invcPdf1.Currency__c = 'EUR';
		insert invcPdf2; */

		jobProp=TestObjectHelper.JobPrptyJn(opps[0],property);
		insert jobProp;
		jobProp1=TestObjectHelper.JobPrptyJn(opps[1],property);
		insert jobProp1;



		Address__c add = TestObjectHelper.createAddress(a);
		insert add;

		Care_Of__c careof = TestObjectHelper.createCareOf(a,cons[0]);
		careof.Colliers_Int__c  = true;
		careof.client__c = a.id;
		insert careof;

		Account_Address_Junction__c accadd =  TestObjectHelper.createAccountAddressJunction(a,add);
		insert accadd;

		invList=new List<Invoice__c>();
		inv = TestObjectHelper.createInvoice(opp,c);
		inv.Address__c = add.id;
		inv.Invoice_contact__c = c.Id;
		inv.company__c = a.id;
		inv.Invoice_Address__c='This address is being given';
		if(!lstofcurrency.isEmpty() && lstofcurrency.size()>1){
			inv.Invoice_Currency__c = lstofcurrency.get(1);
		}else{
			inv.Invoice_Currency__c = 'EUR';
		}
		//inv.Invoice_Currency__c = 'EUR';
		invList.add(inv);
		inv1 = TestObjectHelper.createInvoice(opps[1],c);
		inv1.Invoice_Address__c='This address is being given';
		inv1.Care_Of_Information__c = careof.id;
		inv1.Invoice_contact__c = c.Id;
		//inv1.Care_Of_Information__r.client__c = a.id;
		inv1.Address__c = add.id;
		if(!lstofcurrency.isEmpty()){
			inv1.Invoice_Currency__c = lstofcurrency.get(0);
		}else{
			inv1.Invoice_Currency__c = 'GBP';
		}
		inv1.Invoice_Currency__c = 'GBP';
		invList.add(inv1);
		
		inv2 = TestObjectHelper.createInvoice(mainOpp,c);
		inv2.Invoice_Address__c='This address is being given';
		inv2.Care_Of_Information__c = careof.id;
		inv2.Invoice_contact__c = c.Id;
		//inv1.Care_Of_Information__r.client__c = a.id;
		inv2.Address__c = add.id;
		inv2.Invoice_Currency__c = 'GBP';
		invList.add(inv2);
		insert  invList;

		invLineItem = TestObjectHelper.createInvoiceLineItem(inv);
		insert invLineItem;
		System.assertNotEquals(invLineItem.id , null);




	}
	private static testMethod void testCS_InvoicePdfController() {

		setupData();
		//inve = [Select id from Invoice__c ];

		Test.setCurrentPageReference(new PageReference('Page.cs_InvoicepdfView'));

		System.currentPageReference().getParameters().put('Invid', invList[0].id);
		System.currentPageReference().getParameters().put('Copy', 'COPY');


		Test.startTest();

		CS_InvoicePdfController obj = new CS_InvoicePdfController();

		System.assertEquals(obj.isCapital , false);
		System.currentPageReference().getParameters().put('Invid', inv2.Id);
		System.currentPageReference().getParameters().put('Copy', 'DRAFT');
		obj = new CS_InvoicePdfController();


		Test.stopTest();


	}
	private static testMethod void testCS_InvoicePdfListController(){
		setupData();
		inv.Status__c = 'Approved';
		update inv;
		inv1.Status__c = 'Approved';
		update inv1;
		PageReference pageRef = Page.CS_PrintInvoice;
		Test.setCurrentPage(pageRef);
		// inve = [Select id from Invoice__c Limit 1];
		invtlist=new List<Invoice__c>([Select id from Invoice__c]);
		system.debug('--->invList'+invtlist);
		//inv1 = [Select id from Invoice__c Limit 1];
		Test.startTest();
		ApexPages.StandardSetController sc = new ApexPages.StandardSetController(invtlist);
		sc.setSelected(invtlist);
		cs_pdflistController pdfList = new cs_pdflistController( sc);
		pdfList.updateinvoice();
		Test.stopTest();
	}
	private static testMethod void testCS_multipdflistController(){
		setupData();
		PageReference pageRef = Page.CS_PrintInvoiceSingle;
		Test.setCurrentPage(pageRef);
		//inve = [Select id from Invoice__c Limit 1];
		invtlist=new List<Invoice__c>([Select id from Invoice__c]);
		system.debug('--->invList'+invtlist);
		//inv1 = [Select id from Invoice__c Limit 1];
		Test.startTest();
		ApexPages.StandardSetController sc = new ApexPages.StandardSetController(invtlist);
		sc.setSelected(invtlist);
		cs_multipdflistController pdfList = new cs_multipdflistController( sc);
		pdfList.saveattch();
		Test.stopTest();
	}
	private static testMethod void testCS_creditNotePdfController() {

		setupData();
		//inve = [Select id from Invoice__c ];

		Test.setCurrentPageReference(new PageReference('Page.cs_CreditNotes'));

		System.currentPageReference().getParameters().put('Invid', invList[0].id);
		System.currentPageReference().getParameters().put('Copy', 'COPY');


		Test.startTest();
		Credit_Note__c obj =new Credit_Note__c();
		ApexPages.StandardController sc = new ApexPages.StandardController(obj);
		CS_CreditNotePdfController objected = new CS_CreditNotePdfController(sc);
		//System.assertEquals(obj.isCapital , false);

		System.currentPageReference().getParameters().put('Invid', invList[1].id);
		System.currentPageReference().getParameters().put('Copy', 'DRAFT');
		objected = new CS_CreditNotePdfController(sc);


		Test.stopTest();


	}
	static testmethod void createDataForD0450(){
		User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		// Insert account as current user
		System.runAs (currentUser) {
			// create user
			userObj = TestObjectHelper.createAdminUser(true);
			userobj.UserName ='Test_123@dquotient.com';
			insert userObj;
			System.assert(userObj.id!=NULL);
		}
		//account;
		account = new Account();
		account.Name = 'Some Name';
		account.Town__c = 'acc town';
		account.Street_No__c ='#12';
		account.Street__c ='Street';
		account.Post_Code__c = '2345';
		account.Country__c ='UK';
		account.Company_Status__c = 'Active';
		insert account;

		//contact
		contact = (Contact)SmartFactory.createSObject('Contact', false);
		contact.AccountId = account.Id;
		contact.lastName='Test';
		contact.Email ='test1@test1.com';
		insert contact;


		//Address__c
		address = new Address__c();
		address = (Address__c)SmartFactory.createSObject('Address__c', false);
		address.Address_Type__c = 'Primary';
		address.Area__c = '500';
		address.Building__c = 'XYZ Apartment';
		address.Country__c = 'Netherlands';
		address.Flat_Number__c = '301';
		address.Head_Office_Address__c = true;
		address.Postcode__c = '123 456';
		address.Street__c = 'Abc';
		address.Street_Number__c = '13A';
		address.Town__c = 'some town';

		address.Estate__c = 'estate';
		address.Flat_Number__c = '#12';
		address.Postcode__c = '2345';
		address.Country__c = 'United Kingdom';
		insert address;

		//Staff__c
		staff = new Staff__c();
		staff.name='Jaredresrstt8SDhHHZSHxzM';
		staff.Email__c='resrstt8SDhHHZSHxzM.ZMSDLM@gmail.com';
		staff.active__c = true;
		staff.HR_No__c = String.valueOf(Math.round(Math.random()*1000) + Math.random()*345);
		staff.User__c = userInfo.getUserId();
		insert staff;

		//Work_Type__c
		workType = new Work_Type__c();
		workType.Name = 'worktype';
		workType.Active__c = true;
		insert workType;

		codingStructure = new Coding_Structure__c();
		codingStructure.Work_Type__c = workType.Id;
		codingStructure.Staff__c = staff.Id;
		codingStructure.Status__c = 'Active';
		codingStructure.Department__c = 'International Properties';
		codingStructure.Office__c = 'Amsterdam';
		insert codingStructure;


		//Contact Address Jn
		contactAddressJn = new Contact_Address_Junction__c();
		contactAddressJn.Address__c = address.Id;
		contactAddressJn.Contact__c = contact.Id;
		insert contactAddressJn;


		//account Address Jn
		accountAddressJn = new Account_Address_Junction__c();
		accountAddressJn.Address__c = address.Id;
		accountAddressJn.Account__c = account.Id;
		insert accountAddressJn;


		//Opportunity__c
		Date today = Date.today();
		SmartFactory.IncludedFields.put('Opportunity', new Set<String> { 'Sales_Region__c' });
		opportunity = (Opportunity)SmartFactory.createSObject('Opportunity',false);
		opportunity.Name = 'test' + Math.random()*1000;
		opportunity.Amount = 1500;
		opportunity.Date_Instructed__c = today;
		opportunity.CloseDate = today+30;
		opportunity.Source_Of_Instruction__c = 'Client - new/own initiative';
		opportunity.Manager__c = staff.Id;
		opportunity.Department__c = 'International Properties';
		opportunity.Office__c = 'Amsterdam';
		opportunity.Work_Type__c = 'worktype';

		opportunity.AccountId = account.Id;
		opportunity.Instrcting_Contact_name__c = contact.Id;
		opportunity.InstructingCompanyAddress__c = address.Id;
		opportunity.Instructing_Company_Role__c = 'Landlord';
		opportunity.Instructing_Contact__c = contact.Id;

		opportunity.Engagement_Letter__c = true;
		opportunity.Engagement_Letter_Sent_Date__c = Date.today();
		opportunity.Property_Area__c = 1000;
		opportunity.Property_Area_UOM__c = 'sqm';
		insert opportunity;

		//Care_Of__c
		careOf = new Care_Of__c();
		careOf= (Care_Of__c)SmartFactory.createSObject('Care_Of__c', false);
		careOf.Client__c = account.id;
		careOf.Contact__c = contact.id;
		careOf.Colliers_Int__c = true;
		insert careOf;


		//Allocation__c
		allocation = (Allocation__c)SmartFactory.createSObject('Allocation__c',false);
		allocation.job__c= opportunity.id;
		allocation.Main_Allocation__c = true;
		allocation.Department_Allocation__c = 'International Properties';
		allocation.Assigned_To__c=staff.id;
		allocation.complete__c = false;
		insert allocation;

		//not creating purchase order
		//Invoice__c
		invoice = new Invoice__c();
		invoice = (Invoice__c)SmartFactory.createSObject('Invoice__c',false);
		invoice.Opportunity__c= opportunity.id;
		invoice.contact__c=contact.id;
		invoice.is_International__c=true;
		invoice.status__c ='Draft';
		invoice.Invoice_Wording__c = 'testWord';
		invoice.Invoice_Currency__c = 'EUR';
		insert invoice;


		//Invoice_Line_Item__c
		invoiceLineItems = new List<Invoice_Line_Item__c>();
		Invoice_Line_Item__c invoiceLineItem1 = new Invoice_Line_Item__c();
		invoiceLineItem1 = (Invoice_Line_Item__c)SmartFactory.createSObject('Invoice_Line_Item__c',false);
		invoiceLineItem1.Invoice__c = invoice.id;
		invoiceLineItem1.type__c = 'Fee';
		invoiceLineItem1.Amount_ex_VAT__c = 1000;
		invoiceLineItem1.VAT_Amount__c = 200;
		invoiceLineItem1.International_Amount__c = 1000;
		invoiceLineItems.add(invoiceLineItem1);

		Invoice_Line_Item__c invoiceLineItem2 = new Invoice_Line_Item__c();
		invoiceLineItem2 = (Invoice_Line_Item__c)SmartFactory.createSObject('Invoice_Line_Item__c',false);
		invoiceLineItem2.Invoice__c = invoice.id;
		invoiceLineItem2.type__c = 'Cost';
		invoiceLineItem2.Amount_ex_VAT__c = 500;
		invoiceLineItem2.VAT_Amount__c = 100;
		invoiceLineItem2.International_Amount__c = 500;
		invoiceLineItems.add(invoiceLineItem2);

		insert invoiceLineItems;

		//Cost__c
		cost1 = new Disbursements__c();
		cost1 = (Disbursements__c)SmartFactory.createSObject('Disbursements__c',false);
		cost1.Job__c = opportunity.id;
		cost1.InvoiceId__c = invoice.Id;
		cost1.Purchase_Cost__c = 1000;
		cost1.Recharge_Cost__c = 1000;
		cost1.Category__c ='Cost Flow Through (Residential only)';
		cost1.Created_For__c = staff.id;
		cost1.Sub_Category__c='Cost Flow Through (RESIDENTIAL ONLY) - With VAT';
		cost1.Purchase_Date__c  = System.today();
		cost1.Description__c = 'Tsts';
		cost1.Invoice_Details__c = 'Tsts';
		insert cost1;


		//Forecasting__c
		forecasting = new Forecasting__c();
		forecasting = (Forecasting__c)SmartFactory.createSObject('Forecasting__c',false);
		forecasting.Allocation__c =allocation.id;
		forecasting.CS_Forecast_Date__c = system.today().toStartOfMonth();
		//amount must be equal to Invoice line items total
		forecasting.Amount__c=1500.0;
		insert forecasting;

		//Invoice Cost Junction
		invoiceCostJn = new Invoice_Cost_Junction__c();
		invoiceCostJn = (Invoice_Cost_Junction__c)SmartFactory.createSObject('Invoice_Cost_Junction__c',false);
		invoiceCostJn.Disbursement__c = cost1.id;
		invoiceCostJn.Invoice__c = invoice.id;
		insert invoiceCostJn;

		//Invoice Allocation Jn
		invoiceAllocationJn = new Invoice_Allocation_Junction__c();
		invoiceAllocationJn = (Invoice_Allocation_Junction__c)SmartFactory.createSObject('Invoice_Allocation_Junction__c',false);
		invoiceAllocationJn.Allocation__c= allocation.id;
		invoiceAllocationJn.Invoice__c=invoice.id;
		invoiceAllocationJn.Forecasting__c = forecasting.id;
		insert invoiceAllocationJn;

		invoicePdf = new Invoice_PDF_Value__c();
		invoicePdf.Company__c = account.Id;
		invoicePdf.Currency__c = 'EUR';
		invoicePdf.Department_Code__c = '620';
		invoicePdf.Remittance__c = 'ACCOUNTS DEPARTMENT,'+
			'COLLIERS INTERNATIONAL CAPITAL MARKETS UK LLP,'+
			'1ST FLOOR,'+
			'50 GEORGE STREET,'+
			'LONDON,'+
			'W1U 7GA,'+
			'CCUK@COLLIERS.COM';
		invoicePdf.External_Id_JM_UK__c = '20';
		invoicePdf.Company_Name__c = 'COLLIERS INTERNATIONAL CAPITAL MARKETS UK LLP';
		invoicePdf.Payment_Footer__c = 'Account Name: Colliers International Property Consultants Ltd<br/>Bank: HSBC Bank Plc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sort Code: 40-05-30&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account No: 84299388&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IBAN NO: GB67MIDL40053084299388&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Swift Code (BIC): MIDLGB22';
		invoicePdf.Footer__c = 'Colliers International is the licensed trading name of Colliers International Capital Markets UK LLP.'+
			'Company Registered in England no. OC392075'+
			'Registered Office: 50 George Street, London W1U 7GA For any queries please contact UK Corporate Credit Control on +44 207 344 6944';
		invoicePdf.Dept_Cost_Centre__c = '620';
		insert invoicePdf;
		System.assertNotEquals(null,invoicePdf.Id);
	}
	@isTest
	private static void testD0450_InvoicePdf(){
		createDataForD0450();

		pageReference pr = Page.CS_ManageMyJobPage;
		pr.getParameters().put('id',opportunity.id);
		Test.setCurrentPage(pr);

		Test.startTest();
		cs_companiesContactsController controller1 = new cs_companiesContactsController();
		controller1.changeInv = 'YES';
		controller1.jobDetails.Instructing_Contact__c = contact.Id;
		controller1.renderInvoiceDetails();
		controller1.save();

		Invoice_Allocation_Junction__c invAllocJn = [Select Id From Invoice_Allocation_Junction__c Where Invoice__c =: invoice.Id AND Allocation__r.Main_Allocation__c = TRUE];
		System.assertNotEquals(null, invAllocJn);

		Opportunity opp = [Select Id, StageName From Opportunity Where Id =: opportunity.Id];
		System.assertNotEquals(null,opp.StageName);

		pr = Page.CS_JobInvoiceViewClone;
		pr.getParameters().put('id', opportunity.Id);
		CS_JobInvoiceController controller2 = new CS_JobInvoiceController();
		controller2.refreshAll();
		controller2.initInvoice();
		controller2.selectedId = invoice.Id;
		controller2.selectedInvoiceDetails();
		System.assertEquals(true, controller2.showPDFTab);

		// look for cost of 'Fee' & 'Cost' invoice line item above
		//pdfAmountTotal should match sum of all invoice line items
		controller2.feesList[0].PDF_Amount__c = 1300;
		controller2.feesList[0].PDF_VAT__c = 0;
		controller2.costsList[0].PDF_Amount__c = 200;
		controller2.costsList[0].PDF_VAT__c = 0;
		controller2.pdfAmountTotal = controller2.pdfInvoice.Total_PDF_Amount__c;

		controller2.saveAll();
		controller2.updatePDFValues();
		System.assertEquals(TRUE,controller2.pdfTabSuccess);

		pr = new PageReference('/apex/CS_InvoicePDFView?Copy=DRAFT&invid=' + invoice.Id);
		Test.setCurrentPage(pr);
		CS_InvoicePdfController controller3 = new CS_InvoicePdfController();

		Invoice_Line_Item__c feeLI = new Invoice_Line_Item__c();
		feeLI.Id = invoiceLineItems[0].Id;
		feeLI.PDF_Amount__c = 0;
		update feeLI;
		controller3 = new CS_InvoicePdfController();

		Test.stopTest();
	}

	@isTest
	private static void testD0450_CreditNotesPdf(){
		createDataForD0450();

		//for creating credit notes
		invoice.Assigned_Invoice_Number__c = '0111134556678';
		invoice.Status__c = 'Cancelled';
		invoice.Final_Invoice__c = TRUE;
		update invoice;

		Credit_Note__c creditNote = new Credit_Note__c();
		creditNote.Status__c = 'Approved';
		creditNote.Credit_Note_Date__c = Date.today();
		creditNote.Invoice__c = invoice.Id;
		creditNote.Approved_By__c = staff.Name;
		creditNote.Approved_Date__c = Date.today();
		//picklist
		creditNote.CN_Reason__c = 'Amend billing amount';
		insert creditNote;

		pageReference pr = Page.CS_ManageMyJobPage;
		pr.getParameters().put('id',opportunity.id);
		Test.setCurrentPage(pr);

		Test.startTest();
		cs_companiesContactsController controller1 = new cs_companiesContactsController();
		controller1.changeInv = 'YES';
		controller1.jobDetails.Instructing_Contact__c = contact.Id;
		controller1.renderInvoiceDetails();
		controller1.save();

		Invoice_Allocation_Junction__c invAllocJn = [Select Id From Invoice_Allocation_Junction__c Where Invoice__c =: invoice.Id AND Allocation__r.Main_Allocation__c = TRUE];
		System.assertNotEquals(null, invAllocJn);

		Opportunity opp = [Select Id, StageName From Opportunity Where Id =: opportunity.Id];
		System.assertNotEquals(null,opp.StageName);

		pr = Page.CS_JobInvoiceViewClone;
		pr.getParameters().put('id', opportunity.Id);
		CS_JobInvoiceController controller2 = new CS_JobInvoiceController();
		controller2.refreshAll();
		controller2.initInvoice();
		controller2.selectedId = invoice.Id;
		controller2.selectedInvoiceDetails();
		System.assertEquals(true, controller2.showPDFTab);

		// look for cost of 'Fee' & 'Cost' invoice line item above
		//pdfAmountTotal should match sum of all invoice line items
		controller2.feesList[0].PDF_Amount__c = 1300;
		controller2.feesList[0].PDF_VAT__c = 0;
		controller2.costsList[0].PDF_Amount__c = 200;
		controller2.costsList[0].PDF_VAT__c = 0;
		controller2.pdfAmountTotal = controller2.pdfInvoice.Total_PDF_Amount__c;

		controller2.saveAll();
		controller2.updatePDFValues();
		System.assertEquals(TRUE,controller2.pdfTabSuccess);

		pr = new PageReference('/apex/cs_creditnodepdfview?id=' + creditNote.Id + '&invid=' + invoice.Id);
		Test.setCurrentPage(pr);
		ApexPages.StandardController sc = new ApexPages.StandardController(creditNote);
		CS_CreditNotePdfController controller3 = new CS_CreditNotePdfController(sc);

		Invoice_Line_Item__c feeLI = new Invoice_Line_Item__c();
		feeLI.Id = invoiceLineItems[0].Id;
		feeLI.PDF_Amount__c = 0;
		update feeLI;
		controller3 = new CS_CreditNotePdfController(sc);

		Test.stopTest();
	}

}