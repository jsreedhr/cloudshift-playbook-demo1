public with sharing class CS_Property_Trigger_Handler {
// This should be used in conjunction with the ApexTriggerComprehensive.trigger template
// The origin of this pattern is http://www.embracingthecloud.com/2010/07/08/ASimpleTriggerTemplateForSalesforce.aspx
    public CS_Property_Trigger_Handler(){

    }

    public void OnBeforeInsert(Property__c[] newRecords){
        for(Property__c newRecord : newRecords){
            if(newRecord.Property_Type__c == null && String.isBlank(newRecord.Property_Type__c)){
                newRecord.Property_Type__c = 'Mixed Use';
            } else {
                newRecord.Property_Type__c = newRecord.Property_Type__c+';'+'Mixed Use';
            }
        }
    }

}