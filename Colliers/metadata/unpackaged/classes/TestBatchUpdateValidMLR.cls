@isTest 
public class TestBatchUpdateValidMLR 
{
    static testMethod void UpdateValidMLR(){ 
        MLR__c mlr = new MLR__c();
		mlr.Status__c = 'Pass';
		mlr.MLR_Set_Date__c = system.today();
		mlr.Expiry_Date__c =system.today().addDays(3);
		insert mlr;
		
        MLR__c mlr1 = new MLR__c();		
		mlr1.Status__c = 'Expired';
		mlr1.MLR_Set_Date__c = system.today();
		mlr1.Expiry_Date__c = system.today().addDays(4);
		insert mlr1;
		
		Account acc1 = new Account();
        acc1.name = 'testaccount1';
		acc1.MLR_Id__c = mlr.id;
		insert acc1;
		
		Account acc2 = new Account();
        acc2.name = 'testaccount2';
		acc2.MLR_Id__c = mlr1.id;
		insert acc2;
		
		Care_Of__c care1 = new Care_Of__c();
		care1.Name='test care of';
		care1.Reporting_Client__c = acc2.id;
        care1.Colliers_Int__c = true;
		insert care1;
		
        Opportunity opp1= new Opportunity();
		opp1.Name= 'test opp1';
		opp1.Invoicing_Company2__c = acc1.id;
		opp1.StageName ='Instructed';
		opp1.CloseDate = system.today().addDays(8);
		
		insert opp1;
		
		Opportunity opp2= new Opportunity();
		opp2.Name= 'test opp2';
		opp2.Invoicing_Care_Of__c = care1.id;
		opp2.StageName ='Instructed';
		opp2.CloseDate = system.today().addDays(7);
		insert opp2;
    
    
        
		
		
		
        Test.startTest();

            BatchUpdateValidMLR obj = new BatchUpdateValidMLR();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
}