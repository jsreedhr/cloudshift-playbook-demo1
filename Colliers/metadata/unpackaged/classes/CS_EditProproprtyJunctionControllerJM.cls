public with sharing class CS_EditProproprtyJunctionControllerJM {

    
    Public String Ids{get;set;}
    Public List<Property__c> ListProperty;
    public boolean isClosedJob{get;set;}
    Public List<booleanProproprtyWrapper> ListbooleanProproprtyWrapper{get;set;}
    Public List<booleanProproprtyJnWrapper> ListbooleanProproprtyJnWrapper{get;set;}
    Public List<Company_Property_Affiliation__c> CompnyPrptyJnToDel;
    public Map<id,Property__c> MapIdProprty;
    Public string IdtoDelete{get;set;}
    public List<Job_Property_Junction__c> JobPrptyJn;
    Public string keyPrefix{get;set;}
    Public Boolean isError{get;set;}
    Public string ErrorMessage{get;set;}
    Public List<Company_Property_Affiliation__c> CompnyPrptyJn;
     Public Set<Id> PrptyId;
     public List<Opportunity> lstOpp{get;set;}
     public set<id> idSet;
     Public List<id> idList;
     Public string BldingFilter{get;set;}
     //public string recordId{get;set;}
     Public string TownFilter{get;set;}
     Public string StreetFilter{get;set;}
     Public string PostalFilter{get;set;}
     Public boolean Tofilter{get;set;}
     Public Boolean IsSelectAll{get;set;}
     Public Boolean IsQueryAll;
     Public Boolean IsInitialised{get;set;}
     public Boolean IsWithoutSearch{get;set;}

    // instantiate the StandardSetController from a query locator
   
    public CS_EditProproprtyJunctionControllerJM(){
        //setClosedJob();
        getPropertyDetails();
        // ListbooleanProproprtyWrapper = new List<booleanProproprtyWrapper>(); 
        // CompnyPrptyJnToDel = new List<Company_Property_Affiliation__c>(); 
        // ListbooleanProproprtyJnWrapper= new List<booleanProproprtyJnWrapper>(); 
        // CompnyPrptyJn= new List<Company_Property_Affiliation__c>(); 
        // JobPrptyJn= new List<Job_Property_Junction__c>();
        // idSet= new set<id>();
    }
    
    
    public void getPropertyDetails(){
        IsInitialised=false;
        IsWithoutSearch=true;
        idList= new List<id>();
        ListbooleanProproprtyWrapper = new List<booleanProproprtyWrapper>(); 
        CompnyPrptyJnToDel = new List<Company_Property_Affiliation__c>(); 
        ListbooleanProproprtyJnWrapper= new List<booleanProproprtyJnWrapper>(); 
        CompnyPrptyJn= new List<Company_Property_Affiliation__c>(); 
        JobPrptyJn= new List<Job_Property_Junction__c>();
        idSet= new set<id>();
        String recordId ='';
        if(ApexPages.currentPage().getParameters().containsKey('id')){
            recordId = ApexPages.currentPage().getParameters().get('id');
            if(!String.isBlank(recordId)){
                lstOpp = new List<Opportunity>();
                lstOpp = [select Id, AccountId,isClosed,Account.Name, Name,Relates_To__c,StageName,Amount,Manager__c,Manager__r.Name,Coding_Structure__r.Status__c,Job_Number__c From Opportunity where id =:recordId limit 1];
                if(!lstOpp.isEmpty()){
                    
                    Ids = lstOpp[0].id;
                    isClosedJob = lstOpp[0].isClosed;
                    JobPrptyJn=[select id ,Property__c,Property__r.id ,Property__r.Building_Name__c,Property__r.Street__c, Property__r.Full_Street__c,Property__r.Post_Code__c, Property__r.Town__c from Job_Property_Junction__c where Job__c=:lstOpp[0].id Limit 49999];
                    for(Job_Property_Junction__c CompntJn:JobPrptyJn){
                        ListbooleanProproprtyJnWrapper.add(new booleanProproprtyJnWrapper(CompntJn.Property__r.id,CompntJn.Property__r.Building_Name__c,CompntJn.Property__r.Post_Code__c,CompntJn.Property__r.Full_Street__c,CompntJn.Property__r.Town__c, CompntJn.id));
                        idSet.add(CompntJn.Property__r.id); 
                    }
                    idList.addAll(idSet);
                }
                
            }
        }
            
    }
    public void setClosedJob(){
        String recordId = '';
        isClosedJob = false;
        if(ApexPages.currentPage().getParameters().containsKey('id')){
            recordId = ApexPages.currentPage().getParameters().get('id');
            if(!String.isBlank(recordId)){
                List<Opportunity> lstOpp = new List<Opportunity>();
                lstOpp = [Select id, name, isClosed From Opportunity where id =:recordId limit 1];
                if(!lstOpp.isEmpty()){
                    isClosedJob = lstOpp[0].isClosed;
                }
            }
            
        }
        
    }
    
    public ApexPages.StandardSetController cons {
        
        
        get {
            system.debug('------Testcons' );
             if (cons == null) {
                if(IsQueryAll==null){
                    IsQueryAll=false;
                }
            
            
            
                Tofilter=true;
                String queryStr='';
                String tempFilter1='';
                String tempFilter2='';
                String tempFilter3='';
                String tempFilter4='';

                queryStr='select id , name,Building_Name__c,Street__c, Full_Street__c, Post_Code__c,Town__c from Property__c where id != null ';
               
                if(PostalFilter!=null && PostalFilter!='')
                {
                     tempFilter1=PostalFilter;
                   queryStr+= 'and( Post_Code__c like  '+'\'%'+tempFilter1+'%\' or Post_Code_Formula__c like  '+'\'%'+tempFilter1+'%\')';
                    system.debug('--'+tempFilter1);
                } 
                if(BldingFilter!=null && BldingFilter!='')
                {
                    tempFilter2=BldingFilter;
                   queryStr+= 'and Building_Name__c like '+'\'%'+tempFilter2+'%\'';
                    system.debug('--'+tempFilter2);
                }
                 if(TownFilter!=null && TownFilter!='')
                {
                    tempFilter3=TownFilter;
                  queryStr+= ' and Town__c like '+'\'%'+tempFilter3+'%\'';
                  system.debug('--'+tempFilter3);
                }
                  if(StreetFilter!=null && StreetFilter!='')
                {
                    tempFilter4=StreetFilter;
                   queryStr+= ' and Full_Street__c like '+'\'%'+tempFilter4+'%\'';
                    system.debug('--'+tempFilter4);
                }
                if(!idList.isEmpty()){
                    queryStr+=  ' and Id Not IN :idList';
                }
                queryStr+=  ' Limit 100 '; 
                system.debug('query!!'+queryStr);
                cons = new ApexPages.StandardSetController(Database.query(queryStr));
                system.debug('------result' + cons);
                system.debug('555555');
                if(IsQueryAll== false) {
                    system.debug('66666');
                    cons.setPageSize(15);
                }
            }

            return cons;
        }
        set;
    }
    
    
    
    
    
    public void getCategorieskeypreix() {
        IsInitialised=false;
        IsWithoutSearch=true;
         idList= new List<id>();
         Ids = apexpages.currentpage().getparameters().get('id');
         if(!String.isBlank(Ids)){
           keyPrefix = String.valueOf(Ids).substring(0,3);
            ListbooleanProproprtyWrapper = new List<booleanProproprtyWrapper>(); 
            CompnyPrptyJnToDel = new List<Company_Property_Affiliation__c>(); 
            ListbooleanProproprtyJnWrapper= new List<booleanProproprtyJnWrapper>(); 
            CompnyPrptyJn= new List<Company_Property_Affiliation__c>(); 
            JobPrptyJn= new List<Job_Property_Junction__c>();
              idSet= new set<id>();
            if(keyPrefix=='001'){
                CompnyPrptyJn=[select id ,Property__c,Property__r.id ,Property__r.Building_Name__c,Property__r.Street__c, Property__r.Full_Street__c, Property__r.Post_Code__c, Property__r.Town__c from Company_Property_Affiliation__c where Company__c=:Ids Limit 49999];
                for(Company_Property_Affiliation__c CompntJn:CompnyPrptyJn){
                    ListbooleanProproprtyJnWrapper.add(new booleanProproprtyJnWrapper(CompntJn.Property__r.id,CompntJn.Property__r.Building_Name__c,CompntJn.Property__r.Post_Code__c,CompntJn.Property__r.Full_Street__c,CompntJn.Property__r.Town__c, CompntJn.id));
                    idSet.add(CompntJn.Property__r.id);
                    
                }
            }
            if(keyPrefix=='006'){
                JobPrptyJn=[select id ,Property__c,Property__r.id ,Property__r.Building_Name__c,Property__r.Street__c, Property__r.Full_Street__c,Property__r.Post_Code__c, Property__r.Town__c from Job_Property_Junction__c where Job__c=:Ids Limit 49999];
                for(Job_Property_Junction__c CompntJn:JobPrptyJn){
                    ListbooleanProproprtyJnWrapper.add(new booleanProproprtyJnWrapper(CompntJn.Property__r.id,CompntJn.Property__r.Building_Name__c,CompntJn.Property__r.Post_Code__c,CompntJn.Property__r.Full_Street__c,CompntJn.Property__r.Town__c, CompntJn.id));
                     idSet.add(CompntJn.Property__r.id); 
                }
            }
            idList.addAll(idSet);
         }
    }
    
    
    
    

    // returns a list of wrapper objects for the sObjects in the current page set
    public void getCategories() {
        isError=false;
        IsInitialised=true;
        PrptyId=new Set<Id> ();
       
       //String keyPrefix = '001';
        
         

        ListbooleanProproprtyWrapper = new List<booleanProproprtyWrapper>(); 
       
        //ListbooleanProproprtyJnWrapper= new List<booleanProproprtyJnWrapper>(); 
        
        CompnyPrptyJn= new List<Company_Property_Affiliation__c>(); 
        JobPrptyJn= new List<Job_Property_Junction__c>(); 
        
        MapIdProprty= new Map<id,Property__c>(); 
        //ListProperty= [select id , name,Building_Name__c,Street__c,Post_Code__c,Town__c from Property__c];
        
        for(Property__c prpty:(List<Property__c>)cons.getRecords()){
            //ListbooleanProproprtyWrapper.add(new booleanProproprtyWrapper(false,prpty));
            MapIdProprty.put(prpty.id,prpty);
        }
         for(Property__c prpty:(List<Property__c>)cons.getRecords()){
              if(IsSelectAll==true){
                ListbooleanProproprtyWrapper.add(new booleanProproprtyWrapper(True,prpty));
            }
            else{
            if(idSet.contains(prpty.id)){ 
                ListbooleanProproprtyWrapper.add(new booleanProproprtyWrapper(False,prpty));
            }
            else{
                 ListbooleanProproprtyWrapper.add(new booleanProproprtyWrapper(false,prpty));
            }
            }
        }
    }
    
    
    
    
    
    public void cancelJobs(){
        
        List<booleanProproprtyJnWrapper> ListbooleanProproprtyJnWrapperToCancel = new List<booleanProproprtyJnWrapper>();
        if(keyPrefix=='001'){
            for(booleanProproprtyJnWrapper CmpnyJn :ListbooleanProproprtyJnWrapper){
                if(!String.isBlank(CmpnyJn.JnID)){
                    ListbooleanProproprtyJnWrapperToCancel.add(CmpnyJn);
                }
            }
        }else if(keyPrefix=='006'){
             
            for(booleanProproprtyJnWrapper CmpnyJn :ListbooleanProproprtyJnWrapper){
           
                if(!String.isBlank(CmpnyJn.JnID)){
                    ListbooleanProproprtyJnWrapperToCancel.add(CmpnyJn);
                }
            }
        }
        ListbooleanProproprtyJnWrapper = new List<booleanProproprtyJnWrapper>();
        ListbooleanProproprtyJnWrapper.addAll(ListbooleanProproprtyJnWrapperToCancel);
    }
    
    
    
    public void getNewCategories() {
        isError=false;
      idSet= new set<id>();
     idList= new List<id>();
         For(booleanProproprtyJnWrapper JnWrp:ListbooleanProproprtyJnWrapper){
            idSet.add(JnWrp.Idss);
            
        }
         idList.addAll(idSet);

        ListbooleanProproprtyWrapper = new List<booleanProproprtyWrapper>(); 
       
     
        MapIdProprty= new Map<id,Property__c>(); 
       
        
        for(Property__c prpty:(List<Property__c>)cons.getRecords()){
            //ListbooleanProproprtyWrapper.add(new booleanProproprtyWrapper(false,prpty));
            MapIdProprty.put(prpty.id,prpty);
        }
     
         for(Property__c prpty:(List<Property__c>)cons.getRecords()){
            system.debug('-----idsss'+prpty.id);
             if(IsSelectAll==true){
                ListbooleanProproprtyWrapper.add(new booleanProproprtyWrapper(True,prpty));
            }
            else{
           
            if(idSet.contains(prpty.id)){ 
                
            ListbooleanProproprtyWrapper.add(new booleanProproprtyWrapper(True,prpty));
            }
            else{
                 ListbooleanProproprtyWrapper.add(new booleanProproprtyWrapper(false,prpty));
            }
            }
            
        }
    }
    
    
        Public Void SelectedProperty(){
             List<booleanProproprtyJnWrapper> SaveRecordsList = new List<booleanProproprtyJnWrapper>();
            if(IsSelectAll==true){
                system.debug('11111');
                IsQueryAll=true;
                cons = null;
                system.debug('----size444'+ListbooleanProproprtyJnWrapper.size());
                cons.first();
                system.debug('----size555'+ListbooleanProproprtyJnWrapper.size());
                getCategories();
                system.debug('----size666'+ListbooleanProproprtyJnWrapper.size());
               
                for(booleanProproprtyWrapper PrpWrp:ListbooleanProproprtyWrapper){
            
                if(PrptyId.contains(PrpWrp.Prprty.id)){
                    
                }
                else{
                     ListbooleanProproprtyJnWrapper.add(new booleanProproprtyJnWrapper(MapIdProprty.get(PrpWrp.Prprty.id).id,MapIdProprty.get(PrpWrp.Prprty.id).Building_Name__c,MapIdProprty.get(PrpWrp.Prprty.id).Post_Code__c,MapIdProprty.get(PrpWrp.Prprty.id).Full_Street__c,MapIdProprty.get(PrpWrp.Prprty.id).Town__c));
                    SaveRecordsList.add(new booleanProproprtyJnWrapper(MapIdProprty.get(PrpWrp.Prprty.id).id,MapIdProprty.get(PrpWrp.Prprty.id).Building_Name__c,MapIdProprty.get(PrpWrp.Prprty.id).Post_Code__c,MapIdProprty.get(PrpWrp.Prprty.id).Full_Street__c,MapIdProprty.get(PrpWrp.Prprty.id).Town__c));
            }
                        
                        
                }
                
             }
    
        system.debug('444444');
        PrptyId=new Set<Id> ();
        system.debug('----size777'+ListbooleanProproprtyJnWrapper.size());
        For(booleanProproprtyJnWrapper JnWrp:ListbooleanProproprtyJnWrapper){
            PrptyId.add(JnWrp.Idss);
            
        }
        for(booleanProproprtyWrapper PrpWrp:ListbooleanProproprtyWrapper){
            if(PrpWrp.isSelected== true){
                if(PrptyId.contains(PrpWrp.Prprty.id)){
                    
                }
                else{
                     ListbooleanProproprtyJnWrapper.add(new booleanProproprtyJnWrapper(MapIdProprty.get(PrpWrp.Prprty.id).id,MapIdProprty.get(PrpWrp.Prprty.id).Building_Name__c,MapIdProprty.get(PrpWrp.Prprty.id).Post_Code__c,MapIdProprty.get(PrpWrp.Prprty.id).Full_Street__c,MapIdProprty.get(PrpWrp.Prprty.id).Town__c));
                        SaveRecordsList.add(new  booleanProproprtyJnWrapper(MapIdProprty.get(PrpWrp.Prprty.id).id,MapIdProprty.get(PrpWrp.Prprty.id).Building_Name__c,MapIdProprty.get(PrpWrp.Prprty.id).Post_Code__c,MapIdProprty.get(PrpWrp.Prprty.id).Full_Street__c,MapIdProprty.get(PrpWrp.Prprty.id).Town__c));
            }
            
            
        }
         
        
        
        
    }
    IsQueryAll=false;
    idSet= new set<id>();
        idList= new List<id>();
         system.debug('----size'+ListbooleanProproprtyJnWrapper.size());
         For(booleanProproprtyJnWrapper JnWrp:ListbooleanProproprtyJnWrapper){
            idSet.add(JnWrp.Idss);
            
        }
        idList.addAll(idSet);
        integer pgeno=1;
         if(pageNumber != 1){
             pgeno=pageNumber;
         }
        firstfilter();
         system.debug('----size111'+ListbooleanProproprtyJnWrapper.size());
         system.debug('-----pageno------'+pageNumber);
         
       
        IsSelectAll=false;
        
        SaveRecords(keyprefix, SaveRecordsList);
        }
        
        
    Public void selectAllProperty(){
        IsSelectAll=true;
        getCategories();
        for(booleanProproprtyWrapper PrpWrp:ListbooleanProproprtyWrapper){
            if(PrpWrp.isSelected== true){
                
                PrpWrp.isSelected = true;
            
            }
            else{
                PrpWrp.isSelected = False;
            }
         
        }
        
    }
    
    
    Public void deleteSelectedRow(){
        
        try{
            IsSelectAll=false;
            system.debug('------idno'+IdtoDelete);
            system.debug('------id'+ListbooleanProproprtyJnWrapper[Integer.valueof(IdtoDelete)].Idss);
            /* for(booleanProproprtyWrapper PrpWrp:ListbooleanProproprtyWrapper){
                 if(PrpWrp.Prprty.id==ListbooleanProproprtyJnWrapper[Integer.valueof(IdtoDelete)].Idss){
                     PrpWrp.isSelected=false;
                 }
             }*/
            
            //ListbooleanProproprtyWrapper.add(new booleanProproprtyWrapper(false,MapIdProprty.get(ListbooleanProproprtyJnWrapper[Integer.valueof(IdtoDelete)].Idss)));
            String Deleteid = ListbooleanProproprtyJnWrapper.get(Integer.valueof(IdtoDelete)).idss;
            //booleanProproprtyJnWrapper dellist=ListbooleanProproprtyJnWrapper.get(Integer.valueof(IdtoDelete));
            ListbooleanProproprtyJnWrapper.remove(Integer.valueof(IdtoDelete));
            system.debug('---------------------'+Deleteid);
            idSet= new set<id>();
            idList= new List<id>();
         
             For(booleanProproprtyJnWrapper JnWrp:ListbooleanProproprtyJnWrapper){
                idSet.add(JnWrp.Idss);
                
            }
            idList.addAll(idSet);
             if(IsWithoutSearch==false){
            integer pgeno=1;
             if(pageNumber != 1){
                 pgeno=pageNumber;
             }
            firstfilter();
             system.debug('-----pageno------'+pageNumber);
             
            for(Integer i=1; i<pgeno;i++){
                system.debug('-----pageno'+pageNumber);
                next();
                
            }
                
            }
            System.debug('----CompnyPrptyJn----'+CompnyPrptyJn);
            CompnyPrptyJn = new List<Company_Property_Affiliation__c>(); 
            CompnyPrptyJn=[select id ,Property__c,Property__r.id ,Property__r.Building_Name__c,Property__r.Street__c,Property__r.Post_Code__c, Property__r.Town__c from Company_Property_Affiliation__c where Property__c=:Deleteid and Company__c=:Ids Limit 999];
            if(!CompnyPrptyJn.isEmpty()){
                Delete CompnyPrptyJn;
            }
            JobPrptyJn= new List<Job_Property_Junction__c>();
            JobPrptyJn=[select id ,Property__c,Property__r.id ,Property__r.Building_Name__c,Property__r.Street__c,Property__r.Post_Code__c, Property__r.Town__c from Job_Property_Junction__c where Job__c=:Ids AND Property__c=:Deleteid Limit 999];
            
            if(!JobPrptyJn.isEmpty()){
                Delete JobPrptyJn;
            }
            //ListbooleanProproprtyWrapper.add()
        }catch(Exception e ){
            isError=true;
            ErrorMessage='Unexpected Error: Please contact Adminsitrator';
        }
    }
    
    
    
    // Logic to save all records
     Public void SaveRecords(String keyPrefix,List<booleanProproprtyJnWrapper> ListbooleanProproprtyJnWrapper){
         try{
             isError=false;
        
        
       // List<Company_Property_Affiliation__c> CompnyPrptyJnToDelonSave = new List<Company_Property_Affiliation__c>();
        //List<Job_Property_Junction__c> JobPrptyJnToDelonSave = new List<Job_Property_Junction__c>();
        //ListbooleanProproprtyJnWrapper.remove(Integer.valueof(IdtoDelete));
        // if(keyPrefix=='001'){
        // List<Company_Property_Affiliation__c> ToUpdateProptyAccJn= new List<Company_Property_Affiliation__c>();
        // for(booleanProproprtyJnWrapper CmpnyJn :ListbooleanProproprtyJnWrapper){
            
            // Company_Property_Affiliation__c CpnyJn= new Company_Property_Affiliation__c();
            // CpnyJn.Company__c=Ids;
            // CpnyJn.Property__c=CmpnyJn.Idss;
            // ToUpdateProptyAccJn.add(CpnyJn);
            
            // if(!String.isBlank(CmpnyJn.JnID)){
                // Company_Property_Affiliation__c objCompanyProperty = new Company_Property_Affiliation__c();
                // objCompanyProperty.id = CmpnyJn.JnID;
                // CompnyPrptyJnToDelonSave.add(objCompanyProperty);            
            // }
            
            // }
            // if(!CompnyPrptyJnToDelonSave.isEmpty()){
                // delete CompnyPrptyJnToDelonSave;
            // }
            // Insert ToUpdateProptyAccJn;
        
            
        // }
        // else if(keyPrefix=='006'){
             List<Job_Property_Junction__c> ToUpdateJobProptyAccJn= new List<Job_Property_Junction__c>();
            for(booleanProproprtyJnWrapper CmpnyJn :ListbooleanProproprtyJnWrapper){
               
                Job_Property_Junction__c CpnyJn= new Job_Property_Junction__c();
                CpnyJn.Job__c=Ids;
                CpnyJn.Property__c=CmpnyJn.Idss;
                ToUpdateJobProptyAccJn.add(CpnyJn);
                // if(!String.isBlank(CmpnyJn.JnID)){
                    // Job_Property_Junction__c objJobProperty = new Job_Property_Junction__c();
                    // objJobProperty.id =  CmpnyJn.JnID;
                    // JobPrptyJnToDelonSave.add(objJobProperty);
                // }
            }
       
            Insert ToUpdateJobProptyAccJn;
        // }
         }
         catch(Exception e) {
            ApexPages.addMessages(e);
           isError=true;
            //return null;
        }
        //PageReference myVFPage = new PageReference('/'+Ids);
     // myVFPage.setRedirect(true);
  
      //return myVFPage;
         
    }
    

    // displays the selected items
    

    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            try{
                if(cons != null){
                    return cons.getHasNext();
                }else{
                    return false;
                }
            }catch(exception e){
                return false;
            }
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            try{
            if(cons != null){
                 return cons.getHasPrevious();
            }else{
                return false;
            }
            }catch(exception e){
                return false;
            }
           
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return cons.getPageNumber();
        }
        set;
    }

    // returns the first page of records
     public void first() {
         SelectedProperty();
         cons.first();
         
         getNewCategories();
     }
     
     public void firstfilter() {
         // try{
        IsWithoutSearch=false;
             cons=null;
             
         cons.first();
         if(IsInitialised==true){
             getNewCategories();
         }
         else{
          getCategories();
         }
         // }catch(exception e){
             
         // }
         
     }

     // returns the last page of records
     public void last() {
          // try{
         SelectedProperty();
         cons.last();
        
         getNewCategories();
          // }catch(exception e){
             
         // }
         
     }

     // returns the previous page of records
     public void previous() {
         SelectedProperty();
         if(cons.getHasPrevious()){
            cons.previous();
         }
         
         getNewCategories();
     }

     // returns the next page of records
     public void next() {
         system.debug('----nextcalled');
         SelectedProperty();
          cons.next();
         
         getNewCategories();
         
     }

     // returns the PageReference of the original page, if known, or the home page.
    /* public void cancel() {
         cons.cancel();
        // getCategories();
     }*/
     public class booleanProproprtyWrapper{
        public Boolean isSelected{get;set;}
        public Property__c Prprty{get;set;}
       
        
        public booleanProproprtyWrapper(boolean isSelect, Property__c Prprtys ){
          Prprty = Prprtys;
          isSelected= isSelect;
         
          
          
           
        }
    }
    
    public class booleanProproprtyJnWrapper{
        //public Boolean isSelected{get;set;}
        public id Idss{get;set;}
        Public string Building{get;set;}
        Public string Postal{get;set;}
        Public string Street{get;set;}
        Public string Town{get;set;}
        Public string JnID{get;set;}
       
        
        public booleanProproprtyJnWrapper(id idsss ,string Buildings, string Postals,string Streets,string Towns ){
          Building = Buildings;
          Postal=Postals;
          Town=Towns;
          Street=Streets;
          Idss=Idsss;
          JnID = '';
          
           
        }
        
         public booleanProproprtyJnWrapper(id idsss ,string Buildings, string Postals,string Streets,string Towns, String junctionId ){
          Building = Buildings;
          Postal=Postals;
          Town=Towns;
          Street=Streets;
          Idss=Idsss;
          JnID = junctionId;
          
           
        }
    }

}