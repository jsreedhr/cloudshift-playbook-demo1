/* Name : BatchSendConflictEmails
 * Created By : Jyoti H/28-11-2016
 * Description : Class to send the Conflict Reminder Emails daily
 */

global class BatchSendConflictEmails implements Database.Batchable<sObject>{

   public List<String> emailList;
   public List<String> reminderEmailList;
   
   public map<id,Conflict_Line_Item__c> conflictMap;
   public map<id,Staff__c> staffUsrMap;
   public set<Id> cId;
   public set<Id> conflictIds;
     
   List<Conflict__c> updateConflictList {get;set;}
  
   global Database.QueryLocator start(Database.BatchableContext BC){
       return Database.getQueryLocator([Select id,name,Conflict__c,Conflict__r.Conflict__c,Conflict__r.Job_1__r.Manager__r.User__c,Conflict__r.Job_1__c,Conflict__r.Job_2__c,Conflict__r.Job_1__r.Manager__c,Conflict__r.Job_2__r.Manager__c,Conflict__r.Job_1__r.Name,Conflict__r.Job_2__r.Name,Conflict__r.Job_1__r.Job_Manager_Email__c,Conflict__r.Job_2__r.Job_Manager_Email__c,Conflict__r.Reminder_Sent_Date__c,Conflict__r.Potential_Conflict_resolved__c,Distance__c,sameproperty__c from Conflict_Line_Item__c where (Distance__c<= 0.25 or sameproperty__c = true or Conflict__r.Conflict__c = 'Yes') AND Conflict__r.conflicMailSend__c = false AND Conflict__r.Potential_Conflict_resolved__c = false limit 10000]);
   } 
   global void execute(Database.BatchableContext BC, List<Conflict_Line_Item__c> scope){
        cId = new Set<Id>();
        conflictMap = new map<id,Conflict_Line_Item__c>();
        conflictIds = new Set<Id>();
        System.debug('LoggingLevel logLevel'+ scope.size());
        for(Conflict_Line_Item__c cf:scope){
            if(cf.Conflict__c != null && String.isNotBlank(cf.Conflict__c)){
               if(!cId.contains(cf.Conflict__c)){
                   cId.add(cf.Conflict__c);
                   conflictMap.put(cf.Conflict__c,cf);
               }
            }
        }     
        updateConflictList = new List<Conflict__c>();
        
        

        List<Conflict_Reminder_days__c> cfRem = Conflict_Reminder_days__c.getall().values();
        Integer remDays = cfRem[0].No_of_Days__c.intValue();
        remDays = remDays* -1;
        if(!conflictMap.values().isEmpty()){
            for(Conflict_Line_Item__c cf:conflictMap.values()){
                Conflict__c cfRec = new Conflict__c();
                if(cf.Conflict__r.Reminder_Sent_Date__c == null){
                    if(cId.contains(cf.Conflict__c)){
                        system.debug('remCId---->'+cId);
    
                        //conflictMap.put(cf.id,cf); 
                        //cId.add(cf.Id);
                        cfRec.Id = cf.Conflict__c;
                        cfRec.Reminder_Sent_Date__c = date.today();
                        cfRec.conflicMailSend__c = true;
                        cfRec.conflictMailsendDate__c = date.today();
                        updateConflictList.add(cfRec);
                    }
                }  
                     
            }
        }
   
        if(cId.size()>0){
            sendmail(cId);
        }
        if(updateConflictList.size()>0){
            update updateConflictList;
        }
 
   }

   global void finish(Database.BatchableContext BC){
        
   }

   
   public void sendmail(set<Id> remIdSet)
    {
        List<Messaging.SingleEmailMessage> sendEmailList = new List<Messaging.SingleEmailMessage>();
        
        //Prefetch email Template id
        EmailTemplate et = [SELECT id FROM EmailTemplate WHERE developerName = 'Conflict_Email_Template'];
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

        List<String> emailLst = new List<String>();
        for(Id i:remIdSet){
            string whoId = conflictMap.get(i).Conflict__r.Job_1__r.Manager__r.User__c ;
            string watId = i;
            String templateId =et.id ;
     
            Messaging.SingleEmailMessage msgDetails = Messaging.renderStoredEmailTemplate(templateId,whoId,watId);
            
            //String mailTextBody = msgDetails.getPlainTextBody();
            String mailHtmlBody = msgDetails.getHTMLBody();
            String mailSubject = msgDetails.getSubject();
            if(!string.isblank(conflictMap.get(i).Conflict__r.Job_1__r.Job_Manager_Email__c)){
            emailLst.add(conflictMap.get(i).Conflict__r.Job_1__r.Job_Manager_Email__c);
            }
            if(!string.isblank(conflictMap.get(i).Conflict__r.Job_2__r.Job_Manager_Email__c)){
            emailLst.add(conflictMap.get(i).Conflict__r.Job_2__r.Job_Manager_Email__c);
            }
            system.debug('emailLst--->'+emailLst);
            email.setToAddresses(emailLst);
           // email.setTargetObjectId(i);
            email.setTemplateId(et.id);
            email.setSaveAsActivity(false);
            email.setHtmlBody(mailHtmlBody);
            //email.setPlainTextBody(mailTextBody);
            sendEmailList.add(email);
        }
        
        if(sendEmailList.size()>0){
            if(!Test.isRunningTest()) {
                Messaging.sendEmail(sendEmailList);
            }
        }
    }  
    
}