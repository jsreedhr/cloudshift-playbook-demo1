public class allocationTriggerHandler {


    public void checkMLR(Map<Id,Allocation__c> jobnewMap){
        set<Id> oppIdSet = new set<Id>();
        List<Allocation__c> allocList = new List<Allocation__c>();
        List<Work_Type__c> wrkTypLst = new List<Work_Type__c>();
        Map<string,Boolean> wkMLRmap = new Map<string,Boolean>();
        List<Opportunity> updateOppList = new List<Opportunity>();
        
        system.debug('jobnewMap--->'+jobnewMap);
        wrkTypLst = [SELECT id,name,MLR__c from Work_Type__c];
        
        for(Allocation__c alloc:jobnewMap.values()){
            oppIdSet.add(alloc.job__c);
        }
        
        system.debug('wrkTypLst --->'+wrkTypLst);
        
        for(Work_Type__c wt : wrkTypLst){
            wkMLRmap.put(wt.name,wt.MLR__c);
        }
        allocList = [SELECT id,name,Job__c,workType_Allocation__c,Job__r.Amount from Allocation__c where job__c IN:oppIdSet AND Complete__c = false ];
        system.debug('wkMLRmap    --->'+wkMLRmap);
        set<Id> nonMLRset = new set<Id>();
        oppIdSet.clear();
        for(Allocation__c alloc:allocList){
           // for(Allocation__c alloc :allocList){
                opportunity opty = new Opportunity();
                system.debug('alloc---->'+alloc);
                system.debug('alloc.workType_Allocation__c--->'+alloc.workType_Allocation__c);
                system.debug('alloc.Job__r.Amount---->'+alloc.Job__r.Amount);
                system.debug('wkMLRmap.get(alloc.workType_Allocation__c)---->'+wkMLRmap.get(alloc.workType_Allocation__c));
                
                List<Job_Amount__c> cfRem = Job_Amount__c.getall().values();
                system.debug('cfrem'+cfRem);
                
                // edited by Nidheesh because it is causing error in allocation test class (List index out of bounds: 0 )
                Decimal jobAmt = 0.00;
                if(!cfRem.isEmpty()){
                     jobAmt = jobAmt +cfRem[0].Amount__c;
                }
               
                system.debug('jobAmt --->'+jobAmt );
                if(alloc.Job__r.Amount > jobAmt && !string.isblank(alloc.workType_Allocation__c) && wkMLRmap.get(alloc.workType_Allocation__c) == true){
                    opty.id=alloc.job__c;
                    opty.MLR_Required__c = true;
                    if(!oppIdSet.contains(alloc.Job__c)){
                        updateOppList.add(opty);
                    }
                    oppIdSet.add(alloc.Job__c);
                }else{
                    /*opty.id=alloc.job__c;
                    opty.MLR_Required__c = false;
                    if(!oppIdSet.contains(alloc.Job__c)){
                        updateOppList.add(opty);
                    }
                    oppIdSet.add(alloc.Job__c);*/
                }
            //}
            system.debug('oppIdSet--------------->'+oppIdSet);
            system.debug('updateOppList--------------->'+updateOppList);
        }
        
        if(updateOppList.size()>0){
            update updateOppList;
        }
    }

    public void populateFieldsOnOpp(list<Allocation__c> allocationList)
    {
        Set<id> oppIdSet = new Set<id>();
        for(Allocation__c alloc : allocationList)
        {
            if(alloc.Job__c!=NULL)
                oppIdSet.add(alloc.Job__c);
        }
        if(oppIdSet.size()>0)
        {
            list<Opportunity> oppList = new list<Opportunity>();
            map<id,list<Allocation__c>> oppAllocMap = new map<id,list<Allocation__c>>();
            oppList = [Select id,Forecasted_Fee_Total__c,Original_Fee_Total__c,Total_Accrued_Amount__c,Total_Invoice_Amount__c from Opportunity where ID IN :oppIdSet];
            if(oppList.size()>0)
            {
                list<Allocation__c> allocationQuery = [Select id,Job__c,Total_Amount__c,Allocation_Amount__c,Total_Accured_Amount__c,Total_Invoice_Amount__c from Allocation__c where Job__c IN :oppIdset AND Complete__c = false ];
                if(allocationQuery.size()>0)
                {
                    for(Allocation__c alloc : allocationQuery)
                    {
                        if(oppAllocMap.containsKey(alloc.Job__c))
                            oppAllocMap.get(alloc.Job__c).add(alloc);
                        else
                        {
                            list<Allocation__c> allocAddList = new list<Allocation__c>();
                            allocAddList.add(alloc);
                            oppAllocMap.put(alloc.Job__c,allocAddList);
                        }
                    }
                }
                    for(Opportunity opp : oppList)
                    {
                        Decimal forecastedFeeTot = 0.0;
                        Decimal orignalFeeTot = 0.0;
                        Decimal accruedAmt = 0.0;
                        Decimal invoiceamt = 0.0;
                        if(oppAllocMap.containsKey(opp.id))
                        {
                            for(Allocation__c alloc : oppAllocMap.get(opp.id))
                            {
                                if(alloc.Total_Amount__c!=NULL)
                                    forecastedFeeTot+=alloc.Total_Amount__c;
                                if(alloc.Allocation_Amount__c!=NULL)
                                    orignalFeeTot+=alloc.Allocation_Amount__c;
                                if(alloc.Total_Accured_Amount__c!=NULL)
                                    accruedAmt+=alloc.Total_Accured_Amount__c;
                                if(alloc.Total_Invoice_Amount__c!=NULL)
                                    invoiceamt+=alloc.Total_Invoice_Amount__c;
                            }   
                        }
                        opp.Forecasted_Fee_Total__c=forecastedFeeTot;
                        opp.Original_Fee_Total__c=orignalFeeTot;
                        opp.Total_Accrued_Amount__c=accruedAmt;
                        opp.Total_Invoice_Amount__c=invoiceamt;
                    }
                    update oppList;
            }
        }
    }
    
    public void updateAllocationComplete(Map<Id,Allocation__c> mapIdOldAllocation,Map<Id,Allocation__c> mapIdNewAllocation ){
        
        set<id> setAllocationIds = new set<id>();
        for(Allocation__c objAllocation : mapIdNewAllocation.values()){
            
            if(objAllocation.Complete__c != mapIdOldAllocation.get(objAllocation.id).Complete__c && objAllocation.Complete__c == true){
                setAllocationIds.add(objAllocation.id);
            }
        }
        
        if(!setAllocationIds.isEmpty()){
            List<Invoice_Allocation_Junction__c> lstInvoiceAllocation = new List<Invoice_Allocation_Junction__c>();
            lstInvoiceAllocation = [Select id, Name ,
                                            Allocation__c,
                                            Invoice__c,
                                            Invoice__r.Status__c,
                                            Forecasting__c,
                                            Forecasting__r.Allocation__c
                                            From
                                            Invoice_Allocation_Junction__c
                                            where (Invoice__r.Status__c = 'Draft'
                                            Or Invoice__r.Status__c = 'Archived') 
                                            AND Forecasting__r.Allocation__c in : setAllocationIds limit 10000];
                                            
            
            try{
                if(!lstInvoiceAllocation.isEmpty()){
                    delete lstInvoiceAllocation;
                }
            }catch(exception e){
                System.debug('----->'+e.getMessage());
            }
            
            
            List<Accrual_Forecasting_Junction__c> lstAccuralForecasting = new List<Accrual_Forecasting_Junction__c>();
            List<Accrual__c> lstAccrual = new List<Accrual__c>();
            set<id> setAccrualIds = new set<id>();
            lstAccuralForecasting = [Select id, Name ,
                                        Accrual__c,
                                        Accrual__r.Status__c,
                                        Forecasting__c,
                                        Forecasting__r.Allocation__c
                                        From
                                        Accrual_Forecasting_Junction__c
                                        where Accrual__r.Status__c = 'Awaiting Approval'
                                        AND Forecasting__r.Allocation__c in : setAllocationIds limit 10000];
                                            
            for(Accrual_Forecasting_Junction__c objAccuralForecasting : lstAccuralForecasting){
                if(!setAccrualIds.contains(objAccuralForecasting.Accrual__c)){
                    Accrual__c objAccrual = new Accrual__c();
                    objAccrual.id = objAccuralForecasting.Accrual__c;
                    objAccrual.Status__c = 'Rejected';
                    lstAccrual.add(objAccrual);
                }
            }
            
            try{
                if(!lstAccrual.isEmpty()){
                    update lstAccrual;
                }
            }catch(exception e){
                System.debug('----->'+e.getMessage());
            }
            
            List<Invoice_Allocation_Junction__c> lstInvoiceAllocationCredit = new List<Invoice_Allocation_Junction__c>();
            set<id> setInvoiceIds = new set<id>();
            lstInvoiceAllocationCredit = [Select id, Name ,
                                            Allocation__c,
                                            Invoice__c,
                                            Invoice__r.Status__c,
                                            Forecasting__c,
                                            Forecasting__r.Allocation__c
                                            From
                                            Invoice_Allocation_Junction__c
                                            where (Invoice__r.Status__c != 'Draft'
                                            Or Invoice__r.Status__c != 'Archived') 
                                            and  Invoice__r.Status__c != null
                                            AND Forecasting__r.Allocation__c in : setAllocationIds limit 10000];
            
            for(Invoice_Allocation_Junction__c objInvoiceAllocation: lstInvoiceAllocationCredit){
                setInvoiceIds.add(objInvoiceAllocation.Invoice__c);
            }
            if(!setInvoiceIds.isEmpty()){
                List<Credit_Note__c> lstCreditNote = new List<Credit_Note__c>();
                lstCreditNote = [Select id, Invoice__c,
                                            Status__c
                                            From 
                                            Credit_Note__c
                                            where Status__c = 'Awaiting Approval'
                                            and Invoice__c in: setInvoiceIds];
                                            
                                            
                for(Credit_Note__c objCreditNote : lstCreditNote){
                    objCreditNote.Status__c = 'Rejected';
                }
                
                if(!lstCreditNote.isEmpty()){
                    update lstCreditNote;
                }
                
            }
        }
    }
    public void checkUniqueCodingStrucute(Map<Id,Allocation__c> mapIdOldAllocation,Map<Id,Allocation__c> mapIdNewAllocation ){
        
        List<Allocation__c> lstAllocation  = new List<Allocation__c>();
        for(Allocation__c objAllocation : mapIdNewAllocation.values()){
            
            if(objAllocation.Coding_Structure__c != mapIdOldAllocation.get(objAllocation.id).Coding_Structure__c){
                lstAllocation.add(objAllocation);
            }
        }
        checkUniqueCodingStructure(lstAllocation);
    }
    
    
    public void checkUniqueCodingStructure(List<Allocation__c> lstNewAllocation){
        
        set<id> setCodingStructure = new set<id>();
        set<id> setJobIds = new set<id>();
        List<Allocation__c> lstAllocationExisting = new List<Allocation__c>();
        List<Allocation__c> lstAllocationNew  = new List<Allocation__c>();
        map<id, set<id>> mapIdCodingStructure = new map<id, set<id>>();
        for(Allocation__c objAllocation: lstNewAllocation){
            if(objAllocation.Job__c != null){
                setJobIds.add(objAllocation.Job__c);
                System.debug('Object msg objAllocation'+objAllocation.Coding_Structure__c);
                if(objAllocation.Assigned_To__c != null && objAllocation.Coding_Structure__c != null){
                    if(!setCodingStructure.contains(objAllocation.Coding_Structure__c)){
                        setCodingStructure.add(objAllocation.Coding_Structure__c);
                        lstAllocationNew.add(objAllocation);
                    }else{
                        objAllocation.addError('Allocation Record already exists for this Surveyor, Department, Office and WorkType');
                    }
                }
            }
        }
        if(!setJobIds.isEmpty() && !setCodingStructure.isEmpty()){
            lstAllocationExisting   = [Select id, name,
                                                Job__c,
                                                Coding_Structure__c
                                                From
                                                Allocation__c
                                                where Job__c in : setJobIds
                                                and Job__c != null
                                                and Coding_Structure__c != null
                                                and Assigned_To__c != null];
                                                
            
            for(Allocation__c objAllocation: lstAllocationExisting){
                if(mapIdCodingStructure.containsKey(objAllocation.Job__c)){
                    mapIdCodingStructure.get(objAllocation.Job__c).add(objAllocation.Coding_Structure__c);
                }else{
                    mapIdCodingStructure.put(objAllocation.Job__c, new set<id>{objAllocation.Coding_Structure__c});
                }
            
            }
            
            for(Allocation__c objAllocation: lstNewAllocation){
                if(mapIdCodingStructure.containsKey(objAllocation.Job__c)){
                    set<id> setCodingStructureExt = new set<id>();
                    setCodingStructureExt = mapIdCodingStructure.get(objAllocation.Job__c);
                    if(setCodingStructureExt.contains(objAllocation.Coding_Structure__c)){
                        objAllocation.addError('Allocation Record already exists for this Surveyor, Department, Office and WorkType');
                    }
                }
            }
        
        }
    }
    
    public void populateCodingStructure(List<Allocation__c> lstNewAllocation){
        
        set<id> setStaffIds = new set<id>();
        List<Allocation__c> lstNewAllocationNew = new List<Allocation__c>();
        for(Allocation__c objAllocation : lstNewAllocation){
            if(objAllocation.Assigned_To__c != null && !String.isBlank(objAllocation.Department_Allocation__c) 
            && !String.isBlank(objAllocation.Office__c) && !String.isBlank(objAllocation.workType_Allocation__c)){
                lstNewAllocationNew.add(objAllocation);
                setStaffIds.add(objAllocation.Assigned_To__c);
            }else{
                if(objAllocation.externalCompany__c == null){
                    objAllocation.Coding_Structure__c = null;
                }
            }
        }
        
        if(!setStaffIds.isEmpty()){
            List<Coding_Structure__c> lstCodingStructure = new List<Coding_Structure__c>();
            Map<String, id> MapCodingStructure = new Map<String, id>();
            lstCodingStructure = [SELECT Id, Name,
                                        Department__c,
                                        Office__c,
                                        Work_Type__c,
                                        Staff__c,
                                        Work_Type__r.Name 
                                        FROM Coding_Structure__c 
                                        WHERE Staff__c in:setStaffIds
                                        and Department__c != null
                                        and Office__c != null
                                        and Work_Type__c != null];
            
            for(Coding_Structure__c objCodingStructure : lstCodingStructure){
                String codingStructure = objCodingStructure.Staff__c+ objCodingStructure.Department__c + objCodingStructure.Office__c +objCodingStructure.Work_Type__r.Name;
                mapCodingStructure.put(codingStructure, objCodingStructure.id);
            }
            
            for(Allocation__c objAllocation : lstNewAllocationNew){
                String codingStructure = objAllocation.Assigned_To__c+ objAllocation.Department_Allocation__c + objAllocation.Office__c +objAllocation.workType_Allocation__c;
                if(mapCodingStructure.containsKey(codingStructure)){
                    objAllocation.Coding_Structure__c = mapCodingStructure.get(codingStructure);
                }
            }
        }
        
    }

}