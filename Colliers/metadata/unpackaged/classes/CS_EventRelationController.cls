/**
 *	Class Name:	RescheduleProjectRevenueController 
 *	Description: This is a class to Reschedule Project Revenue Records 
 *	Company: dQuotient
 * 	CreatedDate: 18/08/2016
 *
 *	Modification Log
 *	-----------------------------------------------------------
 *	Developer			Modification Date			Comments
 *	-----------------------------------------------------------
 *	Hemant garg			19/08/2016  			    Orginal Version
 *
 *
 */
public with sharing class CS_EventRelationController{
    Public String Ids{get;set;}
    Public List<EventRelation> ListEventRel{get;set;}
    Public List<ID> UserIds{Get;set;}
    Public List<ID> LeadIds;
    Public List<ID> ContactIds;
    Public List<user> ListUser;
    Public List<Contact> ListContact;
    Public List<Lead> ListLead;
    Public List<booleanDetailsWrapper> ListbooleanDetailsWrapper{get;set;}
    Public boolean Invitee{get;set;}
    private final Event acct;

   
    Public string keyPrefix{get;set;}
   
    
    
    public CS_EventRelationController(ApexPages.StandardController stdController) {
        this.acct = (Event)stdController.getRecord();

        UserIds=new List<ID>();
        LeadIds=new List<ID>();
        ContactIds=new List<ID>();
        ListUser= new List<user>(); 
        Map<id,boolean> mapIDInvite= new Map<id,boolean>();
        ListContact= new List<Contact>();
        ListLead= new List<Lead>();
        ListEventRel= new List<EventRelation>();
        ListbooleanDetailsWrapper= new List<booleanDetailsWrapper> ();
        Invitee=true;
        Ids=acct.id;
        //Ids = apexpages.currentpage().getparameters().get('id');
        ListEventRel=[SELECT Response,RelationId,IsParent,status,isInvitee   FROM EventRelation WHERE   eventId=:Ids];
        for(EventRelation Rel:ListEventRel){
            mapIDInvite.put(Rel.RelationId,Rel.isInvitee);
        }
        
        set<id> idsss=new set<id>();
        for(EventRelation LstEvent: ListEventRel){
          if(LstEvent.status=='Accepted'){
              idsss.add(LstEvent.RelationId);
          }
          keyPrefix = String.valueOf(LstEvent.RelationId).substring(0,3);
          if(keyPrefix=='003'){
              ContactIds.add(LstEvent.RelationId);
          }
          else if(keyPrefix=='005'){
              UserIds.add(LstEvent.RelationId);
          } 
          else if(keyPrefix=='00Q'){
              LeadIds.add(LstEvent.RelationId);
          }
          
      
          
      }
      ListUser=[SELECT id ,CompanyName , name ,Phone, MobilePhone, Email,title FROM User where id in:UserIds];
      ListContact=[SELECT Salutation, id,Name,email,Account.name ,Job_Title__c,Phone,MobilePhone  FROM Contact where id in: ContactIds];
      listlead=[SELECT  Salutation,name,Company, Email,Job_Title__c , MobilePhone ,phone FROM Lead where id in: LeadIds];
      for(User Usr:ListUser){
          if(idsss.contains(Usr.id)){
           ListbooleanDetailsWrapper.add(new booleanDetailsWrapper(true,mapIDInvite.get(Usr.id),'',Usr.name,Usr.Email,Usr.CompanyName,Usr.Title,Usr.Phone,Usr.MobilePhone));
          }else{
              ListbooleanDetailsWrapper.add(new booleanDetailsWrapper(False,mapIDInvite.get(Usr.id),'',Usr.name,Usr.Email,Usr.CompanyName,Usr.Title,Usr.Phone,Usr.MobilePhone));
          }
      }
      for(Contact Cntct:ListContact){
           if(idsss.contains(Cntct.id)){
           ListbooleanDetailsWrapper.add(new booleanDetailsWrapper(true,mapIDInvite.get(Cntct.id),Cntct.Salutation,Cntct.name,Cntct.Email,Cntct.Account.name,Cntct.Job_Title__c,Cntct.Phone,Cntct.MobilePhone));
          }else{
              ListbooleanDetailsWrapper.add(new booleanDetailsWrapper(False,mapIDInvite.get(Cntct.id),Cntct.Salutation,Cntct.name,Cntct.Email,Cntct.Account.name,Cntct.Job_Title__c,Cntct.Phone,Cntct.MobilePhone));
              
          }
          
      }
      for(Lead Led:listlead){
           if(idsss.contains(Led.id)){
           ListbooleanDetailsWrapper.add(new booleanDetailsWrapper(true,mapIDInvite.get(Led.id),Led.Salutation,Led.name,Led.Email,Led.Company,Led.Job_Title__c,Led.Phone,Led.MobilePhone));
          }else{
           ListbooleanDetailsWrapper.add(new booleanDetailsWrapper(False,mapIDInvite.get(Led.id),Led.Salutation,Led.name,Led.Email,Led.Company,Led.Job_Title__c,Led.Phone,Led.MobilePhone));
              
          }
          
      }
      
      
       
       //String keyPrefix = '001';
        
         

     
        
    }
  

    
    
   
    
    public class booleanDetailsWrapper{
        public Boolean isAttending{get;set;}
        public String Sal{get;set;}
        public Boolean IsInvitee{get;set;}
        Public string Names{get;set;}
        Public string Emails{get;set;}
        Public string Companys{get;set;}
        Public string Titles{get;set;}
        Public string Phones{get;set;}
        Public string Mobiles{get;set;}
       
        
        public booleanDetailsWrapper(Boolean isAttendings ,Boolean IsInvitees,String sals,string Namess, string Emailss,string Companyss,string Titless,string Phoness,string Mobiless ){
          sal=sals;
          IsInvitee=IsInvitees;
          Names = Namess;
          Emails=Emailss;
          Companys=Companyss;
          Titles=Titless;
          Phones=Phoness;
          Mobiles=Mobiless;
          isAttending=isAttendings;
          
         
          
          
           
        }
    }
}