@isTest
private class CS_EventRelationControllerTest {
   
    Public static Account a;
    static Contact c;
    static Contact c2;
    static opportunity opp;
    static Property__c prpty;
    static event evnt;
    static EventRelation Ertion;
   
    
     static testmethod void setupData(){
        //create account
        a = TestObjectHelper.createAccount();
        insert a ;
         c = TestObjectHelper.createContact(a);
         c.Email ='test1@test1.com';
        insert c;
        c2 = TestObjectHelper.createContact(a);
        c2.Email ='test2@test2.com';
        insert c2;
        
        prpty=TestObjectHelper.createProperty();
        prpty.Street_No__c ='Park Lane';
        prpty.Post_Code__c ='W1K 3DD';
        prpty.Country__c='United Kingdom';
        prpty.Geolocation__Latitude__s=51.51;
        prpty.Geolocation__Longitude__s=-0.15;
        insert prpty;
        
        evnt= TestObjectHelper.createEvent(c);
        insert evnt ;
        
        Ertion= TestObjectHelper.CreateEventRel(evnt,c2);
        insert Ertion;
        
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
        
        opp = [Select id from opportunity Limit 1];
        
      
      
    }
    
    
    private static testMethod void test1() {
         setupData();
         
        
        Test.startTest();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(evnt);
        CS_EventRelationController testAccPlan = new CS_EventRelationController(sc);
         Test.stopTest();


    }
}