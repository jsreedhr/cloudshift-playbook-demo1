/**
*  Class Name: QuoteSummaryPDFController  
*  Description: This is a Trigger Handler Class for trigger on QuoteLineItem
*  Company: dQuotient
*  CreatedDate: 14/04/2017 
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------  
*  Anand              14/04/2017                  Orginal Version
*
*/
public with sharing class QuoteSummaryPDFController{

    public QuoteSummaryPDFController(ApexPages.StandardController controller) {

    }

    
    
    public QuoteSummaryPDFController(){
        
    }
    
}