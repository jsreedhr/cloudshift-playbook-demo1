public class job_RelatedfieldTriggerHandler {
    Public List<worktype_Job_Related_to__c> lstofworkTypeMapping = new List<worktype_Job_Related_to__c>();
    public Map<string,string> worktypemapping = new Map<string,string>();
    Public job_RelatedfieldTriggerHandler(){
        
    }
    
    public void uniquerelatedfieldinsert(List<Opportunity> lstofinsert){
        
        if(!lstofinsert.isEmpty()){
            set<Id> SetOfstaff = new set<Id>(); 
            for(Opportunity oppobj:lstofinsert ){
                if(oppobj.Manager__c != null){
                    SetOfstaff.add(oppobj.Manager__c);  
                }
            }
            List<Coding_Structure__c> lstOfcodinngstruct = new List<Coding_Structure__c>();
            Map<Id,List<string>> mapofStaffcode = new Map<Id,List<string>>();
            Set<string> lstOfworktypename = new set<string>();
            if(!SetOfstaff.isEmpty()){
                lstOfcodinngstruct = [SELECT Id,Work_Type__c,Work_Type__r.Name,Name,Staff__c FROM Coding_Structure__c WHERE Staff__c IN:SetOfstaff];
            }
            System.debug('staffvalue'+lstOfcodinngstruct);
            for(Id idobj :SetOfstaff){
                List<string> worktypename = new List<string>();
                if(!lstOfcodinngstruct.isEmpty()){
                    for(Coding_Structure__c codeobj :lstOfcodinngstruct ){
                        System.debug('staffvalue'+codeobj.Work_Type__r.Name);
                        if(codeobj.Staff__c == idobj){
                            worktypename.add(codeobj.Work_Type__r.Name);
                            lstOfworktypename.add(codeobj.Work_Type__r.Name);
                        }
                    }
                }
                 mapofStaffcode.put(idobj,worktypename);
            }
            
            lstofworkTypeMapping = [SELECT Id,Job_related_field__c,worktype_Name__c FROM worktype_Job_Related_to__c WHERE worktype_Name__c IN:lstOfworktypename ];
            system.debug('customsettings'+lstofworkTypeMapping);
            if(!lstofworkTypeMapping.isEmpty()){
                for(worktype_Job_Related_to__c obj :lstofworkTypeMapping){
                    worktypemapping.put(obj.worktype_Name__c,obj.Job_related_field__c);    
                }
                for(Opportunity oppobj:lstofinsert ){
                    if(oppobj.Manager__c != null){
                        if(mapofStaffcode.containsKey(oppobj.Manager__c)){
                            if(!mapofStaffcode.get(oppobj.Manager__c).isEmpty()){
                                if(worktypemapping.containsKey(mapofStaffcode.get(oppobj.Manager__c).get(0))){
                                    //oppobj.Relates_To__c = worktypemapping.get(mapofStaffcode.get(oppobj.Manager__c).get(0));  
                                }  
                            }
                               
                        } 
                    }
                }     
            }
        }
            
    }
    public void uniquerelatedfieldupdate(Map<Id,Opportunity> mapold,Map<Id,Opportunity> mapnew){
        List<Opportunity> lstOfOpportunity = new List<Opportunity>();
        if(!mapold.isEmpty() && !mapnew.isEmpty()){
            for(id objId : mapnew.Keyset()){
                if(mapnew.get(objId).Manager__c !=mapold.get(objId).Manager__c ){
                    lstOfOpportunity.add(mapnew.get(objId));    
                }
            }
            uniquerelatedfieldinsert(lstOfOpportunity);
        }    
    }
    
    
    
    

}