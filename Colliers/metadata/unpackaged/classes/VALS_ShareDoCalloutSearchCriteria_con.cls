/**
 *  Class Name:  ShareDoCalloutController 
 *  Description: This is a class for performing ShareDo Integration call outs from the Custom
 *               button on SerachCriteria Object
 */
 public with sharing class VALS_ShareDoCalloutSearchCriteria_con {

 Public String responseString{get;set;}
  Public Boolean dataValidationError{get;set;}
  public void doCallout(){
   //Getting ShareDo Credentials from Custom Setting
    Map<string,ShareDo_Credentials__c> shareDo = ShareDo_Credentials__c.getAll();
    dataValidationError = false;
  
  
  String SearchCriteriaID=ApexPages.currentPage().getParameters().get('SearchCriteriaID');
  
  String jsonData;
  Search_Criteria__c SearchObj= [Select id,Property_Category__c,Size_Max_sqFt__c,Tenancy__c,No_of_Bedrooms_Min__c,
                     Type_Parking_Spaces__c,Bathrooms_Max__c,Bathrooms_Min__c,Borough__c,Include_undisclosed_sizes__c,Keyword__c,Job_Number__c,Lease_Date_From__c,Lease_Date_To__c,
                     Building_Status__c,Development_SIte__c,Distance_Miles__c,Include_Let_Agreed_Properties__c,Lease_Type_Direct__c,Lease_Type_Sublease__c,LonRes_Status__c,Molior_Status__c,
                     No_of_Bedrooms_Max__c,Planning_Status__c,Property__c,Property_Postcode__c,RIght_Move_Property_Type__c,Zoopla_Property_Type__c,Region__c,Sale_Date_From__c,
                     Sale_Date_To__c,Sale_Status_For_Sale__c,Sale_Status_Sold__c,Sale_Status_Under_Offer__c,Sale_Type_Investment__c,Sale_Type_OwnerUser__c,Search_CoStar__c,
                     Search_EGi_Commercial__c,Search_EGi_Residential_Dev__c,Search_EIG__c, Search_LonRes__c, Search_Molior__c, Search_Property_Data__c, Search_Rightmove__c, Search_Zoopla__c,
                     Signed_within_the_last__c, Size_Min_sqFt__c, Sold_in_the_Room__c, Street__c, Tenancy_EIG__c, Tenure_Freehold__c, Tenure_Long_Leasehold__c ,
                     Term_Long_Lets__c,Term_Short_Lets__c,Town__c,Transaction_Type_Auction__c,Transaction_Type_Sale__c,type__c,Type_Blocks_of_Flats__c,
                     Type_Development_Opportunities__c,Type_Flats__c,Type_Houses__c,Type_Land_Only__c from Search_Criteria__c where id=:SearchCriteriaID LIMIT 1];
      
      //searchTermValues searchTermValues = new searchTermValues(SearchObj.Size_Min_sqFt__c, SearchObj.Size_Max_sqFt__c,SearchObj.type__c);
      //searchTermValues searchTermValuesObj = New searchTermValues(SearchObj);
      SearchCriteriaWrapper SearchCriteriaWrapper = new SearchCriteriaWrapper(SearchObj.id,SearchObj);

                     
                     //Encoding Api Key for authorization
      String apiKey = EncodingUtil.base64Encode(Blob.valueOf(shareDo.get('Sharedo Search Criteria').API_Key__c));
      String authorizationHeader = 'bearer '+apiKey;
      //String username = shareDo.get('Sharedo Search Criteria').ShareDo_Username__c;
      String username = 'EU\\svc_sharedoAPI';
      String password = shareDo.get('Sharedo Search Criteria').ShareDo_Password__c;
      Blob headerValue = Blob.valueOf(username + ':' + password);
      String authorizationHeaderBasic = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
                     
      jsonData = JSON.serialize(SearchCriteriaWrapper);
      

      if(jsonData!=NULL)
      {
        Map<String,Schema.SObjectType> objectMap = Schema.getGlobalDescribe(); 
        SObjectType searchCriteriaToken = objectMap.get('Search_Criteria__c');
        DescribeSObjectResult objDef = searchCriteriaToken.getDescribe();
        Map<String, SObjectField> fields = objDef.fields.getMap();
        Set<String> fieldSet = fields.keySet();
        for(String s:fieldSet)
        {
            SObjectField fieldToken = fields.get(s);
            DescribeFieldResult selectedField = fieldToken.getDescribe();
            String fieldLabel = selectedField.getLabel();
            String fieldName = selectedField.getName();
            if(jsonData.containsIgnoreCase(fieldName))
              jsonData=jsonData.replace(fieldName,fieldLabel);
            System.Debug(fieldName);
        }
        if(jsonData.contains('{"attributes":{"type":"Search_Criteria__c"},"Record ID":null,')) 
          jsonData = jsonData.replace('{"attributes":{"type":"Search_Criteria__c"},"Record ID":null,','{'); 
      }
      System.debug('Le JSON: '+jsonData);

        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        req.setHeader('Authorization', authorizationHeaderBasic);
        req.setHeader('X-Authorization', authorizationHeader);
        req.setBody(jsonData);
        System.debug('Le AuthKey: '+req.getHeader('Authorization'));
        if(Test.isRunningTest())
        {
          responseString = 'Test Mode';
        }
        else if(!dataValidationError)
        {
         req.setEndpoint(shareDo.get('Sharedo Search Criteria').shareDoUrl__c);
         // req.setEndpoint('http://requestb.in/1moyakm1'); //Used for Testing
          System.debug('You shall not pass: ' + shareDo.get('Sharedo Search Criteria').shareDoUrl__c);
          Http http = new Http();
          HTTPResponse res = http.send(req);
          System.debug('ShareDo Response:' + res.getBody());
          responseString = res+'-- ' + res.getBody().substringAfter('errorMessage').substringBefore('}]}');
		
            Vals__c log   = new Vals__c();
            log.Response_String__c = responseString; 
            log.JsonData__c = jsonData;
            insert log ;
           System.debug('log---------->' +log);
        }
      
      else
                   {
                           Vals__c log   = new Vals__c();
                          
                           log.Response_String__c = responseString; 
                           log.JsonData__c = jsonData;
                           insert log ;
                       System.debug('log---------->' +log);
                   }
       // System.Debug('Data Error: '+dataValidationError);               
    }
  

    public class SearchCriteriaWrapper 
    {
        public String id;
        public Search_Criteria__c searchTermValues;
        public SearchCriteriaWrapper(String id, Search_Criteria__c searchTermValues)
        {
          if(id!=NULL)
            this.id = id;
          if(searchTermValues!=NULL)
          {
            searchTermValues.Id = NULL;
            this.searchTermValues = searchTermValues;
          }  
        }
    }
  }