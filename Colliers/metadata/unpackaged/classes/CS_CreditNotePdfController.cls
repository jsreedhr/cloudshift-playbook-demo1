/**
*  Class Name: CS_InvoicePdfController
*  Description: This is a class to edit all invoice related to the jobs
*  Company: dQuotient
*  CreatedDate: 27/09/2016
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------
*  Hemant garg         17/10/2016                  Orginal Version
*  M Rizwan			   13/10/2017				   Changes to display User entered values for some Invoices
*
*/
public with sharing class CS_CreditNotePdfController{

	Public String prospectId{get;set;}
	Public String InvId{get;set;}
	//Public boolean summery{get;set;}
	//Public string CreatedFor{get;set;}
	Public List<invoice__c> listInvoive {get;set;}
	public List<Invoice_PDF_Value__c> InvPdf{get;set;}
	public List<string> Remitence{get;set;}
	public List<string> invoiceAddress{get;set;}
	public string FooterMsg{get;set;}
	public string accountcode{get;set;}
	public Decimal totalamount{get;set;}
	public date todaysdate{get;set;}
	public Boolean isCapital{get;set;}
	public Boolean isnilemanage{get;set;}
	public string IsDraftOrApproved{get;set;}
	public string Paymentfoot{get;set;}
	public string Footer{get;set;}
	public string faoname{get;set;}
	public string careofClient{get;set;}
	public string currencysymbiol{get;set;}
	public string PoRef{get;set;}
	public Opportunity oppobject{get;set;}
	public string status{get;set;}
	public string currencyCode {get;set;}
	public List<Invoice_Line_Item__c>FeeList{get;set;}
	public List<Invoice_Line_Item__c>CostList{get;set;}
	public currency_Symbol__c currencyobj {get;set;}
	public boolean showCareof{get;set;}

    //variables added for D-0450
    Public Boolean showChangedValues{get;set;}
	Public Double feeTotal{get;set;}
	Public Double costTotal{get;set;}
	Public Double vatTotal{get;set;}


	/**
	*   Method Name:    CS_InvoicePdfController
	*   Description:    Constructor
	*/
	public CS_CreditNotePdfController(ApexPages.StandardController stdController) {
		isCapital = false;
		isnilemanage = false;
		todaysdate=date.today();
		IsDraftOrApproved = '';
		faoname = '';
		totalamount = 0.00;

		currencyobj = new currency_Symbol__c();
		oppobject = new Opportunity();
		PoRef='';
		accountcode ='';
		currencysymbiol = '';
		FooterMsg= Label.Default_FooterMsg;
		InvId = apexpages.currentpage().getparameters().get('Invid');
		CostList= new List<Invoice_Line_Item__c>();
		FeeList= new List<Invoice_Line_Item__c>();
		prospectId = apexpages.currentpage().getparameters().get('id');
		status = apexpages.currentpage().getparameters().get('Copy');
		listInvoive = new List < invoice__c > ();
		InvPdf= new List<Invoice_PDF_Value__c>();
		currencyCode = '';
		Remitence= new List<string> ();
		invoiceAddress = new List<string> ();
		List<Account_Address_Junction__c> accAdressobj  = new List<Account_Address_Junction__c>();

        //checking whether to show actual data or changed values (13-Oct-2017)
		showChangedValues = false;
		Invoice__c pdfInvoice = new Invoice__c();
		/*
		* Label.Allocation_Department_Option1 = Residential - Development
		* Label.Allocation_Department_Option2 = Residential - New Homes
		* Label.Allocation_Department_Option3 = International Properties
		* Label.Invoice_Cost_Category = Cost Flow Through (Residential only)
		*/
		try{
			pdfInvoice = [Select Id, Name, Total_PDF_Amount__c,
				(Select Invoice__c From Invoice_Allocation_Junctions__r
					Where Allocation__r.Main_Allocation__c = TRUE AND
					(Allocation__r.Department_Allocation__c =: Label.Allocation_Department_Option1 OR Allocation__r.Department_Allocation__c =: Label.Allocation_Department_Option2 OR Allocation__r.Department_Allocation__c =: Label.Allocation_Department_Option3)
				),
				(Select Invoice__c From Invoice_Cost_Junctions__r
				Where Disbursement__r.Category__c =: Label.Invoice_Cost_Category)
				From Invoice__c
				Where Id =: InvId Limit 1
			];
		}catch(Exception ex){
			System.debug('-- Error while fetching data for PDF Values tab : '+ ex);
		}
		if(pdfInvoice.Invoice_Allocation_Junctions__r.size()> 0 && pdfInvoice.Invoice_Cost_Junctions__r.size()> 0){
			showChangedValues = true;
		}
		//end of block1 of code

		listInvoive = [select Id, Total_PDF_Amount__c,Invoice_Currency__c,Description__c,Total_Internaional_Vat_Amount__c,Total_cost_Ex_Vat__c,Total_Fee_Ex_Vat__c , Date_of_Invoice__c, Invoice_Address__c,Company__r.name,Opportunity__r.Invoicing_Company__c,Opportunity__r.Invoicing_Care_Of__r.client__r.name,Care_Of_Information__r.name,Invoice_contact__r.name,OriginalInvoicePrinted__c,Final_Invoice__c , Company__c,Opportunity__c,CurrencyIsoCode,PO_Number__r.name,(select name from Purchase_Order_Invoice_Junctions__r),Opportunity__r.Job_Number__c,Opportunity__r.Invoicing_Company2__r.MLR_Number__c,Opportunity__r.Invoicing_Company2__c,Total_Fee__c ,Total_cost__c,Total_amount__c,VAT_Amount__c,Opportunity__r.Invoicing_Company2__r.Client_VAT_Number__c,Invoice_Wording__c,Opportunity__r.name,Opportunity__r.Manager__r.HR_No__c,Opportunity__r.Invoicing_Company2__r.name,Opportunity__r.Manager__r.Coding_Structure__r.Department__c,Opportunity__r.id,Assigned_Invoice_Number__c,File_Reference__c,Vat_Reg_No__c ,Foreign_Country__c,Opportunity__r.Department_Integer_Id__c,Opportunity__r.Manager__r.id, name,  Contact__c, Contact__r.name, FE_Write_Of_Created_Date__c, is_International__c, Payable_By__c,status__c,VAT__c, createdDate,(select id,fee__c,type__c,Cost_Type__c,Amount_ex_VAT__c,VAT_Amount__c,International_Amount__c,International_VAT_Amount_Formula__c,Description__c, PDF_Amount__c, PDF_VAT__c from Invoice_Line_Items__r where Type__c='Fee' Or Type__c='Cost') from invoice__c where id=:InvId limit 1];
		if(listInvoive[0].Opportunity__c != null){
			oppobject = [SELECT Id,Invoicing_Company__r.name,Invoicing_Company__r.Address__r.Building__c,Invoicing_Company__r.Address__r.Street__c,Invoicing_Company__r.Address__r.Town__c,Invoicing_Company__r.Address__r.country__c FROM Opportunity WHERE Id=:listInvoive[0].Opportunity__c];

		}

        //check whether to show original valu or changed value
		for(Invoice_Line_Item__c inv: listInvoive[0].Invoice_Line_Items__r){
			if(showChangedValues == true && (inv.PDF_Amount__c == null || inv.PDF_Amount__c<=0) ){
				showChangedValues = false;
				break;
			}
		}
		//if showChangedValues is still true then calculate new feeTotal, costTotal & VATTotal
		if(showChangedValues){
			feeTotal = costTotal = vatTotal = 0;
			for(Invoice_Line_Item__c inv: listInvoive[0].Invoice_Line_Items__r){
				vatTotal += inv.PDF_VAT__c;
				if(inv.Type__c == 'Fee')
					feeTotal += inv.PDF_Amount__c;
				else if(inv.Type__c == 'Cost')
					costTotal += inv.PDF_Amount__c;
			}
			/*vatTotal = vatTotal.toScale(2);
			feeTotal = feeTotal.toScale(2);
			costTotal = costTotal.toScale(2);
			*/
		}


		if(listInvoive[0].Company__c != null){
			accAdressobj =  [SELECT Account_ID__c,Id,Name FROM Account_Address_Junction__c WHERE Account__c =: listInvoive[0].Company__c ];
			if(!accAdressobj.isEmpty()){
				accountcode  = accAdressobj[0].Account_ID__c;
			}
		}
		if(listInvoive[0].VAT__c== null){
			listInvoive[0].VAT__c=0;
		}
		if(listInvoive[0].Company__r.name!= null && string.isNotBlank(listInvoive[0].Company__r.name)){
			faoname = listInvoive[0].Company__r.name;
			showCareof=false;
		}else if(listInvoive[0].Care_Of_Information__r.name!= null && string.isNotBlank(listInvoive[0].Care_Of_Information__r.name)){
			faoname = listInvoive[0].Care_Of_Information__r.name;
			careofClient = listInvoive[0].Opportunity__r.Invoicing_Care_Of__r.client__r.name;//Defect-191
			showCareof=true;
		}
		List<String> addresslist= new List<string> ();
		if(listInvoive[0].Invoice_Address__c != null && string.isNotBlank(listInvoive[0].Invoice_Address__c)){
			addresslist = listInvoive[0].Invoice_Address__c.split(',');
		}
		for(string objstr: addresslist){
			if(string.isNotBlank(objstr) && objstr != null){
				invoiceAddress.add(objstr);
			}
		}
		if(!listInvoive.isEmpty()){
			if(listInvoive[0].Invoice_Currency__c != null){
				currencyCode = listInvoive[0].Invoice_Currency__c;
			}else{
				currencyCode = 'GBP';
			}
		}
		LisT<currency_Symbol__c> lstCurrencyCode = new LisT<currency_Symbol__c>();
		lstCurrencyCode = [SELECT Id,currencyName__c,currencySymbol__c FROM currency_Symbol__c WHERE currencyName__c=:currencyCode LIMIT 1];
		if(!lstCurrencyCode.isEMpty()){
			currencyobj = lstCurrencyCode[0];
			if(currencyobj != null){
				currencysymbiol = currencyobj.currencySymbol__c;
			}

		}
		System.debug('-------'+listInvoive[0].Opportunity__r.Invoicing_Company2__c);
		System.debug('-------'+listInvoive[0].Opportunity__r.Invoicing_Company__c);
		if(listInvoive != null && listInvoive.size()>0){


			system.debug('pdf value'+InvPdf);
			if(status == 'COPY'){
				IsDraftOrApproved='CERTIFIED COPY';
			}
			else if(status == 'DRAFT'){
				IsDraftOrApproved='DRAFT INVOICE';
			}
			else if(status== 'FINAL'){
				IsDraftOrApproved='ORIGINAL INVOICE';
			}
			system.debug('invoice pdf values'+invpdf);
			string NilehrNo = Label.Nile_Hr_No;
			if(listInvoive[0].Opportunity__r.Manager__r.HR_No__c == NilehrNo ){
				isnilemanage = true;
				string RemitanceBlank=Label.Default_Nile_Remittance;
				Remitence=RemitanceBlank.split('  ');
				Paymentfoot= Label.Default_Paymentfoot;
				Footer = Label.Default_Nile_Footer;
				

			}
			else{
				InvPdf = [SELECT Id,Cheque_Footer__c,Company__c,  External_Id_JM_UK__c,Footer__c, Currency__c,Remittance__c,Payment_Footer__c,Header_Details__c,  Department_Code__c, Company_Name__c from Invoice_PDF_Value__c WHERE  Department_Code__c includes (:listInvoive[0].Opportunity__r.Department_Integer_Id__c ) AND Currency__c =: listInvoive[0].Invoice_Currency__c AND  Department_Code__c != '' AND Currency__c != NULL ];
				if(InvPdf.size() > 0){

					if(InvPdf[0].Remittance__c != null && InvPdf[0].Remittance__c != ''){
						isCapital=InvPdf[0].Remittance__c.contains('COLLIERS CAPITAL');
						string RemitanceBlank=Label.Default_Remittance;


						if(InvPdf[0].Remittance__c != null && InvPdf[0].Remittance__c!=''){
							Remitence=InvPdf[0].Remittance__c.split(',');
						}
						else{
							Remitence=RemitanceBlank.split('  ');
						}
						if(InvPdf[0].Payment_Footer__c != null && InvPdf[0].Payment_Footer__c != ''){
							Paymentfoot=invPdf[0].Payment_Footer__c;
						}
						else if(InvPdf[0].Payment_Footer__c==null || InvPdf[0].Payment_Footer__c==''){
							Paymentfoot= Label.Default_Paymentfoot;

						}

						if(InvPdf[0].Footer__c != null && InvPdf[0].Footer__c != ''){
							Footer=invPdf[0].Footer__c;
						}
						else if(InvPdf[0].Footer__c==null || InvPdf[0].Footer__c==''){
							Footer= Label.Default_Footer;

						}
					}
				}
				else{
					string RemitanceBlank=Label.Default_Remittance;
					Remitence=RemitanceBlank.split('  ');
					Paymentfoot= Label.Default_Paymentfoot;
					Footer = Label.Default_Footer;
					

				}
			}
			for(Purchase_Order_Invoice_Junction__c PoJn: listInvoive[0].Purchase_Order_Invoice_Junctions__r){

				if(PoRef==''){
					PoRef= PoJn.name;
				}
				else{
					PoRef=PoRef+','+PoJn.name;
				}

			}
			for(Invoice_Line_Item__c cst: listInvoive[0].Invoice_Line_Items__r){
				if( cst.type__c=='Cost'){
					CostList.add(cst);
				}
				else{
					FeeList.add(cst);
				}

			}

		}
	}
	
}