@isTest 
private without sharing class Test_cs_conflictCheck
{
  static Account accountObj;
  static Opportunity oppObj;
    static Opportunity oppObj2;
    static Opportunity oppObj3;
  static Contact contactObj;
  static Invoice__c invoiceObj;
  static User portalAccountOwner1;
  static User userObj;
  static Address__c addressObj;
  static UserLogin__c userloginobj;
  static Staff__c staffObj;
  static Conflict_Reminder_days__c conflictDay;
  static Allocation__c allocationObj;
  static Property__c propertyObj;
    static Property__c propertyObj2;
    static Property__c propertyObj3;
  static Job_Property_Junction__c jobPropObj;
    static Job_Property_Junction__c jobPropObj1;
    static Job_Property_Junction__c jobPropObj2;
    static Job_Property_Junction__c jobPropObj3;
    static Other_Party__c otherPartyobj;
    static Conflict__c conflictObj;
    static Conflict__c conflictObjnew;
    static list<Conflict_Line_Item__c> conflictLiList;
  static void createData()
  {
    userObj = TestObjectHelper.createAdminUser(true);
    userobj.UserName ='Test_123@dquotient.com';
    Database.Insert(userObj, true);
        
    Profile profile1 = [Select Id from Profile where name = 'customer public access Profile'];
        portalAccountOwner1 = new User(
                                            ProfileId = profile1.Id,
                                            Username ='hormese@test.com',
                                            Alias = 'batman',
                                            Email='bruce.wayne@wayneenterprises.com',
                                            EmailEncodingKey='UTF-8',
                                            Firstname='Bruce',
                                            Lastname='Wayne',
                                            LanguageLocaleKey='en_US',
                                            LocaleSidKey='en_US',
                                            TimeZoneSidKey='America/Chicago'
                                            );
        insert portalAccountOwner1;
    System.runAs(userObj) {
    accountObj = TestObjectHelper.createAccount();
    accountObj.Company_Status__c = 'Active';
      insert accountobj;
      System.assert(accountObj.id!=NULL);
      // create user-login custom settings
      userloginobj = new UserLogin__c();
      userloginobj.Auth_Code__c = '1213adhdhg135';
      userloginobj.Name = 'user-login';
      insert userloginobj;
      // create custom settings
      conflictDay = new Conflict_Reminder_days__c();
      conflictDay.Name = 'Test';
      conflictDay.No_of_Days__c = 5;
      insert conflictDay;
      propertyObj = TestObjecthelper.createProperty();
        propertyObj.Street_No__c ='Park Lane';
        propertyObj.Post_Code__c ='W1K 3DD';
        propertyObj.Country__c = 'United Kingdom';
        propertyObj.Geolocation__Latitude__s=51.51;
        propertyObj.Geolocation__Longitude__s=-0.15;
        propertyObj.Property_Type__c = 'Mixed Use';
      insert propertyObj;
      System.assert(propertyObj.id!=NULL);
        propertyObj2 = TestObjecthelper.createProperty();
        propertyObj2.Street_No__c ='Park Lane Clone';
        propertyObj2.Post_Code__c ='W1K 3DD';
        propertyObj2.Country__c = 'United Kingdom';
        propertyObj2.Geolocation__Latitude__s=51.53;
        propertyObj2.Geolocation__Longitude__s=-0.16;
        propertyObj2.Property_Type__c = 'Mixed Use';
        insert propertyObj2;
        System.assert(propertyObj2.id!=NULL);
      addressObj = TestObjecthelper.createAddress(accountObj);
      addressObj.Country__c = 'United Kingdom';
      insert addressObj;
      System.assert(addressObj.id!=NULL);
      oppObj = TestObjectHelper.createOpportunity(accountObj);
      oppObj.Invoicing_Company2__c = accountObj.id;
      oppObj.Invoicing_Address__c = addressObj.id;
        oppObj.Date_Instructed__c = system.Today();
      insert oppObj;
      System.assert(oppObj.id!=NULL);
        oppObj3= TestObjectHelper.createOpportunity(accountObj);
        oppObj3.Date_Instructed__c = system.today().addDays(-10);
        insert oppObj3;
        System.assert(oppObj3.id!=NULL);
      jobPropObj = TestObjecthelper.JobPrptyJn(oppObj,propertyObj);
      insert jobPropObj;
      System.assert(jobPropObj.id!=NULL);
      contactObj = TestObjectHelper.createContact(accountObj);
      contactobj.lastName='Test';
      contactObj.Email ='test1@test1.com';
      insert contactobj;
      System.assert(contactobj.id!=NULL);
        staffObj = TestObjecthelper.createStaff();
        staffObj.User__c = userObj.id;
        insert staffObj;
        System.assert(staffObj.id!=NULL);
      allocationObj = TestObjectHelper.createAllocation(oppObj,staffObj);
      insert allocationObj;
      System.assert(allocationObj.id!=NULL);
        otherPartyobj = new Other_Party__c();
        otherPartyobj.Company__c = accountObj.id;
        otherPartyobj.Job__c = oppObj.id;
        otherPartyobj.Company_Role__c = 'Landlord';
        insert otherPartyobj;
        System.assert(otherPartyobj.id!=NULL);
        oppObj2 = TestObjecthelper.createOpportunity(accountObj);
        oppObj2.Date_Instructed__c = system.today().addDays(-10);
        insert oppObj2;
        System.assert(oppObj2!=NULL);
        propertyObj3 = TestObjecthelper.createProperty();
        propertyObj3.Street_No__c ='Park Lane';
        propertyObj3.Post_Code__c ='W1K 3DD';
        propertyObj3.Country__c = 'United Kingdom';
        propertyObj3.Property_Type__c = 'Mixed Use';
        propertyObj3.Geolocation__Latitude__s=51.51;
        propertyObj3.Geolocation__Longitude__s=-0.15;
        insert propertyObj3;
        jobPropObj1 = TestObjecthelper.JobPrptyJn(oppObj2,propertyObj3);
        insert jobPropObj1;
        jobPropObj2 = TestObjecthelper.JobPrptyJn(oppObj,propertyObj3);
        insert jobPropObj2;
        jobPropObj3 = TestObjecthelper.JobPrptyJn(oppObj3,propertyObj3);
        insert jobPropObj3;
        System.assert(jobPropObj3.id!=NULL);
        System.assert(jobPropObj1.id!=NULL);
        System.assert(propertyObj3.id!=NULL);
        System.assert(jobPropObj2.id!=NULL);
        conflictObj = TestObjecthelper.createConflict(oppObj,oppObj2);
        insert conflictObj;
        conflictObjnew = TestObjecthelper.createConflict(oppObj,oppObj3);
        conflictObjnew.conflicMailSend__c = true;
        conflictObjnew.Potential_Conflict_resolved__c =false;
        insert conflictObjnew;
        System.assert(conflictObj!=NULL);
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='Conflict_Line_Item__c' and isActive=true];
        Map<String,String> lineItemRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
            lineItemRecordTypes.put(rt.Name,rt.Id);
        conflictLiList = new list<Conflict_Line_Item__c>();
        Conflict_Line_Item__c conflictLiObj = new Conflict_Line_Item__c();

        //conflictLiObj.RecordTypeId = lineItemRecordTypes.get('Client_conflict'); 
        conflictLiObj.RecordTypeId =  Schema.SObjectType.Conflict_Line_Item__c.getRecordTypeInfosByName().get('Client conflict').getRecordTypeId();
        System.Debug('There: '+conflictLiObj.RecordTypeId);
        conflictLiObj.Conflict__c = conflictObj.id;
        conflictLiObj.Customer__c = accountobj.id;
        conflictLiList.add(conflictLiObj);
         Conflict_Line_Item__c conflictLiObj1 = new Conflict_Line_Item__c(); 
        //conflictLiObj.RecordTypeId = lineItemRecordTypes.get('Property_conflict');
        conflictLiObj1.RecordTypeId =  Schema.SObjectType.Conflict_Line_Item__c.getRecordTypeInfosByName().get('Property conflict').getRecordTypeId();
        conflictLiObj1.Conflict__c = conflictObj.id;
        conflictLiObj1.Property__c = propertyObj2.id;
        conflictLiObj1.Conflicted_Property__c = propertyObj.id;
        conflictLiList.add(conflictLiObj1);
        insert conflictLiList;
      }
  }

  /*@isTest
  static void testMethod1()
  {
            createData();

            PageReference pageRef = Page.CS_conflictcheck_clone;
            pageRef.getParameters().put('id', oppObj.id );
            pageRef.getParameters().put('indexnumber','1' );
            Test.setCurrentPage(pageRef);

            cs_conflictCheck controller = new cs_conflictCheck();
            controller.conflict_check_method();
            controller.redirect();
            controller.selected_Type.clear();
            controller.selected_Type.add('Airport Hanger');
            controller.selected_Property = propertyObj.Name;
            controller.radius ='0.25';
            controller.show_All_Jobs = 'Yes';
            controller.conflict_check_method();
            controller.fetchconflict();
            controller.save();
            controller.savemethod();
            controller.createwrapper();
            
            
            
        
  }*/
   @isTest
  static void testMethod3()
  {
            createData();
             System.runAs(portalAccountOwner1){
            PageReference pageRef = Page.CS_conflictcheck_clone;
            pageRef.getParameters().put('id', oppObj.id );
            pageRef.getParameters().put('auth', userloginobj.Auth_Code__c );
            pageRef.getParameters().put('indexnumber','1' );
            Test.setCurrentPage(pageRef);
            PageReference pageRef1 = Page.CS_conflictcheck_clone;
            Test.setCurrentPage(pageRef1);
            cs_conflictCheck_clone controller = new cs_conflictCheck_clone();
            controller.conflict_check_method();
            controller.redirect();
            controller.selected_Type.clear();
            controller.selected_Type.add('Airport Hanger');
            controller.selected_Property = propertyObj.Name;
            controller.radius ='0.25';
            controller.show_All_Jobs = 'Yes';
            controller.Companyfilter = 'Instructing Company';
            controller.selected_Type.add('All');
            controller.conflict_check_method();
            controller.fetchconflict();
            controller.save();
            controller.savemethod();
            controller.createwrapper();
             }
            
            
            
        
  }
   @isTest
  static void testMethod2()
  {
            createData();
            System.runAs(userObj) {
            PageReference pageRef = Page.CS_JobInvoiceViewClone;
            pageRef.getParameters().put('id', oppObj.id );
            pageRef.getParameters().put('indexnumber','1' );
            Test.setCurrentPage(pageRef);

            cs_conflictCheck_clone controller = new cs_conflictCheck_clone();
            controller.conflict_check_method();
            controller.redirect();
            controller.selected_Type.clear();
            controller.selected_Type.add('Airport Hanger');
            controller.selected_Property = propertyObj.Name;
            controller.radius ='0.25';
            controller.show_All_Jobs = 'Yes';
            controller.Companyfilter = 'Instructing Company';
            controller.conflict_check_method();
            controller.fetchconflict();
            controller.save();
            controller.savemethod();
            controller.createwrapper();
            controller.selected_Type.add('All');
            controller.selected_Property = propertyObj.Name;
            controller.radius ='0.25';
            controller.show_All_Jobs = 'Yes';
            controller.Companyfilter = 'Instructing company plus other parties';
            controller.conflict_check_method();
            controller.fetchconflict();
            System.runAs(portalAccountOwner1){
            PageReference pageRef1 = Page.CS_conflictcheck_clone;
            pageRef1.getParameters().put('id', oppObj.id );
            pageRef1.getParameters().put('auth', userloginobj.Auth_Code__c );
            pageRef1.getParameters().put('indexnumber','1' );
            Test.setCurrentPage(pageRef1);
            /*PageReference pageRef1 = Page.CS_conflictcheck_clone;
            Test.setCurrentPage(pageRef1);*/
            cs_conflictCheck_clone controller1 = new cs_conflictCheck_clone();
            controller1.conflict_check_method();
            controller1.redirect();
            controller1.selected_Type.clear();
            controller1.selected_Type.add('Airport Hanger');
            controller1.selected_Property = propertyObj.Name;
            controller1.radius ='0.25';
            controller1.show_All_Jobs = 'Yes';
            controller1.Companyfilter = 'Instructing Company';
            controller1.selected_Type.add('All');
            controller1.conflict_check_method();
            controller1.fetchconflict();
            controller1.save();
            controller1.savemethod();
            controller1.createwrapper();
             }
           }
            
            
        
  }
   @isTest
  static void testMethodbatch(){
        createData();
        Test.startTest();
        BatchSendConflictEmails sendmail= new BatchSendConflictEmails();
        Database.executeBatch(sendmail);

        Test.stopTest(); 
  }
  @isTest
  static void testMethodbatch2(){
        createData();
        Test.startTest();
        BatchSendReminderConflictEmails sendmail2= new BatchSendReminderConflictEmails();
        Database.executeBatch(sendmail2);

        Test.stopTest(); 
  }

}