/**
*  Class Name: Test_CS_WhoKnowsWhoTriggerHandler  
*  Description: This is a Test Class for CS_WhoKnowsWhoTriggerHandler 
*  Company: dQuotient
*  CreatedDate:23/12/2016 
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------  
*  Anand              23/12/2016                 Orginal Version
*
*/
@isTest(seeAllData=false)
private class Test_CS_WhoKnowsWhoTriggerHandler {
    
    static testMethod void testInsertFollowContactForUser(){
        
        User objUser = TestObjectHelper.createAdminUser(true);
        objUser.UserName ='Test_123@dquotient.com';
        insert objUser;
        //System.assert(objUser.id != null);
        
        
        System.runAs(objUser){
        
            Account objAccount = TestObjectHelper.createAccount();
            insert objAccount;
            //System.assert(objAccount.id != null);
            
            Staff__c  objStaff = TestObjectHelper.createStaff();
            objStaff.User__c=objUser.id;
            insert objStaff;
            objStaff.User__c = Userinfo.getUserId();
            //System.assert(objStaff.id != null);
            
            Contact objContact = TestObjectHelper.createContact(objAccount);
            objContact.Email ='test1@test1.com';
            insert objContact;
            System.assert(objContact.id != null);
            
            
            Who_Knows_Who__c objWhoKnowsWho = new Who_Knows_Who__c();
            objWhoKnowsWho.Staff__c = objStaff.id;
            objWhoKnowsWho.Contact__c = objContact.id;
            objWhoKnowsWho.Sync_with_Outlook__c = true;
            
            Who_Knows_Who__c objWhoKnowsWhoNew = new Who_Knows_Who__c();
            objWhoKnowsWhoNew.Staff__c = objStaff.id;
            objWhoKnowsWhoNew.Contact__c = objContact.id;
            objWhoKnowsWhoNew.Sync_with_Outlook__c = true;
            insert objWhoKnowsWho;
            List<EntitySubscription> lstEntitySubscription = new List<EntitySubscription>();
            
            Test.startTest();
            
                
                lstEntitySubscription = [Select id, SubscriberId, ParentId From EntitySubscription Limit 1];
                //System.assertEquals(lstEntitySubscription.size(), 1);
                
                
                //insert objWhoKnowsWhoNew;
                // lstEntitySubscription  =new List<EntitySubscription>();
                // lstEntitySubscription = [Select id, SubscriberId, ParentId From EntitySubscription];
                // System.assertEquals(lstEntitySubscription.size(), 1);
            Test.stopTest();
            
        }
    
    }

    static testMethod void testUpdateFollowContactForUser(){
        
        User objUser = TestObjectHelper.createAdminUser(true);
        objUser.UserName ='Test_123@dquotient.com';
        objUser.Email = 'user1@gmail.com';
        insert objUser;
        System.assert(objUser.id != null);
        
        
        System.runAs(objUser){
        
            Account objAccount = TestObjectHelper.createAccount();
            insert objAccount;
            System.assert(objAccount.id != null);
            
            Staff__c  objStaff = TestObjectHelper.createStaff();
            objStaff.User__c=objUser.id;
            
            insert objStaff;
            System.assert(objStaff.id != null);

           
            
            Contact objContact = TestObjectHelper.createContact(objAccount);
            objContact.Email ='test1@test1.com';
            insert objContact;
            System.assert(objContact.id != null);

            User objUser1 = TestObjectHelper.createAdminUser(true);
            objUser1.UserName ='Test_1234@dquotient.com';
            objUser1.Email = 'user2@gmail.com';
            insert objUser1;
            System.assert(objUser1.id != null);
            
            Staff__c  objStaff1 = TestObjectHelper.createStaff();
            objStaff1.User__c=objUser1.id;
            objStaff1.name='Test';
            objStaff1.Email__c='stvcxasghsgf3sf4423434ff@gmail.com';
            insert objStaff1;
            System.assert(objStaff1.id != null);

            EntitySubscription esObj = new EntitySubscription();
            esObj.parentId = objContact.id;
            esObj.subscriberId = objUser1.id;
            insert esObj;

            Who_Knows_Who__c objWhoKnowsWhoNew = new Who_Knows_Who__c();
            objWhoKnowsWhoNew.Staff__c = objStaff1.id;
            objWhoKnowsWhoNew.Contact__c = objContact.id;
            objWhoKnowsWhoNew.Sync_with_Outlook__c = true;
            insert objWhoKnowsWhoNew;
    
            Who_Knows_Who__c objWhoKnowsWho = new Who_Knows_Who__c();
            objWhoKnowsWho.Staff__c = objStaff.id;
            objWhoKnowsWho.Contact__c = objContact.id;
            objWhoKnowsWho.Sync_with_Outlook__c = true;
            insert objWhoKnowsWho;
            System.assert(objWhoKnowsWho.id!=null);

            objWhoKnowsWho.Sync_with_Outlook__c=false;
            update objWhoKnowsWho;
            objWhoKnowsWho.Sync_with_Outlook__c=true;
            update objWhoKnowsWho;
            
            List<EntitySubscription> lstEntitySubscription = new List<EntitySubscription>();
            
            
                lstEntitySubscription = [Select id, SubscriberId, ParentId From EntitySubscription];
                //System.assertEquals(lstEntitySubscription.size(), 2);
            
            
            
        }
    
    }

    static testMethod void testDeleteUndeleteFollowContactForUser(){
        
        User objUser = TestObjectHelper.createAdminUser(true);
        objUser.UserName ='Test_123@dquotient.com';
        insert objUser;
        System.assert(objUser.id != null);
        
        
        System.runAs(objUser){
        
            Account objAccount = TestObjectHelper.createAccount();
            insert objAccount;
            System.assert(objAccount.id != null);
            
            Staff__c  objStaff = TestObjectHelper.createStaff();
            objStaff.User__c=objUser.id;
            insert objStaff;
            objStaff.User__c = Userinfo.getUserId();
            System.assert(objStaff.id != null);
            
            Contact objContact = TestObjectHelper.createContact(objAccount);
            objContact.Email ='test1@test1.com';
            insert objContact;
            System.assert(objContact.id != null);
            
            Who_Knows_Who__c objWhoKnowsWho = new Who_Knows_Who__c();
            objWhoKnowsWho.Staff__c = objStaff.id;
            objWhoKnowsWho.Contact__c = objContact.id;
            objWhoKnowsWho.Sync_with_Outlook__c = true;
            insert objWhoKnowsWho;
            System.assert(objWhoKnowsWho.id!=null);

            List<EntitySubscription> lstEntitySubscription = new List<EntitySubscription>();
            
            delete objWhoKnowsWho;
            lstEntitySubscription = [Select id, SubscriberId, ParentId From EntitySubscription Limit 1];
            System.assertEquals(lstEntitySubscription.size(), 0);
            
            undelete objWhoKnowsWho;
            lstEntitySubscription = new List<EntitySubscription>();
            lstEntitySubscription = [Select id, SubscriberId, ParentId From EntitySubscription Limit 1];
            //System.assertEquals(lstEntitySubscription.size(), 1);
            
        }
    
    }

}