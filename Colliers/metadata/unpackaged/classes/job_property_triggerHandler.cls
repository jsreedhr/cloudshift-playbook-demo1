/*  Class Name: job_property_triggerHandler 
 *  Description: Trigger handler to create and edit the Job_Property_Junction_Lookup__c object
 *  Company: dQuotient
 *  CreatedDate: 3/3/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nidheesh               3/3/2017                  Orginal Version
 *
 */
public class job_property_triggerHandler{
    public job_property_triggerHandler(){
        
    }
    // creating Job_Property_Junction_Lookup__c after insertion of job_property__c
    public void afterinsertfunction(List<Job_Property_Junction__c> job_property_list){
        List<Job_Property_Junction_Lookup__c> recordToInsert = new List<Job_Property_Junction_Lookup__c>();
        if(!job_property_list.isEmpty()){
            for(Job_Property_Junction__c objins : job_property_list){
                Job_Property_Junction_Lookup__c newobj = new Job_Property_Junction_Lookup__c();
                if(objins.Job__c != null && string.isNotBlank(objins.Job__c)){
                    newobj.Job__c = objins.Job__c;
                }
                if(objins.Property__c != null && string.isNotBlank(objins.Property__c)){
                    newobj.Property__c = objins.Property__c;
                }
                if(objins.External_Id_Custom__c != null && string.isNotBlank(objins.External_Id_Custom__c)){
                    newobj.External_Id_Custom__c = objins.External_Id_Custom__c;
                }
                if(objins.Id != null && string.isNotBlank(objins.Id)){
                    newobj.Job_Property_Id__c = objins.Id;
                }
                recordToInsert.add(newobj);
            }
        }
        if(!recordToInsert.isEmpty()){
            try{
                insert recordToInsert;
            }catch(System.DmlException e){
                
            }
        }
    }
    public void afterdeletefunction(List<Job_Property_Junction__c> old_job_property_list){
        List<Job_Property_Junction_Lookup__c> recordToDelete = new List<Job_Property_Junction_Lookup__c>();
        Set<Id> jobIds = new Set<Id>();
        Set<Id> propIds = new Set<Id>();
        Set<Id> jobpropIds = new Set<Id>();
        Map<Id,Job_Property_Junction__c> JobIdRecord = new Map<Id,Job_Property_Junction__c>();
        if(!old_job_property_list.isEmpty()){
            for(Job_Property_Junction__c objdel: old_job_property_list){
                if(objdel.Job__c != null && string.isNotBlank(objdel.Job__c)){
                    jobIds.add(objdel.Job__c);
                }
                if(objdel.Property__c != null && string.isNotBlank(objdel.Property__c)){
                    propIds.add(objdel.Property__c);
                }
                if(objdel.Id != null && string.isNotBlank(objdel.Id)){
                    jobpropIds.add(objdel.Id);
                }
                
                
            }
        }
        recordToDelete = [SELECT Id,Job__c,Property__c FROM Job_Property_Junction_Lookup__c WHERE Job__c IN :jobIds AND Property__c IN:propIds AND Job_Property_Id__c IN : jobpropIds];
        if(!recordToDelete.isEmpty()){
            try{
                insert recordToDelete;
            }catch(System.DmlException e){
                
            }
        }
    }
    
}