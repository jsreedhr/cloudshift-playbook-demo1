global class ContactRelationBatch implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'select id,name,accountId,account.ultimateParentId__c,(select contactid,AccountId from AccountContactRelations),account.ParentId from contact where account.ParentId != null ';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Contact> scope)
    {   
        map<id,set<id>> mapAcr =new map<id,set<id>>();
        list<accountcontactrelation> acrList =new list<accountcontactrelation>();
        Set<Id> setAccIds = new Set<Id>();
        for (Contact con : scope){
            if(con.AccountId != null && String.isNotBlank(con.AccountId))
            {
                setAccIds.add(con.AccountId);
            }               
            for(AccountContactRelation ac : con.AccountContactRelations){
                if(mapAcr.containskey(ac.contactid)){
                    mapAcr.get(ac.contactid).add(ac.AccountId);
                }
                else{
                    mapAcr.put(ac.contactid, new set<Id>{ac.accountId});
                }
            }
        }
        List<Account> listParentAccounts = [Select Id,ParentId,Parent.ParentId,Parent.Parent.ParentId,Parent.Parent.Parent.ParentId,Parent.Parent.Parent.Parent.ParentId from Account where Id In :setAccIds];
        
        // Create map of Id => parentId and Id => UltimateParentId
        Map<Id,Id> mapOfParentIds = new Map<Id,Id>();
        Map<Id,Id> mapOfUltimateParentIds = new Map<Id,Id>();
        if(!listParentAccounts.isEmpty())
        {
            for(Account acc : listParentAccounts)
            {
                if(acc.ParentId != null && String.isNotBlank(acc.ParentId))
                {
                    mapOfParentIds.put(acc.Id,acc.ParentId);
                }
                if(acc.Parent.Parent.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.Parent.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.Parent.Parent.Parent.ParentId);
                }
                else if(acc.Parent.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.Parent.Parent.ParentId);
                }
                else if(acc.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.Parent.ParentId);
                }
                else if(acc.Parent.ParentId != null && String.isNotBlank(acc.Parent.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.Parent.ParentId);
                }
                else if(acc.ParentId != null && String.isNotBlank(acc.ParentId))
                {
                    mapOfUltimateParentIds.put(acc.id,acc.ParentId);
                }
            }
        }
        
        for(contact con : scope){
        	
            if(mapAcr.containskey(con.id)){
                if(mapOfUltimateParentIds.containskey(con.accountId) && !mapAcr.get(con.id).contains(mapOfUltimateParentIds.get(con.accountId)))
                {	
                    // Creating Account relation to ultimate parent.
                    AccountContactRelation acr = new AccountContactRelation();
                    acr.AccountId = mapOfUltimateParentIds.get(con.accountId);
                    acr.ContactId = con.id;
                    acrList.add(acr);
                    // Add the new ultimate ParentId to Map
                    mapAcr.get(con.id).add(mapOfUltimateParentIds.get(con.accountId));
                    
                }
            
           
                if(con.account.parentid != null && mapOfParentIds.containskey(con.accountId) && !mapAcr.get(con.id).contains(con.account.parentid)){            
                    // Creating Account relation to Parent.
                    AccountContactRelation acr = new AccountContactRelation();
                    acr.AccountId =  con.account.ParentId;
                    acr.ContactId = con.id;
                    acrList.add(acr);
                }
            }
        }
            
            
        

        if(acrList!=NULL && !acrList.isEmpty()){
            insert acrList;
        }
    }  
    global void finish(Database.BatchableContext BC)
    {
        
    }
}