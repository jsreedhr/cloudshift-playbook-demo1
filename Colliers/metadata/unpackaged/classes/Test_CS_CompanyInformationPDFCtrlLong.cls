/**
*  Class Name: Test_CS_CompanyInformationPDFControllerLong  
*  Description: This is a Test Class for CS_CompanyInformationPDFController 
*  Company: dQuotient
*  CreatedDate:29/12/2016 
*
*/
@isTest
private Class Test_CS_CompanyInformationPDFCtrlLong
{
    @isTest 
    static void testController()
    {	User userObj = TestObjectHelper.createAdminUser(true);
        userObj.UserName ='Test_123@dquotient.com';
        //userObj.Email = 'user1@gmail.com';
        insert userObj;
        System.assert(userObj.id != NULL);
     
     
     system.runAs(userObj){
         Account parentAccountObj = TestObjectHelper.createAccount();
        parentAccountObj.name = 'Parent Account';
        insert parentAccountObj;
        System.assert(parentAccountObj.id!=NULL);

        Account parentAccountObj1 = TestObjectHelper.createAccount();
        parentAccountObj1.name = 'Parent Account1';
        insert parentAccountObj1;
        System.assert(parentAccountObj1.id!=NULL);

        Account accountObj1 = TestObjectHelper.createAccount();
        accountObj1.parentId = parentAccountObj1.id;
        insert accountObj1;
        System.assert(accountObj1.id!=NULL);
        
         Account accountObj = TestObjectHelper.createAccount();
        accountObj.parentId = parentAccountObj.id;
        insert accountObj;
        System.assert(accountObj.id!=NULL);
        
       Property__c  propertyObj=TestObjectHelper.createProperty();
       propertyObj.Suite_Unit__c = '11';
       propertyObj.Floor_No__c = '5th';
       propertyObj.Building_Name__c = 'testbuild';
      propertyObj.Estate__c = 'testesta';
       propertyObj.Street__c = 'Teststreet';
       propertyObj.Area__c = 'TestArea';
       propertyObj.Town__c = 'testTown';
        propertyObj.Street_No__c ='Park Lane';
        propertyObj.Post_Code__c ='W1K 3DD';
        propertyObj.Country__c='United Kingdom';
        propertyObj.Geolocation__Latitude__s=51.51;
        propertyObj.Geolocation__Longitude__s=-0.15;
        insert propertyObj;

        Opportunity opportunityObj = TestObjectHelper.createOpportunity(accountObj);
        opportunityObj.StageName = 'Instructed';
        opportunityObj.CloseDate = system.today()-10;
        insert opportunityObj;
        
        System.assert(opportunityObj.id!=NULL);
        
        Job_Property_Junction__c jobPropObj = TestObjecthelper.JobPrptyJn(opportunityObj,propertyObj);
        insert jobPropObj;
        
        Opportunity opportunityObj1 = TestObjectHelper.createOpportunity(accountObj1);
        insert opportunityObj1;
        System.assert(opportunityObj1.id!=NULL);
        
        Job_Property_Junction__c  jobPropObj1 = TestObjecthelper.JobPrptyJn(opportunityObj1,propertyObj);
        insert jobPropObj1;

        Contact contactObj = TestObjectHelper.createContact(accountObj);
        contactObj.Email ='test1@test1.com';
		ContactTriggerHandler.isAfterInsert=false;
         
        insert contactObj;
        System.assert(contactObj.id!=NULL);

        Staff__c  staffObj = TestObjectHelper.createStaff();
        staffObj.User__c = userObj.id;
        staffObj.Department__c = 'Rating';
        staffObj.Active__c = true;
        insert staffObj;
        System.assert(staffObj.id != NULL);
     
     	User userObj1 = TestObjectHelper.createAdminUser(true);
        userObj1.UserName ='Test_1234@dquotient.com';
        userObj1.Email = 'user2@gmail.com';
        insert userObj1;
        System.assert(userObj1.id != NULL);
     
        Staff__c  staffObj1 = TestObjectHelper.createStaff();
        staffObj1.User__c = userObj1.id;
        staffObj1.Email__c = '1234@gmail.com';
        staffObj1.Active__c = true;
        insert staffObj1;
        System.assert(staffObj1.id != NULL);

        opportunityObj.Manager__c=staffObj.id;
        update opportunityObj;

        Opportunity opportunityObj3 = TestObjectHelper.createOpportunity(accountObj);
        opportunityObj3.Manager__c=staffObj.id;
        insert opportunityObj3;
        System.assert(opportunityObj3.id!=NULL);

        Opportunity opportunityObj4 = TestObjectHelper.createOpportunity(accountObj);
        opportunityObj4.Manager__c=staffObj1.id;
        insert opportunityObj4;
        System.assert(opportunityObj4.id!=NULL);

        Who_Knows_Who__c whoKnowsWhoObj = new Who_Knows_Who__c();
        whoKnowsWhoObj.Staff__c = staffObj.id;
        whoKnowsWhoObj.Contact__c = contactObj.id;
        insert whoKnowsWhoObj;
        System.assert(whoKnowsWhoObj.id!=NULL);

        Event eventObj = TestObjectHelper.createEvent(contactObj);
        insert eventObj;
        System.assert(eventObj!=NULL);
     
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(accountObj);
            CS_CompanyInformationPDFController_Long con = new CS_CompanyInformationPDFController_Long(sc);
        Test.stopTest(); 
     
         
     }	
        
        
        
    }

}