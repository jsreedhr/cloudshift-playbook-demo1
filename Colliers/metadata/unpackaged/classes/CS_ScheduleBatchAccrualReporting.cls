global class CS_ScheduleBatchAccrualReporting implements Schedulable  {
    global void execute(SchedulableContext SC) {
        CS_BatchAccrualReporting objBatch = new CS_BatchAccrualReporting();
        database.executeBatch(objBatch, 200);
    }
}