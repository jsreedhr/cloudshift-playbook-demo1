/**
 *  Class Name: CS_BatchAccrualReporting
 *  Description: Class to update the fields of Fee Income Staging Table with Accural
 *  Company: dQuotient
 *  CreatedDate:24-10-2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Anand             23-03-2016                 Orginal Version
 * 
 */
global class CS_BatchAccrualReporting implements Database.Batchable<sObject>, Database.Stateful{
    
    
    public CS_BatchAccrualReporting(){
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){

        String query = 'SELECT id,name, Job__c, Job__r.Manager__c, Reason__c, Status__c, Job__r.Invoicing_Company2__c, Job__r.Invoicing_Care_Of__c,Job__r.AccountId, Job__r.Invoicing_Care_Of__r.Reporting_Client__c, (Select id, name,  Accrual__c, Amount__c,Forecasting__c, Forecasting__r.Allocation__c,  Forecasting__r.Allocation__r.externalCompany__c, Forecasting__r.Allocation__r.Assigned_To__c, Staging_Table_Record__c From Accrual_Forecasting_Junction__r Limit 50000) from Accrual__c where (Status__c = \'Accrual - Reversal\' OR  Status__c =  \'Approved\' OR  Status__c =  \'Reversed By Invoice\'  OR  Status__c =  \'Reversed By User\' OR  Status__c =  \'Reversed By User JM\')  and No_of_Records_with_No_Fee_Income__c > 0 order by CreatedDate asc limit 50000000';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<Accrual__c> scope){
        
        List<Fee_Income_Staging_Table__c> lstFeeIncomeStagingTable = new List<Fee_Income_Staging_Table__c>();
        set<id> setForecastAccuralIds = new set<id>();
        List<Fee_Income_Staging_Table__c> lstFeeIncomeStagingTableExisting = new List<Fee_Income_Staging_Table__c>();
        Map<id,Fee_Income_Staging_Table__c> mapIdFeeIncomeStagingTableExisting = new Map<id, Fee_Income_Staging_Table__c>();
        
        // Population of the Client
        
        
        for(Accrual__c objAccural : scope){
            // Calculation of Net Amounts
            if(!objAccural.Accrual_Forecasting_Junction__r.isEmpty()){
                for(Accrual_Forecasting_Junction__c objAccuralForecasting : objAccural.Accrual_Forecasting_Junction__r){
                    setForecastAccuralIds.add(objAccuralForecasting.id);
                    
                }
                
            }
        }
        
        lstFeeIncomeStagingTableExisting = [Select id, Accrual_Forecasting_Junction__c
                                                    From
                                                    Fee_Income_Staging_Table__c
                                                    where 
                                                    Accrual_Forecasting_Junction__c in : setForecastAccuralIds
                                                    and (Type__c = 'Accrual-Reversal' OR Type__c = 'Accrual')
                                                    and Accrual_Forecasting_Junction__c != null];
        if(!lstFeeIncomeStagingTableExisting.isEmpty()){                                            
            for(Fee_Income_Staging_Table__c objFeeIncomeStagingTable : lstFeeIncomeStagingTableExisting){
                mapIdFeeIncomeStagingTableExisting.put(objFeeIncomeStagingTable.Accrual_Forecasting_Junction__c, objFeeIncomeStagingTable);
            }
        }
        Set<Id> opportunityAccountIdSet = new Set<Id>();    
        for(Accrual__c objAccural : scope){
            
            id idJM = null;
            id idInvoicingCompany = null;
            id opportunityAccountId = null;
            String reasonAccural = '';
            if(objAccural.Job__r.Manager__c != null){
                idJM = objAccural.Job__r.Manager__c;
            }
            if(!String.isBlank(objAccural.Reason__c)){
                reasonAccural = objAccural.Reason__c;
            }
            if(objAccural.Job__r.Invoicing_Company2__c != null){
                idInvoicingCompany = objAccural.Job__r.Invoicing_Company2__c;
            }else if(objAccural.Job__r.Invoicing_Care_Of__c != null && objAccural.Job__r.Invoicing_Care_Of__r.Reporting_Client__c != null){
                idInvoicingCompany = objAccural.Job__r.Invoicing_Care_Of__r.Reporting_Client__c;
                
            }
            if(objAccural.Job__r.AccountId != null){
                opportunityAccountId = objAccural.Job__r.AccountId;
                if(!opportunityAccountIdSet.contains(opportunityAccountId)){
                    opportunityAccountIdSet.add(opportunityAccountId);
                }
            }
            
            if(!objAccural.Accrual_Forecasting_Junction__r.isEmpty()){
                System.debug('-----Size-----'+objAccural.Accrual_Forecasting_Junction__r.size());
                for(Accrual_Forecasting_Junction__c objAccuralForecasting : objAccural.Accrual_Forecasting_Junction__r){
                    System.debug('-----MapCheck-----'+mapIdFeeIncomeStagingTableExisting.containsKey(objAccuralForecasting.id));
                    System.debug('-----BooleanCheck-----'+objAccuralForecasting.Staging_Table_Record__c);
                    System.debug('-----id-----'+objAccuralForecasting.id);
                    if(!(objAccuralForecasting.Staging_Table_Record__c) && !mapIdFeeIncomeStagingTableExisting.containsKey(objAccuralForecasting.id)){
                    
                        Fee_Income_Staging_Table__c objFeeIncomeStagingTable = new Fee_Income_Staging_Table__c();
                        
                        // Fee Income Master Details
                        objFeeIncomeStagingTable.Allocation__c = objAccuralForecasting.Forecasting__r.Allocation__c;
                        objFeeIncomeStagingTable.Client__c = idInvoicingCompany;
                        objFeeIncomeStagingTable.Forecasting__c =  objAccuralForecasting.Forecasting__c;
                        objFeeIncomeStagingTable.Accrual__c = objAccuralForecasting.Accrual__c;
                        objFeeIncomeStagingTable.Job_Manager__c = idJM;
                        objFeeIncomeStagingTable.Notes__c = reasonAccural;
                        //Value for the Instructing Client
                        objFeeIncomeStagingTable.Instructing_Client__c = opportunityAccountId;
                        
                        // String statusAccural = objAccural.Status__c.deleteWhitespace();
                        if(objAccural.Status__c.equalsIgnoreCase('Accrual - Reversal')){
                            objFeeIncomeStagingTable.Type__c = 'Accrual-Reversal';
                        }else{
                            objFeeIncomeStagingTable.Type__c = 'Accrual';
                        }
                        objFeeIncomeStagingTable.Accrual_Forecasting_Junction__c = objAccuralForecasting.id;
                        
                        // Amount Calculations
                        decimal amountTemp = 0.0;
                        if(objAccuralForecasting.Amount__c != null){
                            amountTemp = objAccuralForecasting.Amount__c;
                        }
                        objFeeIncomeStagingTable.Allocation_Net__c = amountTemp;
                        objFeeIncomeStagingTable.Less_Sub_Contractor__c = 0.0;
                        objFeeIncomeStagingTable.Net_Fees__c = 0.0;
                        objFeeIncomeStagingTable.Job_Manager_Allocation__c = 0.0;
                        objFeeIncomeStagingTable.Other_Allocations__c = 0.0;
                        
                        if(objAccuralForecasting.Forecasting__r.Allocation__r.externalCompany__c != null){
                            objFeeIncomeStagingTable.External_Company__c = objAccuralForecasting.Forecasting__r.Allocation__r.externalCompany__c;
                        }else if(objAccuralForecasting.Forecasting__r.Allocation__r.Assigned_To__c != null){
                            objFeeIncomeStagingTable.Fee_Allocated_To__c = objAccuralForecasting.Forecasting__r.Allocation__r.Assigned_To__c;
                        }
                        
                        
                        lstFeeIncomeStagingTable.add(objFeeIncomeStagingTable);
                    }
                }
                
            }
        }
        
        System.debug('-----Insert-----'+lstFeeIncomeStagingTable);
        // Adding instructing client fields
        Map<Id, Account> accountMap = new Map<Id, Account>([Select Id, Ultimate_Parent_Name__c, Ultimate_Parent__c, Company_Classification__c, Geographical_Client__c, Ultimate_Parent_Sector__c, Ultimate_Parent_Sub_Sector__c From Account Where Id IN :opportunityAccountIdSet]);
        // Adding Ultimate parent values
        if(!lstFeeIncomeStagingTable.isEmpty()){
            for(Fee_Income_Staging_Table__c objFeeIncomeStagingTable :lstFeeIncomeStagingTable)
            {
                if(objFeeIncomeStagingTable.Instructing_Client__c != null)
                {
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Name__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Name__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Name__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent__c != null) {
                        String textString= accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent__c;
                        Integer int1 = textString.indexOf('/');
                        objFeeIncomeStagingTable.Ultimate_Instructing_ParentId__c = textString.substring(int1 + 1,int1 + 16);
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sector__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Sector__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sector__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Geographical_Client__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Geo_Class__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Geographical_Client__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sub_Sector__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Sub_Sector__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sub_Sector__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c = null;
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Developer' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Developer,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Lender' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Lender,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Occupier' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Occupier,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Owner' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Owner,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Service Provider' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Service Provider';
                        }
                        
                    }
                }
            }
        }
        if(!lstFeeIncomeStagingTable.isEmpty()){
            System.debug('-----inside-----'+lstFeeIncomeStagingTable);
            Database.SaveResult[] lstDatabaseSaveResultInsert = Database.insert(lstFeeIncomeStagingTable, false);
            
            integer index = 0;
            List<Accrual_Forecasting_Junction__c> lstAccuralForecasting = new List<Accrual_Forecasting_Junction__c>();
            for (Database.SaveResult objDatabaseSaveResult : lstDatabaseSaveResultInsert) {
                System.debug('Error---' + objDatabaseSaveResult.isSuccess());
                for(Database.Error err : objDatabaseSaveResult.getErrors()){
                    System.debug('Error---' + err.getMessage());
                }
                
                if (objDatabaseSaveResult.isSuccess() && lstFeeIncomeStagingTable[index] != null 
                    && lstFeeIncomeStagingTable[index].Accrual_Forecasting_Junction__c != null) {
                    
                    Accrual_Forecasting_Junction__c objAccuralForecasting = new Accrual_Forecasting_Junction__c();
                    objAccuralForecasting.id = lstFeeIncomeStagingTable[index].Accrual_Forecasting_Junction__c ;
                    objAccuralForecasting.Staging_Table_Record__c = true;
                    lstAccuralForecasting.add(objAccuralForecasting);
                }
                
                index = index+1;
            }
            
            if(!lstAccuralForecasting.isEmpty()){
                Database.SaveResult[] lstDatabaseSaveResultUpdate = Database.update(lstAccuralForecasting, false);
            }
        }
        
        
    }
    global void finish(Database.BatchableContext BC){
        
    }
    
}