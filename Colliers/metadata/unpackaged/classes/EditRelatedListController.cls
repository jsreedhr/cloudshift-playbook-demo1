/**
 *  Class Name: EditRelatedListController 
 *  Description: This class is a controller for EditRelatedList Page
 *  Company: dQuotient
 *  CreatedDate: 25/08/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Anand(P)            25/08/2016                  Orginal Version
 *
 */
public class EditRelatedListController  {
    
    //Class level variables
    // Related List query
    public String query;
    // parent object id
    public String recordId;
    // parent object Label
    public String parentObjectLabel{get;set;}
     // child object Label
    public String childObjectLabel{get;set;}
     // child object Label
    public String childObjectPluralLabel{get;set;}
    // parent object Record Name
    public String parentObjectRecName{get;set;}
    // Api name of related List
    public String sObjectName{get;set;}
    // parent Sobject
    public sObject pObject{get;set;}
    // object field list
    public List<String> fields{get;set;}
    // Map of Fields and Label
    public map<String,String> mapFieldLabels{get;set;}
    //Map for required field mapping
    public map<String,boolean> mapFieldRequired{get;set;}
    public map<String,boolean> mapFieldLookUp{get;set;}
    public String childRecordTypeId;
    public static map<String,string> mapLookUpObject{get;set;}
    
    public List<SObject> lstSObjectRL {get;set;}
    public List<rlWrapper> rlRecs{get;set;}
    public String rlName{get;set;}
    public String parentObjectField{get;set;}
    public set<String> ChildFieldSetFields{get;set;}
    public String parentRecordNameFromId{get;set;}
    public List<FieldWrapperClass> fieldDetails{get;set;}
    public Map<String,FieldWrapperClass> mapFieldDetails{get;set;}
   
    
    public class rlWrapper{
        public string index{get;set;}
        public sObject objRec{get;set;}
        public rlWrapper(Integer i, sObject s){
            this.index = String.valueof(i);
            this.objRec = s;
        }
    }
    /**
    *   Method Name: initializeVariables 
    *   Description: This is a method for initializing class level variables
    *   Param: None
    *   Return: None
    */
    public void initializeVariables(){
        recordId ='';
        query = '';
        sObjectName = '';
        rlName = '';
        childObjectLabel ='';
        childObjectPluralLabel ='';
        parentObjectField ='';
        childRecordTypeId='';
        fields = new List<String>();
        mapFieldLabels = new Map<String,String>();
        mapFieldRequired = new Map<string,boolean>();
        mapFieldLookUp = new Map<string,boolean>();
        mapLookUpObject=new Map<String,string>();
        fieldDetails=new list<FieldWrapperClass>();
        lstSObjectRL = new List<SObject>();
        rlRecs = new List<rlWrapper>();
        ChildFieldSetFields=new set<String>(); 
        parentRecordNameFromId='';
        mapFieldDetails=new Map<String,FieldWrapperClass>();
    }
    
    /**
    *   Method Name: EditRelatedListController 
    *   Description: This is a method for constructor for EditRelatedListController class
    *   Param: None
    *   Return: None
    */
    
    public EditRelatedListController(){
        //initialize clas ariables
        initializeVariables();
        if(ApexPages.currentPage().getParameters().containsKey('id')){
            //get parent record id
            recordId = ApexPages.currentPage().getParameters().get('id');
            if(!String.isBlank(recordId)){
                if(recordId InstanceOf Id){
                    if(ApexPages.currentPage().getParameters().containsKey('rl')){
                        //get related list object name
                        sObjectName = ApexPages.currentPage().getParameters().get('rl');
                        if((sObjectName)!=null){
                            if(Schema.getGlobalDescribe().containsKey(sObjectName)){
                                //get parent sObject Type
                                Schema.SObjectType parentSobjectType = Id.valueOf(recordId).getSObjectType();
                                if(parentSobjectType!=null){
                                    //get parent obj api name
                                    String parentObjectName = parentSobjectType.getDescribe().getName();
                                    Schema.DescribeSObjectResult parentDeSchema = parentSobjectType.getDescribe();
                                    String parentRecQuery='SELECT RecordTypeId,Recordtype.Name From '+parentObjectName+' Where Id=:recordId LIMIT 1';
                                    sObject parentRec= Database.query(parentRecQuery);
                                    Map<ID,Schema.RecordTypeInfo> rt_Map = parentDeSchema.getRecordTypeInfosById();
                                    Schema.RecordTypeInfo rtById =  rt_Map.get((ID)parentRec.get('RecordTypeId'));                                  
                                    parentObjectLabel = parentSobjectType.getDescribe().getLabel();
                                    //get related list object details 
                                    Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
                                    Schema.SobjectType oType = gd.get(sObjectName);
                                    childObjectLabel = oType.getDescribe().getLabel();
                                    childObjectPluralLabel = oType.getDescribe().getLabelPlural();
                                    
                                    Schema.ChildRelationship chdRel = UtilClass.findChildRelationship(parentSobjectType,oType);
                                    parentObjectField = chdRel.getField().getDescribe().getName();
                                    if(oType != null){
                                        //get field list for related lsi object 
                                        if(oType.getDescribe().isAccessible() && oType.getDescribe().isQueryable() && 
                                           oType.getDescribe().isCreateable() && oType.getDescribe().isUpdateable() &&
                                           oType.getDescribe().isDeletable()){
                                           Set <String> fieldSetFieldNames = new Set<String>();
                                           if(rtById!=null){
                                                parentRecordNameFromId=rtById.getName();
                                                System.debug('---->recordtypeName$$$$$$$$'+parentRecordNameFromId);
                                                string relchildquery='Select Id,SobjectType,Name From RecordType WHERE Name =:parentRecordNameFromId and SobjectType =:sObjectName limit 1';
                                                RecordType recordQuerObj=new RecordType();
                                                Schema.DescribeSObjectResult DescribeSObjectChildObj = oType.getDescribe();
                                                if(parentRecordNameFromId!=null){
                                                if(DescribeSObjectChildObj.getRecordTypeInfosByName().get(parentRecordNameFromId)!=null){
                                                Id childRecId=DescribeSObjectChildObj.getRecordTypeInfosByName().get(parentRecordNameFromId).getRecordTypeId();                                                         
                                                childRecordTypeId=String.valueOf(childRecId);
                                                }
                                                }   
                                            List<ObjectFieldSetMapping__c> objCustomChldRecFld=new List<ObjectFieldSetMapping__c>();
                                            List<ObjectFieldSetMapping__c> objCustomChldFld=new List<ObjectFieldSetMapping__c>();
                                            List<ObjectFieldSetMapping__c> objCustom=new List<ObjectFieldSetMapping__c>();
                                            if(parentRecordNameFromId!=null){
                                                system.debug('!!!!parentobjectname '+parentObjectName);
                                                system.debug('!!!!parentobjectname '+sObjectName);
                                                system.debug('!!!!parentobjectname '+parentRecordNameFromId);
                                                objCustomChldRecFld=[SELECT Field_Set_Name__c FROM ObjectFieldSetMapping__c WHERE (Object_API_Name__c= :sObjectName) AND (ParentObjectAPIName__c=:parentObjectName)AND (ParentRecordType__c=:parentRecordNameFromId )];
                                            }else{
                                                objCustomChldFld=[SELECT Field_Set_Name__c FROM ObjectFieldSetMapping__c WHERE (Object_API_Name__c= :sObjectName) AND (ParentObjectAPIName__c=:parentObjectName)AND (ParentRecordType__c=null )];
                                            }
                                            if((!objCustomChldRecFld.isEmpty())||(!objCustomChldFld.isEmpty())){
                                                if(!objCustomChldRecFld.isEmpty()){
                                                    objCustom=objCustomChldRecFld;
                                                }
                                                else{
                                                    objCustom=objCustomChldFld;
                                                }
                                                String fieldsetname=((String)objCustom[0].get('Field_Set_Name__c'));
                                                Schema.FieldSet fieldSetObj = oType.getDescribe().FieldSets.getMap().get(fieldsetname);
                                                List<Schema.FieldSetMember> fieldSetFields = new List<Schema.FieldSetMember>();
                                                if(fieldSetObj != null){
                                                    fieldSetFields = fieldSetObj.getFields();
                                                }
                                                if(!fieldSetFields.isEmpty()){
                                                    for(Schema.FieldSetMember f :  fieldSetFields) {
                                                        String fieldName=f.getFieldPath();
                                                        String fieldLabel=f.getLabel();
                                                        ChildFieldSetFields.add(fieldName);
                                                        mapFieldRequired.put(fieldName,f.getRequired());
                                                        if(f.getDBRequired()== true){
                                                             mapFieldRequired.put(fieldName,f.getDBRequired());
                                                        }
                                                    }
                                                }
                                            }
                                            Map<String,string> dummychild1=new Map<String,string>();
                                            Map<String,string> dummychild2=new Map<String,string>();
                                            List<Schema.SObjectField> lstRLFields = new List<Schema.SObjectField>();
                                            lstRLFields = UtilClass.findRequiredFields(oType);
                                            system.debug('---->fieldList'+lstRLFields);
                                            
                                            if(!lstRLFields.isEmpty()){
                                                //construct query for getting records of related list
                                                String query = 'Select ';
                                               
                                                for(Schema.SObjectField objSObjectField : lstRLFields){
                                                    if(objSObjectField.getDescribe().isAccessible()){  
                                                        System.debug('---->appu'+objSObjectField.getDescribe().isIdLookup());
                                                        String fieldName = objSObjectField.getDescribe().getName();
                                                        String fieldLabel = objSObjectField.getDescribe().getLabel();
                                                        if(objSObjectField.getDescribe().getReferenceTo()!=null && objSObjectField.getDescribe().getReferenceTo().size()>0 && objSObjectField.getDescribe().isNamePointing()== false){
                                                            Boolean lookUp=true;
                                                            mapFieldLookUp.put(fieldName,lookUp);
                                                            if(objSObjectField.getDescribe().isNamePointing()== false){
                                                                List<schema.SObjectType> lstofSobject =  objSObjectField.getDescribe().getReferenceTo();
                                                                
                                                                
                                                                mapLookUpObject.put(fieldName,lstofSobject[0].getDescribe().getName());
                                                            }
                                                        }
                                                        else{
                                                            Boolean lookUp=false;
                                                            mapFieldLookUp.put(fieldName,lookUp);                                                           
                                                        }
                                                        dummychild1.put(fieldName,fieldLabel);
                                                        if(objSObjectField.getDescribe().isNillable()==true){
                                                            boolean required = false;
                                                            if(!mapFieldRequired.containsKey(fieldName)){
                                                                mapFieldRequired.put(fieldName,required);
                                                            }
                                                        }else{
                                                            boolean required = true;
                                                            if(!mapFieldRequired.containsKey(fieldName)){
                                                                mapFieldRequired.put(fieldName,required);
                                                            }
                                                                                
                                                        }
                                                         
                                                        //check if related list object has a assigned field set
                                                        if(objSObjectField.getDescribe().isCreateable()|| objSObjectField.getDescribe().isUpdateable()){
                                                                dummychild2.put(fieldName,fieldLabel);
                                                        }
                                                    }
                                                }
                                                if(!ChildFieldSetFields.isEmpty()){
                                                    for(String childflds:ChildFieldSetFields){
                                                        if(dummychild1.containsKey(childflds)){
                                                            query+=childflds+',';
                                                            if(dummychild2.containsKey(childflds)){
                                                                if(childflds!=parentObjectField && !childflds.equalsIgnoreCase('OwnerId')){
                                                                    //setFieldNames.add(childflds);
                                                                    fields.add(childflds);
                                                                    mapFieldLabels.put(childflds,dummychild1.get(childflds));
                                                                    mapFieldDetails.put(childflds,new FieldWrapperClass(childflds,mapFieldRequired.get(childflds),mapFieldLookUp.get(childflds)));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }else{
                                                    for(String dumyy:dummychild1.keySet()){
                                                        query+=dumyy+',';
                                                        if(dummychild2.containsKey(dumyy)){
                                                            if(dumyy!=parentObjectField && !dumyy.equalsIgnoreCase('OwnerId')){
                                                                fields.add(dumyy);
                                                                mapFieldLabels.put(dumyy,dummychild2.get(dumyy) );
                                                                fieldDetails.add(new FieldWrapperClass(dumyy,mapFieldRequired.get(dumyy),mapFieldLookUp.get(dumyy) ));
                                                                mapFieldDetails.put(dumyy,new FieldWrapperClass(dumyy,mapFieldRequired.get(dumyy),mapFieldLookUp.get(dumyy)));
                                                            }
                                                        }
                                                    }
                                               }
                                               mapFieldLabels.remove(parentObjectField);
                                               mapFieldDetails.remove(parentObjectField);
                                                query = query.substringBeforeLast(',');
                                                query += ' From '+sObjectName+' where '+parentObjectField+' =:recordId';
                                                System.debug('query=====>'+query);
                                                //rlQuery = query;
                                                //query related list records 
                                                lstSObjectRL = Database.query(query);
                                                System.debug('lstSObjectRL=====>'+mapLookUpObject);
                                                Integer index = 0;
                                                for(sObject s: lstSObjectRL){
                                                    rlWrapper r = new rlWrapper(index,s);
                                                    rlRecs.add(r);
                                                    index ++;
                                                }
                                                System.debug('rlRecs=====>'+rlRecs);
                                            }
                                                       
                                           }
                                    }                                                                       
                                    else{
                                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'InvalidParamterError'));
                                    }
                                }else{
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'InvalidParamterError'));
                                }
                            }else{
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'InvalidParamterError'));
                            }
                        }else{
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'InvalidParamterError'));
                        }
                    }else{
                         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'InvalidParamterError'));
                    }
                }else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'InvalidParamterError'));
                }
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'InvalidParamterError'));
            }
        }else{
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'InvalidParamterError'));
        }
    }
    }
    
    /**
    *   Method Name: save 
    *   Description: This is a method for saving the records
    *   Param: None
    *   Return: None
    */
    public PageReference save(){
        List<sObject> lstRecordToInsert =  new List<sObject>();
        List<sObject> lstRecordToUpdate =  new List<sObject>();  
        if(!rlRecs.isEmpty()){
            for(rlWrapper r: rlRecs){
                if(r.objRec.get('id') != null){
                    lstRecordToUpdate.add(r.ObjRec);    
                }
                else{
                    lstRecordToInsert.add(r.ObjRec);
                }
            }
            try{
                if(!lstRecordToUpdate.isEmpty()){
                    update lstRecordToUpdate;
                }
                if(!lstRecordToInsert.isEmpty()){
                    insert lstRecordToInsert;
                }
                PageReference pRef = new PageReference('/'+recordid);
                pref.setRedirect(true);
                return pref;
            }
            catch(Exception e){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
            }
        }else{
            PageReference pRef = new PageReference('/'+recordid);
            pref.setRedirect(true);
            return pref;
        }    
        return null;
    }
    
    /**
    *   Method Name: addRow 
    *   Description: This is a method for adding new related list records
    *   Param: None
    *   Return: None
    */
    public void addRow(){
        try{
            Schema.SObjectType t  = Schema.getGlobalDescribe().get(sObjectName);
            sObject chldsObj = t.newSObject();
            if(childRecordTypeId!=null && string.isNotBlank(childRecordTypeId)){
                    chldsObj.put('RecordTypeId',childRecordTypeId);
                }
            chldsObj.put(parentObjectField, recordId);
            Integer indexValue;
            if(!rlRecs.isEmpty()){
                Integer lastIndex;
                lastIndex = rlRecs.size()-1;
                indexValue = Integer.valueof(rlRecs[lastIndex].index);
            }
            else{
                indexValue = -1;
            }
            rlWrapper r = new rlWrapper(indexValue+1,chldsObj);
            rlRecs.add(r);
        }
        catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));    
        }
    }
    
    public PageReference cancel(){
       PageReference OpporunityPageCancel = new PageReference('/'+recordId);
       OpporunityPageCancel.setRedirect(true);
       return OpporunityPageCancel; 
    }
    
    /**
    *   Method Name: deleteRow 
    *   Description: This is a method for deleteing Related list record
    *   Param: None
    *   Return: None
    */
    public void deleteRow(){
        
        String delId = ApexPages.currentPage().getParameters().get('deletedID');
        System.debug('delId---->'+delId);
        Integer index = 0;
        sObject s ;
        Boolean hasRec = false;
        for(rlWrapper r: rlRecs){
            if(r.index == delId){
                s = r.objRec;
                hasRec = true;
                break;
            }
            index++;
        }
        if(hasRec){
            rlRecs.remove(index);
            if(s.get('id') != null){
                try{
                delete s;
                }
                catch(Exception e){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));    
                }
            }
        }
        
    }
   
    public class FieldWrapperClass {
        public string fieldName{get;set;}
        public Boolean isRequired {get;set;}
        public Boolean isLookUp{get;set;}
        public FieldWrapperClass(string fieldname2,Boolean isRequired2,Boolean isLookUp2) {
            fieldName = fieldname2;
            isRequired=isRequired2;
            isLookUp=isLookUp2;         
        }
        
    }
    

}