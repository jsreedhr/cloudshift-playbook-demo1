public  with sharing class  ContanctRetrievalController {
    
   

    // account and selection set/variables
    private list<Contact> AccountList {get; set;}
    private set<Id> AccountSelectedSet;
    Set<id> contactidset = new Set<id> ();
    public String SelectedOneAccount {get; set;}
    
    // selection and filter 
    public list<String> AlphaList {get; set;}
    public String AlphaFilter {get; set;}
    public String SearchName {get; set;}
   
    private String SaveSearchName;
  
    private String QueryAccount;
    
    // display sort and number
    public String RecPerPage {get; set;}
    public list<SelectOption> RecPerPageOption {get; set;}  
    public String SortFieldSave;
    public  String searchparam{get; set;}  
    public String accntid{get;set;}
    /***
    * TableExampleController - Constructor initialization
    ***/
    public ContanctRetrievalController(){
        AccountList = new list<Contact>();
        AccountSelectedSet = new set<Id>();
        contactidset =  new set<Id>();
        //records for page initialization
        RecPerPageOption = new list<SelectOption>();
        RecPerPageOption.add(new SelectOption('10','10'));
        RecPerPageOption.add(new SelectOption('25','25'));
        RecPerPageOption.add(new SelectOption('50','50'));
        RecPerPageOption.add(new SelectOption('100','100'));
        RecPerPageOption.add(new SelectOption('200','200'));
        RecPerPage = '10'; //default records per page
        
        // initialization alpha list
        AlphaList = new list<String> {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Other', 'All'};
            SortFieldSave = SortField;
        
        // alpha filter, use page parameter or set default to all
        if (apexpages.currentpage().getparameters().get('alpha') == null) {
            AlphaFilter = 'All';
        } else {
            AlphaFilter = apexpages.currentpage().getparameters().get('alpha');
        }
        
        // list generation
        BuildQuery();  
    }
    
    /***
    * StdSetControllerAccount - paging through the Account list
    ***/ 
    public ApexPages.StandardSetController StdSetControllerAccount {
        get {
            if(StdSetControllerAccount == null) {
                BuildQuery();
                system.debug('----'+QueryAccount);
                if(!contactidset.isEMpty()){
                    StdSetControllerAccount = new ApexPages.StandardSetController(Database.getQueryLocator(QueryAccount));
                // sets the number of records in each page set
                    StdSetControllerAccount.setPageSize(Integer.valueOf(RecPerPage));
                }else{
                    
                    list<Contact> lstEmptyContacts = new list<Contact>();
                    StdSetControllerAccount = new ApexPages.StandardSetController(lstEmptyContacts);
                    StdSetControllerAccount.setPageSize(Integer.valueOf(RecPerPage));
                }
            }
            return StdSetControllerAccount;
        }
        set;
    }
    
    /***
    * getCurrentAccountList - return an Account list for the table
    ***/  
    public list<Contact> getCurrentAccountList() {
       
        
        AccountList = new list<Contact>();
        for (Contact a : (list<Contact>)StdSetControllerAccount.getRecords()) {
            AccountList.add(a);
        }
        return AccountList;
    }
    
   
   
    
    /***
    * SearchAccount - set search criteria fields and refresh Account table
    ***/
    public PageReference SearchAccount() {
        SaveSearchName = SearchName;
       
        
        BuildQuery();
        
        return null;
    }
    
    /***
    * BuildQuery - build query command for list selection change
    ***/
    public void BuildQuery() {
        StdSetControllerAccount = null;
        String QueryWhere = '';
       
           accntid = '';
       if (ApexPages.currentPage().getParameters().containsKey('id')) {
                accntid = ApexPages.currentPage().getParameters().get('id');
                 searchparam  = ApexPages.currentPage().getParameters().get('searchbox');
                if(searchparam=='instructingcnt')
              
             { List<AccountContactRelation> listContactRelation = [SELECT id,Contact.Name,Contact.FirstName, Contact.LastName, ContactId,Account.Id from AccountContactRelation where Account.Id=:accntid LIMIT 9999];
           
            for(AccountContactRelation acc : listContactRelation )
            {
                contactidset.add(acc.ContactId);
                
            }
             }
             else 
             {
                  String isCareof  = ApexPages.currentPage().getParameters().get('isCareof');
                   accntid = ApexPages.currentPage().getParameters().get('id');
                 if(isCareof=='No')
                 {
                       List<AccountContactRelation> listContactRelation = new List<AccountContactRelation>();
                       String InvContactAddress='';
                         
                        List<AccountContactRelation> accContactLst = [SELECT id,Contact.Name,ContactId,Account.Id from AccountContactRelation where Account.Id=:accntid LIMIT 600];
                       
                        if(accContactLst.size()>0)
                        {
                            
                            for(AccountContactRelation accRol :accContactLst)
                            {
                                contactidset.add(accRol.ContactId);
                            }
                           
                        }
                                   
                                 
                 }
                 else 
                 {
                     
                    
            List<Care_Of__c> careOfLst =[SELECT id,Name,Client__c, Client__r.Name,Address__c,Care_Of_Address__c, Contact__c,Contact__r.Id,Contact__r.Name from Care_Of__c where id=:accntid];  
         
           Set<id> cid = new Set<Id>();
            if(careOfLst.size()>0){
               
                for(Care_Of__c co : careOfLst){
                    cId.add(co.Client__c);
                    
                    }
                    
                
                }
                if(!cId.isEmpty()){
                       List<Contact> contList= [SELECT id,name from Contact where AccountId In :cId and AccountId != null Limit 10000];
                    
                        
                        
                         if( contList!=null && contList.size()>0 )
                            {
                                
                                
                                for( Contact accRol :contList)
                                {
                                    
                                    contactidset.add(accRol.id);
                                    
                                }
                               
                             }
                              
                }
                 
                        
                }
               
      
    }

                 
             }
          
             
      
        system.debug('invoicingcnt'+contactidset.size());  
        
        if (AlphaFilter == null || AlphaFilter.trim().length() == 0) {
            AlphaFilter = 'All';
        }
          
            
            
        QueryAccount = 'SELECT Name,FirstName, LastName,AccountId  ' +
            ' from Contact  '; 
        
        if (AlphaFilter == 'Other') 
        {
            QueryWhere = BuildWhere(QueryWhere, '(' + String.escapeSingleQuotes(SortField) + ' < \'A\' OR ' + 
                                    String.escapeSingleQuotes(SortField) + ' > \'Z\') AND (NOT ' + 
                                    String.escapeSingleQuotes(SortField) + ' LIKE \'Z%\') ');
        } 
        else if (AlphaFilter != 'All') {
            QueryWhere = BuildWhere(QueryWhere, '(' + String.escapeSingleQuotes(SortField) + ' LIKE \'' + String.escapeSingleQuotes(AlphaFilter) + '%\')' );
        }
        
     
         if (!contactidset.isEmpty()) {
             
         QueryWhere = BuildWhere(QueryWhere, ' Id IN :contactidset');
         
         }
         QueryWhere = BuildWhere(QueryWhere, '  Contact_Status__c = \'Active\' ');
        
        if (SaveSearchName != null) {
            QueryWhere = BuildWhere(QueryWhere, ' (Contact.Name LIKE \'%' + String.escapeSingleQuotes(SaveSearchName) + '%\')');
        }
       
        
        
        QueryAccount += QueryWhere;
        QueryAccount += ' ORDER BY ' + String.escapeSingleQuotes(SortField) + ' ' + String.escapeSingleQuotes(SortDirection) + ' LIMIT 10000';
        
        // Set<Id> cId = new Set<Id>();
        // instructingAddressList = new List<SelectOption>();
        // InstrContactAddress = '';
        // if(jobDetails.Invoicing_Care_Of__c!=null){
        //     isCareOf='YES';
        // }
       system.debug('------>'+QueryAccount);
    
    }
    
    /***
    * BuildWhere - build soql string for where criteria 
    ***/
    public String BuildWhere(String QW, String Cond) {
        if (QW == '') {
            return ' WHERE ' + Cond;
        } else {
            return QW + ' AND ' + Cond;
        }
    }
    
    /***
    * SortDirection - return sort direction. Default ascending(asc)
    ***/
    public String SortDirection {
        get { if (SortDirection == null) {  SortDirection = 'asc'; } return SortDirection;  }
        set;
    }
    
    /***
    * SortField - return sort by field. Default to Name
    ***/
    public String SortField {
        get { if (SortField == null) {SortField = 'Contact.FirstName'; } return SortField;  }
        set; 
    }
    
    /***
    * SortToggle - toggles the sorting of query from asc<-->desc
    ***/
    public void SortToggle() {
        SortDirection = SortDirection.equals('asc') ? 'desc NULLS LAST' : 'asc';
        // reset alpha filter and sort sequence when sorted field is changed
        if (SortFieldSave != SortField) {
            SortDirection = 'asc';
            AlphaFilter = 'All';
            SortFieldSave = SortField;
        }
        // run the query again
        BuildQuery();
    }
    
    /***
    * DoSomethingOne - do something with one selected account
    ***/
    public PageReference DoSomethingOne() {
        system.debug('SelectedOneAccount: ' + SelectedOneAccount);
        return null;
    }
    
    
}