/**
 *  Class Name: CS_BatchInvoiceReporting
 *  Description: Class to update the fields of Fee Income Staging Table with Invoice
 *  Company: dQuotient
 *  CreatedDate:24-10-2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Anand             23-03-2016                 Orginal Version
 * 
 */
global class CS_BatchInvoiceReporting implements Database.Batchable<sObject>, Database.Stateful{
    
    
    public CS_BatchInvoiceReporting(){
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){

        String query = 'SELECT id,name, Opportunity__r.AccountId, Opportunity__c, Opportunity__r.Manager__c, Opportunity__r.Invoicing_Company2__c, Opportunity__r.Invoicing_Care_Of__c, Opportunity__r.Invoicing_Care_Of__r.Reporting_Client__c, (Select id, name, Allocation__c, Invoice__c, Allocation__r.externalCompany__c, Allocation__r.Assigned_To__c,Amount__c,Forecasting__c, Forecasting__r.Allocation__c,  Forecasting__r.Allocation__r.externalCompany__c, Forecasting__r.Allocation__r.Assigned_To__c, Staging_Table_Record_Invoice__c From Invoice_Allocation_Junctions__r  Limit 500000) from Invoice__c where Status__c != \'Draft\' and  Status__c != \'Archived\' and  Status__c != null and No_of_Records_with_No_Fee_Income__c > 0 order by CreatedDate asc limit 50000';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<Invoice__c> scope){
        
        List<Fee_Income_Staging_Table__c> lstFeeIncomeStagingTable = new List<Fee_Income_Staging_Table__c>();
        set<id> setInvoiceAllocationIds = new set<id>();
        List<Fee_Income_Staging_Table__c> lstFeeIncomeStagingTableExisting = new List<Fee_Income_Staging_Table__c>();
        Map<id,Fee_Income_Staging_Table__c> mapIdFeeIncomeStagingTableExisting = new Map<id, Fee_Income_Staging_Table__c>();
        Map<id,Decimal> mapIdSubContractor= new Map<id, Decimal>();
        Map<id,Decimal> mapIdNetFee = new Map<id, Decimal>();
        Map<id,Decimal> mapIdJMAllocation = new Map<id, Decimal>();
        Map<id,Decimal> mapIdOtherAllocation = new Map<id, Decimal>();
        // Population of the Client
        for(Invoice__c objInvoice : scope){
            
            decimal subContractor = 0.0;
            decimal netFee = 0.0;
            decimal JMAllocation = 0.0;
            decimal OtherAllocation = 0.0;
            id idJM = null;
            if(objInvoice.Opportunity__r.Manager__c != null){
                idJM = objInvoice.Opportunity__r.Manager__c;
            }
            
            
            // Calculation of Net Amounts
            if(!objInvoice.Invoice_Allocation_Junctions__r.isEmpty()){
                for(Invoice_Allocation_Junction__c objInvoiceAllocation : objInvoice.Invoice_Allocation_Junctions__r){
                    decimal amountTemp = 0.0;
                    if(objInvoiceAllocation.Amount__c != null){
                        amountTemp = objInvoiceAllocation.Amount__c;
                    }
                    if(objInvoiceAllocation.Forecasting__r.Allocation__r.externalCompany__c != null){
                        // Sum of External Allocations
                        subContractor = subContractor+amountTemp;
                    }else if(objInvoiceAllocation.Forecasting__r.Allocation__r.Assigned_To__c != null){
                        // Sum of Internal Allocation
                        netFee = netFee+amountTemp;
                        
                        if(objInvoiceAllocation.Forecasting__r.Allocation__r.Assigned_To__c == idJM){
                            // JM Allocation
                            JMAllocation = JMAllocation+amountTemp;
                        }else{
                            // Non JM Allocation
                            OtherAllocation= OtherAllocation+amountTemp;
                        }
                    }
                    setInvoiceAllocationIds.add(objInvoiceAllocation.id);
                    
                }
                
            }
            
            mapIdSubContractor.put(objInvoice.id,subContractor);                
            mapIdNetFee.put(objInvoice.id,netFee);              
            mapIdJMAllocation.put(objInvoice.id,JMAllocation);              
            mapIdOtherAllocation.put(objInvoice.id,OtherAllocation);                

        }
        
        lstFeeIncomeStagingTableExisting = [Select id, Invoice_Allocation_Junction__c
                                                    From
                                                    Fee_Income_Staging_Table__c
                                                    where 
                                                    Invoice_Allocation_Junction__c in : setInvoiceAllocationIds
                                                    and Type__c = 'Invoice'
                                                    and Invoice_Allocation_Junction__c != null];
        if(!lstFeeIncomeStagingTableExisting.isEmpty()){                                            
            for(Fee_Income_Staging_Table__c objFeeIncomeStagingTable : lstFeeIncomeStagingTableExisting){
                mapIdFeeIncomeStagingTableExisting.put(objFeeIncomeStagingTable.Invoice_Allocation_Junction__c, objFeeIncomeStagingTable);
            }
        }
        
        Set<Id> opportunityAccountIdSet = new Set<Id>();
        for(Invoice__c objInvoice : scope){
            
            decimal subContractor = 0.0;
            decimal netFee = 0.0;
            decimal JMAllocation = 0.0;
            decimal OtherAllocation = 0.0;
            
            subContractor = mapIdSubContractor.get(objInvoice.id);
            netFee = mapIdNetFee.get(objInvoice.id);
            JMAllocation = mapIdJMAllocation.get(objInvoice.id);
            OtherAllocation = mapIdOtherAllocation.get(objInvoice.id);
            
            id idJM = null;
            id idInvoicingCompany = null;
            id opportunityAccountId = null;
            if(objInvoice.Opportunity__r.Manager__c != null){
                idJM = objInvoice.Opportunity__r.Manager__c;
            }
            if(objInvoice.Opportunity__r.Invoicing_Company2__c != null){
                idInvoicingCompany = objInvoice.Opportunity__r.Invoicing_Company2__c;
            }else if(objInvoice.Opportunity__r.Invoicing_Care_Of__c != null && objInvoice.Opportunity__r.Invoicing_Care_Of__r.Reporting_Client__c != null){
                idInvoicingCompany = objInvoice.Opportunity__r.Invoicing_Care_Of__r.Reporting_Client__c;
                
            }
            
            if(objInvoice.Opportunity__r.AccountId != null){
                opportunityAccountId = objInvoice.Opportunity__r.AccountId;
                if(!opportunityAccountIdSet.contains(opportunityAccountId)){
                    opportunityAccountIdSet.add(opportunityAccountId);
                }
            }
            
            if(!objInvoice.Invoice_Allocation_Junctions__r.isEmpty()){
                for(Invoice_Allocation_Junction__c objInvoiceAllocation : objInvoice.Invoice_Allocation_Junctions__r){
                    
                    if(!(objInvoiceAllocation.Staging_Table_Record_Invoice__c) && !mapIdFeeIncomeStagingTableExisting.containsKey(objInvoiceAllocation.id)){
                    
                        Fee_Income_Staging_Table__c objFeeIncomeStagingTable = new Fee_Income_Staging_Table__c();
                        
                        // Fee Income Master Details
                        objFeeIncomeStagingTable.Allocation__c = objInvoiceAllocation.Forecasting__r.Allocation__c;
                        objFeeIncomeStagingTable.Client__c = idInvoicingCompany;
                        objFeeIncomeStagingTable.Forecasting__c =  objInvoiceAllocation.Forecasting__c;
                        objFeeIncomeStagingTable.Invoice__c = objInvoiceAllocation.Invoice__c;
                        objFeeIncomeStagingTable.Job_Manager__c = idJM;
                        objFeeIncomeStagingTable.Type__c = 'Invoice';
                        objFeeIncomeStagingTable.Invoice_Allocation_Junction__c = objInvoiceAllocation.id;
                        //Value for the Instructing Client
                        objFeeIncomeStagingTable.Instructing_Client__c = opportunityAccountId;
                        
                        // Amount Calculations
                        decimal amountTemp = 0.0;
                        if(objInvoiceAllocation.Amount__c != null){
                            amountTemp = objInvoiceAllocation.Amount__c;
                        }
                        objFeeIncomeStagingTable.Allocation_Net__c = amountTemp;
                        objFeeIncomeStagingTable.Less_Sub_Contractor__c = -(subContractor);
                        objFeeIncomeStagingTable.Net_Fees__c = netFee;
                        objFeeIncomeStagingTable.Job_Manager_Allocation__c = JMAllocation;
                        objFeeIncomeStagingTable.Other_Allocations__c = -(OtherAllocation);
                        
                        if(objInvoiceAllocation.Forecasting__r.Allocation__r.externalCompany__c != null){
                            objFeeIncomeStagingTable.External_Company__c = objInvoiceAllocation.Forecasting__r.Allocation__r.externalCompany__c;
                        }else if(objInvoiceAllocation.Forecasting__r.Allocation__r.Assigned_To__c != null){
                            objFeeIncomeStagingTable.Fee_Allocated_To__c = objInvoiceAllocation.Forecasting__r.Allocation__r.Assigned_To__c;
                        }
                        
                        
                        lstFeeIncomeStagingTable.add(objFeeIncomeStagingTable);
                    }
                }
            }
        }
        system.debug('lstFeeIncomeStagingTable----->'+lstFeeIncomeStagingTable);
        
        // Adding instructing client fields
        Map<Id, Account> accountMap = new Map<Id, Account>([Select Id, Ultimate_Parent_Name__c, Ultimate_Parent__c, Company_Classification__c, Geographical_Client__c, Ultimate_Parent_Sector__c, Ultimate_Parent_Sub_Sector__c From Account Where Id IN :opportunityAccountIdSet]);
        system.debug('accountMap----->'+accountMap);
        if(!lstFeeIncomeStagingTable.isEmpty()){
            for(Fee_Income_Staging_Table__c objFeeIncomeStagingTable :lstFeeIncomeStagingTable)
            {
                if(objFeeIncomeStagingTable.Instructing_Client__c != null)
                {
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Name__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Name__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Name__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent__c != null) {
                        String textString= accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent__c;
                        Integer int1 = textString.indexOf('/');
                        objFeeIncomeStagingTable.Ultimate_Instructing_ParentId__c = textString.substring(int1 + 1,int1 + 16);
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sector__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Sector__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sector__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Geographical_Client__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Geo_Class__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Geographical_Client__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sub_Sector__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Sub_Sector__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sub_Sector__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c = null;
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Developer' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Developer,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Lender' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Lender,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Occupier' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Occupier,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Owner' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Owner,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Service Provider' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Service Provider';
                        }
                        
                    }
                }
            }
        }
        system.debug('lstFeeIncomeStagingTable----->'+lstFeeIncomeStagingTable);
        
        if(!lstFeeIncomeStagingTable.isEmpty()){
            Database.SaveResult[] lstDatabaseSaveResultInsert = Database.insert(lstFeeIncomeStagingTable, false);
            
            integer index = 0;
            List<Invoice_Allocation_Junction__c> lstInvoiceAllocation = new List<Invoice_Allocation_Junction__c>();
            for (Database.SaveResult objDatabaseSaveResult : lstDatabaseSaveResultInsert) {
                if (objDatabaseSaveResult.isSuccess() && lstFeeIncomeStagingTable[index] != null 
                    && lstFeeIncomeStagingTable[index].Invoice_Allocation_Junction__c != null) {
                    
                    Invoice_Allocation_Junction__c objInvoiceAllocation = new Invoice_Allocation_Junction__c();
                    objInvoiceAllocation.id = lstFeeIncomeStagingTable[index].Invoice_Allocation_Junction__c ;
                    objInvoiceAllocation.Staging_Table_Record_Invoice__c = true;
                    lstInvoiceAllocation.add(objInvoiceAllocation);
                }
                
                index = index+1;
            }
            
            if(!lstInvoiceAllocation.isEmpty()){
                Database.SaveResult[] lstDatabaseSaveResultUpdate = Database.update(lstInvoiceAllocation, false);
            }
        }
        
        
    }
    global void finish(Database.BatchableContext BC){
        
    }
    
}