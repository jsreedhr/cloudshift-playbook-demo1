@isTest
public class Vals_SummaryPageController_Test
{
    @isTest
    private static void testMethod1()
    {
        Inspection__c inspObj = New Inspection__c();
        inspObj.Name = 'Test Inspection';
        insert inspObj;
        System.assert(inspObj.id!=NULL);
        
        Surrounding__c surrInspObj = New Surrounding__c();
        surrInspObj.Inspection__c = inspObj.id;
        insert surrInspObj;
        System.assert(surrInspObj.id!=NULL);
        
        External_Inspection__c extInspObj = New External_Inspection__c();
        extInspObj.Inspection__c = inspObj.id;
        insert extInspObj;
        System.assert(extInspObj.id!=NULL);
        
        Internal_Inspection__c intInspObj = New Internal_Inspection__c();
        intInspObj.Inspection__c = inspObj.id;
        intInspObj.External_Inspection__c = extInspObj.id;
        insert intInspObj;
        System.assert(intInspObj.id!=NULL);
        
        PageReference pageRefObj = Page.vals_summaryPage;
        Test.setCurrentPage(pageRefObj);
        System.currentPageReference().getParameters().put('id',inspObj.id);
        Vals_SummaryPageController controllerObj = New Vals_SummaryPageController();
    }
    
    @isTest
    private static void testMethod2()
    {
        Inspection__c inspObj = New Inspection__c();
        inspObj.Name = 'Test Inspection';
        insert inspObj;
        System.assert(inspObj.id!=NULL);
        
        Surrounding__c surrInspObj = New Surrounding__c();
        surrInspObj.Inspection__c = inspObj.id;
        insert surrInspObj;
        System.assert(surrInspObj.id!=NULL);
        
        External_Inspection__c extInspObj = New External_Inspection__c();
        extInspObj.Inspection__c = inspObj.id;
        insert extInspObj;
        System.assert(extInspObj.id!=NULL);
        
        
        
        PageReference pageRefObj = Page.vals_summaryPage;
        Test.setCurrentPage(pageRefObj);
        System.currentPageReference().getParameters().put('id',inspObj.id);
        Vals_SummaryPageController controllerObj = New Vals_SummaryPageController();
        controllerObj.toTitleCase('Test');
    }
}