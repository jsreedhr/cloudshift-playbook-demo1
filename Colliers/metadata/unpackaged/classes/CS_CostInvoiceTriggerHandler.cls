/*
*  Class Name: CS_CostInvoiceTriggerHandler  
*  Description: This is a Trigger Handler Class for trigger on invoice_Cost
*  Company: dQuotient
*  CreatedDate:23/12/2016 
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------  
*  Anand              25/01/2017                 Orginal Version
*  
*/
public with sharing class CS_CostInvoiceTriggerHandler 
 {

    /**
    *  Method Name: setstatus
    *  Description: Method to set status for cost included on invoice based on status.
    *  Param:  Trigger.new
    *  Return: None
    */
    
            
     public static void setstatus(List<Invoice_Cost_Junction__c> lstinvoiceCost)
     {
        
        set<id> setInvoiceIds = new set<id>();
        for(Invoice_Cost_Junction__c objInvoice: lstinvoiceCost)
        
        {
            setInvoiceIds.add(objInvoice.Invoice__c);
        }
        map<id, Invoice__c> mapIdInvoice = new map<id, Invoice__c>();
        if(!setInvoiceIds.isEmpty())
        {
            mapIdInvoice = new map<id, Invoice__c>([Select id, Status__c From Invoice__c where id in: setInvoiceIds]);
        }
        
        for(Invoice_Cost_Junction__c objInvoice: lstinvoiceCost)
        {
            
            if(mapIdInvoice.containsKey(objInvoice.Invoice__c))
            {
                if(mapIdInvoice.get(objInvoice.Invoice__c).Status__c != null)
                {
                    objInvoice.Invoice_Status__c = mapIdInvoice.get(objInvoice.Invoice__c).Status__c;
                }
            }
        }
    }
 }