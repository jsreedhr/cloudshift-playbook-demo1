@isTest 
public with sharing class VALS_ParticipantApexSharingTest
{
    @isTest
    private static void testMethod1()
    {
        Inspection__c inspObj = New Inspection__c();
    	inspObj.Name = 'Test Inspection';
    	insert inspObj;
    	System.assert(inspObj.id!=NULL);
    	
    	Staff__c staff1 = new Staff__c();
          staff1.Name = 'abc';
          staff1.HR_No__c = String.valueOf(Math.round(Math.random()*365));
          insert staff1; 
          System.assert(staff1.id!=NULL);
    	
    	Inspection_Participant__c inspPartObj = New Inspection_Participant__c();
    	inspPartObj.Inspection__c = inspObj.id;
    	inspPartObj.Staff__c = staff1.id;
    	insert inspPartObj;
    	System.assert(inspPartObj.id!=NULL);
    }


}