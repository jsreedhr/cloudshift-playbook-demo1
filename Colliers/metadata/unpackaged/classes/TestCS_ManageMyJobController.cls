/*  Class Name: TestCS_ManageMyJobController 
 *  Description: This class is a controller for ManageMyJob Page
 *  Company: dQuotient
 *  CreatedDate: 18/08/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Aparna               21/09/2016                  Orginal Version
 *
 */

@isTest 
public class TestCS_ManageMyJobController {
    public static Account a;
    public static User portalAccountOwner1;
    public static Account b;
    public static Contact c;
    public static Contact cont;
    public static Contact invc;
    public static opportunity opp;
    public static opportunity oppr;
    public static opportunity opty;
    public static opportunity careOfopty;
    public static Address__c addr;
    public static Address__c invaddr;
    public static Staff__c stf;
    public static Contact_Address_Junction__c conAddrsJunc;
    public static Account_Address_Junction__c accAddrJunc;
    public static List<AccountContactRelation> acr;
    public static Care_Of__c careOf;
    public static AssignAccountId__c assignAccId;

   // @testsetup
    static testmethod void setupData(){
        //create guest user
        Profile profile1 = [Select Id from Profile where name = 'customer public access Profile'];
        portalAccountOwner1 = new User(
                                            ProfileId = profile1.Id,
                                            Username ='hormese@test.com',
                                            Alias = 'batman',
                                            Email='bruce.wayne@wayneenterprises.com',
                                            EmailEncodingKey='UTF-8',
                                            Firstname='Bruce',
                                            Lastname='Wayne',
                                            LanguageLocaleKey='en_US',
                                            LocaleSidKey='en_US',
                                            TimeZoneSidKey='America/Chicago'
                                            );
        insert portalAccountOwner1;

        //create account
        
        a = TestObjectHelper.createAccount();
        a.Company_Status__c = 'Active';
        insert a ;
        b = TestObjectHelper.createAccount();
        b.Company_Status__c = 'Pending';
        insert b ;
        Schema.DescribeFieldResult fieldResult = Address__c.Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //create contact
        c = TestObjectHelper.createContact(a);
        c.Email ='test1@test1.com';
        insert c;
        cont = TestObjectHelper.createContact(a);
        cont.Email ='test1@test1.com';
        insert cont;
        
        //create Contact
        invc = TestObjectHelper.createContact(a);
        invc.Email ='test2@test1.com';
        insert invc;
        
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
        
        //create realted Opportunity
        List <Contact> cons = TestObjectHelper.createMultipleContact(a, 10);
        integer loopNo = 1;
        for(Contact objContact: cons){
            objContact.Email = 'test'+loopNo+'@test.com';
        }
        insert cons;
        
        //create Address Object
        addr = TestObjectHelper.createAddress(a);
        if(!ple.isEmpty()){
            addr.Country__c =ple[0].getValue();
        }
        insert addr;
        
        //create invAddress object
        invaddr = TestObjectHelper.createAddress(a);
        if(!ple.isEmpty()){
            invaddr.Country__c =ple[0].getValue();
        }
        insert invaddr;
        
        //Create Contact Address Junction
        conAddrsJunc = TestObjectHelper.createContactAddressJunction(c,addr);
        insert conAddrsJunc;
        
        //Create Account Address Junction
        accAddrJunc = TestObjectHelper.createAccountAddressJunction(a,addr);
        insert accAddrJunc;
        
        //Create Account Contact Relation Object
        acr = [SELECT id,AccountId,ContactId,Account.Id from AccountContactRelation where Account.Id=:a.Id];
        
        //Create Care OF Object
        careOf = TestObjectHelper.createCareOf(a,c);
        careOf.Colliers_Int__c = true;
        careOf.Reporting_Client__c = a.id;
        insert careOf;
        // create worktype 
        Work_Type__c workObj = new Work_Type__c();
        workObj.Name = 'Valuation - Redbook';
        workObj.Active__c = true;
        insert workObj;
        //Create Assign Account Id
        assignAccId = TestObjectHelper.createAssignAccCode();
        insert assignAccId;
        
        //create staff
        stf = TestObjectHelper.createStaff();
        stf.Active__c = true;
        insert stf;
        // create coding structure object
        Coding_Structure__c codobj = new Coding_Structure__c();
        codobj.Staff__c = stf.Id;
        codobj.Work_Type__c = workObj.Id;
        codobj.Department__c = 'Accurates';
        codobj.Office__c = 'London - West End';
        codobj.Status__c = 'Active';
        codobj.Department_Cost_Code_Centre__c = '620';
        insert codobj;
        //Create a opportunity
        oppr = TestObjectHelper.createOpportunity(a);
        oppr.AccountId = a.id;
        oppr.Name = 'Test';
        oppr.Date_Instructed__c = date.today();
        oppr.CloseDate = system.today().addDays(7);
        oppr.Amount = 8000;
        oppr.Instructing_Contact__c = c.id;
        oppr.InstructingCompanyAddress__c =addr.id;
        oppr.Invoicing_Company2__c =a.id;
        oppr.Invoice_Contact__c = c.id;
        oppr.Invoicing_Address__c = addr.id;
        oppr.Manager__c = stf.id;
        oppr.Department__c = 'Accurates';
        oppr.Work_Type__c = 'Valuation - Redbook';
        oppr.Office__c = 'London - West End';
        oppr.Instructing_Company_Role__c ='Landlord';
    
        insert oppr;
        
        //Create a opportunity
        opty= TestObjectHelper.createOpportunity(a);
        opty.AccountId = a.id;
        opty.Name = 'Test';
        opty.CloseDate = system.today().addDays(7);
        opty.Amount = 8000;
        opty.Instructing_Contact__c = c.id;
        opty.InstructingCompanyAddress__c =addr.id;
        opty.Invoicing_Company2__c =a.id;
        opty.Invoice_Contact__c = invc.id;
        opty.Invoicing_Address__c = invaddr.id;
        opty.Manager__c = stf.id;
        opty.Department__c = 'Accurates';
        opty.Work_Type__c = 'Valuation - Redbook';
        opty.Office__c = 'London - West End';
        opty.Instructing_Company_Role__c ='Landlord';
        
        insert opty;
        
        //Create a care Of opportunity
        careOfopty= TestObjectHelper.createOpportunity(a);
        careOfopty.AccountId = a.id;
        careOfopty.Name = 'Test';
        careOfopty.CloseDate = system.today().addDays(7);
        careOfopty.Amount = 8000;
        careOfopty.Instructing_Contact__c = c.id;
        careOfopty.InstructingCompanyAddress__c =addr.id;
        careOfopty.Invoicing_Care_Of__c=careOf.id;
        careOfopty.Invoice_Contact__c = invc.id;
        careOfopty.Invoicing_Address__c = invaddr.id;
        careOfopty.Manager__c = stf.id;
        careOfopty.Department__c = 'Accurates';
        careOfopty.Work_Type__c = 'Valuation - Redbook';
        careOfopty.Office__c = 'London - West End';
        careOfopty.Instructing_Company_Role__c ='Landlord';
        
        insert careOfopty;
    }    
    static testMethod void testCS_ManageMyJobControllerForOpps(){
        System.debug('opp test---->');
        setupData();
        Test.startTest();
        PageReference pf = new PageReference('/apex/CS_ManageMyJobPage?id='+oppr.Id);
        Test.setCurrentPage(pf);
        CS_ManageMyJobController con = new CS_ManageMyJobController();
        con.renderMainCompany();
        con.renderMainProperty();
        con.renderMainAllocation();
        con.renderMainCost();
        con.renderMainAccural();
        con.renderMainInvoice();
        con.renderMainCreditNote();
        con.renderMainJournal();
        con.cancel();
        Test.stopTest();       
    }
   static testMethod void testCS_ManageMyJobControllerForOppsforid(){
        System.debug('opp test---->');
        //opportunity a = [Select id from opportunity Limit 1];
        setupData();
        Test.startTest();
        PageReference pf = new PageReference('/apex/CS_ManageMyJobPage?AccId='+a.id);
        Test.setCurrentPage(pf);
        CS_ManageMyJobController con = new CS_ManageMyJobController();    
    }
    
    static testMethod void testCS_ManageMyJobLiteControllerForOppsy(){
        System.debug('opp test---->');
        setupData();
        Test.startTest();
        System.runAs(portalAccountOwner1){
            PageReference pf = new PageReference('/apex/cs_ManageJobLitePage?id='+oppr.Id+'&auth=731263829');
            Test.setCurrentPage(pf);
            CS_ManageMyJobLiteController con = new CS_ManageMyJobLiteController();
            con.renderMainCompany();
            con.renderMainProperty();
            con.renderMainAllocation();
            con.renderMainCost();
            con.renderMainAccural();
            con.renderMainInvoice();
            con.renderMainCreditNote();
            con.renderMainJournal();
            con.redirect();
            con.cancel();
        }
        Test.stopTest();     
    }
    /* 
    static testMethod void testCS_ManageMyJobControllerForOppsforidblank(){
        System.debug('opp test---->');
        opportunity a = [Select id from opportunity Limit 1];
        Test.startTest();
        PageReference pf = new PageReference('/apex/CS_ManageMyJobPage?id= ');
        Test.setCurrentPage(pf);
        CS_ManageMyJobController con = new CS_ManageMyJobController();    
    }*/
}