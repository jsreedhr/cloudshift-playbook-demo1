@isTest 
public class Tst_EditRelatedListController {
    @testsetup
    static void setupData(){
        //Create Custom setting data for opportunity
        ObjectFieldSetMapping__c ofs = new ObjectFieldSetMapping__c();
        ofs.Name = 'Opportunity';
        ofs.Object_API_Name__c = 'Opportunity';
        ofs.Field_Set_Name__c = 'OppFieldSet1';
        
        insert ofs;
        
        
        //create account
        Account a = TestObjectHelper.createAccount();
        insert a;
        
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
        System.debug('opps---->'+opps);
        //create realted Opportunity
        List <Contact> cons = TestObjectHelper.createMultipleContact(a, 10);
        integer loopNo = 1;
        for(Contact objContact: cons){
            objContact.Email = 'test'+loopNo+'@test.com';
        }
        insert cons;
        System.debug('cons---->'+cons);
    }
    
    static testMethod void testRelatedListPafeForOpps(){
        System.debug('opp test---->');
        Account a = [Select id from Account Limit 1];
        Test.startTest();
        PageReference pf = new PageReference('/apex/EditRelatedListPage?id='+a.id+'&rl=Opportunity');
        Test.setCurrentPage(pf);
        EditRelatedListController con = new EditRelatedListController();
        con.addRow();
        ApexPages.currentPage().getParameters().put('deletedID','0' );
        con.deleteRow();
        con.cancel();
        Test.stopTest();
        
    }
    
    static testMethod void testRelatedListPafeForContacts(){
        System.debug('con test---->');
        Account a = [Select id from Account Limit 1];
        Test.startTest();
        PageReference pf = new PageReference('/apex/EditRelatedListPage?id='+a.id+'&rl=Contact');
        Test.setCurrentPage(pf);
        EditRelatedListController con = new EditRelatedListController();
        con.save();
        Test.stopTest();
        
    }

}