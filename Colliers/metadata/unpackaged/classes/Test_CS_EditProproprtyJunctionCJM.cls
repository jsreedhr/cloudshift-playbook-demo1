@isTest
private class Test_CS_EditProproprtyJunctionCJM{
   
    Public static Account a;
    static Contact c;
    static opportunity opp;
    static Property__c property;
    static Company_Property_Affiliation__c CmpnyPrptyJn;
    static Job_Property_Junction__c JobPrptyJn;
   
    
     static testmethod void setupData(){
        //create account
        a = TestObjectHelper.createAccount();
        insert a ;
        
        property=TestObjectHelper.createProperty();
        property.Street_No__c ='Park Lane';
        property.Post_Code__c ='W1K 3DD';
        property.Country__c='United Kingdom';
        property.Geolocation__Latitude__s=51.51;
        property.Geolocation__Longitude__s=-0.15;
        insert property;
        
        
   
        
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
        CmpnyPrptyJn= TestObjectHelper.CmpnyPrptyJn(a);
        insert CmpnyPrptyJn;
        
        JobPrptyJn=  TestObjectHelper.JobPrptyJn(opps[0],property);
        insert JobPrptyJn;
        opp = [Select id from opportunity Limit 1];
        
      
      
    }
    private static testMethod void test1() {
         setupData();
        opp = [Select id from opportunity Limit 1];
       
        Test.setCurrentPageReference(new PageReference('Page.CS_PropertyJnView'));

        System.currentPageReference().getParameters().put('id', opp.id);
        System.currentPageReference().getParameters().put('Selids', property.id);
        
        Test.startTest();
        
            CS_EditProproprtyJunctionControllerJM ctrl = new CS_EditProproprtyJunctionControllerJM();  
            ctrl.getCategorieskeypreix();
            ctrl.getPropertyDetails();
            ctrl.setClosedJob();
            ctrl.getCategories();
            ctrl.getNewCategories();
            ctrl.SelectedProperty();
            ctrl.IsSelectAll = true;
            ctrl.SelectedProperty();
             
        Test.stopTest();


    }
    private static testMethod void test2() 
    {
        setupData();
        opp = [Select id from opportunity Limit 1];
       
        Test.setCurrentPageReference(new PageReference('Page.CS_PropertyJnView'));
        System.currentPageReference().getParameters().put('id', a.id);
        System.currentPageReference().getParameters().put('Selids', property.id);
        
        Test.startTest();
            CS_EditProproprtyJunctionControllerJM ctrl = new CS_EditProproprtyJunctionControllerJM(); 
            ctrl.getCategorieskeypreix();
            ctrl.getCategories();
            ctrl.getNewCategories();
            ctrl.SelectedProperty();
        Test.stopTest();
    }

    private static testMethod void test3() 
    {
        setupData();
        opp = [Select id from opportunity Limit 1];
       
        Test.setCurrentPageReference(new PageReference('Page.CS_PropertyJnView'));
        System.currentPageReference().getParameters().put('id', a.id);
        System.currentPageReference().getParameters().put('Selids', property.id);

        Test.startTest();
            CS_EditProproprtyJunctionControllerJM ctrl = new CS_EditProproprtyJunctionControllerJM(); 
            ctrl.getCategorieskeypreix();
            ctrl.getCategories();
            ctrl.first();
            ctrl.Last();
            ctrl.Previous();
            ctrl.Next();
            ctrl.selectAllProperty();
            ctrl.IdtoDelete = '0';
            ctrl.deleteSelectedRow();
            ctrl.cancelJobs();
            ctrl.keyPrefix='006';
            ctrl.cancelJobs(); 
            ctrl.ListbooleanProproprtyJnWrapper.add(new CS_EditProproprtyJunctionControllerJM.booleanProproprtyJnWrapper(NULL,'S','1','1','1'));      
        Test.stopTest();


    }
}