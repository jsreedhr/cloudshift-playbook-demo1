public class TestRizwan{
    User userObj;
    Account account;
    Contact contact;
    Opportunity opportunity;
    Invoice__c invoice;
    Invoice_Allocation_Junction__c invAllocationJn;
    Allocation__c allocation;
    Disbursements__c cost;
    List<Invoice_Line_Item__c> invoiceLineItems;

    public TestRizwan(){

    }
    public void createData(){
        User user = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        //account;
        account = new Account();
        account.Name = 'Some Name';
        account.Town__c = 'acc town';
        account.Street_No__c ='#12';
        account.Street__c ='Street';
        account.Post_Code__c = '2345';
        account.Country__c ='UK';
        account.Company_Status__c = 'Active';
        insert account;
        System.debug('---- Account Created : ' + account);

        //contact
        contact = (Contact)SmartFactory.createSObject('Contact', false);
        contact.AccountId = account.Id;
        contact.lastName='Test';
        contact.Email ='test1@test1.com';
        insert contact;
        System.debug('---- Contact Created : ' + contact);

        //Property__c
        /*
        Property__c property = new Property__c();
        property = (Property__c)SmartFactory.createSObject('Property__c', false);
        property.Address_Validation_Status__c = 'Verified';
        property.Street_No__c ='Park Lane';
        property.Post_Code__c ='W1K 3DD';
        property.Country__c = 'United Kingdom';
        property.Geolocation__Latitude__s=51.51;
        property.Geolocation__Longitude__s=-0.15;
        insert property;
        System.debug('---- Property Created : ' + property);


        //AssingId__c
        AssignId__c assign = new AssignId__c();
        assign.Name = 'test';
        assign.Invoice_Pattern__c = 'pattern';
        assign.AssignNo__c = 123;
        insert assign;
        System.debug('---- AssignId Created : ' + assign);

        //AssignAccountId__c
        AssignAccountId__c assignAccount = new AssignAccountId__c();
        assignAccount.Name = 'test';
        assignAccount.AccountNumber__c = 123;
        assignAccount.Account_Pattern__c = '123';
        insert assignAccount;
        System.debug('---- AssignAccount Created : ' + assignAccount);
		*/
        //Address__c
        Address__c address = new Address__c();
        address = (Address__c)SmartFactory.createSObject('Address__c', false);
        address.Address_Type__c = 'Primary';
        address.Area__c = '500';
        address.Building__c = 'XYZ Apartment';
        address.Country__c = 'Netherlands';
        address.Flat_Number__c = '301';
        address.Head_Office_Address__c = true;
        address.Postcode__c = '123 456';
        address.Street__c = 'Abc';
        address.Street_Number__c = '13A';
        address.Town__c = 'some town';

        address.Estate__c = 'estate';
        address.Flat_Number__c = '#12';
        address.Postcode__c = '2345';
        address.Country__c = 'United Kingdom';
        insert address;
        System.debug('---- Address Created : ' + address);

        //Staff__c
        Staff__c staff = new Staff__c();
        staff.name='Jaredresrstt8SDhHHZSHxzM';
        staff.Email__c='resrstt8SDhHHZSHxzM.ZMSDLM@gmail.com';
        staff.active__c = true;
        staff.HR_No__c = String.valueOf(Math.round(Math.random()*1000) + Math.random()*345);
        staff.User__c = userInfo.getUserId();
        insert staff;
        System.debug('---- staff Created : ' + staff);

        //Work_Type__c
        Work_Type__c workType = new Work_Type__c();
        workType.Name = 'worktype';
        workType.Active__c = true;
        insert workType;
        System.debug('---- workType Created : ' + worktype);

        Coding_Structure__c codingStructure = new Coding_Structure__c();
        codingStructure.Work_Type__c = workType.Id;
        codingStructure.Staff__c = staff.Id;
        codingStructure.Status__c = 'Active';
        codingStructure.Department__c = 'International Properties';
        codingStructure.Office__c = 'Amsterdam';
        insert codingStructure;
        System.debug('---- codingStructure Created : ' + codingStructure);


        //Contact Address Jn
        Contact_Address_Junction__c contactAddressJn = new Contact_Address_Junction__c();
        contactAddressJn.Address__c = address.Id;
        contactAddressJn.Contact__c = contact.Id;
        insert contactAddressJn;
        System.debug('---- contactAddressJn Created : ' + contactAddressJn);


        //account Address Jn
        Account_Address_Junction__c accountAddressJn = new Account_Address_Junction__c();
        accountAddressJn.Address__c = address.Id;
        accountAddressJn.Account__c = account.Id;
        insert accountAddressJn;
        System.debug('---- accountAddressJn Created : ' + accountAddressJn);


        //Opportunity__c
        Date today = Date.today();
        SmartFactory.IncludedFields.put('Opportunity', new Set<String> { 'Sales_Region__c' });
        opportunity = (Opportunity)SmartFactory.createSObject('Opportunity',false);
        opportunity.Name = 'test' + Math.random()*1000;
        opportunity.Amount = 2000;
        opportunity.Date_Instructed__c = today;
        opportunity.CloseDate = today+30;
        opportunity.Source_Of_Instruction__c = 'Client - new/own initiative';
        opportunity.Manager__c = staff.Id;
        opportunity.Department__c = 'International Properties';
        opportunity.Office__c = 'Amsterdam';
        opportunity.Work_Type__c = 'worktype';

        opportunity.AccountId = account.Id;
        opportunity.Instrcting_Contact_name__c = contact.Id;
        opportunity.InstructingCompanyAddress__c = address.Id;
        opportunity.Instructing_Company_Role__c = 'Landlord';
        opportunity.Instructing_Contact__c = contact.Id;

        opportunity.Engagement_Letter__c = true;
        opportunity.Engagement_Letter_Sent_Date__c = Date.today();
        opportunity.Property_Area__c = 1000;
        opportunity.Property_Area_UOM__c = 'sqm';
        insert opportunity;
        System.debug('---- opportunity Created : ' + opportunity);

        //Care_Of__c
        Care_Of__c careOf = new Care_Of__c();
        careOf= (Care_Of__c)SmartFactory.createSObject('Care_Of__c', false);
        careOf.Client__c = account.id;
        careOf.Contact__c = contact.id;
        careOf.Colliers_Int__c = true;
        insert careOf;
        System.debug('---- careOf Created : ' + careOf);

        //not creating another opp.
        //Job_Property_Junction__c
        /*
        Job_Property_Junction__c jobPropertyJn = new Job_Property_Junction__c();
        jobPropertyJn = (Job_Property_Junction__c)SmartFactory.createSObject('Job_Property_Junction__c', false);
        jobPropertyJn.Job__c = opportunity.id;
        //jobPropertyJn.Property__c=property.id;
        insert jobPropertyJn;
        System.debug('---- jobPropertyJn Created : ' + jobPropertyJn);
		*/
        //Allocation__c
        allocation = (Allocation__c)SmartFactory.createSObject('Allocation__c',false);
        allocation.job__c= opportunity.id;
        allocation.Main_Allocation__c = true;
        allocation.Department_Allocation__c = 'International Properties';
        allocation.Assigned_To__c=staff.id;
        allocation.complete__c = false;
        insert allocation;
        System.debug('---- allocation Created : ' + allocation);

        //not creating purchase order
        //Invoice__c
        invoice = new Invoice__c();
        invoice = (Invoice__c)SmartFactory.createSObject('Invoice__c',false);
        invoice.Opportunity__c= opportunity.id;
        invoice.contact__c=contact.id;
        invoice.is_International__c=true;
        invoice.status__c ='Draft';
        invoice.Invoice_Wording__c = 'testWord';
        insert invoice;
        System.debug('---- invoice Created : ' + invoice);


        //Invoice_Line_Item__c
        invoiceLineItems = new List<Invoice_Line_Item__c>();
        Invoice_Line_Item__c invoiceLineItem1 = new Invoice_Line_Item__c();
        invoiceLineItem1 = (Invoice_Line_Item__c)SmartFactory.createSObject('Invoice_Line_Item__c',false);
        invoiceLineItem1.Invoice__c = invoice.id;
        invoiceLineItem1.type__c = 'Fee';
        invoiceLineItem1.Amount_ex_VAT__c = 1000;
        invoiceLineItem1.VAT_Amount__c = 200;
        invoiceLineItem1.International_Amount__c = 1000;
        invoiceLineItems.add(invoiceLineItem1);

        Invoice_Line_Item__c invoiceLineItem2 = new Invoice_Line_Item__c();
        invoiceLineItem2 = (Invoice_Line_Item__c)SmartFactory.createSObject('Invoice_Line_Item__c',false);
        invoiceLineItem2.Invoice__c = invoice.id;
        invoiceLineItem2.type__c = 'Cost';
        invoiceLineItem2.Amount_ex_VAT__c = 500;
        invoiceLineItem2.VAT_Amount__c = 100;
        invoiceLineItem2.International_Amount__c = 500;
        invoiceLineItems.add(invoiceLineItem2);

        insert invoiceLineItems;
        System.debug('---- invoiceLineItems Created : ' + invoiceLineItems);

        //Cost__c
        cost = new Disbursements__c();
        cost = (Disbursements__c)SmartFactory.createSObject('Disbursements__c',false);
        cost.Job__c = opportunity.id;
        cost.InvoiceId__c = invoice.Id;
        cost.Purchase_Cost__c = 1000;
        cost.Recharge_Cost__c = 1000;
        cost.Category__c ='Cost Flow Through (Residential only)';
        cost.Created_For__c = staff.id;
        cost.Sub_Category__c='Cost Flow Through (RESIDENTIAL ONLY) - With VAT';
        cost.Purchase_Date__c  = System.today();
        cost.Description__c = 'Tsts';
        cost.Invoice_Details__c = 'Tsts';
        insert cost;
        System.debug('---- cost Created : ' + cost);


        //Forecasting__c
        Forecasting__c forecasting = new Forecasting__c();
        forecasting = (Forecasting__c)SmartFactory.createSObject('Forecasting__c',false);
		forecasting.Allocation__c =allocation.id;
        forecasting.CS_Forecast_Date__c = system.today().toStartOfMonth();
		forecasting.Amount__c=0.0;
		insert forecasting;
        System.debug('---- forecasting Created : ' + forecasting);

        //Invoice Cost Junction
        Invoice_Cost_Junction__c invoiceCostJn = new Invoice_Cost_Junction__c();
        invoiceCostJn = (Invoice_Cost_Junction__c)SmartFactory.createSObject('Invoice_Cost_Junction__c',false);
        invoiceCostJn.Disbursement__c = cost.id;
        invoiceCostJn.Invoice__c = invoice.id;
        insert invoiceCostJn;
        System.debug('---- invoiceCostJn Created : ' + invoiceCostJn);

        //Invoice Allocation Jn
        Invoice_Allocation_Junction__c invoiceAllocationJn = new Invoice_Allocation_Junction__c();
        invoiceAllocationJn = (Invoice_Allocation_Junction__c)SmartFactory.createSObject('Invoice_Allocation_Junction__c',false);
        invoiceAllocationJn.Allocation__c= allocation.id;
        invoiceAllocationJn.Invoice__c=invoice.id;
        invoiceAllocationJn.Forecasting__c = forecasting.id;
        insert invoiceAllocationJn;
        System.debug('---- invoiceAllocationJn Created : ' + invoiceAllocationJn);

        allocation.Main_Allocation__c = true;
        update allocation;

    }
}