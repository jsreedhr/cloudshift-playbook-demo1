/**
 *  Class Name: cs_departmentForecastClass 
 *  Description: Controller class for the cs_departmentforecastPage
 *  Company: dQuotient
 *  CreatedDate: 24/10/2016 
 * 
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nidheesh               24/10/2016                  Orginal Version          
 *
 */
public class cs_departmentForecastClass {
    public List<AggregateResult> Result {get;set;}
    public String departments{get;set;}
    public String currentmonthName{get;set;}
    public Map<string,set<string>> lstOfDeptrecords{get;set;}
    public set<string> lstOfDept{get;set;}
    public wrapperclassTotal totalwrapObject {get;set;}
    public set<string> lstOfDeptdata{get;set;}
    public List<string> lstOfDeptname{get;set;}
    Public List<String> monthListName{get;set;}
    public Map<string,List<String>> lstOfoffice{get;set;}
    public Map<string,Department_Forecast__c> lstOfdeparts{get;set;}
    public List<Department_Forecast__c> lstOfdeptFC{get;set;}
    public Map<String,Map<string,List<Integer>>> lstOfmonth{get;set;}
    Public Map<String,Map<String,Map<Integer,Decimal>>> Mapdata{get;set;}
    Public Map<String,Map<String,Map<Integer,Decimal>>> MapdataFees{get;set;}
    public Map<string,List<wrapperclass>> mapWraperdata{get;set;}
    public Map<string,wrapperclassTotal> maptotalWraperdata{get;set;}
    public cs_departmentForecastClass(){
        lstOfdeptFC = new List<Department_Forecast__c>();
        lstOfDept = new set<string>();
        lstOfDeptrecords = new Map<string,set<string>>();
        lstOfdeparts = new Map<string,Department_Forecast__c>();
        departments='';
        Id userId = UserInfo.getUserId();
        List<string> lstofdeptmnt = new list<string>();
        List <Staff__c> lstOfStaff = new List<Staff__c>([SELECT Id,headOfDepartment__c FROM Staff__c WHERE User__c =:userId]);
        System.debug('list'+lstOfStaff);
        
        if(!lstOfStaff.Isempty()){
            for(Staff__c obj :lstOfStaff){
                if(obj.headOfDepartment__c != null){
                    String[] tmpString = obj.headOfDepartment__c.split(';');
                    lstofdeptmnt.addAll(tmpString);
                    system.debug('objstring'+tmpString);
                }
                
            }
        }
        if(!lstofdeptmnt.Isempty()){
            for(String objstring :lstofdeptmnt ){
                lstOfDept.add(objstring);
                if(objstring != ''){
                    system.debug('objstring'+objstring);
                    departments = departments+''+objstring+',';
                    System.debug('departments'+departments);
                    
                }
            }
            //departments = departments.escapeXml();
        }
         
        lstOfdeptFC = [SELECT id,Adjusted_Month_1_Amount__c,Adjusted_Month_2_Amount__c,Submitted__c,Adjusted_Month_3_Amount__c,Department__c,Office__c,officename__c FROM Department_Forecast__c WHERE
                       Calendar_Month(Month_1_Name__c) >=: System.today().month() AND Calendar_Year(Month_1_Name__c) >=: System.today().Year()];
        System.debug('check---'+lstOfdeptFC);
        if(!lstOfdeptFC.isEmpty()){
            for(Department_Forecast__c objdeptfct :lstOfdeptFC ){
                set<String> officeset = new set<String>();
                if(String.isNotBlank(objdeptfct.officename__c)){
                    officeset.add(objdeptfct.officename__c);
                }
                if(lstOfDeptrecords.containsKey(objdeptfct.Department__c)){
                   lstOfDeptrecords.get(objdeptfct.Department__c).add(objdeptfct.officename__c); 
                }else{
                   lstOfDeptrecords.put(objdeptfct.Department__c,officeset);
                }
                
                
                
            }
        }
        InitialDepartForecast();
        
        
    }
    public void InitialDepartForecast(){
       
        List<Fee_Income_Staging_Table__c> lstoffeeIncome = new List<Fee_Income_Staging_Table__c>();
        Map<String,Map<String,Map<Integer,Decimal>>> MapdataFeeincome = new Map<String,Map<String,Map<Integer,Decimal>>>();
        lstOfDeptname = new List<String>();
        totalwrapObject = new wrapperclassTotal();
        mapWraperdata = new Map<String,list<wrapperclass>>();
        maptotalWraperdata = new Map<string,wrapperclassTotal> ();
        Integer currentmonth = System.today().month();
        Integer currentyear = System.today().year();
        List<Integer> monthlist = new List<Integer>();
        monthlist.add(currentmonth);
        if(currentmonth+1 >12){
            monthlist.add((currentmonth+1)-12);
        }else{
            monthlist.add(currentmonth+1);
        }
        if(currentmonth+2 >12){
            monthlist.add((currentmonth+2)-12);
        }else{
            monthlist.add(currentmonth+2);
        }
        currentmonthName = '';
        Date monthPlus1 = System.today().addMonths(1);
        Date monthPlus2 = System.today().addMonths(2);
        lstOfDeptdata = new set<string>();
        lstOfoffice = new Map<string,List<String>>();
        lstOfmonth = new  Map<String,Map<string,List<Integer>>>();
        monthListName = new List<String>();
        Mapdata = new Map<String,Map<String,Map<Integer,Decimal>>>();
        MapdataFees = new Map<String,Map<String,Map<Integer,Decimal>>>();
        lstoffeeIncome = [SELECT id,Invoice_Date__c,Allocation_Net__c,Total_Fees__c,Allocation__c,Allocation__r.Department_Allocation__c,Allocation__r.Office__c FROM Fee_Income_Staging_Table__c WHERE Calendar_Month(Invoice_Date__c) >=: currentmonth AND (Calendar_Month(Invoice_Date__c)<=: monthPlus2.month() OR Calendar_Year(Invoice_Date__c)<=: monthPlus2.year()) AND Calendar_Year(Invoice_Date__c)=: currentyear AND Allocation__r.Office__c!=Null 
                            AND Allocation__r.Department_Allocation__c != Null AND Allocation__r.Department_Allocation__c IN:lstOfDept ];
        Result = new  List<AggregateResult>();
        Result = [SELECT Allocation__r.Department_Allocation__c allocation,Allocation__r.Job__r.StageName status,Allocation__r.Office__c office, Calendar_Month(CS_Forecast_Date__c) month,Sum(Amount__c) total,Sum(Invoiced_Amount__c) totalInvoiced,Sum(Accured_Amount__c) totalAccrued,Calendar_Year(CS_Forecast_Date__c) year
                    FROM Forecasting__c  group by Allocation__r.Department_Allocation__c,Allocation__r.Office__c,Calendar_Year(CS_Forecast_Date__c),Calendar_Month(CS_Forecast_Date__c), Allocation__r.Job__r.StageName
                    HAVING  Allocation__r.Office__c!=Null AND Allocation__r.Department_Allocation__c != Null AND Allocation__r.Department_Allocation__c IN:lstOfDept AND 
                            ((Calendar_Month(CS_Forecast_Date__c) = :System.today().month() AND Calendar_Year(CS_Forecast_Date__c) = :System.today().year())
                            OR (Calendar_Month(CS_Forecast_Date__c) = :monthPlus1.month() AND Calendar_Year(CS_Forecast_Date__c) = :monthPlus1.year())
                            OR (Calendar_Month(CS_Forecast_Date__c) = :monthPlus2.month() AND Calendar_Year(CS_Forecast_Date__c) = :monthPlus2.year())) ORDER BY Allocation__r.Department_Allocation__c ];
        
        System.debug('data---'+Result);
        System.debug('datafee---'+lstoffeeIncome);
        
        if(!Result.isEmpty()){
            for(AggregateResult a :Result){
                if((String)a.get('status') !='Closed'){
                    lstOfDeptdata.add((String)a.get('allocation'));
                    if(Mapdata.containsKey((String)a.get('allocation'))){
                        if(Mapdata.get((String)a.get('allocation')).containsKey((String)a.get('office'))){
                            if(Mapdata.get((String)a.get('allocation')).get((String)a.get('office')).containsKey((Integer)a.get('month'))){
                                
                            }else{
                                Mapdata.get((String)a.get('allocation')).get((String)a.get('office')).put((Integer)a.get('month'),(Decimal)a.get('Total'));    
                            }
                        }else{
                            Map<Integer,Decimal> monthdataoffice = new Map<Integer,Decimal>();
                            monthdataoffice.put((Integer)a.get('month'),(Decimal)a.get('Total'));
                            Mapdata.get((String)a.get('allocation')).put((String)a.get('office'),monthdataoffice);    
                        }
                        
                    }else{
                        Map<Integer,Decimal> monthdata = new Map<Integer,Decimal>();
                        monthdata.put((Integer)a.get('month'),(Decimal)a.get('Total'));
                        Map<String,Map<Integer,Decimal>> officedata = new Map<String,Map<Integer,Decimal>>();
                        officedata.put((String)a.get('office'),monthdata);
                        Mapdata.put((String)a.get('allocation'),officedata);
                        System.debug('check'+Mapdata);
                    }
                    
                    if(MapdataFees.containsKey((String)a.get('allocation'))){
                        decimal fees = 0.00;
                        fees = fees + (Decimal)a.get('totalInvoiced') + (Decimal)a.get('totalAccrued');
                        if(MapdataFees.get((String)a.get('allocation')).containsKey((String)a.get('office'))){
                            if(MapdataFees.get((String)a.get('allocation')).get((String)a.get('office')).containsKey((Integer)a.get('month'))){
                                
                            }else{
                                
                                
                                MapdataFees.get((String)a.get('allocation')).get((String)a.get('office')).put((Integer)a.get('month'),fees); 
                            }
                        }else{
                            Map<Integer,Decimal> monthdataoffice = new Map<Integer,Decimal>();
                            monthdataoffice.put((Integer)a.get('month'),fees);
                            MapdataFees.get((String)a.get('allocation')).put((String)a.get('office'),monthdataoffice);    
                        }
                        
                    }else{
                        decimal fees = 0.00;
                        fees = fees + (Decimal)a.get('totalInvoiced') + (Decimal)a.get('totalAccrued');
                        Map<Integer,Decimal> monthdata = new Map<Integer,Decimal>();
                        monthdata.put((Integer)a.get('month'),fees);
                        Map<String,Map<Integer,Decimal>> officedata = new Map<String,Map<Integer,Decimal>>();
                        officedata.put((String)a.get('office'),monthdata);
                        MapdataFees.put((String)a.get('allocation'),officedata);
                        System.debug('check'+MapdataFees);
                    }
                }
            }
        }
        System.debug('data---'+Mapdata);
        System.debug('datafee---'+MapdataFees);
        /* creating map using feeincome data*/
        if(!lstoffeeIncome.isEmpty()){
            for(Fee_Income_Staging_Table__c objfee:lstoffeeIncome){
                if(objfee.Allocation__r.Department_Allocation__c != null && MapdataFeeincome.containsKey(objfee.Allocation__r.Department_Allocation__c)){
                    if(objfee.Allocation__r.Office__c != null && MapdataFeeincome.get(objfee.Allocation__r.Department_Allocation__c).containsKey(objfee.Allocation__r.Office__c)){
                        
                        if(objfee.Invoice_Date__c != null && MapdataFeeincome.get(objfee.Allocation__r.Department_Allocation__c).get(objfee.Allocation__r.Office__c).containsKey(objfee.Invoice_Date__c.month())){
                            if(objfee.Allocation_Net__c != null){
                                Decimal sumoffee = 0.00;
                                sumoffee = sumoffee + MapdataFeeincome.get(objfee.Allocation__r.Department_Allocation__c).get(objfee.Allocation__r.Office__c).get(objfee.Invoice_Date__c.month()) + objfee.Allocation_Net__c;
                                MapdataFeeincome.get(objfee.Allocation__r.Department_Allocation__c).get(objfee.Allocation__r.Office__c).put(objfee.Invoice_Date__c.month(),sumoffee);
                            }
                            
                        }else if(objfee.Invoice_Date__c != null){
                            if(objfee.Allocation_Net__c != null){
                                MapdataFeeincome.get(objfee.Allocation__r.Department_Allocation__c).get(objfee.Allocation__r.Office__c).put(objfee.Invoice_Date__c.month(),objfee.Allocation_Net__c);
                            }
                        }
                    }else if(objfee.Allocation__r.Office__c != null){
                        Map<Integer,Decimal> monthdatamap = new Map<Integer,Decimal>();
                        if(objfee.Invoice_Date__c != null ){
                            if(objfee.Allocation_Net__c != null ){
                                monthdatamap.put(objfee.Invoice_Date__c.month(),objfee.Allocation_Net__c);
                            }else{
                                monthdatamap.put(objfee.Invoice_Date__c.month(),0.00);
                            }
                            MapdataFeeincome.get(objfee.Allocation__r.Department_Allocation__c).put(objfee.Allocation__r.Office__c,monthdatamap);
                        }
                    }
                }else if(objfee.Allocation__r.Department_Allocation__c != null){
                    Map<Integer,Decimal> monthdatamap = new Map<Integer,Decimal>();
                    if(objfee.Invoice_Date__c != null ){
                        if(objfee.Allocation_Net__c != null ){
                            monthdatamap.put(objfee.Invoice_Date__c.month(),objfee.Allocation_Net__c);
                        }else{
                            monthdatamap.put(objfee.Invoice_Date__c.month(),0.00);
                        }
                        Map<String,Map<Integer,Decimal>> officedatamap= new Map<String,Map<Integer,Decimal>>();
                        if(objfee.Allocation__r.Office__c != null && String.isNotBlank(objfee.Allocation__r.Office__c)){
                            officedatamap.put(objfee.Allocation__r.Office__c,monthdatamap);
                        }
                        MapdataFeeincome.put(objfee.Allocation__r.Department_Allocation__c,officedatamap);
                    }
                }
            }
        }
        
        lstOfDeptname.addAll(lstOfDeptdata);
        for (integer i = 0; i < 3; i++) {
            Datetime monthfinder = datetime.newInstance(system.today().year(), currentmonth + i, 1);
            String monthvalue = monthfinder.format('MMMMM');
            monthListName.add(monthvalue);
            
            
        }
        if(!monthListName.isEmpty()){
            currentmonthName = monthListName[0];
        }
        if(!Mapdata.isEmpty()){
            if(!lstOfDeptdata.Isempty()){
                for(String objstring:lstOfDeptdata){
                    List<wrapperclass> lstOfwrapper = new List<wrapperclass>();
                    if(!Mapdata.get(objstring).Isempty()){
                        for(String objstr :Mapdata.get(objstring).Keyset()){
                            wrapperclass wrapObj = new wrapperclass();
                             Decimal totalfrcst = 0;
                            List<Decimal> lstofamnt = new List<Decimal>();
                            if(!Mapdata.get(objstring).get(objstr).Isempty()){
                                
                                for(Integer intobj:monthlist){
                                    Integer i =currentmonth;
                                    if(i>12){
                                        i = i-12;
                                    }
                                    if(intobj == i){
                                        if(MapdataFeeincome.containsKey(objstring)){
                                            if(MapdataFeeincome.get(objstring).containsKey(objstr)){
                                                if(MapdataFeeincome.get(objstring).get(objstr).containsKey(intobj)){
                                                    if(MapdataFeeincome.get(objstring).get(objstr).get(intobj) != null){
                                                        wrapObj.currentmonthfees = MapdataFeeincome.get(objstring).get(objstr).get(intobj);
                                                    }else{
                                                        wrapObj.currentmonthfees = 0.00;
                                                    }
                                                    
                                                }else{
                                                    wrapObj.currentmonthfees = 0.00;
                                                }
                                            }else{
                                                wrapObj.currentmonthfees = 0.00;
                                            }
                                            
                                        }else{
                                            wrapObj.currentmonthfees = 0.00;
                                        }
                                        
                                    }
                                    if(Mapdata.get(objstring).get(objstr).get(intobj)!= null){
                                        
                                        lstofamnt.add(Mapdata.get(objstring).get(objstr).get(intobj));
                                        totalfrcst = totalfrcst+Mapdata.get(objstring).get(objstr).get(intobj);
                                    }else{
                                        lstofamnt.add(0);
                                        totalfrcst = totalfrcst +0;
                                        
                                    }
                                    i++;
                                }      
                            }
                            wrapObj.OfficeName = objstr;
                            wrapObj.amount1 = lstofamnt[0];
                            wrapObj.amount2 = lstofamnt[1];
                            wrapObj.amount3 = lstofamnt[2];
                            wrapObj.amount1adjust = 0.00;
                            wrapObj.amount2adjust = 0.00;
                            wrapObj.amount3adjust = 0.00; 
                            
                            lstOfwrapper.add(wrapObj);
                        }  
                    }
                     
                    mapWraperdata.put(objstring,lstOfwrapper);
                }
                
            }
        }
        Map<string,set<string>> deptToOffice = new Map<string,set<string>>();
        List<Department_Forecast__c> lsttodelete = new List<Department_Forecast__c>();
        System.debug('Object msg'+mapWraperdata);
        if(!lstOfdeptFC.isEmpty()){
            for(Department_Forecast__c objdep :lstOfdeptFC ){
                if(mapWraperdata.containsKey(objdep.Department__c)){
                    for(wrapperclass wrpobj :mapWraperdata.get(objdep.Department__c) ){
                        Decimal totaladjustamount = 0;
                        if(wrpobj.OfficeName == objdep.officename__c){
                            if(objdep.Adjusted_Month_1_Amount__c != null && objdep.Adjusted_Month_1_Amount__c > 0.00){  
                                wrpobj.amount1adjust = objdep.Adjusted_Month_1_Amount__c;
                                totaladjustamount = totaladjustamount+objdep.Adjusted_Month_1_Amount__c;
                            }else{
                                wrpobj.amount1adjust = 0.00;
                                totaladjustamount = totaladjustamount + wrpobj.amount1;
                            }
                            if(objdep.Adjusted_Month_1_Amount__c != null && objdep.Adjusted_Month_2_Amount__c > 0.00){
                                wrpobj.amount2adjust = objdep.Adjusted_Month_2_Amount__c;
                                totaladjustamount = totaladjustamount + objdep.Adjusted_Month_2_Amount__c;
                            }else{
                                wrpobj.amount2adjust = 0.00; 
                                totaladjustamount = totaladjustamount + wrpobj.amount2;
                            }
                            if(objdep.Adjusted_Month_1_Amount__c != null && objdep.Adjusted_Month_2_Amount__c > 0.00){
                                wrpobj.amount3adjust = objdep.Adjusted_Month_3_Amount__c;  
                                totaladjustamount = totaladjustamount + objdep.Adjusted_Month_3_Amount__c;
                            }else{
                                wrpobj.amount3adjust = 0.00; 
                                totaladjustamount = totaladjustamount + wrpobj.amount3;
                            }
                            if(totaladjustamount>0.00){
                                wrpobj.total =totaladjustamount;     
                            }else{
                                wrpobj.total =0.00; 
                            }
                        }   
                    }
                }
            }   
        }
        if(!mapWraperdata.isEmpty()){ 
            totalwrapObject.totalLabel = 'Total';
            totalwrapObject.totalamount1 = 0.00;
            totalwrapObject.totalamount2 = 0.00;
            totalwrapObject.totalamount3 = 0.00;
            totalwrapObject.totalamount1adjust = 0.00;
            totalwrapObject.totalamount2adjust = 0.00;
            totalwrapObject.totalamount3adjust = 0.00;
            totalwrapObject.totalcurrentmonthfees = 0.00;
            totalwrapObject.totalamount = 0.00;
            for(string objdept :mapWraperdata.Keyset() ){
                if(!maptotalWraperdata.containsKey(objdept)){
                    wrapperclassTotal wrapptotalObj = new wrapperclassTotal();
                    wrapptotalObj.totalLabel = 'Total';
                    wrapptotalObj.totalamount1 = 0.00;
                    wrapptotalObj.totalamount2 = 0.00;
                    wrapptotalObj.totalamount3 = 0.00;
                    wrapptotalObj.totalamount1adjust = 0.00;
                    wrapptotalObj.totalamount2adjust = 0.00;
                    wrapptotalObj.totalamount3adjust = 0.00;
                    wrapptotalObj.totalcurrentmonthfees = 0.00;
                    wrapptotalObj.totalamount = 0.00;
                    if(!mapWraperdata.get(objdept).isEmpty()){
                        for(wrapperclass wrappobject:mapWraperdata.get(objdept)){
                            
                            if(wrappobject.amount1 != null){
                                wrapptotalObj.totalamount1 = wrapptotalObj.totalamount1 + wrappobject.amount1;
                            }
                            if(wrappobject.currentmonthfees != null){
                                wrapptotalObj.totalcurrentmonthfees = wrapptotalObj.totalcurrentmonthfees + wrappobject.currentmonthfees;
                            }
                            if(wrappobject.amount2 != null){
                                wrapptotalObj.totalamount2 = wrapptotalObj.totalamount2 + wrappobject.amount2;
                            }
                            if(wrappobject.amount3 != null){
                                wrapptotalObj.totalamount3 = wrapptotalObj.totalamount3 + wrappobject.amount3;
                            }
                            if(wrappobject.amount1adjust != null){
                                wrapptotalObj.totalamount1adjust = wrapptotalObj.totalamount1adjust + wrappobject.amount1adjust;
                            }
                            if(wrappobject.amount2adjust != null){
                                wrapptotalObj.totalamount2adjust = wrapptotalObj.totalamount2adjust + wrappobject.amount2adjust;
                            }
                            if(wrappobject.amount3adjust != null){
                                wrapptotalObj.totalamount3adjust = wrapptotalObj.totalamount3adjust + wrappobject.amount3adjust;
                            }
                            if(wrappobject.total != null){
                                wrapptotalObj.totalamount = wrapptotalObj.totalamount + wrappobject.total;
                            }
                        }
                    }
                    maptotalWraperdata.put(objdept,wrapptotalObj);
                    totalwrapObject.totalamount1 = totalwrapObject.totalamount1 + wrapptotalObj.totalamount1;
                    totalwrapObject.totalcurrentmonthfees = totalwrapObject.totalcurrentmonthfees + wrapptotalObj.totalcurrentmonthfees;
                    totalwrapObject.totalamount2 = totalwrapObject.totalamount2 + wrapptotalObj.totalamount2;
                    totalwrapObject.totalamount3 = totalwrapObject.totalamount3 + wrapptotalObj.totalamount3;
                    totalwrapObject.totalamount1adjust = totalwrapObject.totalamount1adjust + wrapptotalObj.totalamount1adjust;
                    totalwrapObject.totalamount2adjust = totalwrapObject.totalamount2adjust + wrapptotalObj.totalamount2adjust;
                    totalwrapObject.totalamount3adjust = totalwrapObject.totalamount3adjust + wrapptotalObj.totalamount3adjust;
                    totalwrapObject.totalamount = totalwrapObject.totalamount + wrapptotalObj.totalamount;
                }
               
            }
        }
        system.debug('Object msg mapnewdata----'+maptotalWraperdata);
    }
    public void initialSave(){
        List<Department_Forecast__c> InsertLstofDF = new List<Department_Forecast__c>();
       
        if(!mapWraperdata.isEmpty()){
            if(lstOfdeptFC.isEmpty()){
                if(!lstOfDeptdata.Isempty()){
                    for(String objstrg :lstOfDeptdata ){
                        if(!mapWraperdata.get(objstrg).isEmpty()){
                            for(wrapperclass wrapObject :mapWraperdata.get(objstrg)){
                                Department_Forecast__c newDepFcObj = new Department_Forecast__c();
                                newDepFcObj.Department__c = objstrg;
                                newDepFcObj.officename__c = wrapObject.OfficeName;
                                newDepFcObj.Month_1_Amount__c = wrapObject.amount1;
                                newDepFcObj.Month_2_Amount__c = wrapObject.amount2;
                                newDepFcObj.Month_3_Amount__c = wrapObject.amount3;
                                newDepFcObj.Month_1_Name__c = System.today().toStartOfMonth();
                                newDepFcObj.Month_2_Name__c = System.today().addMonths(1).toStartOfMonth();
                                newDepFcObj.Month_3_Name__c = System.today().addMonths(2).toStartOfMonth();
                                InsertLstofDF.add(newDepFcObj);
                            }
                        }
                    }
                }
               
            }else{
                
                System.debug('no empty');
                for(String objstring : lstOfDeptdata){
                    if(lstOfDeptrecords.containsKey(objstring)){
                       
                        for(wrapperclass wrpobj :mapWraperdata.get(objstring) ){
                           
                            if(lstOfDeptrecords.get(objstring).contains(wrpobj.OfficeName)){
                                
                            
                            }else{
                                System.debug('no office');
                                Department_Forecast__c newDepFcObj = new Department_Forecast__c();
                                newDepFcObj.Department__c = objstring;
                                newDepFcObj.officename__c = wrpobj.OfficeName;
                                newDepFcObj.Month_1_Amount__c = wrpobj.amount1;
                                newDepFcObj.Month_2_Amount__c = wrpobj.amount2;
                                newDepFcObj.Month_3_Amount__c = wrpobj.amount3;
                                newDepFcObj.Month_1_Name__c = System.today().toStartOfMonth();
                                newDepFcObj.Month_2_Name__c = System.today().addMonths(1).toStartOfMonth();
                                newDepFcObj.Month_3_Name__c = System.today().addMonths(2).toStartOfMonth();
                                InsertLstofDF.add(newDepFcObj);
                            }   
                       
                    }
                    }else{
                        
                        for(wrapperclass wrpobj :mapWraperdata.get(objstring) ){
                            System.debug('no dept');
                                Department_Forecast__c newDepFcObj = new Department_Forecast__c();
                                newDepFcObj.Department__c = objstring;
                                newDepFcObj.officename__c = wrpobj.OfficeName;
                                newDepFcObj.Month_1_Amount__c = wrpobj.amount1;
                                newDepFcObj.Month_2_Amount__c = wrpobj.amount2;
                                newDepFcObj.Month_3_Amount__c = wrpobj.amount3;
                                newDepFcObj.Month_1_Name__c = System.today().toStartOfMonth();
                                newDepFcObj.Month_2_Name__c = System.today().addMonths(1).toStartOfMonth();
                                newDepFcObj.Month_3_Name__c = System.today().addMonths(2).toStartOfMonth();
                                InsertLstofDF.add(newDepFcObj);
                             
                        }
                    
                    }
                    
                }   
            }   
        
        System.debug('save '+InsertLstofDF.size());
        if(!InsertLstofDF.isEmpty()){
            try{
                insert InsertLstofDF;
            }catch(Exception e){
                ApexPages.addMessages(e);
            }
        }
        }
    }
    public PageReference Exportmethod(){
        system.debug('departments'+departments);
        set<string> setofstring = new set<string>();
        string departmentlist = '';
        string [] lstofdepart = departments.split(',');
        if(!lstofdepart.isEmpty()){
            setofstring.addAll(lstofdepart);
            for(string objstr : setofstring){
                if(objstr != null && string.isNotBlank(objstr)){
                    if(mapWraperdata.containsKey(objstr)){
                        departmentlist = departmentlist+''+objstr+',';
                    }
                }
                
            }
        }
        PageReference reportpage = new PageReference('/'+Label.DepartmentForecast+'?pv0='+departmentlist.replace('&', '%26'));
        reportpage.setRedirect(true);
        return reportpage;
    }
    public void savemethod(){
        List<Department_Forecast__c> UpdateLstofDF = new List<Department_Forecast__c>();
        List<Department_Forecast__c> lsttodelete = new List<Department_Forecast__c>();
        Map<string,set<string>> deptToOffice = new Map<string,set<string>>();
        if(!mapWraperdata.isEmpty()){
            totalwrapObject.totalamount1adjust = 0.00;
            totalwrapObject.totalamount2adjust = 0.00;
            totalwrapObject.totalamount3adjust = 0.00;
            if(!lstOfdeptFC.isEmpty()){
                system.debug('Object msg'+lstOfdeptFC.size());
                for(String objstrng :mapWraperdata.Keyset()){
                    for(Department_Forecast__c objdf :lstOfdeptFC ){
                        /*if(deptToOffice.containsKey(objdf.Department__c)){
                            if(!deptToOffice.get(objdf.Department__c).isEmpty()){
                                if(deptToOffice.get(objdf.Department__c).contains(objdf.officename__c)){
                                    lsttodelete.add(objdf);
                                }else{
                                    deptToOffice.get(objdf.Department__c).add(objdf.officename__c);
                                }
                            }
                        }else{
                            set<string> officelist = new set<string>();
                            officelist.add(objdf.officename__c);
                            deptToOffice.put(objdf.Department__c,officelist);
                        }*/
                              
                        if(objdf.Department__c == objstrng){
                            if(!mapWraperdata.get(objstrng).isEmpty()){
                                maptotalWraperdata.get(objstrng).totalamount1adjust =0.00;
                                maptotalWraperdata.get(objstrng).totalamount2adjust =0.00;
                                maptotalWraperdata.get(objstrng).totalamount3adjust =0.00;
                                
                                for(wrapperclass wrapObj:mapWraperdata.get(objstrng)){
                                    Decimal totalvalue = 0;
                                    if(wrapObj.amount1adjust != null){
                                        maptotalWraperdata.get(objstrng).totalamount1adjust = maptotalWraperdata.get(objstrng).totalamount1adjust + wrapObj.amount1adjust;
                                    }
                                    if(wrapObj.amount2adjust != null){
                                        maptotalWraperdata.get(objstrng).totalamount2adjust = maptotalWraperdata.get(objstrng).totalamount2adjust + wrapObj.amount2adjust;
                                    }
                                    if(wrapObj.amount3adjust != null){
                                        maptotalWraperdata.get(objstrng).totalamount3adjust = maptotalWraperdata.get(objstrng).totalamount3adjust + wrapObj.amount3adjust;
                                    }
                                    if(wrapObj.OfficeName ==objdf.officename__c ){
                                        if(wrapObj.amount1adjust>0){
                                            totalvalue = totalvalue + wrapObj.amount1adjust;
                                        }else{
                                            totalvalue = totalvalue + wrapObj.amount1;
                                        }
                                        if(wrapObj.amount2adjust>0){
                                            totalvalue = totalvalue + wrapObj.amount2adjust;
                                        }else{
                                            totalvalue = totalvalue + wrapObj.amount2;
                                        }
                                        if(wrapObj.amount3adjust>0){
                                            totalvalue = totalvalue + wrapObj.amount3adjust;
                                        }else{
                                            totalvalue = totalvalue + wrapObj.amount3;
                                        }
                                        
                                        wrapObj.total = totalvalue;
                                        
                                        objdf.Adjusted_Month_1_Amount__c = wrapObj.amount1adjust;
                                        objdf.Adjusted_Month_2_Amount__c = wrapObj.amount2adjust;
                                        objdf.Adjusted_Month_3_Amount__c = wrapObj.amount3adjust;
                                        objdf.Submitted__c = true;
                                        UpdateLstofDF.add(objdf);
                                    }
                                }
                                 
                            }
                            
                        }
                        
                        
                    }
                    totalwrapObject.totalamount1adjust = totalwrapObject.totalamount1adjust + maptotalWraperdata.get(objstrng).totalamount1adjust;
                    totalwrapObject.totalamount2adjust = totalwrapObject.totalamount2adjust + maptotalWraperdata.get(objstrng).totalamount2adjust;
                    totalwrapObject.totalamount3adjust = totalwrapObject.totalamount3adjust + maptotalWraperdata.get(objstrng).totalamount3adjust;
                }  
                    
            }   
        }
        /*if(!lstOfdeptFC.isEmpty()){
            for(Department_Forecast__c objdf :lstOfdeptFC ){
                if(deptToOffice.containsKey(objdf.Department__c)){
                    if(!deptToOffice.get(objdf.Department__c).isEmpty()){
                        if(deptToOffice.get(objdf.Department__c).contains(objdf.officename__c)){
                            lsttodelete.add(objdf);
                        }else{
                            deptToOffice.get(objdf.Department__c).add(objdf.officename__c);
                        }
                    }
                }else{
                    set<string> officelist = new set<string>();
                    officelist.add(objdf.officename__c);
                    deptToOffice.put(objdf.Department__c,officelist);
                }
            }
        }*/
        if(!UpdateLstofDF.isEmpty()){
            try{
                update UpdateLstofDF;
                //delete lsttodelete;
            }catch(Exception e){
                 ApexPages.addMessages(e);    
            }
        }
         if(!lsttodelete.isEmpty()){
            try{
                //update UpdateLstofDF;
                delete lsttodelete;
            }catch(Exception e){
                 ApexPages.addMessages(e);    
            }
        }
        
    }
    public class wrapperclass{
        Public string OfficeName{get;set;}
        Public Decimal currentmonthfees{get;set;}
        Public Decimal amount1{get;set;}
        Public Decimal amount2{get;set;}
        Public Decimal amount3{get;set;}
        Public Decimal amount1adjust{get;set;}
        Public Decimal amount2adjust{get;set;}
        Public Decimal amount3adjust{get;set;}
        Public Decimal total{get;set;}
        
        public wrapperclass(){
            
        } 
    }
    
    public class wrapperclassTotal{
        Public string totalLabel{get;set;}
        Public Decimal totalcurrentmonthfees{get;set;}
        Public Decimal totalamount1{get;set;}
        Public Decimal totalamount2{get;set;}
        Public Decimal totalamount3{get;set;}
        Public Decimal totalamount1adjust{get;set;}
        Public Decimal totalamount2adjust{get;set;}
        Public Decimal totalamount3adjust{get;set;}
        Public Decimal totalamount{get;set;}
        
        public wrapperclassTotal(){
            
        } 
    }
    

}