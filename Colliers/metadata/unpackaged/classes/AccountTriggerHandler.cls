public class AccountTriggerHandler {
    
    public static void afterUpdateMethod(List<Account> acList,Map<Id,Account> acOldMap,Map<Id,Account> acNewMap){
        
        Set<Id> setAccIds = new Set<Id>(); // to store the acc ids where ParentId changes
        Set<Id> oldParentAcctIds = new Set<Id>(); // Old ParentId of the record.
        for(account acc : acList){
            if(acc.parentId != acOldMap.get(acc.id).parentId){
                setAccIds.add(acc.id);
                if(acOldMap.get(acc.id).parentId != null){
                    oldParentAcctIds.add(acOldMap.get(acc.id).parentId);
                }
            }
        }
        List<Account> lstAccount = new List<Account>();
        // Qyery to get Ultimate Parent of AccountId and Old ParentId along with relations
        lstAccount = [Select id,account.ParentId,account.Parent.ParentId,account.Parent.Parent.ParentId,account.Parent.Parent.Parent.ParentId,(Select Id,ContactId,AccountId From AccountContactRelations Where AccountId In : setAccIds),(Select id from Contacts) from Account where id In:setAccIds Or id In:oldParentAcctIds];
        
        Set<Id> contactIds = new Set<Id>();
        Map<Id,Id> mapNewUltimateIds = new Map<Id,Id>();
        Map<Id,Id> mapOldUltimateIds = new Map<Id,Id>();
        // get ContactId list which have relation to this account.
        if(!lstAccount.isEmpty()){
            for(Account acc : lstAccount){
                
                if(setAccIds.contains(acc.Id)){
                    if(acc.Contacts != null && !acc.Contacts.isEmpty()){
                        for(Contact con : acc.Contacts){
                            contactIds.add(con.Id);
                        }
                    }
                    if(acc.Parent.Parent.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.Parent.Parent.ParentId))
                    {
                        
                        mapNewUltimateIds.put(acc.id,acc.Parent.Parent.Parent.Parent.ParentId);
                    }
                    else if(acc.Parent.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.Parent.ParentId))
                    {   
                        mapNewUltimateIds.put(acc.id,acc.Parent.Parent.Parent.ParentId);
                    }
                    else if(acc.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.ParentId))
                    {   
                        mapNewUltimateIds.put(acc.id,acc.Parent.Parent.ParentId);
                    }
                    else if(acc.Parent.ParentId != null && String.isNotBlank(acc.Parent.ParentId))
                    {
                        
                        mapNewUltimateIds.put(acc.id,acc.Parent.ParentId);
                    }
                }else if(oldParentAcctIds.contains(acc.Id)){
                    if(acc.Parent.Parent.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.Parent.Parent.ParentId))
                    {
                        
                        mapOldUltimateIds.put(acc.id,acc.Parent.Parent.Parent.Parent.ParentId);
                    }
                    else if(acc.Parent.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.Parent.ParentId))
                    {   
                        mapOldUltimateIds.put(acc.id,acc.Parent.Parent.Parent.ParentId);
                    }
                    else if(acc.Parent.Parent.ParentId != null && String.isNotBlank(acc.Parent.Parent.ParentId))
                    {   
                        mapOldUltimateIds.put(acc.id,acc.Parent.Parent.ParentId);
                    }
                    else if(acc.Parent.ParentId != null && String.isNotBlank(acc.Parent.ParentId))
                    {
                        
                        mapOldUltimateIds.put(acc.id,acc.Parent.ParentId);
                    }
                    else if(acc.ParentId != null && String.isNotBlank(acc.ParentId))
                    {
                        
                        mapOldUltimateIds.put(acc.id,acc.ParentId);
                    }
                }
            }
        
        
            // get all the Accounts related to contactIds under main account
            List<Contact> contactList = [Select Id,AccountId,(Select Id,ContactId,AccountId from AccountContactRelations Where AccountId In:mapOldUltimateIds.Values() OR AccountId In:oldParentAcctIds OR AccountId In:setAccIds )  From Contact where Id In:contactIds];
            Map<id,Map<id,AccountContactRelation>> mapAccntContRelation =new Map<id,Map<id,AccountContactRelation>>();
            Map<id,Set<id>> mapAccntContRelationInsert =new Map<id,Set<id>>();
            if(!contactList.isEmpty()){
                for(Contact con : ContactList){
                                  
                    for(AccountContactRelation ac : con.AccountContactRelations){
                        if(mapAccntContRelation.containskey(ac.contactid)){                        
                                mapAccntContRelation.get(ac.ContactId).put(ac.AccountId,ac);
                            
                              
                        }
                        else{
                            mapAccntContRelation.put(ac.ContactId, new map<Id,AccountContactRelation>{ac.AccountId => ac});
                        }
                    }
                }
                
            }
            System.debug('Map parentIds '+mapNewUltimateIds);
            System.debug('Map mapOldUltimateIds '+mapOldUltimateIds);
            System.debug('Map mapAccntContRelation '+mapAccntContRelation);
            // loop through AccountList to remove old parent relations and create new one.
            List<AccountContactRelation> deleteList = new List<AccountContactRelation>();
            List<AccountContactRelation> acrList = new List<AccountContactRelation>();
            Map<Id,AccountContactRelation> deleteMap = new Map<Id,AccountContactRelation>();
            for(Account acc : lstAccount){
                if(setAccIds.contains(acc.Id) && acc.Contacts != null && !acc.Contacts.isEmpty())
                {
                    for(Contact con : acc.Contacts){
                        if(acc.ParentId != null){
                            if(!mapAccntContRelation.get(con.Id).containskey(acc.ParentId) && !(mapAccntContRelationInsert.containsKey(con.Id) && mapAccntContRelationInsert.get(con.Id).contains( acc.ParentId))){
                                    AccountContactRelation acrNew = new AccountContactRelation();
                                    acrNew.AccountId =  acc.ParentId;
                                    acrNew.ContactId = con.Id;
                                    acrList.add(acrNew);
                                    
                                    if(mapAccntContRelationInsert.containskey(con.Id)){                        
                                        mapAccntContRelationInsert.get(con.Id).add(acc.ParentId);
                                    }
                                    else{
                                        mapAccntContRelationInsert.put(con.Id, new Set<Id>{acc.ParentId });
                                    }
                            }
                            if(mapNewUltimateIds.containsKey(acc.Id) &&!mapAccntContRelation.get(con.Id).containskey(mapNewUltimateIds.get(acc.Id)) && !(mapAccntContRelationInsert.containsKey(con.Id) && mapAccntContRelationInsert.get(con.Id).contains( mapNewUltimateIds.get(acc.Id)))){
                                AccountContactRelation acrNew = new AccountContactRelation();
                                acrNew.AccountId =  mapNewUltimateIds.get(acc.Id);
                                acrNew.ContactId = con.Id;
                                acrList.add(acrNew);
                                
                                if(mapAccntContRelationInsert.containskey(con.Id)){                        
                                    mapAccntContRelationInsert.get(con.Id).add(mapNewUltimateIds.get(acc.Id));
                                }
                                else{
                                    mapAccntContRelationInsert.put(con.Id, new Set<Id>{mapNewUltimateIds.get(acc.Id) });
                                }
                            }
                            if(acOldMap.get(acc.Id).ParentId != null){
                                
                                if(mapAccntContRelation.get(con.Id).containskey(acOldMap.get(acc.Id).ParentId)){
                                    if(!(mapNewUltimateIds.containsKey(acc.Id) && mapNewUltimateIds.get(acc.Id) == acOldMap.get(acc.Id).ParentId)){
                                                                            
                                        deleteMap.put(mapAccntContRelation.get(con.Id).get(acOldMap.get(acc.Id).ParentId).Id,mapAccntContRelation.get(con.Id).get(acOldMap.get(acc.Id).ParentId));
                                    }
                                }
                                if(mapOldUltimateIds.containsKey(acOldMap.get(acc.Id).ParentId)){
                                    if(mapAccntContRelation.get(con.Id).containskey(acOldMap.get(acc.Id).ParentId)){
                                        if(!(mapNewUltimateIds.containsKey(acc.Id) && mapNewUltimateIds.get(acc.Id) == mapOldUltimateIds.get(acOldMap.get(acc.Id).ParentId))){                                            
                                       
                                            deleteMap.put(mapAccntContRelation.get(con.Id).get(acOldMap.get(acc.Id).ParentId).Id,mapAccntContRelation.get(con.Id).get(acOldMap.get(acc.Id).ParentId));
                                        }
                                    }
                                }
                            }else{
                                
                            }
                        }else {
                            if(mapAccntContRelation.get(con.Id).containsKey(acOldMap.get(acc.Id).ParentId)){
                                deleteMap.put(mapAccntContRelation.get(con.Id).get(acOldMap.get(acc.Id).ParentId).Id,mapAccntContRelation.get(con.Id).get(acOldMap.get(acc.Id).ParentId));
                            }
                            if(mapOldUltimateIds.containsKey(acOldMap.get(acc.Id).ParentId) && mapAccntContRelation.get(con.Id).containsKey(mapOldUltimateIds.get(acOldMap.get(acc.Id).ParentId))){
                                deleteMap.put(mapAccntContRelation.get(con.Id).get(mapOldUltimateIds.get(acOldMap.get(acc.Id).ParentId)).Id,mapAccntContRelation.get(con.Id).get(mapOldUltimateIds.get(acOldMap.get(acc.Id).ParentId)));
                            }
                        }
                    }
                }
            }
            if(deleteMap!= NULL && !deleteMap.isEmpty()){
                delete deleteMap.Values();
            
            }
            
            if(acrList!=NULL && !acrList.isEmpty()){
                insert acrList;
            }
            
            
            
            
        }
        
        
        
    }
    public static boolean firstRun = true;
    public static boolean isFirstRun(){
        if(firstRun){
            firstRun = false;
            return true;
            
        }else{
            return firstRun;
        }
    }
    
}