@isTest
public class VALS_ExternalInspectionTrigger_Test
{
    @isTest
    private static void TestMethod1()
    {
        Inspection__c inspObj = New Inspection__c();
        inspObj.Name = 'Test Inspection';
        insert inspObj;
        System.assert(inspObj.id!=NULL);

        External_Inspection__c extInspObj = New External_Inspection__c();
        extInspObj.Inspection__c = inspObj.id;
        //Data for entirely
        extInspObj.Wholly_Mainly__c='Brick';
        extInspObj.Wholly_MainlyCovered__c ='Clay tiles';
        extInspObj.Wholly_MainlyFrames__c = 'uPVC framed';
        extInspObj.Wholly_MainlyFront__c = 'Brick';
        extInspObj.Wholly_MainlyOtherEve__c = 'Brick';
        extInspObj.Parking_Surface_Is__c = 'Gravel';
        extInspObj.Wholly_MainlyStaircases__c = 'Metal';
        extInspObj.Wholly_MainlyRoof__c = 'Pitched';
        extInspObj.Wholly_MainlyWindows__c = 'Single Glazed';
        insert extInspObj;
        System.assert(extInspObj.id!=NULL);

        extInspObj.Partly__c='Brick';
        extInspObj.PartlyConvered__c ='Clay tiles';
        extInspObj.PartlyFrames__c = 'uPVC framed';
        extInspObj.PartlyFront__c = 'Brick';
        extInspObj.PartllyOtherEle__c = 'Brick';
        extInspObj.Parking_Surface_is_Partly__c = 'Gravel';
        extInspObj.PartlyStaircases__c = 'Metal';
        extInspObj.PartllyRoof__c = 'Pitched';
        extInspObj.PartlyWindows__c = 'Single Glazed';
        update extInspObj;

        extInspObj.Wholly_Mainly__c= NULL;
        extInspObj.Wholly_MainlyCovered__c = NULL;
        extInspObj.Wholly_MainlyFrames__c = NULL;
        extInspObj.Wholly_MainlyFront__c = NULL;
        extInspObj.Wholly_MainlyOtherEve__c = NULL;
        extInspObj.Parking_Surface_Is__c = NULL;
        extInspObj.Wholly_MainlyStaircases__c = NULL;
        extInspObj.Wholly_MainlyRoof__c = NULL;
        extInspObj.Wholly_MainlyWindows__c = NULL;
        update extInspObj;

        extInspObj.Partly__c= NULL;
        extInspObj.PartlyConvered__c = NULL;
        extInspObj.PartlyFrames__c = NULL;
        extInspObj.PartlyFront__c = NULL;
        extInspObj.PartllyOtherEle__c = NULL;
        extInspObj.Parking_Surface_is_Partly__c = NULL;
        extInspObj.PartlyStaircases__c = NULL;
        extInspObj.PartllyRoof__c = NULL;
        extInspObj.PartlyWindows__c = NULL;
        extInspObj.Loading_Bays__c = NULL;
        extInspObj.Parking_Type__c = NULL;
        extInspObj.No_Parking_Spaces__c = NULL;
        update extInspObj;
    }
}