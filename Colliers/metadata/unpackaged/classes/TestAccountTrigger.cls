@isTest
public class TestAccountTrigger {
    static testMethod void method1(){
        Account acc1 = new Account();
        acc1.name = 'testaccount1';
        insert acc1;
		
        
        Account acc2 = new Account();
        acc2.Name ='testaccount2';
        acc2.ParentId=acc1.Id;
        insert acc2;
       
        Account acc3 = new Account();
        acc3.Name ='testaccount3';
        acc3.ParentId=acc1.Id;
        insert acc3;
		
		Account acc4 = new Account();
        acc4.Name ='testaccount4';
        acc4.ParentId=acc3.Id;
        insert acc4;
		
		Account acc5 = new Account();
        acc5.Name ='testaccount5';
        acc5.ParentId=acc3.Id;
        insert acc5;

        Contact con1 = new Contact();
        con1.lastname = 'testdata1';
        con1.AccountId = acc1.Id;
        insert con1;
		
		Contact con2 = new Contact();
        con2.lastname = 'testdata2';
        con2.AccountId = acc2.Id;
        insert con2;
		
		Contact con3 = new Contact();
        con3.lastname = 'testdata3';
        con3.AccountId = acc3.Id;
        insert con3;
		
		Contact con4 = new Contact();
        con4.lastname = 'testdata4';
        con4.AccountId = acc4.Id;
        insert con4;
		
		Contact con5 = new Contact();
        con5.lastname = 'testdata5';
        con5.AccountId = acc5.Id;
        insert con5;
		
		Account accB = new Account();
        accB.Name ='testaccountB';
        accB.ParentId=acc1.Id;
        insert accB;
		
		Contact conB = new Contact();
        conB.lastname = 'testdata5';
        conB.AccountId = accB.Id;
        insert conB;
		
		acc2.parentId=accB.id;
		update acc2;
		acc3.parentId=accb.id;
		update acc3;
		
	}
	static testMethod void method2(){
        Account acc1 = new Account();
        acc1.name = 'testaccount1';
        insert acc1;
		
        
        Account acc2 = new Account();
        acc2.Name ='testaccount2';
        acc2.ParentId=acc1.Id;
        insert acc2;
       
        Account acc3 = new Account();
        acc3.Name ='testaccount3';
        acc3.ParentId=acc1.Id;
        insert acc3;
		
		Account acc4 = new Account();
        acc4.Name ='testaccount4';
        acc4.ParentId=acc3.Id;
        insert acc4;
		
		Account acc5 = new Account();
        acc5.Name ='testaccount5';
        acc5.ParentId=acc3.Id;
        insert acc5;

        Contact con1 = new Contact();
        con1.lastname = 'testdata1';
        con1.AccountId = acc1.Id;
        insert con1;
		
		Contact con2 = new Contact();
        con2.lastname = 'testdata2';
        con2.AccountId = acc2.Id;
        insert con2;
		
		Contact con3 = new Contact();
        con3.lastname = 'testdata3';
        con3.AccountId = acc3.Id;
        insert con3;
		
		Contact con4 = new Contact();
        con4.lastname = 'testdata4';
        con4.AccountId = acc4.Id;
        insert con4;
		
		Contact con5 = new Contact();
        con5.lastname = 'testdata5';
        con5.AccountId = acc5.Id;
        insert con5;
		
		Account accB = new Account();
        accB.Name ='testaccountB';
        accB.ParentId=acc1.Id;
        insert accB;
		
		Contact conB = new Contact();
        conB.lastname = 'testdata5';
        conB.AccountId = accB.Id;
        insert conB;
		accB.ParentId=acc2.Id;
		update accB;
        accB.ParentId = null;
        update accB;
	}
	static testMethod void method3(){
        Account acc1 = new Account();
        acc1.name = 'testaccount1';
        insert acc1;
		
        
        Account acc2 = new Account();
        acc2.Name ='testaccount2';
        acc2.ParentId=acc1.Id;
        insert acc2;
       
        Account acc3 = new Account();
        acc3.Name ='testaccount3';
        acc3.ParentId=acc1.Id;
        insert acc3;
		
		Account acc4 = new Account();
        acc4.Name ='testaccount4';
        acc4.ParentId=acc3.Id;
        insert acc4;
		
		Account acc5 = new Account();
        acc5.Name ='testaccount5';
        acc5.ParentId=acc3.Id;
        insert acc5;

        Contact con1 = new Contact();
        con1.lastname = 'testdata1';
        con1.AccountId = acc1.Id;
        insert con1;
		
		Contact con2 = new Contact();
        con2.lastname = 'testdata2';
        con2.AccountId = acc2.Id;
        insert con2;
		
		Contact con3 = new Contact();
        con3.lastname = 'testdata3';
        con3.AccountId = acc3.Id;
        insert con3;
		
		Contact con4 = new Contact();
        con4.lastname = 'testdata4';
        con4.AccountId = acc4.Id;
        insert con4;
		
		Contact con5 = new Contact();
        con5.lastname = 'testdata5';
        con5.AccountId = acc5.Id;
        insert con5;
		
		Account accB = new Account();
        accB.Name ='testaccountB';
        accB.ParentId=acc1.Id;
        insert accB;
		
		Contact conB = new Contact();
        conB.lastname = 'testdata5';
        conB.AccountId = accB.Id;
        insert conB;
		
		acc4.parentId = accB.id;
		update acc4;
		acc5.parentId = accB.id;
		update acc5;
						
	}
        
        
		
		
		
		
        
		        
      
    

        
    
    
}