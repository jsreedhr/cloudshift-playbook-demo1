/**
 *  Class Name: Assert
 *  Description: This class adds some additional assertions for simplification in test classes
 *  Company: CloudShift
 *  CreatedDate: 29/05/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Gavin Palmer             29/05/2018                 Orginal Version
 *
 */
@isTest
public with sharing class Assert {

	public static void pageMessageExists(final String expectedPageMessage) {

        System.assert(ApexPages.hasMessages(), 'There are no messages on the page');

        Boolean containsMessage = false;
        for (ApexPages.Message message : ApexPages.getMessages()) {
            String actualErrorMessage = message.getSummary();
            if (actualErrorMessage.containsIgnoreCase(expectedPageMessage)) {
                containsMessage = true;
                break;
            }
        }
        System.assert(
            containsMessage,
            'The message \'' + expectedPageMessage + '\' was not found in the page messages. Actual: ' + ApexPages.getMessages()
        );
    }
}