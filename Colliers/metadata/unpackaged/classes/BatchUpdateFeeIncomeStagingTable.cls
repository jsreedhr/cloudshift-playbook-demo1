global class BatchUpdateFeeIncomeStagingTable implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'select id,Accrual__c,Allocation__c,Instructing_Client__c,Invoice__c,Type__c,Accrual__r.Job__c,Allocation__r.Job__c,Invoice__r.Opportunity__c,Ultimate_Instructing_Parent_Name__c ,Ultimate_Instructing_ParentId__c  ,Ultimate_Instructing_Parent_Sector__c ,Ultimate_Instructing_Parent_Geo_Class__c ,Ultimate_Instructing_Parent_Sub_Sector__c ,Ultimate_Instructing_Parent_Class__c ,Invoice__r.Opportunity__r.accountid,Accrual__r.Job__r.accountid,Allocation__r.Job__r.accountid from Fee_Income_Staging_Table__c ';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Fee_Income_Staging_Table__c> scope)
    {   
        Set<id> acIdSet = new Set<id>();
        
        for(Fee_Income_Staging_Table__c fist : scope){
            if(fist.Invoice__c != null && fist.Invoice__r.Opportunity__c != null && fist.Invoice__r.Opportunity__r.accountid != null){
                    fist.Instructing_Client__c = fist.Invoice__r.Opportunity__r.accountid;
                    acIdSet.add(fist.Invoice__r.Opportunity__r.accountid);
                    
                
            }    
            
        }
        
        Map<Id, Account> accountMap = new Map<Id, Account>([Select Id, Ultimate_Parent_Name__c, Ultimate_Parent__c, Company_Classification__c, Geographical_Client__c, Ultimate_Parent_Sector__c, Ultimate_Parent_Sub_Sector__c From Account Where Id IN :acIdSet]);
        system.debug('accountMap----->'+accountMap);
        if(!scope.isEmpty()){
            for(Fee_Income_Staging_Table__c objFeeIncomeStagingTable :scope)
            {
                if(objFeeIncomeStagingTable.Instructing_Client__c != null)
                {
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Name__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Name__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Name__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent__c != null) {
                        String textString= accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent__c;
                        Integer int1 = textString.indexOf('/');
                        objFeeIncomeStagingTable.Ultimate_Instructing_ParentId__c = textString.substring(int1 + 1,int1 + 16);
                        
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sector__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Sector__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sector__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Geographical_Client__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Geo_Class__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Geographical_Client__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sub_Sector__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Sub_Sector__c = accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Ultimate_Parent_Sub_Sector__c;
                    }
                    if(accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c != null){
                        objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c = null;
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Developer' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Developer,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Lender' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Lender,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Occupier' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Occupier,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Owner' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Owner,';
                        }
                        if( accountMap.get(objFeeIncomeStagingTable.Instructing_Client__c).Company_Classification__c.contains( 'Service Provider' ) ) {
                            objFeeIncomeStagingTable.Ultimate_Instructing_Parent_Class__c += 'Service Provider';
                        }
                        
                    }
                }
            }
        }
        update scope;
      
    }  
    

    global void finish(Database.BatchableContext BC)
    {
        
    }
}