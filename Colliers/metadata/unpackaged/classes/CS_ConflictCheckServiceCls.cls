// ===========================================================================
// Component: CS_ConflictCheckServiceCls
//    Author: Nidheesh N
// Copyright: 2018 by Dquotient
//   Purpose: Service Class for CS_ConflictCheck
// ===========================================================================
// ChangeLog: 2016-11-17 Nidheesh Initial version.
//            
// ===========================================================================
public without sharing class CS_ConflictCheckServiceCls {
     // Selector class for opportunity and opportunity line items
    public class CS_ConflictCheckSelector extends BaseSelector
    {
        /*****************************************************************************************************************************
         *
         * Constants & Private Variables
         *
         *****************************************************************************************************************************/
        private List<String> OPPORTUNITY_SELECT_FIELDS = new List<String>  
        {
                'id'
                
        };
          
        
        /**
         * Default Constructor
         */
        public CS_ConflictCheckSelector()
        {

        }

        /*****************************************************************************************************************************
         *
         * Public Methods
         *
        ****************************************************************************************************************************/
        /**
        * This method returns the Map of recordId, Opportunity object. The fields retrieved are Listed above.
        * @param setOpportunityId - Record Ids
        * @return Returns map of Id, Opportunity
        */
        public Map<Id,Opportunity> getOpportunityById(Set<Id> setOpportunityId){
            List <String> whereClause = new List <String> {'id IN :setOpportunityId'};
            String soql = super.soqlBuilder('Opportunity',OPPORTUNITY_SELECT_FIELDS,whereClause,true,null);                   
            system.debug(soql);
            return new Map<Id, Opportunity>((List<Opportunity>) Database.query(soql));
        } 
    }
    
    /**
    *  Method Name: returnOpp
    *  Description: Method to get opportunity records.
    *  Param:  Id
    *  Return: opportunity record
    */
    public Opportunity getOppRecord(Id recordId) {
        if (String.isNotBlank(recordId)) {
            //Query to retrieve Opportunity records.
            Map<Id, Opportunity> opptyMap = new CS_ConflictCheckSelector().getOpportunityById(new Set<Id>{recordId});
            if(opptyMap != null && opptyMap.size() > 0)
                return opptyMap.get(recordId);
        }
        return null;
    }
    
    /**
     *  Method Name: saveOpp
     *  Description: Method to get update opportunity
     *  Param:  Opportunity object, order values
     *  Return: orderIssue
    */
    public void saveOpp(Opportunity objOpportunity){
        update objOpportunity;
    } 
    
    /**
    *  Method Name: returnOpp
    *  Description: Method to get opportunity records.
    *  Param:  Id
    *  Return: opportunity record
    */
    public  List<Opportunity> getOpportunityById(Set<Id> setOpportunityId){
        return (new CS_ConflictCheckSelector().getOpportunityById(setOpportunityId).values() );
    } 
    
    /**
    *  Method Name: insertOpportunity
    *  Description: Method to get Opportunity LineItems records.
    *  Param:  Set<Id>
    *  Return: opportunity record
    */
    public  List<opportunity> insertOpportunity(List<opportunity> opportunityList) {
        insert   opportunityList;
        return   opportunityList;
    }
    
    /**
    *  Method Name: getOpportunityWithChilds
    *  Description: Method to get Opportunity LineItems records.
    *  Param:  Set<Id>
    *  Return: opportunity record
    */
    public  List<Opportunity> getOpportunityWithChilds(String oppId) {
        List<Opportunity> oppRecord = [SELECT Id,Name,AccountId,Account.Name,Job_Number__c,Account.Ultimate_Parent_Name__c,Account.ultimateParentId__c,Date_Instructed__c, No_property_conflict_found_for_this_job__c, (select Job__c,Property__r.Name,Property__r.Id,Property__r.Street__c,Property__r.Town__c,Property__r.Country__c,Property__r.Post_Code__c,Property__c,Property__r.Property_Type__c,Property__r.Geolocation__c,Property__r.Geolocation__Latitude__s,Property__r.Geolocation__Longitude__s  from Job_Property_Junction__r where property__c != null Order by CreatedDate desc ),(select Id,Company__c,Company__r.Name,Job__c From Other_Parties__r WHERE Job__c =:oppId) FROM Opportunity WHERE Id=:oppId ];
        return oppRecord;
    }
    
    /**
    *  Method Name: getLastFilterValues
    *  Description: Method to get last filter values of user.
    *  Param:  Opportunity
    *  Return: List<Opportunity>
    */ 
    /*public User_ConflictCheck_Info__c getLastFilterValues(Id userId,Id jobId){
        if(userId != null && String.isNotBlank(userId)){
            //return [Select Id,Opportunity__c,User__c,Company_Filter_Value__c,Instructed_Date_Range__c,Last_Checked_Date__c,Property_Info__c,Property_Type_Filter_Value__c,Radius__c,Search_Format__c from User_ConflictCheck_Info__c where User__c=:userId AND Opportunity__c=:jobId Limit 1];
        }else{
          return null;  
        }
        
    }*/
}