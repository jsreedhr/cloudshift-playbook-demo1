/**
 *  Class Name: cs_codingStructureTriggerHandler  
 *  Description: This is a Trigger Handler Class for trigger on Coding_Structure__c
 *  Company: dQuotient
 *  CreatedDate:21/10/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Nidheesh              21/10/2016                 Orginal Version
 *
 */
public class cs_codingStructureTriggerHandler {
    public static boolean run = true;
    /**
    *Metod to Avoiding Recursive Trigger Calls
    */
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
    
    /**
     *  Method Name: insertcodingStructure 
     *  Description: Method to insert coding structure which doesn't have worktype.'
     *  Param:  Trigger.newMap
     *  Return: None
    */
    public static void insertcodingStructure(List<Coding_Structure__c>codestructnewmap){
        
        List<Coding_Structure__c> lstOfCodingstruct = new List<Coding_Structure__c>();
        Map<Coding_Structure__c,String> mapofobject = new Map<Coding_Structure__c,String>();
        set<String> lstofDepartment = new set<String>();
        System.debug('codelist'+codestructnewmap);
        if(codestructnewmap != null && codestructnewmap.size()>0){
            for(Coding_Structure__c codstrObj :codestructnewmap){
                if(codstrObj.Work_Type__c == null){
                    if(codstrObj.Department__c != null && codstrObj.Office__c != null && codstrObj.Staff__c != null){
                        mapofobject.put(codstrObj, codstrObj.Department__c);
                        lstofDepartment.add(codstrObj.Department__c);
                        System.debug(codstrObj.Department__c);
                    } 
                }  
            }
        }
        System.debug('---set'+lstofDepartment);
        List<Work_Type__c> lstOfWorktype = new List<Work_Type__c>();
        lstOfWorktype = [SELECT Id,Name,Department__c FROM Work_Type__c WHERE Department__c IN:lstofDepartment];
        Map<String,List<Work_Type__c>> mapofdepTowork = new Map<String,List<Work_Type__c>>();
        if(mapofobject != null && mapofobject.size()>0){
            for(String objstring :mapofobject.values()){
                List<Work_Type__c> listofcode = new List<Work_Type__c>();
                if(lstOfWorktype != null && lstOfWorktype.size()>0){
                    for(Work_Type__c obj :lstOfWorktype ){
                        if(obj.Department__c == objstring){
                            listofcode.add(obj); 
                        }
                    }
                }
                mapofdepTowork.put(objstring,listofcode);
            }
            for(Coding_Structure__c codeObj :mapofobject.keySet()){
                if(mapofdepTowork.containsKey(mapofobject.get(codeObj))){
                    List<Work_Type__c> lstwork =  mapofdepTowork.get(mapofobject.get(codeObj));
                    if(lstwork != null && lstwork.size()>0){
                        codeObj.Work_Type__c = lstwork[0].Id;
                        lstwork.remove(0);
                    }
                    if(lstwork != null && lstwork.size()>0){
                        for(Integer i=0;i<lstwork.size();i++){
                            Coding_Structure__c  newCodeObj = new Coding_Structure__c();
                            newCodeObj.Staff__c = codeObj.Staff__c;
                            newCodeObj.Department__c = codeObj.Department__c;
                            newCodeObj.Office__c = codeObj.Office__c;
                            newCodeObj.Work_Type__c = lstwork[i].Id;
                            lstOfCodingstruct.add(newCodeObj);
                        }
                    }
                }
            }
        }
        if(lstOfCodingstruct != null && lstOfCodingstruct.size()>0){
            try{
                upsert lstOfCodingstruct;
            }catch(Exception e){
                ApexPages.addMessages(e);
            }
        }
    }
    
    /**
     *  Method Name: insertCompanyCostCentre
     *  Description: Method to update the Company and Cost Centre
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void insertCompanyCostCentre(List<Coding_Structure__c> lstCodingStructure){
        
        List<Coding_Structure__c> lstCodingStructureNew = new List<Coding_Structure__c>();
        
        for(Coding_Structure__c objCodingStructure: lstCodingStructure){
            
            if(!String.isBlank(objCodingStructure.Department__c)){
                lstCodingStructureNew.add(objCodingStructure);
            }
        }
        
        calculateCompanyCostCentre(lstCodingStructureNew);
    }
    
    /**
     *  Method Name: updateCompanyCostCentre
     *  Description: Method to update the Company and Cost Centre
     *  Param:  Trigger.oldMap, Trigger.newMap
     *  Return: None
    */
    public static void updateCompanyCostCentre(map<id,Coding_Structure__c> mapOldCodingStructure, map<id,Coding_Structure__c> mapNewCodingStructure){
        
        List<Coding_Structure__c> lstCodingStructureNew = new List<Coding_Structure__c>();
        
        for(Coding_Structure__c objCodingStructure: mapNewCodingStructure.values()){
            
            if(objCodingStructure.Department__c != mapOldCodingStructure.get(objCodingStructure.id).Department__c){
                lstCodingStructureNew.add(objCodingStructure);
            }
        }
        
        calculateCompanyCostCentre(lstCodingStructureNew);
    }
    
    /**
     *  Method Name: calculateCompanyCostCentre 
     *  Description: Method to update the Company and Cost Centre
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void calculateCompanyCostCentre(List<Coding_Structure__c> lstCodingStructure){
        
        map<String, String> mapDeptCompany = new map<String, String>();
        map<String, String> mapDeptCostCentre = new map<String, String>();
        List<Department__c> lstDepartment = new List<Department__c>();
        lstDepartment = Department__c.getall().values();
        
        for(Department__c objDept: lstDepartment){
            
            if(!String.isBlank(objDept.Department__c)){
                if(!String.isBlank(objDept.Company__c)){
                    mapDeptCompany.put(objDept.Department__c, objDept.Company__c);
                }
                
                if(!String.isBlank(objDept.Cost_Centre__c)){
                    mapDeptCostCentre.put(objDept.Department__c, objDept.Cost_Centre__c);
                }
            }
        }
        
        for(Coding_Structure__c objCodingStructure: lstCodingStructure){
            
            if(!String.isBlank(objCodingStructure.Department__c)){
                
                if(mapDeptCompany.containsKey(objCodingStructure.Department__c)){
                    String company = mapDeptCompany.get(objCodingStructure.Department__c);
                    objCodingStructure.Company__c = company;
                }else{
                    objCodingStructure.Company__c = null;
                }
                
                if(mapDeptCostCentre.containsKey(objCodingStructure.Department__c)){
                    String costCentre = mapDeptCostCentre.get(objCodingStructure.Department__c);
                    objCodingStructure.Department_Cost_Code_Centre__c = costCentre;
                }else{
                    objCodingStructure.Department_Cost_Code_Centre__c = null;
                }
                
            }
            
        }
        
    }
    
    /**
     *  Method Name: updateAllocation 
     *  Description: Method to update 
     *  Param:  Trigger.oldMap, Trigger.newMap,
     *  Return: None
    */
    public static void updateAllocation(map<id, Coding_Structure__c> mapOldCodingStructure, map<id, Coding_Structure__c> mapNewCodingStructure ){
        
        List<Coding_Structure__c> lstCodingStructureNew = new List<Coding_Structure__c>();
        set<id> setCodingStructureIds = new set<id>();
        for(Coding_Structure__c objCodingStructure: mapNewCodingStructure.values()){
            
            if(objCodingStructure.Status__c != mapOldCodingStructure.get(objCodingStructure.id).Status__c && objCodingStructure.Status__c == 'Inactive'){
                setCodingStructureIds.add(objCodingStructure.id);
            }
        }
        
        if(!setCodingStructureIds.isEmpty()){
            
            List<Allocation__c> lstAllocation = new List<Allocation__c>();
            List<Allocation__c> lstAllocationUpdate = new List<Allocation__c>();
            lstAllocation = [Select id, name,
                                        Main_Allocation__c,
                                        Complete__c,
                                        Coding_Structure__c
                                        From
                                        Allocation__c
                                        where 
                                        Coding_Structure__c in : setCodingStructureIds
                                        and Main_Allocation__c = false 
                                        and Complete__c = false];
                                        
            for(Allocation__c objAllocation : lstAllocation){
                
                objAllocation.Complete__c = true;
                lstAllocationUpdate.add(objAllocation);
                
            }
            
            
                if(!lstAllocationUpdate.isEmpty()){
                    Database.saveresult[] lstDMLResult = Database.update(lstAllocationUpdate, false); 
                }
        
        }
    }
}