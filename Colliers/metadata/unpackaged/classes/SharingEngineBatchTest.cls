/**
*  Class Name: SharingEngineBatchTest
*  Description: Test class for SharingEngineBatch

*  Company: CloudShift
*  CreatedDate: 26/06/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  ----------------------------------------------------------- 
*  Kaspars Rezgalis	   26/06/2018					Created
*
*/

@isTest(seeAllData=false)
private class SharingEngineBatchTest{
	private static Map<String, SObject> setupSharedTestData(){
		Map<String, SObject> retValues = new Map<String, SObject>();

		AssignId__c assignId = TestObjectHelper.createAssignId();
		assignId.AssignNo__c = 1;
		AssignAccountId__c assignAccId = TestObjectHelper.createAssignAccCode();
		Job_Amount__c jobAmount = TestObjectHelper.createJobAmount();
		colliers_Vat_Number__c vatNum = (colliers_Vat_Number__c)SmartFactory.createSObject('colliers_Vat_Number__c',false);
		currency_Symbol__c curr= (currency_Symbol__c)SmartFactory.createSObject('currency_Symbol__c',false);

		Bypass_Configuration__c setting = new Bypass_Configuration__c(
			Is_Sharing_Off__c = false,
			SetupOwnerId = UserInfo.getUserId()
		);

		Group testGroup = new Group(
			Name = 'testGroup'
		);

		Database.Insert(new List<SObject>{assignId, assignAccId, jobAmount, vatNum, curr, setting, testGroup}, true);
		retValues.put('Group', testGroup);

		Account a = TestObjectHelper.createAccount();
		a.Name = 'SharingTestAccount';
		Database.Insert(a, true);
		retValues.put('Account', a);

		Staff__c staffMember = TestObjectHelper.createStaff();
		Database.Insert(staffMember, true);
		retValues.put('Staff__c', staffMember);

		return retValues;
	}

	@isTest 
	private static void test_errors(){
		Map<String, SObject> testData = setupSharedTestData();
		String sObjectNameToUse = 'Opportunity';

		ConfigurationManager configManager = ConfigurationManager.getInstance();
		configManager.turnSharingOff();
		configManager.commitConfigurationChanges();

		SharingEngineConfigurationManager configurationManager = SharingEngineConfigurationManager.getInstance(sObjectNameToUse);
		configurationManager.setBatchSharingIsRunning_ON();
		configurationManager.commitConfigurationChanges();

		SharingEngine_Status__c tmp_settings = SharingEngine_Status__c.getInstance(sObjectNameToUse);
		System.assertNotEquals(null, tmp_settings, 'Custom settings for managing sharing off/on should be present in the system for ' + sObjectNameToUse);

		SharingEngineBatch batch;
		Boolean errorTriggered = false;
		try{
			batch = new SharingEngineBatch(null);
		}catch(Exception e){
			System.assert(e.getMessage().contains(SharingEngineBatch.ERROR_NO_SOBJECT), 'An error should be thrown - user was trying to initiate batch class without specifying SObject. Actual: ' + e.getMessage());
			errorTriggered = true;
		}
		System.assert(errorTriggered, 'Error should have been triggered - user was trying to initiate batch class without specifying SObject.');

		errorTriggered = false;
		try{
			batch = new SharingEngineBatch('DummyName__');
		}catch(Exception e){
			System.assert(e.getMessage().contains(SharingEngineBatch.ERROR_INVALID_SOBJECT), 'An error should be thrown - user was trying to initiate batch class with incorrect SObject name. Actual: ' + e.getMessage());
			errorTriggered = true;
		}
		System.assert(errorTriggered, 'Error should have been triggered - user was trying to initiate batch class with incorrect SObject name.');

		errorTriggered = false;
		try{
			batch = new SharingEngineBatch(sObjectNameToUse);
		}catch(Exception e){
			System.assert(e.getMessage().contains(SharingEngineBatch.ERROR_SHARING_IS_OFF), 'An error should be thrown - user was trying to initiate batch class while sharing is turned off. Actual: ' + e.getMessage());
			errorTriggered = true;
		}
		System.assert(errorTriggered, 'Error should have been triggered - user was trying to initiate batch class while sharing is turned off (in both SharingEngineConfigurationManager and ConfigurationManager).');

		configManager.turnSharingOn();
		configManager.commitConfigurationChanges();

		errorTriggered = false;
		try{
			batch = new SharingEngineBatch(sObjectNameToUse);
		}catch(Exception e){
			System.assert(e.getMessage().contains(SharingEngineBatch.ERROR_SHARING_IS_OFF), 'An error should be thrown - user was trying to initiate batch class while sharing is turned off. Actual: ' + e.getMessage());
			errorTriggered = true;
		}
		System.assert(errorTriggered, 'Error should have been triggered - user was trying to initiate batch class while sharing is turned off (in SharingEngineConfigurationManager; on in ConfigurationManager).');
	}

	@isTest 
	private static void test_processOpps(){
		Map<String, SObject> testData = setupSharedTestData();

		ConfigurationManager configManager = ConfigurationManager.getInstance();
		configManager.turnSharingOff();
		configManager.commitConfigurationChanges();

		Group testGroup = (Group)testData.get('Group');

		Sharing_Rule__c s = new Sharing_Rule__c(
			Name = 'OpportunityTestRule',
			Object__c = 'Opportunity',
			Description__c = 'Apex TEST rule',
			Access_Level__c = SharingEngine.SHARING_ACCESS_EDIT,
			Logic__c = SharingEngine.SHARING_LOGIC_ANY,
			Shared_With__c = testGroup.Id,
			Change_Status__c = SharingEngine.SHARING_RULE_CHANGE_STATUS_IN_PROGRESS
		);
		Database.Insert(s, true);

		Sharing_Condition__c sc = new Sharing_Condition__c(
			Sharing_Rule__c = s.Id,
			Field_API_Name__c = 'Name',
			Operator__c = SharingEngine.SHARING_CONDITION_CONTAINS,
			Value__c = 'included opp',
			Index__c = 1
		);
		Database.Insert(sc, true);

		s.Change_Status__c = SharingEngine.SHARING_RULE_CHANGE_STATUS_COMPLETED;
		Database.Update(s, true);

		Account a = (Account)testData.get('Account');
		List<Opportunity>oppsToShare = TestObjectHelper.createMultipleOpportunity(a, 20);
		for(Integer i=0; i<oppsToShare.size();i++){
			oppsToShare[i].Name = sc.Value__c + ' ' + i;
		}
		List<Opportunity>oppsNotToShare = TestObjectHelper.createMultipleOpportunity(a, 20);
		for(Integer i=0; i<oppsNotToShare.size();i++){
			oppsNotToShare[i].Name = 'excluded opp ' + i;
		}

		Database.Insert(oppsToShare, true);
		Database.Insert(oppsNotToShare, true);

		configManager.turnSharingOn();
		configManager.commitConfigurationChanges();

		Test.startTest();
			List<OpportunityShare>sh = [SELECT Id FROM OpportunityShare WHERE RowCause=:SharingEngine.SHARING_ROW_CAUSE_STD_OBJECTS AND UserOrGroupId=:s.Shared_With__c];
			System.assertEquals(0, sh.size(), 'There should be no manual share records because no records have been updated yet (so sharing rules evaluation did not happen yet).');

			SharingEngineBatch batch = new SharingEngineBatch('Opportunity');
			Database.executeBatch(batch, 50);
		Test.stopTest();

		List<Error_Log__c>errors = new List<Error_Log__c>([SELECT Details__c FROM Error_Log__c WHERE Log_Type__c='Sharing Error']);
		System.assertEquals(0, errors.size(), 'There should be no error log entries for sharing error when running batch for opportunities properly. Actual: '+errors);

		sh = [SELECT Id FROM OpportunityShare WHERE RowCause=:SharingEngine.SHARING_ROW_CAUSE_STD_OBJECTS AND UserOrGroupId=:s.Shared_With__c];
		System.assertEquals(20, sh.size(), 'Number of manual share records should be 20 - that is the number of test opportunities with name matching sharing rule condition.');		
	}

	@isTest 
	private static void test_processAllocations(){
		Map<String, SObject> testData = setupSharedTestData();

		ConfigurationManager configManager = ConfigurationManager.getInstance();
		configManager.turnSharingOff();
		configManager.commitConfigurationChanges();

		Group testGroup = (Group)testData.get('Group');
		Account a = (Account)testData.get('Account');
		Staff__c staffMember = (Staff__c)testData.get('Staff__c');

		Opportunity o = TestObjectHelper.createOpportunity(a);
		Database.Insert(o, true);

		Sharing_Rule__c s = new Sharing_Rule__c(
			Name = 'AllocationTestRule',
			Object__c = 'Allocation__c',
			Description__c = 'Apex TEST rule',
			Access_Level__c = SharingEngine.SHARING_ACCESS_EDIT,
			Logic__c = SharingEngine.SHARING_LOGIC_ANY,
			Shared_With__c = testGroup.Id,
			Change_Status__c = SharingEngine.SHARING_RULE_CHANGE_STATUS_IN_PROGRESS
		);
		Database.Insert(s, true);

		Sharing_Condition__c sc = new Sharing_Condition__c(
			Sharing_Rule__c = s.Id,
			Field_API_Name__c = 'Job__c',
			Operator__c = SharingEngine.SHARING_CONDITION_EQUALS,
			Value__c = o.Id,
			Index__c = 1
		);
		Database.Insert(sc, true);

		s.Change_Status__c = SharingEngine.SHARING_RULE_CHANGE_STATUS_COMPLETED;
		Database.Update(s, true);

		List<Allocation__c>allocToShare = TestObjectHelper.createMultipleAllocation(o, staffMember, 50);
		Database.Insert(allocToShare, true);

		configManager.turnSharingOn();
		configManager.commitConfigurationChanges();

		Test.startTest();
			List<Allocation__Share>sh = [SELECT Id FROM Allocation__Share WHERE RowCause=:SharingEngine.SHARING_ROW_CAUSE_CUSTOM_OBJECTS AND UserOrGroupId=:s.Shared_With__c];
			System.assertEquals(0, sh.size(), 'There should be no manual share records because no records have been updated yet (so sharing rules evaluation did not happen yet).');

			SharingEngineBatch batch = new SharingEngineBatch('Allocation__c');
			Database.executeBatch(batch, 50);
		Test.stopTest();

		List<Error_Log__c>errors = new List<Error_Log__c>([SELECT Details__c FROM Error_Log__c WHERE Log_Type__c='Sharing Error']);
		System.assertEquals(0, errors.size(), 'There should be no error log entries for sharing error when running batch for allocations properly. Actual: '+errors);

		sh = [SELECT Id FROM Allocation__Share WHERE RowCause=:SharingEngine.SHARING_ROW_CAUSE_CUSTOM_OBJECTS AND UserOrGroupId=:s.Shared_With__c];
		System.assertEquals(50, sh.size(), 'Number of manual share records should be 50 - that is the number of test allocations with parent job id matching sharing rule condition.');

		System.assert(!SharingEngineConfigurationManager.getInstance('Allocation__c').isBatchSharingAlreadyRunning(), 'Custom setting should indicate that batch sharing is no longer running once processing in batch class is finished.');
	}
}