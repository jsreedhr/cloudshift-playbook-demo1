public class ActivateAccountContactRelationshipCTL{
    public id PrevCompanyID;
    public ActivateAccountContactRelationshipCTL(ApexPages.StandardController controller){
        PrevCompanyID = ApexPages.currentPage().getParameters().get('id');
    }
    public pagereference activateRelationship(){
        Previous_Company__c objprevco = [select id,Start_Date__c,Department__c,Title__c,End_Date__c,Company__c,IsActive__c,Contact__c,IsDirect__c,Roles__c,External_ID_CM_UK__c,
        Floor__c,Position__c,Receive_Email__c,Receive_Hard_Copy__c,Suite__c from Previous_Company__c where id=:PrevCompanyID];
        if(objprevco != null){
            AccountContactRelation objAcctContactRel = new AccountContactRelation();
            if(objprevco.Company__c != null)
                objAcctContactRel.accountId = objprevco.Company__c;
            if(objprevco.Contact__c != null)        
                objAcctContactRel.ContactId = objprevco.Contact__c;
            if(objprevco.Position__c != null && objprevco.Position__c != '')
                objAcctContactRel.Position__c = objprevco.Position__c;
            if(objprevco.External_ID_CM_UK__c != null && objprevco.External_ID_CM_UK__c != '')
                objAcctContactRel.External_ID_CM_UK__c = objprevco.External_ID_CM_UK__c;
            if(objprevco.Roles__c != null && objprevco.Roles__c != '')
                objAcctContactRel.Roles = objprevco.Roles__c;
            if(objprevco.Floor__c != null && objprevco.Floor__c != '')
                objAcctContactRel.Floor__c = objprevco.Floor__c;
            if(objprevco.Suite__c != null && objprevco.Suite__c != '')
                objAcctContactRel.Suite__c = objprevco.Suite__c ;
            if(objprevco.Department__c != null && objprevco.Department__c != '')
                objAcctContactRel.Department__c = objprevco.Department__c;
            if(objprevco.Title__c != null && objprevco.Title__c != '')
                objAcctContactRel.Title__c = objprevco.Title__c;
            objAcctContactRel.IsActive = true;
            objAcctContactRel.Receive_Email__c = objprevco.Receive_Email__c;
            objAcctContactRel.Receive_Hard_Copy__c = objprevco.Receive_Hard_Copy__c;
            objAcctContactRel.StartDate = objprevco.Start_Date__c; 
            objAcctContactRel.EndDate = objprevco.End_Date__c;
            Database.SaveResult dbsr = Database.insert(objAcctContactRel,true);
            if(dbsr.isSuccess()){                
                delete objprevco;
                return new PageReference('/'+objAcctContactRel.contactid);
            }
        }        
        return null;
    }
}