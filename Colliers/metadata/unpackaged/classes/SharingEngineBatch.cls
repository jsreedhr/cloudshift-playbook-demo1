/**
*  Class Name: SharingEngineBatch
*  Description: Batch class for triggering recalculation of sharing rules via ustom-built records sharing functionality.
*  Tests available in SharingEngineBatchTest
*
*  Company: CloudShift
*  CreatedDate: 05/06/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  ----------------------------------------------------------- 
*  Kaspars Rezgalis		05/06/2018					Initial version
*
*/

global class SharingEngineBatch implements Database.Batchable<sObject>, Database.Stateful{
	private String sObjectToProcess;
	private SharingEngineConfigurationManager sharingconfigManager;
	private ConfigurationManager configManager;
	private SharingEngine sharingEngineInstance;
	private String soqlQuery = '';

	public static final String ERROR_NO_SOBJECT = 'No SObject specified for batch class to operate on';
	@TestVisible private static final String ERROR_INVALID_SOBJECT = 'Invalid SObject specified for batch class to operate on - ';
	public static final String ERROR_SHARING_IS_OFF = 'Sharing is turned off or already running.';
	public static final String SUCCESS_BATCH_QUEUED = 'Batch job was successfully queued.';

	public class sharingEngineBatchException extends Exception{

	}

	global SharingEngineBatch(String whichSObject){
		System.Debug('***SharingEngineBatch run***');

		sObjectToProcess = whichSObject;
		System.Debug('sObjectToProcess='+sObjectToProcess);

		if(String.isBlank(sObjectToProcess)){
			throw new sharingEngineBatchException(ERROR_NO_SOBJECT);
		}

		Map<String, Schema.SObjectType> mapOfSObjectsInOrg = Schema.getGlobalDescribe();
		if(!mapOfSObjectsInOrg.containsKey(sObjectToProcess)){
			throw new sharingEngineBatchException(ERROR_INVALID_SOBJECT + sObjectToProcess);
		}

		configManager = ConfigurationManager.getInstance();
		System.Debug('ConfigurationManager.isSharingOff()='+configManager.isSharingOff());

		sharingconfigManager = SharingEngineConfigurationManager.getInstance(sObjectToProcess);
		System.Debug('sharingconfigManager.isBatchSharingOff()='+sharingconfigManager.isBatchSharingAlreadyRunning());

		if(sharingconfigManager.isBatchSharingAlreadyRunning() || configManager.isSharingOff()){
			throw new sharingEngineBatchException(ERROR_SHARING_IS_OFF);
		}

		Schema.SObjectType sObjectTypeToProcess = mapOfSObjectsInOrg.get(sObjectToProcess);
		Boolean isCustomObject = sObjectTypeToProcess.getDescribe().isCustom();
		//Schema.DescribeSObjectResult sObjectTypeToProcessDescribeResult = sObjectTypeToProcess.getDescribe();
		//Set<String>sObjectFields = sObjectTypeToProcessDescribeResult.fields.getMap().KeySet();
		
		sharingEngineInstance = new SharingEngine();
		Set<String>sObjectFields = sharingEngineInstance.getOnlyRelevantFieldNamesForSObject(sObjectToProcess);
		if(!isCustomObject){
			sObjectFields.add('Sharing_Rows__c');
		}

		soqlQuery = 'SELECT ' + String.join(new List<String>(sObjectFields), ', ') + ' FROM ' + sObjectToProcess; 
	}

	global Database.QueryLocator start(Database.BatchableContext bc){
		sharingconfigManager.setBatchSharingIsRunning_ON();
		sharingconfigManager.commitConfigurationChanges();

		//sharingEngineInstance = new SharingEngine();
		return Database.getQueryLocator(soqlQuery);
	}
	
	global void execute(Database.BatchableContext bc, List<SObject> scope){
		sharingEngineInstance.updateSharingForSObjects(scope);
	}

	global void finish(Database.BatchableContext bc){
		AsyncApexJob batchJob = [SELECT Id, ExtendedStatus, NumberOfErrors FROM AsyncApexJob WHERE Id = :bc.getJobId()]; 

		sharingconfigManager.setBatchSharingIsRunning_OFF();
		sharingconfigManager.commitConfigurationChanges();
	}
}