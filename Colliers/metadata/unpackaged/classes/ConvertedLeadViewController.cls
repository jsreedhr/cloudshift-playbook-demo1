public with sharing class ConvertedLeadViewController {
    public List<Lead> record { get;set; }

    public ConvertedLeadViewController(){
        record = [select firstname,lastname,company from lead where Status = 'Qualified'];
        
    }
}