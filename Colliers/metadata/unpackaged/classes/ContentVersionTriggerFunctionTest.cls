/**
 *  @Class Name:    ContentVersionTriggerFunctionTest
 *  @Description:   This is a test class for ContentVersionTriggerFunction
 *  @Company:       dQuotient
 *  CreatedDate:    27/11/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 * 	Swetha											   Original Version
 */
@isTest
public class ContentVersionTriggerFunctionTest {
    
    public static testMethod void RemoveSpecialCharFromFileNameTest()
    {
        
        ContentVersion testContentInsert =new ContentVersion(); 
        testContentInsert.ContentURL='<a target="_blank" href="http://www.google.com/" rel="nofollow">http://www.google.com/</a>';
        testContentInsert.Title ='November-27-2017-16:37:34-IST-+0530.jpg'; 
        insert testContentInsert; 
        
    }
}