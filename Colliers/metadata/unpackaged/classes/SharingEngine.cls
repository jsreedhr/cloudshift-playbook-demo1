/**
*  Class Name: SharingEngine
*  Description: Wrapper class for handling custom-built records sharing functionality. Tests available in SharingEngineTest
*
*  Company: CloudShift
*  CreatedDate: 29/05/2018
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer		   Modification Date		   Comments
*  -----------------------------------------------------------
*  Kaspars Rezgalis		29/05/2018					Core wrapper class functionality and sharing process for Opportunities and Allocations
*
*/

public class SharingEngine{
//
	public class sharingEngineException extends Exception{

	}

	@testVisible private static Set<Schema.DisplayType>DISPLAYTYPES_TEXT = new Set<Schema.DisplayType>{
																Schema.DisplayType.String,
																Schema.DisplayType.Email,
																Schema.DisplayType.Textarea,
																Schema.DisplayType.Phone,
																Schema.DisplayType.Picklist
															};

	private static Set<Schema.DisplayType>DISPLAYTYPES_NUMBER = new Set<Schema.DisplayType>{
																Schema.DisplayType.Double,
																Schema.DisplayType.Currency,
																Schema.DisplayType.Integer,
																Schema.DisplayType.Percent
															};

	private static Set<Schema.DisplayType>DISPLAYTYPES_DATES = new Set<Schema.DisplayType>{
																Schema.DisplayType.Date,
																Schema.DisplayType.Datetime
															};

	private static Set<Schema.DisplayType>DISPLAYTYPES_ONLY_EQUALS_OR_NOT = new Set<Schema.DisplayType>{
																Schema.DisplayType.Reference,
																Schema.DisplayType.Id
															};

	public static final String SHARING_LOGIC_ALL = 'AND';
	public static final String SHARING_LOGIC_ANY = 'OR';
	public static final String SHARING_LOGIC_CUSTOM = 'Custom';

	public static final String SHARING_RULE_CHANGE_STATUS_IN_PROGRESS = 'In Progress';
	public static final String SHARING_RULE_CHANGE_STATUS_COMPLETED = 'Completed';

	public static final String SHARING_CONDITION_EQUALS = 'equals';
	public static final String SHARING_CONDITION_NOT_EQUALS = 'not equal to';
	public static final String SHARING_CONDITION_LESS_THAN = 'less than';
	public static final String SHARING_CONDITION_LESS_OR_EQUAL = 'less or equal';
	public static final String SHARING_CONDITION_GREATER_THAN = 'greater than';
	public static final String SHARING_CONDITION_GREATER_OR_EQUAL = 'greater or equal';
	public static final String SHARING_CONDITION_STARTS_WITH = 'starts with';
	public static final String SHARING_CONDITION_CONTAINS = 'contains';
	public static final String SHARING_CONDITION_DOES_NOT_CONTAIN = 'does not contain';
	public static final String SHARING_CONDITION_INCLUDES = 'includes';
	public static final String SHARING_CONDITION_EXCLUDES = 'excludes';

	@TestVisible private static final String SHARING_ACCESS_EDIT = 'Edit';
	@TestVisible private static final String SHARING_ACCESS_VIEW = 'Read';
	@TestVisible private static final String SHARING_ROW_CAUSE_STD_OBJECTS = 'Manual';
	@TestVisible private static final String SHARING_ROW_CAUSE_CUSTOM_OBJECTS = 'AutoSharing__c';

	private List<SObject>sObjectsToProcessForSharing;
	private String sObjectToProcess;
	private Boolean isCustomObject;
	private String sObjectSharesEntity;
	private Schema.SObjectType sObjectSharesType;
	private Map<String, Schema.SObjectField> sObjectFields;

	private List<String>errorMessagesForLog = new List<String>();

	private Map<Id, Sharing_Rule__c>sharingRules;
	public static Boolean checkRecursiveRunOK = true;

	public Set<String> getOnlyRelevantFieldNamesForSObject(String sObjectName){
	//it is expected that this method is called only from SharingEngineBatch (so sObjectName is correct)
	//this is done to ensure batch query runs only for fields that are mentioned in sharing conditions
		sObjectToProcess = sObjectName;
		if(sharingRules==null){
			fetchSharingRules();
		}

		Set<String>fieldsForSOQLquery = new Set<String>();

		for(Sharing_Rule__c rule : sharingRules.Values()){
			for(Sharing_Condition__c condition : rule.Sharing_Conditions__r){
				fieldsForSOQLquery.add(condition.Field_API_Name__c);
			}
		}

		return fieldsForSOQLquery;
	}

	public void updateSharingForSObjects(List<SObject>records){
        if(records==null || records.isEmpty()){
			return;
		}

		System.Debug('***SharingEngine.updateSharingForSObjects run***');

		sObjectsToProcessForSharing = records;

		Map<SObject, Id>sharesToInsert = new Map<SObject, Id>();  //share to insert => sharing rule reference
		//establish SObject details (based on first record in list) and sObject for sObject share, and remove any previously manually added sharing
		Id firstSObjectId = (Id)sObjectsToProcessForSharing[0].get('Id');

		Schema.SObjectType sObjectTypeToProcess = firstSObjectId.getSObjectType();
		Schema.DescribeSObjectResult sObjectTypeToProcessDescribeResult = sObjectTypeToProcess.getDescribe();
		sObjectToProcess = sObjectTypeToProcessDescribeResult.getName();

		isCustomObject = sObjectTypeToProcessDescribeResult.isCustom();
		sObjectFields = sObjectTypeToProcessDescribeResult.fields.getMap();

		sObjectSharesEntity = (sObjectToProcess.endsWith('__c') ? sObjectToProcess.removeEnd('c') : sObjectToProcess)+'Share';
		sObjectSharesType = Schema.getGlobalDescribe().get(sObjectSharesEntity);

		SharingEngineConfigurationManager sharingConfigurationManager = SharingEngineConfigurationManager.getInstance(sObjectToProcess);

		if(ConfigurationManager.getInstance().isSharingOff()){
			return;
		}

        checkRecursiveRunOK = false;

		removeExistingSharingForSobjects();

		//find relevant sharing rules and apply most permissive rule for each group
		if(sharingRules==null){
			fetchSharingRules();
		}

		for(SObject singlesObjectForSharing : sObjectsToProcessForSharing){
			System.Debug('--Processing record=' + singlesObjectForSharing.get('Id'));
			Set<Id>groupsWithSharingRuleAdded = new Set<Id>();

			for(Sharing_Rule__c sr : sharingRules.Values()){
				if(groupsWithSharingRuleAdded.contains(sr.Shared_With__c)){
					continue;
				}

				System.Debug('---Processing rule=' + sr.Name + ' (' + sr.Id + ')');

				Boolean ruleIsMatched = false;

				if(sr.Logic__c == SHARING_LOGIC_ALL || sr.Logic__c == SHARING_LOGIC_ANY){
					for(Sharing_Condition__c sc : sr.Sharing_Conditions__r){
						if(!sObjectFields.containsKey(sc.Field_API_Name__c)){
							errorMessagesForLog.add('Skipping rule. Field not found: ' + sc.Field_API_Name__c);
							ruleIsMatched = false;
							break;
						}

						Object fieldValue = singlesObjectForSharing.get(sc.Field_API_Name__c);

						Boolean conditionIsMatched = compareValueToCondition(sObjectFields.get(sc.Field_API_Name__c).getDescribe().getType(), fieldValue, sc.Operator__c, sc.Value__c);

						if(conditionIsMatched && sr.Logic__c == SHARING_LOGIC_ANY){
							ruleIsMatched = true;
							break;
						}else if(sr.Logic__c == SHARING_LOGIC_ALL && !conditionIsMatched){
							ruleIsMatched = false;
							break;
						}else if(sr.Logic__c == SHARING_LOGIC_ALL && conditionIsMatched){
							ruleIsMatched = true;
						}
					}
				}else if(sr.Logic__c == SHARING_LOGIC_CUSTOM){
					ruleIsMatched = doInterpreterEval(sr, singlesObjectForSharing);
				}

				if(ruleIsMatched){
					String accessLevel = (sr.Access_Level__c==SHARING_ACCESS_EDIT ? 'Edit': 'Read');
					String parentIdFieldName = (isCustomObject ? 'ParentID' : sObjectToProcess + 'Id');
					String shareNameWithAccessLevel = (isCustomObject ? 'AccessLevel' : sObjectToProcess + 'AccessLevel');
					String shareRowCause = (isCustomObject ? SHARING_ROW_CAUSE_CUSTOM_OBJECTS : SHARING_ROW_CAUSE_STD_OBJECTS);

					SObject newShare = sObjectSharesType.newSObject();
					newShare.put(shareNameWithAccessLevel, accessLevel);
					newShare.put(parentIdFieldName, singlesObjectForSharing.get('Id'));
					newShare.put('RowCause', shareRowCause);
					newShare.put('UserOrGroupId', sr.Shared_With__c);
					sharesToInsert.put(newShare, sr.Id);
					groupsWithSharingRuleAdded.add(sr.Shared_With__c);
				}
			}
		}

		List<SObject>tmp = new List<SObject>(sharesToInsert.KeySet());
		List<Database.SaveResult> dml_results = Database.Insert(tmp, false);

		for(Integer i=0;i<dml_results.size();i++){
			Database.SaveResult r = dml_results.get(i);
			SObject record = tmp.get(i);
			if(!r.isSuccess() || Test.isRunningTest()){
				Sharing_Rule__c affectedRule = sharingRules.get(sharesToInsert.get(record));
				for(Database.Error err : r.getErrors()){
					String errorMsg = 'Share record: ' + record;
					errorMsg += '\nSharing rule: ' + affectedRule.Name + ' (' + affectedRule.Id + ')';
					errorMsg += '\nDML error: ' + err.getStatusCode() + ': ' + err.getMessage();
					System.Debug(errorMsg);
					errorMessagesForLog.add(errorMsg);
				}
			}
		}

		if(sObjectToProcess=='Opportunity'){
			sharingForOpportunitiesPostProcessing(tmp);
		}

		if(!errorMessagesForLog.isEmpty()){
			SharingEngine.saveSharingErrorData(errorMessagesForLog);
		}

		checkRecursiveRunOK = true;
		System.Debug('***SharingEngine.updateSharingForSObjects END***');
	}

	private void fetchSharingRules(){
		sharingRules = new Map<Id, Sharing_Rule__c>([SELECT Name, Logic__c, Shared_With__c, Access_Level__c, Custom_Logic__c,
										(SELECT Field_API_Name__c, Operator__c, Value__c, Index__c FROM Sharing_Conditions__r)
									  FROM Sharing_Rule__c
									  WHERE Object__c=:sObjectToProcess AND Change_Status__c=:SHARING_RULE_CHANGE_STATUS_COMPLETED ORDER BY Permission_Level__c DESC]);
	}

	private void removeExistingSharingForSobjects(){
		List<SObject>sharesToDelete = new List<SObject>();

		if(isCustomObject){
			Set<Id>sObjectRecordIDs = new Set<Id>();
			for(SObject sObjectRecord : sObjectsToProcessForSharing){
				sObjectRecordIDs.add((Id)sObjectRecord.get('Id'));
			}
			sharesToDelete = Database.query('SELECT Id FROM ' + sObjectSharesEntity + ' WHERE ParentID IN :sObjectRecordIDs AND RowCause=:SHARING_ROW_CAUSE_STD_OBJECTS');
		}else{
			for(SObject sObjectRecord : sObjectsToProcessForSharing){
				if(sObjectToProcess=='Opportunity'){  //if extended for other standard objects in the same manner, this IF statement becomes obsolete
					String sharingRows = (String)sObjectRecord.get('Sharing_Rows__c');
					if(String.isNotBlank(sharingRows)){
						for(String sharingRecordID : sharingRows.split(',')){
							sharesToDelete.add(sObjectSharesType.newSObject(sharingRecordID));
						}
					}
				}
			}
		}

		Database.Delete(sharesToDelete, false);
	}

	private Boolean compareValueToCondition(Schema.DisplayType fieldType, Object fieldValue, String operator, String filterValueString){
		Boolean isMatched = false;
		filterValueString = (filterValueString==null ? '' : filterValueString);
		Object filterValue = filterValueString;

		System.Debug('----compareValueToCondition run');
		System.Debug('-----fieldType='+fieldType);
		System.Debug('-----fieldValue='+fieldValue);
		System.Debug('-----operator='+operator);
		System.Debug('-----filterValue='+filterValue);

		if(DISPLAYTYPES_ONLY_EQUALS_OR_NOT.contains(fieldType) && (operator==SHARING_CONDITION_EQUALS || operator==SHARING_CONDITION_NOT_EQUALS)){
			isMatched = (operator==SHARING_CONDITION_EQUALS ? fieldValue==filterValue : fieldValue<>filterValue);

		}else if(DISPLAYTYPES_TEXT.contains(fieldType)){
			String fieldValueConverted = (String) fieldValue;
			fieldValueConverted = (fieldValueConverted==null ? '' : fieldValueConverted);
			if(operator==SHARING_CONDITION_EQUALS || operator==SHARING_CONDITION_NOT_EQUALS){
				isMatched = (operator==SHARING_CONDITION_EQUALS ? fieldValueConverted==filterValueString : fieldValueConverted<>filterValueString);
			}else if(operator==SHARING_CONDITION_STARTS_WITH){
				isMatched = String.isNotBlank(fieldValueConverted) && fieldValueConverted.startsWith(filterValueString);
			}else if(operator==SHARING_CONDITION_CONTAINS || operator==SHARING_CONDITION_DOES_NOT_CONTAIN){
				isMatched = String.isNotBlank(fieldValueConverted) && (operator==SHARING_CONDITION_CONTAINS ? fieldValueConverted.contains(filterValueString) : !fieldValueConverted.contains(filterValueString));
			}else{
				errorMessagesForLog.add('Unknown filter operator for text field: ' + operator);
			}

		}else if(DISPLAYTYPES_NUMBER.contains(fieldType)){
			Decimal fieldValueConverted = (Decimal)fieldValue;
			Decimal filterValueConverted = Decimal.ValueOf(filterValueString.replace(',', ''));
			if(operator==SHARING_CONDITION_EQUALS || operator==SHARING_CONDITION_NOT_EQUALS){
				isMatched = (operator==SHARING_CONDITION_EQUALS ? fieldValueConverted==filterValueConverted : fieldValueConverted<>filterValueConverted);
			}else if(operator==SHARING_CONDITION_LESS_THAN || operator==SHARING_CONDITION_LESS_OR_EQUAL){
				isMatched = (operator==SHARING_CONDITION_LESS_THAN ? (fieldValueConverted<filterValueConverted) : (fieldValueConverted<=filterValueConverted));
			}else if(operator==SHARING_CONDITION_GREATER_THAN || operator==SHARING_CONDITION_GREATER_OR_EQUAL){
				isMatched = (operator==SHARING_CONDITION_GREATER_THAN ? (fieldValueConverted>filterValueConverted) : (fieldValueConverted>=filterValueConverted));
			}else{
				errorMessagesForLog.add('Unknown filter operator for number field: ' + operator);
			}

		}else if(DISPLAYTYPES_DATES.contains(fieldType)){
			Datetime fieldValueConverted, filterValueConverted;
			if(fieldType==Schema.DisplayType.Date){
				Date tmpDate_FieldValue = (Date)fieldValue;
				Date tmpDate_FilterValue = Date.ValueOf(filterValueString.toLowerCase());
				fieldValueConverted = Datetime.newInstance(tmpDate_FieldValue.year(), tmpDate_FieldValue.month(), tmpDate_FieldValue.day());
				filterValueConverted = Datetime.newInstance(tmpDate_FilterValue.year(), tmpDate_FilterValue.month(), tmpDate_FilterValue.day());
			}else{
				fieldValueConverted = (Datetime)fieldValue;
				filterValueConverted = Datetime.valueOf(filterValueString.toLowerCase().replace('t', ' ').replace('z', ' '));
			}
			if(operator==SHARING_CONDITION_EQUALS || operator==SHARING_CONDITION_NOT_EQUALS){
				isMatched = (operator==SHARING_CONDITION_EQUALS ? fieldValueConverted==filterValueConverted : fieldValueConverted<>filterValueConverted);
			}else if(operator==SHARING_CONDITION_LESS_THAN || operator==SHARING_CONDITION_LESS_OR_EQUAL){
				isMatched = (operator==SHARING_CONDITION_LESS_THAN ? (fieldValueConverted<filterValueConverted) : (fieldValueConverted<=filterValueConverted));
			}else if(operator==SHARING_CONDITION_GREATER_THAN || operator==SHARING_CONDITION_GREATER_OR_EQUAL){
				isMatched = (operator==SHARING_CONDITION_GREATER_THAN ? (fieldValueConverted>filterValueConverted) : (fieldValueConverted>=filterValueConverted));
			}else{
				errorMessagesForLog.add('Unknown filter operator for date/datimetime field: ' + operator);
			}

		}else if(fieldType==Schema.DisplayType.Multipicklist){
			String fieldValueString = (String) fieldValue;
			Set<String> fieldValueConverted = new Set<String>();
			if(String.isNotBlank(fieldValueString)){
				fieldValueConverted.addAll(fieldValueString.split(';'));
			}

			Set<String> filterValueConverted = new Set<String>();
			if(String.isNotBlank(filterValueString)){
				filterValueConverted.addAll(filterValueString.split(';'));
			}

			if(operator==SHARING_CONDITION_INCLUDES){
				for(String f: filterValueConverted){
					if(fieldValueConverted.contains(f)){
						isMatched = true;
						break;
					}
				}
			}else if(operator==SHARING_CONDITION_EXCLUDES){
				System.Debug('filterValueConverted='+filterValueConverted);
				System.Debug('fieldValueConverted='+fieldValueConverted);
				for(String f: filterValueConverted){
					if(!fieldValueConverted.contains(f)){
						isMatched = true;
						break;
					}
				}
			}else{
				errorMessagesForLog.add('Unknown filter operator for multipicklist field: ' + operator);
			}
		}else if(fieldType==Schema.DisplayType.Boolean){
			Boolean fieldValueConverted = (Boolean) fieldValue;
			Boolean filterValueConverted = (filterValueString.toLowerCase()=='true');
			isMatched = (operator==SHARING_CONDITION_EQUALS ? fieldValueConverted==filterValueConverted : fieldValueConverted<>filterValueConverted);
		}else{
			errorMessagesForLog.add('Unknown field type for comparison: ' + fieldType);
		}


		//TO-DO - address? ("within" operator works for geolocation only)

		System.Debug('------isMatched='+isMatched);

		return isMatched;
	}


	private void sharingForOpportunitiesPostProcessing(List<OpportunityShare>shares){
		//if extended for other standard objects in the same manner, this postprocessing will become generic
		Map<Id, List<Id>>oppSharingRowIDs = new Map<Id, List<Id>>(); //opportunity Id => all sharing records that were inserted
		for(OpportunityShare os : shares){
			if(String.isNotBlank(os.Id)){
				List<Id>tmpList = (oppSharingRowIDs.containsKey(os.OpportunityId) ? oppSharingRowIDs.get(os.OpportunityId) : new List<Id>());
				tmpList.add(os.Id);
				oppSharingRowIDs.put(os.OpportunityId, tmpList);
			}
		}

		List<Opportunity>oppsForUpdate = new List<Opportunity>();
		for(SObject oppSObject : sObjectsToProcessForSharing){
			Id opportunityId = (Id)oppSObject.get('Id');
			String rowIds = (oppSharingRowIDs.containsKey(opportunityId) ? String.join(oppSharingRowIDs.get(opportunityId), ',') : '');

			Opportunity o = new Opportunity(
				Id = opportunityId,
				Sharing_Rows__c = rowIds
			);
			oppsForUpdate.add(o);
		}

		Database.Update(oppsForUpdate, false);
	}

	public static void saveSharingErrorData(List<String>errorMessagesForLog){
		ErrorLogsHelper errorLogging = ErrorLogsHelper.getInstance();
		errorLogging.addListToErrorLog(errorMessagesForLog);
		errorLogging.setErrorLogType('Sharing Error');
		errorLogging.saveErrorLogs();
	}



	/***********************************
		INTERPRETER FOR CUSTOM LOGIC
	************************************/
	private enum TokenType {IS_NUMBER, IS_OPERATOR, IS_LPAREN, IS_RPAREN}

	private class LexerData{
		private TokenType tokenType;
		private Object tokenValue;

		private LexerData(TokenType t, Object v){
			this.tokenType = t;
			this.tokenValue = v;
		}
	}

	private Id currentSharingRuleId; //for error log
	private Integer currentIndexPosition;
	private Map<Integer, LexerData>lexerDataMap;
	private Map<Integer, Sharing_Condition__c>numberedConditions;

	private Boolean doInterpreterEval(Sharing_Rule__c sharingRuleWithConditions, SObject singlesObjectForSharing){
		Boolean expressionIsMatched = false;

		System.Debug('----doInterpreterEval run');
		System.Debug('-----Custom logic='+sharingRuleWithConditions.Custom_Logic__c);

		try{
			doLexer(sharingRuleWithConditions.Custom_Logic__c);
		}catch(Exception e){
			errorMessagesForLog.add('Exception encountered while processing rule ID: ' + sharingRuleWithConditions.Id + ' - ' + e.getMessage() + ' - ' + e.getStackTraceString());
			return false;
		}

		currentIndexPosition = 0;
		currentSharingRuleId = sharingRuleWithConditions.Id;
		numberedConditions = new Map<Integer, Sharing_Condition__c>();
		for(Sharing_Condition__c c : sharingRuleWithConditions.Sharing_Conditions__r){
			numberedConditions.put(Integer.ValueOf(c.Index__c), c);
		}

		try{
			expressionIsMatched = doInterpretExpression(singlesObjectForSharing);
		}catch(Exception e){
			errorMessagesForLog.add('Exception encountered while processing rule ID: ' + sharingRuleWithConditions.Id + ' - ' + e.getMessage() + ' - ' + e.getStackTraceString());
			return false;
		}


		return expressionIsMatched;
	}

	private void doLexer(String condition){
		lexerDataMap = new Map<Integer, LexerData>();

		List<String>conditionChunks = SharingEngine.splitConditionTextIntoTokens(condition);

		Integer current_index = 0;
		for(String conditionLiteral : conditionChunks){
			conditionLiteral = conditionLiteral.trim();

			if(String.isBlank(conditionLiteral)){
				continue;
			}

			if(conditionLiteral.isNumeric()){
				LexerData digit = new LexerData(TokenType.IS_NUMBER, Integer.valueOf(conditionLiteral));
				lexerDataMap.put(current_index, digit);

			}else if(conditionLiteral=='(' || conditionLiteral==')'){
				LexerData parenthesis = new LexerData((conditionLiteral=='(' ? TokenType.IS_LPAREN : TokenType.IS_RPAREN), conditionLiteral);
				lexerDataMap.put(current_index, parenthesis);

			}else if(conditionLiteral.isAlpha() && (conditionLiteral=='AND' || conditionLiteral=='OR')){
				LexerData operator = new LexerData(TokenType.IS_OPERATOR, conditionLiteral);
				lexerDataMap.put(current_index, operator);

			}else{
				throw new sharingEngineException('Unknown token type when interpreting custom sharing logic conditions: ' + conditionLiteral);
			}
			current_index++;
		}
	}

	public static List<String> splitConditionTextIntoTokens(String condition){
		return condition.replace('(', '( ').replace(')', ' )').split(' ');
	}

	private Boolean doInterpretExpression(SObject singlesObjectForSharing){
		Boolean expressionIsMatched = doEvalTermResult(singlesObjectForSharing);

		while(currentIndexPosition<lexerDataMap.size() && lexerDataMap.get(currentIndexPosition).tokenType == TokenType.IS_OPERATOR){
			lexerData thisLexerData = lexerDataMap.get(currentIndexPosition);
			currentIndexPosition++;

			if(thisLexerData.tokenValue == 'AND'){
				expressionIsMatched = doEvalTermResult(singlesObjectForSharing) && expressionIsMatched;
			}else if(thisLexerData.tokenValue == 'OR'){
				expressionIsMatched = doEvalTermResult(singlesObjectForSharing) || expressionIsMatched;
			}
		}

		return expressionIsMatched;
	}

	private Boolean doEvalTermResult(SObject singlesObjectForSharing){
		Boolean conditionIsMatched;
		LexerData thisLexerData = lexerDataMap.get(currentIndexPosition);

		if(thisLexerData.tokenType == TokenType.IS_NUMBER){
			currentIndexPosition++;
			Integer conditionIndex = (Integer)thisLexerData.tokenValue;
			Sharing_Condition__c relevantCondition;
			if(numberedConditions.containsKey(conditionIndex)){
				relevantCondition = numberedConditions.get(conditionIndex);

				if(!sObjectFields.containsKey(relevantCondition.Field_API_Name__c)){
					throw new sharingEngineException('Skipping rule. Field not found: ' + relevantCondition.Field_API_Name__c);
				}

				Object fieldValue = singlesObjectForSharing.get(relevantCondition.Field_API_Name__c);

				conditionIsMatched = compareValueToCondition(sObjectFields.get(relevantCondition.Field_API_Name__c).getDescribe().getType(), fieldValue, relevantCondition.Operator__c, relevantCondition.Value__c);
			}

		}else if(thisLexerData.tokenType == TokenType.IS_LPAREN){
			currentIndexPosition++;
			conditionIsMatched = doInterpretExpression(singlesObjectForSharing);

			LexerData nextLexerData = lexerDataMap.get(currentIndexPosition); //system proceeds to next token automatically which is expected to be right bracket
			if(nextLexerData==null || nextLexerData.tokenType != TokenType.IS_RPAREN){
				throw new sharingEngineException('Skipping rule ID=' + currentSharingRuleId + '. Custom logic invalid - right parenthesis expected. Actual token: ' + nextLexerData);
			}else{
				currentIndexPosition++;
			}
		}

		return conditionIsMatched;
	}
}