/**
 *  Class Name: test_csjournalController 
 *  Description: Test class for the controller of journalPage
 *  Company: dQuotient
 *  CreatedDate: 13/10/2016 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nidheesh               17/10/2016                  Orginal Version          
 *
 */
@isTest
private class test_csjournalController {

 @testsetup
    static void setupData(){
        //create account
        Account a = TestObjectHelper.createAccount();
        insert a;
        
        Property__c propertyObj = TestObjectHelper.createProperty();
        propertyObj.Street_No__c ='Park Lane';
        propertyObj.Post_Code__c ='W1K 3DD';
        propertyObj.Country__c='United Kingdom';
        propertyObj.Geolocation__Latitude__s=51.51;
        propertyObj.Geolocation__Longitude__s=-0.15;
        insert propertyObj;
        System.assert(propertyObj.id!=NULL);
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
        System.debug('opps---->'+opps);
        list<Job_Property_Junction__c> jobPropertyList = new list<Job_Property_Junction__c>();
        for(Opportunity opp : opps)
        {   
            Job_Property_Junction__c jobPropObj =TestObjecthelper.JobPrptyJn(opp,propertyObj);
            jobPropertyList.add(jobPropObj);
        }
        insert jobPropertyList;
       
        //create realted Opportunity
        List <Contact> cons = TestObjectHelper.createMultipleContact(a, 10);
        integer loopNo = 1;
        for(Contact objContact: cons){
            objContact.Email = 'test'+loopNo+'@test.com';
        }

        insert cons;
        System.debug('cons---->'+cons);
        Staff__c staffObj =  new Staff__c();
        staffObj.Name = 'staffdummy';
        //staffObj.Email__c = 'staff@gmail.com';
        staffObj.Email__c = 'stvcxa1456423ff@gmail.com';
        staffObj.HR_No__c = String.valueOf(Math.round(Math.random()*1000));
        staffObj.active__c = true;
        insert staffObj;
        Work_Type__c workObj = new Work_Type__c();
        workObj.Name = 'worktype';
        insert workObj;
        Coding_Structure__c codestructObj = new Coding_Structure__c();
        //codestructObj.Office__c = 'office';
        codestructObj.Status__c = 'Active';
        //codestructObj.Department__c = 'DepartMent';
        codestructObj.Work_Type__c = workObj.Id;
        codestructObj.Staff__c = staffObj.Id;
        insert codestructObj;
        for(Opportunity opp : opps)
        {
            opp.Coding_Structure__c = codestructObj.Id;
        }
        update opps;
        //create journal transaction custom setting
        Journal_Transaction_Num__c journaltran = new Journal_Transaction_Num__c();
        journaltran.Pattern__c = 'xxxxxxxxxx';
        journaltran.Transaction_Num__c = 1;
        journaltran.Name = 'test';
        insert journaltran;
        //Create Disbursements__c data for opportunity
        opportunity x = [Select id from opportunity Limit 1];
        Contact b = [Select id from Contact Limit 1];
        Invoice__c inv = new Invoice__c();
        inv.Opportunity__c= x.id;
        inv.contact__c=b.id;
        inv.Status__c = 'Approved';
        insert inv;
        Invoice__c invobj = new Invoice__c();
        invobj.Opportunity__c= x.id;
        invobj.contact__c=b.id;
        invobj.Status__c = 'Approved';
        insert invobj;
        Allocation__c allocObj = new Allocation__c();
        allocObj.Job__c = x.Id;
        allocobj.Assigned_To__c = staffObj.Id;
        allocobj.Complete__c = false;
        insert allocObj;
        Forecasting__c forcastobj = new Forecasting__c();
        forcastobj.Allocation__c = allocObj.Id;
        forcastobj.Amount__c = 100;
        forcastobj.CS_Forecast_Date__c = system.today();
        insert forcastobj;
        Allocation__c allocObject = new Allocation__c();
        allocObject.Job__c = x.Id;
        allocObject.Assigned_To__c = staffObj.Id;
        allocObject.Allocation_Amount__c = 1000;
        allocobject.Complete__c = false;
        insert allocObject;
        Allocation__c exallocObject = new Allocation__c();
        exallocObject.Job__c = x.Id;
        exallocObject.externalcompany__c = a.Id;
        exallocObject.externalcontact__c = b.id;
        exallocObject.Allocation_Amount__c = 1000;
        exallocobject.Complete__c = false;
        insert exallocObject;
        Forecasting__c forcastobj1 = new Forecasting__c();
        forcastobj1.Allocation__c = allocObject.Id;
        forcastobj1.Amount__c = 1000;
        forcastobj1.CS_Forecast_Date__c = system.today();
        insert forcastobj1;
        Invoice_Allocation_Junction__c invoAllocObj = new Invoice_Allocation_Junction__c();
        invoAllocObj.Forecasting__c = forcastobj1.Id;
        invoAllocObj.Allocation__c =allocObject.Id;
        invoAllocObj.Invoice__c =inv.Id;
        insert invoAllocObj;
        Invoice_Allocation_Junction__c invoAllocObject = new Invoice_Allocation_Junction__c();
        invoAllocObject.Forecasting__c = forcastobj.Id;
        invoAllocObject.Allocation__c =allocObj.Id;
        invoAllocObject.Invoice__c =inv.Id;
        insert invoAllocObject;
        Journal__c journalObj = new Journal__c();
        journalObj.Allocation__c = allocObject.Id;
        journalObj.Allocation_Value__c = 1000;
        insert journalObj;
    }
    static testMethod void testCS_allocationControllerForOpps(){
        System.debug('opp test---->');
        opportunity a = [Select id from opportunity Limit 1];
        Staff__c staffobject = [Select id from Staff__c Limit 1];
        Test.startTest();
        PageReference pf = new PageReference('/apex/cs_journalPage?id='+a.Id);
        Test.setCurrentPage(pf);
        cs_journalsController con = new cs_journalsController();
        con.initialJournal();
        ApexPages.currentPage().getParameters().put('invId','1' );
        con.fetchjournalDetails();
        if(con.lstofJournalWrapper != null && con.lstofJournalWrapper.size()>1){
           con.lstofJournalWrapper[0].journalDeduct = '100';
           con.lstofJournalWrapper[1].journalAllocate = '100';
        }
        //ApexPages.currentPage().getParameters().put('financecode','2' );
        con.financecode = '2';
        con.saveJournal();
        con.cancel();
        ApexPages.currentPage().getParameters().put('invId','1' );
        con.fetchjournalDetails();
        if(con.lstofJournalWrapper != null && con.lstofJournalWrapper.size()>1){
           con.lstofJournalWrapper[0].journalDeduct = '100';
           con.lstofJournalWrapper[1].journalAllocate = '100';
        }
        con.lstofJournalWrapper[1].invoiceAlocationObj.externalCompany__c=a.id;
        con.saveJournal();
        Test.stopTest();       
    }
    static testMethod void testCS_allocationControllerForOppsforid(){
        System.debug('opp test---->');
        opportunity a = [Select id from opportunity Limit 1];
        Test.startTest();
        PageReference pf = new PageReference('/apex/CS_allocationPage?id=1234');
        Test.setCurrentPage(pf);
        cs_allocationController con = new cs_allocationController();    
    }
}