/**
 *  Class Name: contactAddressController
 *  Description: Class to generate the Taxi Report on Job
 *  Company: dQuotient
 *  CreatedDate:12/12/2016  
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Jyoti             12/12/2016                 Orginal Version
 *
 */
 
public with sharing class contactAddressController{
    
    Public String Ids{get;set;}
    Public List<Address__c> ListProperty;
    Public Address__c newAddress{get;set;}
    Public List<booleanContactWrapper> ListbooleanContactWrapper{get;set;}
    Public List<booleanContactJnWrapper> ListbooleanContactJnWrapper{get;set;}
    Public List<Company_Property_Affiliation__c> CompnyPrptyJnToDel{get;set;}
    public Map<id,Address__c> MapIdProprty{get;set;}
    Public string IdtoDelete{get;set;}
    public List<Contact_Address_Junction__c> JobPrptyJn;
    Public string keyPrefix{get;set;}
    Public Boolean isError{get;set;}
    Public List<Company_Property_Affiliation__c> CompnyPrptyJn;
     Public Set<Id> PrptyId;
     public set<id> idSet;
     Public List<id> idList;
     public ErrorHandling ObjModal{get;set;}
     Public string BldingFilter{get;set;}
     Public string TownFilter{get;set;}
     Public string StreetFilter{get;set;}
     Public string StreetNoFilter{get;set;}
     Public string PostalFilter{get;set;}
     Public boolean Tofilter{get;set;}
     Public Boolean IsSelectAll{get;set;}
     Public Boolean IsQueryAll;
     Public Boolean disablecreate{get;set;}
     Public Boolean IsInitialised{get;set;}
     public Boolean IsWithoutSearch{get;set;}
     
     public Boolean isInsrt{get;set;}
     public Boolean showErrorMsg{get;set;}
     public String addError{get;set;}
    
    public ApexPages.StandardSetController cons {
            get {

                system.debug('------Testcons' );
             //   system.debug('orderSort------' + orderSort);
                
                //con.clear();
                 if (cons == null) {
                     if(IsQueryAll==null){
                     IsQueryAll=false;
                         
                     }
                Tofilter=true;
                String queryStr='';
                String tempFilter1='';
                String tempFilter2='';
                String tempFilter3='';
                String tempFilter4='';
                String tempFilter5='';
               // if (toFilter == null || toFilter == '') {
                    queryStr='select id , name,Building__c ,Street__c,Postcode__c,Town__c, Street_Number__c  from Address__c where id != null ';
                   
                    if(PostalFilter!=null && PostalFilter!='')
                    {
                         tempFilter1=PostalFilter;
                      queryStr+= 'and( Postcode__c like  '+'\'%'+tempFilter1+'%\' or Post_Code_Formula__c like  '+'\'%'+tempFilter1+'%\')';
                        system.debug('--'+tempFilter1);
                    }
                    if(BldingFilter!=null && BldingFilter!='')
                    {
                        tempFilter2=BldingFilter;
                       queryStr+= 'and Building__c  like '+'\'%'+tempFilter2+'%\'';
                        system.debug('--'+tempFilter2);
                    }
                     if(TownFilter!=null && TownFilter!='')
                    {
                        tempFilter3=TownFilter;
                      queryStr+= ' and Town__c like '+'\'%'+tempFilter3+'%\'';
                      system.debug('--'+tempFilter3);
                    }
                      if(StreetFilter!=null && StreetFilter!='')
                    {
                        tempFilter4=StreetFilter;
                       queryStr+= ' and Street__c like '+'\'%'+tempFilter4+'%\'';
                        system.debug('--'+tempFilter4);
                    }   
                    if(StreetNoFilter!=null && StreetNoFilter!='')
                    {
                        tempFilter5=StreetNoFilter;
                       queryStr+= ' and Street_Number__c like '+'\'%'+tempFilter5+'%\'';
                        system.debug('--'+tempFilter5);
                    }
                    queryStr+=  ' and Id Not IN :idList Limit 1000 ';
                    system.debug('query!!'+queryStr);
                   //system
                   cons = new ApexPages.StandardSetController(Database.query(queryStr));
                    system.debug('------result' + cons);
                  system.debug('555555');
                  if(IsQueryAll== false) {
                      system.debug('66666');
                  cons.setPageSize(15);
                  }
                 }

                return cons;
            }
            set;
    }
    
    public void getCategorieskeypreix() {
        IsInitialised=false;
        IsWithoutSearch=true;
        disablecreate = true;
        ObjModal=new ErrorHandling();
         idList= new List<id>();
         Ids = apexpages.currentpage().getparameters().get('id');
         //Ids = '0018E00000Sdxe7';
         newAddress = new Address__c();
         if(!String.isBlank(Ids)){
           keyPrefix = String.valueOf(Ids).substring(0,3);
            ListbooleanContactWrapper= new List<booleanContactWrapper>(); 
            CompnyPrptyJnToDel = new List<Company_Property_Affiliation__c>(); 
            ListbooleanContactJnWrapper= new List<booleanContactJnWrapper>(); 
            CompnyPrptyJn= new List<Company_Property_Affiliation__c>(); 
            JobPrptyJn= new List<Contact_Address_Junction__c>();
              idSet= new set<id>();
            newAddress = new Address__c();
             
            List<Address__c> ListAddress = new List<Address__c>();
            ListAddress = [SELECT id,name,Building__c,Street__c,Postcode__c,Town__c from Address__c Limit 1000];
              
            if(keyPrefix=='003'){
                JobPrptyJn=[select id ,Address__c,Address__r.id ,Address__r.Building__c,Address__r.Street_Number__c,Address__r.Street__c,Address__r.Postcode__c,Address__r.Town__c from Contact_Address_Junction__c where Contact__c=:Ids Limit 1000];
                system.debug('JobPrptyJn--->'+JobPrptyJn);
                for(Contact_Address_Junction__c CompntJn:JobPrptyJn){
                    ListbooleanContactJnWrapper.add(new booleanContactJnWrapper(CompntJn.Address__r.id,CompntJn.Address__r.Building__c,CompntJn.Address__r.Postcode__c,CompntJn.Address__r.Street__c,CompntJn.Address__r.Town__c, CompntJn.Address__r.Street_Number__c));
                     idSet.add(CompntJn.Address__r.id); 
                }
            }
            system.debug('ListbooleanContactJnWrapper------->'+ListbooleanContactJnWrapper);
            idList.addAll(idSet);
         }
         //getCategories();
    }
    
    // returns a list of wrapper objects for the sObjects in the current page set
    public void getCategories() {
        isError=false;
        IsInitialised=true;
        PrptyId=new Set<Id> ();
 
        ListbooleanContactWrapper= new List<booleanContactWrapper>(); 
       
        JobPrptyJn= new List<Contact_Address_Junction__c>(); 
        
        MapIdProprty= new Map<id,Address__c>(); 
        
        for(Address__c prpty:(List<Address__c>)cons.getRecords()){
            MapIdProprty.put(prpty.id,prpty);
        }
            
         for(Address__c prpty:(List<Address__c>)cons.getRecords()){
              if(IsSelectAll==true){
                ListbooleanContactWrapper.add(new booleanContactWrapper(True,prpty));
            }
            else{
            if(idSet.contains(prpty.id)){ 
                ListbooleanContactWrapper.add(new booleanContactWrapper(False,prpty));
            }
            else{
                 ListbooleanContactWrapper.add(new booleanContactWrapper(false,prpty));
            }
            }
        }
        if(ListbooleanContactWrapper.isEmpty()){
             ObjModal=new ErrorHandling(true,'Your search did not match any addresses','Error'); 
        }
        system.debug('ListbooleanContactWrapper---->'+ListbooleanContactWrapper);
    }
    
    public void cancelJobs(){
        
        List<booleanContactJnWrapper> ListbooleanContactJnWrapperToCancel = new List<booleanContactJnWrapper>();
        if(keyPrefix=='003'){
            for(booleanContactJnWrapper CmpnyJn :ListbooleanContactJnWrapper){
                if(!String.isBlank(CmpnyJn.JnID)){
                    ListbooleanContactJnWrapperToCancel.add(CmpnyJn);
                }
            }
        }
        /*else if(keyPrefix=='001'){
             
            for(booleanContactJnWrapper CmpnyJn :ListbooleanContactJnWrapper){
           
                if(!String.isBlank(CmpnyJn.JnID)){
                    ListbooleanContactJnWrapperToCancel.add(CmpnyJn);
                }
            }
        }*/
        ListbooleanContactJnWrapper = new List<booleanContactJnWrapper>();
        ListbooleanContactJnWrapper.addAll(ListbooleanContactJnWrapperToCancel);
    }
    
    public void saveAddress(){
        Address__c addr = new Address__c();
        Contact_Address_Junction__c accAddrjun = new Contact_Address_Junction__c();
        isInsrt = true;
        //showErrorMsg = false;
        addr.Name ='test';
        addr.Status__c = 'Active';
        if(!string.isBlank(newAddress.Flat_Number__c)){
            addr.Flat_Number__c = newAddress.Flat_Number__c;
        }else{
            
        }
        if(!string.isBlank(newAddress.Building__c)){
            addr.Building__c = newAddress.Building__c;    
        }else{
        
        }
        if(!string.isBlank(newAddress.Estate__c)){
            addr.Estate__c= newAddress.Estate__c;    
        }else{
        
        }
        if(!string.isBlank(newAddress.Area__c)){
            addr.Area__c= newAddress.Area__c;    
        }else{
        
        }
        /*if(!string.isBlank(newAddress.Street_Number__c)){
            addr.Street_Number__c= newAddress.Street_Number__c;   
        }else{
            isInsrt = false;
            showErrorMsg = true;
        }*/
        if(!string.isBlank(newAddress.Town__c)){
            addr.Town__c= newAddress.Town__c;    
        }else{
            isInsrt = false;
            showErrorMsg = true;
        }
        if(!string.isBlank(newAddress.Postcode__c)){
            addr.Postcode__c= newAddress.Postcode__c;    
        }else{
            isInsrt = false;
            showErrorMsg = true;
        }
        if(!string.isBlank(newAddress.Country__c)){
            addr.Country__c= newAddress.Country__c;   
        }else{
            isInsrt = false;
            showErrorMsg = true;
        }
        if(isInsrt == true){
            insert newAddress;
            
            accAddrjun.Contact__c= Ids;
            accAddrjun.Address__c = newAddress.id;
            insert accAddrjun;
            ListbooleanContactJnWrapper.add(new booleanContactJnWrapper(newAddress.id,newAddress.Building__c,newAddress.Postcode__c,newAddress.Street__c,newAddress.Town__c, newAddress.Street_Number__c));
            
            showErrorMsg = false;
            reDirect();
            //return myVFPage;
        }else{
            showErrorMsg = true;
            //addError = 'Address Cannot be Added.';
           // return null;
        }
    }
    
    public pageReference reDirect(){
        PageReference myVFPage = new PageReference('apex/contactAddress?id='+Ids);
        myVFPage.setRedirect(true);
        return myVFPage;
    }
    
    public void getNewCategories() {
        isError=false;
      idSet= new set<id>();
     idList= new List<id>();
         For(booleanContactJnWrapper JnWrp:ListbooleanContactJnWrapper){
            idSet.add(JnWrp.Idss);
            
        }
         idList.addAll(idSet);

        ListbooleanContactWrapper= new List<booleanContactWrapper>(); 
       
     
        MapIdProprty= new Map<id,Address__c>(); 
       
        
        for(Address__c prpty:(List<Address__c>)cons.getRecords()){
            MapIdProprty.put(prpty.id,prpty);
        }
     
         for(Address__c prpty:(List<Address__c>)cons.getRecords()){
            system.debug('-----idsss'+prpty.id);
             if(IsSelectAll==true){
                ListbooleanContactWrapper.add(new booleanContactWrapper(True,prpty));
            }
            else{
           
            if(idSet.contains(prpty.id)){ 
                ListbooleanContactWrapper.add(new booleanContactWrapper(True,prpty));
            }
            else{
                 ListbooleanContactWrapper.add(new booleanContactWrapper(false,prpty));
            }
            }
            
        }
        if(ListbooleanContactWrapper.isEmpty()){
             ObjModal=new ErrorHandling(true,'Your search did not match any addresses','Error'); 
        }
    }
    
    Public Void SelectedProperty(){
    List<booleanContactJnWrapper> SaveRecordsList = new List<booleanContactJnWrapper>();
            if(IsSelectAll==true){
                system.debug('11111');
                IsQueryAll=true;
                cons = null;
                system.debug('----size444'+ListbooleanContactJnWrapper.size());
                cons.first();
                system.debug('----size555'+ListbooleanContactJnWrapper.size());
                getCategories();
                system.debug('----size666'+ListbooleanContactJnWrapper.size());
               
                for(booleanContactWrapper PrpWrp:ListbooleanContactWrapper){
                    if(PrptyId.contains(PrpWrp.Prprty.id)){
                    
                    }
                    else{
                    ListbooleanContactJnWrapper.add(new booleanContactJnWrapper(MapIdProprty.get(PrpWrp.Prprty.id).id,MapIdProprty.get(PrpWrp.Prprty.id).Building__c,MapIdProprty.get(PrpWrp.Prprty.id).Postcode__c,MapIdProprty.get(PrpWrp.Prprty.id).Street__c,MapIdProprty.get(PrpWrp.Prprty.id).Town__c, MapIdProprty.get(PrpWrp.Prprty.id).Street_Number__c));
                        SaveRecordsList.add(new booleanContactJnWrapper(MapIdProprty.get(PrpWrp.Prprty.id).id,MapIdProprty.get(PrpWrp.Prprty.id).Building__c,MapIdProprty.get(PrpWrp.Prprty.id).Postcode__c,MapIdProprty.get(PrpWrp.Prprty.id).Street__c,MapIdProprty.get(PrpWrp.Prprty.id).Town__c, MapIdProprty.get(PrpWrp.Prprty.id).Street_Number__c));
                    }
                        
                }
                
             }
    
        PrptyId=new Set<Id> ();
        system.debug('----size777'+ListbooleanContactJnWrapper.size());
        For(booleanContactJnWrapper JnWrp:ListbooleanContactJnWrapper){
            PrptyId.add(JnWrp.Idss);
            
        }
        for(booleanContactWrapper PrpWrp:ListbooleanContactWrapper){
            if(PrpWrp.isSelected== true){
                if(PrptyId.contains(PrpWrp.Prprty.id)){
                    
                }
                else{
                    system.debug('MapIdProprty---->'+MapIdProprty);
                    system.debug('PrpWrp---->'+PrpWrp);
                    system.debug('PrpWrp.Prprty.id---->'+PrpWrp.Prprty.id);
                     ListbooleanContactJnWrapper.add(new booleanContactJnWrapper(MapIdProprty.get(PrpWrp.Prprty.id).id,MapIdProprty.get(PrpWrp.Prprty.id).Building__c,MapIdProprty.get(PrpWrp.Prprty.id).Postcode__c,MapIdProprty.get(PrpWrp.Prprty.id).Street__c,MapIdProprty.get(PrpWrp.Prprty.id).Town__c, MapIdProprty.get(PrpWrp.Prprty.id).Street_Number__c));
                     SaveRecordsList.add(new  booleanContactJnWrapper(MapIdProprty.get(PrpWrp.Prprty.id).id,MapIdProprty.get(PrpWrp.Prprty.id).Building__c,MapIdProprty.get(PrpWrp.Prprty.id).Postcode__c,MapIdProprty.get(PrpWrp.Prprty.id).Street__c,MapIdProprty.get(PrpWrp.Prprty.id).Town__c, MapIdProprty.get(PrpWrp.Prprty.id).Street_Number__c));
            }
            
            
        }
        
    }
    
    IsQueryAll=false;
    idSet= new set<id>();
        idList= new List<id>();
         system.debug('----size'+ListbooleanContactJnWrapper.size());
         For(booleanContactJnWrapper JnWrp:ListbooleanContactJnWrapper){
            idSet.add(JnWrp.Idss);
            
        }
        idList.addAll(idSet);
        integer pgeno=1;
         if(pageNumber != 1){
             pgeno=pageNumber;
         }
        firstfilter();
         system.debug('----size111'+ListbooleanContactJnWrapper.size());
         system.debug('-----pageno------'+pageNumber);
         
        for(Integer i=1; i<pgeno;i++){
            system.debug('-----pageno'+pageNumber);
            next();
             system.debug('----size222'+ListbooleanContactJnWrapper.size());
            
        }
        IsSelectAll=false;
        
        SaveRecords(keyprefix, SaveRecordsList);
       firstfilter();
    }
    public void close_ErrorModal(){
        ObjModal=new ErrorHandling();
    }
    Public void selectAllProperty(){
        IsSelectAll=true;
        getCategories();
        for(booleanContactWrapper PrpWrp:ListbooleanContactWrapper){
            if(PrpWrp.isSelected== true){
                
                PrpWrp.isSelected = true;
            
            }
            else{
                PrpWrp.isSelected = False;
            }
         
        }
        
    }
    
    Public void deleteSelectedRow(){
        IsSelectAll=false;
        system.debug('------idno'+IdtoDelete);
        system.debug('------id'+ListbooleanContactJnWrapper[Integer.valueof(IdtoDelete)].Idss);
        /* for(booleanContactWrapperPrpWrp:ListbooleanProproprtyWrapper){
             if(PrpWrp.Prprty.id==ListbooleanContactJnWrapper[Integer.valueof(IdtoDelete)].Idss){
                 PrpWrp.isSelected=false;
             }
         }*/
        
        //ListbooleanProproprtyWrapper.add(new booleanProproprtyWrapper(false,MapIdProprty.get(ListbooleanContactJnWrapper[Integer.valueof(IdtoDelete)].Idss)));
        String Deleteid = ListbooleanContactJnWrapper.get(Integer.valueof(IdtoDelete)).idss;
        //booleanContactJnWrapper dellist=ListbooleanContactJnWrapper.get(Integer.valueof(IdtoDelete));
        ListbooleanContactJnWrapper.remove(Integer.valueof(IdtoDelete));
        system.debug('---------------------'+Deleteid);
        idSet= new set<id>();
        idList= new List<id>();
     
         For(booleanContactJnWrapper JnWrp:ListbooleanContactJnWrapper){
            idSet.add(JnWrp.Idss);
            
        }
        idList.addAll(idSet);
         if(IsWithoutSearch==false){
        integer pgeno=1;
         if(pageNumber != 1){
             pgeno=pageNumber;
         }
        firstfilter();
         system.debug('-----pageno------'+pageNumber);
         
        for(Integer i=1; i<pgeno;i++){
            system.debug('-----pageno'+pageNumber);
            next();
            
        }
            
        }
     
        JobPrptyJn= new List<Contact_Address_Junction__c>();
        JobPrptyJn=[select id ,Address__c,Address__r.id ,Address__r.Building__c,Address__r.Street__c,Address__r.Postcode__c, Address__r.Town__c from Contact_Address_Junction__c where Contact__c=:Ids AND Address__c=:Deleteid Limit 1000];
        
        if(!JobPrptyJn.isEmpty()){
            Delete JobPrptyJn;
        }
        //ListbooleanProproprtyWrapper.add()
    }
    
    // Logic to save all records
     Public void SaveRecords(String keyPrefix,List<booleanContactJnWrapper> ListbooleanContactJnWrapper){
         try{
             isError=false;
     
          if(keyPrefix=='003'){
             List<Contact_Address_Junction__c> ToUpdateAccAddressJn= new List<Contact_Address_Junction__c >();
            for(booleanContactJnWrapper CmpnyJn :ListbooleanContactJnWrapper){
               
                Contact_Address_Junction__c CpnyJn= new Contact_Address_Junction__c();
                CpnyJn.Contact__c=Ids;
                CpnyJn.Address__c=CmpnyJn.Idss;
                ToUpdateAccAddressJn.add(CpnyJn);
               
            }
       
            Insert ToUpdateAccAddressJn;
        }
         }
         catch (Exception e) {
            ApexPages.addMessages(e);
           isError=true;
            //return null;
        }
         
    }
    
    /*Public PageReference SaveRecords(){
         try{
             isError=false;
        System.debug('----CompnyPrptyJn----'+CompnyPrptyJn);
    
        JobPrptyJn= new List<Contact_Address_Junction__c>();
        JobPrptyJn=[select id ,Address__c,Address__r.id ,Address__r.Building__c,Address__r.Street__c,Address__r.Postcode__c, Address__r.Town__c from Contact_Address_Junction__c where Contact__c=:Ids Limit 49999];
        
        if(!JobPrptyJn.isEmpty()){
            Delete JobPrptyJn;
        }
        
        List<Job_Property_Junction__c> JobPrptyJnToDelonSave = new List<Job_Property_Junction__c>();
        if(keyPrefix=='003'){
        List<Contact_Address_Junction__c> ToUpdateProptyAccJn= new List<Contact_Address_Junction__c>();
        for(booleanContactJnWrapper CmpnyJn :ListbooleanContactJnWrapper){
            
            Contact_Address_Junction__c CpnyJn= new Contact_Address_Junction__c();
            CpnyJn.Contact__c=Ids;
            CpnyJn.Address__c=CmpnyJn.Idss;
            ToUpdateProptyAccJn.add(CpnyJn);

        }
        Insert ToUpdateProptyAccJn;
        
        }

         }
         catch (Exception e) {
            ApexPages.addMessages(e);
           isError=true;
            return null;
        }
        PageReference myVFPage = new PageReference('/'+Ids);
      myVFPage.setRedirect(true);
  
      return myVFPage;
         
    }*/
    
    public void closeModal(){
    
    newAddress = new Address__c();
    }
    
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            
            return cons.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return cons.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return cons.getPageNumber();
        }
        set;
    }

    // returns the first page of records
     public void first() {
         SelectedProperty();
         cons.first();
         
         getNewCategories();
     }
     
     public void firstfilter() {
        disablecreate = false;
        IsWithoutSearch=false;
             cons=null;
             
         cons.first();
         if(IsInitialised==true){
             getNewCategories();
         }
         else{
          getCategories();
         }
         
     }

     // returns the last page of records
     public void last() {
         SelectedProperty();
         cons.last();
        
         getNewCategories();
     }

     // returns the previous page of records
     public void previous() {
         SelectedProperty();
       cons.previous();
         
         getNewCategories();
     }
    
    
    
    
    // returns the next page of records
    public void next() {
      SelectedProperty();
      cons.next();
         
      getNewCategories();
         
    }
    
    public class booleanContactWrapper{
        public Boolean isSelected{get;set;}
        public Address__c Prprty{get;set;}
       
        
        public booleanContactWrapper(boolean isSelect, Address__c Prprtys ){
          Prprty = Prprtys;
          isSelected= isSelect;
        }
    }
    
    public class booleanContactJnWrapper{
        //public Boolean isSelected{get;set;}
        public id Idss{get;set;}
        Public string Building{get;set;}
        Public string Postal{get;set;}
        Public string Street{get;set;}
        Public string Town{get;set;}
        Public string JnID{get;set;}
        Public string StreetNumber{get;set;}
        
        public booleanContactJnWrapper(id idsss ,string Buildings, string Postals,string Streets,string Towns, String StreetNo ){
          Building = Buildings;
          Postal=Postals;
          Town=Towns;
          Street=Streets;
          Idss=Idsss;
          JnID = '';
          StreetNumber = StreetNo;
           
        }
        
         public booleanContactJnWrapper(id idsss ,string Buildings, string Postals,string Streets,string Towns, String junctionId, String StreetNo ){
          Building = Buildings;
          Postal=Postals;
          Town=Towns;
          Street=Streets;
          Idss=Idsss;
          JnID = junctionId;
          StreetNumber = StreetNo;
           
        }
    }
    public class ErrorHandling{
        public boolean displayerror{get;set;}
        public string Errormsg{get;set;}
        public string Title{get;set;}
        Public List<string> ErrorList{get;set;}
        public boolean hasmore{get;set;}
        public ErrorHandling(boolean displayerror, string Errormsg,string Title){
            this.displayerror =displayerror;
            this.Errormsg=Errormsg;
            this.Title=Title;
            this.hasmore=false;
            ErrorList = new List<string>();
        }
        public  void ErrorHandling(boolean displayerror, string Errormsg,string Title,boolean hasmore){
            this.displayerror =displayerror;
            ErrorList.add(Errormsg);
            this.Title=Title;
            this.hasmore=hasmore;
        }
        public ErrorHandling(){
            this.displayerror =false;
            this.Errormsg='';
            ErrorList = new List<string>();
        }
    }


}