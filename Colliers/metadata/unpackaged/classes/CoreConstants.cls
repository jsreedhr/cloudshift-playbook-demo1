/**
 * Defines Constants that are used by the organization
 *
 * @author     Nidheesh N
 * @date       Jan 4,2018
 */
public class CoreConstants 
{
    /*****************************************************************************************************************************
    *
    * Constants 
    *
    *****************************************************************************************************************************/
 
    /**
     * Delimiters
     */
    public static final String DELIMITER_AT = '@';
    public static final String DELIMITER_COLON = ':';
    public static final String DELIMITER_COMMA = ',';
    public static final String DELIMITER_DOT = '.';
    public static final String DELIMITER_EQUAL = '=';
    public static final String DELIMITER_EXCLAMATION = '!';
    public static final String DELIMITER_HASH = '#';
    public static final String DELIMITER_SEMICOLON = ';';
    public static final String DELIMITER_SPACE = ' ';
    public static final String DELIMITER_UNDERSCORE = '_';
    public static final String DELIMITER_HYPHEN = '-';
    public static final String DELIMITER_AMPERSAND = '&';
    public static final String DELIMITER_QUESTION_MARK = '?';
    public static final String DELIMITER_BACKWARD_SLASH = '\'';
    public static final String DELIMITER_SINGLE_QUOTE = '\',';
    public static final String DELIMITER_FORWARD_SLASH = '/';
    public static final String DEFAULT_ONE = '-- Select One --';
    public static final String OPTION_NONE = '--None--';
    public static final String DELIMITER_PERCENT = '%';
    
    public static final String BLANK_STRING = '';
    public static final String DELIMITER_PLUS = '+';
    public static final String DELIMITER_PIPE = '|';
    public static final String CARRIAGE_RETURN = '\r\n';

    public static final String STRING_ALL = 'All';
    public static final String STRING_YES = 'Yes';
    public static final String STRING_NO = 'No';
    
   

    /*
     * User Type strings
     */
    public static final String USER_TYPE_STANDARD = 'Standard';

    /**
     * Sharing constants
     */
    public static final String SHARE_LEVEL_EDIT = 'Edit';
    public static final String SHARE_LEVEL_READ = 'Read';
    public static final String SHARE_CAUSE_MANUAL = 'Manual';

    /**
     *  Roles
     */
    public static final String ROLES_ROLES_AND_SUBORDINATES = 'RoleAndSubordinates';

    /**
     *  Approval process constants
     */
     public static final String APPROVAL_PROCESS_PENDING = 'Pending';
    

    /**
     * Regex constants
     */
    public static final String REGEX_WILDCARD = '.*';



}