public with sharing class PropertyDataQualityExt {
 public PropertyDataQualityExt(ApexPages.StandardController
stdController) {
 this.PageRenderer = new EDQ.PageRenderer(stdController, 'Property__c');
 }
 public EDQ.PageRenderer PageRenderer { get; private set; }
}