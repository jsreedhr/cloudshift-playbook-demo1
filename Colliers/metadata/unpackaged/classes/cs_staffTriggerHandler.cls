/**
 *  Class Name: cs_staffTriggerHandler  
 *  Description: This is a Trigger Handler Class for trigger on Staff__c
 *  Company: dQuotient
 *  CreatedDate:08/03/2017 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Nidheesh              08/03/2017                 Orginal Version
 *
 */
public class cs_staffTriggerHandler {
    public static boolean run = true;
    public cs_staffTriggerHandler(){
        
    }
    /**
    *Metod to Avoiding Recursive Trigger Calls
    */
    /*public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }*/
    /**
     *  Method Name: insertCompanyCostCentre
     *  Description: Method to update the Company and Cost Centre
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void insertCompanyCostCentre(List<Staff__c> lsofstaff){
        
        List<Staff__c> lsofstaffNew = new List<Staff__c>();
        
        for(Staff__c objstaff: lsofstaff){
            
            if(!String.isBlank(objstaff.Department__c)){
                lsofstaffNew.add(objstaff);
            }
        }
        
        calculateCompanyCostCentre(lsofstaffNew);
    }
    
    /**
     *  Method Name: updateCompanyCostCentre
     *  Description: Method to update the Company and Cost Centre
     *  Param:  Trigger.oldMap, Trigger.newMap
     *  Return: None
    */
    public static void updateCompanyCostCentre(map<id,Staff__c> mapOldstaff, map<id,Staff__c> mapNewstaff){
        
        List<Staff__c> lsofstaffNew = new List<Staff__c>();
        
        for(Staff__c objStaff: mapNewstaff.values()){
            if(objStaff.Department__c != null){
                if(objStaff.Department__c != mapOldstaff.get(objStaff.id).Department__c || objStaff.Dept_Cost_Centre__c == null){
                    lsofstaffNew.add(objStaff);
                }
            }
        }
        
        calculateCompanyCostCentre(lsofstaffNew);
    }
    
    /**
     *  Method Name: calculateCompanyCostCentre 
     *  Description: Method to update the Company and Cost Centre
     *  Param:  Trigger.new
     *  Return: None
    */
    public static void calculateCompanyCostCentre(List<Staff__c> lststaff){
        
        map<String, String> mapDeptCompany = new map<String, String>();
        map<String, String> mapDeptCostCentre = new map<String, String>();
        List<Department__c> lstDepartment = new List<Department__c>();
        lstDepartment = Department__c.getall().values();
        
        for(Department__c objDept: lstDepartment){
            
            if(!String.isBlank(objDept.Department__c)){
                if(String.isNotBlank(objDept.Company__c)){
                    mapDeptCompany.put(objDept.Department__c, objDept.Company__c);
                }
                
                if(String.isNotBlank(objDept.Cost_Centre__c)){
                    mapDeptCostCentre.put(objDept.Department__c, objDept.Cost_Centre__c);
                }
            }
        }
        
        for(Staff__c objstaff: lststaff){
            
            if(!String.isBlank(objstaff.Department__c)){
                
                if(mapDeptCompany.containsKey(objstaff.Department__c)){
                    String company = mapDeptCompany.get(objstaff.Department__c);
                    objstaff.Company__c = company;
                }else{
                    objstaff.Company__c = null;
                }
                
                if(mapDeptCostCentre.containsKey(objstaff.Department__c)){
                    system.debug('Object msg');
                    String costCentre = mapDeptCostCentre.get(objstaff.Department__c);
                    objstaff.Dept_Cost_Centre__c = costCentre;
                }else{
                    system.debug('Object msg failed'+objstaff.Id);
                    objstaff.Dept_Cost_Centre__c = null;
                }
                
            }
            
        }
        
    }
    
    /**
     *  Method Name: updateStaffStatus
     *  Description: Method to update the Staff Status
     *  Param:  Trigger.oldMap, Trigger.newMap
     *  Return: None
    */
    public static void updateStaffStatus(map<id,Staff__c> mapOldstaff, map<id,Staff__c> mapNewstaff){
        
        List<Staff__c> lsofstaffNew = new List<Staff__c>();
        set<id> setStaffIds = new set<id> ();
        for(Staff__c objStaff: mapNewstaff.values()){
            if(objStaff.Department__c != null){
                if(objStaff.Active__c != mapOldstaff.get(objStaff.id).Active__c && objStaff.Active__c == false){
                    lsofstaffNew.add(objStaff);
                    setStaffIds.add(objStaff.id);
                }
            }
        }
        
        if(!setStaffIds.isEmpty()){
            
            List<Coding_Structure__c> lstCodingStructure = new List<Coding_Structure__c>();
            Map<id, List<Coding_Structure__c>> mapIdCOdingStrcuture  = new Map<id, List<Coding_Structure__c>>();
            lstCodingStructure = [Select id, name,
                                            Staff__c,
                                            Status__c
                                            From
                                            Coding_Structure__c     
                                            where 
                                            Staff__c in: setStaffIds
                                            and Status__c = 'Active'
                                            and Staff__c != null];
                                            
                                            
            for(Coding_Structure__c objCodingStructure :  lstCodingStructure){
                if(mapIdCOdingStrcuture.containsKey(objCodingStructure.Staff__c)){
                    mapIdCOdingStrcuture.get(objCodingStructure.Staff__c).add(objCodingStructure);
                }else{
                    mapIdCOdingStrcuture.put(objCodingStructure.Staff__c, new List<Coding_Structure__c>{objCodingStructure});
                }
                
            }
            
            for(Staff__c objStaff: lsofstaffNew){
                
                if(mapIdCOdingStrcuture.containsKey(objStaff.id)){
                    objStaff.addError('There are still active Coding Structures associated with this Staff. Please make them Inactive before proceeding to make the Staff inactive');
                }else{
                    objStaff.Disable_Lookup_Filter__c = true;
                }
            }
            
        }
    }
}