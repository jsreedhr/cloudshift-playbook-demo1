@isTest(SeeAllData=false)
public class TestCS_CompaniesContactsController{
    public static Account a;
    public static Account b;
    public static Contact c;
    public static Contact cont;
    public static Contact invc;
    public static opportunity opp;
    public static opportunity oppr;
    public static opportunity opty;
    public static opportunity careOfopty;
    public static Address__c addr;
    public static Address__c invaddr;
    public static Staff__c stf;
    public static Contact_Address_Junction__c conAddrsJunc;
    public static Account_Address_Junction__c accAddrJunc;
    public static List<AccountContactRelation> acr;
    public static Care_Of__c careOf;
    public static Care_Of__c careOfnew;
    public static AssignAccountId__c assignAccId;

   // @testsetup
    static testmethod void setupData(){
        //create account
        a = TestObjectHelper.createAccount();
        a.Company_Status__c = 'Active';
        insert a ;
        b = TestObjectHelper.createAccount();
        b.Company_Status__c = 'Pending';
        insert b ;
        Schema.DescribeFieldResult fieldResult = Address__c.Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //create contact
        c = TestObjectHelper.createContact(a);
        c.Email ='test1@test1.com';
        insert c;
        cont = TestObjectHelper.createContact(a);
        cont.Email ='test1@test1.com';
        insert cont;
        
        //create Contact
        invc = TestObjectHelper.createContact(a);
        invc.Email ='test2@test1.com';
        insert invc;
        
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
        
        //create realted Opportunity
        List <Contact> cons = TestObjectHelper.createMultipleContact(a, 10);
        integer loopNo = 1;
        for(Contact objContact: cons){
            objContact.Email = 'test'+loopNo+'@test.com';
        }
        insert cons;
        
        //create Address Object
        addr = TestObjectHelper.createAddress(a);
        if(!ple.isEmpty()){
            addr.Country__c =ple[0].getValue();
        }
        insert addr;
        
        //create invAddress object
        invaddr = TestObjectHelper.createAddress(a);
        if(!ple.isEmpty()){
            invaddr.Country__c =ple[0].getValue();
        }
        insert invaddr;
        
        //Create Contact Address Junction
        conAddrsJunc = TestObjectHelper.createContactAddressJunction(c,addr);
        insert conAddrsJunc;
        
        //Create Account Address Junction
        accAddrJunc = TestObjectHelper.createAccountAddressJunction(a,addr);
        insert accAddrJunc;
        
        //Create Account Contact Relation Object
        acr = [SELECT id,AccountId,ContactId,Account.Id from AccountContactRelation where Account.Id=:a.Id];
        
        //Create Care OF Object
        careOf = TestObjectHelper.createCareOf(a,c);
        careOf.Colliers_Int__c = true;
        careOf.Reporting_Client__c = a.id;
        insert careOf;
        careOfnew = TestObjectHelper.createCareOf(a,c);
        careOfnew.Care_Of_Address__c = invaddr.Id;
        careOfnew.Colliers_Int__c = true;
        careOfnew.Reporting_Client__c = a.id;
        insert careOfnew;
        // create worktype 
        Work_Type__c workObj = new Work_Type__c();
        workObj.Name = 'Valuation - Redbook';
        workObj.Active__c = true;
        insert workObj;
        //Create Assign Account Id
        assignAccId = TestObjectHelper.createAssignAccCode();
        insert assignAccId;
        
        //create staff
        stf = TestObjectHelper.createStaff();
        stf.Active__c = true;
        insert stf;
        // create coding structure object
        Coding_Structure__c codobj = new Coding_Structure__c();
        codobj.Staff__c = stf.Id;
        codobj.Work_Type__c = workObj.Id;
        codobj.Department__c = 'Accurates';
        codobj.Office__c = 'London - West End';
        codobj.Status__c = 'Active';
        codobj.Department_Cost_Code_Centre__c = '620';
        insert codobj;
        //Create a opportunity
        oppr = TestObjectHelper.createOpportunity(a);
        oppr.AccountId = a.id;
        oppr.Name = 'Test';
        oppr.Date_Instructed__c = date.today();
        oppr.CloseDate = system.today().addDays(7);
        oppr.Amount = 8000;
        oppr.Instructing_Contact__c = c.id;
        oppr.InstructingCompanyAddress__c =addr.id;
        oppr.Invoicing_Company2__c =a.id;
        oppr.Invoice_Contact__c = c.id;
        oppr.Invoicing_Address__c = addr.id;
        oppr.Manager__c = stf.id;
        oppr.Department__c = 'Accurates';
        oppr.Work_Type__c = 'Valuation - Redbook';
        oppr.Office__c = 'London - West End';
        oppr.Instructing_Company_Role__c ='Landlord';
    
        insert oppr;
        
        //Create a opportunity
        opty= TestObjectHelper.createOpportunity(a);
        opty.AccountId = a.id;
        opty.Name = 'Test';
        opty.CloseDate = system.today().addDays(7);
        opty.Amount = 8000;
        opty.Instructing_Contact__c = c.id;
        opty.InstructingCompanyAddress__c =addr.id;
        opty.Invoicing_Company2__c =a.id;
        opty.Invoice_Contact__c = invc.id;
        opty.Invoicing_Address__c = invaddr.id;
        opty.Manager__c = stf.id;
        opty.Department__c = 'Accurates';
        opty.Work_Type__c = 'Valuation - Redbook';
        opty.Office__c = 'London - West End';
        opty.Instructing_Company_Role__c ='Landlord';
        
        insert opty;
        
        //Create a care Of opportunity
        careOfopty= TestObjectHelper.createOpportunity(a);
        careOfopty.AccountId = a.id;
        careOfopty.Name = 'Test';
        careOfopty.CloseDate = system.today().addDays(7);
        careOfopty.Amount = 8000;
        careOfopty.Instructing_Contact__c = c.id;
        careOfopty.InstructingCompanyAddress__c =addr.id;
        careOfopty.Invoicing_Care_Of__c=careOfnew.id;
        careOfopty.Invoice_Contact__c = invc.id;
        careOfopty.Invoicing_Address__c = invaddr.id;
        careOfopty.Manager__c = stf.id;
        careOfopty.Department__c = 'Accurates';
        careOfopty.Work_Type__c = 'Valuation - Redbook';
        careOfopty.Office__c = 'London - West End';
        careOfopty.Instructing_Company_Role__c ='Landlord';
        
        insert careOfopty;
    }
    
    static testmethod void TestCS_CompaniesContactsController(){
        setupData();
        opp = [Select id from opportunity Limit 1]; 
        
        Test.startTest();
        PageReference pf = new PageReference('/apex/cs_CompaniesContacts?id='+oppr.Id);
        
        Test.setCurrentPage(pf);
        cs_companiesContactsController ctrl = new cs_companiesContactsController();
        ctrl.changeInv = 'YES';
        ctrl.initCompaniesContacts();
        ctrl.save();
        ctrl.getJobContacts();
        ctrl.checkCareOf();
        ctrl.getInvoicingContact();
        ctrl.getInvoicingContactAddress();
        ctrl.getItems();
        ctrl.getInvItems();
        ctrl.cancel();
        ctrl.closeModal();
        ctrl.createContactAddressJunction();
        ctrl.createCustomSetting();
        ctrl.resetInvPanel();
        ctrl.changeInvoiceDetails();
        
        PageReference pf1 = new PageReference('/apex/CS_companiesContact_edit?id='+oppr.Id);
        cs_companiesContacts_editController objController = new cs_companiesContacts_editController();
        objController.changeInv = 'YES';
        objController.initCompaniesContacts();
        // objController.save();
        objController.getJobContacts();
        //objController.checkCareOf();
        objController.getsourceofinstruct();
        // objController.getInvoicingContact();
        // objController.getInvoicingContactAddress();
        objController.getItems();
        objController.getInvItems();
        // objController.cancel();
        // objController.closeModal();
        //objController.createContactAddressJunction();
        // objController.createCustomSetting();
        objController.resetInvPanel();
        objController.changeInvoiceDetails();
        Test.stopTest();  
    }
    
    /*static testmethod void TestInvoiceChanges(){
        setupData();
        opp = [Select id from opportunity Limit 1];
        
        Test.startTest();
        PageReference pf = new PageReference('/apex/cs_CompaniesContacts?id='+oppr.Id);
        Test.setCurrentPage(pf);
        cs_companiesContactsController ctrl = new cs_companiesContactsController();
        ctrl.changeInv = 'YES';
        ctrl.isCareOf='NO';
        ctrl.initCompaniesContacts();
        ctrl.save();
        ctrl.getJobContacts();
        ctrl.checkCareOf();
        ctrl.getInvoicingContact();
        ctrl.getInvoicingContactAddress();
        ctrl.getItems();
        ctrl.getInvItems();
        ctrl.cancel();
        ctrl.closeModal();
        ctrl.createContactAddressJunction();
        ctrl.createCustomSetting();
        ctrl.resetInvPanel();
        ctrl.changeInvoiceDetails();
        Test.stopTest();  
    }*/
    
    static testmethod void TestCS_companiesContacts(){
        setupData();
        Test.startTest();
            PageReference pf = new PageReference('/apex/cs_CompaniesContacts');
            Test.setCurrentPage(pf);  
            cs_companiesContactsController ctrl = new cs_companiesContactsController();
            ctrl.initCompaniesContacts();
            ctrl.jobDetails.AccountId = a.id;
            ctrl.jobDetails.Name = 'Test';
            ctrl.jobDetails.Date_Instructed__c = date.today();
            ctrl.jobDetails.CloseDate = system.today().addDays(7);
            ctrl.jobDetails.Amount = 8000;
            ctrl.jobDetails.Instructing_Contact__c = cont.id;
            ctrl.jobDetails.InstructingCompanyAddress__c =addr.id;
            ctrl.jobDetails.Invoicing_Company2__c =a.id;
            ctrl.jobDetails.Invoice_Contact__c = cont.id;
            ctrl.jobDetails.Invoicing_Address__c = addr.id;
            ctrl.jobDetails.Manager__c = stf.id;
            ctrl.jobDetails.Department__c = 'Accurates';
            ctrl.jobDetails.Work_Type__c = 'Valuation - Redbook';
            ctrl.jobDetails.Office__c = 'London - West End';
            ctrl.jobDetails.Source_Of_Instruction__c = 'Referral from Colliers overseas (inc. Belfast)';
            ctrl.jobDetails.Instructing_Company_Role__c ='Landlord';
            ctrl.isCareOf = 'No';
            ctrl.save();
            ctrl.getJobContacts();
            ctrl.getInstructingAddress();
            ctrl.getsourceofinstruct();
            ctrl.checkCareOf();
            ctrl.getInvoicingContact();
            ctrl.getInvoicingContactAddress();
            ctrl.getItems();
            ctrl.getInvItems();
            ctrl.cancel();
            ctrl.closeModal();
            ctrl.createContactAddressJunction();
            ctrl.createCustomSetting();
            ctrl.resetInvPanel();
            ctrl.changeInvoiceDetails();
            Test.stopTest();   
    }
    
    static testmethod void TestCS_InvAddress(){
        setupData();
        opp = [Select id from opportunity Limit 1];
        Test.startTest();
            PageReference pf = new PageReference('/apex/cs_CompaniesContacts?id='+opty.Id);
            Test.setCurrentPage(pf);  
            cs_companiesContactsController ctrl = new cs_companiesContactsController();
            ctrl.initCompaniesContacts();
            ctrl.jobDetails.Source_Of_Instruction__c = 'Referral from Colliers UK';
            ctrl.save();
            ctrl.getJobContacts();
            ctrl.isCareOf = 'Yes';
            ctrl.changeInv = 'YES';
            ctrl.checkCareOf();
            ctrl.getInvoicingContact();
            ctrl.getInstructingAddress();
            ctrl.getInvoicingContactAddress();
            ctrl.getItems();
            ctrl.getsourceofinstruct();
            ctrl.getInvItems();
            ctrl.getRelatedToWorkTyp();
            ctrl.cancel();
            ctrl.closeModal();
            ctrl.createContactAddressJunction();
            ctrl.createCustomSetting();
            ctrl.resetInvPanel();
            ctrl.changeInvoiceDetails();
            Test.stopTest();
    }
    static testmethod void TestCS_InvAddressnew(){
        setupData();
        opp = [Select id from opportunity Limit 1];
        Test.startTest();
            PageReference pf = new PageReference('/apex/cs_CompaniesContacts?id='+opty.Id);
            Test.setCurrentPage(pf);  
            cs_companiesContactsController ctrl = new cs_companiesContactsController();
            ctrl.initCompaniesContacts();
            ctrl.jobDetails.Source_Of_Instruction__c = 'Referral from Colliers UK';
            ctrl.save();
            ctrl.getJobContacts();
            ctrl.isCareOf = 'No';
            ctrl.changeInv = 'YES';
            ctrl.checkCareOf();
            ctrl.getInvoicingContact();
            ctrl.getInstructingAddress();
            ctrl.getsourceofinstruct();
            ctrl.getInvoicingContactAddress();
            ctrl.getItems();
            ctrl.getInvItems();
            ctrl.cancel();
            ctrl.closeModal();
            ctrl.createContactAddressJunction();
            ctrl.createCustomSetting();
            ctrl.resetInvPanel();
            ctrl.changeInvoiceDetails();
            Test.stopTest();
    }
    static testmethod void TestCS_InvAddress_withountaccid(){
        setupData();
        opp = [Select id from opportunity Limit 1];
        Account acc = [Select id from Account where Company_Status__c = 'Pending' Limit 1];
        Test.startTest();
            PageReference pf = new PageReference('/apex/cs_CompaniesContacts?AccId='+acc.id);
            Test.setCurrentPage(pf);  
            cs_companiesContactsController ctrl = new cs_companiesContactsController();
            ctrl.initCompaniesContacts();
            ctrl.jobDetails.Source_Of_Instruction__c = 'Client - referred by another client';
            ctrl.jobDetails.Invoice_Contact__c = cont.id;
            //ctrl.getInvoicingContactAddress();
            ctrl.save();
            ctrl.getJobContacts();
            ctrl.checkCareOf();
            ctrl.getInvoicingContact();
            ctrl.getInvoicingContactAddress();
            ctrl.getInstructingAddress();
            ctrl.getItems();
            ctrl.getInvItems();
            ctrl.cancel();
            ctrl.showerrormessage();
            ctrl.closeModal();
            ctrl.createContactAddressJunction();
            ctrl.createCustomSetting();
            ctrl.resetInvPanel();
            ctrl.changeInvoiceDetails();
            Test.stopTest();
    }
    
    static testmethod void TestCS_InvcareOfAddress(){
        setupData();
        opp = [Select id from opportunity Limit 1];
        Test.startTest();
            PageReference pf = new PageReference('/apex/cs_CompaniesContacts?id='+careOfopty.Id);
            Test.setCurrentPage(pf);  
            cs_companiesContactsController ctrl = new cs_companiesContactsController();
            ctrl.initCompaniesContacts();
            ctrl.jobDetails.Source_Of_Instruction__c = 'Client - referred by another client';
            ctrl.jobDetails.Client_Referral__c = a.Id;
            ctrl.save();
            ctrl.getJobContacts();
            ctrl.getInstructingAddress();
            ctrl.checkCareOf();
            ctrl.getInvoicingContact();
            ctrl.getInvoicingContactAddress();
            ctrl.getItems();
            ctrl.getInvItems();
            ctrl.cancel();
            ctrl.closeModal();
            ctrl.createContactAddressJunction();
            ctrl.createCustomSetting();
            ctrl.resetInvPanel();
            ctrl.changeInvoiceDetails();
            Test.stopTest();
    }
    static testmethod void TestCS_InvcareOfAddressWithAcctId(){
        setupData();
        opp = [Select id from opportunity Limit 1];
        Test.startTest();
            PageReference pf = new PageReference('/apex/cs_CompaniesContacts?AccId='+a.Id);
            Test.setCurrentPage(pf);  
            cs_companiesContactsController ctrl = new cs_companiesContactsController();
            ctrl.initCompaniesContacts();
            ctrl.save();
            ctrl.getJobContacts();
            ctrl.getInstructingAddress();
            ctrl.checkCareOf();
            ctrl.getInvoicingContact();
            ctrl.getInvoicingContactAddress();
            ctrl.getItems();
            ctrl.getInvItems();
            ctrl.cancel();
            ctrl.closeModal();
            ctrl.createContactAddressJunction();
            ctrl.createCustomSetting();
            ctrl.resetInvPanel();
            ctrl.changeInvoiceDetails();
            Test.stopTest();
    }

}