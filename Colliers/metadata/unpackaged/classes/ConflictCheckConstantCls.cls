// ===========================================================================
// Component: ConflictCheckConstantCls
//    Author: Nidheesh N
// Copyright: 2018 by Dquotient
//   Purpose: To store the constant values, which were using in conflictcheck 
//            class.
// ===========================================================================
// ChangeLog: 2018-01-14 Nidheesh Initial version.
//            
// ===========================================================================

public without sharing class ConflictCheckConstantCls {
    // Picklist value constants
    public Static final String CONFLICT_PICKLIST_ALL = 'All';
    public Static final String CONFLICT_PICKLIST_NONE = '---None---';
    public Static final String CONFLICT_PICKLIST_NO_PROPERTY = 'No Property';
    public Static final String CONFLICT_MONTH_CONSTANT = ' Months';
    // Radius distance map b/w labels and values.
    public Static final Map<String,String> CONFLICT_RADIUS_VALUE_MAP = new Map<String,String>{'0' =>'0 Miles','.125' =>'1/8 Miles','.25' =>'1/4 Miles','.5' =>'1/2 Miles','1' =>'1 Miles'};
}