/**
 *  Description: Class to implement create and edit jobs(recordType == joblite)
 *  Company: dQuotient
 *  CreatedDate:26/12/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Nidheesh             26/12/2016               Orginal Version 
 *
 */
 public without sharing class CS_otherpartyPageController{
     public String oppId{get;set;}
     public List<Opportunity> mainjob{get;set;}
     public List<Other_Party__c> otherpartylist{get;set;}
     public Boolean showErrMsg{get;set;}
     public Boolean showsuccess{get;set;}
     public List<string>errMsgLst{get;set;}
     public List<string>successmsg{get;set;}
     public String oathcode{get;set;}
     // Constructor
    public CS_otherpartyPageController() {
        mainjob =new List<Opportunity>();
        showErrMsg = false;
        showsuccess = false;
        errMsgLst = new List<String>();
        successmsg = new List<String>();
        otherpartylist = new List<Other_Party__c>();
        oathcode = ApexPages.currentPage().getParameters().get('auth');
        if(ApexPages.currentPage().getParameters().containsKey('auth')){
            oathcode = ApexPages.currentPage().getParameters().get('auth');
            if(ApexPages.currentPage().getParameters().containsKey('id')){
                initiatlfunction();
            }
        }else{
            initiatlfunction();
        }
    }
     /**
    *   Method Name: closeModal
    *   Description: method to close Modal
    *   Param: None
    *   Return: None
    */
    public void closeModal(){
        errMsgLst = new List<String>();
        showErrMsg = false;
    }
    
    public void initiatlfunction(){
        otherpartylist = new List<Other_Party__c>();
        oppId =  ApexPages.currentPage().getParameters().get('id');
        if(!string.isBlank(oppId) && oppId!= null){
            mainjob = [Select id,Name,(select id,Company__c,Company_Role__c from Other_Parties__r) from Opportunity where id=:oppId];
            if(!mainjob.isEmpty()){
                if(!mainjob[0].Other_Parties__r.isEmpty()){
                    if(mainjob[0].Other_Parties__r.size()>=3){
                        for(Integer i=0;i<3;i++){
                            otherpartylist.add(mainjob[0].Other_Parties__r[i]);
                        }
                    }else{
                        otherpartylist.addAll(mainjob[0].Other_Parties__r);
                        Integer x= 3 -mainjob[0].Other_Parties__r.size();
                        for(Integer i=0;i<x;i++){
                            Other_Party__c newobj = new Other_Party__c();
                            otherpartylist.add(newobj);
                        }
                    }
                }else{
                    for(Integer i=0;i<3;i++){
                        Other_Party__c newobj = new Other_Party__c();
                        otherpartylist.add(newobj);
                    }
                }
            }
        }
            
    }
    // saving Other Party
    public void Save(){
        System.debug('Object msg'+otherpartylist);
        //otherpartylist.clear();
        successmsg = new List<String>();
        errMsgLst = new List<String>();
        //jobDetails.Client_contact_name__c = 'tested';
        List<Other_Party__c> lsttoupsert = new List<Other_Party__c>();
        if(!otherpartylist.isEmpty()){
            for(Other_Party__c objother : otherpartylist){
                if(objother.Company__c != null && string.isNotBlank(objother.Company__c)){
                    objother.Job__c = oppId;
                    lsttoupsert.add(objother);
                }
            }
        }
        if(!lsttoupsert.isEmpty()){
            try{
                upsert lsttoupsert;
                showsuccess = true;
                successmsg.add('OtherParty Record created successfully ');
                /*PageReference OpporunityPageCancel = new PageReference('/apex/cs_MainJobLitePage?id='+oppId+'&auth='+oathcode );
                OpporunityPageCancel.setRedirect(true);*/
                //return OpporunityPageCancel;
                //jobDetails.Client_contact_name__c = 'tested';
            }catch(System.DMLException e){
                showErrMsg = true;
                errMsgLst.add(''+e.getmessage());
            }
        }
       
    }
 }