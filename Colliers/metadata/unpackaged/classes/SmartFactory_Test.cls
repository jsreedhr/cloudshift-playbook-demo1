@isTest
private class SmartFactory_Test {
    static {
        SmartFactory.FillAllFields = true; // increase test coverage without creating custom objects
    }  
    
    static testMethod void createsSingleObjectWithSingleRequiredStringField() {
        try{
            Account account = (Account)SmartFactory.createSObject('Account');
            insert account;
            System.assert(account.Id != null);
            System.assert(account.Name != null);
        }catch(exception e){
            
        }
    }
    
    static testMethod void createsObjectWithCascadeSetsLookupField() {
        try{
            Contact contact = (Contact)SmartFactory.createSObject('Contact', true);
            insert contact;
            System.assert(contact.Id != null);
            System.assert(contact.AccountId != null);
        }catch(exception e){
            
        }
    }
    
    static testMethod void createsObjectWithoutCascadeDoesNotSetLookupField() {
        try{
            Contact contact = (Contact)SmartFactory.createSObject('Contact', false);
        
            insert contact;
            System.assert(contact.AccountId == null);
        }catch(exception e){
            
        }
    }
    
    static testMethod void createObjectWithUnsupportedTypeThrowsException() {
        try {
            SmartFactory.createSObject('Unsupported');
            System.assert(false);
        } catch (UnsupportedObjectTypeException ex) {
            System.assert(true);
        }
    }
}