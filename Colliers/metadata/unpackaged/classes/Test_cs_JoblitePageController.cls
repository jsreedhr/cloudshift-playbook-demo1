/**
*  Class Name: Test_CS_InvoiceTriggerHandler  
*  Description: This is a Test Class for CS_InvoiceTriggerHandler  
*  Company: dQuotient
*  CreatedDate:29/12/2016 
*
*/
@isTest
private class Test_cs_JoblitePageController
{
    static Account accountObj;
    static Opportunity oppObj;
    static Contact contactObj;
    static Invoice__c invoiceObj;
    static User userObj;
    static User portalAccountOwner1;
    static Staff__c staffObj;
    static Allocation__c allocationObj;
    static Property__c propertyObj;
    static Property__c propertyObj1;
    static Purchase_Order__c purOrderObj;
    static Job_Property_Junction__c jobPropObj;
    static Forecasting__c forecastObj;
    static Disbursements__c disbursementObj;

    static void createData()
    {   
        userObj = TestObjectHelper.createAdminUser(true);
        userobj.UserName ='Test_123@dquotient.com';
        Database.Insert(userObj, true);

        //create guest user
        Profile profile1 = [Select Id from Profile where name = 'customer public access Profile'];
        portalAccountOwner1 = new User(
                                            ProfileId = profile1.Id,
                                            Username ='hormese@test.com',
                                            Alias = 'batman',
                                            Email='bruce.wayne@wayneenterprises.com',
                                            EmailEncodingKey='UTF-8',
                                            Firstname='Bruce',
                                            Lastname='Wayne',
                                            LanguageLocaleKey='en_US',
                                            LocaleSidKey='en_US',
                                            TimeZoneSidKey='America/Chicago'
                                            );
        insert portalAccountOwner1;
        System.runAs(userObj) {
            accountObj = TestObjectHelper.createAccount();
            accountObj.name='Default Company';
            insert accountobj;
            System.assert(accountObj.id!=NULL);
            propertyObj = TestObjecthelper.createProperty();
            propertyObj=TestObjectHelper.createProperty();
            propertyObj.Street_No__c ='Park Lane';
            propertyObj.Post_Code__c ='W1K 3DD';
            propertyObj.Country__c='United Kingdom';
            propertyObj.Geolocation__Latitude__s=51.51;
            propertyObj.Geolocation__Longitude__s=-0.15;
            insert propertyObj;
            System.assert(propertyObj.id!=NULL);
            propertyObj1=TestObjectHelper.createProperty();
            propertyObj1.Street_No__c ='Park Lane,';
            propertyObj1.Post_Code__c ='W1K 3DD,';
            propertyObj1.Country__c='United Kingdom';
            propertyObj1.Geolocation__Latitude__s=51.51;
            propertyObj1.Geolocation__Longitude__s=-0.15;
            insert propertyObj1;
            System.assert(propertyObj1.id!=NULL);
            oppObj = TestObjectHelper.createOpportunity(accountObj);
            insert oppObj;
            System.assert(oppObj.id!=NULL);
            jobPropObj = TestObjecthelper.JobPrptyJn(oppObj,propertyObj);
            insert jobPropObj;
            System.assert(jobPropObj.id!=NULL);
            contactObj = TestObjectHelper.createContact(accountObj);
            contactobj.lastName='Test';
            contactobj.Email ='test1@test1.com';
            insert contactobj;
            System.assert(contactobj.id!=NULL);
           
            staffObj = TestObjecthelper.createStaff();
            staffObj.User__c = userObj.id;
            staffObj.Active__c = true;
            insert staffObj;
            System.assert(staffObj.id!=NULL);
            allocationObj = TestObjectHelper.createAllocation(oppObj,staffObj);
            insert allocationObj;
            System.assert(allocationObj.id!=NULL);
            purOrderObj = TestObjecthelper.createPurchaseOrder(oppObj);
            purOrderObj.PO_Status__c = 'Original';
            purOrderObj.Amount__c =1000;
            purOrderObj.Used_Amount__c=500;
            insert purOrderObj;
            System.assert(purOrderObj.id!=NULL);
            forecastObj = TestObjecthelper.createForecast(allocationObj);
            forecastObj.CS_Forecast_Date__c = system.today().toStartOfMonth();
            insert forecastObj;
            System.assert(forecastObj.id!=NULL);
            // disbursementObj = TestObjecthelper.createDisbursement(oppObj);
            // disbursementObj.Purchase_Cost__c = 1000;
            // disbursementObj.Recharge_Cost__c = 1000;
            // disbursementObj.Category__c = 'Motor Car';
            // disbursementObj.Sub_Category__c ='Damage';
          
            // insert disbursementObj;
            // System.assert(disbursementObj.id!=NULL);
        }
    }

    @isTest
    static void testMethod1()
    {
        createData();
        System.runAs(portalAccountOwner1){
            cs_JoblitePageController cont1 = new cs_JoblitePageController();
    
            PageReference pf = new PageReference('/apex/cs_ManageJobLitePage');
            //PageReference pageRef = Page.cs_ManageJobLitePage;
            //pageRef.getParameters().put('id', oppObj.id );
            //pageRef.getParameters().put('auth', 'yafsf646367' );
            Test.setCurrentPage(pf);
    
            cs_JoblitePageController cont = new cs_JoblitePageController();
            //cont.save();
            
            
            cont.jobDetails.name='Test';
            cont.jobDetails.Client_company_name__c = 'Default Company';
            cont.jobDetails.Client_contact_name__c= 'James Bond';
            cont.jobDetails.Manager__c=staffObj.id;
            cont.jobDetails.Department__c = 'Accurates';
            cont.jobDetails.Work_Type__c = 'License to Kill';
            cont.jobDetails.Office__c = 'Belfast';
            cont.jobDetails.Property_Address__c = propertyObj1.id;
            
            Work_Type__c workTypeObj = new Work_Type__c();
            workTypeObj.name='License To  Kill';
            insert workTypeObj;
            System.assert(workTypeObj.id!=NULL);
    
            Coding_Structure__c codingStructObj = new Coding_Structure__c();
            codingStructObj.Staff__c=staffObj.id;
            codingStructObj.Department__c = 'Accurates';
            codingStructObj.Work_Type__c = workTypeObj.id;
            insert codingStructObj;
            System.assert(codingStructObj.id!=NULL);
            cont.initCompaniesContacts();
            cont.save();
            cont.redirectToconflict();
            cont.redirect();
            cont.ConvertToJob();
            
        }


    }
    @isTest
    static void testMethod2()
    {
        createData();
        system.runAs(userobj)
        {

            //labelObj=new CustomLabels(Name='Accurals', Value='Accurals');
            //insert labelObj;
            //System.assert(labelObj.id!=NULL);

            //labelObj1=new Label(Name='Belfast', Value='Belfast');
            //insert labelObj1;
            //System.assert(labelObj1.id!=NULL);

            PageReference pageRef = Page.cs_ManageJobLitePage;
            pageRef.getParameters().put('AccId', accountObj.id );
            Test.setCurrentPage(pageRef);
            cs_JoblitePageController cont = new cs_JoblitePageController();
            cont.jobDetails.Client_company_name__c = 'Default Company';
            cont.jobDetails.Property_Address__c = propertyObj1.id;
            cont.save();
            cont.jobDetails.Property_Address__c = propertyObj.id;
            cont.save();
            cont.closeModal();
            cont.ConvertToJob();

            cont.jobDetails.name='Test';
            cont.jobDetails.Client_company_name__c = 'Default Company';
            cont.jobDetails.Client_contact_name__c= 'James Bond';
            cont.jobDetails.Manager__c=staffObj.id;
            cont.jobDetails.Department__c = 'Accurates';
            cont.jobDetails.Work_Type__c = 'License to Kill';
            cont.jobDetails.Office__c = 'Belfast';
            
            Work_Type__c workTypeObj = new Work_Type__c();
            workTypeObj.name='License To  Kill';
            insert workTypeObj;
            System.assert(workTypeObj.id!=NULL);

            Coding_Structure__c codingStructObj = new Coding_Structure__c();
            codingStructObj.Staff__c=staffObj.id;
            codingStructObj.Department__c = 'Accurates';
            codingStructObj.Work_Type__c = workTypeObj.id;
            insert codingStructObj;
            System.assert(codingStructObj.id!=NULL);

            cont.save();
            cont.getStaffDept();
            cont.getStaffDeptOffice();
            cont.getStaffDeptOfcWorkType();
        }
    }
     @isTest
    static void testMethod3()
    {
        createData();
            PageReference pf = new PageReference('/apex/cs_ManageJobLitePage?id='+oppObj.Id);
            Test.setCurrentPage(pf);
    
            cs_JoblitePageController cont = new cs_JoblitePageController();
            

            cont.save();
            cont.ConvertToJob();
            cont.getStaffDept();
            cont.getStaffDeptOffice();
            cont.getStaffDeptOfcWorkType();
    }
    




}