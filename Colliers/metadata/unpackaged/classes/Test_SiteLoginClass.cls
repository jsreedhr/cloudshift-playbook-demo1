/**
 *  Class Name: Test_SiteLoginClass 
 *  Description: Test class for the SiteLoginClass
 *  Company: dQuotient
 *  CreatedDate: 31/03/2017 
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nidheesh               31/03/2017                Orginal Version          
 *
 */
@isTest
private class Test_SiteLoginClass {
    //static Site_User_Login__c siteObj;
    static void createData(){
        
    }
    @isTest
    static void testmethod1(){
        Site_User_Login__c siteObj = new Site_User_Login__c();
        siteObj.Login_Email__c = 'hormese@gmail.com';
        siteObj.Password__c = 'qwerty123';
        insert siteObj;
        PageReference pf = new PageReference('/apex/SiteLoginPage');
        Test.setCurrentPage(pf);
        CS_SiteLoginClass con = new CS_SiteLoginClass();
        con.Username = 'hormese@gmail.com';
        con.Password = 'qwerty123';
        con.loginApexPage();
        
    }
}