@isTest
private class Test_CustomAccountLookupController {
    Public static Account a;
    
    static testmethod void setupData(){
        //create account
        a = TestObjectHelper.createAccount();
        a.name = 'test';
        insert a ;
    }
    private static testMethod void test1() {
        setupData();
       
       
        Test.setCurrentPageReference(new PageReference('Page.customAccountLookup'));

        System.currentPageReference().getParameters().put('lksrch', 'Account');
        System.currentPageReference().getParameters().put('txt', 'Account');
        System.currentPageReference().getParameters().put('frm', 'Account');
        
        
        Test.startTest();
        
        CustomAccountLookupController ctrl = new CustomAccountLookupController();  
        ctrl.search();
        ctrl.saveAccount();
        ctrl.getFormTag();
        ctrl.getTextBox();
       
        Test.stopTest();


    }

}