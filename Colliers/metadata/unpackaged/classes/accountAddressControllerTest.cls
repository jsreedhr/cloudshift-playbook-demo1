@isTest
private class accountAddressControllerTest {
   
    Public static Account a;
    static Contact c;
    static opportunity opp;
    static Address__c addr;
    static Account_Address_Junction__c accAddrJn;
    static Address__c addrr = new Address__c();
    
     static testmethod void setupData(){
        //create account
        a = TestObjectHelper.createAccount();
        insert a ;
        Schema.DescribeFieldResult fieldResult = Address__c.Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        c = TestObjectHelper.createContact(a);
        c.Email ='test1@test1.com';
        insert c;
        
        addr = TestObjectHelper.createAddress(a);
        if(!ple.isEmpty()){
            addr.Country__c =ple[0].getValue();
        }
        insert addr;
        
        addrr = TestObjectHelper.createAddress(a);
        addrr.Flat_Number__c='#45';
        addrr.Building__c = 'buildin';
        addrr.Estate__c = 'estate';
        addrr.Area__c= 'area';
        addrr.Street_Number__c='56';
        addrr.Street__c = 'Street';
        addrr.Town__c = 'town';
        addrr.Postcode__c = '12erts45';
        if(!ple.isEmpty()){
            addrr.Country__c =ple[0].getValue();
        }
        //insert addr;
        
        accAddrJn = TestObjectHelper.createAccountAddressJunction(a,addr);
        insert accAddrJn;
              
    }
    
    private static testMethod void test2() {
        setupData();

        List<Address__c> addrLst = new List<Address__c>();
        addrLst.add(addr);
        
        List<accountAddressController.booleanAccountJnWrapper> jnWrper = new List<accountAddressController.booleanAccountJnWrapper>();
        PageReference testPage = new pageReference('/apex/accountAddress');
        testPage.getParameters().put('id', a.id);

        test.setCurrentPage(testPage);   

        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(addrLst);
       
        Test.startTest();
        
        accountAddressController ctrl = new accountAddressController(); 
        ctrl.cons = sc;

        accountAddressController.booleanAccountWrapper wrapp= new accountAddressController.booleanAccountWrapper(true,addr);
        accountAddressController.booleanAccountJnWrapper jnwrapp= new accountAddressController.booleanAccountJnWrapper(addr.id,'building','bc23pr','street','town',accAddrJn.id);
        jnWrper.add(jnwrapp);
        
        ctrl.ListbooleanAccountJnWrapper = jnWrper;
        ctrl.newAddress = addrr;
        ctrl.Ids = a.id;
        ctrl.IdtoDelete = '0';
        ctrl.IsSelectAll = true;

        ctrl.reDirect();
        ctrl.getNewCategories();
        ctrl.getCategorieskeypreix();
        ctrl.saveAddress();
        ctrl.deleteSelectedRow();
        ctrl.selectAllProperty();
        ctrl.first();
        ctrl.firstfilter();
        ctrl.last();
        ctrl.next();
        
        ctrl.cancelJobs();
        ctrl.closeModal();
        ctrl.previous();
       
        Test.stopTest();

    }
}