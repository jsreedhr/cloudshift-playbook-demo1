@isTest(seealldata=True)
public with sharing class VALS_ReflectJobStatusCalloutBatch_Test
{ 
    @isTest
    private static void testMethod1()
    {
     account accountObj = new account ();
      accountObj = TestObjectHelper.createAccount();
      accountObj.PO_Required__c = 'Yes';
      accountObj.Client_VAT_Number__c='123456';
      insert accountobj;
      System.assert(accountObj.id!=NULL);
       
     Property__c propertyObj2 = new Property__c ();
      propertyObj2=TestObjectHelper.createProperty();
      propertyObj2.Street_No__c ='Park Lane 2';
      propertyObj2.Post_Code__c ='W1K 3DD';
      propertyObj2.Country__c = 'United Kingdom';
      propertyObj2.Geolocation__Latitude__s=51.51;
      propertyObj2.Geolocation__Longitude__s=-0.15;
      propertyObj2.Override_Experian_validation__c=true;
      insert propertyObj2;
      System.assert(propertyObj2.id!=NULL);
      
      Address__c addressObj = new Address__c ();
      addressObj = TestObjecthelper.createAddress(accountObj);
      addressObj.Country__c = 'United Kingdom';
      insert addressObj;
      System.assert(addressObj.id!=NULL);

      opportunity oppObj = new opportunity ();   
      oppObj = TestObjectHelper.createOpportunity(accountObj);
      oppObj.Sent_to_ShareDo__c=True;
      oppObj.StageName='Instructed';
      oppObj.Work_Type__C='Test';
      oppObj.AccountId = accountObj.id;
      oppObj.InstructingCompanyAddress__c = addressObj.id;
      oppObj.Property_Address__c = propertyObj2.id;
      oppObj.Instructing_Contact__c = oppObj.Instructing_Contact__c;
       insert oppObj;
      System.assert(oppObj.id!=NULL);
      
      contact contactObj = new contact ();
      contactObj = TestObjecthelper.createContact(accountObj);
      contactObj.Email='Cersei@kingslanding.com';
      contactobj.LastName='Lannister';
      contactObj.HomePhone='12345678';
      contactObj.MobilePhone='12345678';
      contactObj.Phone='12345678';
      contactObj.Fax='12345678';
      insert contactObj;
      System.assert(contactObj.id!=NULL);
      
      Date todaydate = Date.today();
      todaydate=todaydate.addDays(0);
        
        ShareDo_Credentials__c sdc = new ShareDo_Credentials__c();
        sdc.Name = 'Sharedo Inspe';
        sdc.API_Key__c = 'e141df7b-b4e7-458c-a7a7-0f267803fcc8';
        sdc.ShareDo_Password__c = 'SFPsh123!';
        sdc.ShareDo_Username__c = 'EU\\svc_sharedoAPI';
        sdc.shareDoUrl__c = 'https://uatvals-express.colliersemea.com/api/inspections/';
        insert sdc;
        System.assert(sdc.id!=NULL);
        
      Invoice__C inv=new Invoice__C();
      inv.Contact__c=contactObj.id;
      inv.Date_of_Invoice__c=todaydate;
      inv.Opportunity__C=oppObj.id;
      insert inv;
      System.assert(inv.id!=NULL);
        

        
     Test.startTest();      
      VALS_ReflectJobStatusCalloutBatch  bat = new VALS_ReflectJobStatusCalloutBatch ();
      Database.executebatch(bat);
      test.stopTest();      
    }
}