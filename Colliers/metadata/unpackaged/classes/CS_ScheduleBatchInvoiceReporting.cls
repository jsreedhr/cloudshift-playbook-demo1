global class CS_ScheduleBatchInvoiceReporting implements Schedulable  {
    global void execute(SchedulableContext SC) {
        CS_BatchInvoiceReporting objBatch = new CS_BatchInvoiceReporting();
        database.executeBatch(objBatch, 200);
    }
}