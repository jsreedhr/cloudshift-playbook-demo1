/*  Class Name: TestCS_JobCostsController 
 *  Description: This test class for the CS_JobCostsControllercontroller
 *  Company: dQuotient
 *  CreatedDate: 18/08/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Aparna               21/09/2016                  Orginal Version
 *
 */
@isTest 
public class TestCS_JobCostsController {
    @testsetup
    static void setupData(){
        //create account
        Account a = TestObjectHelper.createAccount();
        insert a;       
        //create realted Opportunity
        List <Opportunity> opps = TestObjectHelper.createMultipleOpportunity(a, 10);
        insert opps;
        System.debug('opps---->'+opps);
        //create realted contact
        List <Contact> cons = TestObjectHelper.createMultipleContact(a, 10);
        integer loopNo = 1;
        for(Contact objContact: cons){
            objContact.Email = 'test'+loopNo+'@test.com';
        }
        insert cons;
        System.debug('cons---->'+cons);
        Staff__c staffObj =  new Staff__c();
        staffObj.Name = 'staffdummy';
        staffObj.HR_No__c = String.valueOf(Math.round(Math.random()*1000));
        staffObj.Email__c = 'staff@gmail.com';
        staffObj.Active__c = true;
        insert staffObj;
        Staff__c staffObj1 =  new Staff__c();
        staffObj1.Name = 'staffdummy1';
        staffObj1.HR_No__c = String.valueOf(Math.round(Math.random()*1000));
        staffObj1.Email__c = 'staff1@gmail.com';
        staffObj1.Active__c = true;
        insert staffObj1;
        //Create Disbursements__c data for opportunity
        opportunity x = [Select id from opportunity Limit 1];
        Allocation__c allocObj = new Allocation__c();
        allocObj.Job__c = x.Id;
        allocObj.Allocation_Amount__c = 20000;
        allocobj.Assigned_To__c = staffObj.Id;
        insert allocObj;
        Allocation__c allocObj1 = new Allocation__c();
        allocObj1.Job__c = x.Id;
        allocObj1.Allocation_Amount__c = 20000;
        allocobj1.Assigned_To__c = staffObj1.Id;
        insert allocObj1;
        Disbursements__c cstObj = new Disbursements__c();
        cstObj.Job__c = x.Id;
        cstObj.Allocation__c = allocObj.Id;
        cstObj.Created_For__c = staffObj.Id;
        cstObj.Purchase_Date__c = System.today();
        cstObj.Category__c = 'Motor Car';
        cstObj.Sub_Category__c = 'Car Hire(Fuel Only)';
        cstObj.Description__c = 'test';
        cstObj.Purchase_Cost__c = 1000;
        cstObj.Recharge_Cost__c = 1000;
        cstObj.Invoice_Details__c = 'testinvoice';
        insert cstObj;
        Disbursements__c cstObj1 = new Disbursements__c();
        cstObj1 = cstObj.clone();
        insert cstObj1;
        
    }    
    static testMethod void testCS_JobCostsControllerForOpps(){
        System.debug('opp test---->');
        //setupData();
        opportunity a = [Select id from opportunity Limit 1];
        Test.startTest();
        PageReference pf = new PageReference('/apex/CS_JobCostsControllerPage?id='+a.Id);
        Test.setCurrentPage(pf);
        CS_JobCostsController con = new CS_JobCostsController();
        // if(!con.lstCostWrapper.isEmpty()){
            // con.lstCostWrapper[1].isChecked = true;
            // con.detailWrpId = con.lstCostWrapper[1].wrapperId;
        // }
        con.initcost();
         if(!con.lstCostWrapper.isEmpty()){
            con.lstCostWrapper[1].isChecked = true;
            con.detailWrpId = con.lstCostWrapper[1].wrapperId;
        }
        con.detailJob();
         con.UpdateRechargeCost();
        con.UpdateInvoiceDetail();
        
        // con.getAllocationOpts();
        // con.getRiasedForOpts();
       
        //ApexPages.currentPage().getParameters().put('checkedvalue','true' );        
        con.renderpanel();
        con.close_ErrorModal();
        con.save();
        con.saveAndNew();
        ApexPages.currentPage().getParameters().put('deletedID','2' );
        con.deleteRow();
        con.newJobb();
        con.save();
        con.saveAndNew();
        // con.cancel();
        Test.stopTest();       
    }
   /*static testMethod void testCS_JobCostsControllerForOppsforid(){
        System.debug('opp test---->');
        opportunity a = [Select id from opportunity Limit 1];
        Test.startTest();
        PageReference pf = new PageReference('/apex/CS_JobCostsControllerPage?id=1234');
        Test.setCurrentPage(pf);
        CS_JobCostsController con = new CS_JobCostsController();    
    }*/
}