@RestResource(urlMapping='/ShareDo/inspections/v1/')
global with sharing Class VALS_FeedImageInbound
{
    public Blob responseBody{get;set;}
    @HttpPost
    global static void retrieveImagesRecord()
    {
        
        Set<Id> feedids = new Set<Id> ();
        RestRequest request = RestContext.request;
        List <String> strList = (List <String>)JSON.deserialize(request.requestbody.tostring(), List <String>.Class);
        system.debug('strList---->'+strList);
        for(String imgid : strList)
            feedids.add(imgid);   
        List<FeedItem> feeditemList= [Select Id,ParentId,(SELECT RecordId, Title,type,Value FROM FeedAttachments),Type, Title, Body, CommentCount, LikeCount, LinkUrl, RelatedRecordId,parent.type From FeedItem where id in :feedids]; 
        Blob  body = Blob.valueOf(Json.serialize(feeditemList));  
        RestContext.response.addHeader('Content-Type', 'application/json');
        if(body!=NULL)
        {
            RestContext.response.responseBody = body;
            RestContext.response.statusCode = 200;
        }
        else
        {
           
          RestContext.response.responseBody = Blob.valueOf('Operation Failed');
          RestContext.response.statusCode = 400;
        }
    }
}