<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Amount_without_Concession</fullName>
        <field>Amount_without_Concession__c</field>
        <formula>Amount__c</formula>
        <name>Set Amount without Concession</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Payment_Success_Date_Time</fullName>
        <description>To set Payment Success Date Time</description>
        <field>Payment_Success_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Set Payment Success Date Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Tipi Payments set Amount without Concession</fullName>
        <actions>
            <name>Set_Amount_without_Concession</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To set the &apos;Amount without Concession&apos; field (if it&apos;s blank and if Amount__c has been set).</description>
        <formula>AND(    $Setup.Global_Application_Settings__c.Run_Workflow_Rules__c,   NOT(ISBLANK(Amount__c)),   NOT(ISNULL(Amount__c)),   OR(ISBLANK(Amount_without_Concession__c), ISNULL(Amount_without_Concession__c), Amount_without_Concession__c = 0) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tipi Payments set Collected from Customer Date Time</fullName>
        <actions>
            <name>Set_Payment_Success_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To set the Collected from Customer Date Time field</description>
        <formula>AND(  $Setup.Global_Application_Settings__c.Run_Workflow_Rules__c,  OR( ISNEW(),  ISCHANGED(Status__c)), ISPICKVAL(Status__c, &apos;Collected from customer&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
