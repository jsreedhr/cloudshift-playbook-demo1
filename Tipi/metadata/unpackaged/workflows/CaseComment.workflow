<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Resident Community User Creates Case Comment</fullName>
        <active>false</active>
        <formula>CreatedBy.Profile.Name=&apos;Tipi Resident Community User&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
