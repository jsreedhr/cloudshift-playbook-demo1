<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Task_set_Unit_Inspection_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>leonie@tipi.london</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Task - set Unit Inspection Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Task - set Unit Inspection Owner</fullName>
        <actions>
            <name>Task_set_Unit_Inspection_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Unit Inspection</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
