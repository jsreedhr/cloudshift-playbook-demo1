<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Quote_Leasing_Manager_Quote_Approved</fullName>
        <description>Quote - Leasing Manager Quote Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Tipi_Homes_Email_Templates/Quote_Approved_Quote</template>
    </alerts>
    <alerts>
        <fullName>Quote_Quote_Rejected</fullName>
        <description>Quote - Quote Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Tipi_Homes_Email_Templates/Quote_Rejected_Quote</template>
    </alerts>
    <fieldUpdates>
        <fullName>Quote_Approved_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Quote Approved Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Set_Pricebook_ID</fullName>
        <field>SBQQ__PricebookId__c</field>
        <formula>&quot;01s0N000003w4BE&quot;</formula>
        <name>Quote - Set Pricebook ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Set_Quote_Process_ID</fullName>
        <field>SBQQ__QuoteProcessId__c</field>
        <formula>&quot;a2N0N0000007ZuU&quot;</formula>
        <name>Quote - Set Quote Process ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Set_Quote_Template_ID</fullName>
        <field>SBQQ__QuoteTemplateId__c</field>
        <formula>&quot;a2O0N0000002FMS&quot;</formula>
        <name>Quote - Set Quote Template ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Update_Status_Awaiting_Approval</fullName>
        <field>SBQQ__Status__c</field>
        <literalValue>Awaiting_Approval</literalValue>
        <name>Quote - Update Status Awaiting Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Quote_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Reset Quote Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Approval_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Approval_Required</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Quote Approval Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_To_In_Approval</fullName>
        <field>RecordTypeId</field>
        <lookupValue>In_Approval</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Quote To In Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_HoL_Approval_Required</fullName>
        <field>HoL_Approval_Required__c</field>
        <literalValue>1</literalValue>
        <name>Tick HoL Approval Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_HoL_Approved</fullName>
        <field>HoL_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Tick HoL Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_In_HoL_Approval</fullName>
        <field>In_HoL_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Tick In HoL Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_In_LM_Approval</fullName>
        <field>In_Leasing_Manager_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Tick In LM Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_In_RM_Approval</fullName>
        <field>In_RM_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Tick In RM Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_LM_Approval_Required</fullName>
        <field>Leasing_Manager_Approval_Required__c</field>
        <literalValue>1</literalValue>
        <name>Tick LM Approval Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_Leasing_Manager_Approved</fullName>
        <field>Leasing_Manager_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Tick Leasing Manager Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_RM_Approval_Required</fullName>
        <field>RM_Approval_Required__c</field>
        <literalValue>1</literalValue>
        <name>Tick RM Approval Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_RM_Approved</fullName>
        <field>RM_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Tick RM Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_HoL_Approval_Required</fullName>
        <field>HoL_Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>Untick HoL Approval Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_HoL_Approved</fullName>
        <field>HoL_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Untick HoL Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_In_HoL_Approval</fullName>
        <field>In_HoL_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Untick In HoL Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_In_LM_Approval</fullName>
        <field>In_Leasing_Manager_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Untick In LM Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_In_RM_Approval</fullName>
        <field>In_RM_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Untick In RM Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_Leasing_Manager_Approval_Req_d</fullName>
        <field>Leasing_Manager_Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>Untick Leasing Manager Approval Req&apos;d</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_Leasing_Manager_Approved</fullName>
        <field>Leasing_Manager_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Untick Leasing Manager Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_RM_Approval_Required</fullName>
        <field>RM_Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>Untick RM Approval Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_RM_Approved</fullName>
        <field>RM_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Untick RM Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Approved</fullName>
        <field>SBQQ__Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Draft</fullName>
        <field>SBQQ__Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Update Status Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AP - %C2%A3 Off First Month %28HoL%29</fullName>
        <actions>
            <name>Set_Quote_Approval_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tick_HoL_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_Leasing_Manager_Approval_Req_d</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_RM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,   AND(   Off_First_Month__c &gt;  $Setup.CPQ_Approval_Limits__c.Off_First_Month_LM__c ,   Off_First_Month__c &lt;= $Setup.CPQ_Approval_Limits__c.Off_First_Month_HoL__c  ),  OR(    $UserRole.Name = &quot;Leasing User&quot;,    $UserRole.Name = &quot;Leasing Manager&quot;  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP - %C2%A3 Off First Month %28RM%29</fullName>
        <actions>
            <name>Set_Quote_Approval_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tick_RM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_HoL_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_Leasing_Manager_Approval_Req_d</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,   Off_First_Month__c &gt; $Setup.CPQ_Approval_Limits__c.Off_First_Month_HoL__c,   OR(    $UserRole.Name = &quot;Leasing User&quot;,    $UserRole.Name = &quot;Leasing Manager&quot;,   $UserRole.Name = &quot;Head Of Leasing&quot;  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP - Concession %28HoL%29</fullName>
        <actions>
            <name>Set_Quote_Approval_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tick_HoL_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_Leasing_Manager_Approval_Req_d</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_RM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,   OR(   Concession_Amount__c &gt;  $Setup.CPQ_Approval_Limits__c.Concession_Amount_LM__c   ,   AND(   Concession_Length__c &gt;  $Setup.CPQ_Approval_Limits__c.Concession_Length_LM__c ,   Concession_Length__c &lt;= $Setup.CPQ_Approval_Limits__c.Concession_Length_HoL__c    )  ),  OR(    $UserRole.Name = &quot;Leasing User&quot;,    $UserRole.Name = &quot;Leasing Manager&quot;   )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP - Concession %28Leasing Manager%29</fullName>
        <actions>
            <name>Set_Quote_Approval_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tick_LM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_HoL_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_RM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,   
 OR(
  Concession_Amount__c &gt;  $Setup.CPQ_Approval_Limits__c.Concession_Amount_LA__c ,
  AND(
   Concession_Length__c &gt;  $Setup.CPQ_Approval_Limits__c.Concession_Length_LA__c ,
   Concession_Length__c &lt;= $Setup.CPQ_Approval_Limits__c.Concession_Length_LM__c 
  )
 ),
 $UserRole.Name = &quot;Leasing User&quot;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP - Concession %28RM%29</fullName>
        <actions>
            <name>Set_Quote_Approval_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tick_RM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_HoL_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_Leasing_Manager_Approval_Req_d</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,   OR(   Concession_Amount__c &gt; $Setup.CPQ_Approval_Limits__c.Concession_Amount_HoL__c,   Concession_Length__c &gt; $Setup.CPQ_Approval_Limits__c.Concession_Length_HoL__c    ),   OR(    $UserRole.Name = &quot;Leasing User&quot;,    $UserRole.Name = &quot;Leasing Manager&quot;,   $UserRole.Name = &quot;Head Of Leasing&quot;  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP - Free Month %28HoL%29</fullName>
        <actions>
            <name>Set_Quote_Approval_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tick_HoL_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_Leasing_Manager_Approval_Req_d</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_RM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,  Free_Month__c &gt; $Setup.CPQ_Approval_Limits__c.Free_Month_LA_LM__c,   OR(    $UserRole.Name = &quot;Leasing User&quot;,    $UserRole.Name = &quot;Leasing Manager&quot;  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP - Parking %C2%A3 Off First Month %28Leasing Manager%29</fullName>
        <actions>
            <name>Tick_LM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,  Parking_Off_First_Month__c  &gt; $Setup.CPQ_Approval_Limits__c.Parking_Off_First_Month_LM__c,   $UserRole.Name = &quot;Leasing User&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP - Parking Concession %28HoL%29</fullName>
        <actions>
            <name>Set_Quote_Approval_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tick_HoL_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_Leasing_Manager_Approval_Req_d</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_RM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,  AND(   Parking_Concession_Length__c &gt;  $Setup.CPQ_Approval_Limits__c.Parking_Concession_Length_LM__c,   Parking_Concession_Length__c &lt;= $Setup.CPQ_Approval_Limits__c.Parking_Concession_Amount_HoL__c   ),   OR(    $UserRole.Name = &quot;Leasing User&quot;,    $UserRole.Name = &quot;Leasing Manager&quot;  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP - Parking Concession %28Leasing Manager%29</fullName>
        <actions>
            <name>Set_Quote_Approval_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tick_LM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_HoL_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_RM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,  AND(   Parking_Concession_Length__c &gt;  $Setup.CPQ_Approval_Limits__c.Parking_Concession_Length_LA__c,   Parking_Concession_Length__c &lt;= $Setup.CPQ_Approval_Limits__c.Parking_Concession_Amount_LM__c   ),  $UserRole.Name = &quot;Leasing User&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP - Parking Concession %28RM%29</fullName>
        <actions>
            <name>Set_Quote_Approval_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tick_RM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_HoL_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_Leasing_Manager_Approval_Req_d</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,  Parking_Concession_Length__c &gt; $Setup.CPQ_Approval_Limits__c.Parking_Concession_Amount_HoL__c,   OR(    $UserRole.Name = &quot;Leasing User&quot;,    $UserRole.Name = &quot;Leasing Manager&quot;,   $UserRole.Name = &quot;Head Of Leasing&quot;  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP - Reset Quote Approvals Unit</fullName>
        <actions>
            <name>Reset_Quote_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_HoL_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_Leasing_Manager_Approval_Req_d</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_RM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,

  Off_First_Month__c &lt;= $Setup.CPQ_Approval_Limits__c.Off_First_Month_LA__c ,
  Concession_Amount__c &lt;= $Setup.CPQ_Approval_Limits__c.Concession_Amount_LA__c ,
  Concession_Length__c &lt;= $Setup.CPQ_Approval_Limits__c.Concession_Length_LA__c ,
  Free_Month__c &lt; 1,

  Parking_Off_First_Month__c &lt;= $Setup.CPQ_Approval_Limits__c.Parking_Off_First_Month_LA__c ,
  Parking_Concession_Length__c &lt;= $Setup.CPQ_Approval_Limits__c.Parking_Concession_Length_LA__c,

  Storage_Concession_Length__c &lt;= $Setup.CPQ_Approval_Limits__c.Storage_Concession_Length_LA__c 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP - Voucher Amount %28HoL%29</fullName>
        <actions>
            <name>Set_Quote_Approval_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tick_HoL_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_Leasing_Manager_Approval_Req_d</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_RM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,    AND(   Voucher_Amount__c &gt;  $Setup.CPQ_Approval_Limits__c.Voucher_Amount_LM__c ,   Voucher_Amount__c &lt;= $Setup.CPQ_Approval_Limits__c.Voucher_Amount_HoL__c   ),   OR(   $UserRole.Name = &quot;Leasing User&quot;,   $UserRole.Name = &quot;Leasing Manager&quot;  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP - Voucher Amount %28Leasing Manager%29</fullName>
        <actions>
            <name>Set_Quote_Approval_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tick_LM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_HoL_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_RM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,  AND(  Voucher_Amount__c &gt;  $Setup.CPQ_Approval_Limits__c.Voucher_Amount_LA__c ,  Voucher_Amount__c &lt;= $Setup.CPQ_Approval_Limits__c.Voucher_Amount_LM__c   ),  $UserRole.Name = &quot;Leasing User&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AP - Voucher Amount %28RM%29</fullName>
        <actions>
            <name>Set_Quote_Approval_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tick_RM_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_HoL_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Untick_Leasing_Manager_Approval_Req_d</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $Setup.Global_Application_Settings__c.Run_Validation_Rules__c,   Voucher_Amount__c &gt; $Setup.CPQ_Approval_Limits__c.Voucher_Amount_HoL__c,   OR(    $UserRole.Name = &quot;Leasing User&quot;,    $UserRole.Name = &quot;Leasing Manager&quot;,   $UserRole.Name = &quot;Head Of Leasing&quot;  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Set Quote Process %26 Template %26 Pricebook</fullName>
        <actions>
            <name>Quote_Set_Pricebook_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Set_Quote_Process_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Quote_Set_Quote_Template_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__QuoteProcessId__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
