<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Viewing_Reminder</fullName>
        <description>Send Viewing Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Tipi_Homes_Email_Templates/Viewing_Reminder_Template</template>
    </alerts>
    <alerts>
        <fullName>Viewing_Cancellation_Alert</fullName>
        <description>Viewing Cancellation Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Tipi_Homes_Email_Templates/Viewing_Cancellation_Template</template>
    </alerts>
    <alerts>
        <fullName>Viewing_Confirmation_Alert</fullName>
        <description>Viewing Confirmation Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Tipi_Homes_Email_Templates/Viewing_Confirmation_Template</template>
    </alerts>
    <alerts>
        <fullName>Viewing_Reminder_Alert</fullName>
        <description>Viewing Reminder Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Tipi_Homes_Email_Templates/Viewing_Reminder_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Populate_Contact_Email_field</fullName>
        <field>Contact_Email__c</field>
        <formula>Primary_Contact_Email__c</formula>
        <name>Populate Contact Email field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Populate Contact Email field</fullName>
        <actions>
            <name>Populate_Contact_Email_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Viewing__c.Primary_Contact_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This WF rules populates Contact_Email__c field from Primary_Contact_Email__c formula field. This is required for PB to send out Viewing Confirmation, Cancellation and Reminder emails.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
