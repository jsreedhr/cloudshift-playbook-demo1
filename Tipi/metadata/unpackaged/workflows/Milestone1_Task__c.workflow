<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Tick_Blocked_Checkbox</fullName>
        <field>Blocked__c</field>
        <literalValue>1</literalValue>
        <name>Tick Blocked Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Blocked</fullName>
        <field>Task_Stage__c</field>
        <literalValue>Blocked</literalValue>
        <name>Update Stage to Blocked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Blocked Checkbox</fullName>
        <actions>
            <name>Tick_Blocked_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Milestone1_Task__c.Task_Stage__c</field>
            <operation>equals</operation>
            <value>Blocked</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Status to Blocked</fullName>
        <actions>
            <name>Update_Stage_to_Blocked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Milestone1_Task__c.Blocked__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Milestone1_Task__c.Task_Stage__c</field>
            <operation>notEqual</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
