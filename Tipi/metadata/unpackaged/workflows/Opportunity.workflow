<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Date_Normal_Payments_Start</fullName>
        <field>Date_Normal_Payments_Start2__c</field>
        <formula>IF( 
DAY(Tenancy_Start_Date__c ) &lt;15, 
First_Day_Next_Month__c, 
First_Day_2_Months__c 
)</formula>
        <name>Date Normal Payments Start</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Update_Opportunities_to_Ope</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opportunity: Update Opportunities to Ope</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Update_Record_Type_to_Won</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Won_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opportunity: Update Record Type to Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Temporary_Rent_Concession_End_Date</fullName>
        <field>Temporary_Rent_Concession_End_Date2__c</field>
        <formula>IF ( 

DAY( Tenancy_Start_Date__c ) &lt; 15, 
Last_Day_of_Month__c , 
Last_Day_Next_Month__c 
)</formula>
        <name>Temporary Rent Concession End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X6th_Rent_Due_Date_Calculation</fullName>
        <field>X6th_Rent_Due_Date_2__c</field>
        <formula>DATE( 
YEAR( Tenancy_Start_Date__c ) + FLOOR( ( MONTH ( Tenancy_Start_Date__c ) + 6 - 1 ) / 12 ), 
MOD( MONTH ( Tenancy_Start_Date__c ) + 6 - 1 + 
IF( DAY ( Tenancy_Start_Date__c ) &gt; CASE( MOD( MONTH( Tenancy_Start_Date__c ) + 6 - 1, 12 ) + 1, 
2, 28, 
4, 30, 
6, 30, 
9, 30, 
11, 30, 
31 ), 1, 0 ), 12 ) + 1, 
IF( DAY( Tenancy_Start_Date__c ) &gt; CASE( MOD( MONTH( Tenancy_Start_Date__c ) + 6 - 1, 12 ) + 1, 
2, 28, 
4, 30, 
6, 30, 
9, 30, 
11, 30, 
31 ), 
1, DAY( Tenancy_Start_Date__c ) 
) 
)</formula>
        <name>6th Rent Due Date Calculation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AST Dates</fullName>
        <actions>
            <name>Date_Normal_Payments_Start</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Temporary_Rent_Concession_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>X6th_Rent_Due_Date_Calculation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Tenancy_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Update Closed Won Opportunities to Won Opportunity Record Type</fullName>
        <actions>
            <name>Opportunity_Update_Record_Type_to_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Awaiting Payment,Move In,Resident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Won Opportunity</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Update Opportunities to Open Opportunity Record Type</fullName>
        <actions>
            <name>Opportunity_Update_Opportunities_to_Ope</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Interested,Quoting,Reservation,Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Opportunity</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
