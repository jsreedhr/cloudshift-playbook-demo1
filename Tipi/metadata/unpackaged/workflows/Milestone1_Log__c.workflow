<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Action_Resolution_Date</fullName>
        <field>Resolution_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Action Resolution Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Action_Returned_to_CloudShift_Dat</fullName>
        <field>Date_Returned_to_CloudShift__c</field>
        <formula>TODAY()</formula>
        <name>Update Action Returned to CloudShift Dat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Action Resolution Date</fullName>
        <actions>
            <name>Update_Action_Resolution_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Milestone1_Log__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Action Returned to CloudShift Date</fullName>
        <actions>
            <name>Update_Action_Returned_to_CloudShift_Dat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Milestone1_Log__c.Status__c</field>
            <operation>equals</operation>
            <value>Returned to CloudShift</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
