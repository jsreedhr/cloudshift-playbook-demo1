<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Stamp_Payment_Date</fullName>
        <field>asp04__Payment_Date__c</field>
        <formula>TODAY()</formula>
        <name>Stamp Payment Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Payment Collected from Customer</fullName>
        <actions>
            <name>Stamp_Payment_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
  $Setup.Global_Application_Settings__c.Run_Workflow_Rules__c,
  ISPICKVAL(asp04__Payment_Stage__c, &apos;Collected from customer&apos;),
  ISCHANGED(asp04__Payment_Stage__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
