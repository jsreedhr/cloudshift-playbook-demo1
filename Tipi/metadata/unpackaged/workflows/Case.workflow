<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Community_User_Case_Closed</fullName>
        <description>Community User Case Closed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Tipi_Homes_Email_Templates/Community_User_Case_Closed</template>
    </alerts>
</Workflow>
