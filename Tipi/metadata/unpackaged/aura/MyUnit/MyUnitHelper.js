({
    getCurrentContactRecord : function(component) {
        var action = component.get("c.getCurrentContact");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.currentContact", response.getReturnValue());
                component.set("v.currentContactSerialized", JSON.stringify(response.getReturnValue()));
            }
        });
        $A.enqueueAction(action);
    },

    getUserBookings : function(component) {
        var action = component.get("c.getContactBookings");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.userBookings", response.getReturnValue());
                if (response.getReturnValue()) {
                    component.set("v.bookingsLoaded", true);
                }
            }
        });
        $A.enqueueAction(action);
    },

    getUnitBookings : function(component) {
        var action = component.get("c.getUnitBookings");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.userUnitBookings", response.getReturnValue());
                if (response.getReturnValue()) {
                    component.set("v.unitBookingsLoaded", true);
                }
            }
        });
        $A.enqueueAction(action);
    },

})