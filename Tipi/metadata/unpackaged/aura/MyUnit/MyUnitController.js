({
   doInit : function (component, event, helper) {
        helper.getCurrentContactRecord(component);
        helper.getUserBookings(component);
        helper.getUnitBookings(component);
    },

    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'show');
    },

    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, 'show');
    },

})