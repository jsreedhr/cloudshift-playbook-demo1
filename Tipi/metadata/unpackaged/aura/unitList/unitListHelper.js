({
    getUnitList: function(component) {
        var action = component.get('c.getUnits');
        var listUnits = component.get('v.options_');
        var labels = [];
        listUnits.forEach(function(element) {
            if (element.selected) {
                labels.push(element.label);
            }
        });
        var listBuildings = component.get('v.building_options_');
        var buildings = [];
        listBuildings.forEach(function(element) {
            if (element.selected) {
                buildings.push(element.value);
            }
        });
        action.setParams({
            "furniture" : component.get("v.furniturePicklistChange"),
            "bedrooms" : labels,
            "buildings" : buildings,
            "minPrice" : component.get("v.minPricePicklistChange"),
            "maxPrice" : component.get("v.maxPricePicklistChange"),
            "houseShare" : false,
            "shortTerm" : false,
            "accessibility" : false,
            "parking" : component.get("v.carParkingChange"),
            "storage" : component.get("v.storageChange"),
            "pets" : component.get("v.petsChange"),
            "dateAvailable": new Date(component.get('v.myMoveInDate')).toJSON()
        });
        action.setCallback(this, function(actionResult) {
            var units = actionResult.getReturnValue();
            var unitsAdded = component.get("v.unitsAdded");
            var newUnits = [];
            for (var a = 0; a < units.length; a++) {
                var contains = true;
                for (var i = 0; i < unitsAdded.length; i++) {
                    if (unitsAdded[i].Id == units[a].Id) {
                        contains = false;
                    }
                }
                if(contains) {
                    newUnits.push(units[a]);
                }
            }
            component.set('v.units', newUnits);
            component.set("v.maxPage", Math.floor((newUnits.length + 4) / 5));
            this.renderPage(component);
        });
        $A.enqueueAction(action);
    },

    renderPage: function(component) {
        var records = component.get("v.units"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber - 1) * 5, pageNumber * 5);
        component.set("v.currentUnitsList", pageRecords);
    },

})