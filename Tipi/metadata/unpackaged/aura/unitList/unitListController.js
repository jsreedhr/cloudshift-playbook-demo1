({
    doInit: function(component, event, helper) {        
        helper.getUnitList(component);
    },
    addSelectUnit: function(component, event, helper) {
        event.preventDefault();
        var btnClicked = event.getSource();         
        var btnMessage = btnClicked.get("v.value");
        var selectedCount = component.get("v.selectedCount");
        selectedCount++;
        var newUnits = [];
        var units = component.get("v.units");
        var unitsAdded = component.get("v.unitsAdded");
        for(var i = 0; i < units.length; i++) {
            if(units[i].Id == btnMessage) {
                unitsAdded.push(units[i]);
            } else {
                newUnits.push(units[i]);
            }
        }
        component.set("v.unitsAdded", unitsAdded);
        component.set("v.units", newUnits);
        component.set("v.selectedCount", selectedCount);
        helper.renderPage(component);
        component.set("v.maxPage", Math.floor((newUnits.length+4)/5));
        if (component.get("v.currentUnitsList").length < 1 && 
            component.get("v.pageNumber") != 1 && 
            component.get("v.pageNumber") != 0) {
            component.set("v.pageNumber", component.get("v.pageNumber") - 1);
        }
    },
    removeSelectUnit: function(component, event, helper) {
        event.preventDefault();
        var btnClicked = event.getSource();         
        var btnMessage = btnClicked.get("v.value");
        var selectedCount = component.get("v.selectedCount");
        selectedCount--;
        var newUnits = [];
        var units = component.get("v.units");
        var unitsAdded = component.get("v.unitsAdded");
        for(var i = 0; i < unitsAdded.length; i++) {
            if(unitsAdded[i].Id == btnMessage) {
                units.push(unitsAdded[i]);
            } else {
                newUnits.push(unitsAdded[i]);
            }
        }
        component.set("v.units", units);
        component.set("v.unitsAdded", newUnits);
        component.set("v.selectedCount", selectedCount);
        helper.renderPage(component);
        component.set("v.maxPage", Math.floor((units.length+4)/5));
        if (component.get("v.currentUnitsList").length < 1 && 
            component.get("v.pageNumber") != 1 && 
            component.get("v.pageNumber") != 0) {
            component.set("v.pageNumber", component.get("v.pageNumber") - 1);
        }
    },
    picklistChange : function(component, event, helper) {
        component.set("v.pageNumber", 1),
        helper.getUnitList(component);
    },
    renderPage: function(component, event, helper) {
        helper.renderPage(component);
    },
})