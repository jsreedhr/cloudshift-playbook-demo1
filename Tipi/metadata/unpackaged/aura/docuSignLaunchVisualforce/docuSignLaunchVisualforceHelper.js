({
    // initialise & load the URL
    initialiseHelper : function (cmp, event, helper) {
        const url = '/apex/dsfs__DocuSign_CreateEnvelope?DSEID=0&SourceID=' + cmp.get('v.recordId')
                    + helper.getCRL(cmp, helper)
                    + helper.getCCRM(cmp);

        // Opens url in new tab
        window.open(url, '_blank');
        $A.get('e.force:closeQuickAction').fire();

        /*
        // Opens url in same window
        var urlEvent = $A.get('e.force:navigateToURL');
        urlEvent.setParams({
          'url': url
        });
        urlEvent.fire();
        */
    },

    // To map Salesforce Roles to DocuSign Roles
    getCCRM : function (cmp) {
        return '&CCRM=Tenant 1~Tenant 1;Tenant 2~Tenant 2;Tenant 3~Tenant 3;Tenant 4~Tenant 4;Tenant 5~Tenant 5;' +
               'Tenant 6~Tenant 6;Tenant 7~Tenant 7;Tenant 8~Tenant 8;Guarantor~Guarantor;Guarantor 1~Guarantor 1;' +
               'Guarantor 2~Guarantor 2;Guarantor 3~Guarantor 3;Guarantor 4~Guarantor 4;Guarantor 5~Guarantor 5;' +
               'Salesforce~Salesforce;Landlord~Landlord;Licensor~Licensor;Licensee~Licensee';
    },

    // to map the required lookup fields
    getCRL : function (cmp, helper) {
        const obj = cmp.get('v.simpleRecord');
        const sObjectName = cmp.get('v.sObjectName');
        let crl = '&CRL=';
        if(sObjectName === 'Opportunity') {
            crl += helper.getLookupVal(obj, 'Guarantor__r', 'Guarantor', 1);
            crl += helper.getLookupVal(obj, 'Primary_Contact_Guarantor__r', 'Guarantor 1', 1);
            crl += helper.getLookupVal(obj, 'Resident_2_Guarantor__r', 'Guarantor 2', 1);
            crl += helper.getLookupVal(obj, 'Resident_3_Guarantor__r', 'Guarantor 3', 1);
            crl += helper.getLookupVal(obj, 'Resident_4_Guarantor__r', 'Guarantor 4', 1);
            crl += helper.getLookupVal(obj, 'Resident_5_Guarantor__r', 'Guarantor 5', 1);
            crl += helper.getLookupVal(obj, 'Primary_Contact__r', 'Tenant 1', 1);
            crl += helper.getLookupVal(obj, 'Tenant_2__r', 'Tenant 2', 1);
            crl += helper.getLookupVal(obj, 'Tenant_3__r', 'Tenant 3', 1);
            crl += helper.getLookupVal(obj, 'Tenant_4__r', 'Tenant 4', 1);
            crl += helper.getLookupVal(obj, 'Tenant_5__r', 'Tenant 5', 1);
            crl += helper.getLookupVal(obj, 'Landlord__r', 'Landlord', 2);
        } else if (sObjectName === 'Booked_Rentable_Item__c') {
            crl += helper.getLookupVal(obj, 'Licensor__r', 'Licensor', 1);
            crl += helper.getLookupVal(obj, 'Contact__r', 'Licensee', 2);
        }

        // remove last comma
        if(crl.endsWith(',')) {
            crl = crl.substring(0, crl.length - 1);
        }
        return crl;
    },

    // to retrieve values for a lookup field
    getLookupVal : function (obj, lookup, role, routingOrder) {
        let stringVal = '';

        if(obj[lookup] != null) {
            stringVal += 'Email~' + obj[lookup]['Email'] + ';';
            stringVal += 'Role~' + role + ';';
            stringVal += 'FirstName~' + obj[lookup]['FirstName'] + ';';
            stringVal += 'LastName~' + obj[lookup]['LastName'] + ';';
            stringVal += 'RoutingOrder~' + routingOrder + ',';
        }

        return stringVal;
    }
});