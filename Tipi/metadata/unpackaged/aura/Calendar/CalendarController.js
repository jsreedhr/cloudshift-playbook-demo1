({
    afterScriptsLoaded: function(component, event, helper){
        helper.fetchEvents(component);
    },

    cancelBooking: function(component, event, helper) {
        component.set("v.bookingHours", 4);
        component.set("v.choosenTimeInterval", null);
        component.set("v.strTimeInterval", '');
        component.set("v.showTimePicker", false);
        component.set("v.showDayPicker", false);
        component.set("v.bookingEndTimes", []);
        component.set("v.choosenStartTime", '');
    },

    callConfirmationScreen: function(component, event, helper) {
        if (component.get("v.showDayPicker")) {
            if (!component.get("v.carNumber")) {
                var carNumberField = component.find("carNumberInput");
                carNumberField.set("v.errors", [{message: component.get("v.carNumberLabel")/*'Please, enter the Car Number!'*/}]);
                return;
            }
        }
        if (component.get("v.showTimePicker")) {
            if (!component.get("v.strTimeInterval")) {
                var inputTimeInterval = component.find("inputTimeInterval");
                if (component.get("v.choosenStartTime") != '') {
                    inputTimeInterval.set("v.errors", [{message: component.get("v.chooseEndLabel")/*'Please, select End Time!'*/}]);
                } else {
                    inputTimeInterval.set("v.errors", [{message: component.get("v.chooseStartLabel")/*'Please, select Start Time!'*/}]);
                }
                return;
            }
        }
        component.set("v.bookingConfirmation", true);
    },

    confirmButton: function(component, event, helper) {
        component.set("v.bookingConfirmation", false);
        if (component.get("v.showTimePicker")) {
            var a = component.get('c.checkAndCreateNewBooking');
            $A.enqueueAction(a);
        }
        if (component.get("v.showDayPicker")) {
            var a = component.get('c.createNewBooking');
            $A.enqueueAction(a);
        }
    },

    cancelConfirmation: function(component, event, helper) {
        component.set("v.bookingConfirmation", false);
    },

    timeChoosen: function(component, event, helper) {
        var inputTimeInterval = component.find("inputTimeInterval");
        if (inputTimeInterval) {
            inputTimeInterval.set("v.errors", [{message: ''}]);
        }
        helper.updateClasses(component, event, 'selectedTime');

        var selectedItem = event.currentTarget;
        if ($A.util.hasClass(selectedItem, "selectedTime")) {
            component.set("v.choosenStartTime", selectedItem.dataset.start);
        } else {
            component.set("v.choosenStartTime", '');
        }
    },

    dayChoosen: function(component, event, helper) {
        var selectedItem = event.currentTarget;
        $A.util.toggleClass(selectedItem, 'selectedTime');
        helper.addDaysInterval(component, event);
    },

    createNewBooking: function(component, event, helper) {
        helper.createBookingRecord(component);
    },

    okButton: function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },

    startTimeChanged: function(component, event, helper) {
        component.set('v.strTimeInterval', '');
        component.set("v.choosenTimeInterval", null);
        component.set("v.endBookingDateTime", '');
        var dateTime = component.get("v.currentDay");
        component.set("v.startBookingDateTime", dateTime+'T'+component.get("v.choosenStartTime")+':00Z');
        helper.calculateEndTimeSlots(component);
    },

    checkAndCreateNewBooking: function(component, event, helper) {
        if (helper.bookingAvailable(component, event, helper)) {
            helper.createBookingRecord(component);
        } else {
            var timeIntervalField = component.find("inputTimeInterval");
            timeIntervalField.set("v.errors", [{message: component.get("v.bookingNotAvailableLabel")}]);
        }
    },

    endTimeChoosen: function(component, event, helper) {
        var inputTimeInterval = component.find("inputTimeInterval");
        inputTimeInterval.set("v.errors", [{message: ''}]);
        helper.updateClasses(component, event, 'selectedEndTime');
        var selectedItem = event.currentTarget;
        if ($A.util.hasClass(selectedItem, 'selectedEndTime')) {
            var dateTime = component.get("v.currentDay");
            if (selectedItem.dataset.start === '00:00') {
                dateTime = new Date(dateTime);
                dateTime.setDate(dateTime.getDate() + 1);
                dateTime = dateTime.toISOString().substr(0, dateTime.toISOString().indexOf("T"));
            }
            component.set("v.endBookingDateTime", dateTime+'T'+selectedItem.dataset.start+':00Z');
            helper.timeIntervalChoosen(component);
        } else {
            component.set('v.strTimeInterval', '');
            component.set("v.choosenTimeInterval", null);
            component.set("v.endBookingDateTime", '');
        }
    },

})