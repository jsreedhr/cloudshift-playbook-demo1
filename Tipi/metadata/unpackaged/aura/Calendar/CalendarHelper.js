({
    loadDataToCalendar :function(component, data){
        var self = this;
        var ele = component.find('calendar').getElement();
        var isAllDayEvents = component.get("v.eventType") === 'Social_Space__c' ? false : true;
        $(ele).fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            defaultDate: Date.now(),
            defaultView: 'month',
            eventLimit: true,
            //events:data,
            displayEventTime: false,
            dayClick: function (date, jsEvent, view, resource) {
                var dayAvailability = $(this).hasClass('dayAvailable') || $(this).hasClass('dayPartiallyAvailable');
                if (moment(new Date(date.format())).format("YYYY-MM-DD") >= moment(new Date()).format("YYYY-MM-DD") && dayAvailability) {
                    component.set("v.currentDay", date.format());
                    self.createAvailableTimeList(component);
                    if (component.get("v.eventType") === 'Social_Space__c') {
                        var timeDefault = moment(new Date(date.format())).format("YYYY-MM-DD");
                        component.set("v.startBookingDateTime", moment(new Date(date.format())).format("YYYY-MM-DD"));
                        self.createTimePickers(component);
                        component.set("v.showTimePicker", true);
                    }
                    if (component.get("v.eventType") === 'Rentable_Item__c') {
                        self.createDayPickers(component);
                        component.set("v.showDayPicker", true);
                    }
                }
            },
            dayRender: function (date, cell) {
                if (moment(new Date(date.format())).format("YYYY-MM-DD") >= moment(new Date()).format("YYYY-MM-DD")) {
                    if(self.getAvailability(component, date) == null) {
                        cell.addClass('dayPartiallyAvailable');
                    } else if (self.getAvailability(component, date)) {
                        cell.addClass('dayAvailable');
                    } else {
                        cell.addClass('dayNotAvailable');
                    }
                }
            }
        });
        component.set("v.showLegend", true);
    },

    tranformToFullCalendarFormat : function(component, events) {
        var eventArr = [];
        var self = this;
        for(var i = 0;i < events.length;i++) {
            var title = '';
            var start, end;
            if (component.get("v.eventType") === 'Social_Space__c') {
                var timezone = $A.get("$Locale.timezone");
                start = new Date(events[i].Booking_Start__c).toLocaleString('en-US', { timeZone: timezone });
                end = new Date(events[i].Booking_End__c);
                if (!self.datecompare(new Date(events[i].Booking_Start__c), '===', end)) {
                    end.setDate(end.getDate() + 1 );
                }
                end = end.toLocaleString('en-US', { timeZone: timezone });
                var options = { timeZone: timezone, hour12: false, hour: '2-digit', minute:'2-digit'};
                title = new Date(events[i].Booking_Start__c).toLocaleTimeString('en-US', options) + ' - ' + new Date(events[i].Booking_End__c).toLocaleTimeString('en-US', options);
            }
            if (component.get("v.eventType") === 'Rentable_Item__c') {
                start = events[i].Start_Date__c;
                end = new Date(events[i].End_Date__c);
                if (!self.datecompare(new Date(start), '===', end)) {
                    end.setDate(end.getDate() + 1);
                }
                title = events[i].Rentable_Item__r.Name;
            }
            eventArr.push({
                'time':'',
                'id': events[i].Id,
                'start': start,
                'end': end,
                'title': title
            });
        }
        return eventArr;
    },

    fetchEvents : function(component, event) {
        var self = this;
        var action = component.get("c.getEvents");
        action.setParams({
            "objectId" : component.get("v.objectId"),
            "eventType" : component.get("v.eventType")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                var eventArr = self.tranformToFullCalendarFormat(component, response.getReturnValue());
                component.set("v.events", response.getReturnValue());
                self.loadDataToCalendar(component, eventArr);
            }
        });
        $A.enqueueAction(action);
    },

    createAvailableTimeList: function(component) {
        var self = this;
        var timezone = 'Etc/UTC';
        var timezoneLocal = $A.get("$Locale.timezone");
        var options = {timeZone: timezone, hour12: false, hour: '2-digit', minute:'2-digit'};
        var start = new Date(Number(component.get("v.socialSpaceStart")));
        var end = new Date(Number(component.get("v.socialSpaceEnd")));
        if (end.getUTCHours() === 0) {
            end.setDate(end.getDate() + 1);
        }

        var timeAvailable = [];
        var currentDay = new Date(component.get("v.currentDay"));
        var events = component.get("v.events");
        events = events.filter(item =>
            self.datecompare(
                new Date(new Date(item.Booking_Start__c).getUTCFullYear(), new Date(item.Booking_Start__c).getUTCMonth(), new Date(item.Booking_Start__c).getUTCDate()),
                '<=',
                currentDay)
            &&
            self.datecompare(
                new Date(new Date(item.Booking_End__c).getUTCFullYear(), new Date(item.Booking_End__c).getUTCMonth(), new Date(item.Booking_End__c).getUTCDate()),
                '>=',
                currentDay)
        );

        do {
            var timeAvailableValue = true
            for (var i = 0; i < events.length; i++) {
                var startDate = new Date(events[i].Booking_Start__c);
                var endDate = new Date(events[i].Booking_End__c);
                var startHours = parseInt(moment(new Date(startDate).toLocaleString('en-US', { timeZone: timezoneLocal })).format("HH"));
                var startMinutes = parseInt(moment(new Date(startDate).toLocaleString('en-US', { timeZone: timezoneLocal })).format("mm"));
                var endHours = parseInt(moment(new Date(endDate).toLocaleString('en-US', { timeZone: timezoneLocal })).format("HH"));
                var endMinutes= parseInt(moment(new Date(endDate).toLocaleString('en-US', { timeZone: timezoneLocal })).format("mm"));
                if (endHours === 0) {
                    endHours = 24;
                }
                var startTimeValue = parseInt(moment(new Date(start).toLocaleString('en-US', { timeZone: timezone })).format("HH"));
                var startTimeValueMin = parseInt(moment(new Date(start).toLocaleString('en-US', { timeZone: timezone })).format("mm"));
                if (startHours < startTimeValue  && startTimeValue < endHours) {
                    timeAvailableValue = false;
                    break;
                }
                if (startHours === startTimeValue  && startMinutes < startTimeValueMin) {
                    timeAvailableValue = false;
                    break;
                }
            }
            if (timeAvailableValue) {
                if (timeAvailable.filter(item => item === start.toLocaleTimeString('en-US', options)).length === 0) {
                    timeAvailable.push(start.toLocaleTimeString('en-US', options));
                } else if (start.toLocaleTimeString('en-US', options) ===  '00:00' && end.getUTCHours() == 0) {
                    timeAvailable.push('24:00');
                }
            }
            start.setMinutes(start.getMinutes() + 30);
        } while (start <= end);

        var unavailableHours = [];
        for (var i = 0; i < timeAvailable.length; i++) {
            var endBooking = '';
            var start = parseInt(timeAvailable[i].substr(0, timeAvailable[i].indexOf(":")));
            start += 4;
            if (start > 9) {
                endBooking = start + ':' + timeAvailable[i].substr(timeAvailable[i].indexOf(":") + 1, 2);
            } else {
                endBooking = '0' + start + ':' + timeAvailable[i].substr(timeAvailable[i].indexOf(":") + 1, 2);
            }
            if (timeAvailable.filter(item => item === endBooking).length === 0) {
                unavailableHours.push(timeAvailable[i]);
            }
        }

        timeAvailable = timeAvailable.filter(item => !unavailableHours.includes(item));
        var unavailableHours = [];
        for (var i = 0; i < timeAvailable.length; i++) {
            for (var j = 0; j < events.length; j++) {
                var startDate = new Date(events[j].Booking_Start__c);
                var endDate = new Date(events[j].Booking_End__c);
                var startHours = parseInt(moment(new Date(startDate).toLocaleString('en-US', { timeZone: timezoneLocal })).format("HH"));
                var startMinutes = parseInt(moment(new Date(startDate).toLocaleString('en-US', { timeZone: timezoneLocal })).format("mm"));
                var startTimeValue = parseInt(timeAvailable[i].substr(0, timeAvailable[i].indexOf(":")));
                var startTimeValueMin = parseInt(timeAvailable[i].substr(timeAvailable[i].indexOf(":") + 1, 2));
                if (startHours === startTimeValue && startMinutes === startTimeValueMin) {
                    unavailableHours.push(timeAvailable[i]);
                }
            }
        }
        timeAvailable = timeAvailable.filter(item => !unavailableHours.includes(item));
        component.set("v.availableTimeList", timeAvailable);
    },

    getAvailability: function (component, date) {
        var result = true;
        var self = this;
        var events = component.get("v.events");
        var timezone = $A.get("$Locale.timezone");
        var availableHours = (new Date(Number(component.get("v.socialSpaceEnd"))).getTime() - new Date(Number(component.get("v.socialSpaceStart"))).getTime()) / 3600000;
        if (availableHours <= 0) {
            availableHours = 24
        }

        if (component.get("v.eventType") === 'Social_Space__c') {
            var currentDate = new Date(date);
            events = events.filter(item =>
                self.datecompare(
                    new Date(new Date(item.Booking_Start__c).getUTCFullYear(), new Date(item.Booking_Start__c).getUTCMonth(), new Date(item.Booking_Start__c).getUTCDate()),
                    '<=',
                    currentDate)
                &&
                self.datecompare(
                    new Date(new Date(item.Booking_End__c).getUTCFullYear(), new Date(item.Booking_End__c).getUTCMonth(), new Date(item.Booking_End__c).getUTCDate()),
                    '>=',
                    currentDate)
            );

            var totalBookedHours = 0;
            for (var i = 0; i < events.length; i++) {
                var startDate = new Date(events[i].Booking_Start__c);
                startDate = new Date(startDate.toLocaleString('en-US', { timeZone: timezone }));
                var endDate = new Date(events[i].Booking_End__c);
                endDate = new Date(endDate.toLocaleString('en-US', { timeZone: timezone }));
                if (self.datecompare(startDate, '<', endDate) && self.datecompare(endDate, '>', currentDate)) {
                    endDate.setDate(currentDate.getDate() + 1);
                    endDate.setHours(0);
                    endDate.setMinutes(0);
                }
                if (self.datecompare(startDate, '<', endDate) && self.datecompare(startDate, '<', currentDate)) {
                    startDate = currentDate;
                    startDate.setHours(0);
                    startDate.setMinutes(0);
                }
                totalBookedHours += ((endDate.getTime() - endDate.getTimezoneOffset() / 60) - (startDate.getTime() - startDate.getTimezoneOffset() / 60)) / 3600000;
            }
            if (totalBookedHours > availableHours - 4) {
                result = false;
            } else if (totalBookedHours > 0) {
                result = null;
            }
        } else if (component.get("v.eventType") === 'Rentable_Item__c') {
            var currentDate = new Date(date);
            var parkingItems = component.get("v.parkingItems");
            parkingItems = parkingItems.filter(item => item.Building__c === component.get("v.objectId"));
            var availableItems = [];
            for (var j = 0; j < events.length; j++) {
                for (var i = 0; i < parkingItems.length; i++) {
                    if (self.datecompare(new Date(events[j].Start_Date__c), '<=', currentDate) && self.datecompare(new Date(events[j].End_Date__c), '>=', currentDate)) {
                        if (parkingItems[i].Id == events[j].Rentable_Item__c) {
                            availableItems.push(parkingItems[i]);
                        }
                    }
                }
            }
            if (availableItems.length >= parkingItems.length) {
                result = false;
            }
        }

        return result;
    },

    createTimePickers : function (component) {
        var self = this;
        var timePickers = [];
        var events = component.get("v.events");
        var selectedDay = new Date(component.get("v.currentDay"));
        selectedDay.setHours(0);
        events = events.filter(item =>
            self.datecompare(
                new Date(new Date(item.Booking_Start__c).getUTCFullYear(), new Date(item.Booking_Start__c).getUTCMonth(), new Date(item.Booking_Start__c).getUTCDate()),
                '<=',
                selectedDay)
            &&
            self.datecompare(
                new Date(new Date(item.Booking_End__c).getUTCFullYear(), new Date(item.Booking_End__c).getUTCMonth(), new Date(item.Booking_End__c).getUTCDate()),
                '>=',
                selectedDay)
        );
        var timeIntervalValue = component.get("v.timeInterval");

        var startAvailability = new Date(Number(component.get("v.socialSpaceStart")));
        var endAvailability = new Date(Number(component.get("v.socialSpaceEnd")));

        var t = 0;
        do {
         var timeInterval = new Object();
            var d = new Date(selectedDay);
            d.setHours(t);
            timeInterval.startTime = moment(d).format("HH:mm");
            timeInterval.startDate = moment(d);
            d = new Date(selectedDay);
            if (t + component.get("v.timeInterval") > 23) {
                d.setHours(0);
            } else {
             d.setHours(t + component.get("v.timeInterval"));
            }
            timeInterval.endDate = moment(d);
            timeInterval.endTime = moment(d).format("HH:mm");
            timeInterval.interval = timeInterval.startTime;//+ ' - ' + timeInterval.endTime;
            timeInterval.isAvailable = true;
            timeInterval.isPartiallyAvailable = false;
            timeInterval.isDisplay = true;
            timePickers.push(timeInterval);
            t += timeIntervalValue;
        } while (t < 24);

        var self = this;
        var timezone = $A.get("$Locale.timezone");
        for (var i = 0; i < events.length; i++) {
            for (var t = 0; t < timePickers.length; t++) {
                var startDate = new Date(events[i].Booking_Start__c);
                var endDate = new Date(events[i].Booking_End__c);
                if (self.datecompare(new Date(startDate), '<=' , selectedDay) && self.datecompare(selectedDay, '<=' , new Date(endDate))) {
                    var startHours = parseInt(moment(new Date(startDate).toLocaleString('en-US', { timeZone: timezone })).format("HH"));
                    var endHours = parseInt(moment(new Date(endDate).toLocaleString('en-US', { timeZone: timezone })).format("HH"));
                    var timePickerStart = parseInt(timePickers[t].startTime.substr(0,timePickers[t].startTime.indexOf(":")));
                    var timePickerEnd = parseInt(timePickers[t].endTime.substr(0,timePickers[t].endTime.indexOf(":")));
                    if (timePickerEnd === 0) {
                        timePickerEnd = 24;
                    }
                    if (endHours === 0) {
                        endHours = 24;
                    }
                    if (self.datecompare(selectedDay, '===' , new Date(startDate))) {
                        if (self.datecompare(selectedDay, '===' , new Date(endDate))) {
                            if (startHours < timePickerEnd && endHours > timePickerStart) {
                                timePickers[t].isAvailable = false;
                            }
                        } else if (self.datecompare(selectedDay, '<' , new Date(endDate))) {
                            if (startHours < timePickerEnd ||  timePickerEnd === 0) {
                                timePickers[t].isAvailable = false;
                            }
                        }
                    } else if (self.datecompare(selectedDay, '>' , new Date(startDate))) {
                        if (self.datecompare(selectedDay, '===' , new Date(endDate))) {
                            if (endHours > timePickerStart || endHours === 0) {
                                timePickers[t].isAvailable = false;
                            }
                        } else if (self.datecompare(selectedDay, '<' , new Date(endDate))) {
                            timePickers[t].isAvailable = false;
                        }
                    }
                }
            }
        }

        var availableStartTimeList = component.get("v.availableTimeList");
        var startAvailabilityHours = parseInt(moment(new Date(startAvailability).toLocaleString('en-US', { timeZone: 'Etc/UTC' })).format("HH"));
        var endAvailabilityHours = parseInt(moment(new Date(endAvailability).toLocaleString('en-US', { timeZone: 'Etc/UTC' })).format("HH"));
        if (endAvailabilityHours === 0) {
            endAvailabilityHours = 24;
        }
        for (var t = 0; t < timePickers.length; t++) {
            if (parseInt(timePickers[t].endTime.substr(0, timePickers[t].endTime.indexOf(":"))) != 0 && parseInt(timePickers[t].endTime.substr(0, timePickers[t].endTime.indexOf(":"))) <=
                startAvailabilityHours) {
                timePickers[t].isAvailable = false;
                timePickers[t].isDisplay = false;
            }
            if (parseInt(timePickers[t].startTime.substr(0, timePickers[t].startTime.indexOf(":"))) >=
                endAvailabilityHours) {
                timePickers[t].isAvailable = false;
                timePickers[t].isDisplay = false;
            }
            if (availableStartTimeList.filter(item => item === timePickers[t].startTime).length === 0) {
                timePickers[t].isPartiallyAvailable = true;
            }
        }
        timePickers = timePickers.filter(item => item.isDisplay == true);
        component.set("v.bookingTimes", timePickers);
    },

    createDayPickers : function (component) {
        var self = this;
        var timePickers = [];
        var events = component.get("v.events");
        var selectedDay = new Date(component.get("v.currentDay"));
        selectedDay.setHours(0);

        var t = 0;
        var d = new Date(selectedDay);
        d.setHours(0);
        var availabilityMap = new Map();
        do {
            var timeInterval = new Object();
            timeInterval.date = moment(d);
            timeInterval.formattedDate = moment(d).format('dddd, MMMM D YYYY');

            var parkingItems = component.get("v.parkingItems");
            parkingItems = parkingItems.filter(item => item.Building__c === component.get("v.objectId"));
            var events = component.get("v.events");

            var availableItems = [];
            for (var j = 0; j < events.length; j++) {
                for (var i = 0; i < parkingItems.length; i++) {
                    if (self.datecompare(new Date(events[j].Start_Date__c), '<=', d) &&
                        self.datecompare(new Date(events[j].End_Date__c), '>=', d))
                    {
                        if (parkingItems[i].Id == events[j].Rentable_Item__c) {
                            availableItems.push(parkingItems[i]);
                        }
                    }
                }
            }
            availabilityMap.set(d.getTime(), availableItems);

            if (availabilityMap.get(d.getTime()) != null) {
                if (availabilityMap.get(d.getTime()).length >= parkingItems.length) {
                    timeInterval.isAvailable = false;
                } else {
                    timeInterval.isAvailable = true;
                }
            } else {
                timeInterval.isAvailable = true;
            }
            timePickers.push(timeInterval);

            t += 1;
            d.setDate(d.getDate() + 1);
        } while (t <= component.get("v.visibleDaysNumber"));

        component.set("v.bookingDays", timePickers);
        component.set("v.availabilityMap", availabilityMap);
    },

    createBookingRecord: function(component) {
        var bookedItemId;
        var startDate, endDate;
        if (component.get("v.eventType") === 'Rentable_Item__c') {
            var self = this;
            var availabilityMap = component.get("v.availabilityMap");
            var parkingItems = component.get("v.parkingItems");
            parkingItems = parkingItems.filter(item => item.Building__c === component.get("v.objectId"));
            var start = new Date(component.get("v.choosenTimeInterval").startIntervalDate);
            var end = new Date(component.get("v.choosenTimeInterval").endIntervalDate);

            for (var i = 0; i < parkingItems.length; i++) {
                var parkingAvailable = true;
                do {
                    var bookedItems = availabilityMap.get(start.getTime());
                    if (bookedItems && bookedItems != null && bookedItems.length > 0) {
                        for (var j = 0; j < bookedItems.length; j++) {
                            if (bookedItems[j].Id == parkingItems[i].Id) {
                                parkingAvailable = false;
                            }
                        }
                    }
                    start.setDate(start.getDate() + 1);
                } while (self.datecompare(start, '<=', end));
                if (parkingAvailable) {
                    bookedItemId = parkingItems[i].Id;
                    break;
                }
            }
            startDate = moment(component.get("v.choosenTimeInterval").startIntervalDate).format('YYYY-MM-DD');
            endDate = moment(component.get("v.choosenTimeInterval").endIntervalDate).format('YYYY-MM-DD');
        } else {
            bookedItemId = component.get("v.objectId");
            startDate = component.get("v.choosenTimeInterval").startIntervalDate;
            endDate = component.get("v.choosenTimeInterval").endIntervalDate;
        }

        if (bookedItemId) {
            var action = component.get("c.createBooking");
            action.setParams({
                "bookedItemId" : bookedItemId,
                "currentContactSerialized" : component.get("v.currentContact"),
                "startTime" : startDate,
                "endTime" :  endDate,
                "carNumber": component.get("v.carNumber")
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                component.set("v.showTimePicker", false);
                if (response.getReturnValue()) {
                    component.set("v.bookingSuccessful", true);
                    var newBookingRecord = response.getReturnValue();
                    if (component.get("v.eventType") === 'Rentable_Item__c') {
                        newBookingRecord.Start_Date__c = moment(newBookingRecord.Start_Date__c).format('YYYY-MM-DD');
                        newBookingRecord.End_Date__c = moment(newBookingRecord.End_Date__c).format('YYYY-MM-DD');
                    } else {
                        var timezone = $A.get("$Locale.timezone");
                        newBookingRecord.Booking_Start__c = new Date(newBookingRecord.Booking_Start__c).toLocaleString('en-US', { timeZone: timezone });
                        newBookingRecord.Booking_End__c = new Date(newBookingRecord.Booking_End__c).toLocaleString('en-US', { timeZone: timezone });
                    }
                    component.set("v.bookedItemConfirmation", newBookingRecord);
                } else {
                    $A.get('e.force:refreshView').fire();
                }
            } else {
                $A.get('e.force:refreshView').fire();
            }
        });

        $A.enqueueAction(action);
        }

    },

    addDaysInterval: function(component, event) {
        var selectedIntervals = document.getElementsByClassName("selectedTime");
        var selectedItem = event.currentTarget;
        var startIntervalDate, endIntervalDate;
        var startIntervalDateFormatted, endIntervalDateFormatted;
        var self = this;
        for (var i = 0; i < selectedIntervals.length; i++) {
            if (!startIntervalDate || self.datecompare(new Date(selectedIntervals[i].dataset.date), '<', new Date(startIntervalDate))) {
                startIntervalDate = selectedIntervals[i].dataset.date;
                startIntervalDateFormatted = selectedIntervals[i].dataset.dateformatted;
            }
            if (!endIntervalDate || self.datecompare(new Date(selectedIntervals[i].dataset.date), '>', new Date(endIntervalDate))) {
                endIntervalDate = selectedIntervals[i].dataset.date;
                if(startIntervalDate !== endIntervalDate) {
                    endIntervalDateFormatted = selectedIntervals[i].dataset.dateformatted;
                }
            }
        }

        if (self.datecompare(new Date(selectedItem.dataset.date), '>', new Date(startIntervalDate)) &&
            self.datecompare(new Date(selectedItem.dataset.date), '<', new Date(endIntervalDate))) {
            $A.util.addClass(selectedItem, 'selectedTime');
            endIntervalDate = selectedItem.dataset.date;
            endIntervalDateFormatted = selectedItem.dataset.dateformatted;
        }

        var allIntervals = component.find("timeInterval");
        for (var i = 0; i < allIntervals.length; i++) {
            if (self.datecompare(new Date(allIntervals[i].getElement().dataset.date), '>=', new Date(startIntervalDate)) &&
                self.datecompare(new Date(allIntervals[i].getElement().dataset.date), '<=', new Date(endIntervalDate))) {
                if (!$A.util.hasClass(allIntervals[i].getElement(), "timeNotAvailable")) {
                    $A.util.addClass(allIntervals[i].getElement(), 'selectedTime');
                } else {
                    endIntervalDate  = startIntervalDate;
                    endIntervalDateFormatted = '';
                }
            } else {
                $A.util.removeClass(allIntervals[i].getElement(), 'selectedTime');
            }
        }

            var strTimeInterval;
            if(startIntervalDateFormatted != undefined){
                strTimeInterval = startIntervalDateFormatted;
                if (endIntervalDateFormatted != undefined) {
                    strTimeInterval += ' - ' + endIntervalDateFormatted;
                }
            } else {
                strTimeInterval = '';
            }
            component.set('v.strDaysInterval', strTimeInterval);

            var choosenTimeInterval = new Object();
            choosenTimeInterval.startInterval = startIntervalDateFormatted;
            choosenTimeInterval.endInterval = endIntervalDateFormatted;
            choosenTimeInterval.startIntervalDate = new Date(startIntervalDate);
            choosenTimeInterval.endIntervalDate = new Date(endIntervalDate);
            component.set("v.choosenTimeInterval", choosenTimeInterval);
        },

    timeIntervalChoosen: function(component) {
        var strTimeInterval;
        var timezone = 'Etc/UTC';
        strTimeInterval = moment(new Date(component.get("v.startBookingDateTime")).toLocaleString('en-US', { timeZone: timezone })).format("HH:mm") + ' - ' + moment(new Date(component.get("v.endBookingDateTime")).toLocaleString('en-US', { timeZone: timezone })).format("HH:mm");
        component.set('v.strTimeInterval', strTimeInterval);
        var choosenTimeInterval = new Object();
        choosenTimeInterval.startIntervalDate = component.get("v.startBookingDateTime");
        choosenTimeInterval.endIntervalDate = component.get("v.endBookingDateTime");
        component.set("v.choosenTimeInterval", choosenTimeInterval);
    },

    calculateEndTimeSlots: function(component) {
        var timezone = 'Etc/UTC';
        var startBookingDateTime = new Date(component.get("v.startBookingDateTime"));
        var currentDay = parseInt(startBookingDateTime.toLocaleString('en-US', { timeZone: timezone, day:'2-digit'}));
        var startTime = startBookingDateTime.getHours();

        var availableStartTimeList = component.get("v.bookingTimes");
        var endOfAvailableTime = availableStartTimeList[availableStartTimeList.length - 1].endTime;
        endOfAvailableTime = parseInt(endOfAvailableTime.substr(0, endOfAvailableTime.indexOf(":")));
        if (endOfAvailableTime === 0) {
            endOfAvailableTime = 24;
        }
        var endTimeSlots = [];

        var timeIntervalValue = component.get("v.bookingHours");
        var endBookingDateTime = new Date(startBookingDateTime.setHours(startTime + timeIntervalValue));
        var t = timeIntervalValue;
        do {
            var timeInterval = new Object();
            var options = { timeZone: timezone, hour12: false, hour: '2-digit', minute:'2-digit'};

            timeInterval.endTime = endBookingDateTime.toLocaleString('en-US', options);
            timeInterval.interval = timeInterval.endTime;
            timeInterval.isAvailable = true;
            timeInterval.isDisplay = true;
            if (endOfAvailableTime >= parseInt(timeInterval.endTime.substr(0, timeInterval.endTime.indexOf(":")))
                &&
                availableStartTimeList.filter(item => item.endTime === timeInterval.endTime && item.isAvailable).length > 0
            ) {
                endTimeSlots.push(timeInterval);
            } else if (availableStartTimeList.filter(item => item.endTime === timeInterval.endTime && item.isAvailable).length === 0) {
                break;
            }
            t += timeIntervalValue;
            endBookingDateTime = new Date(endBookingDateTime.setHours(startTime + t));
        } while (t < 24 && parseInt(endBookingDateTime.toLocaleString('en-US', { timeZone: timezone, day:'2-digit'})) ===  currentDay);
        component.set("v.bookingEndTimes", endTimeSlots);
    },

    bookingAvailable: function(component, event, helper) {
        var timezone = 'Etc/UTC';
        var result = true;

        var events = component.get("v.events");
        var self = this;
        var startBookingDateTime = new Date(component.get("v.choosenTimeInterval").startIntervalDate);
        var endBookingDateTime = new Date(component.get("v.choosenTimeInterval").endIntervalDate);
        var sartTimeBooking = parseInt(startBookingDateTime.toLocaleString('en-US', { timeZone: timezone, hour:'2-digit', hour12:false}));
        var endTimeBooking = parseInt(endBookingDateTime.toLocaleString('en-US', { timeZone: timezone, hour:'2-digit', hour12:false}));

        for (var i = 0; i < events.length; i++) {
            var startDate = new Date(events[i].Booking_Start__c);
            var endDate = new Date(events[i].Booking_End__c);
            var startTimeEvent = parseInt(startDate.toLocaleString('en-US', { timeZone: $A.get("$Locale.timezone"), hour:'2-digit', hour12:false}));
            var endTimeEvent = parseInt(endDate.toLocaleString('en-US', { timeZone: $A.get("$Locale.timezone"), hour:'2-digit', hour12:false}));
            if (self.datecompare(new Date(startDate), '<=' , endBookingDateTime) && self.datecompare(startBookingDateTime, '<=' , new Date(endDate))) {
                /*if (moment(startDate).isBefore(moment(endBookingDateTime)) && moment(startBookingDateTime).isBefore(moment(endDate))) {
                    result = false;
                }*/
                if (startTimeEvent < endTimeBooking && sartTimeBooking < endTimeEvent) {
                    result = false;
                }
            }
        }
        return result;
    },

    updateClasses : function(component, event, className) {
        var selectedItem = event.currentTarget;
        var selectedIntervals = document.getElementsByClassName(className);
        for (var i = 0; i < selectedIntervals.length; i++) {
            if (selectedIntervals[i].dataset.start !== selectedItem.dataset.start) {
                $A.util.toggleClass(selectedIntervals[i], className);
            }
        }
        $A.util.toggleClass(selectedItem, className);
    },

    datecompare : function(date1, sign, date2) {
        var day1 = date1.getDate();
        var mon1 = date1.getMonth();
        var year1 = date1.getFullYear();
        var day2 = date2.getDate();
        var mon2 = date2.getMonth();
        var year2 = date2.getFullYear();
        if (sign === '===') {
            if (day1 === day2 && mon1 === mon2 && year1 === year2) return true;
            else return false;
        } else if (sign === '>=') {
            if (year1 > year2) return true;
            else if (year1 === year2 && mon1 > mon2) return true;
            else if (year1 === year2 && mon1 === mon2 && day1 >= day2) return true;
            else return false;
        } else if (sign === '<=') {
            if (year1 < year2) return true;
            else if (year1 === year2 && mon1 < mon2) return true;
            else if (year1 === year2 && mon1 === mon2 && day1 <= day2) return true;
            else return false;
        } else if (sign === '>') {
            if (year1 > year2) return true;
            else if (year1 === year2 && mon1 > mon2) return true;
            else if (year1 === year2 && mon1 === mon2 && day1 > day2) return true;
            else return false;
        } else if (sign === '<') {
            if (year1 < year2) return true;
            else if (year1 === year2 && mon1 < mon2) return true;
            else if (year1 === year2 && mon1 === mon2 && day1 < day2) return true;
            else return false;
        }
    },

})