({
    fetchDefaultValue: function(component, oppId) {
        var relatedContact = component.get("c.getContact");
        relatedContact.setParams({ "opportunity": oppId });
        relatedContact.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues.Primary_Contact__c) {
                    component.set("v.furniturePicklistChange", allValues.Primary_Contact__r.Furniture__c);
                    component.set("v.bedroomsPicklistChange", allValues.Primary_Contact__r.Bedrooms__c);
                    component.set("v.minPricePicklistChange", allValues.Primary_Contact__r.Min_Price_Range__c);
                    component.set("v.maxPricePicklistChange", allValues.Primary_Contact__r.Max_Price_Range__c);
                    component.set("v.myMoveInDate", allValues.Primary_Contact__r.Move_In_Date__c);
                }
            }
        });
        $A.enqueueAction(relatedContact);
    },

    fetchPickListVal: function(component, fieldName, elementId) {
        var action = component.get("c.getPickListValuesIntoList");
        action.setParams({
            "ObjectApiName": 'Contact',
            "FieldApiName": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues != undefined && allValues.length > 0) {
                    opts.push("--None--");
                }
                for (var i = 0; i < allValues.length; i++) {
                    var selectPickList = false;
                    opts.push(allValues[i]);
                }
                component.set("v." + elementId, opts);
            }
        });
        $A.enqueueAction(action);
    },

    fetchPickListBedrooms: function(component, fieldName, elementId) {
        var action = component.get("c.getPickListValuesIntoList");
        action.setParams({
            "ObjectApiName": 'Contact',
            "FieldApiName": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                for (var i = 0; i < allValues.length; i++) {
                    var selectPickList = false;
                    opts.push({
                        'label': allValues[i],
                        'value': allValues[i],
                        'selected': allValues[i] == component.get("v.bedroomsPicklistChange") ? true : false
                    });
                }
                component.set("v." + elementId, opts);
                var labels = [];
                opts.forEach(function(element) {
                    if (element.selected) {
                        labels.push(element.label);
                    }
                });
                this.setInfoText(component,labels);
            }
        });
        $A.enqueueAction(action);
    },

    fetchPickListBuildings: function(component, fieldName, elementId) {
        var action = component.get("c.getBuildingsList");
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                for (var i = 0; i < allValues.length; i++) {
                    var selectPickList = false;
                    opts.push({
                        'label': allValues[i].Name,
                        'value': allValues[i].Id,
                        'selected': allValues[i].Id == component.get("v.buildingsPicklistChange") ? true : false
                    });
                }
                component.set("v." + elementId, opts);
                var labels = [];
                opts.forEach(function(element) {
                    if (element.selected) {
                        labels.push(element.label);
                    }
                });
                this.setInfoText(component,labels);
            }
        });
        $A.enqueueAction(action);
    },

    helperFun : function(component,event,secId) {
        var acc = component.find(secId);
        for(var cmp in acc) {
            $A.util.toggleClass(acc[cmp], 'slds-show');
            $A.util.toggleClass(acc[cmp], 'slds-hide');
        }
    },

    saveNewViewing: function(component) {
        var action = component.get('c.saveNewViewing');
        action.setParams({
            "opportunityId": component.get("v.recordId"),
            "requestedStartDate": component.get("v.myDateTime"),
            "units": component.get("v.unitsAdded"),
            "attendeeInfo": component.get("v.myText"),
            "tipiStaffId": component.get("v.userId")
        });
        $A.enqueueAction(action);
        $A.get("e.force:closeQuickAction").fire()
    },

    validateFields : function(component, event, fieldName) {
        var inputCmp = component.find(fieldName);
        var value = inputCmp.get("v.value");

        if (value == undefined || value == null) {
            inputCmp.set("v.errors", [{message: "This field is required"}]);
        } else {
            inputCmp.set("v.errors", null);
        }

        var lookupCmp = component.find("usersLookup");
        var lookupInput = lookupCmp.find("lookup");
        if (!component.get("v.userId")) {
            lookupInput.set("v.errors", [{message: "This field is required"}]);
        }
    },

    handleSelectChangeEvent: function(component, event, helper) {
        var items = component.get("v.items");
        items = event.getParam("values");
        component.set("v.items", items);
    },

    setInfoText: function(component, labels) {
        if (labels.length == 0) {
            component.set("v.infoText", "Select an option...");
        }
        if (labels.length == 1) {
            component.set("v.infoText", labels[0]);
        }
        else if (labels.length > 1) {
            component.set("v.infoText", labels.length + " options selected");
        }
    },
})