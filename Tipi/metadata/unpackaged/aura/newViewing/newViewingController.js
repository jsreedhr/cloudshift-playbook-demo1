({
    doInit: function(component, event, helper) {
        var oppId = component.get("v.recordId");
        helper.fetchDefaultValue(component, oppId);
        helper.fetchPickListVal(component, 'Furniture__c', 'pickListFurniture');
        helper.fetchPickListBedrooms(component, 'Bedrooms__c', 'pickListBedrooms');
        helper.fetchPickListBuildings(component, 'Building__c', 'pickListBuildings');
        helper.fetchPickListVal(component, 'Min_Price_Range__c', 'pickListMinPrice');
        helper.fetchPickListVal(component, 'Max_Price_Range__c', 'pickListMaxPrice');
    },

    furniturePicklistChange: function(component, event, helper) {
        var selected = component.find("Furniture");
        component.set("v.furniturePicklistChange", selected.get("v.value"));
    },

    bedroomsPicklistChange: function(component, event, helper) {
        var selected = component.find("Bedrooms");
        component.set("v.bedroomsPicklistChange", selected.get("v.value"));
    },

    minPricePicklistChange: function(component, event, helper) {
        var selected = component.find("MinPrice");
        component.set("v.minPricePicklistChange", selected.get("v.value"));
    },

    maxPricePicklistChange: function(component, event, helper) {
        var selected = component.find("MaxPrice");
        component.set("v.maxPricePicklistChange", selected.get("v.value"));
    },

    myMoveInDate: function(component, event, helper) {
        var selected = component.find("MoveInDate");
        component.set("v.myMoveInDate", selected.get("v.value"));
    },

    sectionOne : function(component, event, helper) {
        helper.helperFun(component,event,'articleOne');
    },

    saveViewing : function(component, event, helper) {
        var myDateTime = component.get("v.myDateTime");
        var myText = component.get("v.myText");
        var opportunity = component.get("v.opportunity");

        helper.validateFields(component, event, "MyDate");
        //helper.validateFields(component, event, "accLookup");
        helper.validateFields(component, event, "info");

        var MyDate = component.find("MyDate");
        var accLookup = component.find("accLookup");
        var info = component.find("info");
        if (MyDate.get("v.errors") != null ||
            //accLookup.get("v.errors") != null ||
            component.get("v.userId") == null ||
            info.get("v.errors") != null) {
            component.set("v.hasErrors", true);
        } else {
            helper.saveNewViewing(component);
        }
    },

    getUserId: function(cmp, event, helper) {
        cmp.set("v.userId", event.getParam("recordId"));
        cmp.set("v.userLabel", event.getParam("recordLabel"));
    },

    clearUserLookup: function(cmp, event, helper) {
         cmp.set("v.userId", null);
         cmp.set("v.userLabel", null);
    },
})