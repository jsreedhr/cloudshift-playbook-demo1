({
    // to initialise the component
    initialiseHelper : function (cmp, event, helper) {
        helper.callServer(
            cmp,
            'c.initialiseComponent',
            function (response) {
                cmp.set('v.contactsList', JSON.parse(JSON.stringify(response)));
            },
            {
                recordId: cmp.get('v.recordId')
            },
            false
        );
    }
});