({
    init : function(component, event) {
        var action = component.get("c.payDeposit");
        action.setParams({
            contactId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var spinner = component.find("mySpinner");
            $A.util.toggleClass(spinner, "slds-hide");
             if (state === "SUCCESS") {
                var result = JSON.parse(response.getReturnValue());
                 if(result.length > 0) {
                    component.set("v.contactmail", result[0].Email);
                    component.set("v.success", true);
                 } else {
                    component.set("v.noData", true);
                 }
             } else if (state === "ERROR") {
                 component.set("v.err", true);
            }
            component.set("v.finished", true);
        });
        $A.enqueueAction(action);
    }
})