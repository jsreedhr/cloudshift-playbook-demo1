({
	doInit : function(component, event, helper) {
		helper.init(component, event);
	},
	closeAction: function(component, event, helper) {
    	$A.get("e.force:closeQuickAction").fire();
	}
})