({
	init : function(component, event) {
		var action = component.get("c.directDebitAuthorization");
		action.setParams({ contactId : component.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var spinner = component.find("mySpinner");
        	$A.util.toggleClass(spinner, "slds-hide");
           	if (state === "SUCCESS") {
             	component.set("v.success", true);
             } 
            else if (state === "ERROR") {
            	component.set("v.err", true);
            }
        });
		$A.enqueueAction(action);	
	}
})