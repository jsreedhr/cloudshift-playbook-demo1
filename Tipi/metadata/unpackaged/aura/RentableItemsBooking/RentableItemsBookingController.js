({
   doInit : function (component, event, helper) {
        helper.getCurrentContactRecord(component);
        if (component.get("v.itemType") == 'Social_Space__c') {
            helper.getSocialSpaceRecords(component);
        } else if (component.get("v.itemType") == 'Rentable_Item__c') {
            helper.getRentableItemRecords(component);
        }
    },

    clickBooking : function (component, event, helper) {
        component.set("v.selectedRecord", event.currentTarget.getAttribute('data-index'));
        component.set("v.buildingName", event.currentTarget.getAttribute('data-building'));
        if (!component.get("v.buildingName")) {
            component.set("v.buildingName", event.currentTarget.getAttribute('data-buildingnameparking'));
        }
        component.set("v.socialSpaceName", event.currentTarget.getAttribute('data-itemname'));
        component.set("v.selectedSocialSpaceStart", event.currentTarget.getAttribute('data-availabilitystart'));
        component.set("v.selectedSocialSpaceEnd", event.currentTarget.getAttribute('data-availabilityend'));
        component.set("v.showCalendar", true);
    },

    backToBooking: function(component, event, helper) {
        component.set("v.showCalendar", false);
        component.find("calendar").destroy();
    },

    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'show');
    },

    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, 'show');
    },

})