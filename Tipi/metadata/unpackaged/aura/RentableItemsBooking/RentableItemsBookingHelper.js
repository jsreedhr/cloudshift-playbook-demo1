({
    getCurrentContactRecord : function(component) {
        var action = component.get("c.getCurrentContact");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.currentContact", response.getReturnValue());
                component.set("v.currentContactSerialized", JSON.stringify(response.getReturnValue()));
            }
        });
        $A.enqueueAction(action);
    },

    getSocialSpaceRecords : function(component) {
        var actionSocial = component.get("c.getSocialSpacesForContact");
        actionSocial.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.socialSpaces", response.getReturnValue());
                if (response.getReturnValue().length > 0) {
                    component.set("v.developmentName", response.getReturnValue()[0].Building__r.Development_Site__r.Name);
                }
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(actionSocial);
    },

    getRentableItemRecords : function(component) {
        var action = component.get("c.getRentableItemsForContact");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.rentableItems", response.getReturnValue());
                if (response.getReturnValue().length > 0) {
                    component.set("v.developmentName", response.getReturnValue()[0].Building__r.Development_Site__r.Name);
                }

                if (response.getReturnValue()) {
                    var buildingsSet = new Set();
                    var buildings = [];
                    for (var i = 0; i < response.getReturnValue().length; i++) {
                        if(!buildingsSet.has(response.getReturnValue()[i].Building__c)) {
                            var building = new Object();
                            building.Id = response.getReturnValue()[i].Building__c;
                            building.Name = response.getReturnValue()[i].Building__r.Name;
                            building.Development_Site__c = response.getReturnValue()[i].Building__r.Development_Site__c;
                            building.Guest_Parking_Cost__c = response.getReturnValue()[i].Building__r.Development_Site__r.Guest_Parking_Cost__c;
                            if (response.getReturnValue()[i].Building__r.Guest_Parking_Image__c) {
                                building.Image__c = response.getReturnValue()[i].Building__r.Guest_Parking_Image__c;
                            }
                            buildings.push(building);
                        }
                        buildingsSet.add(response.getReturnValue()[i].Building__c);
                    }
                    component.set("v.rentableItemsBuildings", buildings);
                    console.log(component.get("v.rentableItemsBuildings"));
                }
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
})