({
    /*
     *  Map the Field to the desired component config, including specific attribute values
     *  Source: https://www.salesforce.com/us/developer/docs/apexcode/index_Left.htm#CSHID=apex_class_Schema_FieldSetMember.htm|StartTopic=Content%2Fapex_class_Schema_FieldSetMember.htm|SkinName=webhelp
     *
     *  Change the componentDef and attributes as needed for other components
     */
    configMap: {
        'anytype': { componentDef: 'ui:inputText', attributes: {} },
        'base64': { componentDef: 'ui:inputText', attributes: {} },
        'boolean': {componentDef: 'ui:inputCheckbox', attributes: {} },
        'combobox': { componentDef: 'ui:inputText', attributes: {} },
        'currency': { componentDef: 'ui:inputText', attributes: {} },
        'datacategorygroupreference': { componentDef: 'ui:inputText', attributes: {} },
        'date': {
            componentDef: 'ui:inputDate',
            attributes: {
                displayDatePicker: true,
                format: 'MM/dd/yyyy'
            }
        },
        'datetime': { componentDef: 'ui:inputDateTime', attributes: {} },
        'double': { componentDef: 'ui:inputNumber', attributes: {} },
        'email': { componentDef: 'ui:inputEmail', attributes: {} },
        'encryptedstring': { componentDef: 'ui:inputText', attributes: {} },
        'id': { componentDef: 'ui:inputText', attributes: {} },
        'integer': { componentDef: 'ui:inputNumber', attributes: {} },
        'multipicklist': { componentDef: 'ui:inputText', attributes: {} },
        'percent': { componentDef: 'ui:inputNumber', attributes: {} },
        'phone': { componentDef: 'ui:inputPhone', attributes: {} },
        'picklist': { componentDef: 'ui:inputText', attributes: {} },
        'reference': { componentDef: 'ui:inputText', attributes: {} },
        'string': { componentDef: 'ui:inputText', attributes: {} },
        'textarea': { componentDef: 'ui:inputText', attributes: {} },
        'richtext': { componentDef: 'aura:unescapedHtml', attributes: {} },
        'time': { componentDef: 'ui:inputDateTime', attributes: {} },
        'url': { componentDef: 'ui:inputText', attributes: {} }
    },

    createForm: function(cmp) {
        var fields = cmp.get('v.fields');
        var record = cmp.get('v.record');
        var inputDesc = [];

        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            var type = field.Type.toLowerCase();
            var configTemplate = this.configMap[type];

            if (!configTemplate) {
                console.log(`type ${ type } not supported`);
            	continue;
            }

            var config = JSON.parse(JSON.stringify(configTemplate));
            var value = '' + cmp.getReference('v.record.' + field.APIName);

            config.attributes.label = field.Label;
            config.attributes.value = cmp.getReference(' v.record.' + field.APIName);
            config.attributes.fieldPath = field.APIName;

            if (!config.attributes['class']) {
            	config.attributes['class'] = '';
            }

            if (config.attributes.fieldPath !== 'Unit_Image__c') {
                inputDesc.push([
                    config.componentDef,
                    config.attributes
                ]);
            } else {
                cmp.set("v.unitImage", config.attributes.value);
            }
        }


        $A.createComponents(inputDesc, function(cmps) {
            for (let i = 0; i < cmps.length; i++) {
                $A.util.addClass(cmps[i], 'disabled slds-col slds-size_1-of-2');
            }
            cmp.set('v.body', cmps);
        });
    },

    deserializeContact : function(cmp) {
        var contact = JSON.parse(cmp.get("v.serializedContact"));
        cmp.set("v.contact", contact);
        console.log(cmp.get("v.contact"));
    },
})