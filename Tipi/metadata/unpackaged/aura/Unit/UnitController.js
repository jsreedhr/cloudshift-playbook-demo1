({
    init: function(cmp, event, helper) {
        helper.deserializeContact(cmp);
        var fieldSetName = cmp.get('v.fieldSetName');
        var sobjectName = cmp.get('v.sObjectName');
        var recordId = cmp.get('v.recordId');

        if (!fieldSetName) {
            console.log('The field set is required.');
            return;
        }

        var getFormAction = cmp.get('c.getForm');

        getFormAction.setParams({
            fieldSetName: fieldSetName,
            objectName: sobjectName,
            recordId: recordId
        });

        getFormAction.setCallback(this,
            function(response) {
                var state = response.getState();

                if (cmp.isValid() && state === "SUCCESS") {
                    var form = response.getReturnValue();
                    cmp.set('v.fields', form.Fields);
                    cmp.set('v.record', form.Record);
                    helper.createForm(cmp);
                }
            }
        );
        $A.enqueueAction(getFormAction);
    },

})