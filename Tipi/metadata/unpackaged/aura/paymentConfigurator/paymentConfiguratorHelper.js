({
    // to initialise the component
    initialiseHelper : function (cmp, event, helper) {
        helper.validatePermissions(cmp, 'Financial_Configuration_Permission');
        helper.maximiseQuickActionModal(cmp);

        var action = cmp.get('c.initialiseComponent');
        action.setParams({
            'recordId' : cmp.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            if(cmp.isValid() && response.getState() === 'SUCCESS') {
                cmp.set('v.paymentData', JSON.parse(JSON.stringify(response.getReturnValue())));
                console.log('pat initialised paymentData: ' + JSON.stringify(cmp.get('v.paymentData')));
                if(response.getReturnValue().errorMessage === '') {
                    helper.calcAllValueChangeHelper(cmp, helper);
                    cmp.set('v.initialisingDone', true);
                }
            }
        });
        $A.enqueueAction(action);
    },

    // to handle the operation on click of save
    saveClickHelper : function (cmp, event, helper) {
        const paymentData = cmp.get('v.paymentData');
        const isValid = helper.validateForm(cmp, helper, paymentData);

        if(isValid) {
            cmp.set('v.isLoading', true);

            // invoking apex controller method to save a validated payment configuration
            var action = cmp.get('c.savePaymentConfiguration');
            action.setParams({
                'paymentDataStr' : JSON.stringify(paymentData),
                'recordId' : cmp.get('v.recordId')
            });
            action.setCallback(this, function(response) {
                if(cmp.isValid() && response.getState() === 'SUCCESS') {
                    helper.saveClickHelperCallback(cmp, helper, response.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        }
    },

    saveClickHelperCallback : function (cmp, helper, response) {
        cmp.set('v.isLoading', false);
        $A.get('e.force:closeQuickAction').fire();
        $A.get('e.force:refreshView').fire();

        const responseObj = JSON.parse(JSON.stringify(response));
        let toastEvent = $A.get('e.force:showToast');
        if(responseObj.hasOwnProperty('isSavedSuccessfully')) {
            if(responseObj['isSavedSuccessfully']) {
                toastEvent.setParams({ 'type': 'success', 'title': 'Success!', 'message': 'The payment configuration has been saved successfully.' });
            } else {
                toastEvent.setParams({ 'type': 'error', 'title': 'Error!', 'message': 'The payment configuration could not be saved. Error: ' + responseObj['errorMessage'] });
            }
        } else {
            toastEvent.setParams({ 'type': 'error', 'title': 'Error!', 'message': 'The payment configuration could not be saved. Error: ' + responseObj['errorMessage'] });
        }
        toastEvent.fire();
    },

    // to validate a form upon click of save
    validateForm : function (cmp, helper, paymentData) {
        cmp.set('v.validationErrorMsg', '');
        let isValid = false;

        if(Array.isArray(paymentData['tenants'])) {
            const rentIsValid               = helper.columnIsValid(cmp, helper, paymentData, 'rent', 'Rent');
            const storageIsValid            = helper.multipicklistIsValid(cmp, helper, 'storageString', 'Storage', true);
            const parkingIsValid            = helper.multipicklistIsValid(cmp, helper, 'parkingString', 'Parking', true);
            const depositPaymentIsValid     = helper.columnIsValid(cmp, helper, paymentData, 'depositPayment', 'Deposit Payment');
            const upfrontPaymentTypeIsValid = helper.paymentTypeIsValid(cmp, helper, paymentData, 'upfrontPaymentType', 'totalInitialPayment');

            if(rentIsValid && storageIsValid && parkingIsValid && depositPaymentIsValid && upfrontPaymentTypeIsValid) {
                isValid = true;
            }
        }

        console.log('Save click -> is Valid: ' + isValid);
        return isValid;
    },

    // used for validating the Rent, Deposit, and Up Front Payment columns
    columnIsValid : function (cmp, helper, paymentData, columnName, columnLabel) {
        let isValid = false;
        const columnSum = helper.getSumForColumn(cmp, helper, paymentData, columnName);
        const columnVal = parseFloat(paymentData[columnName]);

        if(columnSum === columnVal && columnVal >= 0) {
            isValid = true
        } else {
            const errorMsg = '<br>' + $A.get('$Label.c.ERROR_PAYMENT_CONFIGURATOR_COLUMN_SUM')
                                + ' (expected ' + columnLabel + ': ' + columnVal
                                + ', actual ' + columnLabel + ': ' + columnSum + ').<br>';
            cmp.set('v.validationErrorMsg', cmp.get('v.validationErrorMsg') + errorMsg );
        }

        return isValid;
    },

    // to calculate the sum for a specified column
    getSumForColumn : function (cmp, helper, paymentData, columnName) {
        let sum = 0;
        if(Array.isArray(paymentData['tenants'])) {
            for(let i = 0; i < paymentData['tenants'].length; i++) {
                if(paymentData['tenants'][i]['isPayee']) {
                    let value = parseFloat(paymentData['tenants'][i][columnName]);

                    // if more than 2 decimal places
                    if((value + '').includes('.')) {
                        if((value + '').split('.')[1].length > 2) {
                            return false;
                        }
                    }

                    if(value >= 0) {
                        // if positive value
                        sum += value;
                    } else {
                        // if value < 0 or undefined
                        return false;
                    }
                }
            }
        }
        return sum;
    },

    // to validate the payment type column
    paymentTypeIsValid : function (cmp, helper, paymentData, paymentTypeColumn, valueColumn) {
        let isValid = true;
        if(Array.isArray(paymentData['tenants'])) {
            const tenantCount = paymentData['tenants'].length;

            for(let i = 0; i <  tenantCount; i++) {
                let tenant = paymentData['tenants'][i];

                let picklistField = cmp.find(paymentTypeColumn);
                if (tenantCount > 1) {
                    picklistField = cmp.find(paymentTypeColumn)[i];
                }

                if(tenant['isPayee'] && tenant[valueColumn] > 0 && tenant[paymentTypeColumn] === '') {
                    picklistField.set('v.errors', [{ message:'Payment Type Required' }]);
                    isValid = false;
                } else  {
                    picklistField.set('v.errors', null);
                 }
            }
        }
        return isValid;
    },

    // to validate the storage / parking columns
    multipicklistIsValid : function (cmp, helper, multipicklistColumn, columnLabel, showError) {
        let isValid = true;
        const paymentData = cmp.get('v.paymentData');
        if(Array.isArray(paymentData['tenants'])) {

            let selectedIds = [];
            let duplicateIds = [];
            for(let i = 0; i <  paymentData['tenants'].length; i++) {
                let tenant = paymentData['tenants'][i];
                if(tenant['isPayee'] && tenant[multipicklistColumn] !== '') {

                    if(tenant[multipicklistColumn].includes(';')) {
                        //multiple values selected
                        let tenantIds = tenant[multipicklistColumn].split(';');
                        for(let j = 0; j <  tenantIds.length; j++) {
                            if(!selectedIds.includes(tenantIds[j])) {
                                selectedIds.push(tenantIds[j]);
                            } else {
                                duplicateIds.push(tenantIds[j]);
                                isValid = false;
                            }
                        }
                    } else if (tenant[multipicklistColumn].length >= 15 && tenant[multipicklistColumn].length <=18 ) {
                        //single value selected
                        if(!selectedIds.includes(tenant[multipicklistColumn])) {
                            selectedIds.push(tenant[multipicklistColumn]);
                        } else {
                            duplicateIds.push(tenant[multipicklistColumn]);
                            isValid = false;
                        }
                    }
                }
            }

            // compare expected column count with actual count and return validity
            let expectedCount = 0;
            if(multipicklistColumn == 'storageString') {
                expectedCount = paymentData.storageCount;
            } else if(multipicklistColumn = 'parkingString') {
                expectedCount = paymentData.parking;
            }
            if(selectedIds.length !== expectedCount) {
                isValid = false;
            }

            // set column sum
            const actualCount = selectedIds.length + duplicateIds.length;
            cmp.find(multipicklistColumn).set('v.value', actualCount);

            // show error message
            if(showError) {
                if(!isValid) {
                    const errorMsg = '<br>' + $A.get('$Label.c.ERROR_PAYMENT_CONFIGURATOR_COLUMN_SUM')
                                        + ' (expected ' + columnLabel + ': ' + expectedCount
                                        + ', actual ' + columnLabel + ': ' + actualCount + ').<br>';
                    cmp.set('v.validationErrorMsg', cmp.get('v.validationErrorMsg') + errorMsg );
                }
            }
        }
        return isValid;
    },

    // to calculate & visualise value change for a single column
    calcSingleValueChangeHelper : function (cmp, helper, columnChanged) {
        const paymentData = cmp.get('v.paymentData');
        let columnSum = helper.getSumForColumn(cmp, helper, paymentData, columnChanged);
        columnSum = (columnSum !== false && columnSum >= 0) ? columnSum : '';
        cmp.find(columnChanged).set('v.value', columnSum);
        helper.recalcFirstInitialPayments(cmp, helper);
    },

    // to calculate & visualise value change for a all 5 number columns
    calcAllValueChangeHelper : function (cmp, helper) {
        helper.calcSingleValueChangeHelper(cmp, helper, 'rent');
        helper.multipicklistIsValid(cmp, helper, 'storageString', 'Storage', false);
        helper.multipicklistIsValid(cmp, helper, 'parkingString', 'Parking', false);
        helper.calcSingleValueChangeHelper(cmp, helper, 'depositPayment');
    },

    // to calculate & visualise value change for a single column
    recalcFirstInitialPayments : function (cmp, helper) {
        let paymentData = cmp.get('v.paymentData');

        if(Array.isArray(paymentData['tenants'])) {
            for(let i = 0; i < paymentData['tenants'].length; i++) {
                let rentValue = parseFloat(paymentData['tenants'][i]['rent']);
                let depositValue = parseFloat(paymentData['tenants'][i]['depositPayment']);
                if(!(rentValue > 0)) rentValue = 0;
                if(!(depositValue > 0)) depositValue = 0;

                // calc payment share
                let paymentShare = 0;
                if(parseFloat(paymentData['rent']) > 0) {
                    paymentShare = rentValue / parseFloat(paymentData['rent']);
                }
                paymentData['tenants'][i]['paymentShare'] = paymentShare;

                // calc rent up front
                let rentUpfront = parseFloat(paymentData['tenants'][i]['monthsUpfront']) * rentValue;
                paymentData['tenants'][i]['rentUpfront'] = rentUpfront;

                // calc first rent payment
                let firstRentPayment = paymentShare * parseFloat(paymentData['firstRentPayment']);
                if(!(firstRentPayment > 0)) {
                    firstRentPayment = 0;
                } else if((firstRentPayment + '').includes('.')) {
                    firstRentPayment = (Math.round( firstRentPayment * 100 ) / 100).toFixed(2);
                }
                paymentData['tenants'][i]['firstRentPayment'] = firstRentPayment;

                // calc initial Payment
                let totalInitialPayment = depositValue + rentUpfront;
                if(!rentUpfront) {
                    totalInitialPayment = depositValue + parseFloat(firstRentPayment) - parseFloat(paymentData['tenants'][i]['reservationAmount']);
                }

                if((totalInitialPayment + '').includes('.')) {
                    totalInitialPayment = (Math.round( totalInitialPayment * 100 ) / 100).toFixed(2);
                }
                paymentData['tenants'][i]['totalInitialPayment'] = totalInitialPayment;
            }
        }

        cmp.set('v.paymentData', paymentData);
    }
});