({
    initialise : function (cmp, event, helper) {
        helper.initialiseHelper(cmp, event, helper);
    },

    cancelClick : function (cmp, event, helper) {
        $A.get('e.force:closeQuickAction').fire();
    },

    saveClick : function (cmp, event, helper) {
        helper.saveClickHelper(cmp, event, helper);
    },

    calcSingleValueChange : function (cmp, event, helper) {
        helper.calcSingleValueChangeHelper(cmp, helper, event.getSource().get('v.name'));
    },

    calcAllValueChange : function (cmp, event, helper) {
        helper.calcAllValueChangeHelper(cmp, helper);
    }
});