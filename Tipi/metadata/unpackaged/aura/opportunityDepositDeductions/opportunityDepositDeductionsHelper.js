({
    // to initialise the component
    initialiseHelper : function (cmp, event, helper) {
        let action = cmp.get('c.initialiseComponent');
        action.setParams({
            'recordId' : cmp.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            if(cmp.isValid() && response.getState() === 'SUCCESS') {
                helper.initialiseHelperResponse(cmp, event, helper, JSON.parse(JSON.stringify(response.getReturnValue())));
            }
        });
        $A.enqueueAction(action);
    },

    initialiseHelperResponse : function (cmp, event, helper, response) {
        cmp.set('v.paymentsList', response);

        if(Array.isArray(response)) {
            try {
                const depositPayees = response.length;
                if(depositPayees && depositPayees >= 1) {
                    cmp.set('v.depositPayees', depositPayees);
                    const refreshCharge = response[0].Opportunity__r.Total_Refresh_Charged_To_Customer__c;
                    cmp.set('v.totalRefreshCharge', refreshCharge);
                }
            } catch (err) {
                console.log('Could not parse response: ' + JSON.stringify(response) + '; error: ' + JSON.stringify(err));
            }
        }
    }
});