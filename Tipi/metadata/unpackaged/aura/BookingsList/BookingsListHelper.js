({
    getBookingsList: function(component) {
        var bookings = component.get("v.userBookings");
        component.set("v.maxPage", Math.floor((bookings.length + 9) / 10));
        this.renderPage(component);
    },

    renderPage: function(component) {
        var records = component.get("v.userBookings");
        var pageNumber = component.get("v.pageNumber");
        var pageRecords = records.slice((pageNumber - 1) * 10, pageNumber * 10);
        component.set("v.currentBookingsList", pageRecords);
    },

    getSortType: function(component, event, sortIcon) {
        var sortType = event.currentTarget.getAttribute('aria-sort');
        var arrowIcon = component.find(sortIcon);
        if (sortType === 'none' || sortType === 'descending') {
            sortType = 'ascending';
            event.currentTarget.setAttribute('aria-sort', 'ascending');
            arrowIcon.set("v.iconName", "utility:arrowup");
        } else if (sortType === 'ascending') {
            sortType = 'descending';
            event.currentTarget.setAttribute('aria-sort', 'descending');
            arrowIcon.set("v.iconName", "utility:arrowdown");
        }
        return sortType;
    },

    sort: function(field, asc_desc, array) {
        var bookings = array;
        for (var i = 0; i < bookings.length; i++) {
            for (var j = i + 1; j < bookings.length; j++) {
                if (field === 'type') {
                    if (asc_desc === 'ascending') {
                        if (bookings[i]['RecordType']['Name'] > bookings[j]['RecordType']['Name']) {
                            var swap = bookings[i];
                            bookings[i] = bookings[j];
                            bookings[j] = swap;
                        }
                    }
                    if (asc_desc === 'descending') {
                        if (bookings[i]['RecordType']['Name'] < bookings[j]['RecordType']['Name']) {
                            var swap = bookings[i];
                            bookings[i] = bookings[j];
                            bookings[j] = swap;
                        }
                    }
                }
                if (field === 'startDate') {
                    if (asc_desc === 'ascending') {
                        if (bookings[i]['Booking_Start__c'] && bookings[j]['Booking_Start__c'] &&
                            this.datecompare(new Date(bookings[i]['Booking_Start__c']), '>', new Date(bookings[j]['Booking_Start__c']))
                        ) {
                            var swap = bookings[i];
                            bookings[i] = bookings[j];
                            bookings[j] = swap;
                        } else if (bookings[i]['Start_Date__c'] && bookings[j]['Start_Date__c'] &&
                            this.datecompare(new Date(bookings[i]['Start_Date__c']), '>', new Date(bookings[j]['Start_Date__c']))
                        ) {
                            var swap = bookings[i];
                            bookings[i] = bookings[j];
                            bookings[j] = swap;
                        } else if (bookings[i]['Booking_Start__c'] && bookings[j]['Start_Date__c'] &&
                            this.datecompare(new Date(bookings[i]['Booking_Start__c']), '>', new Date(bookings[j]['Start_Date__c']))
                        ) {
                            var swap = bookings[i];
                            bookings[i] = bookings[j];
                            bookings[j] = swap;
                        } else if (bookings[i]['Start_Date__c'] && bookings[j]['Booking_Start__c'] &&
                            this.datecompare(new Date(bookings[i]['Start_Date__c']), '>', new Date(bookings[j]['Booking_Start__c']))
                        ) {
                            var swap = bookings[i];
                            bookings[i] = bookings[j];
                            bookings[j] = swap;
                        }
                    }
                    if (asc_desc === 'descending') {
                        if (bookings[i]['Booking_Start__c'] && bookings[j]['Booking_Start__c'] &&
                            this.datecompare(new Date(bookings[i]['Booking_Start__c']), '<', new Date(bookings[j]['Booking_Start__c']))
                        ) {
                            var swap = bookings[i];
                            bookings[i] = bookings[j];
                            bookings[j] = swap;
                        } else if (bookings[i]['Start_Date__c'] && bookings[j]['Start_Date__c'] &&
                            this.datecompare(new Date(bookings[i]['Start_Date__c']), '<', new Date(bookings[j]['Start_Date__c']))
                        ) {
                            var swap = bookings[i];
                            bookings[i] = bookings[j];
                            bookings[j] = swap;
                        } else if (bookings[i]['Booking_Start__c'] && bookings[j]['Start_Date__c'] &&
                            this.datecompare(new Date(bookings[i]['Booking_Start__c']), '<', new Date(bookings[j]['Start_Date__c']))
                        ) {
                            var swap = bookings[i];
                            bookings[i] = bookings[j];
                            bookings[j] = swap;
                        } else if (bookings[i]['Start_Date__c'] && bookings[j]['Booking_Start__c'] &&
                            this.datecompare(new Date(bookings[i]['Start_Date__c']), '<', new Date(bookings[j]['Booking_Start__c']))
                        ) {
                            var swap = bookings[i];
                            bookings[i] = bookings[j];
                            bookings[j] = swap;
                        }
                    }
                }
            }
        }
        return bookings;
    },

    datecompare : function(date1, sign, date2) {
        var day1 = date1.getDate();
        var mon1 = date1.getMonth();
        var year1 = date1.getFullYear();
        var day2 = date2.getDate();
        var mon2 = date2.getMonth();
        var year2 = date2.getFullYear();
        if (sign === '===') {
            if (day1 === day2 && mon1 === mon2 && year1 === year2) return true;
            else return false;
        } else if (sign === '>') {
            if (year1 > year2) return true;
            else if (year1 === year2 && mon1 > mon2) return true;
            else if (year1 === year2 && mon1 === mon2 && day1 > day2) return true;
            else return false;
        } else if (sign === '<') {
            if (year1 < year2) return true;
            else if (year1 === year2 && mon1 < mon2) return true;
            else if (year1 === year2 && mon1 === mon2 && day1 < day2) return true;
            else return false;
        }
    },
})