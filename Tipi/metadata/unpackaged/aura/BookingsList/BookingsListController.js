({
    doInit: function(component, event, helper) {
        helper.getBookingsList(component);
    },

    renderPage: function(component, event, helper) {
        helper.renderPage(component);
    },

    sortByType: function(component, event, helper) {
        var sortType = helper.getSortType(component, event, 'sortIconType');
        var bookings = component.get("v.currentBookingsList");
        bookings = helper.sort('type', sortType, bookings);
        var arrowIcon = component.find("sortIconStartDate");
        arrowIcon.set("v.iconName", "utility:sort");
        component.set("v.currentBookingsList", bookings);
    },

    sortByStartDate: function(component, event, helper) {
        var sortType = helper.getSortType(component, event, 'sortIconStartDate');
        var bookings = component.get("v.currentBookingsList");
        bookings = helper.sort('startDate', sortType, bookings);
        var arrowIcon = component.find("sortIconType");
        arrowIcon.set("v.iconName", "utility:sort");
        component.set("v.currentBookingsList", bookings);
    },
    
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'show');
    },

    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, 'show');
    },
})