({
    doInit : function(cmp, event, helper) {
        if (cmp.get("v.recordId")) {
            // helper.setDefaultRecord(cmp, helper);
            helper.handleSelection(cmp, null, cmp.get("v.recordId"), cmp.get("v.recordLabel"));
        }
    },
    search : function(cmp, event, helper) {
        if (!cmp.get("v.stopExecution")) {
            cmp.set("v.stopExecution", true);
            helper.doSearch(cmp);
        }
    },

    /**
     * Select an SObject from a list
     */
    select: function(cmp, event, helper) {
        console.log('SELECT');
        helper.handleSelection(cmp, event);
    },

    /**
     * Clear the currently selected SObject
     */
    clear: function(cmp, event, helper) {
        helper.clearSelection(cmp);
    },
    trimSpaces: function(cmp, event, helper) {
        var field       = event.getSource();
        var fieldValue  = field.get("v.value");

        if(!$A.util.isEmpty(fieldValue)) {
            field.set("v.value", fieldValue.trim());
        }
    },

    dayClick: function(date, jsEvent, view) {
        $A.getCallback(function() {
              console.log('CLICKED');
        });
    }
})