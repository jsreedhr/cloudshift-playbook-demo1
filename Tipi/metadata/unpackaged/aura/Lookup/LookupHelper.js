({
    /**
     * Perform the SObject search via an Apex Controller
     */
    doSearch : function(cmp) {
        // Get the search string, input element and the selection container
        var searchString    = cmp.get("v.searchString").trim().replace(/[?*]/g, "");
        var inputElement    = cmp.find('lookup');
        var lookupList      = cmp.find("lookuplist");
        var lookupListItems = cmp.find("lookuplist-items");
        var userType        = cmp.get("v.userType");
        var excludeIdsList  = cmp.get("v.excludeIdsList");

        // Clear any errors and destroy the old lookup items container
        inputElement.set('v.errors',    null);
        lookupListItems.set('v.body',   new Array());

        // We need at least 2 characters for an effective search
        if (typeof searchString === 'undefined' || searchString.length < 2)
        {
            // Hide the lookuplist
            $A.util.removeClass(lookupList, 'show');
            $A.util.addClass(lookupList, 'slds-hide');
            cmp.set("v.stopExecution", false);
            return;
        }

        // Show the lookuplist
        $A.util.removeClass(lookupList, 'slds-hide');
        $A.util.addClass(lookupList, 'show');

        // Get the API Name
        var sObjectAPIName = cmp.get('v.sObjectAPIName');

        // Create an Apex action
        var action = cmp.get("c.lookup");

        // Mark the action as abortable, this is to prevent multiple events from the keyup executing
        action.setAbortable();

        // Set the parameters
        action.setParams({ "searchString" : searchString, "sObjectAPIName" : sObjectAPIName, "userType" : userType, "excludeIds" : excludeIdsList});

        // Define the callback
        action.setCallback(this, function(response) {
            var state = response.getState();

            // Callback succeeded
            if (cmp.isValid() && state === "SUCCESS")
            {
                // Get the search matches
                var matches = response.getReturnValue();

                // If we have no matches, return
                if (matches.length === 0)
                {
                    cmp.set("v.stopExecution", false);
                    return;
                }
                // Render the results
                this.renderLookupComponents(cmp, lookupListItems, matches);
                cmp.set("v.stopExecution", false);
            }
            else if (state === "ERROR") // Handle any error by reporting it
            {
                var errors = response.getError();

                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        this.displayToast('Error', errors[0].message);
                    }
                }
                else
                {
                    this.displayToast('Error', 'Unknown error.');
                }
                cmp.set("v.stopExecution", false);
            }
        });

        // Enqueue the action
        $A.enqueueAction(action);
    },

    /**
     * Render the LookupCmp List Components
     */
    renderLookupComponents : function(cmp, lookupListItems, matches) {
        // list Icon SVG Path and Class
        var iconName        = cmp.get('v.iconName');
        var listIconClass   = cmp.get('v.listIconClass');

        // Array of components to create
        var newComponents = new Array();

        // Add a set of components for each match found
        for (var i=0; i<matches.length; i++) {
            // li element
            newComponents.push(["aura:html", {
                "tag" : "li",
                "HTMLAttributes" : {
                    "class" : "slds-lookup__item"
                }
            }]);

            // a element
            newComponents.push(["aura:html", {
                "tag" : "a",
                "HTMLAttributes" : {
                    "id"            : cmp.getGlobalId() + '_id_' + matches[i].SObjectId,
                    "data-id"       : matches[i].SObjectId,
                    "data-label"    : matches[i].SObjectLabel,
                    "role"          : "option",
                    "onclick"       : cmp.getReference("c.select")
                }
            }]);

            // svg component
            newComponents.push(["lightning:icon", {
                "class"     : listIconClass,
                "size"      : "small",
                "iconName"  : iconName
            }]);

            // output text component
            // For some reason adding an aura:id to this component failed to record the id for subsequent cmp.find requests
            newComponents.push(["ui:outputText", {
                "value" : matches[i].SObjectLabel
            }]);
        }

        // Create the components
        $A.createComponents(newComponents, function(components, status) {
            // Creation succeeded
            if (status === "SUCCESS")
            {
                // Get the List Component Body
                var lookupListItemsBody = lookupListItems.get('v.body');

                // Iterate the created components in groups of 4, correctly parent them and add them to the list body
                for (var i=0; i<components.length; i+=4)
                {
                    // Identify the releated components
                    var li          = components[i];
                    var a           = components[i+1];
                    var svg         = components[i+2];
                    var outputText  = components[i+3];

                    // Add the <a> to the <li>
                    var liBody = li.get('v.body');
                    liBody.push(a);
                    li.set('v.body', liBody);

                    // Add the <svg> and <outputText> to the <a>
                    var aBody = a.get('v.body');
                    aBody.push(svg);
                    aBody.push(outputText);
                    a.set('v.body', aBody);

                    // Add the <li> to the container
                    lookupListItemsBody.push(li);
                }

                // Update the list body
                lookupListItems.set('v.body', lookupListItemsBody);
           }
           else // Report any error
           {
                this.displayToast('Error', 'Failed to create list components.');
           }
        });

    },

    /**
     * Handle the Selection of an Item
     */
    handleSelection : function(cmp, event, dataid, datalabel) {
        // Resolve the Object Id from the events Element Id (this will be the <a> tag)
        var objectId = dataid || event.currentTarget.dataset.id;

        // The Object label is the 2nd child (index 1)
        var objectLabel = datalabel || event.currentTarget.dataset.label;

        // Create the UpdateLookupId event
        var updateEvent = cmp.getEvent("updateLookupIdEvent");

        // Populate the event with the selected Object Id
        updateEvent.setParams({
            "recordId"      : objectId,
            "recordLabel"   : objectLabel,
            "lookupName"    : cmp.get("v.lookupName")
        });

        // Fire the event
        updateEvent.fire();

        // Update the Searchstring with the Label
        cmp.set("v.searchString", objectLabel);

        // Hide the LookupCmp List
        var lookupList = cmp.find("lookuplist");
        $A.util.removeClass(lookupList, 'show');
        $A.util.addClass(lookupList, 'slds-hide');

        // Hide the InputCmp Element
        var inputElement = cmp.find('lookup');
        $A.util.addClass(inputElement, 'slds-hide');

        // Show the LookupCmp pill
        var lookupPill = cmp.find("lookup-pill");
        $A.util.removeClass(lookupPill, 'slds-hide');

        // LookupCmp Div has selection
        var inputElement = cmp.find('lookup-div');
        $A.util.addClass(inputElement, 'slds-has-selection');

    },

    /**
     * Clear the Selection
     */
    clearSelection : function(cmp) {
        // Create the ClearLookupId event
        var clearEvent = cmp.getEvent("clearLookupIdEvent");

        clearEvent.setParams({
            "lookupName" : cmp.get("v.lookupName")
        });

        // Fire the event
        clearEvent.fire();

        // Clear the Searchstring
        cmp.set("v.searchString", '');

        // Hide the LookupCmp pill
        var lookupPill = cmp.find("lookup-pill");
        $A.util.addClass(lookupPill, 'slds-hide');

        // Show the InputCmp Element
        var inputElement = cmp.find('lookup');
        $A.util.removeClass(inputElement, 'slds-hide');

        // LookupCmp Div has no selection
        var inputElement = cmp.find('lookup-div');
        $A.util.removeClass(inputElement, 'slds-has-selection');
    },

    /**
     * Display a message
     */
    displayToast : function (title, message) {
        var toast = $A.get("e.force:showToast");

        // For lightning1 show the toast
        if (toast)
        {
            //fire the toast event in Salesforce1
            toast.setParams({
                "title"     : title,
                "message"   : message
            });

            toast.fire();
        }
        else // otherwise throw an alert
        {
            alert(title + ': ' + message);
        }
    },

    setDefaultRecord: function(cmp, helper) {
        var action = cmp.get("c.lookupById");
        action.setParams({ "recordId" : cmp.get("v.recordId"), "sObjectAPIName" : cmp.get('v.sObjectAPIName')});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
                var record = response.getReturnValue();
                if (record){
                    this.handleSelection(cmp, null, record.SObjectId, record.SObjectLabel);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message){
                        this.displayToast('Error', errors[0].message);
                    }
                } else {
                    this.displayToast('Error', 'Unknown error.');
                }
            }
        });
        $A.enqueueAction(action);
    }
})