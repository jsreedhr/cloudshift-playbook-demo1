({
    doInit : function(component, event, helper) {
        helper.getRelatedContacts(component, event, helper);
    },

    sendPaymentToContact : function(component, event, helper) {
        var selectedContact = event.currentTarget.dataset.record;
        helper.sendPaymentToSelectedContact(component, selectedContact);
    },

    okButton: function(component, evt, helper) {
        //helper.getRelatedContacts(component, event, helper);
        //component.set("v.paymentSuccess", false);
        $A.get("e.force:closeQuickAction").fire();
    },

})