({
    setCurentOpportunityId : function(component, recordId) {
        if (recordId.startsWith('006')) {
            component.set("v.oppId", recordId);
            var action = component.get('c.getReservedUnitName');
            action.setParams({
               'opportunityId' : recordId
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    component.set("v.unitName", response.getReturnValue());
                    if(component.get("v.unitName") != null) {
                        component.set("v.unitExists", true);
                    }
                }
            });
            $A.enqueueAction(action);
        } else {
            var action = component.get('c.getOpportunity');
            action.setParams({
               'leaseId' : recordId
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    var opportunity = response.getReturnValue();
                    if (opportunity) {
                        component.set("v.oppId", opportunity.Id);
                        if (opportunity.Leased_Reserved_Unit__c) {
                            component.set("v.unitName", opportunity.Leased_Reserved_Unit__r.Name);
                            component.set("v.unitExists", true);
                        }
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },

    getRelatedContacts : function(component, event, helper) {
        this.setCurentOpportunityId(component, component.get("v.recordId"));

        var action;
        if (component.get("v.recordId").startsWith('006')) {
            action = component.get('c.getAllContactsForOpportunity');
            action.setParams({
                'opportunityId' : component.get("v.recordId")
            });
        } else {
            action = component.get('c.getAllContactsForLease');
            action.setParams({
                'contractId' : component.get("v.recordId")
            });
        }
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var responseDeserialized = JSON.parse(response.getReturnValue());
                var contacts = responseDeserialized.Contacts;
                var payments = responseDeserialized.Payments;
                var contactsWithPayments = [];
                for (var i = 0; i < contacts.length; i++) {
                    var contactRecord = new Object();
                    contactRecord.Id = contacts[i].Id;
                    contactRecord.Name = contacts[i].Name;
                    contactRecord.AccountName = contacts[i].Account.Name;
                    contactRecord.PaymentStatus = 'None';
                    if(contacts[i].Leased_Reserved_Unit__r != null) {
                        contactRecord.UnitName = contacts[i].Leased_Reserved_Unit__r.Name;
                    }
                    if(payments[contacts[i].Id] && payments[contacts[i].Id].Payment__c) {
                        contactRecord.PaymentStatus = payments[contacts[i].Id].Payment__r.asp04__Payment_Stage__c;
                    }
                    contactsWithPayments.push(contactRecord);
                }
                component.set("v.relatedContacts", contactsWithPayments);
                component.set("v.totalRecords", contacts.length);
            } else if (state === 'INCOMPLETE') {
                console.log('Action incomplete');
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    console.log('Error: ' + errors[0].message);
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },

    sendPaymentToSelectedContact : function(component, selectedContact) {
        var unitName = component.get("v.unitName");
        var action = component.get('c.createAndSendPayment');
        var oppId = component.get("v.oppId");
        action.setParams({
            'contactId' : selectedContact,
            'opportunityId' : oppId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                component.set("v.paymentSuccess", true);
                if(response.getReturnValue()){
                    component.set("v.status", 'Success');
                    component.set("v.message", component.get("v.reservationSuccessLabel"));
                } else {
                    if(unitName == null) {
                        unitName = 'Unit';
                    }
                    component.set("v.status", 'Warning');
                    var label = component.get("v.reservationWarningLabel");
                    label = label.replace(/\{0\}/gi, unitName);
                    component.set("v.message",  label);
                }
            } else if (state === 'INCOMPLETE') {
                console.log('Action incomplete');
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    console.log('Error: ' + errors[0].message);
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },
})