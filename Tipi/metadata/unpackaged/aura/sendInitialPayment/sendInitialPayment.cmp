<aura:component implements="force:lightningQuickAction,force:hasRecordId,force:hasSObjectName"
                description="To send the initial payment request: Deposit and Initial Rent Payment"
                controller="SendInitialPaymentController"
                extends="c:lightningUtility"
                access="global">

    <aura:attribute name="recordId"             type="String" />
    <aura:attribute name="initialPaymentData"   type="Object[]" />
    <aura:attribute name="message"              type="String"   default="" />
    <aura:attribute name="status"               type="String"   default="" />
    <aura:attribute name="paymentSuccess"       type="Boolean"  default="false" />
    <aura:attribute name="showModal"            type="Boolean"  default="false" />
    <aura:attribute name="isLoading"            type="Boolean"  default="false" />
    <aura:attribute name="currencyFormat"       type="String"   default="£##,##,###,##0.00" access="private"/>

    <aura:handler name="init"           value="{!this}"                 action="{!c.initialise}" />

    <!--<lightning:overlayLibrary aura:id="overlayLib"/>-->
    <lightning:notificationsLibrary aura:id="notifLib"/>

    <aura:if isTrue="{!v.isLoading}">
        <lightning:spinner alternativeText="Loading" size="medium" />
    </aura:if>

    <ui:scrollerWrapper class="scrollerSize">
        <table class="slds-table slds-table_bordered slds-table--cell-buffer slds-p-right_small">
            <thead>
                <tr class="slds-text-title--caps">
                    <th scope="col">
                        <div class="slds-truncate">Contact</div>
                    </th>
                    <th scope="col">
                        <div class="slds-truncate">Deposit</div>
                    </th>
                    <th scope="col">
                        <div class="slds-truncate">Initial Payment</div>
                    </th>
                    <th scope="col">
                        <div class="slds-truncate">Reservation</div>
                    </th>
                    <th scope="col">
                        <div class="slds-truncate">Amount</div>
                    </th>
                    <th scope="col">
                    </th>
                </tr>
            </thead>

            <tbody>
                <aura:iteration items="{!v.initialPaymentData}" var="row">

                    <tr>
                        <td>
                            <a href="{!'/' + row.contact.Id}">{!row.contact.FirstName + ' ' + row.contact.LastName}</a>
                            <div class="slds-truncate">
                                Account:
                                <a href="{!'/' + row.contact.AccountId}">{!row.contact.Account.Name}</a>
                            </div>
                        </td>
                        <td>
                            <aura:if isTrue="{!row.deposit.Id}">
                                <a href="{!'/' + row.deposit.Id}">{!row.deposit.Name}</a>
                                <div class="slds-truncate">
                                    <div class="custom-inline">Amount: </div>
                                    <ui:outputCurrency class="custom-inline" value="{!row.deposit.Amount__c}" format="{!v.currencyFormat}"/>
                                </div>
                                <div class="slds-truncate">
                                    <div class="custom-inline">Status: </div>
                                    <div class="{!'custom-inline slds-text-color_' + if(row.deposit.Status__c eq 'Collected from customer', 'success', 'error')}">
                                        {!row.deposit.Status__c}
                                    </div>
                                </div>
                                <div class="slds-truncate">
                                    <aura:if isTrue="{!row.deposit.Payment__c}">
                                        Asperato Payment:
                                        <a class="custom-inline" href="{!'/' + row.deposit.Payment__c}">{!row.deposit.Payment__r.Name}</a>
                                        <div class="custom-inline"> (</div>
                                        <ui:outputCurrency class="custom-inline" value="{!row.deposit.Payment__r.asp04__Amount__c}" format="{!v.currencyFormat}"/>
                                        <div class="custom-inline">)</div>
                                        <aura:set attribute="else">
                                            No Asperato Payment
                                        </aura:set>
                                    </aura:if>
                                </div>

                                <aura:set attribute="else">
                                    No Deposit record found
                                </aura:set>
                            </aura:if>
                        </td>
                        <td>
                            <aura:if isTrue="{!row.initialPayment.Id}">
                                <div class="slds-truncate">
                                    <a class="custom-inline" href="{!'/' + row.initialPayment.Id}">{!row.initialPayment.Name}</a>
                                    <div class="custom-inline" style="font-weight:bold">{!if(row.isUpFrontCredit,'' ,' (Rent Only)' )}</div>
                                </div>
                                <div class="slds-truncate">
                                    <div class="custom-inline">Amount: </div>
                                    <ui:outputCurrency class="custom-inline" value="{!row.initialPayment.Amount__c}" format="{!v.currencyFormat}"/>
                                </div>
                                <div class="slds-truncate">
                                    <div class="custom-inline">Status: </div>
                                    <div class="{!'custom-inline slds-text-color_' + if(row.initialPayment.Status__c eq 'Collected from customer', 'success', 'error')}">
                                        {!row.initialPayment.Status__c}
                                    </div>
                                </div>

                                <div class="slds-truncate">
                                    <aura:if isTrue="{!row.initialPayment.Payment__c}">
                                        Asperato Payment:
                                        <a class="custom-inline" href="{!'/' + row.initialPayment.Payment__c}">{!row.initialPayment.Payment__r.Name}</a>
                                        <div class="custom-inline"> (</div>
                                        <ui:outputCurrency class="custom-inline" value="{!row.initialPayment.Payment__r.asp04__Amount__c}" format="{!v.currencyFormat}"/>
                                        <div class="custom-inline">)</div>
                                        <aura:set attribute="else">
                                            No Asperato Payment
                                        </aura:set>
                                    </aura:if>
                                </div>

                                <aura:set attribute="else">
                                    No Initial Payment record found
                                </aura:set>
                            </aura:if>
                        </td>
                        <td>
                            <aura:if isTrue="{!row.reservation.Id}">
                                <a href="{!'/' + row.reservation.Id}">{!row.reservation.Name}</a>
                                <div class="slds-truncate">
                                    <div class="custom-inline">Amount: </div>
                                    <div class="{!'custom-inline ' + if(row.isUpFrontCredit,'custom-line-through' ,'')}">
                                        <ui:outputCurrency class="custom-inline"
                                                           value="{!'-'+row.reservation.Amount__c}"
                                                           format="{!v.currencyFormat}" />
                                        <div class="custom-inline"> (Remaining Balance: </div>
                                        <ui:outputCurrency class="custom-inline"
                                                           value="{!'-'+row.reservation.Total_Remaining_Balance__c}"
                                                           format="{!v.currencyFormat}" />
                                        <div class="custom-inline">)</div>
                                    </div>
                                </div>
                                <div class="slds-truncate">
                                    <div class="custom-inline">Status: </div>
                                    <div class="{!'custom-inline slds-text-color_' + if(row.reservation.Status__c eq 'Collected from customer', 'success', 'error')}">
                                        {!row.reservation.Status__c}
                                    </div>
                                </div>

                                <div class="slds-truncate">
                                    <aura:if isTrue="{!row.reservation.Payment__c}">
                                        Asperato Payment:
                                        <a class="custom-inline" href="{!'/' + row.reservation.Payment__c}">{!row.reservation.Payment__r.Name}</a>
                                        <div class="custom-inline"> (</div>
                                        <ui:outputCurrency class="custom-inline" value="{!row.reservation.Payment__r.asp04__Amount__c}" format="{!v.currencyFormat}"/>
                                        <div class="custom-inline">)</div>
                                        <aura:set attribute="else">
                                            No Asperato Payment
                                        </aura:set>
                                    </aura:if>
                                </div>

                                <aura:set attribute="else">
                                    No Reservation Payment record found
                                </aura:set>
                            </aura:if>
                        </td>
                        <td>
                            <ui:outputCurrency value="{!row.totalAmount}" format="{!v.currencyFormat}"/>
                        </td>
                        <td>
                            <button class="{! 'slds-button custom-width-140 ' + if(row.isFirstRequest, 'slds-button_brand', 'slds-button_neutral') }"
                                    data-contactid="{!row.contact.Id}"
                                    onclick="{!c.sendPayment}"
                                    disabled="{! not(row.totalAmount gt 0)}">
                                {! if(row.isFirstRequest, 'Request Payment', 'Resend Request') }
                            </button>
                        </td>
                    </tr>
                </aura:iteration>
            </tbody>
        </table>
    </ui:scrollerWrapper>

    <aura:if isTrue="{!v.showModal}">
        <div class="slds-modal slds-fade-in-open slds-align_absolute-center" style="width: 300px">
            <div class="slds-modal__container ">
                <div class="slds-border_left slds-border_right slds-border_top slds-border_bottom">
                    <header class="{! 'slds-modal__header ' + if(v.paymentSuccess, 'slds-theme_success', 'slds-theme_error')}">
                        <button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" title="Close">
                            <span class="slds-assistive-text">Close</span>
                        </button>
                        <h2 id="modal-heading-01" class="slds-text-heading_medium slds-hyphenate">{!v.status}</h2>
                    </header>
                    <div class="slds-grid slds-grid_align-center slds-modal__content slds-p-around_small">
                        {!v.message}
                    </div>
                    <footer class="slds-modal__footer">
                        <button class="slds-button slds-button_neutral" onclick="{!c.okButton}">OK</button>
                    </footer>
                </div>
            </div>
        </div>
    </aura:if>

    {!v.body}
</aura:component>