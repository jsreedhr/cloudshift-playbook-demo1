({
    initialise : function(cmp, event, helper) {
        helper.initialiseHelper(cmp, event, helper);
    },

    sendPayment : function(cmp, event, helper) {
        helper.sendPaymentHelper(cmp, event, helper);
    },

    okButton: function(cmp, event, helper) {
        cmp.set('v.showModal', false);
    }
});