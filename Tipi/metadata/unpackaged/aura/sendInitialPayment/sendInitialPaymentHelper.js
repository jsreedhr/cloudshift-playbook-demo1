({
    initialiseHelper : function(cmp, event, helper) {
        helper.validatePermissions(cmp, 'Financial_Configuration_Permission');
        helper.maximiseQuickActionModal(cmp);
        helper.getInitialPayments(cmp, helper);
    },

    getInitialPayments : function(cmp, helper) {
        let action = cmp.get('c.getInitialPayments');
        action.setParams({
           'recordId' : cmp.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                cmp.set('v.initialPaymentData', JSON.parse(JSON.stringify(response.getReturnValue())));
            }
        });
        $A.enqueueAction(action);
    },

    sendPaymentHelper : function(cmp, event, helper) {
        const selectedContactId = event.currentTarget.dataset.contactid;

        cmp.set('v.isLoading', true);
        console.log('sending for ' + selectedContactId);

        let action = cmp.get('c.requestPayment');
        action.setParams({
           'recordId' : cmp.get('v.recordId'),
           'contactId' : selectedContactId
        });
        action.setCallback(this, function(response) {
            const state = response.getState();
            cmp.set('v.isLoading', false);
            if (state === 'SUCCESS') {
                helper.handleShowPopover(cmp, helper, response.getReturnValue());
            } else {
                // show response message
                helper.handleShowPopover(cmp, helper, false);
            }
            // refresh current page
            helper.getInitialPayments(cmp, helper);
        });
        $A.enqueueAction(action);
    },

    handleShowPopover : function(cmp, helper, isSuccess) {
        let header = 'Something has gone wrong!';
        let message = 'Unfortunately, there was a problem updating the record.';
        if(isSuccess) {
            header = 'Success!';
            message = 'Successfully send out payment request';
        }

        cmp.set('v.paymentSuccess', isSuccess);
        cmp.set('v.status', header);
        cmp.set('v.message', message);
        cmp.set('v.showModal', true);
    }
});