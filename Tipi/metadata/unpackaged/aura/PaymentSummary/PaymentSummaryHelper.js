({
    getPaymentsList: function(component, helper) {
        var action = component.get('c.getPaymentsForContact');
        if(component.get('v.contactId')) {
            action.setParams({
                'contactId' : component.get('v.contactId')
            });
        }
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.getPaymentsListResponse(component, helper, JSON.parse(JSON.stringify(response.getReturnValue())));
            }
            console.log(response);
        });
        $A.enqueueAction(action);
    },

    getPaymentsListResponse: function(component, helper, response) {
        component.set('v.payments', response);
        let maxPage = Math.floor((response.length + 9) / 10);
        if (!(maxPage > 0)) maxPage = 1;
        component.set('v.maxPage', maxPage);
        if(Array.isArray(response)) {
            let chargeSum = 0;
            let paymentsSum = 0;
            for(let i = 0; i < response.length; i++) {
                console.log('pat: ' + response[i].charge + '; ' + response[i].payment);
                chargeSum += parseFloat(response[i].charge);
                paymentsSum += parseFloat(response[i].payment);
            }

            component.set('v.chargeSum', parseFloat(chargeSum).toFixed(2));
            component.set('v.paymentsSum', parseFloat(paymentsSum).toFixed(2));
        }
        helper.renderPage(component);
    },

    renderPage: function(component) {
        var records = component.get('v.payments');
        var pageNumber = component.get('v.pageNumber');
        var pageRecords = records.slice((pageNumber - 1) * 10, pageNumber * 10);
        component.set('v.currentList', pageRecords);
    },

    getSortType: function(component, event, sortIcon) {
        var sortType = event.currentTarget.getAttribute('aria-sort');
        var arrowIcon = component.find(sortIcon);
        if (sortType === 'none' || sortType === 'descending') {
            sortType = 'ascending';
            event.currentTarget.setAttribute('aria-sort', 'ascending');
            arrowIcon.set('v.iconName', 'utility:arrowup');
        } else if (sortType === 'ascending') {
            sortType = 'descending';
            event.currentTarget.setAttribute('aria-sort', 'descending');
            arrowIcon.set('v.iconName', 'utility:arrowdown');
        }
        return sortType;
    },

    sortInverse: function(array) {
        const sortedList = [];
        for (var i = array.length; i > 0; i--) {
            sortedList.push(array[(i-1)]);
        }
        return sortedList;
    },

    // to be used in the future
    sort: function(field, asc_desc, array) {
        var payments = array;
        const pd = 'paymentDate';
        for (var i = 0; i < payments.length; i++) {
            for (var j = i + 1; j < payments.length; j++) {
                if (field === 'date') {
                    if (asc_desc === 'ascending') {
                        if (this.datecompare(new Date(payments[i][pd]), '>', new Date(payments[j][pd]))) {
                            var swap = payments[i];
                            payments[i] = payments[j];
                            payments[j] = swap;
                        }
                    }
                    if (asc_desc === 'descending') {
                        if (this.datecompare(new Date(payments[i][pd]), '<', new Date(payments[j][pd]))) {
                            var swap = payments[i];
                            payments[i] = payments[j];
                            payments[j] = swap;
                        }
                    }
                }
            }
        }
        return payments;
    },

    datecompare : function(date1, sign, date2) {
        var day1 = date1.getDate();
        var mon1 = date1.getMonth();
        var year1 = date1.getFullYear();
        var day2 = date2.getDate();
        var mon2 = date2.getMonth();
        var year2 = date2.getFullYear();
        if (sign === '===') {
            if (day1 === day2 && mon1 === mon2 && year1 === year2) return true;
            else return false;
        } else if (sign === '>') {
            if (year1 > year2) return true;
            else if (year1 === year2 && mon1 > mon2) return true;
            else if (year1 === year2 && mon1 === mon2 && day1 > day2) return true;
            else return false;
        } else if (sign === '<') {
            if (year1 < year2) return true;
            else if (year1 === year2 && mon1 < mon2) return true;
            else if (year1 === year2 && mon1 === mon2 && day1 < day2) return true;
            else return false;
        }
    }
});