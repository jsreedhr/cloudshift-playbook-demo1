({
    doInit: function(component, event, helper) {
        helper.maximiseQuickActionModal(component);
        helper.getPaymentsList(component, helper);
    },

    renderPage: function(component, event, helper) {
        helper.renderPage(component);
    },

    sortByDate: function(component, event, helper) {
        const sortType = helper.getSortType(component, event, 'sortIconDate');
        //const payments = helper.sort('date', sortType, component.get('v.payments'));
        const payments = helper.sortInverse(component.get('v.payments'));
        component.set('v.payments', payments);
        helper.renderPage(component);
    },

    showSpinner : function (component, event, helper) {
        $A.util.addClass(component.find('spinner'), 'show');
    },

    hideSpinner : function (component, event, helper) {
        $A.util.removeClass(component.find('spinner'), 'show');
    }
});