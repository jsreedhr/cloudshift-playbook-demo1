({
	requestPayment : function(component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        var amount = component.get("v.amount");
		var action = component.get("c.createPayment");
        action.setParams({ contactId : component.get("v.recordId"), paymentAmount: amount }); 
        action.setCallback(this, function(response) {
        	$A.util.toggleClass(spinner, "slds-hide");
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = JSON.parse(response.getReturnValue());
                $A.get("e.force:closeQuickAction").fire();
                if(result.success) {
                	this.showSuccessToast(component, event, helper, true, result.message);
                } else {
                    this.showSuccessToast(component, event, helper, false, result.message);
                }
            } else {
            	this.showSuccessToast(component, event,  false, $A.get("$Label.c.Something_went_wrong_Please_try_again"));
            }
 		});
		$A.enqueueAction(action);	
	},
    showSuccessToast: function(component, event, helper, success, msg) {
        var type = success ? 'success' : 'error';
        var title = success ? 'Success' : 'Error';
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: msg,
            duration:' 5000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
	}
})