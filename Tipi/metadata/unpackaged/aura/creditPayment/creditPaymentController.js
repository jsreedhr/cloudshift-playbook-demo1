({
	requestPayment : function(component, event, helper) {
		helper.requestPayment(component, event, helper);
	},
    updDisableProperty : function(component, event, helper) {
        var amount = component.get("v.amount");
        if(amount > 0){
            component.set("v.buttDisabled", false);
        } else {
            component.set("v.buttDisabled", true);
        }
    },
})