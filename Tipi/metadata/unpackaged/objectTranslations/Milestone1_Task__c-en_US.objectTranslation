<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>User Stories</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>User Stories</value>
    </caseValues>
    <fields>
        <label><!-- AC Approved --></label>
        <name>AC_Approved__c</name>
    </fields>
    <fields>
        <label><!-- AC Defined --></label>
        <name>AC_Defined__c</name>
    </fields>
    <fields>
        <label><!-- Acceptance Criteria --></label>
        <name>Acceptance_Criteria__c</name>
    </fields>
    <fields>
        <label><!-- Additional Information --></label>
        <name>Additional_Information__c</name>
    </fields>
    <fields>
        <help><!-- Text to describe reason for additional user story or to describe where User Story has changed due to change in business requirement --></help>
        <label><!-- Additional User Story Text --></label>
        <name>Additional_User_Story_Text__c</name>
    </fields>
    <fields>
        <help><!-- Indicates additional User story identified and added to scope --></help>
        <label><!-- Additional User Story --></label>
        <name>Additional_User_Story__c</name>
    </fields>
    <fields>
        <label><!-- As a --></label>
        <name>As_a__c</name>
        <picklistValues>
            <masterLabel>Automation User</masterLabel>
            <translation><!-- Automation User --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>External Service Provider</masterLabel>
            <translation><!-- External Service Provider --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Finance Agent</masterLabel>
            <translation><!-- Finance Agent --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Finance Manager</masterLabel>
            <translation><!-- Finance Manager --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Leasing Agent</masterLabel>
            <translation><!-- Leasing Agent --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Leasing Manager</masterLabel>
            <translation><!-- Leasing Manager --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Marketing User</masterLabel>
            <translation><!-- Marketing User --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Operations Agent</masterLabel>
            <translation><!-- Operations Agent --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Operations Manager</masterLabel>
            <translation><!-- Operations Manager --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Potential Customer</masterLabel>
            <translation><!-- Potential Customer --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Resident</masterLabel>
            <translation><!-- Resident --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Revenue Manager</masterLabel>
            <translation><!-- Revenue Manager --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>System Adminstrator</masterLabel>
            <translation><!-- System Adminstrator --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Tipi Manager</masterLabel>
            <translation><!-- Tipi Manager --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Tipi Staff Applicant</masterLabel>
            <translation><!-- Tipi Staff Applicant --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The user responsible for completing the task. Defaults to current user if left blank. --></help>
        <label><!-- Assigned To --></label>
        <name>Assigned_To__c</name>
        <relationshipLabel><!-- User Stories --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- If blocked, why? --></help>
        <label><!-- Reason --></label>
        <name>Blocked_Reason__c</name>
    </fields>
    <fields>
        <help><!-- Is this task blocked for some reason? --></help>
        <label><!-- Blocked --></label>
        <name>Blocked__c</name>
    </fields>
    <fields>
        <label><!-- Change Set --></label>
        <name>Change_Set__c</name>
    </fields>
    <fields>
        <help><!-- Classify this task as appropriate for your use. --></help>
        <label><!-- Class --></label>
        <name>Class__c</name>
        <picklistValues>
            <masterLabel>Ad Hoc</masterLabel>
            <translation><!-- Ad Hoc --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Defect</masterLabel>
            <translation><!-- Defect --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rework</masterLabel>
            <translation><!-- Rework --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Community Requirement --></label>
        <name>Community_Requirement__c</name>
    </fields>
    <fields>
        <help><!-- The status of this task. A task is either in-complete or complete. --></help>
        <label><!-- Complete --></label>
        <name>Complete__c</name>
    </fields>
    <fields>
        <label><!-- Consultant Type --></label>
        <name>Consultant_Type__c</name>
        <picklistValues>
            <masterLabel>Config and Dev</masterLabel>
            <translation><!-- Config and Dev --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Configuraion</masterLabel>
            <translation><!-- Configuraion --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Development</masterLabel>
            <translation><!-- Development --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Days Late Formula --></label>
        <name>Days_Late_Formula__c</name>
    </fields>
    <fields>
        <help><!-- How many days late is this task? --></help>
        <label><!-- Days Late --></label>
        <name>Days_Late__c</name>
    </fields>
    <fields>
        <help><!-- The general description of the task. For example, what is the purpose of the task. --></help>
        <label><!-- Description --></label>
        <name>Description__c</name>
    </fields>
    <fields>
        <help><!-- The date this task is due to be completed. Defaults to milestone due date if left blank. --></help>
        <label><!-- Due Date --></label>
        <name>Due_Date__c</name>
    </fields>
    <fields>
        <label><!-- Email GUID --></label>
        <name>Email_GUID__c</name>
    </fields>
    <fields>
        <help><!-- The estimated amount of expenses which will be incurred during the task. --></help>
        <label><!-- Estimated Expense --></label>
        <name>Estimated_Expense__c</name>
    </fields>
    <fields>
        <help><!-- The estimated time to complete the task. --></help>
        <label><!-- Estimated Hours --></label>
        <name>Estimated_Hours__c</name>
    </fields>
    <fields>
        <label><!-- Expected Hours (pointed) --></label>
        <name>Expected_Hours__c</name>
    </fields>
    <fields>
        <help><!-- Calculated as estimated expense - total expense --></help>
        <label><!-- Expense Balance --></label>
        <name>Expense_Balance__c</name>
    </fields>
    <fields>
        <label><!-- Functional Group --></label>
        <name>Functional_Group__c</name>
    </fields>
    <fields>
        <help><!-- Calculated as estimated hours - total hours --></help>
        <label><!-- Hours Balance --></label>
        <name>Hours_Balance__c</name>
    </fields>
    <fields>
        <label><!-- I want to --></label>
        <name>I_want_to__c</name>
    </fields>
    <fields>
        <label><!-- Implementation Details --></label>
        <name>Implementation_Details__c</name>
    </fields>
    <fields>
        <label><!-- Import ID --></label>
        <name>ImportID__c</name>
    </fields>
    <fields>
        <label><!-- Index Helper --></label>
        <name>Index_Helper__c</name>
    </fields>
    <fields>
        <label><!-- Index --></label>
        <name>Index__c</name>
    </fields>
    <fields>
        <help><!-- The date the last email was attached to this task. A task can be created via an email, and additional emails can be attached to the task in the notes section. --></help>
        <label><!-- Last Email Received --></label>
        <name>Last_Email_Received__c</name>
    </fields>
    <fields>
        <label><!-- Milestone Grandparent --></label>
        <name>Milestone_Grandparent__c</name>
    </fields>
    <fields>
        <label><!-- No Closed Actions --></label>
        <name>No_Closed_Actions__c</name>
    </fields>
    <fields>
        <label><!-- No Open Actions --></label>
        <name>No_Open_Actions__c</name>
    </fields>
    <fields>
        <label><!-- No of Actions --></label>
        <name>No_of_Actions__c</name>
    </fields>
    <fields>
        <help><!-- Calculated field indicating if the task is complete, open, late, blocked or late and blocked. --></help>
        <label><!-- Overall Status --></label>
        <name>Overall_Status__c</name>
    </fields>
    <fields>
        <label><!-- OwnerId --></label>
        <name>OwnerId__c</name>
    </fields>
    <fields>
        <label><!-- Playback Sequence --></label>
        <name>Playback_Sequence__c</name>
    </fields>
    <fields>
        <label><!-- Points as Number --></label>
        <name>Points_as_Number__c</name>
    </fields>
    <fields>
        <label><!-- Predecessor_Task_Import_Id__c --></label>
        <name>Predecessor_Task_Import_Id__c</name>
    </fields>
    <fields>
        <help><!-- The predecessor task for this task if applicable. --></help>
        <label><!-- Associated User Story --></label>
        <lookupFilter>
            <errorMessage><!-- User story does is not assigned to the assigned user for this story --></errorMessage>
        </lookupFilter>
        <name>Predecessor_Task__c</name>
        <relationshipLabel><!-- Associated User Stories --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Sprint user story was previously allocated to --></help>
        <label><!-- Previous Sprint Allocated to --></label>
        <name>Previous_Sprint__c</name>
        <picklistValues>
            <masterLabel>1</masterLabel>
            <translation><!-- 1 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>2</masterLabel>
            <translation><!-- 2 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>3</masterLabel>
            <translation><!-- 3 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>4</masterLabel>
            <translation><!-- 4 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>5</masterLabel>
            <translation><!-- 5 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>6</masterLabel>
            <translation><!-- 6 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Previous Stage --></label>
        <name>Previous_Stage__c</name>
        <picklistValues>
            <masterLabel>Could</masterLabel>
            <translation><!-- Could --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Must</masterLabel>
            <translation><!-- Must --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Should</masterLabel>
            <translation><!-- Should --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Won&apos;t</masterLabel>
            <translation><!-- Won&apos;t --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- To visualise the degraded priority required for MVP. --></help>
        <label><!-- Priority MVP --></label>
        <name>Priority_MVP__c</name>
        <picklistValues>
            <masterLabel>?</masterLabel>
            <translation><!-- ? --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Could</masterLabel>
            <translation><!-- Could --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Must</masterLabel>
            <translation><!-- Must --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Should</masterLabel>
            <translation><!-- Should --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Won&apos;t</masterLabel>
            <translation><!-- Won&apos;t --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- This is the priority of the task. 0,1,2,3 or 4. --></help>
        <label><!-- Priority --></label>
        <name>Priority__c</name>
        <picklistValues>
            <masterLabel>Could</masterLabel>
            <translation><!-- Could --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Must</masterLabel>
            <translation><!-- Must --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Should</masterLabel>
            <translation><!-- Should --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Won&apos;t</masterLabel>
            <translation><!-- Won&apos;t --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- ProjectId --></label>
        <name>ProjectId__c</name>
    </fields>
    <fields>
        <label><!-- ProjectStatus --></label>
        <name>ProjectStatus__c</name>
    </fields>
    <fields>
        <help><!-- The milestone that this task is assigned to. --></help>
        <label><!-- Epic --></label>
        <name>Project_Milestone__c</name>
        <relationshipLabel><!-- User Stories --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- A link to the Project which this task is included in via its milestone. --></help>
        <label><!-- Project --></label>
        <name>Project__c</name>
    </fields>
    <fields>
        <label><!-- RAG --></label>
        <name>RAG__c</name>
    </fields>
    <fields>
        <label><!-- Reason for Cancellation --></label>
        <name>Reason_for_Cancellation__c</name>
    </fields>
    <fields>
        <label><!-- Requirement Type --></label>
        <name>Requirement_Type__c</name>
    </fields>
    <fields>
        <help><!-- Unique number for each task and across all projects. --></help>
        <label><!-- Short ID --></label>
        <name>Short_ID__c</name>
    </fields>
    <fields>
        <label><!-- So that --></label>
        <name>So_that__c</name>
    </fields>
    <fields>
        <label><!-- Sprint --></label>
        <name>Sprint__c</name>
        <picklistValues>
            <masterLabel>1</masterLabel>
            <translation><!-- 1 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>2</masterLabel>
            <translation><!-- 2 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>3</masterLabel>
            <translation><!-- 3 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>4</masterLabel>
            <translation><!-- 4 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>5</masterLabel>
            <translation><!-- 5 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>6</masterLabel>
            <translation><!-- 6 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>7</masterLabel>
            <translation><!-- 7 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>8</masterLabel>
            <translation><!-- 8 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- When should this task start by? --></help>
        <label><!-- Start Date --></label>
        <name>Start_Date__c</name>
    </fields>
    <fields>
        <label><!-- Story Points --></label>
        <name>Story_Points__c</name>
        <picklistValues>
            <masterLabel>1</masterLabel>
            <translation><!-- 1 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>13</masterLabel>
            <translation><!-- 13 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>2</masterLabel>
            <translation><!-- 2 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>21</masterLabel>
            <translation><!-- 21 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>3</masterLabel>
            <translation><!-- 3 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>34</masterLabel>
            <translation><!-- 34 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>5</masterLabel>
            <translation><!-- 5 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>55</masterLabel>
            <translation><!-- 55 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>8</masterLabel>
            <translation><!-- 8 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>99</masterLabel>
            <translation><!-- 99 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- TS Defined --></label>
        <name>TS_Defined__c</name>
    </fields>
    <fields>
        <help><!-- Where is this task on the road to completion? --></help>
        <label><!-- Stage --></label>
        <name>Task_Stage__c</name>
        <picklistValues>
            <masterLabel>AC Required</masterLabel>
            <translation><!-- AC Required --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Blocked</masterLabel>
            <translation><!-- Blocked --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Cancelled</masterLabel>
            <translation><!-- Cancelled --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Deployed</masterLabel>
            <translation><!-- Deployed --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Done</masterLabel>
            <translation><!-- Done --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>In Dev</masterLabel>
            <translation><!-- In Dev --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>In Draft</masterLabel>
            <translation><!-- In Draft --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>In Testing</masterLabel>
            <translation><!-- In Testing --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>New</masterLabel>
            <translation><!-- New --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>PO Approved</masterLabel>
            <translation><!-- PO Approved --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>PO Review</masterLabel>
            <translation><!-- PO Review --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Ready for Dev</masterLabel>
            <translation><!-- Ready for Dev --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>TA Review</masterLabel>
            <translation><!-- TA Review --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>TD Required</masterLabel>
            <translation><!-- TD Required --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Technical Specification --></label>
        <name>Technical_Specification__c</name>
    </fields>
    <fields>
        <label><!-- Tipi Lead --></label>
        <name>Tipi_Lead__c</name>
        <picklistValues>
            <masterLabel>Chris Charalambous</masterLabel>
            <translation><!-- Chris Charalambous --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Dominic McVeigh</masterLabel>
            <translation><!-- Dominic McVeigh --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Jennie Fojtik</masterLabel>
            <translation><!-- Jennie Fojtik --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Laura Meyer Cutler</masterLabel>
            <translation><!-- Laura Meyer Cutler --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rajesh Shah</masterLabel>
            <translation><!-- Rajesh Shah --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Russell Markou</masterLabel>
            <translation><!-- Russell Markou --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Calculated from all child expense records --></help>
        <label><!-- Total Expense --></label>
        <name>Total_Expense__c</name>
    </fields>
    <fields>
        <help><!-- Calculated from all child time records --></help>
        <label><!-- Total Hours (Logged) --></label>
        <name>Total_Hours__c</name>
    </fields>
    <fields>
        <label><!-- US External Id --></label>
        <name>US_External_Id__c</name>
    </fields>
    <fields>
        <label><!-- User Story Id --></label>
        <name>User_Story_Id__c</name>
    </fields>
    <fields>
        <label><!-- isCode --></label>
        <name>isCode__c</name>
    </fields>
    <layouts>
        <layout>Project Task Layout</layout>
        <sections>
            <label><!-- Acceptance Criteria --></label>
            <section>Acceptance Criteria</section>
        </sections>
        <sections>
            <label><!-- Blocked --></label>
            <section>Blocked</section>
        </sections>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
        <sections>
            <label><!-- Requirements Overview --></label>
            <section>Requirements Overview</section>
        </sections>
        <sections>
            <label><!-- Specification --></label>
            <section>Specification</section>
        </sections>
    </layouts>
    <quickActions>
        <label><!-- Log Time --></label>
        <name>Log_Time</name>
    </quickActions>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <errorMessage><!-- You cannot mark a completed task as blocked. --></errorMessage>
        <name>CannotBeBlockedIfComplete</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- You cannot complete a task marked as blocked. --></errorMessage>
        <name>CannotBeCompleteIfBlocked</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Due Date should be smaller than Milestone Deadline --></errorMessage>
        <name>Due_Date_Greater_Than_Milestone_Deadline</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Due Date should be greater than Milestone Kickoff Date --></errorMessage>
        <name>Due_Date_Smaller_Than_Milestone_Kickoff</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Must be blank if &quot;blocked&quot; is not checked. --></errorMessage>
        <name>Must_be_blank_if_not_blocked</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- If this task is blocked you must enter a reason. --></errorMessage>
        <name>Must_not_be_blank_if_blocked</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- If the user story is blocked, the Stage must = Blocked --></errorMessage>
        <name>Stage_must_be_Blocked_if_checkbox_true</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Due Date should be greater than Start Date --></errorMessage>
        <name>Start_Date_More_Than_Due_Date</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The Change Set and Implementation Details must be entered to set the Stage to Done or to tick Complete --></errorMessage>
        <name>Status_cannot_be_Done_without_CS_and_ID</name>
    </validationRules>
    <webLinks>
        <label><!-- Change_Milestone --></label>
        <name>Change_Milestone</name>
    </webLinks>
    <webLinks>
        <label><!-- Move_to_New_Milestone --></label>
        <name>Move_to_New_Milestone</name>
    </webLinks>
</CustomObjectTranslation>
