<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>4 bed</label>
    <protected>false</protected>
    <values>
        <field>Property_Type_Description__c</field>
        <value xsi:type="xsd:string">Flat</value>
    </values>
    <values>
        <field>Property_Type_Number__c</field>
        <value xsi:type="xsd:double">8.0</value>
    </values>
    <values>
        <field>Unit_Type__c</field>
        <value xsi:type="xsd:string">4 bed</value>
    </values>
</CustomMetadata>
