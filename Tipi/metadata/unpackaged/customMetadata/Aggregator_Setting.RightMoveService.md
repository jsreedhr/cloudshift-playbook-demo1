<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RightMoveService</label>
    <protected>false</protected>
    <values>
        <field>Action__c</field>
        <value xsi:type="xsd:string">Insert</value>
    </values>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Branch_Id__c</field>
        <value xsi:type="xsd:string">148778</value>
    </values>
    <values>
        <field>Certificate__c</field>
        <value xsi:type="xsd:string">cloudshifttest</value>
    </values>
    <values>
        <field>Channel__c</field>
        <value xsi:type="xsd:string">2</value>
    </values>
    <values>
        <field>Content_Type__c</field>
        <value xsi:type="xsd:string">application/json; charset=utf-8</value>
    </values>
    <values>
        <field>Method__c</field>
        <value xsi:type="xsd:string">POST</value>
    </values>
    <values>
        <field>Network_Id__c</field>
        <value xsi:type="xsd:string">10421</value>
    </values>
    <values>
        <field>Overseas__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>URL__c</field>
        <value xsi:type="xsd:string">https://adfapi.adftest.rightmove.com/v1/property/sendpropertydetails</value>
    </values>
</CustomMetadata>
