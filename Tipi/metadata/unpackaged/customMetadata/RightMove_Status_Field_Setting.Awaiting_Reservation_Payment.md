<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Awaiting Reservation Payment</label>
    <protected>false</protected>
    <values>
        <field>Integer_Value__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Status_Description__c</field>
        <value xsi:type="xsd:string">Reserved (sales only)</value>
    </values>
    <values>
        <field>Status_Number__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>Unit_Status_Value__c</field>
        <value xsi:type="xsd:string">Awaiting Reservation Payment</value>
    </values>
</CustomMetadata>
