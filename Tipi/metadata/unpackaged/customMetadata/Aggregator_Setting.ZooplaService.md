<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ZooplaService</label>
    <protected>false</protected>
    <values>
        <field>Action__c</field>
        <value xsi:type="xsd:string">Insert</value>
    </values>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Branch_Id__c</field>
        <value xsi:type="xsd:string">73849</value>
    </values>
    <values>
        <field>Certificate__c</field>
        <value xsi:type="xsd:string">ZooplaAPI</value>
    </values>
    <values>
        <field>Channel__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Content_Type__c</field>
        <value xsi:type="xsd:string">application/json; profile=https://realtime-listings.webservices.zpg.co.uk/docs/v1.2/schemas/listing/update.json</value>
    </values>
    <values>
        <field>Method__c</field>
        <value xsi:type="xsd:string">POST</value>
    </values>
    <values>
        <field>Network_Id__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Overseas__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>URL__c</field>
        <value xsi:type="xsd:string">https://realtime-listings-api.webservices.zpg.co.uk/sandbox/v1/listing/update</value>
    </values>
</CustomMetadata>
