<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Higher than 2nd floor (with lift)</label>
    <protected>false</protected>
    <values>
        <field>Default_Entrance_Floor__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Entrance_Floor_Description__c</field>
        <value xsi:type="xsd:string">Higher than 2nd floor (with lift)</value>
    </values>
    <values>
        <field>Entrance_Floor_Number__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>Unit_Floor_Value__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
