<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default</label>
    <protected>false</protected>
    <values>
        <field>Building_Id__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Deposit__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Reservation_Fee__c</field>
        <value xsi:type="xsd:double">400.0</value>
    </values>
</CustomMetadata>
