/**
    * @Name:        RightMoveService
    * @Description: Class to process callouts for RightMove API.
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 06/04/2018    Eugene Konstantinov    Created Class
*/
public with sharing class RightMoveService implements AggregationsInterface{

	/**
	 * Main method to process callouts for any Aggregator
	 * @param  aggregator  argument contains all needed values to do callout
	 * @param  units list of units to be processed to RightMove API
	 */

	public Map<Id, String> processRequest(List<Unit__c> units, Aggregator_Setting__mdt aggregator) {
        Map<Id, String> responsesByUnitId = new Map<Id, String>();
		// Generating JSON based on RightMove Schema
		Map<Id, String> jsonByUnitId = RightMoveUtilities.generateJson(units, aggregator.Network_Id__c, aggregator.Branch_Id__c, aggregator.Channel__c, aggregator.Overseas__c);
		if (aggregator != null && units != null && !units.isEmpty()) {
			for (Unit__C unit : units) {
				// Invoking Queuelable to do callout and updating appropriate Unit with external Id and URL
				RightMoveAsyncExecutionSendRequest request = new RightMoveAsyncExecutionSendRequest(
						aggregator.Method__c,
						aggregator.URL__c,
						new Map<String, String>{'Content-Type'=>'application/json; charset=utf-8'},
						jsonByUnitId.get(unit.Id),
						aggregator.Certificate__c,
						unit);
				if (!Test.isRunningTest()) {
					System.enqueueJob(request);
				}
			}
		}
		return responsesByUnitId;
	}

	/*public Map<Id, String> processRemoveRequest(List<Unit__c> units, Aggregator_Setting__mdt aggregator) {
		Map<Id, String> responsesByUnitId = new Map<Id, String>();
		return responsesByUnitId;
	}*/
}