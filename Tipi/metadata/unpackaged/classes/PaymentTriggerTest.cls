/**
 * @Name:        PaymentTriggerTest
 * @Description: This is Test class to cover the PaymentTriggerHandler
 *
 * @author:      Andrey Korobovich
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 14/05/2018    Andrey Korobovich    Created Class
 * 21/06/2018	 Patrick Fischer	  Added coverage for processBeforeInsert() context including setAspPaymentsCustomerId() method
 */
@IsTest
public with sharing class PaymentTriggerTest {

	@TestSetup
	public static void testSetup() {
		List<Account> accs = TestFactoryPayment.addAccounts(1, true);
		List<Contact> conts = TestFactoryPayment.addContacts(accs, 1, true);
		Opportunity opp = new Opportunity(
        	Name = 'Test Opportunity',
            AccountId = accs[0].Id,
            Primary_Contact__c = conts[0].Id,
            Type = '1 Bed',
            CloseDate = Date.newInstance(2018, 4, 7),
            StageName = 'Interested'
        ); 
        insert opp;
		Unit__c unit = [SELECT Id FROM Unit__c LIMIT 1];
		Development__c development = [SELECT Id, Name FROM Development__c LIMIT 1];
		Tipi_Payments__c newPayment = new Tipi_Payments__c();
		newPayment.Development__c = development.Id;
		newPayment.Contact__c = conts[0].Id;
		newPayment.Unit__c = unit.Id;
		newPayment.Amount__c = 500;
        newPayment.Status__c = 'Awaiting Submission';
        newPayment.Type__c = 'Deposit';
        newPayment.Opportunity__c = opp.Id;
        insert newPayment;

		Asperato_Customer_Id__c aspCustomerId = new Asperato_Customer_Id__c(
				Name = development.Name,
				Development_Id__c = development.Id,
				Customer_Id__c = '1234'
		);
		insert aspCustomerId;
    }

    private static testMethod void paymentTriggerTest() {
    	Tipi_Payments__c tipipayment = [SELECT  Id, Amount__c, Contact__r.Account.Name, Contact__r.Email, Contact__r.FirstName, Contact__r.LastName FROM Tipi_Payments__c LIMIT 1];
    	asp04__Payment__c payment = new asp04__Payment__c();
		payment.Contact__c = tipipayment.Contact__c;
		payment.asp04__Amount__c = tipipayment.Amount__c;
		payment.asp04__Account_Name__c = tipipayment.Contact__r.Account.Name;
		payment.asp04__Billing_Address_Country__c = 'Test county';
		payment.asp04__Billing_Address_City__c = 'Test city';
		payment.asp04__Billing_Address_PostalCode__c = 'Test postcode';
		payment.asp04__Billing_Address_Street__c = 'Test street';
		payment.asp04__Payment_Stage__c = 'Awaiting submission';
		payment.asp04__Email__c = tipipayment.Contact__r.Email;
		payment.asp04__First_Name__c = tipipayment.Contact__r.FirstName;
		payment.asp04__Last_Name__c = tipipayment.Contact__r.LastName;
		payment.asp04__Payment_Route_Options__c = 'Card';
		System.assertEquals(null, payment.asp04__Customer_ID__c, 'Expecting Asperato Customer Id to be empty');
		PaymentTriggerHelper.processBeforeInsert(new List<asp04__Payment__c> { payment } );
		System.assertEquals('1234', payment.asp04__Customer_ID__c, 'Expecting Asperato Customer Id to be populated');
		insert payment;
		tipipayment.Payment__c = payment.Id;
		update tipipayment;
		Map<Id, asp04__Payment__c> oldPaymentMap = new Map<Id, asp04__Payment__c> {payment.Id => payment};
		PaymentTriggerHelper.processAfterInsert(oldPaymentMap);
		payment.asp04__Payment_Stage__c = 'Collected from customer';
		Map<Id, asp04__Payment__c> newPaymentMap = new Map<Id, asp04__Payment__c> {payment.Id => payment};
		PaymentTriggerHelper.processAfterUpdate(oldPaymentMap, newPaymentMap);
		update payment;
		PaymentTriggerHelper.processBeforeDelete(newPaymentMap);
		PaymentTriggerHelper.processAfterUnDelete(newPaymentMap);
		Opportunity opp = [SELECT Id, Deposit_Amount_Paid__c FROM Opportunity LIMIT 1];
		System.assertEquals(opp.Deposit_Amount_Paid__c, 1000);
	}
}