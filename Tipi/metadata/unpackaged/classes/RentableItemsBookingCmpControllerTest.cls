/**
    * @Name:        CalendarCmpControllerTest
    * @Description: Test class for CalendarCmpControllerT
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 10/05/2018    Eugene Konstantinov    Created Class
*/
@isTest
public with sharing class RentableItemsBookingCmpControllerTest {

    @isTest
    static void testRentableItemsBookingCmpAsAdmin() {
        Test.startTest();
            System.assert(0 == RentableItemsBookingCmpController.getRentableItemsForContact().size());
            System.assert(0 == RentableItemsBookingCmpController.getSocialSpacesForContact().size());
        Test.stopTest();
    }

    @isTest
    static void testRentableItemsBookingCmpAsCommunityUser() {
        User portalUser = TestFactoryPayment.createPortallUser();
        System.runAs(portalUser) {
            Test.startTest();
                System.assert(0 == RentableItemsBookingCmpController.getRentableItemsForContact().size());
                System.assert(0 == RentableItemsBookingCmpController.getSocialSpacesForContact().size());
            Test.stopTest();
        }
    }

}