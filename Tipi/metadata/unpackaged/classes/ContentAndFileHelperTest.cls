/**
 * Created by jaredwatson on 23/03/2018.
 */

@IsTest
private class ContentAndFileHelperTest {


    static testMethod void testUpdateLatestPublishedImageVersionVoid() {

        //must set the custom settings flags to run all automation items
        Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);

        ContentVersion contentVersion = new ContentVersion(
                Title = 'Penguins',
                PathOnClient = 'Penguins.jpg',
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true
        );
        insert contentVersion;

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        Account acc = new Account();
        acc.Name = 'Test';
        insert acc;

        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = acc.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;


        //create a set pf site/building/unit and unit file
        /*Development_Site__c devSite = (Development_Site__c) TestFactory.createSObject(new Development_Site__c(), true);
        Building__c building = (Building__c) TestFactory.createSObject(new Building__c(Development_Site__c = devSite.Id), true);
        Unit__c unit = (Unit__c) TestFactory.createSObject(new Unit__c(Building__c = building.Id), true);
        Unit_File__c unitFile = (Unit_File__c) TestFactory.createSObject(new Unit_File__c(Unit__c = unit.Id), true);

        Test.startTest();
        //now create the content docs to test the trigger and helper
        ContentVersion contentVersion = new ContentVersion(
                Title = 'Penguins',
                PathOnClient = 'Penguins.jpg',
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true
        );
        insert contentVersion;

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = unitFile.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        Test.stopTest();

        //fetch unit file and assert content Version ID os populated
        Unit_File__c updUnitFile = [select id, Latest_Image_Version_ID__c from Unit_File__c where id = :unitFile.Id];
        System.assertNotEquals(null, updUnitFile.Latest_Image_Version_ID__c);*/


    }


}