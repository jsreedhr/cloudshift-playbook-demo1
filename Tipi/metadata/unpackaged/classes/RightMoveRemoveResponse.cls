/**
    * @Name:        RightMoveRemoveResponse
    * @Description: Wrapper class for creating remove response structure from RightMove
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 29/05/2018    Andrey Korobovich    Created Class
*/

public with sharing class RightMoveRemoveResponse {
	public String message {get; set;}
    public Boolean success {get; set;}

    public class Property {
        String agent_ref {get; set;}
        Integer rightmove_id {get; set;}
        String change_type {get; set;}
        public Property(String agent_ref, Integer rightmove_id, String change_type){
            this.agent_ref = agent_ref;
            this.rightmove_id = rightmove_id;
            this.change_type = change_type;
        }
    }
}