/**
 * @Name:        OpportunityDepositDeductionsCtrlTest
 * @Description: Test class for OpportunityDepositDeductionsController
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 14/05/2018    Patrick Fischer    Created Class
 */
@IsTest
public with sharing class OpportunityDepositDeductionsCtrlTest {

    /**
     * Method tested: initialiseComponent()
     * Expected result: Exception handled and no Payments returned
     */
    private static testMethod void initialiseComponentFailure() {
        Test.startTest();
        List<Tipi_Payments__c> tipiPayments = OpportunityDepositDeductionsController.initialiseComponent(null);
        Test.stopTest();

        System.assert(tipiPayments.isEmpty(), 'No Tipi Payments returned');
    }
}