/**
 * @Name:        PaymentSummaryCmpController
 * @Description: Controller class for Payment Summary Lightning Component
 *
 * @author:      Eugene Konstantinov
 * @version:     1.0
 * Change Log
 *
 * Date          author                 Change Description
 * -----------------------------------------------------------------------------------
 * 06/06/2018    Eugene Konstantinov    Created Class
 * 15/06/2018    Patrick Fischer        Refactored Class, introduced PaymentRowWrapper, excluded Tipi_Payments__c charges
 * 16/06/2018    Patrick Fischer        Added functionality to handling positive/negative charges, handling Refunds
 */
public with sharing class PaymentSummaryCmpController {

    private static String PAYMENT_CURRENY = 'GBP';

    /**
     * To retrieve a Contact records for the current User for RentableItemsBooking Lightning Component
     *
     * @return Contact current contact or null if user has not Contact
     */
    @AuraEnabled
    public static Contact getCurrentContact() {
        User currentUser = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if (currentUser != null && currentUser.ContactId != null) {
            List<Contact> currentContact = [
                SELECT Id, Name, Email, Current_Lease__c, Leased_Reserved_Unit__c, Leased_Reserved_Unit__r.Building__r.Development_Site__c, Current_Lease__r.SBQQ__Opportunity__c
                FROM Contact
                WHERE
                    Id = :currentUser.ContactId
                LIMIT 1];
            if (currentContact != null && !currentContact.isEmpty()) {
                return currentContact[0];
            }
        }

        return null;
    }

    /**
     * To retrieve payments for a List of Contact Ids
     *
     * @param contactIdList List of Contact Ids (passed as a serialised String)
     *
     * @return List of PaymentRowWrapper records for all Contacts
     */
    @AuraEnabled
    public static List<PaymentRowWrapper> getPaymentsForContacts(String contactIdList) {
        Set<Id> contactIds = new Set<Id>();

        try {
            contactIds = (Set<Id>) System.JSON.deserialize(contactIdList, Set<Id>.class);
        } catch (Exception e) {
            // continue
        }

        return getPaymentRows(contactIds);
    }

    /**
     * To retrieve payments for a single Contact Ids
     *
     * @param contactId Contact Ids
     *
     * @return List of PaymentRowWrapper records for a single Contact
     */
    @AuraEnabled
    public static List<PaymentRowWrapper> getPaymentsForContact(String contactId) {
        Set<Id> contactIds = new Set<Id>();

        try {
            contactIds.add(Id.valueOf(contactId));
        } catch(Exception e) {
            Contact currentContact = getCurrentContact();
            if (currentContact != null) {
                contactIds.add(currentContact.Id);
            }
        }

        return getPaymentRows(contactIds);
    }

    /**
     * To construct all Asperato Payment records as 'payment' Amounts
     * To construct selected Tipi_Payments__c records (that have a Payment_Date__c in the past/today)
     * and to separating Tipi_Payments__c records into negative-Concession-'Charge' & Full-Rent-'Charge' rows
     *
     * [ Tipi Payment Credit Charges are excluded deliberately. Only Credit Payments shall show on a Balance Sheet. ]
     *
     * @param contactIds Set of Contact Ids for which to retrieve Payment Rows
     *
     * @return List of PaymentRowWrapper records
     */
    @TestVisible
    private static List<PaymentRowWrapper> getPaymentRows(Set<Id> contactIds) {
        List<PaymentRowWrapper> paymentRowWrapperList = new List<PaymentRowWrapper>();

        List<asp04__Payment__c> aspPayments = [
                SELECT Id, Name, CreatedDate, asp04__Amount__c, asp04__Payment_Date__c, asp04__Payment_Stage__c,
                        asp04__Asperato_Reference__c, asp04__Payment_Route_Selected__c, asp04__Total_Refunded__c,
                        Contact__c, Contact__r.Name,
                (
                        SELECT Id, Name, CreatedDate, asp04__Amount__c, asp04__Payment_Date__c, asp04__Refund_Stage__c,
                                asp04__Asperato_Reference__c, asp04__Payment_Route__c
                        FROM asp04__Refunds__r
                )
                FROM asp04__Payment__c
                WHERE Contact__c IN :contactIds AND asp04__Payment_Stage__c = :Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER
                ORDER BY asp04__Payment_Date__c ASC
                LIMIT 10000
        ];
        paymentRowWrapperList.addAll(getAspPaymentRows(aspPayments));

        List<Tipi_Payments__c> tipiPayments = [
                SELECT Id, Name, CreatedDate, Amount__c, Amount_without_Concession__c, Calculation_Long__c, Concession_Amount__c,
                        Contact__c, Contact__r.Name, Credit_Payment_Type__c, Nth_Month_Payment__c,
                        Payment__c, Payment_Date__c, Payment_Success_Date_Time__c, Payment_Gateway__c, Status__c,
                        RecordType.DeveloperName, Tipi_Payment__c, Tipi_Payment__r.Credit_Payment_Type__c,
                        Tipi_Payment__r.Type__c, Total_Amount__c, Total_Remaining_Balance__c, Type__c
                FROM Tipi_Payments__c
                WHERE Contact__c IN :contactIds AND Payment_Date__c <= TODAY AND Status__c != :Utils_Constants.CANCELLED_STATUS
                ORDER BY Payment_Date__c ASC
                LIMIT 10000
        ];
        paymentRowWrapperList.addAll(getTipiPaymentRows(tipiPayments));

        // primary sort by payment date, secondary sort by charge vs payment amount
        // sorting logic implemented by compareTo() in the PaymentRowWrapper class using the Comparable Interface
        paymentRowWrapperList.sort();

        // set Balance
        paymentRowWrapperList = setBalance(paymentRowWrapperList);

        return paymentRowWrapperList;
    }

    /**
     * To parse a List of asp04__Payment__c records into PaymentRowWrapper
     *
     * @param aspPayments List of asp04__Payment__c records
     *
     * @return List of PaymentRowWrapper records
     */
    private static List<PaymentRowWrapper> getAspPaymentRows(List<asp04__Payment__c> aspPayments) {
        List<PaymentRowWrapper> paymentRowWrapperList = new List<PaymentRowWrapper>();

        for(asp04__Payment__c aspPayment : aspPayments) {
            String description = 'Payment via ' + aspPayment.asp04__Payment_Route_Selected__c;
            paymentRowWrapperList.add(new PaymentRowWrapper(aspPayment.Id, aspPayment.Name, aspPayment.Contact__c,
                    aspPayment.Contact__r.Name, aspPayment.CreatedDate, aspPayment.asp04__Payment_Date__c, description,
                    aspPayment.asp04__Payment_Stage__c, description + ', Ref: ' + aspPayment.asp04__Asperato_Reference__c,
                    0, aspPayment.asp04__Amount__c));
            if(aspPayment.asp04__Total_Refunded__c != null && aspPayment.asp04__Total_Refunded__c > 0 && !aspPayment.asp04__Refunds__r.isEmpty()) {
                for(asp04__Refund__c refund : aspPayment.asp04__Refunds__r) {
                    if(refund.asp04__Amount__c > 0) {
                        description = 'Refund Payment via ' + refund.asp04__Payment_Route__c;
                        paymentRowWrapperList.add(new PaymentRowWrapper(refund.Id, refund.Name, aspPayment.Contact__c,
                                aspPayment.Contact__r.Name, refund.CreatedDate, refund.asp04__Payment_Date__c, description,
                                refund.asp04__Refund_Stage__c, description + ', Ref: ' + refund.asp04__Asperato_Reference__c,
                                0, -(refund.asp04__Amount__c)));
                    }
                }
            }
        }

        return paymentRowWrapperList;
    }

    /**
     * To parse a List of Tipi_Payments__c records into PaymentRowWrapper
     *
     * @param tipiPayments List of Tipi_Payments__c records
     *
     * @return List of PaymentRowWrapper records
     */
    private static List<PaymentRowWrapper> getTipiPaymentRows(List<Tipi_Payments__c> tipiPayments) {
        List<PaymentRowWrapper> paymentRowWrapperList = new List<PaymentRowWrapper>();

        for (Tipi_Payments__c tipiPayment : tipiPayments) {
            // if REFUND
            if (tipiPayment.RecordType.DeveloperName == Utils_Constants.PAYMENT_RECORDTYPE_REFUND) {
                // Refund is held as negative Amount value, i.e. -20.00
                paymentRowWrapperList.add(new PaymentRowWrapper(tipiPayment, tipiPayment.Amount__c, 0));
            }
            // else NOT REFUND
            else {
                // if CREDIT
                if (tipiPayment.Type__c != Utils_Constants.PAYMENT_TYPE_RESERVATION &&
                        tipiPayment.Credit_Payment_Type__c == Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT) {
                    /**
                     *  i)   Do not add charges for any type of Credit records. Credit records are always 'Payment' rather than 'Charges'.
                     *  ii)  Do not add rows for 'Direct Debit' & 'SagePay' as these Payments are populated with the queried Asperato records.
                     *  iii) Add 'Other' (BACS) Tipi_Payment__c record as 'Payment' Amount as it's not reflected in Asperato Table.
                     */
                    if (tipiPayment.Payment_Gateway__c == Utils_Constants.PAYMENT_GATEWAY_OTHER && tipiPayment.Status__c == Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER) {
                        paymentRowWrapperList.add(new PaymentRowWrapper(tipiPayment, 0, tipiPayment.Amount__c));
                    }
                }
                // if RESERVATION
                else if (tipiPayment.Type__c == Utils_Constants.PAYMENT_TYPE_RESERVATION) {
                    paymentRowWrapperList.add(new PaymentRowWrapper(tipiPayment, tipiPayment.Amount__c, 0));
                }
                // if DEPOSIT
                else if (tipiPayment.Type__c == Utils_Constants.PAYMENT_TYPE_DEPOSIT) {
                    paymentRowWrapperList.add(new PaymentRowWrapper(tipiPayment, tipiPayment.Amount__c, 0));
                }
                // if RENT / PARKING / STORAGE / SOCIAL SPACE
                else if (Utils_Constants.TIPI_PAYMENT_TYPES.contains(tipiPayment.Type__c)) {

                    // work out Amount & concession (if included)
                    if (Utils_Constants.TIPI_RENT_PAYMENT_TYPES.contains(tipiPayment.Type__c) ||
                            tipiPayment.Type__c == Utils_Constants.PAYMENT_TYPE_PARKING ||
                            tipiPayment.Type__c == Utils_Constants.PAYMENT_TYPE_STORAGE) {

                        if (tipiPayment.Tipi_Payment__c != null) {
                            if (tipiPayment.Tipi_Payment__r.Type__c == Utils_Constants.PAYMENT_TYPE_RESERVATION) {
                                // if parent of tipiPayment record is a Reservation payment add negative Charge
                                PaymentRowWrapper reservationDrawDown = new PaymentRowWrapper(tipiPayment, -(tipiPayment.Amount__c), 0);
                                reservationDrawDown.paymentDescription = 'Reservation Deposit Refund';
                                paymentRowWrapperList.add(reservationDrawDown);
                            }
                        }
                        // if concession has been applied
                        if (tipiPayment.Concession_Amount__c != null && tipiPayment.Concession_Amount__c != 0) {
                            PaymentRowWrapper concessionRow = new PaymentRowWrapper(tipiPayment, -(tipiPayment.Concession_Amount__c), 0);
                            concessionRow.paymentDescription = 'Concession applied for ' + concessionRow.paymentDescription;
                            paymentRowWrapperList.add(concessionRow);

                            PaymentRowWrapper paymentRow = new PaymentRowWrapper(tipiPayment, (tipiPayment.Amount__c + tipiPayment.Concession_Amount__c), 0);
                            paymentRow.paymentDescription = 'Payment charge for ' + paymentRow.paymentDescription;
                            paymentRowWrapperList.add(paymentRow);
                        } else {
                            paymentRowWrapperList.add(new PaymentRowWrapper(tipiPayment, tipiPayment.Amount__c, 0));
                        }
                    }
                    // don't worry about concession
                    else {
                        paymentRowWrapperList.add(new PaymentRowWrapper(tipiPayment, tipiPayment.Amount__c, 0));
                    }
                }
            }
        }

        return paymentRowWrapperList;
    }

    /**
     * To set the balance on a list of (previously) ordered PaymentRowWrapper records
     *
     * @param paymentRowWrappers List of PaymentRowWrapper records
     *
     * @return List of PaymentRowWrapper records
     */
    private static List<PaymentRowWrapper> setBalance(List<PaymentRowWrapper> paymentRowWrappers) {

        Decimal currentBalance = 0;

        for(PaymentRowWrapper paymentRowWrapper : paymentRowWrappers) {
            currentBalance += paymentRowWrapper.balance;
            paymentRowWrapper.balance = currentBalance;
        }

        return paymentRowWrappers;
    }

    /**
     * Inner class PaymentRowWrapper to represent a row in the c:paymentSummary LEX component
     */
    public class PaymentRowWrapper implements Comparable {
        @AuraEnabled public String paymentRecordId { get; private set; }
        @AuraEnabled public String paymentRecordName { get; private set; }
        @AuraEnabled public String contactId { get; private set; }
        @AuraEnabled public String contactName { get; private set; }
        @AuraEnabled public Datetime createdDateTime { get; set; }
        @AuraEnabled public Date paymentDate { get; set; }
        @AuraEnabled public String paymentType { get; private set; }
        @AuraEnabled public String paymentStatus { get; private set; }
        @AuraEnabled public String paymentDescription { get; private set; }
        @AuraEnabled public String paymentCurrency { get; private set; }
        @AuraEnabled public Decimal charge { get; set; }
        @AuraEnabled public Decimal payment { get; set; }
        @AuraEnabled public Decimal balance { get; private set; }

        public PaymentRowWrapper(String paymentRecordId, String paymentRecordName, String contactId, String contactName, Datetime createdDateTime,
                Date paymentDate, String paymentType, String paymentStatus, String paymentDescription, Decimal charge, Decimal payment) {
            this.paymentRecordId = paymentRecordId;
            this.paymentRecordName = paymentRecordName;
            this.contactId = contactId;
            this.contactName = contactName;
            this.createdDateTime = createdDateTime;
            this.paymentDate = paymentDate;
            this.paymentType = paymentType;
            this.paymentStatus = paymentStatus;
            this.paymentDescription = paymentDescription;
            this.paymentCurrency = PAYMENT_CURRENY;
            this.charge = charge;
            this.payment = payment;
            this.balance = charge - payment;
        }

        public PaymentRowWrapper(Tipi_Payments__c tipiPayment, Decimal charge, Decimal payment) {
            this.paymentRecordId = tipiPayment.Id;
            this.paymentRecordName = tipiPayment.Name;
            this.contactId = tipiPayment.Contact__c;
            this.contactName = tipiPayment.Contact__r.Name;
            this.createdDateTime = tipiPayment.CreatedDate;
            this.paymentDate = tipiPayment.Payment_Date__c;
            this.paymentType = tipiPayment.Type__c;
            this.paymentStatus = tipiPayment.Status__c;
            this.paymentDescription = tipiPayment.Calculation_Long__c;
            this.paymentCurrency = PAYMENT_CURRENY;
            this.charge = charge;
            this.payment = payment;
            this.balance = charge - payment;
        }

        /**
         * Comparing algorithm being used during List.sort() operation on PaymentRowWrapper objects
         *
         * @param compareTo Object of type PaymentRowWrapper
         *
         * @return Integer value, 1 if greater, 0 if equal, -1 if smaller
         */
        public Integer compareTo(Object compareTo) {
            // Cast argument to OpportunityWrapper
            PaymentRowWrapper compareToWrapper = (PaymentRowWrapper) compareTo;

            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
 
            // compare for date
            if (paymentDate > compareToWrapper.paymentDate) {
                returnValue = 1;
            } else if (paymentDate < compareToWrapper.paymentDate) {
                returnValue = -1;
            } else if(paymentDate == compareToWrapper.paymentDate) {
                if (charge == 0 && compareToWrapper.charge != 0) {
                    returnValue = 1;
                } else if (payment == 0 && compareToWrapper.payment != 0) {
                    returnValue = -1;
                } else if(createdDateTime > compareToWrapper.createdDateTime) {
                    returnValue = 1;
                } else if(createdDateTime < compareToWrapper.createdDateTime) {
                    returnValue = -1;
                }
            }

            return returnValue;
        }
    }
}