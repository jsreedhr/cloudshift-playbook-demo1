/**
    * @Name:        ZooplaAsyncExecutionSendRequest
    * @Description: Class to create Queueable callout to Zoopla
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 04/05/2018    Andrey Korobovich    Created Class
*/

public class ZooplaAsyncExecutionSendRequest implements Queueable, Database.AllowsCallouts {
	
	private static final String CLASS_NAME = 'ZooplaAsyncExecutionSendRequest';
	private static final String SUCCESS_RESPONSE = 'OK';
	private String method;
	private String url;
	private Map<String, String> additionalHeader;
	private String body;
	private String certificate;
	private Unit__c unit;

	public ZooplaAsyncExecutionSendRequest(String method, String url, Map<String, String> additionalHeader, String body, String certificate, Unit__c unit) {
		this.method = method;
		this.url = url;
		this.additionalHeader = additionalHeader;
		this.body = body;
		this.certificate = certificate;
		this.unit = unit;
	}



	public void execute(QueueableContext context) {
        HTTPResponse response = sendRequest(this.method, this.url, this.additionalHeader, this.body, this.certificate);
        if (response != null) {
            String responseString = response.getBody();
            System.debug(responseString);
            ZooplaSendPropertyResponseStructure parsedResponse = ZooplaSendPropertyResponseStructure.parse(responseString);
            if (response.getStatus() == SUCCESS_RESPONSE && parsedResponse.status == SUCCESS_RESPONSE) {
                List<Unit__c> unitToUpdate = (List<Unit__c>) new UnitSelector().getRecordsByIds(new Set<Id>{this.unit.Id},new Set<String>{
                        Unit__c.Name.getDescribe().getName()
                        });
                if (unitToUpdate != null && !unitToUpdate.isEmpty()) {
                    unitToUpdate[0].Zoopla_URL__c = parsedResponse.url;
                    update unitToUpdate;
                }
    
            }
        }
	}

	/**
	 * Send Request Method
	 * @param  method  string value
	 * @param  url  string value of endpoint
	 * @param  additionalHeader  map of additional header information
	 * @param  body  string value
	 * @param  certificate  string name of cetificate
	 *
	 * @returns HTTPResponse response from endpoint
	 */
	private HTTPResponse sendRequest(String method, String url, Map<String, String> additionalHeader, String body, String certificate){
		try {
			HttpRequest Request = new HttpRequest();
			Request.setMethod(method);
			Request.setEndpoint(url);
			Request.setTimeout(120000);

			if (!String.isEmpty(certificate)) {
				Request.setClientCertificateName(certificate);
			}

			for (String key : additionalHeader.keySet()) {
				Request.setHeader(key, (String)additionalHeader.get(key));
			}

			if (!String.isEmpty(body)) {
				Request.setBody(body);
			}
			System.debug(body);

			Http http = new Http();
			HTTPResponse response = http.send(Request);
			System.debug(response.getBody());
			return response;
		} catch (CalloutException ex) {
			CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'Callout Exception:' + CLASS_NAME + ex.getMessage());
		} catch (Exception ex) {
			CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'Exception:' + CLASS_NAME + ex.getMessage());
		} finally {
			CustomLogger.save();
		}
		return null;
	}
}