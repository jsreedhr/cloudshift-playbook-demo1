/**
 * @Name:        TipiPaymentTriggerHandler
 * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
 *
 * @author:      Eugene Konstantinov
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 24/05/2018    Eugene Konstantinov    Created Class
 * 30/05/2018    Patrick Fischer        Added afterDelete operation
 */

public with sharing class TipiPaymentTriggerHandler extends TriggerHandler {

    protected override void beforeInsert(){
        TipiPaymentTriggerHelper.processBeforeInsert((List<Tipi_Payments__c>) Trigger.new);
    }

    protected override void afterInsert(){
        TipiPaymentTriggerHelper.processAfterInsert(Trigger.newMap);
    }

    protected override void beforeUpdate(){ 
        TipiPaymentTriggerHelper.processBeforeUpdate((List<Tipi_Payments__c>) Trigger.new, (Map<Id, Tipi_Payments__c>) Trigger.oldMap);
    }

    protected override void afterUpdate(){
        TipiPaymentTriggerHelper.processAfterUpdate(Trigger.oldMap, Trigger.newMap);
    }

    protected override void beforeDelete(){
        TipiPaymentTriggerHelper.processBeforeDelete(Trigger.oldMap);
    }

    protected override void afterDelete(){
        TipiPaymentTriggerHelper.processAfterDelete(Trigger.oldMap);
    }

    protected override void afterUndelete(){
        PaymentTriggerHelper.processAfterUnDelete(Trigger.newMap);
    } 
}