/**
 * @Name:        PaymentTriggerHelper
 * @Description: This is helper class, will hold all methods and functionalities that are to be build for Payment Trigger
 *
 * @author:      Andrey Korobovich
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 14/05/2018    Andrey Korobovich    Created Class
 * 21/06/2018	 Patrick Fischer	  Added processBeforeInsert() context including setAspPaymentsCustomerId() method
 */
public with sharing class PaymentTriggerHelper {

	private static final String PAID_STATUS = 'Collected from customer';
	private static final String DEPOSIT_TYPE = 'Deposit';
	private static final String STAGE_FIELD = 'asp04__Payment_Stage__c';
	private static final String AMOUNT_FIELD = 'asp04__Amount__c';

	/**
	 * Method for processing inserted Payment records
	 * @param  new  List of of inserted Payment records
	 */
	public static void processBeforeInsert(List<SObject> newList) {
		List<asp04__Payment__c> aspPayments = (List<asp04__Payment__c>) newList;

		aspPayments = setAspPaymentsCustomerId(aspPayments);
	}

	/**
	 * Method for processing inserted Payment records
	 * @param  newMap  map of inserted Payment records
	 */
	public static void processAfterInsert(Map<Id, sObject> newMap) {
		List<Opportunity> listForUpdate = new List<Opportunity>();
		Set<Id> paymentIdsForIncrement = new Set<Id>();
		for (Id paymentId : newMap.keySet()) {
			if (newMap.get(paymentId).get(STAGE_FIELD) == PAID_STATUS) {
				paymentIdsForIncrement.add(paymentId);
			}
		}
		if(!paymentIdsForIncrement.isEmpty()){
			listForUpdate.addAll(oppDepositAmountFieldUpdate(paymentIdsForIncrement, true));
		}
		update listForUpdate;
	}


	public static void processAfterUpdate(Map<Id, sObject> oldMap, Map<Id, sObject> newMap) {
		List<Opportunity> listForUpdate = new List<Opportunity>();
		Set<Id> paymentIdsForIncrement = new Set<Id>();
		Set<Id> paymentIdsForDecrement = new Set<Id>();
		Set<Id> paymentAmountUpdate = new Set<Id>();

		for (Id paymentId : newMap.keySet()) {
			if (newMap.get(paymentId).get(STAGE_FIELD) == PAID_STATUS  && oldMap.get(paymentId).get(STAGE_FIELD) != PAID_STATUS) {
				paymentIdsForIncrement.add(paymentId);
			}  else if (newMap.get(paymentId).get(STAGE_FIELD) != PAID_STATUS && oldMap.get(paymentId).get(STAGE_FIELD) == PAID_STATUS) {
				paymentIdsForDecrement.add(paymentId);
			} else if(newMap.get(paymentId).get(AMOUNT_FIELD) != oldMap.get(paymentId).get(AMOUNT_FIELD) && newMap.get(paymentId).get(STAGE_FIELD) == PAID_STATUS) {
				paymentAmountUpdate.add(paymentId);
			}
		}
		if(!paymentIdsForIncrement.isEmpty()){
			listForUpdate.addAll(oppDepositAmountFieldUpdate(paymentIdsForIncrement, true));
		}
		if(!paymentIdsForDecrement.isEmpty()){
			listForUpdate.addAll(oppDepositAmountFieldUpdate(paymentIdsForDecrement, false));
		}
		if(!paymentAmountUpdate.isEmpty()){
			listForUpdate.addAll(updateOpportunityAmount(paymentAmountUpdate,oldMap,newMap));
		}
		update listForUpdate;

        Map<Id, asp04__Payment__c> statusChanged = new Map<Id, asp04__Payment__c>();
        for (Id paymentId : newMap.keySet()) {
            if (newMap.get(paymentId).get(STAGE_FIELD) != oldMap.get(paymentId).get(STAGE_FIELD)) {
                statusChanged.put(paymentId, (asp04__Payment__c)newMap.get(paymentId));
            }
        }
        if (statusChanged != null && !statusChanged.isEmpty()) {
            updateTipiPaymentStatus(statusChanged);
        }
	}

	public static void processBeforeDelete(Map<Id, sObject> oldMap){
		List<Opportunity> listForUpdate = new List<Opportunity>();
		Set<Id> paymentIdsForDecrement = new Set<Id>();
		for (Id paymentId : oldMap.keySet()) {
			if (oldMap.get(paymentId).get(STAGE_FIELD) == PAID_STATUS) {
				paymentIdsForDecrement.add(paymentId);
			}
		}
		System.debug(paymentIdsForDecrement);
		if(!paymentIdsForDecrement.isEmpty()){
			listForUpdate.addAll(oppDepositAmountFieldUpdate(paymentIdsForDecrement, false));
		}
		System.debug(listForUpdate);
		update listForUpdate;
	}

	public static void processAfterUnDelete(Map<Id, sObject> newMap) {
		List<Opportunity> listForUpdate = new List<Opportunity>();
		Set<Id> paymentIdsForIncremnt = new Set<Id>();
		for (Id paymentId : newMap.keySet()) {
			if (newMap.get(paymentId).get(STAGE_FIELD) == PAID_STATUS) {
				paymentIdsForIncremnt.add(paymentId);
			}
		}
		if(!paymentIdsForIncremnt.isEmpty()){
			listForUpdate.addAll(oppDepositAmountFieldUpdate(paymentIdsForIncremnt, true));
		}
		System.debug(listForUpdate);
		update listForUpdate;
	}


	private static List<Opportunity> oppDepositAmountFieldUpdate (Set<Id> paymentsIds, Boolean isIncrement) {
		List<Opportunity> listForUpdate = new List<Opportunity>();
		List<Tipi_Payments__c> tipiPayments = [SELECT Id, Opportunity__c, Payment__c, Payment__r.asp04__Amount__c, Payment__r.asp04__Payment_Stage__c, Opportunity__r.Deposit_Amount_Paid__c, Opportunity__r.Deposit_Paid__c, Opportunity__r.Deposit__c
		                                       FROM Tipi_Payments__c
		                                       WHERE Payment__c IN : paymentsIds
		                                       AND Type__c = : DEPOSIT_TYPE
		                                       LIMIT 10000 ];
		for (Tipi_Payments__c payment : tipiPayments) {
			if (isIncrement) {
				payment.Opportunity__r.Deposit_Amount_Paid__c = payment.Opportunity__r.Deposit_Amount_Paid__c != null ? payment.Opportunity__r.Deposit_Amount_Paid__c + payment.Payment__r.asp04__Amount__c : payment.Payment__r.asp04__Amount__c;
				if (payment.Opportunity__r.Deposit__c <= payment.Opportunity__r.Deposit_Amount_Paid__c) {
					payment.Opportunity__r.Deposit_Paid__c = true;
				}
			} else {
				payment.Opportunity__r.Deposit_Amount_Paid__c = payment.Opportunity__r.Deposit_Amount_Paid__c != null ? payment.Opportunity__r.Deposit_Amount_Paid__c - payment.Payment__r.asp04__Amount__c : payment.Payment__r.asp04__Amount__c;
				if (payment.Opportunity__r.Deposit__c >= payment.Opportunity__r.Deposit_Amount_Paid__c) {
					payment.Opportunity__r.Deposit_Paid__c = false;
				}
			}
			listForUpdate.add(payment.Opportunity__r);
		}
		return listForUpdate;
	}

	private static List<Opportunity> updateOpportunityAmount(Set<Id> paymentsIds, Map<Id, sObject> oldMap, Map<Id, sObject> newMap){
		List<Opportunity> listForUpdate = new List<Opportunity>();
		Map<Id, Tipi_Payments__c> tipiPaymentsMap = new Map<Id, Tipi_Payments__c>();
		for(Tipi_Payments__c payment : [SELECT Id, Opportunity__c, Payment__c, Payment__r.asp04__Amount__c, Payment__r.asp04__Payment_Stage__c, Opportunity__r.Deposit_Amount_Paid__c, Opportunity__r.Deposit_Paid__c, Opportunity__r.Deposit__c
		                                       FROM Tipi_Payments__c
		                                       WHERE Payment__c IN : paymentsIds
		                                       LIMIT 10000 ]) {
			tipiPaymentsMap.put(payment.Payment__c , payment);
		}
		for (Id paymentId : tipiPaymentsMap.keySet()) {
			Double oldValue = Double.valueOf(oldMap.get(paymentId).get(AMOUNT_FIELD));
			Double newValue = Double.valueOf(newMap.get(paymentId).get(AMOUNT_FIELD));
			Double currValue = tipiPaymentsMap.get(paymentId).Opportunity__r.Deposit_Amount_Paid__c;
			tipiPaymentsMap.get(paymentId).Opportunity__r.Deposit_Amount_Paid__c = currValue - oldValue + newValue;
			if (tipiPaymentsMap.get(paymentId).Opportunity__r.Deposit__c >= tipiPaymentsMap.get(paymentId).Opportunity__r.Deposit_Amount_Paid__c) {
				tipiPaymentsMap.get(paymentId).Opportunity__r.Deposit_Paid__c = false;
			} else {
				tipiPaymentsMap.get(paymentId).Opportunity__r.Deposit_Paid__c = true;
			}
			listForUpdate.add(tipiPaymentsMap.get(paymentId).Opportunity__r);
		}
		return listForUpdate;
	}

    private static void updateTipiPaymentStatus(Map<Id, asp04__Payment__c> newMap) {
        List<Tipi_Payments__c> tipiPayments = [
            SELECT Id, Payment__c, Payment__r.asp04__Payment_Stage__c, Status__c
            FROM Tipi_Payments__c
            WHERE
                Payment__c IN :newMap.keySet()
            LIMIT 10000
        ];
        for (Tipi_Payments__c payment : tipiPayments) {
            if (newMap.containsKey(payment.Payment__c)) {
                payment.Status__c = newMap.get(payment.Payment__c).asp04__Payment_Stage__c;
            }
        }
        update tipiPayments;
    }

	private static List<asp04__Payment__c> setAspPaymentsCustomerId(List<asp04__Payment__c> aspPayments) {
		Set<Id> relatedContactIds = new Set<Id>();
		for(asp04__Payment__c aspPayment : aspPayments) {
			relatedContactIds.add(aspPayment.Contact__c);
		}

		Map<Id, String> aspCustomerIds = getAspCustomerIds();

		Map<Id, Contact> contactsByIds = new Map<Id, Contact> ([
				SELECT Id, Leased_Reserved_Unit__c, Leased_Reserved_Unit__r.Building__c, Leased_Reserved_Unit__r.Building__r.Development_Site__c
				FROM Contact
				WHERE Id IN :relatedContactIds
		]);

		for(asp04__Payment__c aspPayment : aspPayments) {
			if(contactsByIds.containsKey(aspPayment.Contact__c)) {
				if(contactsByIds.get(aspPayment.Contact__c).Leased_Reserved_Unit__c != null) {
					Id relatedDevelopmentId = contactsByIds.get(aspPayment.Contact__c).Leased_Reserved_Unit__r.Building__r.Development_Site__c;
					if(aspCustomerIds.containsKey(relatedDevelopmentId)) {
						aspPayment.asp04__Customer_ID__c = aspCustomerIds.get(relatedDevelopmentId);
					}
				}
			}
		}

		return aspPayments;
	}

	private static Map<Id, String> getAspCustomerIds() {
		Map<Id, String> aspCustomerIds = new Map<Id, String>();

		for(Asperato_Customer_Id__c customerId : [SELECT Id, Name, Development_Id__c, Customer_Id__c FROM Asperato_Customer_Id__c]) {
			aspCustomerIds.put(customerId.Development_Id__c, customerId.Customer_Id__c);
		}

		return aspCustomerIds;
	}
}