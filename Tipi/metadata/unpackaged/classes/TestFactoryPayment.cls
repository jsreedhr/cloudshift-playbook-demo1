/**
 * @Name:        TestFactoryPayment
 * @Description: Data Factory class for testing social spaces and quest parking booking functionality and payment functionality
 *
 * @author:      Eugene Konstantinov
 * @version:     1.0
 * Change Log
 *
 * Date          author                 Change Description
 * -----------------------------------------------------------------------------------
 * 03/05/2018    Eugene Konstantinov    Created Class
 * 22/05/2018    Patrick Fischer        Refactored Class
 * 23/05/2018    Patrick Fischer        Added methods for Quote, QuoteLine, Contract, Product, PriceBook etc.
 * 01/06/2018    Patrick Fischer        Added methods for Tipi_Payments__c
 */
@IsTest
public class TestFactoryPayment {

    /**
     * To construct a number of Account records
     *
     * @param count amount of records to construct
     * @param isInsert true to insert records, else false
     *
     * @return list of Account records
     */
    public static List<Account> addAccounts(Integer count, Boolean isInsert) {
        List<Account> accounts = new List<Account>();
        for(Integer i = 0; i < count; i++) {
            accounts.add(new Account(Name = 'TestILTNotExisted-' + i));
        }
        if (isInsert) {
            insert accounts;
        }
        return accounts;
    }

    /**
     * To construct a number of Contacts
     *
     * @param account related Accounts record to this Contact
     * @param count amount of records to construct
     * @param isInsert true to insert records, else false
     *
     * @return list of Contact records
     */
    public static List<Contact> getContacts(Account account, Integer count, Boolean isInsert) {
        List<Contact> contacts = new List<Contact>();
        for(Integer i = 0; i < count; i++) {
            contacts.add(
                    new Contact(
                            AccountId = account.Id,
                            LastName = 'lastnamecontact' + i,
                            FirstName = 'firstnamecontact' + i,
                            Email = 'test@email.com'
                    )
            );
        }
        if (isInsert) {
            insert contacts;
        }
        return contacts;
    }

    /**
     * To construct a number of Contact records with related Account, Unit and Contract
     *
     * @param accounts list of account records for which to create Contracts and construct Contacts
     * @param count amount of records to construct
     * @param isInsert true to insert records, else false
     *
     * @return list of Contact records
     */
    public static List<Contact> addContacts(List<Account> accounts, Integer count, Boolean isInsert) {
        Development__c newDev = getDevevelopment();
        List<Building__c> newBuilding = getBuildings(newDev, 1);
        List<Unit__c> newUnints = getUnits(newBuilding[0], 1);
        List<Contact> contacts = new List<Contact>();
        Map<Id, Contract> newContract = getContracts(accounts);
        for(Integer i = 0; i < count; i++) {
            contacts.add(
                    new Contact(
                            AccountId = accounts[Math.mod(i, accounts.size())].Id,
                            LastName = 'lastnamecontact' + i + Math.random(),
                            FirstName = 'firstnamecontact' + i + Math.random(),
                            Email = 'test@email.com' + Math.random(),
                            Leased_Reserved_Unit__c = newUnints[0].Id,
                            Current_Lease__c = newContract.get(accounts[Math.mod(i, accounts.size())].Id).Id
                    )
            );
        }
        if (isInsert) {
            insert contacts;
        }
        return contacts;
    }

    /**
     * To construct a number of Contract records
     *
     * @param account related Account record to this contract
     * @param quote related SBQQ__Quote__c record to this contract
     * @param opportunity related Opportunity record to this contract
     * @param unit related Unit__c record to this contract
     * @param count amount of records to construct
     * @param isInsert true to insert records, else false
     *
     * @return list of Contract records
     */
    public static List<Contract> getContracts(Account account, SBQQ__Quote__c quote, Opportunity opportunity, Unit__c unit, Integer count, Boolean isInsert) {
        List<Contract> contracts = new List<Contract>();
        for(Integer i = 0; i < count; i++) {
            contracts.add(
                    new Contract(
                            AccountId = account.Id,
                            Status = 'Draft',
                            StartDate = quote.SBQQ__StartDate__c,
                            ContractTerm = quote.SBQQ__StartDate__c.monthsBetween(quote.SBQQ__EndDate__c),
                            SBQQ__RenewalQuoted__c = false,
                            SBQQ__Quote__c = quote.Id,
                            SBQQ__Opportunity__c = opportunity.Id,
                            Type__c = 'Rent To Rent',
                            Unit_Type__c = 'OMR',
                            Unit__c = unit.Id,
                            Pricebook2Id = Test.getStandardPricebookId()
                    )
            );
        }
        if(isInsert) {
            insert contracts;
        }
        return contracts;
    }

    /**
     * To construct & insert one Contract for every Account
     *
     * @param accounts list of Accounts for which to create a Contracts
     *
     * @return map of AccountId to Contract record
     */
    public static Map<Id, Contract> getContracts(List<Account> accounts) {
        Map<Id, Contract> contractByAccountId = new Map<Id, Contract>();
        for (Account acc : accounts) {
            contractByAccountId.put(acc.Id, new Contract(AccountId = acc.Id));
        }
        insert contractByAccountId.values();
        return contractByAccountId;
    }

    /**
     * To construct & insert a single Development__c records
     *
     * @return single Development__c record
     */
    public static Development__c getDevevelopment() {
        return getDevelopments(1, true)[0];
    }

    /**
     * To construct a number of Development__c records
     *
     * @param count amount of records to construct
     * @param isInsert true to insert records, else false
     *
     * @return list of Development__c records
     */
    public static List<Development__c> getDevelopments(Integer count, Boolean isInsert) {
        List<Development__c> developments = new List<Development__c>();
        for(Integer i = 0; i < count; i++) {
            developments.add(
                    new Development__c(
                            Name = 'Test Development Site ',
                            POD_Group_Name__c = 'Pod A',
                            Street__c = 'Test Street',
                            City__c = 'Test City',
                            Postcode__c = 'TEST 323',
                            County__c = 'Bristol',
                            Country__c = 'United Kingdom'
                    )
            );
        }
        if(isInsert) {
            insert developments;
        }
        return developments;
    }

    /**
     * To construct & insert list of Building records
     *
     * @param development related Development__c record to this Building
     * @param count amount of records to construct
     *
     * @return list of Building__c records
     */
    public static List<Building__c> getBuildings(Development__c development, Integer count) {
        return getBuildings(development, count, true);
    }

    /**
     * To construct a number of Building__c records
     *
     * @param development related Development__c record to this Building
     * @param count amount of records to construct
     * @param isInsert true to insert records, else false
     *
     * @return list of Building__c records
     */
    public static List<Building__c> getBuildings(Development__c development, Integer count, Boolean isInsert) {
        List<Building__c> buildings = new List<Building__c>();
        for (Integer i = 0; i < count; i++) {
            Building__c building = new Building__c(
                    Name = 'Test Building ' + i,
                    Development_Site__c = development.Id,
                    Street__c = development.Street__c,
                    City__c = development.City__c,
                    Postcode__c = development.Postcode__c,
                    County__c = development.County__c,
                    Country__c = development.Country__c,
                    Allows_Pets__c = i == 1 ? true : false
            );
            buildings.add(building);
        }
        if(isInsert) {
            insert buildings;
        }
        return buildings;
    }

    /**
     * To construct & insert a number of Unit__c records
     *
     * @param building related Building__c to this unit
     * @param count amount of records to construct
     *
     * @return list of Unit__c records
     */
    public static List<Unit__c> getUnits(Building__c building, Integer count) {
        return getUnits(building, count, true);
    }

    /**
     * To construct a number of Unit__c records
     *
     * @param building related Building__c to this unit
     * @param count amount of records to construct
     * @param isInsert true to insert records, else false
     *
     * @return list of Unit__c records
     */
    public static List<Unit__c> getUnits(Building__c building, Integer count, Boolean isInsert) {
        List<Unit__c> units = new List<Unit__c>();
        for (Integer i = 0; i < count; i++) {
            Unit__c unit = new Unit__c(
                    Name = 'Test Unit' + i,
                    Description__c = 'test',
                    Summary__c = 'test',
                    Status__c = 'Occupied',
                    Unit_Number__c = '132',
                    Unit_Type__c = 'Studio',
                    Number_of_Bedrooms__c = 1,
                    Furnished__c = 'Furnished',
                    Rent__c = 1000 + 100 * i,
                    Building__c = building.Id,
                    Date_Available__c = Date.newInstance(2018, 4, 10 + i)
            );
            units.add(unit);
        }
        if(isInsert) {
            insert units;
        }
        return units;
    }

    /**
     * To construct a single Opportunity records
     *
     * @param account related Account to this opportunity
     * @param contact related Contact to this opportunity
     * @param isInsert true to insert records, else false
     *
     * @return single Opportunity record
     */
    public static Opportunity getOpportunity(Account account, Contact contact, Boolean isInsert) {
        Opportunity opp = new Opportunity(
                Name = 'Test Opportunity',
                AccountId = account.Id,
                CloseDate = Date.newInstance(2018, 4, 7),
                Deposit__c = 1000,
                Primary_Contact__c = contact.Id,
                StageName = 'Interested',
                Type = '1 Bed',
            	Amount = 1000,
            	Term_No_of_Months__c = 4
        );
        if (isInsert) {
            insert opp;
        }
        return opp;
    }

    /**
     * To construct a number of Opportunity records
     *
     * @param account related Account to this opportunity
     * @param contacts list of Contacts containing Primary Contact, Guarantor & Tenant 2-8
     * @param count amount of records to construct
     * @param isInsert true to insert records, else false
     *
     * @return list of Opportunity records
     */
    public static List<Opportunity> getOpportunities(Account account, List<Contact> contacts, Integer count, Boolean isInsert) {
        System.assert(contacts.size() >= 10, 'Expecting at least 10 contacts to populate every lookup on the Opportunity');

        List<Opportunity> opportunities = new List<Opportunity>();
        for(Integer i = 0; i < count; i++) {
            Opportunity opportunity = getOpportunity(account, contacts[0], false);
            opportunity.Name = opportunity.Name + i;
            opportunity.Pricebook2Id = Test.getStandardPricebookId();
            opportunity.SBQQ__QuotePricebookId__c = Test.getStandardPricebookId();
            opportunity.Tenant_2__c = contacts[1].Id;
            opportunity.Tenant_3__c = contacts[2].Id;
            opportunity.Tenant_4__c = contacts[3].Id;
            opportunity.Tenant_5__c = contacts[4].Id;
            opportunity.Primary_Contact_Guarantor__c = contacts[5].Id;
            opportunity.Resident_2_Guarantor__c = contacts[6].Id;
            opportunity.Resident_3_Guarantor__c = contacts[7].Id;
            opportunity.Resident_4_Guarantor__c = contacts[8].Id;
            opportunity.Resident_5_Guarantor__c = contacts[9].Id;

            opportunities.add(opportunity);
        }
        if (isInsert) {
            insert opportunities;
        }
        return opportunities;
    }

    /**
     * To construct a number SBQQ__Quote__c records
     *
     * @param account related Account to this quote
     * @param contact related primary Contact to this quote
     * @param opportunity related Opportunity to this quote
     * @param count amount of records to construct
     * @param isInsert true to insert records, else false
     *
     * @return list of SBQQ__Quote__c records
     */
    public static List<SBQQ__Quote__c> getQuotes(Account account, Contact contact, Opportunity opportunity, Integer count, Boolean isInsert) {
        List<SBQQ__Quote__c> quotes = new List<SBQQ__Quote__c>();
        Date startDate = Date.newInstance(2018, 4, 13);
        for(Integer i = 0; i < count; i++) {
            quotes.add(
                    new SBQQ__Quote__c(
                            SBQQ__Account__c = account.Id,
                            SBQQ__PrimaryContact__c = contact.Id,
                            SBQQ__PriceBook__c = Test.getStandardPricebookId(),
                            SBQQ__PricebookId__c = Test.getStandardPricebookId(),
                            Rent_Increase__c = 120,
                            SBQQ__Primary__c = true,
                            SBQQ__Status__c = 'Accepted',
                            SBQQ__Opportunity2__c = opportunity.Id,
                            SBQQ__StartDate__c = startDate,
                            SBQQ__EndDate__c = startDate.addMonths(12),
                            SBQQ__ExpirationDate__c = startDate.addMonths(12)
                    )
            );
        }
        if(isInsert) {
            insert quotes;
        }
        return quotes;
    }

    /**
     * To construct a number of Product2 records
     *
     * @param type String such as 'Unit', 'Storage', 'Parking'
     * @param price Decimal value defining the cost of the product
     * @param count amount of records to construct
     * @param isInsert true to insert records, else false
     *
     * @return list of Product2 records
     */
    public static List<Product2> getProducts(String type, Decimal price, Integer count, Boolean isInsert) {
        List<Product2> products = new List<Product2>();
        List<String> sizes = new List<String>{'S', 'M', 'L', 'XL'};
        for(Integer i = 0; i < count; i++) {
            products.add(
                    new Product2(
                            Name = 'Test ' + type + ' - ' + i,
                            Description = 'Test Product Description',
                            ProductCode = 'TP-CODE-' + i,
                            Product_Price__c = price,
                            Product_Type__c = type,
                            Family = 'Alto',
                            SBQQ__ChargeType__c = 'One-Time',
                            Size__c = sizes[Math.mod(i, sizes.size())],
                            IsActive = true
                    )
            );
        }
        if(isInsert) {
            insert products;
        }
        return products;
    }

    /**
     * To construct a number of Pricebook2 records
     *
     * @param count amount of records to construct
     * @param isInsert true to insert records, else false
     *
     * @return list of Price
     */
    public static List<Pricebook2> getPricebooks(Integer count, Boolean isInsert) {
        List<Pricebook2> pricebooks = new List<Pricebook2>();
        for(Integer i = 0; i < count; i++) {
            pricebooks.add(
                    new Pricebook2(
                            Name = 'Standard Price Book' + i,
                            IsActive = true
                    )
            );
        }
        if(isInsert) {
            insert pricebooks;
        }
        return pricebooks;
    }

    /**
     * To construct PricebookEntry records for every Product
     *
     * @param products list of Product2 records for which to create Pricebook entries
     * @param isInsert true to insert records, else false
     *
     * @return list of PricebookEntry records
     */
    public static List<PricebookEntry> getPricebookEntries(List<Product2> products, Boolean isInsert) {
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        for (Product2 product : products) {
            pricebookEntries.add(
                    new PricebookEntry(
                            Pricebook2Id = Test.getStandardPricebookId(),
                            Product2Id = product.Id,
                            IsActive = true,
                            UnitPrice = product.Product_Price__c
                    )
            );
        }
        if(isInsert) {
            insert pricebookEntries;
        }
        return pricebookEntries;
    }

    /**
     * To construct SBQQ__QuoteLine__c records for every product
     *
     * @param quote SBQQ__Quote__c parent quote record
     * @param products list of Product2 records for which to create SBQQ__QuoteLine__c items
     * @param isInsert true to insert records, else false
     *
     * @return list of SBQQ__QuoteLine__c records
     */
    public static List<SBQQ__QuoteLine__c> getQuoteLines(SBQQ__Quote__c quote, List<Product2> products, Boolean isInsert) {
        List<SBQQ__QuoteLine__c> quoteLines = new List<SBQQ__QuoteLine__c>();
        for (Product2 product : products) {
            quoteLines.add(
                    new SBQQ__QuoteLine__c(
                            SBQQ__Quote__c = quote.Id,
                            SBQQ__Product__c = product.Id,
                            Product_Type__c = product.Product_Type__c
                    )
            );
        }
        if (isInsert) {
            insert quoteLines;
        }
        return quoteLines;
    }

    /**
     * To construct Rentable_Item__c records for every product
     *
     * @param products list of Product2 records for which to create rentable items
     * @param building Building to link to products
     * @param isInsert true to insert records, else false
     *
     * @return list of Rentable_Item__c records
     */
    public static List<Rentable_Item__c> getRentableItems(List<Product2> products, Building__c building, Boolean isInsert) {
        List<Rentable_Item__c> rentableItems = new List<Rentable_Item__c>();
        for(Product2 product : products) {
            if(product.Product_Type__c == 'Storage' || product.Product_Type__c == 'Parking') {
                rentableItems.add(
                        new Rentable_Item__c(
                                Product__c = product.Id,
                                Building__c = building.Id,
                                Development__c = building.Development_Site__c,
                                Status__c = 'Available'
                        )
                );
            }
        }
        if(isInsert){
            insert rentableItems;
        }
        return rentableItems;
    }

    /**
     * To construct asp04__Authorisation__c records to authorise every contact
     *
     * @param contacts list of Contacts to be authorised
     * @param isInsert true to insert records, else false
     *
     * @return list of asp04__Authorisation__c records
     */
    public static List<asp04__Authorisation__c> getAuthorisations(List<Contact> contacts, Boolean isInsert) {
        List<asp04__Authorisation__c> authorisations = new List<asp04__Authorisation__c>();
        for(Contact contact : contacts) {
            authorisations.add(
                    new asp04__Authorisation__c(
                            Contact__c = contact.Id,
                            asp04__Status__c = 'In Force'
                    )
            );
        }
        if(isInsert) {
            insert authorisations;
        }
        return authorisations;
    }

    /**
     * To construct a number of Tipi_Payments__c records
     *
     * @param amount Decimal amount value of the payment
     * @param contactId Id of related Contact who will be billed
     * @param creditPaymentType String of 'Debit' / 'Credit' / ''
     * @param developmentId Id of related Development to this payment
     * @param opportunity Id of related Opportunity to this payment
     * @param parentTipiPayment assign a rent payment to an existing Credit / Reservation payment (parent Tipi Payment)
     * @param paymentDate Date from when onwards the payment can be billed
     * @param status String holding the payment status such as 'Awaiting submission'
     * @param type String holding
     * @param count amount of records to construct
     * @param isInsert true to insert records, else false
     *
     * @return List of Tipi_Payments__c records
     */
    public static List<Tipi_Payments__c> getTipiPayments(Decimal amount, Id contactId, String creditPaymentType,
            Id developmentId, Opportunity opportunity, Id parentTipiPayment, Date paymentDate, String status,
            String type, Integer count, Boolean isInsert) {

        List<Tipi_Payments__c> tipiPayments = new List<Tipi_Payments__c>();

        for(Integer i = 0; i < count; i++) {
            tipiPayments.add(
                    new Tipi_Payments__c(
                            Amount__c = amount,
                            Contact__c = contactId,
                            Credit_Payment_Type__c = creditPaymentType,
                            Development__c = developmentId,
                            Opportunity__c = opportunity.Id,
                            Payment_Date__c = paymentDate,
                            Payment_Gateway__c = Utils_Constants.PAYMENT_GATEWAY_DD,
                            Status__c = status,
                            Tipi_Payment__c = parentTipiPayment,
                            Type__c = type
                    )
            );
        }
        if(isInsert) {
            insert tipiPayments;
        }
        return tipiPayments;
    }

    /**
     * To construct a Community User
     *
     * @return portal User record
     */
    public static User createPortallUser() {
        List<String> userTypeProfile = new List<String>{'PowerCustomerSuccess', 'CspLitePortal', 'PowerPartner', 'CustomerSuccess'};

        Id partner = [
                SELECT Id
                FROM Profile
                WHERE
                        UserType IN :userTypeProfile
                LIMIT 1
        ].Id;

        Id admin = [
                SELECT Id
                FROM Profile
                WHERE
                        Name = 'System Administrator'
                LIMIT 1
        ].Id;

        UserRole role = new UserRole(Name = 'Customer User');
        insert role;

        User userCEO = new User(
                Alias = 'test22',
                Email = 'test22@noemail.com',
                EmailEncodingKey = 'UTF-8',
                LastName ='Testing',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId = admin,
                Country = 'United States',
                IsActive = true,
                UserRoleId = role.Id,
                TimeZoneSidKey = 'America/Los_Angeles',
                Username = 'test22@noemail.com'
        );

        insert userCEO;

        User portalUser = new User();
        List<Account> listAccount = new List<Account>();
        List<Contact> listContact = new List<Contact>();

        System.runAs(userCEO) {
            listAccount = addAccounts(1, false);
            listAccount[0].Name = listAccount[0].Name + 'CommUser';
            insert listAccount;
            listContact = addContacts(listAccount, 1, false);
            insert listContact;

            portalUser = new User(
                    Alias = 'test33',
                    Email = 'test33@noemail.com',
                    EmailEncodingKey = 'UTF-8',
                    LastName = 'Testing',
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_US',
                    ProfileId = partner,
                    Country = 'United States',
                    IsActive = true,
                    ContactId = listContact[0].Id,
                    TimeZoneSidKey = 'America/Los_Angeles',
                    Username='tester33@noemail.com'+Math.random()
            );

            try {
                insert portalUser;
            } catch(Exception e) {
                System.debug('No allow for Portal Users');
            }
        }

        System.assert(portalUser.Email == 'test33@noemail.com');
        return portalUser;
    }
}