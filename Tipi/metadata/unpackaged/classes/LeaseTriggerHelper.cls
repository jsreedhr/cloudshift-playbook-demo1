/**
 * @Name:        LeaseTriggerHelper
 * @Description: This is helper class, will hold all methods and functionalities that are to be build for Lease Trigger
 *
 * @author:      Andrey Korobovich
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 30/05/2018    Andrey Korobovich    Created Class
 * 01/05/2018    Eugene Konstantinov  Added changes to processAfterUpdate and cancelRentPaymentsForOpportunity, updateBRIEndDate, buildTipiPaymentRecords methods
 * 13/06/2018    Patrick Fischer      Changed updateEntities() to support Lease Id & Unit Id populated on Opp + Contact (incl. use of maps)
 * 18/06/2018    Patrick Fischer      Removed Logic to populate Lease Id & Unit Id on Contact (This is now being handled
 *                                    by Opportunity After Insert/updatetrigger). Population on Opportunity remains.
 * 21/06/2018   Eugene Konstantinov   Added processBeforeUpdate for Month_to_Month__c Lease
 */

public with sharing class LeaseTriggerHelper {
    private static final Set<String> RENTABLE_ITEM_ALLOCATED = new Set<String>{'Allocated'};

    public static void processAfterInsert(Map<Id, SObject> newMap) {
        Set<Id> contractIds = getContractIds(newMap);
        List<Contract> contracts = getContractWithRelatedRecords(contractIds);
        if (!contracts.isEmpty()) {
            updateEntities(contracts);
        }
    }

    public static void processBeforeUpdate(Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
        for (Id itemId : newMap.keySet()) {
            Contract currentLease = (Contract)newMap.get(itemId);
            Contract oldLease = (Contract)oldMap.get(itemId);
            if (currentLease.Actual_Tenancy_End_Date__c > oldLease.Actual_Tenancy_End_Date__c) {
                currentLease.Month_to_Month__c = true;
            }
        }
    }

    public static void processAfterUpdate(Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
        Set<Id> contractIds = getContractIds(newMap);
        Set<Id> yardiContactIdsToExclude = new Map<Id, Contact>([SELECT Id FROM Contact WHERE External_Id__c LIKE 'r%']).keySet();
        List<Contract> contracts = getContractWithRelatedRecords(contractIds);
        if (!contracts.isEmpty()) {
            updateEntities(contracts);
        }

        Map<Id, Date> endDateByOpportunityId = new Map<Id, Date>();
        Map<Id, Contract> leasesForBRIUpdateByLeaseId = new Map<Id, Contract>();
        Set<Id> contractIdsToBuildPayments = new Set<Id>();
        for (Id itemId : newMap.keySet()) {
            Contract currentLease = (Contract)newMap.get(itemId);
            Contract oldLease = (Contract)oldMap.get(itemId);
            if (currentLease.Actual_Tenancy_End_Date__c == null) {
                continue;
            }
            if (currentLease.Actual_Tenancy_End_Date__c != oldLease.Actual_Tenancy_End_Date__c) {
                endDateByOpportunityId.put(currentLease.SBQQ__Opportunity__c, currentLease.Actual_Tenancy_End_Date__c);
                contractIdsToBuildPayments.add(itemId);
            }
            if (currentLease.Actual_Tenancy_End_Date__c < oldLease.Actual_Tenancy_End_Date__c) {
                leasesForBRIUpdateByLeaseId.put(itemId, currentLease);
            }
        }

        Map<Id, Map<Date, Tipi_Payments__c>> paymentStatusByDate = new Map<Id, Map<Date, Tipi_Payments__c>>();
        if (endDateByOpportunityId != null && !endDateByOpportunityId.isEmpty()) {
            paymentStatusByDate = cancelRentPaymentsForOpportunity(endDateByOpportunityId);
            cancelFutureRentableItemPaymentsForOpportunity(endDateByOpportunityId);
        }

        if (contractIdsToBuildPayments != null && !contractIdsToBuildPayments.isEmpty()) {
            List<Tipi_Payments__c> newPayments = buildTipiPaymentRecords(contractIdsToBuildPayments);
            List<Tipi_Payments__c> paymentsToInsert = new List<Tipi_Payments__c>();

            for (Tipi_Payments__c payment : newPayments) {
                if (payment == null) {
                    continue;
                }

                if(paymentStatusByDate.containsKey(payment.Opportunity__c) && payment.Payment_Date__c >= System.today().toStartOfMonth() && !yardiContactIdsToExclude.contains(payment.Contact__c) ) {
                    if (!paymentStatusByDate.get(payment.Opportunity__c).containsKey(payment.Payment_Date__c) && payment.Payment_Date__c.day() == 1) {
                        paymentsToInsert.add(payment);
                    } else if (paymentStatusByDate.get(payment.Opportunity__c).get(payment.Payment_Date__c) != null &&
                            paymentStatusByDate.get(payment.Opportunity__c).get(payment.Payment_Date__c).Status__c == Utils_Constants.CANCELLED_STATUS) {
                        paymentsToInsert.add(payment);
                    }
                }
            }

            if (paymentsToInsert != null && !paymentsToInsert.isEmpty()) {
                insert paymentsToInsert;
            }
        }

        if (leasesForBRIUpdateByLeaseId != null && !leasesForBRIUpdateByLeaseId.isEmpty()) {
            updateBRIEndDate(leasesForBRIUpdateByLeaseId);
        }
    }

    private static Set<Id> getContractIds(Map<Id, SObject> newMap) {
        Map<Id, Contract> contractMap = (Map<Id, Contract>) newMap;
        Set<Id> res = new Set<Id>();
        for (Contract c : contractMap.values()) {
            if (c.Unit__c != null && c.SBQQ__Opportunity__c != null) {
                res.add(c.Id);
            }
        }
        return res;
    }

    private static List<Contract> getContractWithRelatedRecords(Set<Id> contractIds) {
        List<Contract> res = [SELECT Id, Unit__c, SBQQ__Opportunity__c, SBQQ__Opportunity__r.Leased_Reserved_Unit__c,
                SBQQ__Opportunity__r.SBQQ__Contracted__c, SBQQ__Opportunity__r.ContractId, External_Id__c,
                SBQQ__Opportunity__r.Primary_Contact__c, SBQQ__Opportunity__r.Primary_Contact__r.Leased_Reserved_Unit__c, SBQQ__Opportunity__r.Primary_Contact__r.Current_Lease__c,
                SBQQ__Opportunity__r.Tenant_2__c, SBQQ__Opportunity__r.Tenant_2__r.Leased_Reserved_Unit__c, SBQQ__Opportunity__r.Tenant_2__r.Current_Lease__c,
                SBQQ__Opportunity__r.Tenant_3__c, SBQQ__Opportunity__r.Tenant_3__r.Leased_Reserved_Unit__c, SBQQ__Opportunity__r.Tenant_3__r.Current_Lease__c,
                SBQQ__Opportunity__r.Tenant_4__c, SBQQ__Opportunity__r.Tenant_4__r.Leased_Reserved_Unit__c, SBQQ__Opportunity__r.Tenant_4__r.Current_Lease__c,
                SBQQ__Opportunity__r.Tenant_5__c, SBQQ__Opportunity__r.Tenant_5__r.Leased_Reserved_Unit__c, SBQQ__Opportunity__r.Tenant_5__r.Current_Lease__c
                FROM Contract
                WHERE Id IN: contractIds];
        return res;
    }

    private static void updateEntities(List<Contract> contractsList) {
        Map<Id, Opportunity> oppsMapForUpd = new Map<Id, Opportunity>();

        for (Contract c : contractsList) {
            // update Unit / Contract on Opportunity
            if (c.SBQQ__Opportunity__r.ContractId != c.Id || c.SBQQ__Opportunity__r.SBQQ__Contracted__c != true ||
                            c.SBQQ__Opportunity__r.Leased_Reserved_Unit__c != c.Unit__c) {
                c.SBQQ__Opportunity__r.ContractId = c.Id;
                c.SBQQ__Opportunity__r.SBQQ__Contracted__c = true;
                c.SBQQ__Opportunity__r.Leased_Reserved_Unit__c = c.Unit__c;
                oppsMapForUpd.put(c.SBQQ__Opportunity__c, c.SBQQ__Opportunity__r);
            }
        }

        if (!oppsMapForUpd.isEmpty()) {
            update oppsMapForUpd.values();
        }
    }

    private static Map<Id, Map<Date, Tipi_Payments__c>> cancelRentPaymentsForOpportunity(Map<Id, Date> opportunityIds) {
        List<Tipi_Payments__c> payments = [
            SELECT Id, Name, Status__c, Opportunity__c, Payment_Date__c, Contact__c, Type__c, isPartialRent__c, Amount__c, CreatedDate
            FROM Tipi_Payments__c
            WHERE
                RecordType.DeveloperName != :Utils_Constants.PAYMENT_RECORDTYPE_REFUND AND
                Payment_Date__c >= TODAY AND
                Credit_Payment_Type__c != :Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT AND
                Opportunity__c IN :opportunityIds.keySet() AND
                Type__c IN :Utils_Constants.TIPI_RENT_PAYMENT_TYPES
            ORDER BY Name DESC, Payment_Date__c DESC
            LIMIT 10000
        ];

        Map<Id, List<Tipi_Payments__c>> paymentsByContact = new Map<Id, List<Tipi_Payments__c>>();
        for (Tipi_Payments__c payment : payments) {
            if (!paymentsByContact.containsKey(payment.Contact__c)) {
                paymentsByContact.put(payment.Contact__c, new List<Tipi_Payments__c>());
            }
            paymentsByContact.get(payment.Contact__c).add(payment);
        }

        Set<Tipi_Payments__c> cancelledPayments = new Set<Tipi_Payments__c>();
        Map<Id, Map<Date, Tipi_Payments__c>> paymentByPaymentDate = new Map<Id, Map<Date, Tipi_Payments__c>>();
        for (Id contactId : paymentsByContact.keySet()) {
            if (paymentsByContact.get(contactId)[0].Status__c == Utils_Constants.AWAITING_SUBMISSION_STATUS) {
                paymentsByContact.get(contactId)[0].Status__c = Utils_Constants.CANCELLED_STATUS;
                cancelledPayments.add(paymentsByContact.get(contactId)[0]);
            }

            for (Tipi_Payments__c payment : paymentsByContact.get(contactId)) {
                if (opportunityIds.containsKey(payment.Opportunity__c) && payment.Status__c == Utils_Constants.AWAITING_SUBMISSION_STATUS) {
                    Date firstDayOfEndDateMonth = opportunityIds.get(payment.Opportunity__c).toStartOfMonth();
                    if (payment.Payment_Date__c >= firstDayOfEndDateMonth) {
                        payment.Status__c = Utils_Constants.CANCELLED_STATUS;
                        cancelledPayments.add(payment);
                    }
                }
                if (Utils_Constants.TIPI_RENT_PAYMENT_TYPES.contains(payment.Type__c)) {
                    if (!paymentByPaymentDate.containsKey(payment.Opportunity__c)) {
                        paymentByPaymentDate.put(payment.Opportunity__c, new Map<Date, Tipi_Payments__c>());
                    }
                    if (!paymentByPaymentDate.get(payment.Opportunity__c).containsKey(payment.Payment_Date__c)) {
                        paymentByPaymentDate.get(payment.Opportunity__c).put(payment.Payment_Date__c, payment);
                    } else if (paymentByPaymentDate.get(payment.Opportunity__c).get(payment.Payment_Date__c).CreatedDate < payment.CreatedDate) {
                        paymentByPaymentDate.get(payment.Opportunity__c).put(payment.Payment_Date__c, payment);
                    }

                }
            }
        }
        List<Tipi_Payments__c> listToUpdate = new List<Tipi_Payments__c>();
        listToUpdate.addAll(cancelledPayments);
        update listToUpdate;

        return paymentByPaymentDate;
    }

    /**
     * To cancel future payments of rentable items
     *
     * @param opportunityIds
     *
     * @return
     */
    private static Boolean cancelFutureRentableItemPaymentsForOpportunity(Map<Id, Date> opportunityIds) {
        Boolean isSuccess = true;

        List<Tipi_Payments__c> tipiPaymentsToCancel = new List<Tipi_Payments__c>();
        List<Tipi_Payments__c> tipiPayments = [
                SELECT Id, Opportunity__c, Payment_Date__c
                FROM Tipi_Payments__c
                WHERE
                RecordType.DeveloperName != :Utils_Constants.PAYMENT_RECORDTYPE_REFUND AND
                Payment_Date__c >= TODAY AND
                Credit_Payment_Type__c != :Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT AND
                Opportunity__c IN :opportunityIds.keySet() AND
                Status__c = :Utils_Constants.AWAITING_SUBMISSION_STATUS AND
                (Type__c = :Utils_Constants.PAYMENT_TYPE_PARKING OR
                Type__c = :Utils_Constants.PAYMENT_TYPE_STORAGE)
                ORDER BY Name DESC, Payment_Date__c DESC
                LIMIT 10000
        ];

        for(Tipi_Payments__c tipiPayment : tipiPayments) {
            if(opportunityIds.containsKey(tipiPayment.Opportunity__c)) {
                Date actualTenancyEndDate = opportunityIds.get(tipiPayment.Opportunity__c);

                if(tipiPayment.Payment_Date__c >= actualTenancyEndDate) {
                    tipiPayment.Status__c = Utils_Constants.CANCELLED_STATUS;
                    tipiPaymentsToCancel.add(tipiPayment);
                }
            }
        }

        try {
            if(!tipiPaymentsToCancel.isEmpty()) {
                update tipiPaymentsToCancel;
            }
        } catch (Exception e) {
            isSuccess = false;
        }

        return isSuccess;
    }

    private static void updateBRIEndDate(Map<Id, SObject> leases) {
        BookedRentableItemSelector briSelector = new BookedRentableItemSelector();
        Id guestParkingId = briSelector.getRecordType('Guest_Parking', 'Rentable_Item__c');
        Id socialSpaceId = briSelector.getRecordType('Social_Space', 'Booked_Rentable_Item__c');
        List<Booked_Rentable_Item__c> bookedItems =
                (List<Booked_Rentable_Item__c>) briSelector.getRecordsByLeaseIds(leases.keySet(), new Set<String>{
                        Booked_Rentable_Item__c.End_Date__c.getDescribe().getName(),
                        Booked_Rentable_Item__c.Start_Date__c.getDescribe().getName(),
                        Booked_Rentable_Item__c.Contract__c.getDescribe().getName(),
                        Booked_Rentable_Item__c.Contact__c.getDescribe().getName(),
                        Booked_Rentable_Item__c.RecordTypeId.getDescribe().getName(),
                        Booked_Rentable_Item__c.Status__c.getDescribe().getName(),
                        Booked_Rentable_Item__c.Quote_Line__c.getDescribe().getName(),
                        'Quote_Line__r.Concession_Length__c',
                        'Quote_Line__r.Monthly_Concession__c',
                        'Quote_Line__r.Off_First_Month__c',
                        'Quote_Line__r.Free_Month__c',
                        'Rentable_Item__r.RecordTypeId',
                        'Rentable_Item__r.Cost__c',
                        'Rentable_Item__r.Building__c',
                        'Rentable_Item__r.Product__r.Size__c',
                        'Rentable_Item__r.Name',
                        'Rentable_Item__r.Product__r.Product_Type__c',
                        'Contact__r.Name',
                        'Contract__r.Type__c',
                        'Rentable_Item__r.Development__c',
                        'Contract__r.SBQQ__Opportunity__c',
                        'Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__c',
                        'Contract__r.Payment_Config_Done__c',
                        'Contract__r.SBQQ__Opportunity__r.Deposit__c',
                        'Contract__r.StartDate',
                        'Contract__r.EndDate',
                        'Contract__r.Actual_Tenancy_End_Date__c',
                        'Contract__r.Unit__r.Building__r.Development_Site__c',
                        'Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Checkbox__c',
                        'Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Length__c',
                        'Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Amount__c',
                        'Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_Checkbox__c',
                        'Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_First_Month__c',
                        'Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Checkbox__c',
                        'Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Text__c',
                        'Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Unit_Rent_Rollup__c'
                        });

        List<Booked_Rentable_Item__c> briToUpdate = new List<Booked_Rentable_Item__c>();
        Set<Id> rentableItemIds = new Set<Id>();
        Set<Id> opportunityIds = new Set<Id>();
        List<Booked_Rentable_Item__c> allocatedBRI = new List<Booked_Rentable_Item__c>();
        Map<Id, Date> dateChanged = new Map<Id, Date>();
        for (Booked_Rentable_Item__c bookingItem : bookedItems) {
            if (bookingItem.RecordTypeId != socialSpaceId && bookingItem.Rentable_Item__c != null &&
                            bookingItem.Rentable_Item__r.RecordTypeId != guestParkingId) {
                rentableItemIds.add(bookingItem.Rentable_Item__c);
                opportunityIds.add(bookingItem.Contract__r.SBQQ__Opportunity__c);
                Contract leaesForBRI = (Contract)leases.get(bookingItem.Contract__c);

                if (bookingItem.End_Date__c > leaesForBRI.Actual_Tenancy_End_Date__c) {
                    bookingItem.End_Date__c = leaesForBRI.Actual_Tenancy_End_Date__c;
                    briToUpdate.add(bookingItem);
                    dateChanged.put(bookingItem.Contract__r.SBQQ__Opportunity__c, leaesForBRI.Actual_Tenancy_End_Date__c);
                }

                if (RENTABLE_ITEM_ALLOCATED.contains(bookingItem.Status__c)) {
                    allocatedBRI.add(bookingItem);
                }
            }
        }
        if (briToUpdate != null && !briToUpdate.isEmpty()) {
            update briToUpdate;
        }

        if (allocatedBRI != null && !allocatedBRI.isEmpty()) {
            List<Tipi_Payments__c> paymentsForRentableItems =
                    BookedRentableItemTriggerHelper.buildTipiPaymentRecordsForBRI(allocatedBRI);
            List<Tipi_Payments__c> existedPayments = [
                SELECT Id, Payment_Date__c, Status__c, Opportunity__c
                FROM Tipi_Payments__c
                WHERE
                    Rentable_Item__c IN :rentableItemIds AND
                    Opportunity__c IN :opportunityIds
                LIMIT 10000
            ];

            Map<Date, Tipi_Payments__c> existedPaymentByDate = new Map<Date, Tipi_Payments__c>();
            List<Tipi_Payments__c> paymentsToCancel = new List<Tipi_Payments__c>();
            for (Tipi_Payments__c payment : existedPayments) {
                existedPaymentByDate.put(payment.Payment_Date__c, payment);
                if (dateChanged.containsKey(payment.Opportunity__c)) {
                    Date firstDayOfEndDateMonth = dateChanged.get(payment.Opportunity__c).toStartOfMonth();
                    if (payment.Payment_Date__c >= firstDayOfEndDateMonth) {
                        payment.Status__c = Utils_Constants.CANCELLED_STATUS;
                        paymentsToCancel.add(payment);
                    }
                }
            }
            if (!paymentsToCancel.isEmpty()) {
                update paymentsToCancel;
            }

            List<Tipi_Payments__c> paymentsToInsert = new List<Tipi_Payments__c>();
            for (Tipi_Payments__c payment : paymentsForRentableItems) {
                if (!existedPaymentByDate.containsKey(payment.Payment_Date__c) && payment.Payment_Date__c.day() == 1) {
                    paymentsToInsert.add(payment);
                } else {
                    if (existedPaymentByDate.get(payment.Payment_Date__c) != null &&
                                    existedPaymentByDate.get(payment.Payment_Date__c).Status__c ==
                                    Utils_Constants.CANCELLED_STATUS) {
                        paymentsToInsert.add(payment);
                    }
                }
            }
            if (!paymentsToInsert.isEmpty()) {
                insert paymentsToInsert;
            }
        }

    }

    private static List<Tipi_Payments__c> buildTipiPaymentRecords(Set<Id> contractIdsToBuildPayments) {
        List<Tipi_Payments__c> resultList = new List<Tipi_Payments__c>();
        List<Opportunity> opportunitiesList = new List<Opportunity>();

        Map<Id, Contract> items = new Map<Id, Contract>(
                (List<Contract>)new LeaseSelector().getLeaseRecords(contractIdsToBuildPayments, new Set<String>{
                        Contract.Type__c.getDescribe().getName(),
                        Contract.SBQQ__Opportunity__c.getDescribe().getName(),
                        Contract.Payment_Config_Done__c.getDescribe().getName(),
                        Contract.StartDate.getDescribe().getName(),
                        Contract.EndDate.getDescribe().getName(),
                        Contract.Actual_Tenancy_End_Date__c.getDescribe().getName(),
                        Contract.External_Id__c.getDescribe().getName(),
                        'SBQQ__Opportunity__r.SBQQ__PrimaryQuote__c',
                        'SBQQ__Opportunity__r.Deposit__c',
                        'Unit__r.Building__r.Development_Site__c',
                        'SBQQ__Opportunity__r.Primary_Contact__r.Name',
                        'SBQQ__Opportunity__r.Primary_Contact__r.External_Id__c',
                        'SBQQ__Opportunity__r.Tenant_2__r.Name',
                        'SBQQ__Opportunity__r.Tenant_2__r.External_Id__c',
                        'SBQQ__Opportunity__r.Tenant_3__r.Name',
                        'SBQQ__Opportunity__r.Tenant_3__r.External_Id__c',
                        'SBQQ__Opportunity__r.Tenant_4__r.Name',
                        'SBQQ__Opportunity__r.Tenant_4__r.External_Id__c',
                        'SBQQ__Opportunity__r.Tenant_5__r.Name',
                        'SBQQ__Opportunity__r.Tenant_5__r.External_Id__c',
                        'SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Checkbox__c',
                        'SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Length__c',
                        'SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Amount__c',
                        'SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_Checkbox__c',
                        'SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_First_Month__c',
                        'SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Checkbox__c',
                        'SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Text__c',
                        'SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Unit_Rent_Rollup__c'
                        }));

        for (Id itemId : items.keySet()) {
            opportunitiesList.add(items.get(itemId).SBQQ__Opportunity__r);
        }

        Map<Id, List<Tenant>> tenants = createTenantsList(getOpportunitiesContacts(opportunitiesList));

        for (Id contr : tenants.keySet()) {
            PaymentDataWrapper wrapper = new PaymentDataWrapper(
                    items.get(contr).Payment_Config_Done__c,
                    items.get(contr).SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Unit_Rent_Rollup__c,
                    Decimal.valueOf(0),
                    '',
                    Decimal.valueOf(0),
                    items.get(contr).SBQQ__Opportunity__r.Deposit__c,
                    tenants.get(contr),
                    '',
                    new List<String>(),
                    new List<PaymentDataWrapper.PicklistWrapper>(),
                    new List<PaymentDataWrapper.PicklistWrapper>(),
                    Date.newInstance(2000, 1, 1),
                    Date.newInstance(2000, 1, 1),
                    0
            );
            List<Tipi_Payments__c> payments =
                    PaymentConfiguratorService.constructPaymentRecords(items.get(contr), wrapper, true);
            System.debug(payments.size());
            resultList.addAll(payments);
        }
        return resultList;
    }

    private static Map<Id, List<Contact>> getOpportunitiesContacts(List<Opportunity> opportunityRecords) {

        Map<Id, List<Contact>> contactsFromOpportunity = new Map<Id, List<Contact>>();

        for (Opportunity opp : opportunityRecords) {
            if (!contactsFromOpportunity.containsKey(opp.Id)) {
                contactsFromOpportunity.put(opp.Id, new List<Contact>());
            }

            contactsFromOpportunity.get(opp.Id).add(opp.Primary_Contact__r);

            if (!String.isBlank(opp.Tenant_2__c)) {
                contactsFromOpportunity.get(opp.Id).add(opp.Tenant_2__r);
            }
            if (!String.isBlank(opp.Tenant_3__c)) {
                contactsFromOpportunity.get(opp.Id).add(opp.Tenant_3__r);
            }
            if (!String.isBlank(opp.Tenant_4__c)) {
                contactsFromOpportunity.get(opp.Id).add(opp.Tenant_4__r);
            }
            if (!String.isBlank(opp.Tenant_5__c)) {
                contactsFromOpportunity.get(opp.Id).add(opp.Tenant_5__r);
            }
        }

        return contactsFromOpportunity;
    }


    private static Map<Id, List<Tenant>> createTenantsList(Map<Id, List<Contact>> opportunitiesContacts) {
        Map<Id, List<Tenant>> contractToTenantMap = new Map<Id, List<Tenant>>();
        List<Contact> contactsList = new List<Contact>();

        for (Id oppId : opportunitiesContacts.keySet()) {
            contactsList.addAll(opportunitiesContacts.get(oppId));
        }

        List<Tipi_Payments__c> payments = [
            SELECT Id, Calculation_Long__c, Amount__c, Contact__c, Contact__r.External_Id__c, External_Id__c,
                Opportunity__r.SBQQ__PrimaryQuote__r.Unit_Rent_Rollup__c,Opportunity__r.Deposit__c,
                Contact__r.Name, Opportunity__r.ContractId, isPartialRent__c, Opportunity__r.Contract.External_Id__c, Amount_without_Concession__c
            FROM Tipi_Payments__c
            WHERE
                Contact__c IN :contactsList AND
                Opportunity__c IN :opportunitiesContacts.keySet()
            ORDER BY Payment_Date__c ASC
            LIMIT 10000
        ];

        Map<String, Decimal> residentToAmount = getYardiResidentsAmount(payments);

        Map<Id, Map<Id, Decimal>> amountForContact = new Map<Id, Map<Id, Decimal>>();
        Map<Id, Opportunity> oppById = new Map<Id, Opportunity>();
        for (Tipi_Payments__c payment : payments) {
            oppById.put(payment.Opportunity__c, payment.Opportunity__r);
            if (!amountForContact.containsKey(payment.Opportunity__c)) {
                amountForContact.put(payment.Opportunity__c, new Map<Id, Decimal>());
            }
            if (String.isNotBlank(payment.Calculation_Long__c) && payment.Calculation_Long__c.contains('Rent (')) {
                amountForContact.get(payment.Opportunity__c).put(payment.Contact__c, payment.Amount__c);
            }
            if (residentToAmount.containsKey(payment.Contact__r.External_Id__c)) {
                amountForContact.get(payment.Opportunity__c).put(payment.Contact__c, residentToAmount.get(payment.Contact__r.External_Id__c));
            }
        }

        for (Id oppId : opportunitiesContacts.keySet()) {
            for (Contact contactItem : opportunitiesContacts.get(oppId)) {
                Opportunity opportunityRecord = oppById.containsKey(oppId) ? oppById.get(oppId) : null;
                if (opportunityRecord != null) {
                    Tenant ten = new Tenant(
                        contactItem.Id,
                        contactItem.Name,
                        null,
                        null
                    );
                    ten.isPayee = true;

                    if (amountForContact.containsKey(oppId) && amountForContact.get(oppId).containsKey(contactItem.Id)) {
                        ten.rent = amountForContact.get(oppId).get(contactItem.Id);
                    } else {
                        ten.rent = opportunityRecord.SBQQ__PrimaryQuote__r.Unit_Rent_Rollup__c;
                    }

                    ten.depositPayment = opportunityRecord.Deposit__c;

                    if (!contractToTenantMap.containsKey(opportunityRecord.ContractId)) {
                        contractToTenantMap.put(opportunityRecord.ContractId, new List<Tenant>());
                    }
                    contractToTenantMap.get(opportunityRecord.ContractId).add(ten);
                }
            }
        }

        return contractToTenantMap;
    }

    private static Map<String, Decimal> getYardiResidentsAmount(List<Tipi_Payments__c> payments) {
        Map<String, Decimal> residentToAmount = new Map<String, Decimal>();
        Set<String> yardiCodes = new Set<String>();

        for (Tipi_Payments__c payment : payments) {
            if (!String.isBlank(payment.Contact__r.External_Id__c)) {
                yardiCodes.add(payment.Contact__r.External_Id__c);
            }
        }

        List<Yardi_Data_Lease_Charge__c> yardiDataList = [SELECT Amount__c, From_Date__c, Charge_Type__c, Resident_Id__c
                                                          FROM Yardi_Data_Lease_Charge__c
                                                          WHERE From_Date__c < TODAY
                                                          AND Charge_Type__c = 'Rent'
                                                          AND Resident_Id__c IN :yardiCodes
                                                          ORDER BY From_Date__c];

        for (Yardi_Data_Lease_Charge__c yard : yardiDataList) {
            residentToAmount.put(yard.Resident_Id__c, yard.Amount__c);
        }

        if (residentToAmount.isEmpty()) {
            return new Map<String, Decimal>();
        } else {
            return residentToAmount;
        }
    }

}