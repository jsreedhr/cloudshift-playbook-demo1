/**
 * @Name:        EmailHandler
 * @Description: Class holds the logic for sending out Email Messages
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 19/06/2018    Patrick Fischer    Created Class
 */
public with sharing class EmailHandler {

    public static final String DEFAULT_ORG_WIDE_EMAIL_ADDRESS = 'support@org.com';

    /**
     * To send emails to a contacts for a given template
     *
     * @param sObjectsByContact Map of SObjects by Contact, i.e. Asperato Payment by Contact
     * @param templateName String name of the template to be send
     *
     * @return True if success, else false
     */
    public static Boolean sendEmailToContact(Map<Contact, SObject> sObjectsByContact, String templateName) {
        Id oweaId = getOrgWideEmailAddressDefaultId();
        return sendEmails(getEmailsForContacts(sObjectsByContact, templateName, oweaId));
    }

    /**
     * To send emails to a contacts for a given template and given Org Wide Email Adress
     *
     * @param sObjectsByContact Map of SObjects by Contact, i.e. Asperato Payment by Contact
     * @param templateName String name of the template to be send
     * @param orgWideAddress String email address for the org
     *
     * @return True if success, else false
     */
    public static Boolean sendEmailToContact(Map<Contact, SObject> sObjectsByContact, String templateName, String orgWideAddress) {
        Id oweaId = getOrgWideEmailAddressId(orgWideAddress);
        return sendEmails(getEmailsForContacts(sObjectsByContact, templateName, oweaId));
    }

    /**
     * To construct a List of Messaging.SingleEmailMessage Emails records to be send to Contacts
     *
     * @param sObjectsByContact Map of SObjects by Contact, i.e. Asperato Payment by Contact
     * @param template String name of the template to be send
     * @param oweaId Id of OrgWideEmailAddress record
     *
     * @return List of Emails to be send to Contacts
     */
    private static List<Messaging.SingleEmailMessage> getEmailsForContacts(Map<Contact, SObject> sObjectsByContact, String templateName, Id oweaId) {
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        Id templateId = getTemplateIdByName(templateName);

        if(templateId == null) {
            emails = null;
        } else {
            for (Contact contact : sObjectsByContact.keySet()) {
                emails.addAll(constructEmail(templateId, contact.Id, sObjectsByContact.get(contact).Id, oweaId, new List<String>{
                        contact.Email
                }));
            }
        }

        return emails;
    }

    /**
     * To construct a single an Email record
     *
     * @param templateId Id of EmailTemplate record
     * @param whoId Id of target record to be send (i.e. Contact Id)
     * @param whatId Id of the record to be send (i.e. Asperato Payment record Id)
     * @param oweaId Id of the OrgWideEmailAdress
     * @param emailAdressList List of Strings contained email adresses of the recipients
     *
     * @return List of Messaging.SingleEmailMessage to be send
     */
    @TestVisible
    private static List<Messaging.SingleEmailMessage> constructEmail(Id templateId, Id whoId, Id whatId, Id oweaId, List<String> emailAdressList) {
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

        if (templateId != null) {
            Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(templateId, whoId, whatId);
            mail.setTargetObjectId(whoId);
            mail.setSubject(mail.getSubject());
            mail.setHtmlBody(mail.getHtmlBody());
            mail.setBccSender(false);
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false);
            mail.setToAddresses(emailAdressList);
            if (oweaId != null) {
                mail.setOrgWideEmailAddressId(oweaId);
            }
            emails.add(mail);
        }

        return emails;
    }

    /**
     * To send out a List of Messaging.SingleEmailMessage records
     *
     * @param emailsToSend List of Messaging.SingleEmailMessage records
     *
     * @return True if success, else false
     */
    @TestVisible
    private static Boolean sendEmails(List<Messaging.SingleEmailMessage> emailsToSend) {
        Boolean isSuccess = true;

        try {
            if (emailsToSend == null) {
                isSuccess = false;
            } else if (!emailsToSend.isEmpty()) {
                Messaging.sendEmail(emailsToSend);
            }
        } catch (Exception e) {
            isSuccess = false;
            CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, e);
            CustomLogger.save();
        }

        return isSuccess;
    }

    /**
     * To retrieve an EmailTemplate record Id by a templates Name or Developer Name
     *
     * @param templateName String Name or Developer Name of the EmailTemplate to retrieve
     *
     * @return Id of the retrieved EmailTemplate record, else null if not found
     */
    @TestVisible
    private static Id getTemplateIdByName(String templateName) {
        List<EmailTemplate> templates = [
                SELECT Id, DeveloperName, Subject, Body, HtmlValue
                FROM EmailTemplate
                WHERE DeveloperName LIKE :templateName OR Name LIKE :templateName
                LIMIT 1
        ];

        if (!templates.isEmpty()) {
            return templates[0].Id;
        }

        return null;
    }

    /**
     * To retrieve the default OrgWideEmailAddress record Id
     *
     * @return Id of the retrieved OrgWideEmailAddress record, else null if not found
     */
    private static Id getOrgWideEmailAddressDefaultId() {
        return getOrgWideEmailAddressId(DEFAULT_ORG_WIDE_EMAIL_ADDRESS);
    }

    /**
     * To retrieve an OrgWideEmailAddress record Id by querying an org wide address
     *
     * @param orgWideAddress String of an Email address to retrieve the Id for
     *
     * @return Id of the retrieved OrgWideEmailAddress record, else null if not found
     */
    private static Id getOrgWideEmailAddressId(String orgWideAddress) {
        List<OrgWideEmailAddress> owea = [
                SELECT Id
                FROM OrgWideEmailAddress
                WHERE Address = :orgWideAddress
                LIMIT 1
        ];

        if (!owea.isEmpty()) {
            return owea[0].Id;
        }

        return null;
    }
}