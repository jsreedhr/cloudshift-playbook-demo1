/**
    * @Name:        NewViewingController
    * @Description: Controller class for NewViewing Lightning Component
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 29/03/2018    Eugene Konstantinov    Created Class
    * 21/06/2018    Eugene Konstantinov    Added Building Filter functionality; updated comments
*/
public with sharing class NewViewingController {
    private static Integer counter;

    /**
    * Method for getting List of Units for NewViewing Lightning Component
    * @param furniture string
    * @param bedrooms list of string
    * @param minPrice string value for minPrice
    * @param maxPrice string value for maxPrice
    * @param houseShare boolean value
    * @param shortTerm boolean value
    * @param accessibility boolean value
    * @param parking boolean value
    * @param storage boolean value
    * @param pets boolean value
    * @param dateAvailable string value
    *
    * @returns List<Unit__c> list of selected unit records
    */
    @AuraEnabled
    public static List<Unit__c> getUnits(
        String furniture, String[] bedrooms, String[] buildings, String minPrice,
        String maxPrice, Boolean houseShare, Boolean shortTerm,
        Boolean accessibility, Boolean parking, Boolean storage, Boolean pets, String dateAvailable) {
            String minPriceUpdated = minPrice != null ? minPrice.replaceAll('[A-z. ]', '') : '';
            String maxPriceUpdated = maxPrice != null ? maxPrice.replaceAll('[A-z. ]', '') : '';
            Set<Id> buildingIds = new Set<Id>();
            for (String bId : buildings) {
                buildingIds.add(Id.valueOf(bId));
            }
            List<Unit__c> sortedUnits = Database.query(getRequestString(furniture, bedrooms, buildingIds, minPriceUpdated, maxPriceUpdated, pets, dateAvailable));
            List<Unit__c> units = new List<Unit__c>();
            if (houseShare == false && shortTerm == false && accessibility == false && parking == false && storage == false) {
                return sortedUnits;
            } else {
                List<Rentable_Item__c> items = getRentableItemRequestString(houseShare, shortTerm, accessibility, parking, storage);
                for (Unit__c unit : sortedUnits) {
                    Set<String> idSet = new Set<String>();
                    for	(Rentable_Item__c item : items) {
                        if (unit.Building__c == item.Building__c) {
                            idSet.add(item.RecordTypeId);
                        }
                    }
                    if (counter == idSet.size()) {
                        units.add(unit);
                    }
                }
            }
            return units;
        }

    /**
    * Method for getting Contact Information by Opportunity for NewViewing Lightning Component
    * @param opportunity Opportunity record Id
    *
    * @returns Opportunity with Contact information
    */
    @AuraEnabled
    public static Opportunity getContact(String opportunity) {
        Opportunity opp = [
            SELECT
                Id,
                Primary_Contact__r.Furniture__c,
                Primary_Contact__r.Bedrooms__c,
                Primary_Contact__r.Preferred_Rent__c,
                Primary_Contact__r.Move_In_Date__c,
                Primary_Contact__r.Min_Price_Range__c,
                Primary_Contact__r.Max_Price_Range__c
            FROM Opportunity
            WHERE
                Id = :opportunity
            LIMIT 1
        ];
        return opp;
    }

    /**
    * Method for getting Picklist Values for NewViewing Lightning Component
    * @param ObjectApiName string value
    * @param FieldApiName string value
    *
    * @returns List<String> list of picklist values for FieldApiName on ObjectApiName
    */
    @AuraEnabled
    public static List<String> getPickListValuesIntoList(String ObjectApiName, String FieldApiName) {
        List<String> pickListValuesList = new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApiName);
        Sobject ObjectName = targetType.newSObject();
        Schema.sObjectType sobject_type = ObjectName.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        List<Schema.PicklistEntry> pick_list_values = field_map.get(FieldApiName).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pick_list_values) {
            pickListValuesList.add(a.getValue());
        }
        return pickListValuesList;
    }

    /**
    * Method for getting Buildings List for NewViewing Lightning Component
    *
    * @returns List<Building__c> list of all Building__c records
    */
    @AuraEnabled
    public static List<Building__c> getBuildingsList() {
        List<Building__c> pickListValuesList = new List<Building__c>();
        pickListValuesList = (List<Building__c>)new BuildingSelector().getAllRecords(new Set<String>{
            Building__c.Name.getDescribe().getName()
        });
        return pickListValuesList;
    }

    /**
    * Method for getting Picklist Values for NewViewing Lightning Component
    * @param opportunityId string value of oportunity recrd Id
    * @param requestedStartDate datetime
    * @param units list of Unit records
    * @param attendeeInfo string value
    */
    @AuraEnabled
    public static void saveNewViewing(String opportunityId, Datetime requestedStartDate, List<Unit__c> units, String attendeeInfo, String tipiStaffId) {
        Opportunity opportunity = [SELECT AccountId, Primary_Contact__r.Email, Id, CreatedById FROM Opportunity WHERE Id = :opportunityId];
        Viewing__c newViewing = new Viewing__c (
            Account__c = opportunity.AccountId,
            Attendee_Information__c = attendeeInfo,
            Contact_Email__c = opportunity.Primary_Contact__r.Email,
            Opportunity__c = opportunity.Id,
            Status__c = 'Tipi Confirmed',
            Tipi_Staff__c = tipiStaffId,
            Requested_Start_Date__c = requestedStartDate
        );
        insert newViewing;
        List<Viewing_Units__c> newUnits = new List<Viewing_Units__c>();
        for (Unit__c unit : units) {
            Viewing_Units__c newViewingUnit = new Viewing_Units__c (
                Viewing__c = newViewing.Id,
                Unit__c = unit.Id
            );
            newUnits.add(newViewingUnit);
        }
        insert newUnits;
    }

    /**
    * Method to build Request string from Unit object
    * @param furniture string
    * @param bedrooms list of string
    * @param buildings list of string
    * @param minPrice string value for minPrice
    * @param maxPrice string value for maxPrice
    * @param pets boolean value
    * @param dateAvailable string value
    *
    * @returns String request string
    */
    private static String getRequestString(String furniture, String[] bedrooms, Set<Id> buildingIds, String minPrice, String maxPrice, Boolean pets, String dateAvailable) {
        String updatedRequest = 'SELECT Name, Unit_Type__c, Number_Of_Bedrooms__c, Furnished__c, Rent__c, Building__c, Building__r.Allows_Pets__c, Building__r.Name FROM Unit__c ';

        if (String.isBlank(furniture) && bedrooms.size() == 0 && buildingIds.size() == 0 && String.isBlank(minPrice) && String.isBlank(maxPrice) && !pets && String.isBlank(dateAvailable)) {
            return updatedRequest;
        } else {
            String ending = ' AND';
            String orBeddrooms = ' OR';
            updatedRequest += ' WHERE';
            if (String.isNotBlank(dateAvailable)) {
                Date available = Date.valueOf(dateAvailable);
                String dateStr = DateTime.newInstance(available.year(),available.month(),available.day()).format('yyyy-MM-dd');
                updatedRequest += ' Date_Available__c <= ' + dateStr + ending;
            }
            if (String.isNotBlank(furniture) && !furniture.equals('--None--')) {
                updatedRequest += ' Furnished__c = \'' + furniture + '\'' + ending;
            }
            if (bedrooms.size() == 1) {
                updatedRequest += ' Unit_Type__c = \'' + bedrooms[0] + '\'' + ending;
            }
            if (bedrooms.size() > 1) {
                updatedRequest += ' (';
                for(String bedroom : bedrooms) {
                    updatedRequest += ' Unit_Type__c = \'' + bedroom + '\'' + orBeddrooms;
                }
                updatedRequest = updatedRequest.removeEnd(' OR');
                updatedRequest += ')' + ending;
            }
            if (buildingIds.size() > 0) {
                updatedRequest += ' Building__c IN :buildingIds ' + ending;
            }

            if (String.isNotBlank(minPrice) && !minPrice.equals('----')) {
                updatedRequest += ' Rent__c >= ' + minPrice + ending;
            }
            if (String.isNotBlank(maxPrice) && !maxPrice.equals('----')) {
                updatedRequest += ' Rent__c <= ' + maxPrice + ending;
            }
            if (pets) {
                updatedRequest += ' Building__r.Allows_Pets__c = ' + pets;
            }
        }
        updatedRequest = updatedRequest.removeEnd(' WHERE');
        updatedRequest = updatedRequest.removeEnd(' AND');
        System.debug(updatedRequest);
        return updatedRequest;
    }

    /**
    * Method to get list of Rentable Item records from salesforce
    * @param houseShare boolean value
    * @param shortTerm boolean value
    * @param accessibility boolean value
    * @param parking boolean value
    * @param storage boolean value
    *
    * @returns List<Rentable_Item__c> list of Rentable Item records
    */
    private static List<Rentable_Item__c> getRentableItemRequestString (
        Boolean houseShare, Boolean shortTerm, Boolean accessibility,
        Boolean parking, Boolean storage) {
            Integer count = 0;
            String updatedRequest = 'SELECT Building__c, RecordType.Name, RecordTypeId FROM Rentable_Item__c WHERE RecordType.Name IN (';
            if (houseShare == true) {
                updatedRequest += '\'Sharing\', ';
                count++;
            }
            if (shortTerm == true) {
                updatedRequest += '\'Short Term\', ';
                count++;
            }
            if (accessibility == true) {
                updatedRequest += '\'Accessible\', ';
                count++;
            }
            if (parking == true) {
                updatedRequest += '\'Parking\', ';
                count++;
            }
            if (storage == true) {
                updatedRequest += '\'Storage\', ';
                count++;
            }
            updatedRequest = updatedRequest.removeEnd(', ');
            updatedRequest += ')';
            System.debug(updatedRequest);
            List<Rentable_Item__c> items = Database.query(updatedRequest);
            counter = count;
            return items;
        }
}