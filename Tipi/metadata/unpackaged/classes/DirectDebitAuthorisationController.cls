/**
    * @Name:        DirectDebitAuthorisationController
    * @Description: This is controller for direct debit authoriaztion quick action
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 15/05/2018    Andrey Korobovich    Created Class
    * 14/06/2018    Eugene Konstantinov  Added Billing Address for Authorisation
*/

public with sharing class DirectDebitAuthorisationController {
    private static final String DIRECT_DEBIT = 'Direct Debit';

    @AuraEnabled
    public static void directDebitAuthorization(Id contactId) {
        Contact currentContact = [
            SELECT Id, FirstName, LastName, Email, Account.Name, MailingCountry, MailingState, MailingCity, MailingPostalCode, MailingStreet
            FROM Contact
            WHERE
                Id = :contactId
            LIMIT 1
        ];

        asp04__Authorisation__c newAuth = new asp04__Authorisation__c();
        newAuth.Contact__c = currentContact.Id;
        newAuth.asp04__Payment_Route_Selected__c = DIRECT_DEBIT;
        newAuth.asp04__Payment_Route_Options__c = DIRECT_DEBIT;
        newAuth.asp04__Account_Name__c = currentContact.Account.Name;
        newAuth.asp04__First_Name__c = currentContact.FirstName;
        newAuth.asp04__Last_Name__c = currentContact.LastName;
        newAuth.asp04__Email__c = currentContact.Email;
        newAuth.asp04__Billing_Address_Street__c = currentContact.MailingStreet;
        newAuth.asp04__Billing_Address_City__c = currentContact.MailingCity;
        newAuth.asp04__Billing_Address_State__c = currentContact.MailingState;
        newAuth.asp04__Billing_Address_PostalCode__c = currentContact.MailingPostalCode;
        newAuth.asp04__Billing_Address_Country__c = currentContact.MailingCountry;
        newAuth.asp04__Status__c = 'Pending';
        insert newAuth;

        EmailHandler.sendEmailToContact( new Map<Contact, asp04__Authorisation__c> { currentContact => newAuth}, 'Payment Authorisation');
    }

}