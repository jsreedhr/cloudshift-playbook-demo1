/**
* @Name:        FeedItemTriggerTest
* @Description: Test class of FeedItemTriggerHandler
*
* @author:      Patrick Fischer
* @version:     1.0
* Change Log
*
* Date          author              Change Description
* -----------------------------------------------------------------------------------
* 12/06/2018    Patrick Fischer     Created Class
*/
@IsTest
public with sharing class FeedItemTriggerTest {

    /**
     * Method tested: beforeInsert()
     * Expected result: Successfully inserted FeedItem record
     */
    private static testMethod void beforeInsertSuccess() {
        TestFactory.createSObject(new Global_Application_Settings__c(), true);

        FeedItem feedItem = new FeedItem(
                Body = 'Test',
                ParentId = UserInfo.getUserId()
        );

        Test.startTest();
        insert feedItem;
        Test.stopTest();

        List<FeedItem> feedItems = [SELECT Id FROM FeedItem];
        System.assertEquals(1, feedItems.size(), 'Expecting 1 FeedItem to exist');
    }

    /**
     * Method tested: beforeUpdate()
     * Expected result: Successfully updated FeedItem record
     */
    private static testMethod void beforeUpdateSuccess() {
        TestFactory.createSObject(new Global_Application_Settings__c(), true);

        FeedItem feedItem = new FeedItem(
                Body = 'Test',
                ParentId = UserInfo.getUserId()
        );
        insert feedItem;
        feedItem.Body = 'Test2';

        Test.startTest();
        update feedItem;
        Test.stopTest();

        List<FeedItem> feedItems = [SELECT Id, Body FROM FeedItem];
        System.assertEquals(1, feedItems.size(), 'Expecting 1 FeedItem to exist');
        System.assertEquals('Test2', feedItems[0].Body + '', 'Expecting Changed FeedItem Body');
    }

    /**
     * Method tested: verifyMentions()
     * Expected result: Successfully verified FeedItem record without Community user mentions
     */
    private static testMethod void verifyMentionsSuccess() {
        TestFactory.createSObject(new Global_Application_Settings__c(), true);

        FeedItem feedItem = new FeedItem(
                Body = 'Test',
                ParentId = UserInfo.getUserId()
        );
        String serialisedFeedItem = JSON.serialize(feedItem);
        serialisedFeedItem = serialisedFeedItem.substring(0, serialisedFeedItem.length() - 1)
                + ',"RawBody":"<p>adding a mention here {@' + UserInfo.getUserId() +'}​ test</p>"}';
        feedItem = (FeedItem) JSON.deserialize(serialisedFeedItem, FeedItem.class);

        Test.startTest();
        Boolean isValid = FeedItemTriggerHelper.verifyMentions(new List<FeedItem> { feedItem });
        Test.stopTest();

        System.assert(isValid, 'Expecting no Errors to be thrown.');
        System.assert(ApexPages.getMessages().isEmpty(), 'Expecting no Errors to be thrown.');
    }

    /**
     * Method tested: verifyMentions()
     * Expected result: Error thrown due to Community user being mentioned in Chatter Post
     */
    private static testMethod void verifyMentionsErrorThrown() {
        Id communityProfileId = [SELECT Id FROM Profile WHERE Name LIKE '%Community%' LIMIT 1].Id;

        Account account = new Account(Name ='Test') ;
        insert account;

        // Creating Community User
        Contact contact = new Contact(LastName ='testCon', AccountId = account.Id);
        insert contact;

        User user = new User(
                Alias = 'test123',
                ContactId = contact.Id,
                Email = 'test123@noemail.com',
                EmailEncodingKey='UTF-8',
                IsActive = true,
                LanguageLocaleKey='en_US',
                LastName='Testing',
                LocaleSidKey='en_US',
                ProfileId = communityProfileId,
                TimeZoneSidKey='America/Los_Angeles',
                Username='tester@noemail.com'
        );
        insert user;

        TestFactory.createSObject(new Global_Application_Settings__c(), true);

        FeedItem feedItem = new FeedItem(
                Body = 'Test',
                ParentId = UserInfo.getUserId()
        );
        String serialisedFeedItem = JSON.serialize(feedItem);
        serialisedFeedItem = serialisedFeedItem.substring(0, serialisedFeedItem.length() - 1)
                + ',"RawBody":"<p>adding a mention here {@' + user.Id +'}​ test</p>"}';
        feedItem = (FeedItem) JSON.deserialize(serialisedFeedItem, FeedItem.class);

        Test.startTest();
        Boolean isValid = FeedItemTriggerHelper.verifyMentions(new List<FeedItem> { feedItem });
        Test.stopTest();

        System.assert(!isValid, 'Expecting Error to be thrown due to Community/Portal User Id being mentioned.');
        System.assertEquals(System.Label.ERROR_CHATTER_POST_MENTIONS_CUSTOMER, ApexPages.getMessages().get(0).getDetail() + '',
                'Expecting Error to be thrown due to Community/Portal User Id being mentioned.');
    }
}