/**
    * @Name:        ZooplaSendPropertyStructure
    * @Description: Wrapper class for creating request structure to Zoopla
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 02/05/2018    Andrey Korobovich    Created Class
*/
public with sharing class ZooplaSendPropertyStructure {
	
	public Boolean accessibility {get;set;}  
	public String administration_fees {get;set;}
	public Double annual_business_rates {get;set;}   
	public Integer available_bedrooms {get;set;}  
	public String available_from_date {get;set;} 
	public Boolean basement {get;set;}
	public Integer bathrooms {get;set;} 
	public Boolean burglar_alarm {get;set;} 
	public Boolean business_for_sale {get;set;}
	public List<String> buyer_incentives {get;set;} // ???
	public String branch_reference {get;set;}
	public String category {get;set;} 
	public String central_heating {get;set;}
	public Boolean chain_free {get;set;} 
	public List<String> commercial_use_classes {get;set;} 
	public List<String> connected_utilities {get;set;} // ???
	public Boolean conservatory {get;set;} 
	public Integer construction_year {get;set;} 
	public List<Content> content {get;set;} 
	public String council_tax_band {get;set;} 
	public String decorative_condition {get;set;} 
	public Double deposit {get;set;} 
	public List<Detailed_description> detailed_description {get;set;} 
	public String display_address {get;set;}
	public Boolean double_glazing {get;set;} 
	public Epc_ratings epc_ratings {get;set;} // ???
	public List<String> feature_list {get;set;} 
	public Boolean fireplace {get;set;} 
	public Boolean fishing_rights {get;set;} 
	public List<Integer> floor_levels {get;set;} 
	public Integer floors {get;set;} 
	public String furnished_state {get;set;} 
	public Google_street_view google_street_view {get;set;} 
	public Double ground_rent {get;set;} // ???
	public Boolean gym {get;set;} 
	public Lease_expiry lease_expiry {get;set;} // ???
	public String listed_building_grade {get;set;} 
	public String listing_reference {get;set;} 
	public Integer living_rooms {get;set;} 
	public Location location {get;set;}
	public Boolean loft {get;set;} 
	public Boolean new_home {get;set;} 
	public String open_day {get;set;} 
	public Boolean outbuildings {get;set;} 
	public List<String> outside_space {get;set;} 
	public List<String> parking {get;set;} // ???
	public Boolean pets_allowed {get;set;} 
	public Boolean porter_security {get;set;} 
	public Pricing pricing {get;set;} 
	public String property_type {get;set;} 
	public Double rateable_value {get;set;} 
	public Rental_term rental_term {get;set;} 
	public Boolean repossession {get;set;} 
	public Boolean retirement {get;set;} 
	public Integer sap_rating {get;set;} 
	public Service_charge service_charge {get;set;} 
	public Boolean serviced {get;set;} 
	public Boolean shared_accommodation {get;set;} 
	public String summary_description {get;set;} 
	public Boolean swimming_pool {get;set;} 
	public Boolean tenanted {get;set;} 
	public Tenant_eligibility tenant_eligibility {get;set;} 
	public Boolean tennis_court {get;set;} 
	public String tenure {get;set;} 
	public Integer total_bedrooms {get;set;} 
	public Boolean utility_room {get;set;} 
	public Boolean waterfront {get;set;} 
	public Boolean wood_floors {get;set;} 
	public Areas areas {get;set;} 
	public List<String> bills_included {get;set;} 
	public String life_cycle_status {get;set;} 

	public class Lease_expiry {
		public Integer years_remaining {get;set;} 
	}
	
	public class Minimum {
		public Double value {get;set;} 
		public String units {get;set;} 

		public Minimum(Double value, String units){
			this.value = value;
			this.units = units;
		}
	}
	
	public class Epc_ratings {
		public Integer eer_current_rating {get;set;} 
		public Integer eer_potential_rating {get;set;} 
		public Integer eir_current_rating {get;set;} 
		public Integer eir_potential_rating {get;set;}
	}
	
	public class Rental_term {
		public Integer minimum_length {get;set;} 
		public String units {get;set;} 
	}
	
	public class Coordinates {
		public Double latitude {get;set;} 
		public Double longitude {get;set;}

		public Coordinates(Double latitude, Double longitude){
			this.latitude = latitude;
			this.longitude = longitude;
		} 
	}
	
	public class Internal {
		public Minimum minimum {get;set;} 
		public Minimum maximum {get;set;} 

		public Internal(Minimum minimum, Minimum maximum){
			this.minimum = minimum;
			this.maximum = maximum;
		}
	}
	
	public class Dimensions {
		public Integer length {get;set;} 
		public Integer width {get;set;} 
		public String units {get;set;} 
	}
	
	public class Service_charge {
		public Integer charge {get;set;} 
		public String per_unit_area_units {get;set;} 
		public String frequency {get;set;} 
	}
	
	public class Price_per_unit_area {
		public Double price {get;set;} 
		public String units {get;set;} 
	}
	
	public class Detailed_description {
		public String heading {get;set;} 
		public Dimensions dimensions {get;set;} 
		public String text {get;set;} 

		public Detailed_description(String heading, Dimensions dimensions, String text) {
			this.heading =  heading;
			this.dimensions = dimensions;
			this.text = text;
		}
	}
	
	public class Areas {
		public Internal internal {get;set;} 
		public Internal external {get;set;} 

		public Areas(Internal internal, Internal external) {
			this.internal = internal;
			this.external = external;
		}
	}
	
	public class Content {
		public String url {get;set;} 
		public String type_Z {get;set;} // in json: type
		public String caption {get;set;} 

		public Content(String url, String type_Z, String caption) {
			this.url = url;
			this.type_Z = type_Z;
			this.caption = caption;
		}
	}
	
	public class Pricing {
		public Double price {get;set;} 
		public String price_qualifier {get;set;} 
		public Boolean auction {get;set;} 
		public String rent_frequency {get;set;} 
		public String currency_code {get;set;} 
		public Price_per_unit_area price_per_unit_area {get;set;} 
		public String transaction_type {get;set;} 

		public Pricing(Double price, String price_qualifier, Boolean auction, String rent_frequency, String currency_code, Price_per_unit_area price_per_unit_area, String transaction_type) {
			this.price = price;
			this.price_qualifier = price_qualifier;
			this.auction = auction;
			this.rent_frequency = rent_frequency;
			this.currency_code = currency_code;
			this.price_per_unit_area = price_per_unit_area;
			this.transaction_type = transaction_type;
		}
	}
	
	public class Tenant_eligibility {
		public String dss {get;set;} 
		public String students {get;set;} 
	}
	
	public class Google_street_view {
		public Google_street_view (Coordinates coordinates, Double heading, Double pitch){
			this.coordinates = coordinates;
			this.heading = heading;
			this.pitch = pitch;
		}
		public Coordinates coordinates {get;set;} 
		public Double heading {get;set;} 
		public Double pitch {get;set;} 
	}
	
	public class Paf_address {
		public String address_key {get;set;} 
		public String organisation_key {get;set;} 
		public String postcode_type {get;set;} 
	}
	
	public class Location {
		public String property_number_or_name {get;set;} 
		public String street_name {get;set;} 
		public String locality {get;set;} 
		public String county {get;set;} 
		public String town_or_city {get;set;} 
		public String postal_code {get;set;} 
		public String country_code {get;set;} 
		public Coordinates coordinates {get;set;} 
		public Paf_address paf_address {get;set;} 
		public String paf_udprn {get;set;} 

		public Location(String property_number_or_name, String street_name, String county, String town_or_city, String postal_code, String country_code, Coordinates coordinates) {
			this.property_number_or_name = property_number_or_name;
			this.street_name = street_name;
			this.county = county;
			this.town_or_city = town_or_city;
			this.postal_code = postal_code;
			this.country_code = country_code;
			this.coordinates = coordinates;
		}
	}
}