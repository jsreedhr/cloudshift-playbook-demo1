/**
    * @Name:        MyUnitCmpControllerTest
    * @Description: Test class for MyUnitCmpController functionality (MyUnit Lightning Component)
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 30/05/2018    Eugene Konstantinov    Created Class
*/
@isTest
public class MyUnitCmpControllerTest {

    @isTest
    static void testMyUnitCmpAsPortalUser() {
        User portalUser = TestFactoryPayment.createPortallUser();
        System.runAs(portalUser) {
            Test.startTest();
                System.assert(0 == MyUnitCmpController.getContactBookings().size());
                System.assert(0 == MyUnitCmpController.getUnitBookings().size());
            Test.stopTest();
        }
    }

    @isTest
    static void testMyUnitCmp() {
        Test.startTest();
            System.assert(null == MyUnitCmpController.getContactBookings());
            System.assert(null == MyUnitCmpController.getUnitBookings());
        Test.stopTest();
    }
}