/**
 * @Name:        TipiPaymentBatchTest
 * @Description: Test class for TipiPaymentBatch and TipiPaymentBatchScheduler
 *
 * @author:      Eugene Konstantinov
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 16/05/2018    Eugene Konstantinov    Created Class
 * 29/05/2018    Patrick Fischer        Class Refactored
 * 04/06/2018    Patrick Fischer        Additional scenarios added; Additional code coverage
 */
@IsTest
public class TipiPaymentBatchTest {

    private static final String TIPI_PAYMENT_BATCH = 'TipiPaymentBatch';

    /**
     * Method tested: executeBatch(new TipiPaymentBatch())
     * Expected result: Successfully iterate of payment record and create Asperato payment record
     */
    private static testMethod void executeBatchNewTipiPaymentBatch1MonthSuccess() {

        Test.startTest();
        Database.executeBatch(new TipiPaymentBatch(Date.newInstance(2014, 1, 1)), 20);
        Test.stopTest();

        List<AsyncApexJob> jobsApexBatch = getBatchApexJobs();
        System.assertEquals(1, jobsApexBatch.size(), 'Expecting one apex batch job');
        System.assertEquals(TIPI_PAYMENT_BATCH, jobsApexBatch[0].ApexClass.Name, 'Expecting specific batch job');

        System.assert(getAsperatoPayments().isEmpty(), 'No Payments created due to remaining Credit/Reservation');
        System.assert(getLogs().isEmpty(), 'No logs created');

        List<Tipi_Payments__c> debitTenant1 = [SELECT Id, Amount__c, Payment_Date__c, Status__c FROM Tipi_Payments__c WHERE Contact__r.FirstName = 'firstnamecontact0' AND
        Credit_Payment_Type__c = :Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT ORDER BY Payment_Date__c, Amount__c];
        System.assertEquals(13, debitTenant1.size(), 'Expecting 13 Payments: ' +
                '-> Collected From Customer (2):    Reservation 400; Credit 2600; ' +
                '-> Submitted For Collection (0):   ' +
                '-> Awaiting Submission (11):       Debit 3000; Debit 3000; Debit 3000; Debit 3000; Debit 3000; Debit 3000; Debit 3000; Debit 3000; Debit 3000; Debit 3000; Debit 3000; ');
        // Jan
        System.assertEquals(400, debitTenant1[0].Amount__c, 'Expecting Payment Amount of 400');
        System.assertEquals(Date.newInstance(2014, 1, 1), debitTenant1[0].Payment_Date__c, 'Expecting Date: 01 Jan');
        System.assertEquals(Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, debitTenant1[0].Status__c, 'Expecting Status: Collected from Customer');
        System.assertEquals(2600, debitTenant1[1].Amount__c, 'Expecting Payment Amount of 2600');
        System.assertEquals(Date.newInstance(2014, 1, 1), debitTenant1[1].Payment_Date__c, 'Expecting Date: 01 Jan');
        System.assertEquals(Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, debitTenant1[1].Status__c, 'Expecting Status: Collected from Customer');
        //Feb -> Dec
        for(Integer i = 0; i < 11; i++) {
            System.assertEquals(3000, debitTenant1[2 + i].Amount__c, 'Expecting Payment Amount of 3000');
            System.assertEquals(Date.newInstance(2014, 2 + i, 1), debitTenant1[2 + i].Payment_Date__c, 'Expecting Payment Date Feb to Dec');
            System.assertEquals(Utils_Constants.AWAITING_SUBMISSION_STATUS, debitTenant1[2 + i].Status__c, 'Expecting Status: Awaiting Submission');
        }

        List<Tipi_Payments__c> creditTenant1 = [SELECT Id, Amount__c, Total_Remaining_Balance__c, Payment_Date__c, Status__c, (SELECT Id FROM Tipi_Payments__r)
        FROM Tipi_Payments__c WHERE Contact__r.FirstName = 'firstnamecontact0' AND Credit_Payment_Type__c = :Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT ];
        System.assertEquals(10000, creditTenant1[0].Amount__c, 'Validating Credit of £10000');
        System.assertEquals(7400, creditTenant1[0].Total_Remaining_Balance__c, 'Expecting Remaining Balanace of £7400 (all credit assigned)');
        System.assertEquals(1, creditTenant1[0].Tipi_Payments__r.size(), 'Expecting 1 Payment assigned to this Credit');

        List<Tipi_Payments__c> reservationTenant1 = [SELECT Id, Amount__c, Total_Remaining_Balance__c, Payment_Date__c, Status__c, (SELECT Id FROM Tipi_Payments__r)
        FROM Tipi_Payments__c WHERE Contact__r.FirstName = 'firstnamecontact0' AND Type__c = :Utils_Constants.PAYMENT_TYPE_RESERVATION];
        System.assertEquals(400, reservationTenant1[0].Amount__c, 'Validating Credit of £400');
        System.assertEquals(0, reservationTenant1[0].Total_Remaining_Balance__c, 'Expecting Remaining Balanace of £0 (all reservation-credit assigned)');
        System.assertEquals(1, reservationTenant1[0].Tipi_Payments__r.size(), 'Expecting 1 Payment assigned to this reservation-credit');

        System.assertEquals(15, [SELECT Id FROM Tipi_Payments__c].size(), 'Expecting 15 Tipi_Payments__c records in total');
    }

    /**
     * Method tested: executeBatch(new TipiPaymentBatch())
     * Expected result: Successfully iterate of payment record and create Asperato payment record
     */
    private static testMethod void executeBatchNewTipiPaymentBatch4MonthsSuccess() {

        Date executionDate = Date.newInstance(2014, 4, 1);

        Test.startTest();
        Database.executeBatch(new TipiPaymentBatch(executionDate), 20); // rent payment begins Date.newInstance(2014, 1, 1)
        Test.stopTest();

        List<AsyncApexJob> jobsApexBatch = getBatchApexJobs();
        System.assertEquals(1, jobsApexBatch.size(), 'Expecting one apex batch job');
        System.assertEquals(TIPI_PAYMENT_BATCH, jobsApexBatch[0].ApexClass.Name, 'Expecting specific batch job');

        List<asp04__Payment__c> asperatoPayments = getAsperatoPayments();
        System.assertEquals(1, asperatoPayments.size(), '1 Payment created');
        System.assertEquals(1600, asperatoPayments[0].asp04__Amount__c, 'Expecting payment amount of £1600');

        List<Tipi_Payments__c> debitTenant1 = [SELECT Id, Amount__c, Payment_Date__c, Status__c FROM Tipi_Payments__c WHERE Contact__r.FirstName = 'firstnamecontact0' AND
        Credit_Payment_Type__c = :Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT ORDER BY Payment_Date__c, Amount__c];
        System.assertEquals(14, debitTenant1.size(), 'Expecting 14 Payments: ' +
                '-> Collected From Customer (5):    Reservation 400; Credit 2600; Credit 3000; Credit 3000; Credit 1400; ' +
                '-> Submitted For Collection (1):   Debit 1600; ' +
                '-> Awaiting Submission (8):        Debit 3000; Debit 3000; Debit 3000; Debit 3000; Debit 3000; Debit 3000; Debit 3000; Debit 3000; ');
        // Jan
        System.assertEquals(400, debitTenant1[0].Amount__c, 'Expecting Payment Amount of 400');
        System.assertEquals(Date.newInstance(2014, 1, 1), debitTenant1[0].Payment_Date__c, 'Expecting Date: 01 Jan');
        System.assertEquals(Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, debitTenant1[0].Status__c, 'Expecting Status: Collected from Customer');
        System.assertEquals(2600, debitTenant1[1].Amount__c, 'Expecting Payment Amount of 2600');
        System.assertEquals(Date.newInstance(2014, 1, 1), debitTenant1[1].Payment_Date__c, 'Expecting Date: 01 Jan');
        System.assertEquals(Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, debitTenant1[1].Status__c, 'Expecting Status: Collected from Customer');
        //Feb
        System.assertEquals(3000, debitTenant1[2].Amount__c, 'Expecting Payment Amount of 3000');
        System.assertEquals(Date.newInstance(2014, 2, 1), debitTenant1[2].Payment_Date__c, 'Expecting Date: 01 Feb');
        System.assertEquals(Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, debitTenant1[2].Status__c, 'Expecting Status: Collected from Customer');
        //Mar
        System.assertEquals(3000, debitTenant1[3].Amount__c, 'Expecting Payment Amount of 3000');
        System.assertEquals(Date.newInstance(2014, 3, 1), debitTenant1[3].Payment_Date__c, 'Expecting Date: 01 Mar');
        System.assertEquals(Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, debitTenant1[3].Status__c, 'Expecting Status: Collected from Customer');
        //Apr
        System.assertEquals(1400, debitTenant1[4].Amount__c, 'Expecting Payment Amount of 1400');
        System.assertEquals(Date.newInstance(2014, 4, 1), debitTenant1[4].Payment_Date__c, 'Expecting Date: 01 Apr');
        System.assertEquals(Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, debitTenant1[4].Status__c, 'Expecting Status: Collected from Customer');
        System.assertEquals(1600, debitTenant1[5].Amount__c, 'Expecting Payment Amount of 1600');
        System.assertEquals(Date.newInstance(2014, 4, 1), debitTenant1[5].Payment_Date__c, 'Expecting Date: 01 Apr');
        System.assertEquals(Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION, debitTenant1[5].Status__c, 'Expecting Status: Collected from Customer');
        //May -> Dec
        for(Integer i = 0; i < 8; i++) {
            System.assertEquals(3000, debitTenant1[6 + i].Amount__c, 'Expecting Payment Amount of 3000');
            System.assertEquals(Date.newInstance(2014, 5 + i, 1), debitTenant1[6 + i].Payment_Date__c, 'Expecting Payment Date May to Dec');
            System.assertEquals(Utils_Constants.AWAITING_SUBMISSION_STATUS, debitTenant1[6 + i].Status__c, 'Expecting Status: Awaiting Submission');
        }

        List<Tipi_Payments__c> creditTenant1 = [SELECT Id, Amount__c, Total_Remaining_Balance__c, Payment_Date__c, Status__c, (SELECT Id FROM Tipi_Payments__r)
        FROM Tipi_Payments__c WHERE Contact__r.FirstName = 'firstnamecontact0' AND Credit_Payment_Type__c = :Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT ];
        System.assertEquals(10000, creditTenant1[0].Amount__c, 'Validating Credit of £10000');
        System.assertEquals(0, creditTenant1[0].Total_Remaining_Balance__c, 'Expecting Remaining Balanace of £0 (all credit assigned)');
        System.assertEquals(4, creditTenant1[0].Tipi_Payments__r.size(), 'Expecting 4 Payments assigned to this Credit');

        List<Tipi_Payments__c> reservationTenant1 = [SELECT Id, Amount__c, Total_Remaining_Balance__c, Payment_Date__c, Status__c, (SELECT Id FROM Tipi_Payments__r)
        FROM Tipi_Payments__c WHERE Contact__r.FirstName = 'firstnamecontact0' AND Type__c = :Utils_Constants.PAYMENT_TYPE_RESERVATION];
        System.assertEquals(400, reservationTenant1[0].Amount__c, 'Validating Credit of £400');
        System.assertEquals(0, reservationTenant1[0].Total_Remaining_Balance__c, 'Expecting Remaining Balanace of £0 (all reservation-credit assigned)');
        System.assertEquals(1, reservationTenant1[0].Tipi_Payments__r.size(), 'Expecting 1 Payment assigned to this reservation-credit');

        System.assertEquals(16, [SELECT Id FROM Tipi_Payments__c].size(), 'Expecting 16 Tipi_Payments__c records in total');

        System.assert(getLogs().isEmpty(), 'No logs created');
    }

    /**
     * Method tested: executeBatch(new TipiPaymentBatch())
     * Expected result: Successfully iterate of payment record and create Asperato payment record
     */
    private static testMethod void executeBatchNewTipiPaymentBatch6MonthsSuccess() {

        Test.startTest();
        Database.executeBatch(new TipiPaymentBatch(Date.newInstance(2014, 6, 1)), 20); // rent payment begins Date.newInstance(2014, 1, 1)
        Test.stopTest();

        List<AsyncApexJob> jobsApexBatch = getBatchApexJobs();
        System.assertEquals(1, jobsApexBatch.size(), 'Expecting one apex batch job');
        System.assertEquals(TIPI_PAYMENT_BATCH, jobsApexBatch[0].ApexClass.Name, 'Expecting specific batch job');

        List<asp04__Payment__c> asperatoPayments = getAsperatoPayments();
        System.assertEquals(1, asperatoPayments.size(), '1 Payment created');
        System.assertEquals(7600, asperatoPayments[0].asp04__Amount__c, '1 Payment created');


        List<Tipi_Payments__c> debitTenant1 = [SELECT Id, Amount__c, Payment_Date__c, Status__c FROM Tipi_Payments__c WHERE Contact__r.FirstName = 'firstnamecontact0' AND
        Credit_Payment_Type__c = :Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT ORDER BY Payment_Date__c, Amount__c];
        System.assertEquals(14, debitTenant1.size(), 'Expecting 14 Payments: ' +
                '-> Collected From Customer (5):    Reservation 400; Credit 2600; Credit 3000; Credit 3000; Credit 1400; ' +
                '-> Submitted For Collection (3):   Debit 1600; Debit 3000; Debit 3000; ' +
                '-> Awaiting Submission (6):        Debit 3000; Debit 3000; Debit 3000; Debit 3000; Debit 3000; Debit 3000; ');
        // Jan
        System.assertEquals(400, debitTenant1[0].Amount__c, 'Expecting Payment Amount of 400');
        System.assertEquals(Date.newInstance(2014, 1, 1), debitTenant1[0].Payment_Date__c, 'Expecting Date: 01 Jan');
        System.assertEquals(Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, debitTenant1[0].Status__c, 'Expecting Status: Collected from Customer');
        System.assertEquals(2600, debitTenant1[1].Amount__c, 'Expecting Payment Amount of 2600');
        System.assertEquals(Date.newInstance(2014, 1, 1), debitTenant1[1].Payment_Date__c, 'Expecting Date: 01 Jan');
        System.assertEquals(Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, debitTenant1[1].Status__c, 'Expecting Status: Collected from Customer');
        //Feb
        System.assertEquals(3000, debitTenant1[2].Amount__c, 'Expecting Payment Amount of 3000');
        System.assertEquals(Date.newInstance(2014, 2, 1), debitTenant1[2].Payment_Date__c, 'Expecting Date: 01 Feb');
        System.assertEquals(Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, debitTenant1[2].Status__c, 'Expecting Status: Collected from Customer');
        //Mar
        System.assertEquals(3000, debitTenant1[3].Amount__c, 'Expecting Payment Amount of 3000');
        System.assertEquals(Date.newInstance(2014, 3, 1), debitTenant1[3].Payment_Date__c, 'Expecting Date: 01 Mar');
        System.assertEquals(Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, debitTenant1[3].Status__c, 'Expecting Status: Collected from Customer');
        //Apr
        System.assertEquals(1400, debitTenant1[4].Amount__c, 'Expecting Payment Amount of 1400');
        System.assertEquals(Date.newInstance(2014, 4, 1), debitTenant1[4].Payment_Date__c, 'Expecting Date: 01 Apr');
        System.assertEquals(Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, debitTenant1[4].Status__c, 'Expecting Status: Collected from Customer');
        System.assertEquals(1600, debitTenant1[5].Amount__c, 'Expecting Payment Amount of 1600');
        System.assertEquals(Date.newInstance(2014, 4, 1), debitTenant1[5].Payment_Date__c, 'Expecting Date: 01 Apr');
        System.assertEquals(Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION, debitTenant1[5].Status__c, 'Expecting Status: Collected from Customer');
        //May
        System.assertEquals(3000, debitTenant1[6].Amount__c, 'Expecting Payment Amount of 3000');
        System.assertEquals(Date.newInstance(2014, 5, 1), debitTenant1[6].Payment_Date__c, 'Expecting Date: 01 May');
        System.assertEquals(Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION, debitTenant1[6].Status__c, 'Expecting Status: Collected from Customer');
        //Jun
        System.assertEquals(3000, debitTenant1[7].Amount__c, 'Expecting Payment Amount of 3000');
        System.assertEquals(Date.newInstance(2014, 6, 1), debitTenant1[7].Payment_Date__c, 'Expecting Date: 01 Jun');
        System.assertEquals(Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION, debitTenant1[7].Status__c, 'Expecting Status: Collected from Customer');
        //Jul -> Dec
        for(Integer i = 0; i < 6; i++) {
            System.assertEquals(3000, debitTenant1[8 + i].Amount__c, 'Expecting Payment Amount of 3000');
            System.assertEquals(Date.newInstance(2014, 7 + i, 1), debitTenant1[8 + i].Payment_Date__c, 'Expecting Payment Date Jul to Dec');
            System.assertEquals(Utils_Constants.AWAITING_SUBMISSION_STATUS, debitTenant1[8 + i].Status__c, 'Expecting Status: Awaiting Submission');
        }

        List<Tipi_Payments__c> creditTenant1 = [SELECT Id, Amount__c, Total_Remaining_Balance__c, Payment_Date__c, Status__c, (SELECT Id FROM Tipi_Payments__r)
        FROM Tipi_Payments__c WHERE Contact__r.FirstName = 'firstnamecontact0' AND Credit_Payment_Type__c = :Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT ];
        System.assertEquals(10000, creditTenant1[0].Amount__c, 'Validating Credit of £10000');
        System.assertEquals(0, creditTenant1[0].Total_Remaining_Balance__c, 'Expecting Remaining Balanace of £0 (all credit assigned)');
        System.assertEquals(4, creditTenant1[0].Tipi_Payments__r.size(), 'Expecting 4 Payments assigned to this Credit');

        List<Tipi_Payments__c> reservationTenant1 = [SELECT Id, Amount__c, Total_Remaining_Balance__c, Payment_Date__c, Status__c, (SELECT Id FROM Tipi_Payments__r)
        FROM Tipi_Payments__c WHERE Contact__r.FirstName = 'firstnamecontact0' AND Type__c = :Utils_Constants.PAYMENT_TYPE_RESERVATION];
        System.assertEquals(400, reservationTenant1[0].Amount__c, 'Validating Credit of £400');
        System.assertEquals(0, reservationTenant1[0].Total_Remaining_Balance__c, 'Expecting Remaining Balanace of £0 (all reservation-credit assigned)');
        System.assertEquals(1, reservationTenant1[0].Tipi_Payments__r.size(), 'Expecting 1 Payment assigned to this reservation-credit');

        System.assertEquals(16, [SELECT Id FROM Tipi_Payments__c].size(), 'Expecting 16 Tipi_Payments__c records in total');

        System.assert(getLogs().isEmpty(), 'No logs created');
    }

    /**
     * Method tested: executeBatch(new TipiPaymentBatch())
     * Expected result: Iterate of payment record but skip Asperato payment record due to missing Auth
     */
    private static testMethod void executeBatchNewTipiPaymentBatchNoAuthorisationSuccess() {

        // remove all authorisations
        delete [SELECT Id FROM asp04__Authorisation__c];

        Test.startTest();
        Database.executeBatch(new TipiPaymentBatch(Date.newInstance(2014, 4, 1)), 20);
        Test.stopTest();

        List<AsyncApexJob> jobsApexBatch = getBatchApexJobs();
        System.assertEquals(1, jobsApexBatch.size(), 'Expecting one apex batch job');
        System.assertEquals(TIPI_PAYMENT_BATCH, jobsApexBatch[0].ApexClass.Name, 'Expecting specific batch job');

        List<Task> tasks = [SELECT Id, Description, Subject FROM Task];
        System.assertEquals(1, tasks.size(), 'Expecting task to be created');
        System.assert(tasks[0].Description.startsWith('This Contact is missing an') &&
                tasks[0].Subject.endsWith('1600.00'), 'Expecting Contact Amount of 1600');
    }

    /**
     * Method tested: executeBatch(new TipiPaymentBatch())
     * Expected result: Scope returns Contacts without Tipi_Payments__c
     */
    private static testMethod void executeBatchNewTipiPaymentBatchNoTipiPaymentsSuccess() {

        // remove all Tipi_Payments__c
        delete [SELECT Id FROM Tipi_Payments__c];

        Test.startTest();
        Database.executeBatch(new TipiPaymentBatch(Date.newInstance(2017, 1, 1)), 20);
        Test.stopTest();

        List<AsyncApexJob> jobsApexBatch = getBatchApexJobs();
        System.assertEquals(1, jobsApexBatch.size(), 'Expecting one apex batch job');
        System.assertEquals(TIPI_PAYMENT_BATCH, jobsApexBatch[0].ApexClass.Name, 'Expecting specific batch job');
    }

    /**
     * Method tested: schedule('TipiPaymentBatchScheduler')
     * Expected result: Successfully iterate of empty payment records
     */
    private static testMethod void scheduleTipiPaymentBatchSchedulerSuccess() {
        Integer daysUntilFirstOfMonth = 0;
        if(Date.today().day() != 1) {
            daysUntilFirstOfMonth = Date.today().daysBetween(Date.today().addMonths(1).toStartOfMonth());
        }
        insert new Batch_Run_Days_Before__c(Number_of_Days__c = daysUntilFirstOfMonth);

        String cronExpr = '0 0 0 1 * ?';
        List<AsyncApexJob> jobsBefore = [
            SELECT Id, ApexClassId, ApexClass.Name, Status, JobType
            FROM AsyncApexJob
        ];
        System.assertEquals(0, jobsBefore.size(), 'Not expecting any asyncjobs');

        // deleting Contacts as default scope for TipiPaymentBatchScheduler is set to 10 and Test Class only supports one executeBatch iteration
        Integer contactsToRemove = [SELECT Id FROM Contact].size() - 10;
        if(contactsToRemove < 0) contactsToRemove = 0;
        delete [SELECT Id FROM Contact LIMIT :contactsToRemove];

        Test.startTest();
        String jobId = System.schedule('TipiPaymentBatchScheduler', cronExpr, new TipiPaymentBatchScheduler());
        Test.stopTest();

        List<AsyncApexJob> jobsScheduled = [SELECT ApexClass.Name FROM AsyncApexJob WHERE JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size(), 'Expecting one scheduled job');
        System.assertEquals('TipiPaymentBatchScheduler', jobsScheduled[0].ApexClass.Name, 'Expecting specific scheduled job');

        List<AsyncApexJob> jobsApexBatch = getBatchApexJobs();
        System.assertEquals(1, jobsApexBatch.size(), 'Expecting one apex batch job');
        System.assertEquals(TIPI_PAYMENT_BATCH, jobsApexBatch[0].ApexClass.Name, 'Expecting specific batch job');
    }

    /**
     * Method tested: schedule('TipiPaymentBatchScheduler')
     * Expected result: Batch does not run as TODAY is not x number of days before the first
     */
    private static testMethod void scheduleTipiPaymentBatchSchedulerFailure() {
        Integer daysUntilFirstOfMonth = 0;
        if(Date.today().day() != 1) {
            daysUntilFirstOfMonth = Date.today().daysBetween(Date.today().addMonths(1).toStartOfMonth());
        }

        // adding one day to achieve negative scenario of Batch not being executed (due to condition within TipiPaymentBatchScheduler)
        daysUntilFirstOfMonth += 1;
        insert new Batch_Run_Days_Before__c(Number_of_Days__c = daysUntilFirstOfMonth);

        String cronExpr = '0 0 0 1 * ?';
        List<AsyncApexJob> jobsBefore = [
                SELECT Id, ApexClassId, ApexClass.Name, Status, JobType
                FROM AsyncApexJob
        ];
        System.assertEquals(0, jobsBefore.size(), 'Not expecting any asyncjobs');

        // deleting Contacts as default scope for TipiPaymentBatchScheduler is set to 10 and Test Class only supports one executeBatch iteration
        Integer contactsToRemove = [SELECT Id FROM Contact].size() - 10;
        if(contactsToRemove < 0) contactsToRemove = 0;
        delete [SELECT Id FROM Contact LIMIT :contactsToRemove];

        Test.startTest();
        String jobId = System.schedule('TipiPaymentBatchScheduler', cronExpr, new TipiPaymentBatchScheduler());
        Test.stopTest();

        List<AsyncApexJob> jobsScheduled = [SELECT ApexClass.Name FROM AsyncApexJob WHERE JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size(), 'Expecting one scheduled job');
        System.assertEquals('TipiPaymentBatchScheduler', jobsScheduled[0].ApexClass.Name, 'Expecting specific scheduled job');

        List<AsyncApexJob> jobsApexBatch = getBatchApexJobs();
        System.assertEquals(0, jobsApexBatch.size(), 'Expecting no apex batch job');
    }

    /**
     * Method tested: schedule('TipiPaymentBatchScheduler_Daily')
     * Expected result: Successfully iterate of empty payment records
     */
    private static testMethod void scheduleTipiPaymentBatchSchedulerDailySuccess() {
        String cronExpr = '0 0 0 1 * ?';
        List<AsyncApexJob> jobsBefore = [
                SELECT Id, ApexClassId, ApexClass.Name, Status, JobType
                FROM AsyncApexJob
        ];
        System.assertEquals(0, jobsBefore.size(), 'Not expecting any asyncjobs');

        // deleting Contacts as default scope for TipiPaymentBatchScheduler is set to 10 and Test Class only supports one executeBatch iteration
        Integer contactsToRemove = [SELECT Id FROM Contact].size() - 10;
        if(contactsToRemove < 0) contactsToRemove = 0;
        delete [SELECT Id FROM Contact LIMIT :contactsToRemove];

        Test.startTest();
        String jobId = System.schedule('TipiPaymentBatchScheduler_Daily', cronExpr, new TipiPaymentBatchScheduler_Daily());
        Test.stopTest();

        List<AsyncApexJob> jobsScheduled = [SELECT ApexClass.Name FROM AsyncApexJob WHERE JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size(), 'Expecting one scheduled job');
        System.assertEquals('TipiPaymentBatchScheduler_Daily', jobsScheduled[0].ApexClass.Name, 'Expecting specific scheduled job');

        List<AsyncApexJob> jobsApexBatch = getBatchApexJobs();
        System.assertEquals(1, jobsApexBatch.size(), 'Expecting one apex batch job');
        System.assertEquals(TIPI_PAYMENT_BATCH, jobsApexBatch[0].ApexClass.Name, 'Expecting specific batch job');
    }

    /**
     * Method tested: executeBatch(new TipiPaymentBatch())
     * Expected result: Exception thrown and handled
     */
    private static testMethod void executeBatchNewTipiPaymentBatchFailure() {

        Test.startTest();
        TipiPaymentBatch tipiPaymentBatch = new TipiPaymentBatch(Date.newInstance(2017, 1, 1));
        tipiPaymentBatch.execute(null);
        Test.stopTest();

        System.assert(getBatchApexJobs().isEmpty(), 'Expecting no apex batch job');
    }

    @TestSetup
    private static void testSetup() {
        Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);
        gas.Run_Process_Builder__c = false;
        update gas;

        insert new asp04__AsperatoOneSettings__c(asp04__BACS_Delay__c = 4);

        TriggerHandler.bypass('AuthorisationTriggerHandler'); // due to AuthorisationTriggerHandler failing

        List<Development__c> developments = TestFactoryPayment.getDevelopments(1, true);
        List<Account> accounts = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 11, true);
        List<Opportunity> opportunities = TestFactoryPayment.getOpportunities(accounts[0], contacts, 1, true);
        List<asp04__Authorisation__c> authorisations = TestFactoryPayment.getAuthorisations(contacts, true);

        List<Tipi_Payments__c> tipiPayments = new List<Tipi_Payments__c>();
        // add 12 rent payments
        for(Integer i = 0; i < 12; i++) {
            tipiPayments.add(TestFactoryPayment.getTipiPayments(3000, contacts[0].Id, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
                    developments[0].Id, opportunities[0], null,  Date.newInstance(2014, 1, 1).addMonths(i),
                    Utils_Constants.AWAITING_SUBMISSION_STATUS, 'Rent - AST', 1, false)[0]);
        }

        // add credit payment
        Tipi_Payments__c tipiPaymentsCredit = TestFactoryPayment.getTipiPayments(10000, contacts[0].Id,
                Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT, developments[0].Id, opportunities[0], null, Date.newInstance(2014, 1, 1),
                Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, 'Rent - AST', 1, false)[0];
        tipiPayments.add(tipiPaymentsCredit);

        // add reservation payment
        Tipi_Payments__c tipiPaymentsReservation = TestFactoryPayment.getTipiPayments(400, contacts[0].Id,
                '', developments[0].Id, opportunities[0], null, Date.newInstance(2013, 12, 1),
                Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, Utils_Constants.PAYMENT_TYPE_RESERVATION, 1, false)[0];
        tipiPayments.add(tipiPaymentsReservation);

        insert tipiPayments;
    }

    private static List<asp04__Payment__c> getAsperatoPayments() {
        return [
                SELECT Id, asp04__Amount__c, Contact__c
                FROM asp04__Payment__c
                WHERE CreatedDate = TODAY
        ];
    }

    private static List<Custom_Log__c> getLogs() {
        return [
                SELECT Id, Message__c
                FROM Custom_Log__c
                WHERE Type__c = 'Error'
        ];
    }

    private static List<AsyncApexJob> getBatchApexJobs() {
        return [
                SELECT Id, ApexClassId, ApexClass.Name, Status, JobType
                FROM AsyncApexJob
                WHERE JobType = 'BatchApex'
        ];
    }
}