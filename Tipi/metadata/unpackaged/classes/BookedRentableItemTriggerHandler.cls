/**
 * @Name:        BookedRentableItemTriggerHandler
 * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
 *
 * @author:      Andrey Korobovich
 * @version:     1.0
 * Change Log
 *
 * Date          author                 Change Description
 * -----------------------------------------------------------------------------------
 * 18/05/2018    Andrey Korobovich      Created Class
 * 11/06/2018    Eugene Konstantinov    Added afterInsert() method
 * 20/07/2018    Patrick Fischer        Added beforeInsert() method
 */

public with sharing class BookedRentableItemTriggerHandler extends TriggerHandler {

    protected override void afterInsert() {
        BookedRentableItemTriggerHelper.processAfterInsert(Trigger.newMap);
    }

    protected override void afterUpdate() {
        BookedRentableItemTriggerHelper.processAfterUpdate(Trigger.oldMap, Trigger.newMap);
    }
}