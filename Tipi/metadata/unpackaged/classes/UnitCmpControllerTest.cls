/**
    * @Name:        UnitCmpControllerTest
    * @Description: Test class for UnitCmpController functionality
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 30/05/2018    Eugene Konstantinov    Created Class
*/
@isTest
public class UnitCmpControllerTest {

    @isTest
    static void testUnitCmp() {
        Development__c dev = TestFactoryPayment.getDevevelopment();
        List<Building__c> buildings = TestFactoryPayment.getBuildings(dev, 1);
        List<Unit__c> units = TestFactoryPayment.getUnits(buildings[0], 1);
        System.assert(1 == units.size());

        Test.startTest();
            UnitCmpController.FieldSetForm fsForm = UnitCmpController.getForm(units[0].Id, 'Unit__c', 'My_Unit_Fieldset');
            System.assert(fsForm != null);
            UnitCmpController.FieldSetForm fsFormWithoutRecord = UnitCmpController.getForm(null, 'Unit__c', 'My_Unit_Fieldset');
            System.assert(fsFormWithoutRecord != null);
        Test.stopTest();
    }

}