/**
    * @Name:        ProxyUrlWrapper
    * @Description: Wrapper class for creating request and response structore for wxternal system
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 12/04/2018    Eugene Konstantinov    Created Class
*/
public class ProxyUrlWrapper {

	String RecordId;
	String ContentBodyId ;
	String ContentVersionId ;
	String OrgId;
	String SharedLink;
	String FileDetails;

	public ProxyUrlWrapper(String RecordId,String ContentBodyId,String ContentVersionId,String OrgId,String SharedLink,String FileDetails){

		this.RecordId = RecordId;
		this.ContentBodyId = ContentBodyId;
		this.ContentVersionId = ContentVersionId;
		this.OrgId = OrgId;
		this.SharedLink = SharedLink;
		this.FileDetails = FileDetails;

	}

	public class Response {

		public String recordId;
		public String url;

	}

}