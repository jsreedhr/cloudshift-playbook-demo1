/**
    * @Name:        UnitTriggerHelper
    * @Description: This is helper class, will hold all methods and functionalities that are to be build for Unit Trigger
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 09/04/2018    Eugene Konstantinov    Created Class
*/
public with sharing class UnitTriggerHelper {

	public static void processAfterInsert(List<sObject> triggerNew) {
		List<Unit_Event__e> unitsToAggregator = new List<Unit_Event__e>();
		for (Unit__c unit : (List<Unit__c>)triggerNew) {
			if (unit.Release_to_Aggregators__c) { // add only ready for aggregators unit records
				Unit_Event__e event = new Unit_Event__e();
				event.isDelete__c = false;
				event.RecordId__c = unit.Id;
				unitsToAggregator.add(event);
			}
		}
		if (!unitsToAggregator.isEmpty()) {
			EventBus.publish(unitsToAggregator);
		}
	}

	public static void processAfterUpdate(Map<Id, sObject> oldMap, Map<Id, sObject> newMap) {
		List<Unit_Event__e> unitsToAggregator = new List<Unit_Event__e>();
		List<Unit_Event__e> unitsToRemoveFromAggregator = new List<Unit_Event__e>();
		for (Unit__c unit : (List<Unit__c>)newMap.values()) {
			if (unit.Release_to_Aggregators__c && oldMap.containsKey(unit.Id) && oldMap.get(unit.Id) != null) {
				Unit__c oldUnit = (Unit__c)oldMap.get(unit.Id);
				if (!oldUnit.Release_to_Aggregators__c) {  // add only ready for aggregators unit records
					Unit_Event__e event = new Unit_Event__e();
					event.isDelete__c = false;
					event.RecordId__c = unit.Id;
					unitsToAggregator.add(event);	
				}
			} else if(! unit.Release_to_Aggregators__c && oldMap.containsKey(unit.Id) && oldMap.get(unit.Id) != null) {
				Unit__c oldUnit = (Unit__c)oldMap.get(unit.Id);
				if (oldUnit.Release_to_Aggregators__c) { 
					Unit_Event__e event = new Unit_Event__e();
					event.isDelete__c = true;
					event.RecordId__c = unit.Id;
					unitsToRemoveFromAggregator.add(event);
				}
			}
		}
		if (unitsToAggregator != null && unitsToAggregator.size() > 0) {
			System.debug(unitsToAggregator);
			EventBus.publish(unitsToAggregator);
		}
		if (unitsToRemoveFromAggregator != null && unitsToRemoveFromAggregator.size() > 0) {
			System.debug(unitsToRemoveFromAggregator);
			EventBus.publish(unitsToRemoveFromAggregator);
		}
	}

}