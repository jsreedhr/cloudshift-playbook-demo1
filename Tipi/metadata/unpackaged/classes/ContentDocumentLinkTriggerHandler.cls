/**
 *  Name: ContentDocumentLinkTriggerHandler
 *
 *  Purpose: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods.
 *
 *  History:
 *  Version         Author              Date               Detail
 *  1.0             Jared Watson        21/03/18           creation
 */

public with sharing class ContentDocumentLinkTriggerHandler extends TriggerHandler{


    protected override void afterInsert(){
        ContentAndFileHelper.updateLatestPublishedImageVersionVoid(Trigger.new);
    }

}