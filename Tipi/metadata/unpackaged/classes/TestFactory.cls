/**
 *  Name: TestFactory
 *
 *  Purpose: This class is responsible data to be used in Test Classes
 *
 *  History:
 *  Version         Author              Date               Detail
 *  1.0             Jared Watson        21/03/18           Class Setup.
 *  1.1             Jared Watson        26/03/18            US-437, added USer object to creation profile
 *
 */
@isTest
public class TestFactory {

    public static Integer userCounter = 0;

    public static SObject createSObject(SObject sObj) {
        // Check what type of object we are creating and add any defaults that are needed.
        String objectName = String.valueOf(sObj.getSObjectType());
        // Construct the default values class. Salesforce doesn't allow '__' in class names
        String defaultClassName = 'TestFactory.' + objectName.replaceAll('__(c|C)$|__', '') + 'Defaults';
        // If there is a class that exists for the default values, then use them
        if (Type.forName(defaultClassName) != null) {
            sObj = createSObject(sObj, defaultClassName);
        }
        return sObj;
    }

    public static SObject createSObject(SObject sObj, Boolean doInsert) {
        SObject retObject = createSObject(sObj);
        if (doInsert) {
            insert retObject;
        }
        return retObject;
    }

    public static SObject createSObject(SObject sObj, String defaultClassName) {
        // Create an instance of the defaults class so we can get the Map of field defaults
        Type t = Type.forName(defaultClassName);
        if (t == null) {
            Throw new TestFactoryException('Invalid defaults class.');
        }
        FieldDefaults defaults = (FieldDefaults)t.newInstance();
        addFieldDefaults(sObj, defaults.getFieldDefaults());
        return sObj;
    }

    public static SObject createSObject(SObject sObj, String defaultClassName, Boolean doInsert) {
        SObject retObject = createSObject(sObj, defaultClassName);
        if (doInsert) {
            insert retObject;
        }
        return retObject;
    }

    public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects) {
        return createSObjectList(sObj, numberOfObjects, (String)null);
    }

    public static SObject[] createSObjectList(SObject sObj, Integer numberOfObjects, Boolean doInsert) {
        SObject[] retList = createSObjectList(sObj, numberOfObjects, (String)null);
        if (doInsert) {
            insert retList;
        }
        return retList;
    }

    public static SObject[] createSObjectList(SObject sObj, Integer numberOfObjects, String defaultClassName, Boolean doInsert) {
        SObject[] retList = createSObjectList(sObj, numberOfObjects, defaultClassName);
        if (doInsert) {
            insert retList;
        }
        return retList;
    }

    public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects, String defaultClassName) {
        SObject[] sObjs = new SObject[] {};
        SObject newObj;

        // Get one copy of the object
        if (defaultClassName == null) {
            newObj = createSObject(sObj);
        } else {
            newObj = createSObject(sObj, defaultClassName);
        }

        // Get the name field for the object
        String nameField = nameFieldMap.get(String.valueOf(sObj.getSObjectType()));
        if (nameField == null) {
            nameField = 'Name';
        }

        // Clone the object the number of times requested. Increment the name field so each record is unique
        for (Integer i = 0; i < numberOfObjects; i++) {
            SObject clonedSObj = newObj.clone(false, true);
            clonedSObj.put(nameField, (String)clonedSObj.get(nameField) + ' ' + i);
            sObjs.add(clonedSObj);
            userCounter++;
        }
        return sObjs;
    }

    private static void addFieldDefaults(SObject sObj, Map<Schema.SObjectField, Object> defaults) {
        // Loop through the map of fields and if they weren't specifically assigned, fill them.
        Map<String, Object> populatedFields = sObj.getPopulatedFieldsAsMap();
        for (Schema.SObjectField field : defaults.keySet()) {
            if (!populatedFields.containsKey(String.valueOf(field))) {
                sObj.put(field, defaults.get(field));
            }
        }
    }

    // When we create a list of SObjects, we need to
    private static Map<String, String> nameFieldMap = new Map<String, String> {
            'Contact' => 'LastName',
            'Case' => 'Subject'
    };

    public class TestFactoryException extends Exception {}

    // Use the FieldDefaults interface to set up values you want to default in for all objects.
    public interface FieldDefaults {
        Map<Schema.SObjectField, Object> getFieldDefaults();
    }

    // To specify defaults for objects, use the naming convention [ObjectName]Defaults.
    // For custom objects, omit the __c from the Object Name

    public class UserDefaults implements FieldDefaults{

        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    User.UserName          => 'unit.Tester' + userCounter + '@test.com',
                    User.FirstName         => 'Unit',
                    User.LastName          => 'Tester' + userCounter,
                    User.Alias             => 'UT' + userCounter,
                    User.Email             => 'dev' + userCounter + '@Tester.com',
                    User.EmailEncodingKey  => 'UTF-8',
                    User.LanguageLocaleKey => 'en_US',
                    User.LocaleSidKey      => 'en_US',
                    User.TimeZoneSidKey    => 'Europe/London'
            };
        }

    }

    public class AccountDefaults implements FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    Account.Name => 'Test Account'
            };
        }
    }

    public class ContactDefaults implements FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    Contact.FirstName => 'First',
                    Contact.LastName => 'Last'
            };
        }
    }

    public class OpportunityDefaults implements FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    Opportunity.Name => 'Test Opportunity',
                    Opportunity.StageName => 'Closed Won',
                    Opportunity.CloseDate => System.today()
            };
        }
    }

    public class CaseDefaults implements FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    Case.Subject => 'Test Case'
            };
        }
    }

    public class Development_SiteDefaults implements FieldDefaults{
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    Development__c.Name => 'Test Dev Site',
                    Development__c.City__c => 'Test City',
                    Development__c.POD_Group_Name__c => 'Pod A',
                    Development__c.Postcode__c => 'JF4 3RR',
                    Development__c.Street__c => 'Test Street'
            };
        }
    }

    public class BuildingDefaults implements FieldDefaults{
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    Building__c.Name => 'Test Building',
                    Building__c.Building_Key_Data_01__c => 'Key Data 1',
                    Building__c.Building_Key_Data_02__c => 'Key Data 2',
                    Building__c.Building_Key_Data_03__c => 'Key Data 3',
                    Building__c.Building_Key_Data_04__c => 'Key Data 4',
                    Building__c.City__c => 'Test City',
                    Building__c.POD_Group_Name__c => 'Pod A',
                    Building__c.Postcode__c => 'JF4 3RR',
                    Building__c.Street__c => 'Test Street'
            };
        }
    }

    public class UnitDefaults implements FieldDefaults{
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    Unit__c.Name => 'Test Unit',
                    Unit__c.Unit_Key_Data_01__c => 'Key Data 1',
                    Unit__c.Unit_Key_Data_02__c => 'Key Data 2',
                    Unit__c.Unit_Key_Data_03__c => 'Key Data 3',
                    Unit__c.Unit_Key_Data_04__c => 'Key Data 4',
                    Unit__c.Unit_Number__c =>'66',
                    Unit__c.Unit_Type__c => 'Studio'
            };
        }
    }

    public class UnitFileDefaults implements FieldDefaults{
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    Unit_File__c.Sequence_Number__c => 1,
                    Unit_File__c.Type__c => 'Image'
            };
        }
    }


    public class Global_Application_SettingsDefaults implements FieldDefaults{
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    Global_Application_Settings__c.Run_Assignment_Rules__c => true,
                    Global_Application_Settings__c.Run_Flow__c => true,
                    Global_Application_Settings__c.Run_Process_Builder__c => true,
                    Global_Application_Settings__c.Run_Triggers__c => true,
                    Global_Application_Settings__c.Run_Validation_Rules__c => true,
                    Global_Application_Settings__c.Run_Workflow_Rules__c => true
            };
        }
    }
}