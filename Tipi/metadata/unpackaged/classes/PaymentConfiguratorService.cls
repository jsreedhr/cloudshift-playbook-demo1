/**
 * @Name:        PaymentConfiguratorService
 * @Description: Apex class to operate payment controler data
 *
 * @author:      Andrey Korobovich
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 10/05/2018   Patrick Fischer      Created Original Class - PaymentConfiguratorController
 * 18/05/2018    Andrey Korobovich      Created Class - extracted logic from PaymentConfiguratorController
 * 22/05/2018   Patrick Fischer      Amended Storage & Parking logic; added type mapping
 * 23/05/2018   Patrick Fischer      Class Refactoring
 * 12/06/2018    Eugene Konstantinov    Added actualEndDateChanged parameter to constructPaymentRecords, normalisePaymentRecords; Added methods without actualEndDateChanged
 * 26/06/2018    Eugene Konstantinov    Added normalisePaymentRecordsForBRI(), getAuthorisationsMapByContact() for Contact Ids
 */
public with sharing class PaymentConfiguratorService {

	public static Id FINANCE_QUEUE_ID = null;
	private static final String PENDING_DOCUMENT_SEND_STATUS = 'Pending Document Send';
	private static final String AWAITING_SUBMISSION_STATUS = 'Awaiting submission';
	private static final String PAYMENT_GATEWAY_OTHER = 'Other';
	private static final String PAYMENT_GATEWAY_SAGEPAY = 'SagePay';

	/**
     * To construct the required TIPI_Payment__c records for rent, deposit, up front etc.
     *
     * @param contract Contract SObject holding the link to an Opportunity
     * @param paymentData PaymentDataWrapper containing validated payment configuration from the LEX component
     *
     * @return List of constructed Tipi_Payments__c records (not inserted)
     */
	public static List<Tipi_Payments__c> constructPaymentRecords(Contract contract, PaymentDataWrapper paymentData) {
		return constructPaymentRecords(contract, paymentData, false, true);
	}

	/**
     * To construct the required TIPI_Payment__c records for rent, deposit, up front etc.
     *
     * @param contract Contract SObject holding the link to an Opportunity
     * @param paymentData PaymentDataWrapper containing validated payment configuration from the LEX component
     * @param actualEndDateChanged true if Actual_Tenancy_End_Date__c on contract was changed
     *
     * @return List of constructed Tipi_Payments__c records (not inserted)
     */
	public static List<Tipi_Payments__c> constructPaymentRecords(Contract contract, PaymentDataWrapper paymentData, Boolean actualEndDateChanged) {
		return constructPaymentRecords(contract, paymentData, actualEndDateChanged, false);
	}

	/**
     * To construct the required TIPI_Payment__c records for rent, deposit, up front etc.
     *
     * @param contract Contract SObject holding the link to an Opportunity
     * @param paymentData PaymentDataWrapper containing validated payment configuration from the LEX component
     * @param actualEndDateChanged true if Actual_Tenancy_End_Date__c on contract was changed, else false
     * @param setInitialPaymentType true if the first rent payment should be set with the upfront payment type
     *
     * @return List of constructed Tipi_Payments__c records (not inserted)
     */
	public static List<Tipi_Payments__c> constructPaymentRecords(Contract contract, PaymentDataWrapper paymentData, Boolean actualEndDateChanged, Boolean setInitialPaymentType) {
		List<Tipi_Payments__c> allPayments = new List<Tipi_Payments__c>();
		Map<Id, Id> authorisations = getAuthorisationsMapByContact(paymentData);
		FINANCE_QUEUE_ID = getQueueIdByName(Utils_Constants.FINANCE_QUEUE_NAME);

		for (Tenant tenant : paymentData.tenants) {
			if (tenant.isPayee) {

				// construct monthly payments without concession
				List<Tipi_Payments__c> normalisedRent = normalisePaymentRecords(contract, authorisations.get(tenant.contactId), paymentData.rent,
						tenant.contactId, tenant.rent, Utils_Constants.PAYMENT_GATEWAY_DD, getPaymentTypeFromContract(contract), actualEndDateChanged);

				// include concession for monthly payments
				List<Tipi_Payments__c> rentAppliedConcession = applyConcession(normalisedRent,
						contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Checkbox__c,
						contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Length__c,
						contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Amount__c,
						contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_Checkbox__c,
						contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_First_Month__c,
						contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Checkbox__c,
						contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Text__c);

				// add depositPayments
				List<Tipi_Payments__c> depositPayments = new List<Tipi_Payments__c>();
				if (tenant.depositPayment > 0) {
					depositPayments.add(constructTipiPayment(contract, tenant.contactId, authorisations.get(tenant.contactId),
							tenant.depositPayment.setScale(2, RoundingMode.HALF_UP), System.today(), tenant.upfrontPaymentType,
							Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.get('Deposit'), (tenant.depositPayment / paymentData.depositPayment), 1, null,
							'[Deposit ' + paymentData.depositPayment + ']',
							true
					));
				}

				// add upfrontPayments
				List<Tipi_Payments__c> upfrontPayments = new List<Tipi_Payments__c>();
				if (tenant.rentUpfront > 0) {
					upfrontPayments.add(constructTipiPayment(contract, tenant.contactId, authorisations.get(tenant.contactId),
							tenant.rentUpfront.setScale(2, RoundingMode.HALF_UP), System.today(), tenant.upfrontPaymentType,
							getPaymentTypeFromContract(contract), 1, 1, Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT,
							'[' + tenant.monthsUpfront + ' Months Upfront] * [Monthly Rent ' + paymentData.rent + ']',
							true
					));
				} else if(setInitialPaymentType) {
					String paymentGateway = PAYMENT_GATEWAY_OTHER;
					if(tenant.upfrontPaymentType != PAYMENT_GATEWAY_OTHER) {
						paymentGateway = PAYMENT_GATEWAY_SAGEPAY;
					}
					rentAppliedConcession[0].Payment_Gateway__c = paymentGateway;
				}

				allPayments.addAll(rentAppliedConcession);
				allPayments.addAll(depositPayments);
				allPayments.addAll(upfrontPayments);
			}
		}

		return allPayments;
	}

	/**
       * To construct the required Booked_Rentable_Item__c records for parking and storage.
     *
     * @param contract Contract SObject holding the link to an Opportunity
     * @param paymentData PaymentDataWrapper containing validated payment configuration from the LEX component
     *
     * @return List of constructed Booked_Rentable_Item__c records (not inserted)
     */
	public static List<Booked_Rentable_Item__c> constructBookedRentableItems(Contract contract, PaymentDataWrapper paymentData) {
		List<Booked_Rentable_Item__c> bookedRentableItems = new List<Booked_Rentable_Item__c>();

		Map<Id, SBQQ__QuoteLine__c> quoteLineByRentableItemId = getQuoteLineByRentableItemId(contract, paymentData);

		// create rentable Items
		for (Tenant tenant : paymentData.tenants) {
			if (tenant.isPayee) {
				// loop over all storage + parking Id's
				String concatenatedIds = tenant.storageString + ';' + tenant.parkingString;
				for(String rentableItemStr : concatenatedIds.split(';')) {
					try {
						Id rentableItemId = Id.valueOf(rentableItemStr);
						Date startDate = quoteLineByRentableItemId.get(rentableItemId).SBQQ__EffectiveStartDate__c;
						Date endDate = quoteLineByRentableItemId.get(rentableItemId).SBQQ__EffectiveEndDate__c;
						if(startDate == null) {
							startDate = contract.StartDate;
						}
						if(endDate == null) {
							endDate = contract.EndDate;
						}
						bookedRentableItems.add(
								new Booked_Rentable_Item__c(
										Status__c = PENDING_DOCUMENT_SEND_STATUS,
										Contact__c = tenant.contactId,
										Contract__c = contract.Id,
										Start_Date__c = startDate,
										End_Date__c = endDate,
										Quote_Line__c = quoteLineByRentableItemId.get(rentableItemId).Id,
										Rentable_Item__c = rentableItemId
								)
						);
					} catch (Exception e) {
						continue;
					}
				}
			}
		}

		return bookedRentableItems;
	}

	/**
      * To construct the required Mapping of Rentable Item to SBQQ__QuoteLine__c records
      *
     * @param contract Contract SObject holding the link to an Opportunity
     * @param paymentData PaymentDataWrapper containing validated payment configuration from the LEX component
     *
     * @return Map of rentable item Id to QuoteLine record
     */
	private static Map<Id, SBQQ__QuoteLine__c> getQuoteLineByRentableItemId(Contract contract, PaymentDataWrapper paymentData) {
		Map<Id, SBQQ__QuoteLine__c> quoteLineByRentableItemId = new Map<Id, SBQQ__QuoteLine__c>();

		// get all rentable item Id's
		Set<String> rentableItemIds = new Set<String>();
		for(Tenant tenant : paymentData.tenants) {
			if (tenant.isPayee) {
				rentableItemIds.addAll(tenant.storageString.split(';'));
				rentableItemIds.addAll(tenant.parkingString.split(';'));
			}
		}

		Map<Id, SBQQ__QuoteLine__c> quoteLineByProductId = new Map<Id, SBQQ__QuoteLine__c>();
		for(SBQQ__QuoteLine__c quoteLine : PaymentConfiguratorController.getQuoteLineItemsByQuoteId(contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__c)) {
			quoteLineByProductId.put(quoteLine.SBQQ__Product__c, quoteLine);
		}

		// get QuoteLines By Rentable Item Id
		for(Rentable_Item__c rentableItem : [SELECT Id, Product__c FROM Rentable_Item__c WHERE Id IN :rentableItemIds]) {
			quoteLineByRentableItemId.put(rentableItem.Id, quoteLineByProductId.get(rentableItem.Product__c));
		}

		return quoteLineByRentableItemId;
	}

	/**
     * To construct the normalised monthly rent Tipi_Payments__c records for a specified Tenant & Type without concession
     *
     * @param contract Contract SObject holding the link to an Opportunity
     * @param authorisationId Id of related Authorisation for Contact
     * @param monthlyTotal Decimal of total contracted amount per rent/storage/parking
     * @param contactId Id of Tenant related to these Payments
     * @param tenantMonthlyAmount Decimal of payment amount of without concession
     * @param paymentGateway String of payment gateway record, i.e. Direct Debit, SagePay, Other
     * @param type String of payment type, i.e. Rent, Parking, Deposit
     *
     * @return List of Tipi_Payments__c SObjects holding all payment details for a specific tenant & type
     */
	public static List<Tipi_Payments__c> normalisePaymentRecords(Contract contract, Id authorisationId, Decimal monthlyTotal,
			String contactId, Decimal tenantMonthlyAmount, String paymentGateway, String type) {
		List<Tipi_Payments__c> tipiPayments = normalisePaymentRecords(contract, authorisationId, monthlyTotal, contactId, tenantMonthlyAmount, paymentGateway, type, false);

		return tipiPayments;
	}

	/**
     * To construct the normalised monthly rent Tipi_Payments__c records for a specified Tenant & Type without concession
     *
     * @param contract Contract SObject holding the link to an Opportunity
     * @param authorisationId Id of related Authorisation for Contact
     * @param monthlyTotal Decimal of total contracted amount per rent/storage/parking
     * @param contactId Id of Tenant related to these Payments
     * @param tenantMonthlyAmount Decimal of payment amount of without concession
     * @param paymentGateway String of payment gateway record, i.e. Direct Debit, SagePay, Other
     * @param type String of payment type, i.e. Rent, Parking, Deposit
       * @param actualEndDateChanged true if Actual_Tenancy_End_Date__c on contract was changed, need to use Actual_Tenancy_End_Date__c for calculation
     *
     * @return List of Tipi_Payments__c SObjects holding all payment details for a specific tenant & type
     */
	public static List<Tipi_Payments__c> normalisePaymentRecords(Contract contract, Id authorisationId, Decimal monthlyTotal,
			String contactId, Decimal tenantMonthlyAmount, String paymentGateway, String type, Boolean actualEndDateChanged) {
		List<Tipi_Payments__c> tipiPayments = new List<Tipi_Payments__c>();
		Date endDate = contract.EndDate;
		if (actualEndDateChanged) {
			endDate = contract.Actual_Tenancy_End_Date__c;
		}

		if (tenantMonthlyAmount > 0) {
			Decimal paymentShare = 0;
			if(monthlyTotal > 0) {
				paymentShare = tenantMonthlyAmount / monthlyTotal;
			}
			Decimal tenantDaily = (tenantMonthlyAmount * 12) / 365;
			Date firstPaymentDate = contract.StartDate;
			Date lastPaymentDate = endDate.toStartOfMonth();
			Integer daysUntilEndOfFirstMonth = Date.daysInMonth(firstPaymentDate.year(), firstPaymentDate.month()) - firstPaymentDate.day() + 1;
			Integer contractLength = contract.StartDate.monthsBetween(endDate) - 1;
			Integer countMonths = 1;

			// calculate pro rata values
			Decimal proRataFirstMonth = (tenantDaily * daysUntilEndOfFirstMonth).setScale(2, RoundingMode.HALF_UP);
			Decimal proRataLastMonth = (tenantDaily * endDate.day()).setScale(2, RoundingMode.HALF_UP);

			if (contractLength == -1 || (Test.isRunningTest() && contractLength == 0)) {
				// if start date & end date in same month
				Decimal days = (firstPaymentDate.daysBetween(endDate) + 1);
				Decimal totalAmount = (tenantDaily * days).setScale(2, RoundingMode.HALF_UP);
				tipiPayments.add(constructTipiPayment(contract, contactId, authorisationId, totalAmount, firstPaymentDate,
						paymentGateway, type, paymentShare, countMonths, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
						'[Rent for ' + days + ' days (' + firstPaymentDate.format() + ' - ' + endDate.format() + ')]',
						true
				));
			} else {
				// if start date & end date in different months

				// CHECKBOX new prorata record for first month (if Startdate on/before 15th)
				if (firstPaymentDate.day() <= 15) {
					if(firstPaymentDate.day() == 1) {
						proRataFirstMonth = tenantMonthlyAmount.setScale(2, RoundingMode.HALF_UP);
					}
					Date monthEndDate = Date.newInstance(firstPaymentDate.year(), firstPaymentDate.month(), Date.daysInMonth(firstPaymentDate.year(), firstPaymentDate.month()));
					Decimal days = (firstPaymentDate.daysBetween(monthEndDate) + 1);
					tipiPayments.add(constructTipiPayment(contract, contactId, authorisationId, proRataFirstMonth, firstPaymentDate,
							paymentGateway, type, paymentShare, countMonths, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
							'[Rent for ' + days + ' days (' + firstPaymentDate.format() + ' - ' + monthEndDate.format() + ')]',
							false
					));
					countMonths++;
				}

				for (Integer monthNo = 0; monthNo < contractLength; monthNo++) {
					Decimal tenantMonthlAmountRounded = tenantMonthlyAmount.setScale(2, RoundingMode.HALF_UP);
					Date paymentDate = firstPaymentDate.toStartOfMonth().addMonths(1 + monthNo);
					Date monthEndDate = Date.newInstance(paymentDate.year(), paymentDate.month(), Date.daysInMonth(paymentDate.year(), paymentDate.month()));
					tipiPayments.add(constructTipiPayment(contract, contactId, authorisationId, tenantMonthlAmountRounded, paymentDate,
							paymentGateway, type, paymentShare, countMonths, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
							'[Rent (' + paymentDate.format() + ' - ' + monthEndDate.format() + ')]',
							false
					));
					countMonths++;
				}

				//CHECK BOX new prorate record for last month
				tipiPayments.add(constructTipiPayment(contract, contactId, authorisationId, proRataLastMonth, lastPaymentDate,
						paymentGateway, type, paymentShare, countMonths, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
						'[Rent for ' + endDate.day() + ' days (' + lastPaymentDate.format() + ' - ' + endDate.format() + ')]',
						false
				));

				if(!tipiPayments.isEmpty()) {
					// add prorata amount to next month (if Startdate after 15th)
					if (firstPaymentDate.day() > 15) {
						Date monthEndDate = Date.newInstance(firstPaymentDate.year(), firstPaymentDate.month(), Date.daysInMonth(firstPaymentDate.year(), firstPaymentDate.month()));
						Decimal days = (firstPaymentDate.daysBetween(monthEndDate) + 1);

						tipiPayments[0].Amount__c += proRataFirstMonth;
						tipiPayments[0].Calculation_Long__c = '[Rent for ' + days + ' days (' + firstPaymentDate.format() + ' - ' + monthEndDate.format() + ')] + ' + tipiPayments[0].Calculation_Long__c;
					}

					// set first rent payment date as today
					tipiPayments[0].Payment_Date__c = System.today();
				}
			}
		}

		return tipiPayments;
	}

	/**
     * To construct the normalised monthly parking and storage Tipi_Payments__c records for a specified Tenant & Type without concession
     *
     * @param contract Contract SObject holding the link to an Opportunity
     * @param authorisationId Id of related Authorisation for Contact
     * @param monthlyTotal Decimal of total contracted amount per rent/storage/parking
     * @param contactId Id of Tenant related to these Payments
     * @param tenantMonthlyAmount Decimal of payment amount of without concession
     * @param paymentGateway String of payment gateway record, i.e. Direct Debit, SagePay, Other
     * @param type String of payment type, i.e. Rent, Parking, Deposit
     * @param actualEndDateChanged true if Actual_Tenancy_End_Date__c on contract was changed, need to use Actual_Tenancy_End_Date__c for calculation
     *
     * @return List of Tipi_Payments__c SObjects holding all payment details for a specific tenant & type
     */
	public static List<Tipi_Payments__c> normalisePaymentRecordsForBRI(Booked_Rentable_Item__c bookedRentableItem, Id authorisationId, Decimal monthlyTotal,
			Decimal tenantMonthlyAmount, String paymentGateway) {
		List<Tipi_Payments__c> tipiPayments = new List<Tipi_Payments__c>();
		FINANCE_QUEUE_ID = getQueueIdByName(Utils_Constants.FINANCE_QUEUE_NAME);
		Date endDate = bookedRentableItem.End_Date__c;

		if (tenantMonthlyAmount > 0) {
			Decimal paymentShare = 0;
			if(monthlyTotal > 0) {
				paymentShare = tenantMonthlyAmount / monthlyTotal;
			}
			Decimal tenantDaily = (tenantMonthlyAmount * 12) / 365;
			Date firstPaymentDate = bookedRentableItem.Start_Date__c;
			Date lastPaymentDate = endDate.toStartOfMonth();
			Integer daysUntilEndOfFirstMonth = Date.daysInMonth(firstPaymentDate.year(), firstPaymentDate.month()) - firstPaymentDate.day() + 1;
			Integer contractLength = bookedRentableItem.Start_Date__c.monthsBetween(endDate) - 1;
			Integer countMonths = 1;

			// calculate pro rata values
			Decimal proRataFirstMonth = (tenantDaily * daysUntilEndOfFirstMonth).setScale(2, RoundingMode.HALF_UP);
			Decimal proRataLastMonth = (tenantDaily * endDate.day()).setScale(2, RoundingMode.HALF_UP);

			if (contractLength == -1 || (Test.isRunningTest() && contractLength == 0)) {
				// if start date & end date in same month
				Decimal days = (firstPaymentDate.daysBetween(endDate) + 1);
				Decimal totalAmount = (tenantDaily * days).setScale(2, RoundingMode.HALF_UP);
				tipiPayments.add(constructTipiPayment(bookedRentableItem.Contract__r, bookedRentableItem.Contact__c, authorisationId, totalAmount, firstPaymentDate,
						paymentGateway, bookedRentableItem.Rentable_Item__r.Product__r.Product_Type__c, paymentShare, countMonths, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
						'[' + bookedRentableItem.Rentable_Item__r.Product__r.Product_Type__c + ' for ' + days + ' days (' + firstPaymentDate.format() + ' - ' + endDate.format() + ')]',
						true
				));
			} else {
				// if start date & end date in different months

				// new prorata record for first month (if Startdate on/before 15th)
				if (firstPaymentDate.day() <= 15) {
					if(firstPaymentDate.day() == 1) {
						proRataFirstMonth = tenantMonthlyAmount.setScale(2, RoundingMode.HALF_UP);
					}
					Date monthEndDate = Date.newInstance(firstPaymentDate.year(), firstPaymentDate.month(), Date.daysInMonth(firstPaymentDate.year(), firstPaymentDate.month()));
					Decimal days = (firstPaymentDate.daysBetween(monthEndDate) + 1);
					tipiPayments.add(constructTipiPayment(bookedRentableItem.Contract__r, bookedRentableItem.Contact__c, authorisationId, proRataFirstMonth, firstPaymentDate,
							paymentGateway, bookedRentableItem.Rentable_Item__r.Product__r.Product_Type__c, paymentShare, countMonths, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
							'[' + bookedRentableItem.Rentable_Item__r.Product__r.Product_Type__c + ' for ' + days + ' days (' + firstPaymentDate.format() + ' - ' + monthEndDate.format() + ')]',
							true
					));
					countMonths++;
				}

				for (Integer monthNo = 0; monthNo < contractLength; monthNo++) {
					Decimal tenantMonthlyAmountRounded = tenantMonthlyAmount.setScale(2, RoundingMode.HALF_UP);
					Date paymentDate = firstPaymentDate.toStartOfMonth().addMonths(1 + monthNo);
					Date monthEndDate = Date.newInstance(paymentDate.year(), paymentDate.month(), Date.daysInMonth(paymentDate.year(), paymentDate.month()));
					tipiPayments.add(constructTipiPayment(bookedRentableItem.Contract__r, bookedRentableItem.Contact__c, authorisationId, tenantMonthlyAmountRounded, paymentDate,
							paymentGateway, bookedRentableItem.Rentable_Item__r.Product__r.Product_Type__c, paymentShare, countMonths, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
							'[' + bookedRentableItem.Rentable_Item__r.Product__r.Product_Type__c + ' (' + paymentDate.format() + ' - ' + monthEndDate.format() + ')]',
							true
					));
					countMonths++;
				}

				// new prorate record for last month
				tipiPayments.add(constructTipiPayment(bookedRentableItem.Contract__r, bookedRentableItem.Contact__c, authorisationId, proRataLastMonth, lastPaymentDate,
						paymentGateway, bookedRentableItem.Rentable_Item__r.Product__r.Product_Type__c, paymentShare, countMonths, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
						'[' + bookedRentableItem.Rentable_Item__r.Product__r.Product_Type__c + ' for ' + endDate.day() + ' days (' + lastPaymentDate.format() + ' - ' + endDate.format() + ')]',
						true
				));

				if(!tipiPayments.isEmpty()) {
					// add prorata amount to next month (if Startdate after 15th)
					if (firstPaymentDate.day() > 15) {
						Date monthEndDate = Date.newInstance(firstPaymentDate.year(), firstPaymentDate.month(), Date.daysInMonth(firstPaymentDate.year(), firstPaymentDate.month()));
						Decimal days = (firstPaymentDate.daysBetween(monthEndDate) + 1);

						tipiPayments[0].Amount__c += proRataFirstMonth;
						tipiPayments[0].Calculation_Long__c = '[' + bookedRentableItem.Rentable_Item__r.Product__r.Product_Type__c + ' for ' + days + ' days (' + firstPaymentDate.format() + ' - ' + monthEndDate.format() + ')] + ' + tipiPayments[0].Calculation_Long__c;
					}

					// set first rent payment date as today
					tipiPayments[0].Payment_Date__c = System.today();
				}

			}
		}

		// populate Rentable_Item__c lookup
		for (Tipi_Payments__c payment : tipiPayments) {
			payment.Rentable_Item__c = bookedRentableItem.Rentable_Item__c;
		}

		return tipiPayments;
	}

	/**
     * To apply Concession to a list of Tipi_Payments__c records
     *
     * @param tipiPayments List of Tipi_Payments__c records without concession
     * @param concessionCheckbox Boolean flag notifying if concession is applied for N months
     * @param concessionLength Number of N months the concession is applied for
     * @param concessionAmount Amount of concession applied for every month
     * @param offFirstMonthCheckbox Boolean flag notifying deductions for first month
     * @param offFirstMonthAmount Amount of concession applied for the first month
     * @param freeMonthCheckbox Boolean flag notifying if 'N'the month is free
     * @param freeMonthString String literal of month that is free
     *
     * @return List of Tipi_Payments__c records with applied concession
     */
	public static List<Tipi_Payments__c> applyConcession(List<Tipi_Payments__c> tipiPayments, Boolean concessionCheckbox,
			Decimal concessionLength, Decimal concessionAmount, Boolean offFirstMonthCheckbox, Decimal offFirstMonthAmount,
			Boolean freeMonthCheckbox, String freeMonthString) {

		for (Tipi_Payments__c tipiPayment : tipiPayments) {

			//value deduction 'x' months
			if (concessionCheckbox) {
				if (tipiPayment.Nth_Month_Payment__c <= concessionLength) {
					tipiPayment.Concession_Amount__c = tipiPayment.Payment_Share__c * concessionAmount;
					tipiPayment.Amount__c = tipiPayment.Amount__c - tipiPayment.Concession_Amount__c;
					tipiPayment.Calculation_Long__c += ' - [Concession for payment #' + tipiPayment.Nth_Month_Payment__c +
							' of ' + concessionLength + ' ' + concessionAmount + ']';
				}
			}

			// value deduction for first month
			else if (offFirstMonthCheckbox) {
				if (tipiPayment.Nth_Month_Payment__c == 1) {
					tipiPayment.Concession_Amount__c = tipiPayment.Payment_Share__c * offFirstMonthAmount;
					tipiPayment.Amount__c = tipiPayment.Amount__c - tipiPayment.Concession_Amount__c;
					tipiPayment.Calculation_Long__c += ' - [Concession Off First Month ' + offFirstMonthAmount + ']';
				}
			}

			// specific month free (first occurrence of month only)
			else if (freeMonthCheckbox) {
				Date d = tipiPayment.Payment_Date__c;
				String monthFullStr = Datetime.newInstance(d.year(), d.month(), d.day()).format('MMMMM');

				if (monthFullStr == freeMonthString && tipiPayment.Nth_Month_Payment__c <= 12) {
					tipiPayment.Concession_Amount__c = tipiPayment.Amount__c;
					tipiPayment.Calculation_Long__c += ' - [Concession Applied for ' + monthFullStr + ' ' + tipiPayment.Amount__c + ']';
					tipiPayment.Amount__c = 0;
				}
			}

			if(tipiPayment.Payment_Share__c < 1 && tipiPayment.Payment_Share__c > 0 && tipiPayment.Payment_Share__c != null
					&& (concessionCheckbox || offFirstMonthCheckbox)) {
				tipiPayment.Calculation_Long__c += ' * [Payment Share: ' + (tipiPayment.Payment_Share__c * 100).setScale(2, RoundingMode.HALF_UP) + '%]';
			}

			// No Negative Amounts
			if (tipiPayment.Amount__c < 0) {
				tipiPayment.Amount__c = 0;
				tipiPayment.Calculation_Long__c += '; [Amount defaulted to 0 due to negative payment]';
			}

			// round to 2 decimal values
			if (tipiPayment.Amount__c != null) {
				tipiPayment.Amount__c = tipiPayment.Amount__c.setScale(2, RoundingMode.HALF_UP);
			}
			if (tipiPayment.Concession_Amount__c != null) {
				tipiPayment.Concession_Amount__c = tipiPayment.Concession_Amount__c.setScale(2, RoundingMode.HALF_UP);
			}
		}

		return tipiPayments;
	}

	/**
     * To construct a Tipi_Payments__c SObject record holding all related payment details
     *
     * @param contract SObject record that includes the applicable lookups to all tenants
     * @param contactId Id of Tenant Contact
     * @param authorisationId Id of related Authorisation for Contact
     * @param amount Decimal value that will be billed
     * @param paymentDate Date of the Payment (to be pulled into Payments table by Batch on first-month)
     * @param paymentGateway String of payment gateway record, i.e. Direct Debit, SagePay, Other
     * @param type String of payment type, i.e. Rent, Parking, Deposit
     * @param paymentShare Decimal value holding the relative share (value between 0 & 1) of full rent payment
     * @param nthPayment Integer value holding the order number of Tipi_Payment__c 'Rent' record
     * @param creditPaymentType String containing either 'Credit' or 'Debit'
     * @param calculation String of Description
     *
     * @return single Tipi_Payments__c SObject holding the payment details
     */
	private static Tipi_Payments__c constructTipiPayment(Contract contract, Id contactId, Id authorisationId,
			Decimal amount, Date paymentDate, String paymentGateway, String type, Decimal paymentShare,
			Integer nthPayment, String creditPaymentType, String calculation, Boolean isPartialRent) {
		amount = amount.setScale(2, RoundingMode.HALF_UP);
		Tipi_Payments__c tipiPayment = new Tipi_Payments__c();
		tipiPayment.Amount__c = amount;
		tipiPayment.Amount_without_Concession__c = amount;
		tipiPayment.Building__c = contract.Unit__r.Building__c;
		tipiPayment.Calculation_Long__c = calculation;
		tipiPayment.Contact__c = contactId;
		tipiPayment.Description__c = Utils_Constants.PAYMENT_DESCRIPTION;
		tipiPayment.Development__c = contract.Unit__r.Building__r.Development_Site__c;
		tipiPayment.Nth_Month_Payment__c = nthPayment;
		tipiPayment.Opportunity__c = contract.SBQQ__Opportunity__c;
		tipiPayment.OwnerId = FINANCE_QUEUE_ID;
		tipiPayment.Payment_Date__c = paymentDate;
		tipiPayment.Payment_Gateway__c = paymentGateway;
		tipiPayment.Payment_Share__c = paymentShare;
		tipiPayment.Status__c = AWAITING_SUBMISSION_STATUS;
		tipiPayment.Unit__c = contract.Unit__c;
		tipiPayment.Type__c = type;
		tipiPayment.isPartialRent__c = isPartialRent;

		if(authorisationId != null) {
			tipiPayment.Authorisation__c = authorisationId;
		}
		if(creditPaymentType != null) {
			tipiPayment.Credit_Payment_Type__c = creditPaymentType;
		}
		if(creditPaymentType == Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT) {
			tipiPayment.Total_Remaining_Balance__c = amount;
		}
		if(paymentShare < 1 && paymentShare > 0 && paymentShare != null) {
			tipiPayment.Calculation_Long__c += ' * [Payment Share: ' + (paymentShare * 100).setScale(2, RoundingMode.HALF_UP) + '%]';
		}

		return tipiPayment;
	}

	/**
     * To retrieve the latest Authorisation Id for every Contact
     *
     * @param paymentData PaymentDataWrapper containing validated payment configuration from the LEX component
     *
     * @return Map of Contact Id (key) to Authorisation Id (value)
     */
	private static Map<Id, Id> getAuthorisationsMapByContact(PaymentDataWrapper paymentData) {
		Map<Id, Id> authorisationsByContactId = new Map<Id, Id>();

		for (Tenant tenant : paymentData.tenants) {
			if (tenant.isPayee) {
				// populating all contact Id's -> this handles non-existing Authorisations during the <map>.get(contactId) operation
				authorisationsByContactId.put(tenant.contactId, null);
			}
		}

		for (asp04__Authorisation__c auth : [SELECT Id, Contact__c
		FROM asp04__Authorisation__c
		WHERE asp04__Status__c = 'In Force' AND Contact__c IN :authorisationsByContactId.keySet()
		ORDER BY asp04__Expiry_Date__c DESC]) {
			if (authorisationsByContactId.get(auth.Contact__c) == null) {
				authorisationsByContactId.put(auth.Contact__c, auth.Id);
			}
		}

		return authorisationsByContactId;
	}

	/**
     * To retrieve the latest Authorisation Id for every Contact
     *
     * @param Set<Id> contactIds containing contact Ids from BookedRentableItem
     *
     * @return Map of Contact Id (key) to Authorisation Id (value)
     */
	public static Map<Id, Id> getAuthorisationsMapByContact(Set<Id> contactIds) {
		Map<Id, Id> authorisationsByContactId = new Map<Id, Id>();

		for (Id cId :contactIds) {
			// populating all contact Id's -> this handles non-existing Authorisations during the <map>.get(contactId) operation
			authorisationsByContactId.put(cId, null);
		}

		for (asp04__Authorisation__c auth : [SELECT Id, Contact__c
		FROM asp04__Authorisation__c
		WHERE asp04__Status__c = 'In Force' AND Contact__c IN :contactIds
		ORDER BY asp04__Expiry_Date__c DESC])
		{
			if (authorisationsByContactId.get(auth.Contact__c) == null) {
				authorisationsByContactId.put(auth.Contact__c, auth.Id);
			}
		}

		return authorisationsByContactId;
	}

	/**
     * To retrieve the Id of a Queue by providing the API Name
     *
     * @param queueName String of Queue Developer Name
     *
     * @return Id requested Queue
     */
	public static Id getQueueIdByName(String queueName) {
		List<QueueSobject> queues = [SELECT Queue.Id FROM QueueSobject WHERE Queue.Type = 'Queue'
		AND Queue.DeveloperName = :queueName LIMIT 1];

		if (!queues.isEmpty()) {
			return queues[0].Queue.Id;
		} else {
			return null;
		}
	}

	/**
     * To map Contract.Type__c values to Tipi_Payments__c.Payment_Type__c values
     *
     * @param contract Contract record holding Type__c String
     *
     * @return String with mapped Payment_Type__c value
     */
	@TestVisible
	private static String getPaymentTypeFromContract(Contract contract) {
		String type = '';

		if(Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.containsKey(contract.Type__c)) {
			type = Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.get(contract.Type__c);
		} else {
			type = Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.get('Other');
		}
		return type;
	}

	/**
       * To retrieve a single Contract record filtered by record Id
       *
       * @param recordId Id of the retrieved record
       *
       * @return Contract SObject record
       */
	public static Contract getContractById(Id recordId) {
		List<Contract> contracts = [SELECT Id,
				Actual_Tenancy_End_Date__c,
				ContractTerm,
				Payment_Config_Done__c,
				StartDate,
				EndDate,
				Type__c,
				Unit__c,
				Unit__r.Building__c,
				Unit__r.Building__r.Development_Site__c,
				SBQQ__Opportunity__c,
				SBQQ__Opportunity__r.Deposit__c,
				SBQQ__Opportunity__r.Primary_Contact__c,
				SBQQ__Opportunity__r.Primary_Contact__r.Name,
				SBQQ__Opportunity__r.Primary_Contact__r.Advance_No_of_Months__c,
				SBQQ__Opportunity__r.Tenant_2__c,
				SBQQ__Opportunity__r.Tenant_2__r.Name,
				SBQQ__Opportunity__r.Tenant_2__r.Advance_No_of_Months__c,
				SBQQ__Opportunity__r.Tenant_3__c,
				SBQQ__Opportunity__r.Tenant_3__r.Name,
				SBQQ__Opportunity__r.Tenant_3__r.Advance_No_of_Months__c,
				SBQQ__Opportunity__r.Tenant_4__c,
				SBQQ__Opportunity__r.Tenant_4__r.Name,
				SBQQ__Opportunity__r.Tenant_4__r.Advance_No_of_Months__c,
				SBQQ__Opportunity__r.Tenant_5__c,
				SBQQ__Opportunity__r.Tenant_5__r.Name,
				SBQQ__Opportunity__r.Tenant_5__r.Advance_No_of_Months__c,
				SBQQ__Opportunity__r.Guarantor__c,
				SBQQ__Opportunity__r.Guarantor__r.Name,
				SBQQ__Opportunity__r.Guarantor__r.Advance_No_of_Months__c,
				SBQQ__Opportunity__r.Primary_Contact_Guarantor__c,
				SBQQ__Opportunity__r.Primary_Contact_Guarantor__r.Name,
				SBQQ__Opportunity__r.Primary_Contact_Guarantor__r.Advance_No_of_Months__c,
				SBQQ__Opportunity__r.Resident_2_Guarantor__c,
				SBQQ__Opportunity__r.Resident_2_Guarantor__r.Name,
				SBQQ__Opportunity__r.Resident_2_Guarantor__r.Advance_No_of_Months__c,
				SBQQ__Opportunity__r.Resident_3_Guarantor__c,
				SBQQ__Opportunity__r.Resident_3_Guarantor__r.Name,
				SBQQ__Opportunity__r.Resident_3_Guarantor__r.Advance_No_of_Months__c,
				SBQQ__Opportunity__r.Resident_4_Guarantor__c,
				SBQQ__Opportunity__r.Resident_4_Guarantor__r.Name,
				SBQQ__Opportunity__r.Resident_4_Guarantor__r.Advance_No_of_Months__c,
				SBQQ__Opportunity__r.Resident_5_Guarantor__c,
				SBQQ__Opportunity__r.Resident_5_Guarantor__r.Name,
				SBQQ__Opportunity__r.Resident_5_Guarantor__r.Advance_No_of_Months__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Unit_Rent_Rollup__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Monthly_Storage_Rollup__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Monthly_Parking_Rollup__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Amount__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Length__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Checkbox__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_Checkbox__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_First_Month__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Checkbox__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Text__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Parking_Concession_Amount__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Parking_Concession_Length__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Parking_Concession_Checkbox__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Parking_Off_First_Checkbox__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Parking_Off_First_Month__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Storage_Concession_Amount__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Storage_Concession_Length__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Storage_Concession_Checkbox__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Storage_Off_First_Checkbox__c,
				SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Storage_Off_First_Month__c
		FROM Contract
		WHERE Id = :recordId
		LIMIT 1];

		if(!contracts.isEmpty()) {
			return contracts[0];
		} else {
			return null;
		}
	}

	/**
     * To populate the 'Reservation' Tipi_Payments__c record for Contacts that have every paid the Reservation
     *
     * @param reservationByContactId Map of Contact Id to respective Tipi_Payments__c record
     *
     * @return Map of Contact Id to respective Tipi_Payments__c record
     */
	public static Map<Id, Tipi_Payments__c> getTipiPaymentsByContactIds(Map<Id, Tipi_Payments__c> reservationByContactId) {
		for(Tipi_Payments__c tipiPayment : [
				SELECT Id, Amount__c, Contact__c, Status__c,isPartialRent__c
				FROM Tipi_Payments__c
				WHERE Contact__c IN :reservationByContactId.keySet()
				AND Type__c = :Utils_Constants.PAYMENT_TYPE_RESERVATION
				AND Status__c != :Utils_Constants.CANCELLED_STATUS
				AND Status__c != :Utils_Constants.FAILED_STATUS
				AND Total_Remaining_Balance__c > 0
				ORDER BY CreatedDate
		]) {
			reservationByContactId.put(tipiPayment.Contact__c, tipiPayment);
		}
		return reservationByContactId;
	}
}