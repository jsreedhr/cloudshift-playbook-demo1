/**
 * @Name:        EmailHandler
 * @Description: Test Class for EmailHandler class
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 19/06/2018    Patrick Fischer    Created Class
 */
@IsTest
public with sharing class EmailHandlerTest {

    private static testMethod void sendEmailToContactSuccess() {
        List<Account> accounts = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 1, true);
        Map<Contact, SObject> sObjectsByContacts = new Map<Contact, SObject> {
                contacts[0] => accounts[0]
        };
        Boolean result = EmailHandler.sendEmailToContact(sObjectsByContacts, '%');
        System.assertEquals(true, result, 'Expecting successful sendEmail operation');
    }

    private static testMethod void sendEmailToContactWithOrgWideAdressFailure() {
        List<Account> accounts = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 1, true);
        Map<Contact, SObject> sObjectsByContacts = new Map<Contact, SObject> {
                contacts[0] => accounts[0]
        };
        Boolean result = EmailHandler.sendEmailToContact(sObjectsByContacts, null, null);
        System.assertEquals(false, result, 'Expecting failed sendEmail operation');
    }

    private static testMethod void constructEmailSuccess() {
        List<Account> accounts = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 1, true);
        Id templateId = EmailHandler.getTemplateIdByName('%');
        Id whoId = contacts[0].Id;
        Id whatId = accounts[0].Id;
        Id oweaId = accounts[0].Id; // it's a false Id as we can't create OrgWideEmailAddress in Test Classes
        List<String> emailAdresses = new List<String> { 'email@test.com' };

        List<Messaging.SingleEmailMessage> emailMessages = EmailHandler.constructEmail(templateId, whoId, whatId, oweaId, emailAdresses);
        System.assertEquals(1, emailMessages.size(), 'Expecting 1 Exception record being logged');
    }

    private static testMethod void sendEmailsFailure() {
        Boolean result = EmailHandler.sendEmails(new List<Messaging.SingleEmailMessage> { new Messaging.SingleEmailMessage() });
        System.assertEquals(false, result, 'Expecting failed sendEmail operation due to missing attributes');
        System.assertEquals(1, [SELECT Id FROM Custom_Log__c].size(), 'Expecting 1 Exception record being logged');
    }

//
//    private static testMethod void sendEmailToContactSuccess() {
//        EmailTemplate emailTemplate = EmailHandler.getTemplateByName('%');
//        OrgWideEmailAddress orgWideEmailAddress = new OrgWideEmailAddress();
//        List<Account> accounts = TestFactoryPayment.addAccounts(1, true);
//        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 1, true);
//        Map<Contact, SObject> sObjectsByContacts = new Map<Contact, SObject> {
//                contacts[0] => accounts[0]
//        };
//        EmailHandler.sendEmailToContact(sObjectsByContacts, emailTemplate, orgWideEmailAddress);
//        System.assertNotEquals(null, emailTemplate, 'Expecting Email Template to exist');
//    }
//
//    private static testMethod void getTemplateByNameSuccess() {
//        EmailTemplate emailTemplate = EmailHandler.getTemplateByName('%');
//        System.assertNotEquals(null, emailTemplate, 'Expecting Email Template to exist');
//    }
//
//    private static testMethod void getTemplateByNameFailure() {
//        EmailTemplate emailTemplate = EmailHandler.getTemplateByName(null);
//        System.assertEquals(null, emailTemplate, 'Expecting No Email Template to be retrieved');
//    }
//
//    private static testMethod void getOrgWideEmailAddressDefaultFailure() {
//        OrgWideEmailAddress orgWideEmailAddress = EmailHandler.getOrgWideEmailAddressDefault();
//        System.assertEquals(null, orgWideEmailAddress, 'Expecting Org Wide Email Address to be retrieved');
//    }
//
//    private static testMethod void getOrgWideEmailAddressFailure() {
//        OrgWideEmailAddress orgWideEmailAddress = EmailHandler.getOrgWideEmailAddress(null);
//        System.assertEquals(null, orgWideEmailAddress, 'Expecting No Org Wide Email Address to be retrieved');
//    }
}