/**
 *  Name: ContentAndFileHelper
 *
 *  Purpose: This is a helper class, will hold all methods and  functionalities that are to be build for File and content
 *
 *  History:
 *  Version         Author              Date               Detail
 *  1.0             Jared Watson        07/30/17           US-476  - Created
 *  1.1             Jared Watson        07/30/17           US-476  - add method updateLatestImageId
 */
public with sharing class ContentAndFileHelper {

    private static final String UNIT_FILE_PREFIX = Unit_File__c.SObjectType.getDescribe().getKeyPrefix();
    private static final String CLASS_NAME = 'ContentAndFileHelper';
    /**
    * @author    Jared Watson (jared@cloudshiftgroup.com)
    * @since     21/03/2018
    * @desc      Set the LatestVersionId on parent object for this inserted attachment.  Allows use for formula fields to create thumbnails.
    * @param     newContentDocumentLinkList New Content Document Link list
    * @US        US-0476
    */
    public static void updateLatestPublishedImageVersionVoid(List<ContentDocumentLink> newContentDocumentLinkList){

        List<Unit_File__c> ufsToUpdate = new List<Unit_File__c>();
        List<Id> contentDocumentIds = new List<Id>();

        //ContentDocumentLink Object can only be filtered on ContentDocumentId or LinkedEntityId, so get list of scope Ids.
        for(ContentDocumentLink cdl: newContentDocumentLinkList){
            contentDocumentIds.add(cdl.ContentDocumentId);
        }

        //for all Ids in new trigger Query and fetch only those for Unit Files Object, also retrieve LatestPublishedVersionId
            //NOTE: this could be extended to be for other objects and will be refactored as such if this is case on project
        for(ContentDocumentLink cdl : [select id, ContentDocumentId, ContentDocument.LatestPublishedVersionId, LinkedEntityId
                                        from ContentDocumentLink
                                        where ContentDocumentId IN :ContentDocumentIds]){
            //only those content uploads against the Unit File object are being processed!
            if(String.valueOf(cdl.LinkedEntityId).startsWith(UNIT_FILE_PREFIX)) {
                Unit_File__c uf = new Unit_File__c();
                uf.Id = cdl.LinkedEntityId;
                uf.Latest_Image_Version_ID__c = cdl.ContentDocument.latestPublishedVersionId;
                ufsToUpdate.add(uf);
            }
        }

        try{
            Database.update(ufsToUpdate);
        }
        catch(DMLException e){
            CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'DML Exception:' + CLASS_NAME + e.getMessage());
        }
        catch(Exception e){
            CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'Exception:' + CLASS_NAME + e.getMessage());
        }
        finally{
            CustomLogger.save();
        }

    }
}