/**
    * @Name:        AggregationsInterface
    * @Description: Interface to be implemented to provide required implementations for integrations with
    *                   Aggregators.
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 06/04/2018    Eugene Konstantinov    Created Class
*/
public interface AggregationsInterface {

     /**
    * Main method to be implemented in any class that implements this interface
    * @param  aggregator  argument contains all needed values to do callout
    * @param  units list of units to be processed to RightMove API
    */
    Map<Id, String> processRequest(List<Unit__c> units, Aggregator_Setting__mdt aggregator);

}