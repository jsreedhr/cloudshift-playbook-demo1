/**
 * @Name:        PaymentConfiguratorController
 * @Description: Aura Controller class to handle server side operations of c:paymentConfigurator component
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 08/05/2018    Patrick Fischer    Created Class
 */
public without sharing class PaymentConfiguratorController {

    public static final String PARKING_STATUS = 'Parking';
    public static final String STORAGE_STATUS = 'Storage';

    /**
     * To initialise the Payment Configurator component and fetch the required information
     *
     * @param recordId related Contract record
     *
     * @return Object Wrapper containing all Tenants and values for Rent, Deposit, Up Front payments
     */
    @AuraEnabled
    public static PaymentDataWrapper initialiseComponent(Id recordId) {

        PaymentDataWrapper paymentData = null;
        Decimal rent = 0;
        Decimal deposit = 0;
        String errorMsg = '';
        List<PaymentDataWrapper.PicklistWrapper> storagePicklist = new List<PaymentDataWrapper.PicklistWrapper>();
        List<PaymentDataWrapper.PicklistWrapper> parkingPicklist = new List<PaymentDataWrapper.PicklistWrapper>();

        try {
            Contract contract = PaymentConfiguratorService.getContractById(recordId);
            List<SBQQ__QuoteLine__c> quoteLineItems = getQuoteLineItemsByQuoteId(contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__c);
            List<Rentable_Item__c> rentableItems = getAvailableRentableItemsByQuoteLine(quoteLineItems);

            if (contract != null) {
                if(contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Unit_Rent_Rollup__c != null) {
                    rent = contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Unit_Rent_Rollup__c;
                }
                Decimal storageCount = countQuoteLineByType(quoteLineItems, STORAGE_STATUS);
                String storageString = constructStorageString(quoteLineItems);
                Decimal parking = countQuoteLineByType(quoteLineItems, PARKING_STATUS);

                if(contract.SBQQ__Opportunity__r.Deposit__c != null) {
                    deposit = contract.SBQQ__Opportunity__r.Deposit__c;
                }

                List<String> paymentGateways = new List<String>();
                for(Schema.PicklistEntry picklistVal : Tipi_Payments__c.Payment_Gateway__c.getDescribe().getPicklistValues()) {
                    if(picklistVal.getValue() != Utils_Constants.PAYMENT_GATEWAY_DD) {
                        paymentGateways.add(picklistVal.getValue());
                    }
                }

                for(Rentable_Item__c rentableItem : rentableItems) {
                    if(rentableItem.Product__r.Product_Type__c == STORAGE_STATUS) {
                        storagePicklist.add(new PaymentDataWrapper.PicklistWrapper(rentableItem.Id, rentableItem.Product__r.Size__c + ' - ' + rentableItem.Name));
                    } else if (rentableItem.Product__r.Product_Type__c == PARKING_STATUS) {
                        parkingPicklist.add(new PaymentDataWrapper.PicklistWrapper(rentableItem.Id, rentableItem.Name));
                    }
                }

                List<Tenant> tenants = constructTenants(contract);
                if(!tenants.isEmpty()) {
                    tenants[0].isPayee = true;
                    tenants[0].rent = rent;
                    tenants[0].depositPayment = deposit;
                }

                if(contract.Payment_Config_Done__c) {
                    errorMsg = System.Label.ERROR_PAYMENT_CONFIGURATOR_ALREADY_CONFIGURED;
                }

                Tipi_Payments__c firstPayment = constructFirstRentPayment(contract, rent);
                paymentData = new PaymentDataWrapper(contract.Payment_Config_Done__c, rent, storageCount, storageString,
                        parking, deposit, tenants, errorMsg, paymentGateways, storagePicklist,
                        parkingPicklist, contract.StartDate, contract.EndDate, firstPayment.Amount__c.setScale(2, RoundingMode.HALF_UP));
                paymentData.firstRentCalculation = firstPayment.Calculation_Long__c;

                if(contract.Type__c == null) {
                    paymentData = new PaymentDataWrapper('Please specify the Lease \'Type\' before using the Payment Configurator.');
                }
            }
        } catch (Exception e) {
            paymentData = new PaymentDataWrapper('Initialisation Error: ' + e.getMessage());
        }

        return paymentData;
    }

    /**
     * To calculate the payment schedule and commit the entered payment details to the Database
     *
     * @param paymentDataStr String of validated paymentData Wrapper
     * @param recordId related Contract record
     *
     * @return Object Wrapper containing all Tenants and values for Rent, Deposit, Up Front payments
     */
    @AuraEnabled
    public static PaymentDataWrapper savePaymentConfiguration(String paymentDataStr, String recordId) {
        PaymentDataWrapper paymentData = new PaymentDataWrapper('');

        try {
            paymentData = (PaymentDataWrapper) System.JSON.deserialize(paymentDataStr, PaymentDataWrapper.class);
            paymentData.isSavedSuccessfully = false;
            Contract contract = PaymentConfiguratorService.getContractById(recordId);
            saveOpportunityChanges(contract, paymentData);
            List<Tipi_Payments__c> tipiPayments = PaymentConfiguratorService.constructPaymentRecords(contract, paymentData);
            List<Booked_Rentable_Item__c> bookedRentableItems = PaymentConfiguratorService.constructBookedRentableItems(contract, paymentData);
            paymentData.errorMessage = insertPaymentConfiguration(tipiPayments, bookedRentableItems, contract);
            if(paymentData.errorMessage == '') {
                paymentData.isSavedSuccessfully = true;
            }
        } catch (Exception e) {
            paymentData.errorMessage = 'Saving Error: ' + e.getMessage();
            paymentData.isSavedSuccessfully = false;
        }

        return paymentData;
    }


    /**
     * To create a List of tenants from the related Opportunity lookups
     *
     * @param contract SObject record that includes the applicable lookups to all tenants
     *
     * @return List of Tenants
     */
    private static List<Tenant> constructTenants(Contract contract) {
        Map<Id, Tipi_Payments__c> tipiPaymentsByContactId = PaymentConfiguratorService.getTipiPaymentsByContactIds(
                new Map<Id, Tipi_Payments__c> {
                        contract.SBQQ__Opportunity__r.Primary_Contact__c => null,
                        contract.SBQQ__Opportunity__r.Tenant_2__c => null,
                        contract.SBQQ__Opportunity__r.Tenant_3__c => null,
                        contract.SBQQ__Opportunity__r.Tenant_4__c => null,
                        contract.SBQQ__Opportunity__r.Tenant_5__c => null,
                        contract.SBQQ__Opportunity__r.Guarantor__c => null,
                        contract.SBQQ__Opportunity__r.Primary_Contact_Guarantor__c => null,
                        contract.SBQQ__Opportunity__r.Resident_2_Guarantor__c => null,
                        contract.SBQQ__Opportunity__r.Resident_3_Guarantor__c => null,
                        contract.SBQQ__Opportunity__r.Resident_4_Guarantor__c => null,
                        contract.SBQQ__Opportunity__r.Resident_5_Guarantor__c => null
                }
        );

        List<Tenant> tenants = new List<Tenant>();
        if(contract.SBQQ__Opportunity__r.Primary_Contact__c != null) {
            tenants.add( new Tenant( contract.SBQQ__Opportunity__r.Primary_Contact__c,
                    contract.SBQQ__Opportunity__r.Primary_Contact__r.Name + ' (R1)',
                    contract.SBQQ__Opportunity__r.Primary_Contact__r.Advance_No_of_Months__c,
                    tipiPaymentsByContactId.get(contract.SBQQ__Opportunity__r.Primary_Contact__c)
            ));
        }
        if(contract.SBQQ__Opportunity__r.Tenant_2__c != null) {
            tenants.add( new Tenant( contract.SBQQ__Opportunity__r.Tenant_2__c,
                    contract.SBQQ__Opportunity__r.Tenant_2__r.Name + ' (R2)',
                    contract.SBQQ__Opportunity__r.Tenant_2__r.Advance_No_of_Months__c,
                    tipiPaymentsByContactId.get(contract.SBQQ__Opportunity__r.Tenant_2__c)
            ));
        }
        if(contract.SBQQ__Opportunity__r.Tenant_3__c != null) {
            tenants.add( new Tenant( contract.SBQQ__Opportunity__r.Tenant_3__c,
                    contract.SBQQ__Opportunity__r.Tenant_3__r.Name + ' (R3)',
                    contract.SBQQ__Opportunity__r.Tenant_3__r.Advance_No_of_Months__c,
                    tipiPaymentsByContactId.get(contract.SBQQ__Opportunity__r.Tenant_3__c)
            ));
        }
        if(contract.SBQQ__Opportunity__r.Tenant_4__c != null) {
            tenants.add( new Tenant( contract.SBQQ__Opportunity__r.Tenant_4__c,
                    contract.SBQQ__Opportunity__r.Tenant_4__r.Name + ' (R4)',
                    contract.SBQQ__Opportunity__r.Tenant_4__r.Advance_No_of_Months__c,
                    tipiPaymentsByContactId.get(contract.SBQQ__Opportunity__r.Tenant_4__c)
            ));
        }
        if(contract.SBQQ__Opportunity__r.Tenant_5__c != null) {
            tenants.add( new Tenant( contract.SBQQ__Opportunity__r.Tenant_5__c,
                    contract.SBQQ__Opportunity__r.Tenant_5__r.Name + ' (R5)',
                    contract.SBQQ__Opportunity__r.Tenant_5__r.Advance_No_of_Months__c,
                    tipiPaymentsByContactId.get(contract.SBQQ__Opportunity__r.Tenant_5__c)
            ));
        }
        if(contract.SBQQ__Opportunity__r.Guarantor__c != null) {
            Tenant tenant = new Tenant( contract.SBQQ__Opportunity__r.Guarantor__c,
                    contract.SBQQ__Opportunity__r.Guarantor__r.Name + ' (G)',
                    contract.SBQQ__Opportunity__r.Guarantor__r.Advance_No_of_Months__c,
                    tipiPaymentsByContactId.get(contract.SBQQ__Opportunity__r.Guarantor__c));
            tenant.isGuarantor = true;
            tenants.add(tenant);
        }
        if(contract.SBQQ__Opportunity__r.Primary_Contact_Guarantor__c != null) {
            Tenant tenant = new Tenant( contract.SBQQ__Opportunity__r.Primary_Contact_Guarantor__c,
                    contract.SBQQ__Opportunity__r.Primary_Contact_Guarantor__r.Name + ' (G1)',
                    contract.SBQQ__Opportunity__r.Primary_Contact_Guarantor__r.Advance_No_of_Months__c,
                    tipiPaymentsByContactId.get(contract.SBQQ__Opportunity__r.Primary_Contact_Guarantor__c));
            tenant.isGuarantor = true;
            tenants.add(tenant);
        }
        if(contract.SBQQ__Opportunity__r.Resident_2_Guarantor__c != null) {
            Tenant tenant = new Tenant( contract.SBQQ__Opportunity__r.Resident_2_Guarantor__c,
                    contract.SBQQ__Opportunity__r.Resident_2_Guarantor__r.Name + ' (G2)',
                    contract.SBQQ__Opportunity__r.Resident_2_Guarantor__r.Advance_No_of_Months__c,
                    tipiPaymentsByContactId.get(contract.SBQQ__Opportunity__r.Resident_2_Guarantor__c));
            tenant.isGuarantor = true;
            tenants.add(tenant);
        }
        if(contract.SBQQ__Opportunity__r.Resident_3_Guarantor__c != null) {
            Tenant tenant = new Tenant( contract.SBQQ__Opportunity__r.Resident_3_Guarantor__c,
                    contract.SBQQ__Opportunity__r.Resident_3_Guarantor__r.Name + ' (G3)',
                    contract.SBQQ__Opportunity__r.Resident_3_Guarantor__r.Advance_No_of_Months__c,
                    tipiPaymentsByContactId.get(contract.SBQQ__Opportunity__r.Resident_3_Guarantor__c));
            tenant.isGuarantor = true;
            tenants.add(tenant);
        }
        if(contract.SBQQ__Opportunity__r.Resident_4_Guarantor__c != null) {
            Tenant tenant = new Tenant( contract.SBQQ__Opportunity__r.Resident_4_Guarantor__c,
                    contract.SBQQ__Opportunity__r.Resident_4_Guarantor__r.Name + ' (G4)',
                    contract.SBQQ__Opportunity__r.Resident_4_Guarantor__r.Advance_No_of_Months__c,
                    tipiPaymentsByContactId.get(contract.SBQQ__Opportunity__r.Resident_4_Guarantor__c));
            tenant.isGuarantor = true;
            tenants.add(tenant);
        }
        if(contract.SBQQ__Opportunity__r.Resident_5_Guarantor__c != null) {
            Tenant tenant = new Tenant( contract.SBQQ__Opportunity__r.Resident_5_Guarantor__c,
                    contract.SBQQ__Opportunity__r.Resident_5_Guarantor__r.Name + ' (G5)',
                    contract.SBQQ__Opportunity__r.Resident_5_Guarantor__r.Advance_No_of_Months__c,
                    tipiPaymentsByContactId.get(contract.SBQQ__Opportunity__r.Resident_5_Guarantor__c));
            tenant.isGuarantor = true;
            tenants.add(tenant);
        }

        return tenants;
    }

    /**
     * To retrieve a single Opportunity record filtered by record Id
     *
     * @param recordId Id of the retrieved record
     *
     * @return Opportunity SObject record
     */
    @TestVisible
    private static Opportunity getOpportunityById(Id recordId) {
        List<Opportunity> opportunities = [SELECT Id, Deposit__c FROM Opportunity WHERE Id = :recordId];

        if(!opportunities.isEmpty()) {
            return opportunities[0];
        } else {
            return null;
        }
    }

    /**
     * To save changes made to the Opportunity (Deposit & Up_Front_Payment) via the LEX component
     *
     * @param contract Contract SObject holding the link to an Opportunity
     * @param paymentData PaymentDataWrapper containing validated payment configuration from the LEX component
     *
     * @return true if successful operation, false if failed to save update
     */
    private static Boolean saveOpportunityChanges(Contract contract, PaymentDataWrapper paymentData) {
        Opportunity opportunity = getOpportunityById(contract.SBQQ__Opportunity__c);
        Boolean isSuccess = true;

        if(opportunity != null) {
            if (opportunity.Deposit__c != paymentData.depositPayment) {
                opportunity.Deposit__c = paymentData.depositPayment;
                isSuccess = Database.update(opportunity).isSuccess();
            }
        }
        return isSuccess;
    }

    /**
     * To insert Tipi Payments and on success: update the flag on contract
     *
     * @param tipiPayments List of Tipi_Payments__c records to be inserted
     * @param bookedRentableItems List of Booked_Rentable_Item__c records to be inserted
     * @param contract Contract SObject holding
     *
     * @return Empty String if success, else Exception Error Message returned
     */
    @TestVisible
    private static String insertPaymentConfiguration(List<Tipi_Payments__c> tipiPayments,
            List<Booked_Rentable_Item__c> bookedRentableItems, Contract contract) {
        String errorMsg = '';
        Savepoint sp = Database.setSavepoint();

        try {
            // insert Booked_Rentable_Item__c records
            if(!bookedRentableItems.isEmpty()) {
                insert bookedRentableItems;
            }

            // insert Tipi_Payments__c records
            if(!tipiPayments.isEmpty()) {
                insert tipiPayments;
            }

            // Set Flag to restrict 'Payment Configurator' modal access
            // -> successful once 'insert bookedRentableItems' and 'insert tipiPayments' don't fail
            contract.Payment_Config_Done__c = true;
            update contract;

        } catch (Exception e) {
            errorMsg = e.getMessage();
            Database.rollback(sp);
        }

        return errorMsg;
    }

    /**
     * To query a list of child SBQQ__QuoteLine__c records for every quote recordId
     *
     * @param recordId Id of SBQQ__Quote__c record
     *
     * @return List of SBQQ__QuoteLine__c records
     */
    public static List<SBQQ__QuoteLine__c> getQuoteLineItemsByQuoteId(Id recordId) {
        return [SELECT Id, SBQQ__EffectiveStartDate__c, SBQQ__EffectiveEndDate__c, SBQQ__Quote__c, SBQQ__Product__c,
                SBQQ__Product__r.Family, SBQQ__Product__r.Size__c, SBQQ__Product__r.Product_Type__c
                FROM SBQQ__QuoteLine__c
                WHERE SBQQ__Quote__c = :recordId
                AND (SBQQ__Product__r.Product_Type__c = :PARKING_STATUS
                OR SBQQ__Product__r.Product_Type__c = :STORAGE_STATUS)];
    }

    /**
     * To query a list of available Rentable_Item__c records by querying quoted products
     *
     * @param quoteLines list of SBQQ__QuoteLine__c records for which to query Rentable_Item__c
     *
     * @return list of Rentable_Item__c records
     */
    private static List<Rentable_Item__c> getAvailableRentableItemsByQuoteLine(List<SBQQ__QuoteLine__c> quoteLines) {
        Set<Id> productIds = new Set<Id>();
        for(SBQQ__QuoteLine__c quoteLine : quoteLines) {
            productIds.add(quoteLine.SBQQ__Product__c);
        }

        return [SELECT Id, Name, Product__r.Size__c, Product__r.Product_Type__c
                FROM Rentable_Item__c
                WHERE Product__c IN :productIds AND Status__c = 'Available'
                ORDER BY Product__r.Size__c DESC];
    }

    /**
     * To count all quoted products of a specific type
     *
     * @param quoteLines list of SBQQ__QuoteLine__c records for which to query Rentable_Item__c
     * @param type String of quoted product types to count, i.e. 'Parking' or 'Storage'
     *
     * @return Decimal amount of quoted products counted
     */
    private static Decimal countQuoteLineByType(List<SBQQ__QuoteLine__c> quoteLineItems, String type) {
        Decimal counter = 0;
        for(SBQQ__QuoteLine__c quoteLineItem : quoteLineItems) {
            if(quoteLineItem.SBQQ__Product__r.Product_Type__c == type) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * To construct a String defining the sizes of rentable storage items
     *
     * @param quoteLineItems list of SBQQ__QuoteLine__c records containing quotes storages
     *
     * @return String containing the amount of quoted items for each storage size
     */
    private static String constructStorageString(List<SBQQ__QuoteLine__c> quoteLineItems) {
        Integer countS = 0;
        Integer countM = 0;
        Integer countL = 0;
        Integer countXL = 0;

        for(SBQQ__QuoteLine__c quoteLineItem : quoteLineItems) {
            if(quoteLineItem.SBQQ__Product__r.Product_Type__c == STORAGE_STATUS) {
                if (quoteLineItem.SBQQ__Product__r.Size__c == 'S') {
                    countS++;
                } else if (quoteLineItem.SBQQ__Product__r.Size__c == 'M') {
                    countM++;
                } else if (quoteLineItem.SBQQ__Product__r.Size__c == 'L') {
                    countL++;
                } else if (quoteLineItem.SBQQ__Product__r.Size__c == 'XL') {
                    countXL++;
                }
            }
        }

        return 'S:' + countS + ', M:' + countM + ', L:' + countL + ', XL:' + countXL;
    }

    /**
     * To construct the total amount to be paid as part of the first payment with concession applied
     *
     * @param contract Contract SObject holding the link to an Opportunity
     * @param rent Decimal amount of monthly rent without any concession applied
     *
     * @return Decimal rent with concession applied
     */
    private static Tipi_Payments__c constructFirstRentPayment(Contract contract, Decimal rent) {
        Tipi_Payments__c firstPayment = null;

        // construct monthly payments without concession
        List<Tipi_Payments__c> normalisedRent = PaymentConfiguratorService.normalisePaymentRecords(contract, null, rent, null, rent, null, null);

        // include concession for monthly payments
        List<Tipi_Payments__c> rentAppliedConcession = PaymentConfiguratorService.applyConcession(normalisedRent,
                contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Checkbox__c,
                contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Length__c,
                contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Amount__c,
                contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_Checkbox__c,
                contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_First_Month__c,
                contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Checkbox__c,
                contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Text__c);

        if(!rentAppliedConcession.isEmpty()) {
            firstPayment = rentAppliedConcession[0];
        }

        return firstPayment;
    }

}