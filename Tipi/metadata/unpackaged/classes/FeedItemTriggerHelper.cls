/**
* @Name:        FeedItemTriggerHelper
* @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
*
* @author:      Patrick Fischer
* @version:     1.0
* Change Log
*
* Date          author              Change Description
* -----------------------------------------------------------------------------------
* 12/06/2018    Patrick Fischer     Created Class
*/
public with sharing class FeedItemTriggerHelper {

    public static final String USER_PREFIX = SObjectType.User.getKeyPrefix();
    public static final String EXPRESSION = '\\{@(' + USER_PREFIX +'[0-9a-zA-Z]{15})\\}';
    public static Pattern RAW_MENTION = Pattern.compile(EXPRESSION);

    public static void processBeforeInsert(List<FeedItem> feedItems) {
        verifyMentions(feedItems);
    }

    public static void processBeforeUpdate(List<FeedItem> feedItems) {
        verifyMentions(feedItems);
    }

    /**
     * To validate Chatter Posts for @mentions of Customers / Community Users / Portal Users and throw front-end errors
     *
     * @param feedItems List of FeedItem records to validate for Portal @mentions
     */
    @TestVisible
    private static Boolean verifyMentions(List<FeedItem> feedItems) {
        Boolean isValid = true;
        Map<FeedItem, Set<Id>> userIdsByFeedItem = new Map<FeedItem, Set<Id>>();
        Set<Id> userIdsToQuery = new Set<Id>();

        for(FeedItem feedItem : feedItems) {
            Set<Id> userIds = new Set<Id>();
            for (Mention mention : (List<Mention>) JSON.deserialize(JSON.serialize(new List<FeedItem> {feedItem}), List<Mention>.class)) {
                userIds.addAll(mention.getUserIds());
            }
            userIdsByFeedItem.put(feedItem, userIds);
            userIdsToQuery.addAll(userIds);
        }

        Set<Id> portalUserIds = (new Map<Id, User>([SELECT Id FROM User WHERE Id IN :userIdsToQuery AND IsPortalEnabled = TRUE])).keySet();

        for(FeedItem feedItem : feedItems) {
            for(Id userId : userIdsByFeedItem.get(feedItem)) {
                if(portalUserIds.contains(userId)) {
                    isValid = false;
                    feedItem.addError(System.Label.ERROR_CHATTER_POST_MENTIONS_CUSTOMER);
                }
            }
        }

        return isValid;
    }

    /**
     * Class to deserialize Chatter Mentions
     */
    public class Mention {

        public final String rawBody; // body of Chatter Post
        public final Id parentId; // parent Id on which object the Chatter Post was made (i.e. Opportunity Id)

        /**
         * To parse User Ids of a Chatter Post to Id's using regex
         *
         * @return Set<Id> of User Ids mentioned in the Chatter Post
         */
        public Set<Id> getUserIds() {
            Set<Id> userIds = new Set<Id>();
            Matcher m = RAW_MENTION.matcher(rawBody);
            while (m.find()) {
                userIds.add(m.group(1));
            }
            return userIds;
        }
    }
}