/**
 * @Name:        RelatedContactsPaymentRollupController
 * @Description: Aura Controller class to handle server side operations of c:relatedContactsPaymentRollup component
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 18/07/2018    Patrick Fischer    Created Class
 */
public with sharing class RelatedContactsPaymentRollupController {

    /**
     * Wrapper Class to hold summarised details of every Contact such as charge and payment amounts
     */
    public class ContactFinancialSummary {
        @AuraEnabled public String Name { get; set; }
        @AuraEnabled public Id contactId { get; set; }
        @AuraEnabled public Decimal paymentAmount { get; set; }
        @AuraEnabled public Decimal chargeAmount { get; set; }
        @AuraEnabled public Decimal futureChargesAmount { get; set; }

        public ContactFinancialSummary(String Name, Id contactId, Decimal chargeAmount, Decimal paymentAmount, Decimal futureChargesAmount) {
            this.Name = Name;
            this.contactId = contactId;
            this.chargeAmount = chargeAmount;
            this.paymentAmount = paymentAmount;
            this.futureChargesAmount = futureChargesAmount;
        }
    }

    /**
     * To initialise the c:relatedContactsPaymentRollup LEX component for the visualised context
     * (on Opportunity, Contract, or Contact)
     *
     * @param recordId Id of the current record context to be initialised (on Opportunity, Contract, Contact)
     *
     * @return List of ContactFinancialSummary wrapper items representing a List of Contacts
     */
    @AuraEnabled
    public static List<ContactFinancialSummary> initialiseComponent(Id recordId) {
        Set<Id> contactIds = new Set<Id>();

        if(recordId != null) {
            if (recordId.getSobjectType() == Contact.SObjectType) {
                contactIds.add(recordId);
            } else if (recordId.getSobjectType() == Opportunity.SObjectType) {
                contactIds.addAll(getContactIdsForOpportunity(recordId));
            } else if (recordId.getSobjectType() == Contract.SObjectType) {
                contactIds.addAll(getContactIdsForContract(recordId));
            }
        }
        
        return rollupPaymentsByContact(contactIds);
    }

    /**
     * To summarise the charge and payment amounts for a set of Contact Ids
     *
     * @param contactIds Set of Contact Ids to summarise
     *
     * @return List of individual Contacts (represented as ContactFinancialSummary wrapper items) for the
     *         visualised context (on Opportunity, Contract, Contact)
     */
    private static List<ContactFinancialSummary> rollupPaymentsByContact(Set<Id> contactIds) {
        List<ContactFinancialSummary> contactFinancialSummaryList = new List<ContactFinancialSummary>();

        for(Contact contact : [
                SELECT Id, Name, (SELECT Id, Charge_Amount__c, Payment_Amount__c, Payment_Date__c FROM Financial_Summary__r)
                FROM Contact
                WHERE Id IN :contactIds
                ORDER BY Name
        ]) {
            Decimal chargeAmount = 0;
            Decimal paymentAmount = 0;
            Decimal futureChargesAmount = 0;

            for(Financial_Summary__c financialSummary : contact.Financial_Summary__r) {
                if(financialSummary.Charge_Amount__c != 0 && financialSummary.Charge_Amount__c != null) {
                    if(financialSummary.Payment_Date__c <= Date.today()) {
                        chargeAmount += financialSummary.Charge_Amount__c;
                    } else {
                        futureChargesAmount += financialSummary.Charge_Amount__c;
                    }
                }

                if(financialSummary.Payment_Amount__c != 0 && financialSummary.Payment_Amount__c != null) {
                    paymentAmount += financialSummary.Payment_Amount__c;
                }
            }

            contactFinancialSummaryList.add(new ContactFinancialSummary(
                    contact.Name, contact.Id, chargeAmount.setScale(2, RoundingMode.HALF_UP),
                    paymentAmount.setScale(2, RoundingMode.HALF_UP), futureChargesAmount.setScale(2, RoundingMode.HALF_UP)
            ));
        }

        return contactFinancialSummaryList;
    }

    /**
     * To get all related contact Ids from a single Opportunity record
     *
     * @param opportunityId Id of an Opportunity record
     *
     * @return Set of Ids of Contact records
     */
    private static Set<Id> getContactIdsForOpportunity(Id opportunityId) {
        Opportunity opportunity = [
                SELECT  Primary_Contact__c, Tenant_2__c, Tenant_3__c, Tenant_4__c, Tenant_5__c, Guarantor__c,
                        Primary_Contact_Guarantor__c, Resident_2_Guarantor__c, Resident_3_Guarantor__c,
                        Resident_4_Guarantor__c, Resident_5_Guarantor__c
                FROM    Opportunity
                WHERE   Id = :opportunityId
        ];

        return new Set<Id> {
                opportunity.Primary_Contact__c,
                opportunity.Tenant_2__c,
                opportunity.Tenant_3__c,
                opportunity.Tenant_4__c,
                opportunity.Tenant_5__c,
                opportunity.Guarantor__c,
                opportunity.Primary_Contact_Guarantor__c,
                opportunity.Resident_2_Guarantor__c,
                opportunity.Resident_3_Guarantor__c,
                opportunity.Resident_4_Guarantor__c,
                opportunity.Resident_5_Guarantor__c
        };
    }

    /**
     * To get all related contact Ids from a single Contract record
     *
     * @param contractId Id of an Contract record
     *
     * @return Set of Ids of Contact records
     */
    private static Set<Id> getContactIdsForContract(Id contractId) {
        Contract contract = [
                SELECT  Primary_Contact__c, Tenant_2__c, Tenant_3__c, Tenant_4__c, Tenant_5__c, Guarantor__c,
                        Primary_Contact_Guarantor__c, Resident_2_Guarantor__c, Resident_3_Guarantor__c,
                        Resident_4_Guarantor__c, Resident_5_Guarantor__c
                FROM    Contract
                WHERE   Id = :contractId
        ];

        return new Set<Id> {
                contract.Primary_Contact__c,
                contract.Tenant_2__c,
                contract.Tenant_3__c,
                contract.Tenant_4__c,
                contract.Tenant_5__c,
                contract.Guarantor__c,
                contract.Primary_Contact_Guarantor__c,
                contract.Resident_2_Guarantor__c,
                contract.Resident_3_Guarantor__c,
                contract.Resident_4_Guarantor__c,
                contract.Resident_5_Guarantor__c
        };
    }
}