/**
 * @Name:        LeaseTriggerTest
 * @Description: Test class for LeaseTrigger functionality
 *
 * @author:      Andrey Korobovich
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 30/05/2018    Andrey Korobovich    Created Class
 * 01/06/2018    Eugene Konstantinov  Class Refactoring; additional scenarios added
 * 13/06/2018    Patrick Fischer      Added Code coverage for Lease Id & Unit Id populated on Opp + Contact
 */
@IsTest
public with sharing class LeaseTriggerTest {

    private static testMethod void testLeaseTrigger() {
        List<Account> accs = [SELECT Id FROM Account LIMIT 1];
        List<Unit__c> units = [SELECT Id FROM Unit__c LIMIT 1];
        List<Opportunity> opps = [SELECT Id FROM Opportunity LIMIT 1];

        Test.startTest();
        Contract contr = new Contract(
                Unit__c = units[0].Id,
                SBQQ__Opportunity__c = opps[0].Id,
                AccountId = accs[0].Id
        );
        insert contr;

        contr.StartDate = Date.today();
        update contr;
        Test.stopTest();

        Contract contrAfterUpd = [SELECT Id, Unit__c, SBQQ__Opportunity__r.Leased_Reserved_Unit__c FROM Contract LIMIT 1];
        System.assertEquals(contrAfterUpd.SBQQ__Opportunity__r.Leased_Reserved_Unit__c , contrAfterUpd.Unit__c);
    }

    private static testMethod void testLeaseTriggerEndDate() {
        List<Account> accs = [SELECT Id FROM Account LIMIT 1];
        List<Unit__c> units = [SELECT Id, Building__c, Building__r.Development_Site__c FROM Unit__c LIMIT 1];
        List<Opportunity> opps = [SELECT Id, Primary_Contact__c FROM Opportunity LIMIT 1];
        SBQQ__Quote__c newQuote = new SBQQ__Quote__c();
        newQuote.SBQQ__Opportunity2__c = opps[0].Id;
        insert newQuote;

        opps[0].SBQQ__PrimaryQuote__c = newQuote.Id;
        update opps;
        Id rentableItemRecTypeId = Schema.SObjectType.Rentable_Item__c.getRecordTypeInfosByName().get('Parking').getRecordTypeId();
        Rentable_Item__c item  = new Rentable_Item__c(
            RecordTypeId = rentableItemRecTypeId,
            Building__c = units[0].Building__c,
            Name = 'Test',
            Development__c = units[0].Building__r.Development_Site__c
        );
        insert item;
        Tipi_Payments__c payment = new Tipi_Payments__c();
        payment.Opportunity__c = opps[0].Id;
        payment.Contact__c = opps[0].Primary_Contact__c;
        payment.Unit__c = units[0].Id;
        payment.Amount__c = 2500;
        payment.Status__c = 'Awaiting submission';
        payment.Payment_Date__c = Date.today().toStartOfMonth() + 31;
        payment.Rentable_Item__c = item.Id;
        payment.Development__c = units[0].Building__r.Development_Site__c;
        insert payment;

        Test.startTest();
        Contract contr = new Contract();
        contr.Unit__c = units[0].Id;
        contr.SBQQ__Opportunity__c = opps[0].Id;
        contr.AccountId = accs[0].Id;
        contr.Actual_Tenancy_End_Date__c = Date.today() + 25;
        insert contr;
        Booked_Rentable_Item__c bookedItem = new Booked_Rentable_Item__c(
            Contract__c = contr.Id,
            Contact__c = opps[0].Primary_Contact__c,
            End_Date__c =  Date.today() + 25,
            Start_Date__c = Date.today(),
            Rentable_Item__c = item.Id,
            Status__c = 'Pending Document Send'
        );
        contr.StartDate = Date.today();
        contr.Actual_Tenancy_End_Date__c = Date.today();
        insert bookedItem;
        bookedItem.Status__c = 'Allocated';
        update bookedItem;
        update contr;
        Test.stopTest();
        List<Booked_Rentable_Item__c> brItems = [SELECT Id, End_Date__c FROM Booked_Rentable_Item__c];
        System.assertEquals(1, brItems.size());
        System.assertEquals(Date.today(), brItems[0].End_Date__c);
    }
    
	private static testMethod void testLeaseTriggerPaymentsCreation() {
        List<Account> accs = [SELECT Id FROM Account LIMIT 1];
        List<Unit__c> units = [SELECT Id, Building__c, Building__r.Development_Site__c FROM Unit__c LIMIT 1];
        List<Opportunity> opps = [SELECT Id, Primary_Contact__c FROM Opportunity LIMIT 1];
        
        SBQQ__Quote__c newQuote = new SBQQ__Quote__c();
        newQuote.SBQQ__Opportunity2__c = opps[0].Id;
        insert newQuote;

        opps[0].SBQQ__PrimaryQuote__c = newQuote.Id;
        update opps;
        
        Tipi_Payments__c payment = new Tipi_Payments__c();
        payment.Opportunity__c = opps[0].Id;
        payment.Contact__c = opps[0].Primary_Contact__c;
        payment.Unit__c = units[0].Id;
        payment.Amount__c = 2500;
        payment.Status__c = 'Awaiting submission';
        payment.Payment_Date__c = Date.today().toStartOfMonth() + 31;
        payment.Development__c = units[0].Building__r.Development_Site__c;
        insert payment;

        Test.startTest();
            Contract contr = new Contract();
            contr.Unit__c = units[0].Id;
            contr.SBQQ__Opportunity__c = opps[0].Id;
            contr.AccountId = accs[0].Id;
            contr.Primary_Contact__c = opps[0].Primary_Contact__c;
            contr.Actual_Tenancy_End_Date__c = Date.today() + 25;
            insert contr;
    
            contr.StartDate = Date.today();
            contr.Actual_Tenancy_End_Date__c = Date.today();
            update contr;
        Test.stopTest();
    }
    
    private static testMethod void testLeaseTriggerMonthToMonth() {
        List<Account> accs = [SELECT Id FROM Account LIMIT 1];
        List<Unit__c> units = [SELECT Id, Building__c, Building__r.Development_Site__c FROM Unit__c LIMIT 1];
        List<Opportunity> opps = [SELECT Id, Primary_Contact__c FROM Opportunity LIMIT 1];
        
        Contract contr = new Contract();
        contr.Unit__c = units[0].Id;
        contr.SBQQ__Opportunity__c = opps[0].Id;
        contr.AccountId = accs[0].Id;
        contr.StartDate = Date.today();
        contr.Actual_Tenancy_End_Date__c = Date.today() ;
        insert contr; 

        Test.startTest();  
        	contr.Actual_Tenancy_End_Date__c = Date.today() + 25;
        	update contr;
        Test.stopTest();
        
        List<Contract> contract = [SELECT Id, Month_To_Month__c FROM Contract WHERE Month_To_Month__c = true];
        System.assertEquals(1, contract.size());
    }
    
    @TestSetup
    private static void testSetup() {
        List<Account> accs = TestFactoryPayment.addAccounts(1, true);
        List<Contact> conts = TestFactoryPayment.addContacts(accs, 5, true);

        Opportunity opp = new Opportunity(
                Name = 'Test Opportunity',
                AccountId = accs[0].Id,
                Primary_Contact__c = conts[0].Id,
                Tenant_2__c = conts[1].Id,
                Tenant_3__c = conts[2].Id,
                Tenant_4__c = conts[3].Id,
                Tenant_5__c = conts[4].Id,
                Type = '1 Bed',
                CloseDate = Date.newInstance(2018, 4, 7),
                StageName = 'Interested',
                Amount = 1000,
                Term_No_of_Months__c = 1
        );
        insert opp;

        Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);
        gas.Run_Process_Builder__c = false;
        update gas;
    }
}