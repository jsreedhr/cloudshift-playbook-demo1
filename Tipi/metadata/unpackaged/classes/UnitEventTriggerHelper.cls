/**
    * @Name:        UnitEventTriggerHelper
    * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 07/06/2018    Andrey Korobovich    Created Class
*/

public with sharing class UnitEventTriggerHelper {

    private static final String INSERT_ACTION = 'Insert';
    private static final String DELETE_ACTION = 'Delete';

    public static final Map<String, AggregationsInterface> strategies;
    private static final List<Aggregator_Setting__mdt> aggregatorsInsertAction;
    private static final List<Aggregator_Setting__mdt> aggregatorsDeleteAction;

    static {
        List<Aggregator_Setting__mdt> aggregators = getAggregatorsSettings();
        aggregatorsInsertAction = new List<Aggregator_Setting__mdt>();
        aggregatorsDeleteAction = new List<Aggregator_Setting__mdt>();
        for (Aggregator_Setting__mdt setting : aggregators) {
            if (setting.Action__c == DELETE_ACTION) {
                aggregatorsDeleteAction.add(setting);
            } else if (setting.Action__c == INSERT_ACTION) {
                aggregatorsInsertAction.add(setting);
            }
        }

        System.debug(JSON.serializePretty(aggregatorsDeleteAction));
        List<String> strategyNames = new List<String>();
        if (aggregators != null && !aggregators.isEmpty()) {
            for (Aggregator_Setting__mdt aggregator : aggregators) {
                if (aggregator.Label != null) {
                    strategyNames.add(aggregator.Label);
                }
            }
        }
        strategies = new Map<String, AggregationsInterface>();
        for (String name : strategyNames) {
            try {
                strategies.put(name, (AggregationsInterface)Type.forName(name).newInstance());
            } catch (Exception e) {
                continue;//skip bad name silently
            }
        }

        System.debug(JSON.serializePretty(strategies));
    }


    public static void processAfterInsert(List<sObject> objects) {
        List<Unit_Event__e> events = (List<Unit_Event__e>) objects;
        Set<Id> unitsToRemove = new Set<Id>();
        Set<Id> unitsToInsert = new Set<Id>();

        for(Unit_Event__e event: events){
            if(event.isDelete__c){
                unitsToRemove.add(event.RecordId__c);
            } else {
                unitsToInsert.add(event.RecordId__c);
            }
        }


        if(!unitsToRemove.isEmpty()) {
            List<Unit__c> units = getUnitsById(unitsToRemove);
            if(!units.isEmpty()){
                removeFromAggregator(units);
            }
        }
        if(!unitsToInsert.isEmpty()) {
            List<Unit__c> units = getUnitsById(unitsToInsert);
            if(!units.isEmpty()){
                sendToAggregator(units);
            }
        }
    }

    /**
    * Method to get all of aggregators and its settings
    * @returns List<Aggregator_Setting__mdt> list of active aggregators
    */
    private static List<Aggregator_Setting__mdt> getAggregatorsSettings() {
        return [
                   SELECT Id, Action__c, Label, Certificate__c, Method__c, URL__c, Content_Type__c, Network_Id__c, Channel__c, Branch_Id__c, Overseas__c
                   FROM Aggregator_Setting__mdt
                   WHERE Active__c = true
                   LIMIT 10000
               ];
    }

    private static List<Unit__c> getUnitsById(Set<Id> unitIds) {
        Set<String> fields = Unit__c.sObjectType.getDescribe().fields.getMap().keySet();
        List<Unit__c> unitToUpdate = (List<Unit__c>) new UnitSelector().getRecordsByIds(unitIds,fields);
        System.debug(JSON.serializePretty(unitToUpdate));
        return unitToUpdate;
    }

    
        /**
     * Method to call process request method
     * @param  newUnits  list of units which should be added to aggregators
     */
    private static void sendToAggregator(List<Unit__c> newUnits) {
        for (Aggregator_Setting__mdt aggregator : aggregatorsInsertAction) {
            if(!strategies.containsKey(aggregator.Label)) continue;
            AggregationsInterface strategy = strategies.get(aggregator.Label);
            strategy.processRequest(newUnits, aggregator);
        }
    }

    /**
     * Method to call processRemoveRequest method
     * @param  newUnits  list of units which should be remove from aggregators
     */
    private static void removeFromAggregator(List<Unit__c> newUnits) {
        for (Aggregator_Setting__mdt aggregator : aggregatorsDeleteAction) {
            System.debug('aggregator.Label >>>' + aggregator.Label);
            if(!strategies.containsKey(aggregator.Label)) continue;
            AggregationsInterface strategy = strategies.get(aggregator.Label);
            strategy.processRequest(newUnits, aggregator);
        }
    }

}