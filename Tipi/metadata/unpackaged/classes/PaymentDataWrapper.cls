/**
 * @Name:        PaymentDataWrapper
 * @Description: Wrapper Class PaymentDataWrapper holding the object/json structure to initialise the Payment Configurator
 *
 * @author:      Andrey Korobovich
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 18/05/2018    Andrey Korobovich    Created Class
 * 25/05/2018	 Patrick Fischer	  Added new constructor supporting contract dates
 */

public with sharing class PaymentDataWrapper {

	private static final String DATE_FORMAT = 'dd/MM/yyyy';

	@AuraEnabled public Boolean paymentConfigDone { get; set; }
	@AuraEnabled public Decimal rent { get; set; }
	@AuraEnabled public Decimal storageCount { get; set; }
	@AuraEnabled public String storageString { get; set; }
	@AuraEnabled public Decimal parking { get; set; }
	@AuraEnabled public Decimal depositPayment { get; set; }
	@AuraEnabled public List<Tenant> tenants { get; set; }
	@AuraEnabled public String errorMessage { get; set; }
	@AuraEnabled public List<String> paymentGateways { get; private set; }
	@AuraEnabled public Boolean isSavedSuccessfully { get; set; }
	@AuraEnabled public List<PicklistWrapper> storagePicklist { get; set; }
	@AuraEnabled public List<PicklistWrapper> parkingPicklist { get; set; }
	@AuraEnabled public String startDate { get; set; }
	@AuraEnabled public String endDate { get; set; }
	@AuraEnabled public Decimal firstRentPayment { get; set; }
	@AuraEnabled public String firstRentCalculation { get; set; }

	public PaymentDataWrapper(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public PaymentDataWrapper(Boolean paymentConfigDone, Decimal rent, Decimal storageCount, String storageString, Decimal parking,
			Decimal depositPayment, List<Tenant> tenants, String errorMessage,
			List<String> paymentGateways, List<PicklistWrapper> storagePicklist, List<PicklistWrapper> parkingPicklist,
			Date startDate, Date endDate, Decimal firstRentPayment) {
		this.paymentConfigDone = paymentConfigDone;
		this.rent = rent;
		this.storageCount = storageCount;
		this.storageString = storageString;
		this.parking = parking;
		this.depositPayment = depositPayment;
		this.tenants = tenants;
		this.errorMessage = errorMessage;
		this.paymentGateways = paymentGateways;
		this.storagePicklist = storagePicklist;
		this.parkingPicklist = parkingPicklist;
		this.startDate = Datetime.newInstance(startDate.year(), startDate.month(), startDate.day()).format(DATE_FORMAT);
		this.endDate = Datetime.newInstance(endDate.year(), endDate.month(), endDate.day()).format(DATE_FORMAT);
		this.firstRentPayment = firstRentPayment;
		this.firstRentCalculation = '';
	}

	public class PicklistWrapper {
		@AuraEnabled public String value { get; set; }
		@AuraEnabled public String label { get; set; }

		public PicklistWrapper(String value, String label) {
			this.value = value;
			this.label = label;
		}
	}
}