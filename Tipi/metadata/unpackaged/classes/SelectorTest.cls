/**
    * @Name:        SelectorTest
    * @Description: Test class for Selector virtual class
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 16/04/2018    Eugene Konstantinov    Created Class
*/
@isTest
public class SelectorTest {
    
    @isTest
    static void testAllMethods() {
        Selector slctr  = new Selector();
        String str1 = slctr.ConstructInClauseString(new Set<String>{'test1', 'test2'});
        System.assert(str1 != null);
        
        String str2 = slctr.ConstructFieldString(new Set<String>{'test1'});
        String str = slctr.ConstructFieldString(new List<String>{'test1'});
        System.assert(str2.contains('test1'));

        String str3 = slctr.ConstructInClauseString(new Set<Integer>{1, 2});
        System.assert(str3 != null);
        
        String str4 = slctr.ConstructInClauseString(new Set<Id>());
        System.assert(str4 != null);
        
        String str5 = slctr.ConstructFieldString(new List<Schema.FieldSetMember>());
        System.assert(str5 != null);  
    }

}