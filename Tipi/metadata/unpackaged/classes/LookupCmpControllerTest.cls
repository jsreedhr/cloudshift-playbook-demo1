/**
    * @Name:        LookupCmpControllerTest
    * @Description: Test class for LookupCmpController
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 20/04/2018    Eugene Konstantinov    Created Class
*/
@isTest
private class LookupCmpControllerTest {

	@isTest
	static void lookupTest() {
		Test.startTest();
			System.assertEquals(0, LookupCmpController.lookup('Tester', 'User', 'Community', new List<String>()).size());
		Test.stopTest();
	}

	@isTest
	static void lookupByIdTest() {
		Test.startTest();
			System.assertEquals(UserInfo.getUserId(), LookupCmpController.lookupById(UserInfo.getUserId(), 'User').SObjectId);
		Test.stopTest();
	}

}