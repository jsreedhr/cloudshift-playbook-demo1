/**
    * @Name:        NewViewingControllerTest
    * @Description: Test class for NewViewingController
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 02/04/2018    Eugene Konstantinov    Created Class
*/
@isTest
private class NewViewingControllerTest {
    @isTest static void getUnitsTest() {
    	String furniture = 'Furnished';
        String[] bedrooms = new String[]{'1 bed', '2 bed'};
        String[] emptyArray = new String[]{};
        String minPrice = '700';
        String maxPrice = '1200';
        String dateAvailable = '2019-04-13';
        Boolean parking = true;
        Boolean storage = true;
        Map<String, List<sObject>> mapOfLists = TestDataFactoryForNewViewingController.getTestUnits();
        List<Rentable_Item__c> items = mapOfLists.get('rentItem');
        Test.startTest();
        List<Unit__c> allUnits = NewViewingController.getUnits('', emptyArray, emptyArray, '', '', false, false, false, false, false, false, dateAvailable);
        List<Unit__c> unitsFurnished = NewViewingController.getUnits(furniture, emptyArray, emptyArray, '', '', false, false, false, false, false, false, dateAvailable);
        List<Unit__c> unitsBedrooms = NewViewingController.getUnits('', bedrooms, emptyArray, '', '', false, false, false, false, false, false, dateAvailable);
        List<Unit__c> unitsWithMinPrice = NewViewingController.getUnits('', emptyArray, emptyArray, minPrice, '', false, false, false, false, false, false, dateAvailable);
        List<Unit__c> unitsWithMaxPrice = NewViewingController.getUnits('', emptyArray, emptyArray, '', maxPrice, false, false, false, false, false, false, dateAvailable);
        List<Unit__c> unitsWithParking = NewViewingController.getUnits('', emptyArray, emptyArray, '', '', false, false, false, parking, false, false, dateAvailable);
        List<Unit__c> unitsWithStorage = NewViewingController.getUnits('', emptyArray, emptyArray, '', '', false, false, false, false, storage, false, dateAvailable);
        Test.stopTest();
        System.assert(allUnits.size() == 10);
        for (Unit__c unit : unitsFurnished) {
            System.assertEquals(furniture, unit.Furnished__c);
        }
        for (Unit__c unit : unitsBedrooms) {
            for (String bedroom : bedrooms) {
                if (bedroom == unit.Unit_Type__c) {
                    System.assert(true, 'Unit_Type__c = ' + bedroom);
                }
            }
        }
        for (Unit__c unit : unitsWithMinPrice) {
            System.assert(unit.Rent__c >= Decimal.valueOf(minPrice));
        }
        for (Unit__c unit : unitsWithMaxPrice) {
            System.assert(unit.Rent__c <= Decimal.valueOf(maxPrice));
        }
        for (Unit__c unit : unitsWithParking) {
            String name = 'Parking';
            for (Rentable_Item__c item : getRentItemByRecordName(items, name)) {
                if (unit.Building__c == item.Building__c && item.Name.contains('Parking')) {
                	System.assert(true);   
                }
            }
        }
        for (Unit__c unit : unitsWithStorage) {
            String name = 'Storage';
            for (Rentable_Item__c item : getRentItemByRecordName(items, name)) {
                if (unit.Building__c == item.Building__c) {
                    System.assert(item.Name.contains('Storage'));
                }
            }
        }
        Database.delete(mapOfLists.get('devSite'), false);
        Database.delete(mapOfLists.get('building'), false);
        Database.delete(mapOfLists.get('unit'), false);
        Database.delete(mapOfLists.get('rentItem'), false);                
    }
    
    @isTest static void saveNewViewingTest() {
        Map<String, sObject> mapOfSObjects = TestDataFactoryForNewViewingController.getMapOfSObjects();
        Map<String, List<sObject>> mapOfLists = TestDataFactoryForNewViewingController.getTestUnits();
        List<Unit__c> units = new List<Unit__c>();
        units.add((Unit__c)mapOfLists.get('unit').get(0));
        Test.startTest();
        Datetime reqDate = Datetime.newInstance(2018, 4, 7, 12, 0, 0); 
        Opportunity opp = NewViewingController.getContact(mapOfSObjects.get('opp').Id);
        NewViewingController.saveNewViewing(mapOfSObjects.get('opp').Id, reqDate, units, 'Attendee Info', UserInfo.getUserId());
        Test.stopTest();
        System.assertEquals(opp.ID, mapOfSObjects.get('opp').Id);
        Viewing__c newViewing = [
            SELECT 
            Id, 
            Account__c, 
            Attendee_Information__c, 
            Contact_Email__c, 
            Opportunity__c, 
            Status__c, 
            Tipi_Staff__c, 
            Requested_Start_Date__c 
            FROM Viewing__c 
            WHERE Opportunity__c =: mapOfSObjects.get('opp').Id];
        Viewing_Units__c newViewingUnits = [
            SELECT 
            Id, 
            Viewing__c, 
            Unit__c 
            FROM Viewing_Units__c 
            WHERE Viewing__c =: newViewing.Id];
        Opportunity oopp = (Opportunity)mapOfSObjects.get('opp');
        System.assertEquals(newViewing.Account__c, oopp.AccountId);
        System.assertEquals(newViewing.Attendee_Information__c, 'Attendee Info');
        System.assertEquals(newViewing.Opportunity__c, mapOfSObjects.get('opp').Id);
        System.assertEquals(newViewing.Status__c, 'Tipi Confirmed');
        System.assertEquals(newViewing.Requested_Start_Date__c, reqDate);
        System.assertEquals(newViewingUnits.Viewing__c, newViewing.Id);
        System.assertEquals(newViewingUnits.Unit__c, units.get(0).Id);
        Database.delete(mapOfSObjects.get('opp'), false);
        Database.delete(mapOfSObjects.get('contact'), false);
        Database.delete(mapOfSObjects.get('acc'), false);
        Database.delete(mapOfLists.get('devSite'), false);
        Database.delete(mapOfLists.get('building'), false);
        Database.delete(mapOfLists.get('unit'), false);
        Database.delete(mapOfLists.get('rentItem'), false);
    }   
    
    private static List<Rentable_Item__c> getRentItemByRecordName(List<Rentable_Item__c> items, String name) {
        for (Integer i = 0; i < items.size(); i++) {
            if (!items.get(i).Name.contains(name)) {
                items.remove(i);
            }
        }
        return items;
    }
}