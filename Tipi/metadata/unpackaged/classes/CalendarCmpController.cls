/**
    * @Name:        CalendarCmpController
    * @Description: Controller class for Calendar Lightning Component
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 23/04/2018    Eugene Konstantinov    Created Class
    * 20/06/2018    Eugene Konstantinov    Refactored Class
*/
public with sharing class CalendarCmpController {
    private static final String CLASS_NAME = 'CalendarCmpController';
    private static final String SocialSpace_sObjectName = Social_Space__c.SObjectType.getDescribe().getName();
    private static final String RentableItem_sObjectName = Rentable_Item__c.SObjectType.getDescribe().getName();

    /**
    * Method for getting List of Booked Rentable Items for Calendar Lightning Component
    * @param objectId string value of Rentable Item or Social Space Id
    * @param eventType string Api Name of Rentable Item or Social Space Object
    * @returns List<sObject> list of Booked Rentable Items for fullcalendar events
    */
    @AuraEnabled
    public static List<SObject> getEvents(String objectId, String eventType) {
        if (eventType == Rentable_Item__c.SObjectType.getDescribe().getName()) {
            return getRentableItemBookings(objectId);
        } else if (eventType == Social_Space__c.SObjectType.getDescribe().getName()) {
            return getSocialSpaceBookings(objectId);
        }
        return new List<SObject>();
    }

    /**
    * Method for Booked Rentable Item creation from Calendar Lightning Component
    * @param bookedItemId string value of Rentable Item or Social Space Id
    * @param currentContactSerialized serialized contact record
    * @param startTime string value of booking start time
    * @param endTime string value of booking end time
    * @param carNumber string value for guest parking bookings
    * @returns Booked_Rentable_Item__c new record
    */
    @AuraEnabled
    public static Booked_Rentable_Item__c createBooking(String bookedItemId, String currentContactSerialized, String startTime, String endTime, String carNumber) {
        Id recordTypeId = getRecordTypeForBookedRenatableItem(bookedItemId);
        Contact currentContact = (Contact)System.JSON.deserialize(currentContactSerialized, Contact.class);
        if (recordTypeId != null && currentContact != null && currentContact.Id != null) {
            Booked_Rentable_Item__c newBookingRecord = new Booked_Rentable_Item__c();
            newBookingRecord.RecordTypeId = recordTypeId;
            Id recordId = Id.valueOf(bookedItemId);
            String objectName = recordId.getSobjectType().getDescribe().getName();
            String templateName = '';
            if (objectName == SocialSpace_sObjectName) {
                newBookingRecord.Social_Space__c = recordId;
                DateTime bookingStart = (Datetime)JSON.deserialize('"' + startTime + '"', Datetime.class);
                newBookingRecord.Booking_Start__c = getGMTDateTime(bookingStart);
                DateTime bookingEnd = (Datetime)JSON.deserialize('"' + endTime + '"', Datetime.class);
                newBookingRecord.Booking_End__c = getGMTDateTime(bookingEnd);
                templateName = 'Community_Social_Space_Confirmation';
            } else if (objectName == RentableItem_sObjectName) {
                newBookingRecord.Rentable_Item__c = recordId;
                newBookingRecord.Start_Date__c = Date.valueOf(startTime);
                newBookingRecord.End_Date__c = Date.valueOf(endTime);
                templateName = 'Community_Guest_Parking_Confirmation';
            }
            newBookingRecord.Contact__c = currentContact.Id;
            newBookingRecord.Contract__c = currentContact.Current_Lease__c;
            newBookingRecord.Car_Number_Plate__c = carNumber;
            insert newBookingRecord;

            EmailHandler.sendEmailToContact(new Map<Contact, SObject> { currentContact => newBookingRecord }, templateName);

            return newBookingRecord;
        }
        return null;
    }

    /**
    * Method to get DateTime from calendar without timeZone of current User
    * @param timeValue DateTime
    * @returns DateTime in GMT
    */
    public static Datetime getGMTDateTime(Datetime timeValue) {
        Datetime convertedDateTime = timeValue;
        Integer offset = UserInfo.getTimeZone().getOffset(Datetime.now());
        convertedDateTime = convertedDateTime.addSeconds(-offset/1000);
        return convertedDateTime;
    }

    /**
    * Method to get Record Type for Booked_Rentable_Item__c depends on booked sObject Type
    * @param bookedItemId String value of booked Item Id
    * @returns Id Booked_Rentable_Item__c Record Type Id
    */
    private static Id getRecordTypeForBookedRenatableItem(String bookedItemId) {
        Id recordId;
        try {
            recordId = Id.valueOf(bookedItemId);
        } catch (Exception e) {
            CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'Exception:' + CLASS_NAME + e.getMessage());
            return null;
        }
        String objectName = recordId.getSobjectType().getDescribe().getName();
        String recordTypeAPIName;
        if (objectName == SocialSpace_sObjectName) {
            recordTypeAPIName = 'Social_Space';
        } else if (objectName == RentableItem_sObjectName) {
            recordTypeAPIName = 'Rentable_item';
        }
        List<RecordType> recordType = [
            SELECT Id
            FROM RecordType
            WHERE
                SobjectType = 'Booked_Rentable_Item__c' AND
                DeveloperName = :recordTypeAPIName
            LIMIT 1
        ];
        if (recordType != null && !recordType.isEmpty()) {
            return recordType[0].Id;
        }
        return null;
    }

    /**
    * Method to get all Social Space bookings for specific Social Space
    * @param objectId String value of Social Space Id
    * @returns List<sObject> List of Booked_Rentable_Item__c
    */
    private static List<SObject> getSocialSpaceBookings(String objectId) {
        List<SObject> eventList = new List<sObject>();
        eventList = new BookedRentableItemSelector().getRecordsBySocialSpaceId(Id.valueOf(objectId), new Set<String>{
            Booked_Rentable_Item__c.Name.getDescribe().getName(),
            Booked_Rentable_Item__c.Booking_End__c.getDescribe().getName(),
            Booked_Rentable_Item__c.Booking_Start__c.getDescribe().getName(),
            Booked_Rentable_Item__c.Contact__c.getDescribe().getName(),
            Booked_Rentable_Item__c.Social_Space__c.getDescribe().getName()
        });
        return eventList;
    }

    /**
    * Method to get Guest Parking bookings for specific Building
    * @param objectId String value of Rentable Item Id with Guest Parking Record Type
    * @returns List<sObject> List of Booked_Rentable_Item__c
    */
    private static List<sObject> getRentableItemBookings(String objectId) {
        List<sObject> eventList = new List<sObject>();
        eventList = new BookedRentableItemSelector().getGuestParkingRecordsByBuildingId(Id.valueOf(objectId), new Set<String>{
                Booked_Rentable_Item__c.Name.getDescribe().getName(),
                Booked_Rentable_Item__c.Start_Date__c.getDescribe().getName(),
                Booked_Rentable_Item__c.End_Date__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Contact__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Rentable_Item__c.getDescribe().getName(),
                'Rentable_Item__r.Name'
                });
        return eventList;
    }
}