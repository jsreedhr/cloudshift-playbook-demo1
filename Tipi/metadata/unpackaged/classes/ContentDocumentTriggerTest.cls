/**
    * @Name:        ContentDocumentTriggerTest
    * @Description: Test class for ContentDocumentTrigger and Helper
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 16/04/2018    Eugene Konstantinov    Created Class
*/
@isTest
public class ContentDocumentTriggerTest {
    
    @isTest
    static void testContentDocumentTrigger() {

        Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);
        Development__c devSite = (Development__c) TestFactory.createSObject(new Development__c(), true);
        Building__c building = (Building__c) TestFactory.createSObject(new Building__c(Development_Site__c = devSite.Id), true);
        Unit__c unit = (Unit__c) TestFactory.createSObject(new Unit__c(Building__c = building.Id), true);
        Unit_File__c unitFile = (Unit_File__c) TestFactory.createSObject(new Unit_File__c(Unit__c = unit.Id), true);

        Test.startTest();
        ContentVersion contentVersion = new ContentVersion(
                Title = 'Penguins',
                PathOnClient = 'Penguins.jpg',
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true
        );
        insert contentVersion;

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = unitFile.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        Test.stopTest();

        Unit_File__c updUnitFile = [select id, Latest_Image_Version_ID__c, Shared_Link__c from Unit_File__c where id = :unitFile.Id];
        System.assertNotEquals(null, updUnitFile.Latest_Image_Version_ID__c);
        System.assertNotEquals(null, updUnitFile.Shared_Link__c);
    }

}