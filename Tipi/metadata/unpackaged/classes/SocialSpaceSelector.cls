/**
    * @Name:        SocialSpaceSelector
    * @Description: Class for fetching data stored in salesforce.
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 25/04/2018    Eugene Konstantinov    Created Class
    * 24/04/2018    Eugene Konstantinov    Added getAllRecords method
    * 30/04/2018    Eugene Konstantinov    Added getRecordById method
*/
public class SocialSpaceSelector extends Selector{
    public static final String objectAPIName = Social_Space__c.SobjectType.getDescribe().getName();

    /**
        * Method to fetch data stored in salesforce
        * @param    Set<String>   fields is a set of Strings that determines which fields are queried of the Unit object
        * @returns  List<SObject> a list of Units objects that are treated and stored as a generic type
        **/
    public List<SObject> getAllRecords(Set<String> fields) {
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Cost__c != null AND Type__c !=\'Guest Parking\' LIMIT 10000';
        List<SObject> resultList = new List<SObject>();
        resultList = Database.query(query);
        return resultList;
    }

    /**
    * Method to fetch data stored in salesforce
    * @param    Id development record Id
    *           Set<String> fields is a set of Strings that determines which fields are queried of the Unit object
    * @returns  List<SObject> a list of Units objects that are treated and stored as a generic type
    **/
    public List<SObject> getRecordsByDevelopment(Id developmentId, Set<String> fields) {
        if (developmentId == null){return new List<SObject>();}
        Id devId = developmentId;
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Building__c IN (SELECT Id FROM Building__c WHERE Development_Site__c = :devId) AND Cost__c != null AND Type__c !=\'Guest Parking\' LIMIT 10000';
        return Database.query(query);
    }

    /**
    * Method to fetch data stored in salesforce
    * @param    Id selected Record Id
    *           Set<String> fields is a set of Strings that determines which fields are queried of the Unit object
    * @returns  List<SObject> a list of Units objects that are treated and stored as a generic type
    **/
    public List<SObject> getRecordById(Id selectedRecordId, Set<String> fields) {
        if (selectedRecordId == null){return new List<SObject>();}
        Id recordId = selectedRecordId;
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Id = :recordId LIMIT 1';
        return Database.query(query);
    }

}