/**
* @Name:        FeedItemTriggerHandler
* @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
*
* @author:      Patrick Fischer
* @version:     1.0
* Change Log
*
* Date          author              Change Description
* -----------------------------------------------------------------------------------
* 12/06/2018    Patrick Fischer     Created Class
*/
public with sharing class FeedItemTriggerHandler extends TriggerHandler {

    protected override void beforeInsert() {
        FeedItemTriggerHelper.processBeforeInsert((List<FeedItem>) Trigger.new);
    }

    protected override void beforeUpdate() {
        FeedItemTriggerHelper.processBeforeUpdate((List<FeedItem>) Trigger.new);
    }
}