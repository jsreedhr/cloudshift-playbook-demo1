/**
    * @Name:        ZooplaHttpResponseGenerator
    * @Description: Class for generating Response for ZooplaAsyncExecutionTest
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 08/05/2018    Andrey Korobovich    Created Class
*/
@isTest
global with sharing class ZooplaMockHttpResponseGenerator implements HttpCalloutMock {
	private static final String UPD_ENDPOINT = 'https://realtime-listings-api.webservices.zpg.co.uk/sandbox/v1/listing/update';
	private static final String REMOVE_ENDPOINT = 'https://realtime-listings-api.webservices.zpg.co.uk/sandbox/v1/listing/delete';


	global HTTPResponse respond(HTTPRequest req){
		System.assertEquals('POST', req.getMethod());
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		if(req.getEndpoint() == UPD_ENDPOINT){
			res.setBody('{"listing_etag": "q1w243r45t", "listing_reference": "34037248", "new_listing": false, "status": "OK", "url": "http://realtime-listings.webservices.zpg.co.uk/preview/listing/1524580825070415/34037248"}');
		} else if(req.getEndpoint() == REMOVE_ENDPOINT){
			res.setBody('{"listing_etag": "q1w243r45t", "listing_reference": "34037248", "new_listing": false, "status": "OK", "url": "http://realtime-listings.webservices.zpg.co.uk/preview/listing/1524580825070415/34037248"}');
		}
		res.setStatusCode(200);
		res.setStatus('OK');
		return res;
	}
}