/**
 * @Name:        OpportunityTriggerHelper
 * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author              Change Description
 * -----------------------------------------------------------------------------------
 * 06/06/2018    Patrick Fischer     Created Class
 * 13/06/2018    Patrick Fischer     Added setUnitId()
 * 18/06/2018    Patrick Fischer     Added updateChildContacts() to set the Leased/Reserved Unit & ContractId on all child Contacts
 */
public with sharing class OpportunityTriggerHelper {

    public static final String UNIT_STATUS = 'Unit';

    public static void processBeforeInsert(List<Opportunity> opportunities) {
        opportunities = setPricebook2Id(opportunities);
        opportunities = setUnitId(opportunities, null);
    }

    public static void processAfterInsert(List<Opportunity> opportunities) {
        Map<Id, Contact> contactsToUpdate = updateChildContacts(opportunities, null);

        if(!contactsToUpdate.isEmpty()) {
            update contactsToUpdate.values();
        }
    }

    public static void processBeforeUpdate(List<Opportunity> opportunities, Map<Id, Opportunity> oldMap) {
        opportunities = setUnitId(opportunities, oldMap);
    }

    public static void processAfterUpdate(List<Opportunity> opportunities, Map<Id, Opportunity> oldMap) {
        Map<Id, Contact> contactsToUpdate = updateChildContacts(opportunities, oldMap);

        if(!contactsToUpdate.isEmpty()) {
            update contactsToUpdate.values();
        }
    }

    /**
     * To set the Pricebook Id on new/inserted opportunities
     *
     * @param opportunities List of Opportunity records to set the Pricebook2Id on
     *
     * @return List of Opportunity records with set Pricebook2Id
     */
    private static List<Opportunity> setPricebook2Id(List<Opportunity> opportunities) {
        List<Pricebook2> pricebooks = [SELECT Id FROM Pricebook2 WHERE IsStandard = TRUE LIMIT 1];

        if(!pricebooks.isEmpty()) {
            for (Opportunity opportunity : opportunities) {
                opportunity.Pricebook2Id = pricebooks[0].Id;
            }
        }

        return opportunities;
    }

    /**
     * To set the Unit Id on inserted/updated opportunities
     *
     * @param opportunities List of Opportunity records on which to set Leased_Reserved_Unit__c
     * @param oldMap Map of Opportunity records by Id of Trigger.oldMap context
     *
     * @return List of Opportunity records with set Leased_Reserved_Unit__c
     */
    @TestVisible
    private static List<Opportunity> setUnitId(List<Opportunity> opportunities, Map<Id, Opportunity> oldMap) {
        Map<Id, Id> unitIdsByPrimaryQuoteId = new Map<Id, Id>();

        for (Opportunity opportunity : opportunities) {
            if(opportunity.SBQQ__PrimaryQuote__c != null) {
                // Opportunity is new
                if (oldMap == null) {
                    unitIdsByPrimaryQuoteId.put(opportunity.SBQQ__PrimaryQuote__c, null);
                }

                // Primary Quote has changed
                else if (opportunity.Leased_Reserved_Unit__c == null ||
                        opportunity.SBQQ__PrimaryQuote__c != oldMap.get(opportunity.Id).SBQQ__PrimaryQuote__c) {
                    unitIdsByPrimaryQuoteId.put(opportunity.SBQQ__PrimaryQuote__c, null);
                }
            }
        }

        if(!unitIdsByPrimaryQuoteId.isEmpty()) {
            for (SBQQ__QuoteLine__c quoteLine : [
                    SELECT Id, SBQQ__Quote__c, SBQQ__Product__r.Related_Apartment__c
                    FROM SBQQ__QuoteLine__c
                    WHERE SBQQ__Quote__c IN :unitIdsByPrimaryQuoteId.keySet()
                    AND SBQQ__Product__r.Product_Type__c = :UNIT_STATUS
                    ORDER BY SBQQ__Quote__c
            ]) {
                unitIdsByPrimaryQuoteId.put(quoteLine.SBQQ__Quote__c, quoteLine.SBQQ__Product__r.Related_Apartment__c);
            }

            for (Opportunity opportunity : opportunities) {
                if(unitIdsByPrimaryQuoteId.containsKey(opportunity.SBQQ__PrimaryQuote__c)) {
                    Id unitId = unitIdsByPrimaryQuoteId.get(opportunity.SBQQ__PrimaryQuote__c);
                    if (unitId != null) {
                        opportunity.Leased_Reserved_Unit__c = unitId;
                    }
                }
            }
        }

        return opportunities;
    }

    /**
     * To set the Leased/Reserved Unit & ContractId on all child Contacts of Opportunities
     *
     * @param opportunities List of Opportunity records of Trigger.new context
     * @param oldMap Map of Opportunity records of Trigger.oldMap context
     *
     * @return Map of Contact records by Contact Id holding records where either Unit or ContractId have been set
     */
    private static Map<Id, Contact> updateChildContacts(List<Opportunity> opportunities, Map<Id, Opportunity> oldMap) {
        Map<Id, Contact> contactMapForUpd = new Map<Id, Contact>();
        Set<Id> opportunityIdsChanged = getIdsOfChangedOpportunities(opportunities, oldMap);
        List<Opportunity> relatedOpportunityContacts = getRelatedOpportunityContacts(opportunityIdsChanged);

        for(Opportunity opportunity : relatedOpportunityContacts) {
            // update Unit / Lease on Primary Contact
            if (opportunity.Primary_Contact__c != null) {
                if (opportunity.Primary_Contact__r.Current_Lease__c != opportunity.ContractId ||
                        opportunity.Primary_Contact__r.Leased_Reserved_Unit__c != opportunity.Leased_Reserved_Unit__c) {
                    opportunity.Primary_Contact__r.Leased_Reserved_Unit__c = opportunity.Leased_Reserved_Unit__c;
                    opportunity.Primary_Contact__r.Current_Lease__c = opportunity.ContractId;
                    contactMapForUpd.put(opportunity.Primary_Contact__c, opportunity.Primary_Contact__r);
                }
            }

            // update Unit / Lease on Tenant 2
            if (opportunity.Tenant_2__c != null) {
                if (opportunity.Tenant_2__r.Current_Lease__c != opportunity.ContractId ||
                        opportunity.Tenant_2__r.Leased_Reserved_Unit__c != opportunity.Leased_Reserved_Unit__c) {
                    opportunity.Tenant_2__r.Leased_Reserved_Unit__c = opportunity.Leased_Reserved_Unit__c;
                    opportunity.Tenant_2__r.Current_Lease__c = opportunity.ContractId;
                    contactMapForUpd.put(opportunity.Tenant_2__c, opportunity.Tenant_2__r);
                }
            }

            // update Unit / Lease on Tenant 3
            if (opportunity.Tenant_3__c != null) {
                if (opportunity.Tenant_3__r.Current_Lease__c != opportunity.ContractId ||
                        opportunity.Tenant_3__r.Leased_Reserved_Unit__c != opportunity.Leased_Reserved_Unit__c) {
                    opportunity.Tenant_3__r.Leased_Reserved_Unit__c = opportunity.Leased_Reserved_Unit__c;
                    opportunity.Tenant_3__r.Current_Lease__c = opportunity.ContractId;
                    contactMapForUpd.put(opportunity.Tenant_3__c, opportunity.Tenant_3__r);
                }
            }

            // update Unit / Lease on Tenant 4
            if (opportunity.Tenant_4__c != null) {
                if (opportunity.Tenant_4__r.Current_Lease__c != opportunity.ContractId ||
                        opportunity.Tenant_4__r.Leased_Reserved_Unit__c != opportunity.Leased_Reserved_Unit__c) {
                    opportunity.Tenant_4__r.Leased_Reserved_Unit__c = opportunity.Leased_Reserved_Unit__c;
                    opportunity.Tenant_4__r.Current_Lease__c = opportunity.ContractId;
                    contactMapForUpd.put(opportunity.Tenant_4__c, opportunity.Tenant_4__r);
                }
            }

            // update Unit / Lease on Tenant 5
            if (opportunity.Tenant_5__c != null) {
                if (opportunity.Tenant_5__r.Current_Lease__c != opportunity.ContractId ||
                        opportunity.Tenant_5__r.Leased_Reserved_Unit__c != opportunity.Leased_Reserved_Unit__c) {
                    opportunity.Tenant_5__r.Leased_Reserved_Unit__c = opportunity.Leased_Reserved_Unit__c;
                    opportunity.Tenant_5__r.Current_Lease__c = opportunity.ContractId;
                    contactMapForUpd.put(opportunity.Tenant_5__c, opportunity.Tenant_5__r);
                }
            }
        }

        return contactMapForUpd;
    }

    /**
     * To get a Set of Opportunity Ids where the Leased/Reserved Unit or ContractId has changed or the Opportunity is new
     *
     * @param opportunities List of Opportunity records of Trigger.new context
     * @param oldMap Map of Opportunity records of Trigger.oldMap context
     *
     * @return Set of Opportunity Ids where the Leased/Reserved Unit or ContractId has changed or the record is new
     */
    private static Set<Id> getIdsOfChangedOpportunities(List<Opportunity> opportunities, Map<Id, Opportunity> oldMap) {
        Set<Id> opportunityIdsChanged = new Set<Id>();

        for(Opportunity opportunity : opportunities) {
            if (oldMap == null) {
                opportunityIdsChanged.add(opportunity.Id);
            } else {
                if(opportunity.Leased_Reserved_Unit__c != oldMap.get(opportunity.Id).Leased_Reserved_Unit__c ||
                        opportunity.ContractId != oldMap.get(opportunity.Id).ContractId) {
                    opportunityIdsChanged.add(opportunity.Id);
                }
            }
        }

        return opportunityIdsChanged;
    }

    /**
     * To query related Tenant details for Opportunity records
     *
     * @param opportunityIds Set of Opportunity Ids to query for
     *
     * @return List of Opportunity records including related Tenant details
     */
    private static List<Opportunity> getRelatedOpportunityContacts(Set<Id> opportunityIds) {
        return [
                SELECT Id, Leased_Reserved_Unit__c, ContractId,
                        Primary_Contact__c, Primary_Contact__r.Leased_Reserved_Unit__c, Primary_Contact__r.Current_Lease__c,
                        Tenant_2__c, Tenant_2__r.Leased_Reserved_Unit__c, Tenant_2__r.Current_Lease__c,
                        Tenant_3__c, Tenant_3__r.Leased_Reserved_Unit__c, Tenant_3__r.Current_Lease__c,
                        Tenant_4__c, Tenant_4__r.Leased_Reserved_Unit__c, Tenant_4__r.Current_Lease__c,
                        Tenant_5__c, Tenant_5__r.Leased_Reserved_Unit__c, Tenant_5__r.Current_Lease__c
                FROM Opportunity
                WHERE Id IN: opportunityIds
        ];
    }

}