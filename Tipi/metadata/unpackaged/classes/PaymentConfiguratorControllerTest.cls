/**
 * @Name:        PaymentConfiguratorControllerTest
 * @Description: Test class for PaymentConfiguratorController
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 23/05/2018    Patrick Fischer    Created Class
 */
@IsTest
public without sharing class PaymentConfiguratorControllerTest {

    /**
     * Method tested: initialiseComponent()
     * Expected result: initialises LEX component successfully populating all fields
     */
    private static testMethod void initialiseComponentSuccess() {
        Contract contract = [SELECT Id, SBQQ__Opportunity__r.Primary_Contact__c,
                Unit__r.Building__r.Development_Site__c, SBQQ__Opportunity__c FROM Contract LIMIT 1];
        // Create reservation Payment
        Tipi_Payments__c reservationPayment = new Tipi_Payments__c(
                Amount__c = 400,
                Contact__c = contract.SBQQ__Opportunity__r.Primary_Contact__c,
                Development__c = contract.Unit__r.Building__r.Development_Site__c,
                Opportunity__c = contract.SBQQ__Opportunity__c,
                Type__c = Utils_Constants.PAYMENT_TYPE_RESERVATION
        );
        insert reservationPayment;

        Test.startTest();
        PaymentDataWrapper paymentDataWrapper = PaymentConfiguratorController.initialiseComponent(contract.Id);
        Test.stopTest();

        System.assertEquals(false, paymentDataWrapper.paymentConfigDone, 'Expecting Payment Configurator to be available');
        System.assertEquals('', paymentDataWrapper.errorMessage, 'Expecting Payment Configurator without error message');
        System.assertEquals(null, paymentDataWrapper.isSavedSuccessfully, 'Expecting Payment Configurator without invocation of save operation');
        System.assertEquals(10, paymentDataWrapper.tenants.size(), 'Expecting 5 tenants + 5 Guarantor to exist');
        System.assertEquals(3000, paymentDataWrapper.tenants[0].rent, 'Expecting Monthly rent of 3000');
        System.assertEquals(1000, paymentDataWrapper.depositPayment, 'Expecting an Deposit Payment of GBP 1000');
        System.assertEquals(4, paymentDataWrapper.storagePicklist.size(), 'Expecting 4 available storage items');
        System.assertEquals('S:1, M:1, L:1, XL:1', paymentDataWrapper.storageString, 'Expecting quote storage items to match');
    }

    /**
     * Method tested: initialiseComponent()
     * Expected result: initialises LEX component but blocks screen as Payment Configuration was done previously
     */
    private static testMethod void initialiseComponentBlocked() {
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        contract.Payment_Config_Done__c = true;
        update contract;

        Test.startTest();
        PaymentDataWrapper paymentDataWrapper = PaymentConfiguratorController.initialiseComponent(contract.Id);
        Test.stopTest();

        System.assertEquals(true, paymentDataWrapper.paymentConfigDone, 'Expecting Payment Configurator to be blocked');
        System.assertNotEquals('', paymentDataWrapper.errorMessage, 'Expecting Error Message to be thrown within Payment Configurator');
        System.assertEquals(null, paymentDataWrapper.isSavedSuccessfully, 'Expecting Payment Configurator without invocation of save operation');
    }

    /**
     * Method tested: initialiseComponent()
     * Expected result: initialises LEX component but blocks screen as Payment Configuration was done previously
     */
    private static testMethod void initialiseComponentTypeNotSpecified() {
        Contract contract = [SELECT Id, Type__c FROM Contract LIMIT 1];
        contract.Type__c = null;
        update contract;

        Test.startTest();
        PaymentDataWrapper paymentDataWrapper = PaymentConfiguratorController.initialiseComponent(contract.Id);
        Test.stopTest();

        System.assert(paymentDataWrapper.errorMessage.startsWith('Please specify'), 'Expecting an Error Message to be thrown as Type was not specified');
        System.assertEquals(null, paymentDataWrapper.isSavedSuccessfully, 'Expecting Payment Configurator without invocation of save operation');
    }

    /**
     * Method tested: initialiseComponent()
     * Expected result: Exception thrown during LEX component initialisation
     */
    private static testMethod void initialiseComponentFailure() {
        Test.startTest();
        PaymentDataWrapper paymentDataWrapper = PaymentConfiguratorController.initialiseComponent(null);
        Test.stopTest();

        System.assertEquals(null, paymentDataWrapper.paymentConfigDone, 'Expecting Payment Configurator to be blocked');
        System.assert(paymentDataWrapper.errorMessage.startsWith('Initialisation Error'), 'Expecting Error Message to be thrown within Payment Configurator');
        System.assertEquals(null, paymentDataWrapper.isSavedSuccessfully, 'Expecting Payment Configurator without invocation of save operation');
    }

    /**
     * Method tested: savePaymentConfiguration()
     * Expected result: saves LEX component successfully creating Tipi_Payment__c & Booked_Rentable_Item__c records
     */
    private static testMethod void savePaymentConfigurationSuccess() {
        // Initialising LEX Component
        Contract contract = [SELECT Id FROM Contract LIMIT 1];
        PaymentDataWrapper paymentDataWrapper = PaymentConfiguratorController.initialiseComponent(contract.Id);
        System.assertEquals(false, paymentDataWrapper.paymentConfigDone, 'Expecting Payment Configurator to be available');
        System.assertEquals('', paymentDataWrapper.errorMessage, 'Expecting Payment Configurator without error message');
        System.assertEquals(null, paymentDataWrapper.isSavedSuccessfully, 'Expecting Payment Configurator without invocation of save operation');
        System.assertEquals(10, paymentDataWrapper.tenants.size(), 'Expecting 5 tenants + 5 Guarantor to exist');

        // Payment Configuration within LEX Component
        paymentDataWrapper.depositPayment = 5000;
        paymentDataWrapper.tenants[0].depositPayment = 5000;
        paymentDataWrapper.tenants[0].monthsUpfront = 3;
        paymentDataWrapper.tenants[0].rentUpfront = paymentDataWrapper.tenants[0].monthsUpfront * paymentDataWrapper.rent;
        paymentDataWrapper.tenants[0].upfrontPaymentType = 'SagePay';
        List<Rentable_Item__c> rentableItems = [SELECT Id FROM Rentable_Item__c];
        paymentDataWrapper.tenants[0].storageString = rentableItems[0].Id + ';' + rentableItems[1].Id;

        // Save Operation within LEX Component
        Test.startTest();
        PaymentDataWrapper paymentDataWrapperSaved = PaymentConfiguratorController.savePaymentConfiguration(JSON.serialize(paymentDataWrapper), contract.Id);
        Test.stopTest();

        System.assertEquals(false, paymentDataWrapperSaved.paymentConfigDone, 'Expecting Payment Configurator to be blocked');
        System.assertEquals('', paymentDataWrapperSaved.errorMessage, 'Expecting Payment Configurator without error message');
        System.assertEquals(true, paymentDataWrapperSaved.isSavedSuccessfully, 'Expecting Payment Configurator without invocation of save operation');
        List<Tipi_Payments__c> tipiPayments = [SELECT Id FROM Tipi_Payments__c];
        System.assertEquals(15, tipiPayments.size(), 'Expecting 15 Tipi Payment Records to be created. ' +
                'Tenant 1: 13 payments + 1 Deposit + 1 Upfront');
        List<Booked_Rentable_Item__c> bookedRentableItems = [SELECT Id FROM Booked_Rentable_Item__c];
        System.assertEquals(2, bookedRentableItems.size(), 'Expecting 2 Booked Rentable Items to be created');
        contract = [SELECT Id, Payment_Config_Done__c FROM Contract LIMIT 1];
        System.assertEquals(true, contract.Payment_Config_Done__c, 'Expecting Payment Configuration to be completed and locked');
    }

    /**
     * Method tested: savePaymentConfiguration()
     * Expected result: saves LEX component successfully creating Tipi_Payment__c & Booked_Rentable_Item__c records
     */
    private static testMethod void savePaymentConfigurationSuccessMultiTenants() {
        // Initialising LEX Component
        Contract contract = [SELECT Id, StartDate FROM Contract LIMIT 1];
        contract.StartDate = contract.StartDate.addDays(10);
        update contract;
        PaymentDataWrapper paymentDataWrapper = PaymentConfiguratorController.initialiseComponent(contract.Id);
        System.assertEquals(false, paymentDataWrapper.paymentConfigDone, 'Expecting Payment Configurator to be available');
        System.assertEquals('', paymentDataWrapper.errorMessage, 'Expecting Payment Configurator without error message');
        System.assertEquals(null, paymentDataWrapper.isSavedSuccessfully, 'Expecting Payment Configurator without invocation of save operation');
        System.assertEquals(10, paymentDataWrapper.tenants.size(), 'Expecting 5 tenants + 5 Guarantor to exist');

        // Payment Configuration within LEX Component
        paymentDataWrapper.depositPayment = 3000;
        paymentDataWrapper.tenants[0].depositPayment = 3000;
        paymentDataWrapper.tenants[0].monthsUpfront = 3;
        paymentDataWrapper.tenants[0].rentUpfront = paymentDataWrapper.tenants[0].monthsUpfront * paymentDataWrapper.rent;
        paymentDataWrapper.tenants[0].rent = 2500;
        paymentDataWrapper.tenants[0].upfrontPaymentType = 'SagePay';
        List<Rentable_Item__c> rentableItems = [SELECT Id FROM Rentable_Item__c];
        paymentDataWrapper.tenants[0].storageString = rentableItems[0].Id + ';' + rentableItems[1].Id;
        paymentDataWrapper.tenants[1].monthsUpfront = 1;
        paymentDataWrapper.tenants[1].rentUpfront = paymentDataWrapper.tenants[1].monthsUpfront * paymentDataWrapper.rent;
        paymentDataWrapper.tenants[1].isPayee = true;
        paymentDataWrapper.tenants[1].rent = 500;

        // Save Operation within LEX Component
        Test.startTest();
        PaymentDataWrapper paymentDataWrapperSaved = PaymentConfiguratorController.savePaymentConfiguration(JSON.serialize(paymentDataWrapper), contract.Id);
        Test.stopTest();

        System.assertEquals('', paymentDataWrapperSaved.errorMessage, 'Expecting Payment Configurator without error message');
        System.assertEquals(true, paymentDataWrapperSaved.isSavedSuccessfully, 'Expecting Payment Configurator without invocation of save operation');
            List<Tipi_Payments__c> tipiPayments = [SELECT Id FROM Tipi_Payments__c];
        System.assertEquals(27, tipiPayments.size(), 'Expecting 27 Tipi Payment Records to be created. ' +
                'Tenant 1: 12 payments + 1 Deposit + 1 Upfront' +
                'Tenant 2: 12 payments + 0 Deposit + 1 Upfront');
        List<Booked_Rentable_Item__c> bookedRentableItems = [SELECT Id FROM Booked_Rentable_Item__c];
        System.assertEquals(2, bookedRentableItems.size(), 'Expecting 2 Booked Rentable Items to be created');
        contract = [SELECT Id, Payment_Config_Done__c FROM Contract LIMIT 1];
        System.assertEquals(true, contract.Payment_Config_Done__c, 'Expecting Payment Configuration to be completed and locked');
    }

    /**
     * Method tested: savePaymentConfiguration()
     * Expected result: Exception thrown and handled
     */
    private static testMethod void savePaymentConfigurationFailure() {
        // Initialising LEX Component
        Contract contract = [SELECT Id, StartDate FROM Contract LIMIT 1];

        // Save Operation within LEX Component
        Test.startTest();
        PaymentDataWrapper paymentDataWrapperSaved = PaymentConfiguratorController.savePaymentConfiguration(null, contract.Id);
        Test.stopTest();

        System.assertEquals(null, paymentDataWrapperSaved.paymentConfigDone, 'Expecting Payment Configurator to be blocked');
        System.assert(paymentDataWrapperSaved.errorMessage.startsWith('Saving Error'), 'Expecting Payment Configurator to throw error message');
        System.assertEquals(false, paymentDataWrapperSaved.isSavedSuccessfully, 'Expecting Payment Configurator without invocation of save operation');
        List<Tipi_Payments__c> tipiPayments = [SELECT Id FROM Tipi_Payments__c];
        System.assertEquals(0, tipiPayments.size(), 'Expecting 0 Tipi Payment Records to be created.');
        List<Booked_Rentable_Item__c> bookedRentableItems = [SELECT Id FROM Booked_Rentable_Item__c];
        System.assertEquals(0, bookedRentableItems.size(), 'Expecting 0 Booked Rentable Items to be created');
    }

    /**
     * Method tested: insertPaymentConfiguration()
     * Expected result: Exception thrown, handled, and insertion rolledBack
     */
    private static testMethod void insertPaymentConfigurationFailure() {
        Test.startTest();
        String errorMsg = PaymentConfiguratorController.insertPaymentConfiguration(null, null, null);
        Test.stopTest();

        System.assert(errorMsg != '', 'Expecting failed insertion. Exception thrown and handled.'); 
    }

    /**
     * Method tested: getOpportunityById()
     * Expected result: Exception thrown and handled
     */
    private static testMethod void getOpportunityByIdFailure() {
        Test.startTest();
        Opportunity opportunity = PaymentConfiguratorController.getOpportunityById(null);
        Test.stopTest();

        System.assertEquals(null, opportunity, 'Expecting failed query. Exception handled and null returned.');
    }

    /**
     * Helper method to setup testdata including Developments, Buildings, Units, Accounts, Opportunities, Products, Quotes
     */
    @TestSetup
    private static void testSetup() {
        // Testdata Items
        List<Development__c> developments = TestFactoryPayment.getDevelopments(1, true);
        List<Building__c> buildings = TestFactoryPayment.getBuildings(developments[0], 1, true);
        List<Unit__c> units = TestFactoryPayment.getUnits(buildings[0], 1, true);
        List<Pricebook2> pricebooks = TestFactoryPayment.getPricebooks(1, true);
        List<Product2> products = new List<Product2>();
        products.addAll(TestFactoryPayment.getProducts('Unit', 3000, 1, false));
        products.addAll(TestFactoryPayment.getProducts('Storage', 200, 4, false));
        products.addAll(TestFactoryPayment.getProducts('Parking', 400, 2, false));
        insert products;
        List<PricebookEntry> pricebookEntries = TestFactoryPayment.getPricebookEntries(products, true);
        List<Rentable_Item__c> rentableItems = TestFactoryPayment.getRentableItems(products, buildings[0], true);

        // Transactional Items
        List<Account> accounts = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 11, true);
        List<Opportunity> opportunities = TestFactoryPayment.getOpportunities(accounts[0], contacts, 1, true);
        List<SBQQ__Quote__c> quotes = TestFactoryPayment.getQuotes(accounts[0], contacts[0], opportunities[0], 1, true);
        List<SBQQ__QuoteLine__c> quoteLines = TestFactoryPayment.getQuoteLines(quotes[0], products, true);
        List<Contract> contracts = TestFactoryPayment.getContracts(accounts[0], quotes[0], opportunities[0], units[0], 1, true);
        List<asp04__Authorisation__c> authorisations = TestFactoryPayment.getAuthorisations(contacts, true);
    }
}