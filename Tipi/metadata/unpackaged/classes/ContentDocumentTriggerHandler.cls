/**
    * @Name:        ContentDocumentTriggerHandler
    * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 09/04/2018    Eugene Konstantinov    Created Class
*/
public with sharing class ContentDocumentTriggerHandler extends TriggerHandler{

	protected override void afterInsert(){
		ContentDocumentTriggerHelper.setSharedLink(Trigger.new);
	}

	protected override void afterUpdate(){
		ContentDocumentTriggerHelper.setSharedLink(Trigger.new);
	}

}