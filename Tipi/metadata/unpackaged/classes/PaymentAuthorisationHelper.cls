/**
 * @Name:        PaymentAuthorisationHelper
 * @Description: Helper class for Payment Functionality
 *
 * @author:      Eugene Konstantinov
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 08/05/2018    Eugene Konstantinov    Created Class
 * 01/06/2018    Eugene Konstantinov    Added processAfterUpdateAuthorisation methods
 * 14/06/2018    Eugene Konstantinov    Added Billing Address for Asperato Payment
 * 15/06/2018    Eugene Konstantinov    Refactored class, updated updateUnitStatusAfterReservationPayment method
 * 30/07/2018    Patrick Fischer        Removed processAfterUpdateAuthorisation method as handled in AuthorisationTriggerHelper class
 */
public with sharing class PaymentAuthorisationHelper {
    private static final String CLASS_NAME = 'PaymentAuthorisationHelper';

    public static void createAndSendReservationPayment(String contactId, String opportunityId) {
        Contact currentContact = getCurrentContactById(Id.valueOf(contactId));
        Opportunity currentOpportunity = getOpportunityById(Id.valueOf(opportunityId));
        if (currentContact != null && currentOpportunity != null) {
            Map<Contact, asp04__Payment__c> paymentByContact = createReservationPayments(currentContact, currentOpportunity);
            EmailHandler.sendEmailToContact(paymentByContact, 'Reservation_Payment_URL');
            updateUnitStatusAfterReservationPayment(currentOpportunity);
        }
    }

    private static void updateUnitStatusAfterReservationPayment(Opportunity opportunityRecord) {
        Unit__c unitToUpdate;

        if (opportunityRecord.Leased_Reserved_Unit__c != null) {
            opportunityRecord.Leased_Reserved_Unit__r.Status__c = 'Awaiting Reservation Payment';
            unitToUpdate = opportunityRecord.Leased_Reserved_Unit__r;
        }

        if (unitToUpdate != null) {
            update unitToUpdate;
        }
    }

    private static Map<Contact, asp04__Payment__c> createReservationPayments(Contact contactRecord, Opportunity opportunityRecord) {
        Map<Contact, asp04__Payment__c> paymentByContact = new Map<Contact, asp04__Payment__c>();
        paymentByContact.put(contactRecord, createPaymentRecord(contactRecord, opportunityRecord));
        try {
            insert paymentByContact.values();
        } catch (DmlException e) {
            CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'DML Exception:' + CLASS_NAME + e.getMessage());
            CustomLogger.save();
        }

        Map<Contact, Tipi_Payments__c> tipiPaymentsByContact = new Map<Contact, Tipi_Payments__c>();
        tipiPaymentsByContact.put(contactRecord, createReservationTIPIPaymentRecord(contactRecord, opportunityRecord, paymentByContact.get(contactRecord)));
        try {
            insert tipiPaymentsByContact.values();
        } catch (DmlException e) {
            CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'DML Exception:' + CLASS_NAME + e.getMessage());
            CustomLogger.save();

            // setting Payment Records as empty, to handle no emails being send out subsequently
            paymentByContact = new Map<Contact, asp04__Payment__c>();
        }

        return paymentByContact;
    }

    private static Tipi_Payments__c createReservationTIPIPaymentRecord(Contact currentContact, Opportunity opportunityRecord, asp04__Payment__c paymentRecord) {
        Tipi_Payments__c newPayments = new Tipi_Payments__c();
        newPayments.Contact__c = currentContact.Id;
        newPayments.Building__c = opportunityRecord.Leased_Reserved_Unit__r.Building__c;
        newPayments.Payment_Gateway__c = Utils_Constants.GATEWAY_SAGEPAY;
        newPayments.Credit_Payment_Type__c = Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT;
        newPayments.Unit__c = opportunityRecord.Leased_Reserved_Unit__c;
        newPayments.Opportunity__c = opportunityRecord.Id;
        newPayments.Payment__c = paymentRecord.Id;
        newPayments.Authorisation__c = paymentRecord.asp04__Authorisation__c;
        newPayments.Amount__c = paymentRecord.asp04__Amount__c;
        newPayments.Development__c = opportunityRecord.Leased_Reserved_Unit__r.Building__r.Development_Site__c;
        newPayments.Description__c = 'Reservation Payment';
        newPayments.Status__c = Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION;
        newPayments.Type__c = Utils_Constants.PAYMENT_TYPE_RESERVATION;
        newPayments.Payment_Date__c = paymentRecord.asp04__Payment_Date__c;
        newPayments.Calculation_Long__c = '[Charge for Reservation Payment ' + paymentRecord.asp04__Amount__c + ']';
        return newPayments;
    }

    private static asp04__Payment__c createPaymentRecord(Contact contactRecord, Opportunity opportunityRecord) {
        asp04__Payment__c newPayment = createPaymentRecordWithoutAmount(contactRecord);
        newPayment.asp04__Amount__c = getReservationFee(opportunityRecord.Leased_Reserved_Unit__r.Building__c);
        newPayment.asp04__Payment_Stage__c = Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION;
        return newPayment;
    }

    public static asp04__Payment__c createPaymentRecordWithoutAmount(Contact contactRecord) {
        contactRecord = getCurrentContactById(contactRecord.Id);
        asp04__Payment__c newPayment = new asp04__Payment__c();
        newPayment.Contact__c = contactRecord.Id;
        newPayment.asp04__Account_Name__c = contactRecord.Account.Name;
        newPayment.asp04__Billing_Address_Street__c = contactRecord.MailingStreet;
        newPayment.asp04__Billing_Address_City__c = contactRecord.MailingCity;
        newPayment.asp04__Billing_Address_State__c = contactRecord.MailingState;
        newPayment.asp04__Billing_Address_PostalCode__c = contactRecord.MailingPostalCode;
        newPayment.asp04__Billing_Address_Country__c = contactRecord.MailingCountry;
        newPayment.asp04__Email__c = contactRecord.Email;
        newPayment.asp04__First_Name__c = contactRecord.FirstName;
        newPayment.asp04__Last_Name__c = contactRecord.LastName;
        newPayment.asp04__Payment_Route_Options__c = 'Card';
        newPayment.asp04__Payment_Route_Selected__c = 'Card';
        newPayment.asp04__Payment_Date__c = Date.today();
        return newPayment;
    }

    private static Decimal getReservationFee(Id buildingId) {
        List<Payment_Setting__mdt> paymentSettings = [
            SELECT Id, MasterLabel, DeveloperName, Building_Id__c, Reservation_Fee__c
            FROM Payment_Setting__mdt
            WHERE
                Building_Id__c = :buildingId
            LIMIT 1
        ];
        if (paymentSettings != null && !paymentSettings.isEmpty()) {
            return paymentSettings[0].Reservation_Fee__c;
        } else {
            List<Payment_Setting__mdt> defaultPaymentSettings = [
                SELECT Id, MasterLabel, DeveloperName, Building_Id__c, Reservation_Fee__c
                FROM Payment_Setting__mdt
                WHERE
                    MasterLabel = 'Default'
                LIMIT 1
            ];
            if (defaultPaymentSettings != null && !defaultPaymentSettings.isEmpty()) {
                return defaultPaymentSettings[0].Reservation_Fee__c;
            }
        }
        return 0;
    }

    public static Contact getCurrentContactById(Id contactId) {
        List<Contact> contacts = (List<Contact>)new ContactSelector().getRecordsById(contactId, new Set<String>{
            Contact.Name.getDescribe().getName(),
            Contact.FirstName.getDescribe().getName(),
            Contact.LastName.getDescribe().getName(),
            Contact.Email.getDescribe().getName(),
            Contact.Current_Lease__c.getDescribe().getName(),
            Contact.Leased_Reserved_Unit__c.getDescribe().getName(),
            Contact.MailingCity.getDescribe().getName(),
            Contact.MailingState.getDescribe().getName(),
            Contact.MailingStreet.getDescribe().getName(),
            Contact.MailingCountry.getDescribe().getName(),
            Contact.MailingPostalCode.getDescribe().getName(),
            'Account.Name',
            'Leased_Reserved_Unit__r.Status__c',
            'Leased_Reserved_Unit__r.Rent__c',
            'Leased_Reserved_Unit__r.Building__c',
            'Leased_Reserved_Unit__r.Building__r.Development_Site__c',
            'Current_Lease__r.SBQQ__Opportunity__c',
            'Leased_Reserved_Unit__r.Description__c'
        });
        if (contacts != null && !contacts.isEmpty()) {
            return contacts[0];
        }
        return null;
    }

    public static Opportunity getOpportunityById(Id opportunityId) {
        List<Opportunity> opps = [
            SELECT Id, Name, Primary_Contact__c, Tenant_2__c, Tenant_3__c, Tenant_4__c, Tenant_5__c,
                Leased_Reserved_Unit__c, Leased_Reserved_Unit__r.Status__c, Leased_Reserved_Unit_Status__c, Leased_Reserved_Unit__r.Name,
                Leased_Reserved_Unit__r.Rent__c, Leased_Reserved_Unit__r.Building__c,
                Leased_Reserved_Unit__r.Building__r.Development_Site__c, Leased_Reserved_Unit__r.Description__c
            FROM Opportunity
            WHERE
                Id = :opportunityId
            LIMIT 1
        ];
        if (opps != null && !opps.isEmpty()) {
            return opps[0];
        }
        return null;
    }

    public static Map<Id, Tipi_Payments__c> getPaymentsForContactsInOpportunity(Id oppId) {
        List<Tipi_Payments__c> payments = [
            SELECT Id, Name, Contact__r.Id, Contact__r.Name, Payment__c, Payment__r.asp04__Payment_Stage__c, Opportunity__r.Name
            FROM Tipi_Payments__c
            WHERE
                Opportunity__c = :oppId AND
                RecordType.DeveloperName != 'Refund'
            ORDER BY CreatedDate ASC LIMIT 10000
        ];

        Map<Id, Tipi_Payments__c> paymentByContactId = new Map<Id, Tipi_Payments__c>();
        for (Tipi_Payments__c tipiPayment : payments) {
            paymentByContactId.put(tipiPayment.Contact__c, tipiPayment);
        }
        return paymentByContactId;
    }
}