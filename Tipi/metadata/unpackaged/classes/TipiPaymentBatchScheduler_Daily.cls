/**
 * @Name:        TipiPaymentBatchScheduler_Daily
 * @Description: Aggregate all payments for each user that are for current month in Tipi Payments
 *               table and insert the corresponding rows into the Payment table (Asperato)
 *               -> runs today
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 04/07/2018    Patrick Fischer    Created Class
 */
public with sharing class TipiPaymentBatchScheduler_Daily implements Schedulable {

    public void execute(SchedulableContext sc) {
        Database.executeBatch(new TipiPaymentBatch(Date.today()), 10);
    }
}