/**
    * @Name:        RentableItemSelector
    * @Description: Class for fetching data stored in salesforce.
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 10/04/2018    Eugene Konstantinov    Created Class
    * 24/04/2018    Eugene Konstantinov    Added getAllRecords Method
    * 27/04/2018    Eugene Konstantinov    Added getRecordsByDevelopment Method
    * 30/04/2018    Eugene Konstantinov    Added getGuestParkingRecordById method
    * 11/06/2018    Eugene Konstantinov    Added getRecordById method
*/
public with sharing class RentableItemSelector extends Selector{
    public static final String objectAPIName = Rentable_Item__c.SobjectType.getDescribe().getName();

    /**
    * Method to fetch data stored in salesforce
    * @param    List<Building__c> list of Buildings
    *           Set<String> fields is a set of Strings that determines which fields are queried of the Unit object
    * @returns  List<SObject> a list of Units objects that are treated and stored as a generic type
    **/
    public List<SObject> getRecordsByBuildings(List<Building__c> buildingList, Set<String> fields) {
        if (buildingList.isEmpty()){return new List<SObject>();}
        List<Building__c> buildings = buildingList;
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Building__c IN :buildings LIMIT 10000';
        return Database.query(query);
    }

    /**
        * Method to fetch data stored in salesforce
        * @param    Id development record Id
        *           Set<String> fields is a set of Strings that determines which fields are queried of the Unit object
        * @returns  List<SObject> a list of Units objects that are treated and stored as a generic type
        **/
    public List<SObject> getGuestParkingRecordsByDevelopment(Id developmentId, Set<String> fields) {
        if (developmentId == null){return new List<SObject>();}
        Id devId = developmentId;
        Id rtId = getRecordType('Guest_Parking');
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Building__c IN (SELECT Id FROM Building__c WHERE Development_Site__c = :devId) AND RecordTypeId = :rtId LIMIT 10000';
        return Database.query(query);
    }

    /**
    * Method to fetch data stored in salesforce
    * @param    Set<String>   fields is a set of Strings that determines which fields are queried of the Unit object
    * @returns  List<SObject> a list of Units objects that are treated and stored as a generic type
    **/
    public List<SObject> getAllGuestParkingRecords(Set<String> fields) {
        Id rtId = getRecordType('Guest_Parking');
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE RecordTypeId = :rtId LIMIT 10000';
        List<SObject> resultList = new List<SObject>();
        resultList = Database.query(query);
        return resultList;
    }

    /**
    * Method to fetch data stored in salesforce
    * @param    Id selected Record Id
    *           Set<String> fields is a set of Strings that determines which fields are queried of the Unit object
    * @returns  List<SObject> a list of Units objects that are treated and stored as a generic type
    **/
    public List<SObject> getGuestParkingRecordById(Id selectedRecordId, Set<String> fields) {
        if (selectedRecordId == null){return new List<SObject>();}
        Id recordId = selectedRecordId;
        Id rtId = getRecordType('Guest_Parking');
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Id = :recordId AND RecordTypeId = :rtId LIMIT 1';
        return Database.query(query);
    }

    /**
    * Method to fetch data stored in salesforce
    * @param    Id selected Record Id
    *           Set<String> fields is a set of Strings that determines which fields are queried of the Unit object
    * @returns  List<SObject> a list of Units objects that are treated and stored as a generic type
    **/
    public List<SObject> getRecordById(Id selectedRecordId, Set<String> fields) {
        if (selectedRecordId == null){return new List<SObject>();}
        Id recordId = selectedRecordId;
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Id = :recordId LIMIT 1';
        return Database.query(query);
    }

    /**
    * Method to get Record Type Id for Rentable_Item__c Object
    * @param    recordTypeAPIName   String value of Record Type Api Name
    * @returns  Id Record Type Id
    **/
    private Id getRecordType(String recordTypeAPIName) {
        List<RecordType> recordTypes = [
            SELECT Id
            FROM RecordType
            WHERE
                sObjectType = :objectAPIName AND
                DeveloperName = :recordTypeAPIName
            LIMIT 1
        ];
        if (recordTypes != null && !recordTypes.isEmpty()) {
            return recordTypes[0].Id;
        }
        return null;
    }

}