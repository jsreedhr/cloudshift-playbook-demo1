/**
    * @Name:        UnitTriggerHandler
    * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 11/04/2018    Eugene Konstantinov    Created Class
*/
public with sharing class UnitTriggerHandler extends TriggerHandler {

	protected override void afterInsert(){
		UnitTriggerHelper.processAfterInsert(Trigger.new);
	}

	protected override void afterUpdate(){
		UnitTriggerHelper.processAfterUpdate(Trigger.oldMap, Trigger.newMap);
	}

}