/**
    * @Name:        ZooplaAsyncExecutionTest
    * @Description: Test class for ZooplaAsyncExecutionSendRequest Queueable class with callouts
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 08/05/2018    Andrey Korobovich    Created Class
*/
@isTest
private class ZooplaAsyncExecutionTest {
	private static Aggregator_Setting__mdt zooplaSettings;
	private static Aggregator_Setting__mdt zooplaRemoveSettings;

	static{
		List<Aggregator_Setting__mdt> aggregatorInsert = [
			SELECT Id, Label, Certificate__c, Method__c, URL__c, Content_Type__c, Network_Id__c, Channel__c, Branch_Id__c, Overseas__c
			FROM Aggregator_Setting__mdt
			WHERE
				Active__c = true AND
				Label = 'ZooplaService'
				AND Action__c = 'Insert'
			LIMIT 10000
		];
		System.assert(aggregatorInsert != null);
		System.assert(aggregatorInsert.size() == 1);
		zooplaSettings = aggregatorInsert[0];
		List<Aggregator_Setting__mdt> aggregatorDelete= [
			SELECT Id, Label, Certificate__c, Method__c, URL__c, Content_Type__c, Network_Id__c, Channel__c, Branch_Id__c, Overseas__c
			FROM Aggregator_Setting__mdt
			WHERE
				Active__c = true AND
				Label = 'ZooplaRemoveService'
				AND Action__c = 'Delete'
			LIMIT 10000
		];
		zooplaRemoveSettings = aggregatorDelete[0];
	}


	@isTest
	static void testCallout() {
		Test.setMock(HttpCalloutMock.class, new ZooplaMockHttpResponseGenerator());

		Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);

		Map<String, List<sObject>> testObjects = TestDataFactoryForNewViewingController.getTestUnits();
		List<Unit__c> units = testObjects.get('unit');
		System.assert(units[0].Zoopla_URL__c == null);

		Test.startTest();
			ZooplaAsyncExecutionSendRequest request = new ZooplaAsyncExecutionSendRequest(
				zooplaSettings.Method__c,
				zooplaSettings.URL__c,
				new Map<String, String>{
				'Content-Type'=>'application/json; charset=utf-8'
				},
				'Test Body',
				zooplaSettings.Certificate__c,
				units[0]
			);
			System.enqueueJob(request);
		Test.stopTest();
			Unit__c unitAfterCallout = [
			SELECT  Id, Zoopla_URL__c
			FROM Unit__c
			WHERE
				Id = :units[0].Id
			LIMIT 1
		];
        System.debug(unitAfterCallout.Zoopla_URL__c);
		System.assert(unitAfterCallout.Zoopla_URL__c == 'http://realtime-listings.webservices.zpg.co.uk/preview/listing/1524580825070415/34037248');
	}


	@isTest
	static void testRemoveCallout() {
		Test.setMock(HttpCalloutMock.class, new ZooplaMockHttpResponseGenerator());
		Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);
		Test.startTest();
			Map<String, List<sObject>> testObjects = TestDataFactoryForNewViewingController.getTestUnits();
        	List<Unit__c> units = testObjects.get('unit');
			units[0].Zoopla_URL__c = 'http://realtime-listings.webservices.zpg.co.uk/preview/listing/1524580825070415/34037248';
			units[0].Release_to_Aggregators__c = false;
        	update units;
        	ZooplaAsyncExecutionRemoveRequest req = new ZooplaAsyncExecutionRemoveRequest(
				zooplaRemoveSettings.Method__c,
				zooplaRemoveSettings.URL__c,
				new Map<String, String>{
				'Content-Type'=>'application/json; charset=utf-8'
				},
				'Test Body',
				zooplaRemoveSettings.Certificate__c,
				units[0],
                false
			);
			System.enqueueJob(req);
		Test.stopTest();
		Unit__c unitAfterCallout = [
			SELECT  Id, Zoopla_URL__c
			FROM Unit__c
			WHERE
				Id = :units[0].Id
			LIMIT 1
		];
		System.debug(unitAfterCallout.Zoopla_URL__c);
		System.assert(unitAfterCallout.Zoopla_URL__c == null);
	}
}