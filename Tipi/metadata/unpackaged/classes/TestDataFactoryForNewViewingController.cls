/**
    * @Name:        TestDataFactoryForNewViewingController
    * @Description: Data Factory class for testing newViewing and integration functionality
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 16/04/2018    Eugene Konstantinov    Created Class
*/
@isTest
public class TestDataFactoryForNewViewingController {
    private static Integer counter;
    private static List<RecordType> recTypes = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Rentable_Item__c'];


    /**
     * Main Method to create test data
     *
     * @returns Map<String, List<sObject>>  key - short name of object, value - list of objects
     */
    public static Map<String, List<sObject>> getTestUnits() {
        Map<String, List<sObject>> mapOfLists = new Map<String, List<sObject>>();
        List<Development__c> devSites = new List<Development__c>();
        List<Unit__c> units = new List<Unit__c>();
        List<Rentable_Item__c> items = new List<Rentable_Item__c>();
        devSites.add(getDevSite());
        mapOfLists.put('devSite', devSites);
        List<Building__c> buildings = getBuildings(devSites.get(0));
        mapOfLists.put('building', buildings);
        units.addAll(getUnits(0, 5, buildings.get(0)));
        units.addAll(getUnits(5, 10, buildings.get(1)));
        insert units;
        mapOfLists.put('unit', units);
        items.addAll(getRentItems(buildings.get(0)));
        items.addAll(getRentItems(buildings.get(1)));
        insert items;
        mapOfLists.put('rentItem', items);
        System.debug(units);
        return mapOfLists;
    }

    /**
     * Main Method to create test data (account, contact, opportunity)
     *
     * @returns Map<String, List<sObject>>  key - short name of object, value - object
     */
    public static Map<String, sObject> getMapOfSObjects() {
        Map<String, sObject> mapOfSObjects = new Map<String, sObject>();
        Account acc = getAccount();
        mapOfSObjects.put('acc', acc);
        Contact contact = getContact(acc);
        mapOfSObjects.put('contact', contact);
        Opportunity opp = getOpportunity(acc, contact);
        mapOfSObjects.put('opp', opp);
        return mapOfSObjects;
    }

    /**
     * Method to create Development record
    */
    private static Development__c getDevSite() {
        Development__c devSite = new Development__c(
            Name = 'Test Development Site ',
            POD_Group_Name__c = 'Pod A',
            Street__c = 'Test Street',
            City__c = 'Test City',
            Postcode__c = 'TEST 323',
            County__c = 'Bristol',
            Country__c = 'United Kingdom'
        );
        insert devSite;
        return devSite;
    }

    /**
    * Method to create Account record
    */
    private static Account getAccount() {
        RecordType recType = [SELECT Id, Name, DeveloperName FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Corporate'];
        Account acc = new Account(
        	Name = 'Test Account',
            RecordTypeId = recType.Id
        );
        insert acc;
        return acc;
    }

    /**
    * Method to create Contact record
     * @param acc - account for contact
    */
    private static Contact getContact(Account acc) {
        Contact contact = new Contact(
        	FirstName = 'Test Contact',
            LastName = 'Test Last Name',
            AccountId = acc.Id,
            Phone = '777777777',
            Email = 'test@test.com',
            Furniture__c = 'Furnished',
            Bedrooms__c = 'Studio',
            Preferred_Rent__c = 1000,
            Move_In_Date__c = Date.newInstance(2018, 4, 10),
            Min_Price_Range__c = '800 pcm',
            Max_Price_Range__c = '1200 pcm'
        );
        insert contact;
        return contact;
    }

    /**
    * Method to create Opportunity record
     * @param acc - account for Opportunity
     * @param contact - contact for Opportunity
    */
    private static Opportunity getOpportunity(Account acc, Contact contact) {
    	Opportunity opp = new Opportunity(
        	Name = 'Test Opportunity',
            AccountId = acc.Id,
            Primary_Contact__c = contact.Id,
            Type = '1 Bed',
            CloseDate = Date.newInstance(2018, 4, 7),
            StageName = 'Interested'
        ); 
        insert opp;
        return opp;
    }

    private static Location getLocation() {

       return  Location.newInstance(28.635308,77.22496);
    }


    /**
    * Method to create list of Building records
     * @param devSite - Development__c record
    */
    private static List<Building__c> getBuildings(Development__c devSite) {
        List<Building__c> buildings = new List<Building__c>();
        for (Integer i = 0; i < 2; i++) {
            Building__c building = new Building__c(
                Name = 'Test Building ' + i,
                Development_Site__c = devSite.Id,
                Street__c = devSite.Street__c,
                City__c = devSite.City__c,
                Postcode__c = devSite.Postcode__c,
                County__c = devSite.County__c,
                Country__c = devSite.Country__c,
                Allows_Pets__c = i == 1 ? true : false
            );

            building.Building_Latitude_Longitude__Latitude__s = getLocation().Latitude;
            building.Building_Latitude_Longitude__Longitude__s = getLocation().Longitude;

            buildings.add(building);
        }
        insert buildings;
        return buildings;
    }

    /**
    * Method to create list of Unit records
     * @param counter - start number of created records
     * @param counter2 - end number of created records
     * @param building - building for Unit
    */
    private static List<Unit__c> getUnits(Integer counter, Integer counter2, Building__c building) {
        List<Unit__c> units = new List<Unit__c>();
        for (Integer i = counter; i < counter2; i++) {
            Unit__c unit = new Unit__c(
                Name = 'Test Unit' + i,
                Description__c = 'test',
                Summary__c = 'test',
                Status__c = 'Occupied',
                Unit_Number__c = '132',
                Unit_Type__c = getNumOfBedrooms(),
                Number_Of_Bedrooms__c = Integer.valueOf(getNumOfBedrooms().substring(0, 1).replace('S', '1')), 
                Furnished__c = getFurnished(),
                Rent__c = 1000 + 100*i, 
                Building__c = building.Id,
                Date_Available__C = Date.newInstance(2018, 4, 10 + i),
                Unit_Key_Data_01__c = 'Key data'
            );
            units.add(unit);
        }
        return units;
    }

    /**
    * Method to create list of Rentable Item records
     * @param building - building for Rentable Item
    */
    private static List<Rentable_Item__c> getRentItems(Building__c building) {
        List<Rentable_Item__c> items = new List<Rentable_Item__c>();
        for (Integer i = 0; i < 5; i++) {
            RecordType recType = getRecType();
        	Rentable_Item__c item = new Rentable_Item__c(
            	Name = recType.Name + i,
                Building__c = building.Id,
                RecordTypeId = recType.Id,
                Development__c = building.Development_Site__c
        	);
            items.add(item);
        }
        return items;
    }

    /**
     * Method to get RecordType
    */
    private static RecordType getRecType() {
        RecordType recType;
    	counter = (Integer)(Math.random()*10);
        if (counter < 5) {
            recType = recTypes.get(0);
        } else {
            recType = recTypes.get(1);
        }
        return recType;
    }

    /**
    * Method to get Bedrooms value
    */
    private static String getNumOfBedrooms() {
        String bedrooms;
        counter = (Integer)(Math.random()*10);
        if (counter == 1) {
            bedrooms = '1 bed';
        } else if (counter == 2) {
            bedrooms = '2 bed';
        } else if (counter == 3) {
            bedrooms = '3 bed';
        } else if (counter == 4) {
            bedrooms = '4 bed';
        } else {
            bedrooms = 'Studio';
        }
        return bedrooms;
    }

    /**
    * Method to get Furnished value
    */
    private static String getFurnished() {
        String furnished;
        counter = (Integer)(Math.random()*10);
        if (counter >= 5) {
            furnished = 'Furnished';
        } else {
            furnished = 'Unfurnished';
        }
        return furnished;
    }
}