/**
    * @Name:        PaymentAuthorisationControllerTest
    * @Description: Test class for PaymentAuthorisationController
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 14/05/2018    Eugene Konstantinov    Created Class
    * 28/06/2018	Eugene Konstantinov	   Additional scenarios added; Increased test coverage
*/
@isTest
private class PaymentAuthorisationControllerTest {

    @isTest
    static void testContactsForLease() {
        Account newAcc = new Account(Name='Test Account');
        insert newAcc;
        List<Contact> contacts = TestFactoryPayment.addContacts(new List<Account>{newAcc}, 1, true);
        System.assert(contacts.size() == 1);
        List<Contract> lease = [
            SELECT Id, SBQQ__Opportunity__c
            FROM Contract
            WHERE
                Id = :contacts[0].Current_Lease__c
            LIMIT 1
        ];
        System.assert(lease.size() == 1);
        Opportunity newOpp = TestFactoryPayment.getOpportunity(newAcc, contacts[0], true);
        lease[0].SBQQ__Opportunity__c = newOpp.Id;
        update lease;

        Test.startTest();
        	Opportunity oppInLease = PaymentAuthorisationController.getOpportunity(String.valueOf(contacts[0].Current_Lease__c));
        	System.assert(oppInLease.Id == newOpp.Id);
            String response = PaymentAuthorisationController.getAllContactsForLease(String.valueOf(contacts[0].Current_Lease__c));
            Map<String, Object> parsedResponse = (Map<String, Object>)System.JSON.deserializeUntyped(response);
            List<Object> parsedContacts = (List<Object>)parsedResponse.get('Contacts');
            System.assert(parsedContacts.size() == 1);
            Map<String, Object> parsedPayments = (Map<String, Object>)parsedResponse.get('Payments');
            System.assert(parsedPayments.size() == 0);
        Test.stopTest();
    }

    @isTest
    static void testContactsForOpportunity() {
        Account newAcc = new Account(Name='Test Account');
        insert newAcc;
        List<Contact> contacts = TestFactoryPayment.addContacts(new List<Account>{newAcc}, 1, true);
        System.assert(contacts.size() == 1);
        Opportunity newOpp = TestFactoryPayment.getOpportunity(newAcc, contacts[0], false);
        newOpp.Tenant_2__c = contacts[0].Id;
        newOpp.Tenant_3__c = contacts[0].Id;
        newOpp.Tenant_4__c = contacts[0].Id;
        newOpp.Tenant_5__c = contacts[0].Id;
        insert newOpp;

        Test.startTest();
            String response = PaymentAuthorisationController.getAllContactsForOpportunity(String.valueOf(newOpp.Id));
            Map<String, Object> parsedResponse = (Map<String, Object>)System.JSON.deserializeUntyped(response);
            List<Object> parsedContacts = (List<Object>)parsedResponse.get('Contacts');
            System.assert(parsedContacts.size() == 1);
            Map<String, Object> parsedPayments = (Map<String, Object>)parsedResponse.get('Payments');
            System.assert(parsedPayments.size() == 0);
        Test.stopTest();
    }

    @isTest
    static void testPaymentCreation() {
        Account newAcc = new Account(Name='Test Account');
        insert newAcc;
        List<Contact> contacts = TestFactoryPayment.addContacts(new List<Account>{newAcc}, 1, true);
        System.assert(contacts.size() == 1);
        Opportunity newOpp = TestFactoryPayment.getOpportunity(newAcc, contacts[0], false);
        newOpp.Leased_Reserved_Unit__c = contacts[0].Leased_Reserved_Unit__c;
        insert newOpp;

        Test.startTest();
        	System.assert(String.isNotBlank(PaymentAuthorisationController.getReservedUnitName(String.valueOf(newOpp.Id))));
            PaymentAuthorisationController.createAndSendPayment(String.valueOf(contacts[0].Id), String.valueOf(newOpp.Id));
            List<Tipi_Payments__c> createdPayments = [
                SELECT Id
                FROM Tipi_Payments__c
            ];
            System.assert(createdPayments.size() == 1);
        Test.stopTest();
    }

}