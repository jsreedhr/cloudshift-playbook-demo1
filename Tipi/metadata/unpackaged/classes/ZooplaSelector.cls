/**
    * @Name:        ZooplaSelector
    * @Description: Class for fetching data stored in salesforce.
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 03/05/2018    Andrey Korobovich    Created Class
*/
public with sharing class ZooplaSelector extends Selector{
	
	public static final String objectAPIName = ZooplaMapping__mdt.SobjectType.getDescribe().getName();

	/**
	* Method to fetch data stored in salesforce
	* @param    Set<String>   fields is a set of Strings that determines which fields are queried of the Unit object
	* @returns  List<SObject> a list of RightMove custom meta data that are treated and stored as a generic type
	**/
	public List<SObject> getRecords(Set<String> fields) {

		if (fields.isEmpty()){return new List<SObject>();}
		String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName;
		return Database.query(query);
	}
}