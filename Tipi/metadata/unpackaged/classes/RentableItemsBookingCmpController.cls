/**
    * @Name:        RentableItemsBookingCmpController
    * @Description: Controller class for RentableItemsBooking Lightning Component
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 24/04/2018    Eugene Konstantinov    Created Class
*/
public with sharing class RentableItemsBookingCmpController {

    /**
    * Method to get Contact for current User for RentableItemsBooking Lightning Component
    * @returns Contact current contact or null if user has not Contact
    */
    @AuraEnabled
    public static Contact getCurrentContact() {
        User currentUser = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if (currentUser != null && currentUser.ContactId != null) {
            List<Contact> currentContact = [
                SELECT Id, Name, Email, Current_Lease__c, Leased_Reserved_Unit__c, Leased_Reserved_Unit__r.Building__r.Development_Site__c, Current_Lease__r.SBQQ__Opportunity__c
                FROM Contact
                WHERE
                    Id = :currentUser.ContactId
                LIMIT 1];
            if (currentContact != null && !currentContact.isEmpty()) {
                return currentContact[0];
            }
        }
        return null;
    }

    /**
    * Method to get List of All Rentable Items with Guset Parking Record Type
    * @returns List<Rentable_Item__c> list of Guest Parkings
    */
    public static List<Rentable_Item__c> getRentableItems() {
        List<Rentable_Item__c> rentableItems = new RentableItemSelector().getAllGuestParkingRecords(new Set<String>{
            Rentable_Item__c.Name.getDescribe().getName(),
            Rentable_Item__c.Allocated__c.getDescribe().getName(),
            Rentable_Item__c.Allocated_Date_Time__c.getDescribe().getName(),
            Rentable_Item__c.Building__c.getDescribe().getName(),
            Rentable_Item__c.Cost__c.getDescribe().getName(),
            Rentable_Item__c.Size__c.getDescribe().getName(),
            Rentable_Item__c.LastModifiedDate.getDescribe().getName(),
            Rentable_Item__c.Image__c.getDescribe().getName(),
            'Building__r.Name',
            'Building__r.Guest_Parking_Image__c',
            'Building__r.Development_Site__r.Name',
            'Building__r.Development_Site__r.Guest_Parking_Cost__c'
        });
        return rentableItems;
    }

    /**
    * Method to get List of All Social Spaces
    * @returns List<Social_Space__c> list of Social Spaces
    */
    public static List<Social_Space__c> getSocialSpaces() {
        List<Social_Space__c> socialSpaces = new SocialSpaceSelector().getAllRecords(new Set<String>{
                Social_Space__c.Name.getDescribe().getName(),
                Social_Space__c.Building__c.getDescribe().getName(),
                Social_Space__c.Image__c.getDescribe().getName(),
                Social_Space__c.Cost__c.getDescribe().getName(),
                Social_Space__c.Availability_Start_Time__c.getDescribe().getName(),
                Social_Space__c.Availability_End_Time__c.getDescribe().getName(),
                'Building__r.Name',
                'Building__r.Development_Site__r.Name'
                });
        return socialSpaces;
    }

    /**
    * Method to get List of Guest Parkings for specific User (depend on Development of Leased_Reserved_Unit__c)
    * @returns List<Rentable_Item__c> list of Guest Parkings for specific development
    */
    @AuraEnabled
    public static List<Rentable_Item__c> getRentableItemsForContact() {
        Contact currentContact = getCurrentContact();
        if (currentContact != null && currentContact.Leased_Reserved_Unit__c != null &&
            currentContact.Leased_Reserved_Unit__r.Building__c != null &&
            currentContact.Leased_Reserved_Unit__r.Building__r.Development_Site__c != null) {
            Id developmentId = currentContact.Leased_Reserved_Unit__r.Building__r.Development_Site__c;
            List<Rentable_Item__c> rentableItems = new RentableItemSelector().getGuestParkingRecordsByDevelopment(developmentId, new Set<String>{
                    Rentable_Item__c.Name.getDescribe().getName(),
                    Rentable_Item__c.Allocated__c.getDescribe().getName(),
                    Rentable_Item__c.Allocated_Date_Time__c.getDescribe().getName(),
                    Rentable_Item__c.Building__c.getDescribe().getName(),
                    Rentable_Item__c.Cost__c.getDescribe().getName(),
                    Rentable_Item__c.Size__c.getDescribe().getName(),
                    Rentable_Item__c.LastModifiedDate.getDescribe().getName(),
                    Rentable_Item__c.Image__c.getDescribe().getName(),
                    'Building__r.Name',
                    'Building__r.Guest_Parking_Image__c',
                    'Building__r.Development_Site__r.Name',
                    'Building__r.Development_Site__r.Guest_Parking_Cost__c'
                    });
            return rentableItems;
        }
        return getRentableItems();
    }

    /**
    * Method to get List of Social Spaces for specific User (depend on Development of Leased_Reserved_Unit__c)
    * @returns List<Social_Space__c> list of Social Spaces for specific development
    */
    @AuraEnabled
    public static List<Social_Space__c> getSocialSpacesForContact() {
        Contact currentContact = getCurrentContact();
        if (currentContact != null && currentContact.Leased_Reserved_Unit__c != null &&
                        currentContact.Leased_Reserved_Unit__r.Building__c != null &&
                        currentContact.Leased_Reserved_Unit__r.Building__r.Development_Site__c != null) {
            Id developmentId = currentContact.Leased_Reserved_Unit__r.Building__r.Development_Site__c;
            List<Social_Space__c> socialSpaces = new SocialSpaceSelector().getRecordsByDevelopment(developmentId,new Set<String>{
                    Social_Space__c.Name.getDescribe().getName(),
                    Social_Space__c.Building__c.getDescribe().getName(),
                    Social_Space__c.Image__c.getDescribe().getName(),
                    Social_Space__c.Cost__c.getDescribe().getName(),
                    Social_Space__c.Availability_Start_Time__c.getDescribe().getName(),
                    Social_Space__c.Availability_End_Time__c.getDescribe().getName(),
                    'Building__r.Name',
                    'Building__r.Development_Site__r.Name'
                    });
            return socialSpaces;
        }
        return getSocialSpaces();
    }

}