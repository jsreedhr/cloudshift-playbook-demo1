/**
    * @Name:        ZooplaRemoveRequestStructure
    * @Description: Wrapper class for remove request to Zoopla
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 28/05/2018    Andrey Korobovich    Created Class
*/

public with sharing class ZooplaRemoveRequestStructure {
	public String listing_reference {get; set;}

	public ZooplaRemoveRequestStructure(String listing_reference){
		this.listing_reference = listing_reference;
	}
}