/**
    * @Name:        RightMoveMockHttpResponseGenerator
    * @Description: Class for generating Response for RightMoveAsyncExecutionTest
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 13/04/2018    Eugene Konstantinov    Created Class
*/
@isTest
global class RightMoveMockHttpResponseGenerator implements HttpCalloutMock{

	global HTTPResponse respond(HTTPRequest req) {
		String UPD_ENDPOINT = 'https://adfapi.adftest.rightmove.com/v1/property/sendpropertydetails';
        String DELETE_ENDPOINT = 'https://adfapi.adftest.rightmove.com/v1/property/removeproperty';

		System.assertEquals('POST', req.getMethod());

		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
        if(req.getEndpoint() == UPD_ENDPOINT){
			res.setBody('{"message":"Your property has successfully been uploaded to Rightmove","success":true,"errors":null,"warnings":[],"request_timestamp":"13-04-2018 12:34:46","response_timestamp":"13-04-2018 12:34:46","request_id":"231301-180413-123446-16510","property":{"agent_ref":"a22322someid","rightmove_id":11002200,"change_type":"UPDATE","rightmove_url":"http://some-url.html"}}');
        } else if(req.getEndpoint() == DELETE_ENDPOINT){
           res.setBody('{"message":"Your property has successfully been removed from Rightmove","success":true,"errors":null,"warnings":[],"request_timestamp":"29-05-2018 17:01:37","response_timestamp":"29-05-2018 17:01:38","request_id":"231301-180529-170137-43187","property":{"agent_ref":"a0k26000004o2QfAAI","rightmove_id":70965995,"change_type":"DELETE","rightmove_url":null}}');
        }
		res.setStatusCode(200);
        res.setStatus('OK'); 
		return res;
	}

}