/**
 * @Name:        TipiPaymentTriggerHelper
 * @Description: This is helper class, will hold all methods and functionalities that are to be build for TipiPaymentTrigger
 *
 * @author:      Eugene Konstantinov
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 24/05/2018    Eugene Konstantinov    Created Class
 * 30/05/2018    Patrick Fischer        Added calculateRemainingBalance() and processAfterDelete() methods
 */
public with sharing class TipiPaymentTriggerHelper {
    private static final String PAID_STATUS = 'Collected from customer';
    private static final String RESERVATION_TYPE = 'Reservation';
    private static final String RESERVED_UNIT_STATUS = 'Unoccupied - New Lease';
    private static final String UNRESERVED_UNIT_STATUS = 'Awaiting Reservation Payment';
    private static final String REFUND_RECORD_TYPE_NAME = 'Refund';

    private static final Map<String, Schema.RecordTypeInfo> recordTypeInfoNameMap = Schema.SObjectType.Tipi_Payments__c.getRecordTypeInfosByName();
    private static final Id refundRecType = recordTypeInfoNameMap.get(REFUND_RECORD_TYPE_NAME).getRecordTypeId();

    /**
     * To process inserted Tipi Payment records
     *
     * @param newList List of new Tipi Payment records (values before insert)
     */
    public static void processBeforeInsert(List<Tipi_Payments__c> tipiPayments) {
        tipiPayments = setCalculation(tipiPayments);
        tipiPayments = validateParentTipiCredit(tipiPayments, null);
    }

    /**
     * To process inserted Tipi Payment records
     *
     * @param  newMap  map of updated Tipi Payment records (values after insert)
     */
    public static void processAfterInsert(Map<Id, SObject> newMap) {
        Map<Id, Tipi_Payments__c> newPaymentsMap = (Map<Id, Tipi_Payments__c>) newMap;
        Set<Id> refundIds = new Set<Id>();
        Map<Id, Tipi_Payments__c> tipiPaymentsToUpdate = new Map<Id, Tipi_Payments__c>();

        for (Tipi_Payments__c payment : newPaymentsMap.values()) {
            if (payment.RecordTypeId == refundRecType) {
                refundIds.add(payment.Id);
            }
        }

        tipiPaymentsToUpdate.putAll(calculateRemainingBalance(newPaymentsMap));

        if (!refundIds.isEmpty()) {
            tipiPaymentsToUpdate.putAll(updateRefundProperty(refundIds, true));
        }

        if (!tipiPaymentsToUpdate.values().isEmpty()) {
            update tipiPaymentsToUpdate.values();
        }
    }

    /**
     * To process inserted Tipi Payment records
     *
     * @param newList List of new Tipi Payment records (values before insert)
     * @param oldMap Map of updated Tipi Payment records (values before update)
     */
    public static void processBeforeUpdate(List<Tipi_Payments__c> tipiPayments, Map<Id, Tipi_Payments__c> oldMap) {
        tipiPayments = setCalculation(tipiPayments);
        tipiPayments = validateParentTipiCredit(tipiPayments, oldMap);
    }

    /**
     * To process updated Tipi Payment records
     *
     * @param  oldMap  map of updated Tipi Payment records (values before update)
     * @param  newMap  map of updated Tipi Payment records (values after update)
     */
    public static void processAfterUpdate(Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
        Map<Id, Tipi_Payments__c> newPaymentsMap = (Map<Id, Tipi_Payments__c>) newMap;
        Map<Id, Tipi_Payments__c> oldPaymensMap = (Map<Id, Tipi_Payments__c>) oldMap;

        Map<Id, Tipi_Payments__c> newPositivePaymensMap = new Map<Id, Tipi_Payments__c>();
        Map<Id, Tipi_Payments__c> oldPositivePaymensMap = new Map<Id, Tipi_Payments__c>();

        Map<Id, Tipi_Payments__c> newRefundsMap = new Map<Id, Tipi_Payments__c>();
        Map<Id, Tipi_Payments__c> oldRefundsMap = new Map<Id, Tipi_Payments__c>();

        Map<Id, Tipi_Payments__c> tipiPaymentsToUpdate = new Map<Id, Tipi_Payments__c>();

        Set<Id> refundIds = new Set<Id>();

        for (Id paymentId : newPaymentsMap.keySet()) {
            if (newPaymentsMap.get(paymentId).RecordTypeId == refundRecType) {
                newRefundsMap.put(paymentId, newPaymentsMap.get(paymentId));
                refundIds.add(paymentId);
            } else {
                newPositivePaymensMap.put(paymentId, newPaymentsMap.get(paymentId));
            }
        }

        for (Id paymentId : oldPaymensMap.keySet()) {
            if (oldPaymensMap.get(paymentId).RecordTypeId == refundRecType) {
                oldRefundsMap.put(paymentId, oldPaymensMap.get(paymentId));
            } else {
                oldPositivePaymensMap.put(paymentId, oldPaymensMap.get(paymentId));
            }
        }

        tipiPaymentsToUpdate.putAll(calculateRemainingBalance(newPaymentsMap));

        tipiPaymentsToUpdate.putAll(refundPropertyUpdValue(refundIds, oldRefundsMap, newRefundsMap));

        Set<Id> unitsToUpdate = new Set<Id>();
        for (Tipi_Payments__c tipiPayment : newPositivePaymensMap.values()) {
            if (tipiPayment.Status__c != oldPositivePaymensMap.get(tipiPayment.Id).Status__c && tipiPayment.Status__c == PAID_STATUS) {
                if (tipiPayment.Type__c == RESERVATION_TYPE) {
                    unitsToUpdate.add(tipiPayment.Unit__c);
                }
            }
        }
        if (unitsToUpdate != null && !unitsToUpdate.isEmpty()) {
            updateUnitStatusAfterReservationPayment(unitsToUpdate);
        }

        if(!tipiPaymentsToUpdate.values().isEmpty()) {
            update tipiPaymentsToUpdate.values();
        }
    }

    /**
     * To process deleted Tipi Payment records
     *
     * @param  oldMap  map of deleted Tipi Payment records (values before delete)
     */
    public static void processBeforeDelete(Map<Id, SObject> oldMap) {
        Map<Id, Tipi_Payments__c> oldPaymensMap = (Map<Id, Tipi_Payments__c>) oldMap;
        Set<Id> refundIds = new Set<Id>();
        List<Tipi_Payments__c> listForUpdate = new List<Tipi_Payments__c>();

        for (Tipi_Payments__c payment : oldPaymensMap.values()) {
            if (payment.RecordTypeId == refundRecType) {
                refundIds.add(payment.Id);
            }
        }

        if (!refundIds.isEmpty()) {
            listForUpdate.addAll(updateRefundProperty(refundIds, false));
        }

        if (!listForUpdate.isEmpty()) {
            update listForUpdate;
        }
    }

    /**
     * To process deleted Tipi Payment records
     *
     * @param  oldMap  map of deleted Tipi Payment records (values after delete)
     */
    public static void processAfterDelete(Map<Id, SObject> oldMap) {
        Map<Id, Tipi_Payments__c> oldPaymensMap = (Map<Id, Tipi_Payments__c>) oldMap;
        List<Tipi_Payments__c> listForUpdate = new List<Tipi_Payments__c>();

        listForUpdate.addAll(calculateRemainingBalance(oldPaymensMap));

        if (!listForUpdate.isEmpty()) {
            update listForUpdate;
        }
    }

    /**
     * To process undeleted Tipi Payment records
     *
     * @param  newMap  map of undeleted Tipi Payment records (values after undelete)
     */
    public static void processAfterUnDelete(Map<Id, SObject> newMap) {
        Map<Id, Tipi_Payments__c> newPaymentsMap = (Map<Id, Tipi_Payments__c>) newMap;
        Set<Id> refundIds = new Set<Id>();
        List<Tipi_Payments__c> listForUpdate = new List<Tipi_Payments__c>();

        for (Tipi_Payments__c payment : newPaymentsMap.values()) {
            if (payment.RecordTypeId == refundRecType) {
                refundIds.add(payment.Id);
            }
        }
        if (!refundIds.isEmpty()) {
            listForUpdate.addAll(updateRefundProperty(refundIds, true));
        }

        listForUpdate.addAll(calculateRemainingBalance(newPaymentsMap));

        if (!listForUpdate.isEmpty()) {
            update listForUpdate;
        }
    }

    /*********** HELPER METHODS BELOW ***********/

    private static void updateUnitStatusAfterReservationPayment(Set<Id> unitIds) {
        List<Unit__c> unitsToUpdate = new UnitSelector().getRecordsByIds(unitIds, new Set<String>{
                Unit__c.Status__c.getDescribe().getName()
        });
        for (Unit__c unit : unitsToUpdate) {
            unit.Status__c = RESERVED_UNIT_STATUS;
        }
        update unitsToUpdate;
    }

    private static List<Tipi_Payments__c> updateRefundProperty(Set<Id> refundIds, Boolean isIncrement) {
        Map<Id, Unit__c> unitMapForUpd = new Map<Id, Unit__c>();
        List<Tipi_Payments__c> resultList = new List<Tipi_Payments__c>();
        for (Tipi_Payments__c refund : [
                SELECT Id, Amount__c, Tipi_Payment__c, Tipi_Payment__r.Refund_Amount__c, Tipi_Payment__r.Type__c, Tipi_Payment__r.Amount__c, Tipi_Payment__r.Unit__c,
                        Tipi_Payment__r.Unit__r.Status__c
                FROM Tipi_Payments__c
                WHERE Id IN :refundIds
        ]) {
            if (refund.Tipi_Payment__c != null) {
                if (isIncrement) {
                    if (refund.Tipi_Payment__r.Unit__c != null && refund.Tipi_Payment__r.Type__c == RESERVATION_TYPE) {
                        refund.Tipi_Payment__r.Unit__r.Status__c = UNRESERVED_UNIT_STATUS;
                        unitMapForUpd.put(refund.Tipi_Payment__r.Unit__c, refund.Tipi_Payment__r.Unit__r);
                    }
                    refund.Tipi_Payment__r.Refund_Amount__c = refund.Tipi_Payment__r.Refund_Amount__c != null ? refund.Tipi_Payment__r.Refund_Amount__c - refund.Amount__c : -refund.Amount__c;
                } else {
                    if (refund.Tipi_Payment__r.Unit__c != null && refund.Tipi_Payment__r.Type__c == RESERVATION_TYPE) {
                        refund.Tipi_Payment__r.Unit__r.Status__c = RESERVED_UNIT_STATUS;
                        unitMapForUpd.put(refund.Tipi_Payment__r.Unit__c, refund.Tipi_Payment__r.Unit__r);
                    }
                    refund.Tipi_Payment__r.Refund_Amount__c = refund.Tipi_Payment__r.Refund_Amount__c != null ? refund.Tipi_Payment__r.Refund_Amount__c + refund.Amount__c : 0;
                    if (refund.Tipi_Payment__r.Refund_Amount__c < 0) {
                        refund.Tipi_Payment__r.Refund_Amount__c = 0;
                    }
                }

                resultList.add(refund.Tipi_Payment__r);
            }
        }
        if (!unitMapForUpd.isEmpty()) {
            update unitMapForUpd.values();
        }
        return resultList;
    }

    private static List<Tipi_Payments__c> refundPropertyUpdValue(Set<Id> refundIds, Map<Id, Tipi_Payments__c> oldMap, Map<Id, Tipi_Payments__c> newMap) {
        List<Tipi_Payments__c> resultList = new List<Tipi_Payments__c>();
        for (Tipi_Payments__c refund : [
                SELECT Id, Amount__c, Tipi_Payment__c, Tipi_Payment__r.Refund_Amount__c, Tipi_Payment__r.Amount__c, Tipi_Payment__r.Unit__c,
                        Tipi_Payment__r.Unit__r.Status__c
                FROM Tipi_Payments__c
                WHERE Id IN:refundIds
        ]) {
            if (refund.Tipi_Payment__c != null) {
                Decimal oldValue = -oldMap.get(refund.Id).Amount__c;
                Decimal newValue = -newMap.get(refund.Id).Amount__c;
                refund.Tipi_Payment__r.Refund_Amount__c = refund.Tipi_Payment__r.Refund_Amount__c != null ? refund.Tipi_Payment__r.Refund_Amount__c - oldValue + newValue : newValue;
                if (refund.Tipi_Payment__r.Refund_Amount__c < 0) {
                    refund.Tipi_Payment__r.Refund_Amount__c = 0;
                }
                resultList.add(refund.Tipi_Payment__r);
            }
        }
        return resultList;
    }

    /**
     * To calculate the Total_Remaining_Balance__c for both Credit and Reservation records
     *
     * @param tipiPaymentsMap Map of Tipi_Payments__c records by Id
     *
     * @return List of Tipi_Payments record with updated Total_Remaining_Balance__c amount
     */
    private static List<Tipi_Payments__c> calculateRemainingBalance(Map<Id, Tipi_Payments__c> tipiPaymentsMap) {
        List<Tipi_Payments__c> tipiPaymentsToUpdate = new List<Tipi_Payments__c>();

        Set<Id> parentTipiPaymentIds = new Set<Id>();
        for (Tipi_Payments__c tipiPayment : tipiPaymentsMap.values()) {
            parentTipiPaymentIds.add(tipiPayment.Id);

            // if payment has parent payment record
            if (tipiPayment.Tipi_Payment__c != null) {
                parentTipiPaymentIds.add(tipiPayment.Tipi_Payment__c);
            }
        }

        for (Tipi_Payments__c parentTipiPayment : [
                SELECT Id, Amount__c, Total_Remaining_Balance__c, External_Id__c, Type__c, (
                        SELECT Id, Amount__c, Status__c, RecordType.DeveloperName
                        FROM Tipi_Payments__r
                        WHERE
                        (
                            (
                                    // either Rent Payment Types and Debit
                                    Type__c IN :Utils_Constants.TIPI_RENT_PAYMENT_TYPES AND
                                    Credit_Payment_Type__c = :Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT
                            ) OR (
                                    // or Refund
                                    RecordType.DeveloperName = :Utils_Constants.PAYMENT_RECORDTYPE_REFUND
                            )
                        ) AND (
                                Status__c = :Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION OR
                                Status__c = :Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER
                        )
                )
                FROM Tipi_Payments__c
                WHERE
                Id IN :parentTipiPaymentIds AND
                ( 
                        Type__c = :Utils_Constants.PAYMENT_TYPE_RESERVATION
                        OR
                        Credit_Payment_Type__c = :Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT
                )
        ]) {
            Decimal remainingBalance = parentTipiPayment.Amount__c;
            for (Tipi_Payments__c childTipiPayment : parentTipiPayment.Tipi_Payments__r) {
                if(childTipiPayment.RecordType.DeveloperName == Utils_Constants.PAYMENT_RECORDTYPE_REFUND) {
                    // because Amount of 'Refund' records are stored as a negative value
                    remainingBalance += childTipiPayment.Amount__c;
                } else {
                    remainingBalance -= childTipiPayment.Amount__c;
                }
            }
            if(     // Set Remaining Balance to 0 for imported records (from Yard) that have an External Id
                    String.isNotBlank(parentTipiPayment.External_Id__c) ||

                    // Set Remaining Balance to 0 for non-rent / non-reservation records
                    (!Utils_Constants.TIPI_RENT_PAYMENT_TYPES.contains(parentTipiPayment.Type__c) &&
                            Utils_Constants.PAYMENT_TYPE_RESERVATION != parentTipiPayment.Type__c)) {
                remainingBalance = 0;
            }

            if (parentTipiPayment.Total_Remaining_Balance__c != remainingBalance) {
                parentTipiPayment.Total_Remaining_Balance__c = remainingBalance;
                tipiPaymentsToUpdate.add(parentTipiPayment);
            }
        }
        return tipiPaymentsToUpdate;
    }

    /**
     * To set the Calculation Amount Id on inserted/updated opportunities
     *
     * @param tipiPayments List of Tipi_Payments__c records on which to set Leased_Reserved_Unit__c
     *
     * @return List of Tipi_Payments__c records with set Calculation__c field
     */
    private static List<Tipi_Payments__c> setCalculation(List<Tipi_Payments__c> tipiPayments) {
        for(Tipi_Payments__c tipiPayment : tipiPayments) {
            if(tipiPayment.Calculation_Long__c != null) {
                tipiPayment.Calculation__c = tipiPayment.Calculation_Long__c.abbreviate(255);
            }
        }

        return tipiPayments;
    }

    /**
     * To validate if the parent Tipi Payment has sufficient funds available
     *
     * @param tipiPayments List of Tipi_Payments__c records to validate
     *
     * @return List of Tipi_Payments__c records with set Calculation__c field
     */
    private static List<Tipi_Payments__c> validateParentTipiCredit (List<Tipi_Payments__c> tipiPayments, Map<Id, Tipi_Payments__c> oldMap) {

        Set<Id> parentTipiPaymentIds = new Set<Id>();
        for(Tipi_Payments__c tipiPayment : tipiPayments) {
            if(tipiPayment.Tipi_Payment__c != null) {
                if(oldMap == null) {
                    // if NEW
                    parentTipiPaymentIds.add(tipiPayment.Tipi_Payment__c);
                } else if(tipiPayment.Amount__c != oldMap.get(tipiPayment.Id).Amount__c ||
                        tipiPayment.Tipi_Payment__c != oldMap.get(tipiPayment.Id).Tipi_Payment__c) {
                    // if UPDATE (Amount has changed or Tipi Payment Lookup has changed)
                    parentTipiPaymentIds.add(tipiPayment.Tipi_Payment__c);
                }
            }
        }

        Map<Id, Tipi_Payments__c> parentTipiPaymentsByIds = new Map<Id, Tipi_Payments__c>([
                SELECT Id, Name, Amount__c, Total_Remaining_Balance__c, (
                        SELECT Id, Amount__c, Status__c, Name, Payment_Date__c
                        FROM Tipi_Payments__r
                        WHERE
                        Type__c IN :Utils_Constants.TIPI_RENT_PAYMENT_TYPES AND
                        Credit_Payment_Type__c = :Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT AND
                        (
                                Status__c = :Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION OR
                                Status__c = :Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER
                        )
                )
                FROM Tipi_Payments__c
                WHERE Id IN :parentTipiPaymentIds
        ]);

        for(Tipi_Payments__c tipiPayment : tipiPayments) {
            if(tipiPayment.Tipi_Payment__c != null) {
                Tipi_Payments__c parentTipiPayment = parentTipiPaymentsByIds.get(tipiPayment.Tipi_Payment__c);
                if(parentTipiPayment != null) {

                    Set<Id> childTipiPaymentIds = new Set<Id>(new Map<Id, Tipi_Payments__c>(parentTipiPayment.Tipi_Payments__r).keySet());

                    String errorMsg = System.Label.ERROR_INSUFFICIENT_FUNDS_AVAILABLE_ON_PARENT_TIPI_PAYMENT
                            + ' Amount of current record: ' + tipiPayment.Amount__c + '.'
                            + ' Remaining balance of selected parent Tipi Payment \'' + parentTipiPayment.Name + '\': '
                            + parentTipiPayment.Total_Remaining_Balance__c + ' (of ' + parentTipiPayment.Amount__c + ')';
                    if(oldMap == null) {
                        // if NEW
                        if(tipiPayment.Amount__c > parentTipiPayment.Total_Remaining_Balance__c) {
                            tipiPayment.addError(errorMsg);
                        }
                    } else {
                        // if EDIT
                        if( (   // Tipi Payment changed and payment not yet child
                                tipiPayment.Tipi_Payment__c != oldMap.get(tipiPayment.Id).Tipi_Payment__c
                                && !childTipiPaymentIds.contains(tipiPayment.Id)
                                && tipiPayment.Amount__c > parentTipiPayment.Total_Remaining_Balance__c
                         ) || ( // Tipi Payment changed and payment already child
                                tipiPayment.Tipi_Payment__c != oldMap.get(tipiPayment.Id).Tipi_Payment__c
                                && childTipiPaymentIds.contains(tipiPayment.Id)
                                && tipiPayment.Amount__c > (parentTipiPayment.Total_Remaining_Balance__c + oldMap.get(tipiPayment.Id).Amount__c)
                         ) || ( // Tipi Payment consistent and payment Amount changed
                                tipiPayment.Tipi_Payment__c == oldMap.get(tipiPayment.Id).Tipi_Payment__c
                                && tipiPayment.Amount__c != oldMap.get(tipiPayment.Id).Amount__c
                                && tipiPayment.Amount__c > (parentTipiPayment.Total_Remaining_Balance__c + oldMap.get(tipiPayment.Id).Amount__c)
                        ) ) {
                            tipiPayment.addError(errorMsg);
                        }
                    }
                }
            }
        }

        return tipiPayments;
    }
}