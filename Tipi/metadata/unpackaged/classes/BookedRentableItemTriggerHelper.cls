/**
 * @Name:        BookedRentableItemTriggerHelper
 * @Description: This is helper class, will hold all methods and functionalities that are to be build for BookedRentableItem Trigger
 *
 * @author:      Andrey Korobovich
 * @version:     1.0
 * Change Log
 *
 * Date          author                 Change Description
 * -----------------------------------------------------------------------------------
 * 18/05/2018    Andrey Korobovich      Created Class
 * 11/06/2018    Eugene Konstantinov    Added processAfterInsert(Map<Id, sObject> newMap) method
 * 19/07/2018    Patrick Fischer        Added logic for Concession calculation
 */
public with sharing class BookedRentableItemTriggerHelper {
    private static final Set<String> RENTABLE_ITEM_PENDING = new Set<String>{'Pending Document Send', 'Awaiting Signature', 'License Signed'};
    private static final Set<String> RENTABLE_ITEM_ALLOCATED = new Set<String>{'Allocated'};
    private static final String SOCIAL_SPACE_OBJECT_NAME = Social_Space__c.SObjectType.getDescribe().getName();
    private static final String RENTABLE_ITEM_OBJECT_NAME = Rentable_Item__c.SObjectType.getDescribe().getName();

    /**
     * Method for processing inserted Booked Rentable Item records
     * @param  newMap  map of inserted Booked Rentable Item records
     */
    public static void processAfterInsert(Map<Id, SObject> newMap) {
        Map<Id, Booked_Rentable_Item__c> newRentableItemMap = (Map<Id, Booked_Rentable_Item__c>) newMap;

        List<Tipi_Payments__c> newTipiPayments = buildTipiPaymentRecords(newRentableItemMap);
        if (newTipiPayments != null && !newTipiPayments.isEmpty()) {
            insert newTipiPayments;
        }
    }

    /**
     * Method for processing updated Booked Rentable Item records
     * @param  oldMap  map of updated Booked Rentable Item records (values before update)
     * @param  newMap  map of updated uBooked Rentable Itemnit records (values after update)
     */
    public static void processAfterUpdate(Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
        Map<Id, Booked_Rentable_Item__c> oldRentableItemMap = (Map<Id, Booked_Rentable_Item__c>) oldMap;
        Map<Id, Booked_Rentable_Item__c> newRentableItemMap = (Map<Id, Booked_Rentable_Item__c>) newMap;

        List<Booked_Rentable_Item__c> bookedItems = getUpdatedItems(oldRentableItemMap, newRentableItemMap);
        if (bookedItems != null && !bookedItems.isEmpty()) {
            List<Tipi_Payments__c> paymentsForInsert = buildTipiPaymentRecordsForBRI(bookedItems);
            System.debug(paymentsForInsert);
            insert paymentsForInsert;
        }
    }

    private static List<Booked_Rentable_Item__c> getUpdatedItems(Map<Id, Booked_Rentable_Item__c> oldMap, Map<Id, Booked_Rentable_Item__c> newMap) {
        List<Booked_Rentable_Item__c> resultList;
        Set<Id> itemIds = new Set<Id>();
        for (Id itemId : newMap.keySet()) {
            if (RENTABLE_ITEM_ALLOCATED.contains(newMap.get(itemId).Status__c) && RENTABLE_ITEM_PENDING.contains(oldMap.get(itemId).Status__c)) {
                itemIds.add(itemId);
            }
        }
        if (!itemIds.isEmpty()) {
            resultList = selectRentableItmes(itemIds);
        }
        return resultList;
    }

    private static List<Booked_Rentable_Item__c> selectRentableItmes(Set<Id> ids) {
        List<Booked_Rentable_Item__c> res =  new List<Booked_Rentable_Item__c>();
        for (Booked_Rentable_Item__c item : [ SELECT Id, Start_Date__c, End_Date__c, Rentable_Item__c, Rentable_Item__r.Cost__c, Rentable_Item__r.Building__c, Contract__c, Contact__c,
                                               Rentable_Item__r.Product__r.Size__c, Rentable_Item__r.Name, Rentable_Item__r.Product__r.Product_Type__c, Contact__r.Name, Contract__r.Type__c,
                                               Rentable_Item__r.Development__c, Contract__r.SBQQ__Opportunity__c, Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__c, Contract__r.Payment_Config_Done__c,
                                               Quote_Line__c, Quote_Line__r.Free_Month__c, Quote_Line__r.Off_First_Month__c, Quote_Line__r.Monthly_Concession__c, Quote_Line__r.Concession_Length__c,
                                               Contract__r.SBQQ__Opportunity__r.Deposit__c, Contract__r.StartDate, Contract__r.EndDate, Contract__r.Unit__r.Building__r.Development_Site__c,
                                               Contract__r.SBQQ__Opportunity__r.Primary_Contact__r.Name, Contract__r.Actual_Tenancy_End_Date__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Checkbox__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Length__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Amount__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_Checkbox__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_First_Month__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Checkbox__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Text__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Unit_Rent_Rollup__c
                                               FROM Booked_Rentable_Item__c
                                               WHERE Id IN: ids]) {
            if (item.Rentable_Item__c == null) {
                Trigger.newMap.get(item.Id).addError(Label.Rentable_Item_field_should_be_filled);
            } else if (item.Contract__r.SBQQ__Opportunity__c == null) {
                Trigger.newMap.get(item.Id).addError(Label.Opportunity_field_on_related_Lease_should_be_filled);
            }  else if (item.Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__c == null) {
                Trigger.newMap.get(item.Id).addError(Label.Primary_Quote_field_on_Opportunity);
            } else {
                res.add(item);
            }
        }
        return res;
    }

    /**
     * Method for Tipi_Payments__c record creation after Booked_Rentable_Item__c was created or End Date Changed
     * @param List<Booked_Rentable_Item__c> list of Booked_Rentable_Item__c records
     * @returns List<Tipi_Payments__c> new records list
     */
    public static List<Tipi_Payments__c> buildTipiPaymentRecordsForBRI(List<Booked_Rentable_Item__c> items) {
        List<Tipi_Payments__c> resultList = new List<Tipi_Payments__c>();
        Set<Id> contactIds = new Set<Id>();
        for (Booked_Rentable_Item__c item : items) {
            contactIds.add(item.Contact__c);
        }
        Map<Id, Id> authorisations = PaymentConfiguratorService.getAuthorisationsMapByContact(contactIds);
        for (Booked_Rentable_Item__c item : items) {

            // construct monthly payments without concession
            List<Tipi_Payments__c> normalisedRent = PaymentConfiguratorService.normalisePaymentRecordsForBRI(
                item,
                authorisations.get(item.Contact__c),
                item.Rentable_Item__r.Cost__c,
                item.Rentable_Item__r.Cost__c,
                Utils_Constants.PAYMENT_GATEWAY_DD
            );

            // include concession for monthly payments
            if(item.Quote_Line__c == null) {
                resultList.addAll(normalisedRent);
            } else {
                List<Tipi_Payments__c> rentAppliedConcession = PaymentConfiguratorService.applyConcession(normalisedRent,
                        (item.Quote_Line__r.Concession_Length__c != null && item.Quote_Line__r.Monthly_Concession__c != null),
                        item.Quote_Line__r.Concession_Length__c,
                        item.Quote_Line__r.Monthly_Concession__c,
                        (item.Quote_Line__r.Off_First_Month__c != null),
                        item.Quote_Line__r.Off_First_Month__c,
                        (item.Quote_Line__r.Free_Month__c != null && item.Quote_Line__r.Free_Month__c != ''),
                        item.Quote_Line__r.Free_Month__c
                );
                resultList.addAll(rentAppliedConcession);
            }
        }

        return resultList;
    }

    /**
     * Method for Tipi_Payments__c record creation after Booked_Rentable_Item__c was created
     * @param currentContact Map<Id, Booked_Rentable_Item__c>
     * @returns List<Tipi_Payments__c> new records list
     */
    private static List<Tipi_Payments__c> buildTipiPaymentRecords(Map<Id, Booked_Rentable_Item__c> newRentableItemMap) {
        List<Tipi_Payments__c> newTipiPayments = new List<Tipi_Payments__c>();

        Set<Id> contactIds = new Set<Id>();
        for (Booked_Rentable_Item__c bri : newRentableItemMap.values()) {
            if (bri.Contact__c != null) {
                contactIds.add(bri.Contact__c);
            }
        }
        List<Contact> contactsFromBRI = new ContactSelector().getRecordsByIds(contactIds, new Set<String>{
            Contact.Name.getDescribe().getName(),
            Contact.Leased_Reserved_Unit__c.getDescribe().getName(),
            'Current_Lease__r.SBQQ__Opportunity__c'
        });
        Map<Id, Contact> contactById = new Map<Id, Contact>(contactsFromBRI);

        for (Booked_Rentable_Item__c bri : newRentableItemMap.values()) {
            Id bookedItemId = bri.Social_Space__c != null ? bri.Social_Space__c : bri.Rentable_Item__c;
            if (contactById.containsKey(bri.Contact__c)) {
                Tipi_Payments__c newPayment = createTipiPayments(contactById.get(bri.Contact__c), bookedItemId, bri.Total_Number_of_Hours__c, bri.Total_Number_of_Days__c, bri.First_Day_Following_Month__c);
                if (newPayment != null) {
                    newTipiPayments.add(newPayment);
                }
            }
        }

        return newTipiPayments;
    }

    /**
     * Method for Tipi_Payments__c record creation after Booked_Rentable_Item__c was created
     * @param currentContact Contact
     * @param bookedItemId string value of Rentable Item or Social Space Id
     * @param rentableHours Long value
      * @param rentableDays Integer value
     * @param type String value ('Social Space', 'Guest Parking', 'Storage')
     * @returns Tipi_Payments__c new record
     */
    private static Tipi_Payments__c createTipiPayments(Contact currentContact, Id bookedItemId, Decimal rentableHours, Decimal rentableDays, Date paymentDate) {
        String type = '';
        Social_Space__c bookedItem;
        Rentable_Item__c bookedItemRentable;
        Id recordId = Id.valueOf(bookedItemId);
        String objectName = recordId.getSobjectType().getDescribe().getName();
        if (objectName == SOCIAL_SPACE_OBJECT_NAME) {
            List<SObject> socialSpaces = new SocialSpaceSelector().getRecordById(recordId, new Set<String>{
                Social_Space__c.Name.getDescribe().getName(),
                Social_Space__c.Building__c.getDescribe().getName(),
                Social_Space__c.Cost__c.getDescribe().getName(),
                'Building__r.Development_Site__c'
            });
            if (socialSpaces != null && !socialSpaces.isEmpty()) {
                bookedItem = (Social_Space__c)socialSpaces[0];
            }
        } else if (objectName == RENTABLE_ITEM_OBJECT_NAME) {
            List<SObject> guestParkings = new RentableItemSelector().getRecordById(recordId, new Set<String>{
                Rentable_Item__c.Name.getDescribe().getName(),
                Rentable_Item__c.Building__c.getDescribe().getName(),
                Rentable_Item__c.Cost__c.getDescribe().getName(),
                Rentable_Item__c.Development__c.getDescribe().getName(),
                'Building__r.Development_Site__c',
                'Building__r.Development_Site__r.Guest_Parking_Cost__c',
                'RecordType.DeveloperName'
            });
            if (guestParkings != null && !guestParkings.isEmpty()) {
                bookedItemRentable = (Rentable_Item__c)guestParkings[0];
            }
        }
        if((bookedItem != null || bookedItemRentable != null) && currentContact != null && currentContact.Current_Lease__c != null) {
            Tipi_Payments__c newPayments = new Tipi_Payments__c();
            newPayments.Contact__c = currentContact.Id;
            newPayments.Opportunity__c = currentContact.Current_Lease__r.SBQQ__Opportunity__c;
            Decimal unitCost;
            if (bookedItem != null) {
                unitCost = bookedItem.Cost__c;
                type = 'Social Space';
            } else if (bookedItemRentable.RecordType.DeveloperName == 'Guest_Parking') {
                unitCost = bookedItemRentable.Building__r.Development_Site__r.Guest_Parking_Cost__c;
                type = 'Guest Parking';
            } else {
                return null;
            }
            if (unitCost == null) {
                unitCost = 0;
            }
            newPayments.Amount__c = rentableHours != null ? unitCost * rentableHours / 4 : unitCost * rentableDays;
            newPayments.Building__c = bookedItem != null ? bookedItem.Building__c : bookedItemRentable.Building__c;
            newPayments.Development__c = bookedItem != null ? bookedItem.Building__r.Development_Site__c : bookedItemRentable.Development__c;
            newPayments.Unit__c = currentContact.Leased_Reserved_Unit__c;
            newPayments.Payment_Date__c = paymentDate;
            newPayments.Description__c = bookedItem != null ? bookedItem.Name : bookedItemRentable.Name;
            newPayments.Status__c = 'Awaiting Submission';
            newPayments.Type__c = type;

            return newPayments;
        }
        return null;
    }
}