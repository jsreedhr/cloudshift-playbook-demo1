/**
    * @Name:        TipiPaymentSelector
    * @Description: Class for fetching data stored in salesforce.
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 24/05/2018    Andrey Korobovich    Created Class
*/

public with sharing class TipiPaymentSelector extends Selector{
	public static final String objectAPIName = Tipi_Payments__c.SobjectType.getDescribe().getName();

	public List<SObject> getRecordsByIds(Set<Id> recordIds, Set<String> fields) {
		if (recordIds.isEmpty()){return new List<SObject>();}
		String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Id in ' + ConstructInClauseString(recordIds);
		return Database.query(query);
	}

	public List<SObject> getRecordsByPaymentIds(Set<Id> recordIds, Set<String> fields) {
		if (recordIds.isEmpty()){return new List<SObject>();}
		String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Payment__c in ' + ConstructInClauseString(recordIds);
		return Database.query(query);
	}

    /**
        * Method to fetch data stored in salesforce
        * @param    Id contact Record Id
        * @param    Set<Id> contact Record Id
        * @param    Set<String>   fields is a set of Strings that determines which fields are queried of the Unit object
        * @returns  List<SObject> a list of Units objects that are treated and stored as a generic type
        **/
    public List<SObject> getTipiPaymentsByPayments(Set<Id> aspPaymentIds, Set<String> fields) {
        if (aspPaymentIds == null){return new List<SObject>();}
        Set<Id> paymentIds = aspPaymentIds;
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Payment__c IN :paymentIds LIMIT 10000';
        return Database.query(query);
    }
}