/**
 * @Name:        Tenant
 * @Description: Wrapper Class Tenant holding the object/json structure to for every single Tenant
 *
 * @author:      Andrey Korobovich
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 18/05/2018    Andrey Korobovich    Created Class
 */

public with sharing class Tenant {
	@AuraEnabled public Id contactId { get; set; }
	@AuraEnabled public String name { get; set; }
	@AuraEnabled public Boolean isPayee { get; set; }
	@AuraEnabled public Decimal rent { get; set; }
	@AuraEnabled public String storageString { get; set; }
	@AuraEnabled public String parkingString { get; set; }
	@AuraEnabled public Decimal depositPayment { get; set; }
	@AuraEnabled public String upfrontPaymentType { get; set; }
	@AuraEnabled public Boolean isGuarantor { get; set; }
	@AuraEnabled public Boolean reservationPaid { get; set; }
	@AuraEnabled public Decimal reservationAmount { get; set; }
	@AuraEnabled public Decimal monthsUpfront { get; set; }
	@AuraEnabled public Decimal rentUpfront { get; set; }
	@AuraEnabled public Decimal totalInitialPayment { get; set; }
	@AuraEnabled public Decimal firstRentPayment { get; set; }
	@AuraEnabled public Decimal paymentShare { get; set; }

	public Tenant(Id contactId, String name, String monthsUpfront, Tipi_Payments__c reservationPayment) {
		this.contactId = contactId;
		this.name = name;
		this.isPayee = false;
		this.rent = 0;
		this.storageString = '';
		this.parkingString = '';
		this.depositPayment = 0;
		this.upfrontPaymentType = '';
		this.isGuarantor = false;
		if(reservationPayment == null) {
			this.reservationAmount = 0;
			this.reservationPaid = false;
		} else {
			this.reservationAmount = reservationPayment.Amount__c;
			if(reservationPayment.Status__c == Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER) {
				this.reservationPaid = true;
			} else {
				this.reservationPaid = false;
			}
		}

		try {
			this.monthsUpfront = Decimal.valueOf(monthsUpfront);
		} catch (Exception e) {
			this.monthsUpfront = 0;
		}
		this.rentUpfront = 0;
		this.totalInitialPayment = 0;
		this.firstRentPayment = 0;
		this.paymentShare = 0;
	}
}