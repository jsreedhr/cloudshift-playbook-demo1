/**
 * @Name:        LightningUtilityControllerTest
 * @Description: Test class for LightningUtilityController functionality
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 22/06/2018    Patrick Fischer      Created Class
 */
@IsTest
public with sharing class LightningUtilityControllerTest {

    /**
     * Method tested: validatePermissionSet()
     * Expected result: Successfully validated that current user has access to the Permission Set
     */
    private static testMethod void validatePermissionSetSuccess() {
        PermissionSet permissionSet = new PermissionSet(
                Name = 'Test_Permission_Set_LEX',
                Label = 'Test Permissions'
        );
        insert permissionSet;

        PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment(
                AssigneeId = UserInfo.getUserId(),
                PermissionSetId = permissionSet.Id
        );
        insert permissionSetAssignment;

        Test.startTest();
        Boolean isValid = LightningUtilityController.validatePermissionSet('Test_Permission_Set_LEX');
        Test.stopTest();

        System.assertEquals(true, isValid, 'Expecting no use to be assigned to a Permission with the name of xxx');
    }

    /**
     * Method tested: validatePermissionSet()
     * Expected result: No assignment of current user to permission set validated
     */
    private static testMethod void validatePermissionSetFailure() {
        PermissionSet permissionSet = new PermissionSet(
                Name = 'Test_Permission_Set_LEX',
                Label = 'Test Permissions'
        );
        insert permissionSet;
        
        Test.startTest();
        Boolean isValid = LightningUtilityController.validatePermissionSet('Test_Permission_Set_LEX');
        Test.stopTest();

        System.assertEquals(false, isValid, 'Expecting no use to be assigned to a Permission with the name of xxx');
    }
}