/**
    * @Name:        ZooplaService
    * @Description: Class to process callouts for Zoopla API.
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 03/05/2018    Andrey Korobovich    Created Class
*/

public with sharing class ZooplaService implements AggregationsInterface {
	/**
	 * Main method to process callouts for any Aggregator
	 * @param  aggregator  argument contains all needed values to do callout
	 * @param  units list of units to be processed to Zoopla API
	 */

	public Map<Id, String> processRequest(List<Unit__c> units, Aggregator_Setting__mdt aggregator) {
		Map<Id, String> responsesByUnitId = new Map<Id, String>();
		// Generating JSON based on Zoopla Schema
		Map<Id, String> jsonByUnitId = ZooplaUtilities.generateJson(units, aggregator.Branch_Id__c);
		System.debug(JSON.serializePretty(jsonByUnitId));
		if (aggregator != null && units != null && !units.isEmpty()) {
			for (Unit__C unit : units) {
				String validJson = ZooplaUtilities.replaceTypeProperty(jsonByUnitId.get(unit.Id));
				// Invoking Queuelable to do callout and updating appropriate Unit with external Id and URL
				ZooplaAsyncExecutionSendRequest request = new ZooplaAsyncExecutionSendRequest(
				    aggregator.Method__c,
				    aggregator.URL__c,
				    new Map<String, String> {'Content-Type' => aggregator.Content_Type__c, 'ZPG-Listing-ETag' => String.valueOf(unit.id) + String.valueOf(unit.LastModifiedDate) },
				    validJson,
				    aggregator.Certificate__c,
				    unit);
				if (!Test.isRunningTest()) {
					System.enqueueJob(request);
				}
			}
		}

		return responsesByUnitId;
	}

}