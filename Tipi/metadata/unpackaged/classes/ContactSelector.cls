/**
    * @Name:        ContactSelector
    * @Description: Class for fetching data stored in salesforce.
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 11/05/2018    Eugene Konstantinov    Created Class
    * 14/05/2018    Eugene Konstantinov    Added getRecordsByIds Method
*/
public with sharing class ContactSelector extends Selector{
    public static final String objectAPIName = Contact.SobjectType.getDescribe().getName();

    /**
    * Method to fetch data stored in salesforce
    * @param    Id            contactId Id of Current Contact
    *           Set<String>   fields is a set of Strings that determines which fields are queried of the Building object
    * @returns  List<SObject> a list of Contact objects that are treated and stored as a generic type
    **/
    public List<SObject> getRecordsById(Id contactId, Set<String> fields) {
        Id cId = contactId;
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Id = :cId LIMIT 10000';
        return Database.query(query);
    }

    /**
    * Method to fetch data stored in salesforce
    * @param    Set<Id>            contactIds
    *           Set<String>   fields is a set of Strings that determines which fields are queried of the Building object
    * @returns  List<SObject> a list of Contact objects that are treated and stored as a generic type
    **/
    public List<SObject> getRecordsByIds(Set<Id> contactIds, Set<String> fields) {
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Id IN ' + ConstructInClauseString(contactIds) + ' LIMIT 10000';
        return Database.query(query);
    }

}