/**
    * @Name:        ZooplaUtilities
    * @Description: Class that contains all needed components to process Zoopla integration
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 03/05/2018    Andrey Korobovich    Created Class
*/
public with sharing class ZooplaUtilities {
	private static final String DEFAULT_COUNTRY_CODE = 'GB';
	private static final String DEAFULT_CATEGORY = 'residential';
	public static final String DEFAULT_SQUARE_UNIT = 'sq_feet';
	private static final Double DEFAULT_ANGLE = 90.00;
	private static final String DEFAULT_TRANSACTION_TYPE = 'rent';

	// Map that contains fields mapping Zoopla JSON Value => Field API Name on Unit object
	private static Map<String, String> getMTDFields = getMTDFields();
	// Map that contains values mapping for Zoopla Property Type based on Unit Type on Unit object
	private static Map<String, String> valueForPropertyType = getStringValueForPropertyType();
	// Map that contains values mapping for Zoopla Status based on Status on Unit object
	private static Map<String, String> valueForStatus = getStringValueForStatus();
	// Map that contains values mapping for Zoopla Furnished Type based on Furnished Type on Unit object
	private static Map<String, String> getValueForFurnishedType = getValueForFurnishedType();


	private static String getValueForPropertyType(String fieldName) {
		return valueForPropertyType.get(fieldName);
	}

	private static String getValueForStatus(String fieldName) {
		return valueForStatus.get(fieldName);
	}

	private static String getValueForFurnishedType(String fieldName) {
		return getValueForFurnishedType.get(fieldName);
	}

	public static String replaceTypeProperty(String json) {
		String res = json.replace('type_Z', 'type');
		return res;
	}


	public static Map<Id, String> generateJson(List<Unit__c> units, String branch_id) {
		Map<Id, String> jsonByUnit = new Map<Id, String>();
		//Map with Unit Id and related Buildings
		Map<Id, Building__c> buildings = getBuildingsById(units);
		//Map with Building ID and related Rentable Items
		Map<Id, List<Rentable_Item__c>> rentableItemByBuildingId = getRentableItemsByBuildings(buildings.values());
		//Map with Parking options for the Zoopla. Key is the Building Id
		Map<Id, List<Integer>> parkingByBuilding = getParkingListForBuilding(rentableItemByBuildingId);
		//Map with list of Key Fetures for the Zoopla. Key is the Unit Id
		Map<Id, List<String>> featuresListByUnitId = getFeaturesForUnits(units);
		//Map with list of Media for the Zoopla. Key is the Unit Id
		Map<Id, Integer> entranceByUnitId = getEntranceForUnit(units);
		//Map with list of Content for Zoopla. Key is the Unit Id
		Map<Id, List<ZooplaSendPropertyStructure.Content>> contentMap = getContentByUnitId(units);
		for (Unit__c unit : units) {
			ZooplaSendPropertyStructure outboundJson = new ZooplaSendPropertyStructure();

			ZooplaSendPropertyStructure.Coordinates coordinates = new ZooplaSendPropertyStructure.Coordinates(
			    String.isBlank(getField('latitude')) ? null : Double.valueOf(buildings.get(unit.Id).Building_Latitude_Longitude__latitude__s),
			    String.isBlank(getField('longitude')) ? null : Double.valueOf(buildings.get(unit.Id).Building_Latitude_Longitude__longitude__s)
			);

			ZooplaSendPropertyStructure.Google_street_view googleCoordinates = new ZooplaSendPropertyStructure.Google_street_view (
			    coordinates,
			    DEFAULT_ANGLE,
			    DEFAULT_ANGLE
			);

			ZooplaSendPropertyStructure.Location location = new ZooplaSendPropertyStructure.Location(
			    String.isBlank(getField('House_Name_Number')) ? null : String.valueOf(unit.get(getField('House_Name_Number'))),
			    String.isBlank(getField('Street')) ? null : String.valueOf(unit.get(getField('Street'))),
			    String.isBlank(getField('county')) ? null : String.valueOf(unit.get(getField('county'))),
			    String.isBlank(getField('city')) ? null : String.valueOf(unit.get(getField('city'))),
			    String.isBlank(getField('postcode')) ? null : String.valueOf(unit.get(getField('postcode'))),
			    DEFAULT_COUNTRY_CODE,
			    coordinates
			);
			ZooplaSendPropertyStructure.Detailed_description description = new ZooplaSendPropertyStructure.Detailed_description(
			    String.isBlank(getField('heading')) ? null : String.valueOf(unit.get(getField('heading'))),
			    null,
			    String.isBlank(getField('description')) ? null : String.valueOf(unit.get(getField('description')))
			);

			ZooplaSendPropertyStructure.Pricing pricing = new ZooplaSendPropertyStructure.Pricing(
			    String.isBlank(getField('price')) ? null : Double.valueOf(unit.get(getField('price'))),
			    String.isBlank(getField('price_qualifier')) ? null : String.valueOf(unit.get(getField('price_qualifier'))),
			    String.isBlank(getField('auction')) ? null : Boolean.valueOf(unit.get(getField('auction'))),
			    String.isBlank(getField('rent_frequency')) ? 'per_month' : String.valueOf(unit.get(getField('rent_frequency'))),
			    UserInfo.isMultiCurrencyOrganization() ? (String) unit.get('CurrencyIsoCode') : UserInfo.getDefaultCurrency(),
			    String.isBlank(getField('price_per_unit_area')) ? null : null, // TODO UPDATE IT
			    DEFAULT_TRANSACTION_TYPE
			);

			ZooplaSendPropertyStructure.Minimum area = new ZooplaSendPropertyStructure.Minimum(
			    String.isBlank(getField('square')) ? null : Double.valueOf(unit.get(getField('square'))),
			    DEFAULT_SQUARE_UNIT
			);

			ZooplaSendPropertyStructure.Areas areas = new ZooplaSendPropertyStructure.Areas(
			    new ZooplaSendPropertyStructure.Internal(area, null),
			    null
			);


			outboundJson.accessibility = String.isBlank(getField('accessibility')) ? null : Boolean.valueOf(unit.get(getField('accessibility')));
			outboundJson.administration_fees = String.isBlank(getField('administration_fee')) ? null : String.valueOf(unit.get(getField('administration_fee')));
			outboundJson.annual_business_rates = String.isBlank(getField('annual_business_rates')) ? null : Double.valueOf(unit.get(getField('administration_fee')));
			outboundJson.areas = areas;
			outboundJson.available_bedrooms = String.isBlank(getField('available_bedrooms')) ? null : Integer.valueOf(unit.get(getField('available_bedrooms')));
			outboundJson.available_from_date = String.isBlank(getField('available_date')) ? null : buildDateString(Date.valueOf(unit.get(getField('available_date'))));
			outboundJson.basement = String.isBlank(getField('basement')) ? null : Boolean.valueOf(unit.get(getField('basement')));
			outboundJson.bathrooms =  String.isBlank(getField('bathrooms')) ? null :  Integer.valueOf(unit.get(getField('bathrooms')));
			outboundJson.burglar_alarm = String.isBlank(getField('burglar_alarm')) ? null : Boolean.valueOf(unit.get(getField('burglar_alarm')));
			outboundJson.business_for_sale = String.isBlank(getField('business_for_sale')) ? null : Boolean.valueOf(unit.get(getField('business_for_sale')));
			outboundJson.branch_reference = branch_id;
			outboundJson.category = DEAFULT_CATEGORY;
			outboundJson.central_heating = String.isBlank(getField('central_heating')) ? null : String.valueOf(unit.get(getField('central_heating')));
			outboundJson.chain_free = String.isBlank(getField('chain_free')) ? null : Boolean.valueOf(unit.get(getField('chain_free')));
			outboundJson.conservatory = String.isBlank(getField('conservatory')) ? null : Boolean.valueOf(unit.get(getField('conservatory')));
			outboundJson.construction_year = String.isBlank(getField('construction_year')) ? null :  Integer.valueOf(unit.get(getField('construction_year')));
			outboundJson.floor_levels = String.isBlank(getField('floor_levels')) ? null :  new List<Integer> {Integer.valueOf(unit.get(getField('floor_levels')))};
			outboundJson.content = contentMap.get(unit.id);
			outboundJson.council_tax_band = String.isBlank(getField('council_tax_band')) ? null :  String.valueOf(unit.get(getField('council_tax_band')));
			outboundJson.decorative_condition = String.isBlank(getField('decorative_condition')) ? null :  String.valueOf(unit.get(getField('decorative_condition')));
			outboundJson.deposit = String.isBlank(getField('deposit')) ? null : Integer.valueOf(unit.get(getField('deposit')));
			outboundJson.detailed_description = new List<ZooplaSendPropertyStructure.Detailed_description> { description };
			outboundJson.display_address = String.isBlank(getField('display_address')) ? null : String.valueOf(unit.get(getField('display_address')));
			outboundJson.double_glazing = String.isBlank(getField('double_glazing')) ? null : Boolean.valueOf(unit.get(getField('double_glazing')));
			outboundJson.feature_list = featuresListByUnitId.get(unit.id);
			outboundJson.fireplace = String.isBlank(getField('fireplace')) ? null : Boolean.valueOf(unit.get(getField('fireplace')));
			outboundJson.fishing_rights = String.isBlank(getField('fishing_rights')) ? null : Boolean.valueOf(unit.get(getField('fishing_rights')));
			outboundJson.furnished_state = String.isBlank(getField('furnished_state')) ? null : getValueForFurnishedType(String.valueOf(unit.get(getField('furnished_state'))));
			outboundJson.ground_rent = String.isBlank(getField('ground_rent')) ? null : Double.valueOf(unit.get(getField('ground_rent')));
			outboundJson.gym = String.isBlank(getField('gym')) ? null : Boolean.valueOf(unit.get(getField('gym')));
			outboundJson.listed_building_grade = String.isBlank(getField('listed_building_grade')) ? null : String.valueOf(unit.get(getField('listed_building_grade')));
			outboundJson.listing_reference = unit.Id;
			outboundJson.living_rooms = String.isBlank(getField('living_rooms')) ? null : Integer.valueOf(unit.get(getField('living_rooms')));
			outboundJson.location = location;
			outboundJson.google_street_view = googleCoordinates;
			outboundJson.loft = String.isBlank(getField('loft')) ? null : Boolean.valueOf(unit.get(getField('loft')));
			outboundJson.new_home = String.isBlank(getField('new_home')) ? null : Boolean.valueOf(unit.get(getField('new_home')));
			outboundJson.open_day = String.isBlank(getField('open_day')) ? null : String.valueOf(unit.get(getField('open_day')));
			outboundJson.outbuildings = String.isBlank(getField('outbuildings')) ? null : Boolean.valueOf(unit.get(getField('outbuildings')));
			outboundJson.outside_space = getOutsideSpaceArray(unit).size() == 0 ? null : getOutsideSpaceArray(unit);
			outboundJson.pets_allowed = String.isBlank(getField('pets_allowed')) ? null : Boolean.valueOf(buildings.get(unit.Id).Allows_Pets__c);
			outboundJson.porter_security = String.isBlank(getField('porter_security')) ? null : Boolean.valueOf(unit.get(getField('porter_security')));
			outboundJson.pricing = pricing;
			outboundJson.property_type = String.isBlank(getField('property_type')) ? null : getValueForPropertyType(String.valueOf(unit.get(getField('property_type'))));
			outboundJson.rateable_value = String.isBlank(getField('rateable_value')) ? null : Double.valueOf(unit.get(getField('rateable_value')));
			outboundJson.repossession = String.isBlank(getField('repossession')) ? null : Boolean.valueOf(unit.get(getField('repossession')));
			outboundJson.retirement = String.isBlank(getField('retirement')) ? null : Boolean.valueOf(unit.get(getField('retirement')));
			outboundJson.sap_rating = String.isBlank(getField('sap_rating')) ? null :  Integer.valueOf(unit.get(getField('sap_rating')));
			outboundJson.serviced = String.isBlank(getField('serviced')) ? null :  Boolean.valueOf(unit.get(getField('serviced')));
			outboundJson.shared_accommodation = String.isBlank(getField('shared_accommodation')) ? null :  Boolean.valueOf(unit.get(getField('shared_accommodation')));
			outboundJson.summary_description = String.isBlank(getField('summary')) ? null : String.valueOf(unit.get(getField('summary')));
			outboundJson.swimming_pool = String.isBlank(getField('swimming_pool')) ? null :  Boolean.valueOf(unit.get(getField('swimming_pool')));
			outboundJson.tenanted = String.isBlank(getField('tenanted')) ? null :  Boolean.valueOf(unit.get(getField('tenanted')));
			outboundJson.tennis_court = String.isBlank(getField('tennis_court')) ? null :  Boolean.valueOf(unit.get(getField('tennis_court')));
			outboundJson.total_bedrooms = String.isBlank(getField('total_bedrooms')) ? null : Integer.valueOf(unit.get(getField('total_bedrooms')));
			outboundJson.tenure = String.isBlank(getField('tenure')) ? null : String.valueOf(unit.get(getField('tenure')));
			outboundJson.utility_room = String.isBlank(getField('utility_room')) ? null :  Boolean.valueOf(unit.get(getField('utility_room')));
			outboundJson.waterfront = String.isBlank(getField('waterfront')) ? null :  Boolean.valueOf(unit.get(getField('waterfront')));
			outboundJson.wood_floors = String.isBlank(getField('wood_floors')) ? null :  Boolean.valueOf(unit.get(getField('wood_floors')));
			outboundJson.life_cycle_status = String.isBlank(getField('status')) ? null : getValueForStatus(String.valueOf(unit.get(getField('status'))));
			System.debug(JSON.serialize(outboundJson, true));
			jsonByUnit.put(unit.id, JSON.serialize(outboundJson, true));

		}

		return jsonByUnit;
	}

	private static String getField(String fieldName) {
		return (String)getMTDFields.get(fieldName);
	}


	/**
	 * Method to get Unit Field - Zoopla field mappting
	 *
	 * @returns Map<String,String> Unit Field Api Names by Zoopla field name
	 */
	private static Map<String, String> getMTDFields() {
		Map<String, String> zmmMap = new Map<String, String>();

		for (ZooplaMapping__mdt zmm : (List<ZooplaMapping__mdt>)new ZooplaSelector().getRecords(new Set<String> {
		ZooplaMapping__mdt.MasterLabel.getDescribe().getName(),
			ZooplaMapping__mdt.FieldAPIName__c.getDescribe().getName()
		})) {
			zmmMap.put(zmm.MasterLabel, zmm.FieldAPIName__c);
		}
		return zmmMap;
	}

	/**
	 * Method to build valid date field for Zoopla Api
	 *
	 * @returns String aaliable unit date
	 */
	private static String buildDateString (Date customDate) {

		if (customDate != null) {

			String res = DateTime.newInstance(customDate.year(),customDate.month(),customDate.day()).format('YYYY-MM-dd');

			return res;
		}
		return null;
	}

	/**
	* Method to get Building for unit
	* @param unitList list of unit added to aggregator
	*
	* @returns Map<Id, Building__c>  key - unit Id
	*/
	private static  Map<Id, Building__c> getBuildingsById(List<Unit__c> unitList) {
		Map<Id, Building__c> buildingByUnit;
		Set<Id> buildingIds = new Set<Id>();
		for (Unit__c unit : unitList) {
			buildingIds.add(unit.Building__c);
		}

		List<Building__c> buildings = (List<Building__c>) new BuildingSelector().getRecordsByUnitIds(buildingIds, new Set<String> {
			Building__c.Name.getDescribe().getName(),
			Building__c.Allows_Pets__c.getDescribe().getName(),
			Building__c.Building_Latitude_Longitude__longitude__s.getDescribe().getName(),
			Building__c.Building_Latitude_Longitude__latitude__s.getDescribe().getName()
		});

		Map<Id, Building__c> buildingById = new Map<Id, Building__c>(buildings);
		if (buildings != null && !buildings.isEmpty()) {
			buildingByUnit = new Map<Id, Building__c>();
			for (Unit__c unit : unitList) {
				if (unit.Building__c != null && buildingById.containsKey(unit.Building__c)) {
					buildingByUnit.put(unit.Id, buildingById.get(unit.Building__c));
				}
			}
		}

		return buildingByUnit;
	}


	private static List<String> getOutsideSpaceArray(Unit__c unit) {
		List<String> outside_spaces = new List<String>();
		if (String.isNotBlank(getField('terrace')) && Boolean.valueOf(unit.get(getField('terrace')))) {
			outside_spaces.add('terrace');
		}
		if (String.isNotBlank(getField('balconies'))) {
			outside_spaces.add('balcony');
		}
		return outside_spaces;
	}

	/**
	* Method to get list of rentable items for Building
	* @param buildings list of buildings related to units
	*
	* @returns Map<Id, List<Rentable_Item__c>>  key - building Id
	*/
	private static Map<Id, List<Rentable_Item__c>> getRentableItemsByBuildings(List<Building__c> buildings) {
		Map<Id, List<Rentable_Item__c>> rentableItemByBuilding;

		List<Rentable_Item__c> rentableItems = (List<Rentable_Item__c>) new RentableItemSelector().getRecordsByBuildings(buildings, new Set<String> {
			Rentable_Item__c.Name.getDescribe().getName(),
			Rentable_Item__c.Building__c.getDescribe().getName(),
			Rentable_Item__c.Allocated__c.getDescribe().getName(),
			'RecordType.DeveloperName'
		});

		if (rentableItems != null && !rentableItems.isEmpty()) {
			rentableItemByBuilding = new Map<Id, List<Rentable_Item__c>>();
			for (Rentable_Item__c rentableItem : rentableItems) {
				if (rentableItem.Building__c != null) {
					if (!rentableItemByBuilding.containsKey(rentableItem.Building__c)) {
						rentableItemByBuilding.put(rentableItem.Building__c, new List<Rentable_Item__c>());
					}
					rentableItemByBuilding.get(rentableItem.Building__c).add(rentableItem);
				}
			}
		}

		return rentableItemByBuilding;
	}

	/**
	* Method to get parking options for building
	* @param  rentableItemsById  map of rentable item records by Id
	*
	* @returns Map<Id, List<Integer>> Map with Parking options for the Zoopla. Key is the Building Id
	*/
	private static Map<Id, List<Integer>> getParkingListForBuilding(Map<Id, List<Rentable_Item__c>> rentableItemsById) {
		Map<Id, List<Integer>> parkingMap = new Map<Id, List<Integer>>();
		if (rentableItemsById != null && !rentableItemsById.isEmpty()) {
			for (Id buildingId : rentableItemsById.keySet()) {
				parkingMap.put(buildingId, new List<Integer>());
				for (Rentable_Item__c rentableItem : rentableItemsById.get(buildingId)) {
					if (rentableItem.RecordType.DeveloperName == 'Parking') {
						if (rentableItem.Allocated__c) {
							parkingMap.get(buildingId).add(13);
						} else {
							parkingMap.get(buildingId).add(22);
						}
					}
				}
			}
		}
		return parkingMap;
	}


	/**
	 * Method to get Value for property type
	 *
	 * @returns Map<String, String>  Map with String value for Unit property type
	 */
	private static Map<String, String> getStringValueForPropertyType() {
		Map<String, String> rtmMap = new Map<String, String>();

		for (Zoopla_Property_Type_Settings__mdt rtm : [SELECT MasterLabel, Zoopla_API_Type__c FROM Zoopla_Property_Type_Settings__mdt]) {
			rtmMap.put(rtm.MasterLabel, rtm.Zoopla_API_Type__c);
		}
		return rtmMap;
	}

	/**
	 * Method to get String value for status
	 *
	 * @returns Map<String, String>  Map with String value for Unit status
	 */
	private static Map<String, String> getStringValueForStatus() {
		Map<String, String> rtmMap = new Map<String, String>();

		for (Zoopla_Status_Field_Setting__mdt rtm : [SELECT MasterLabel, Unit_Status_Value__c FROM Zoopla_Status_Field_Setting__mdt]) {
			rtmMap.put(rtm.MasterLabel, rtm.Unit_Status_Value__c);
		}
		return rtmMap;
	}

	/**
	 * Method to get content list of unit
	 * @param units list of units added to aggreagator
	 *
	 * @returns Map<Id, List<ZooplaSendPropertyStructure.Content>>  key - unit Id
	 */
	private static Map<Id, List<ZooplaSendPropertyStructure.Content>> getContentByUnitId(List<Unit__c> units) {
		Map<Id, List<ZooplaSendPropertyStructure.Content>> resultMap = new Map<Id, List<ZooplaSendPropertyStructure.Content>>();
		Map<Id, Unit__c> unitById = new Map<Id, Unit__c>(units);
		List<Unit_File__c> unitFiles = (List<Unit_File__c>) new UnitFileSelector().getRecordsByUnitIds(unitById.keySet(), new Set<String> {
			Unit_File__c.Name.getDescribe().getName(),
			Unit_File__c.Unit__c.getDescribe().getName(),
			Unit_File__c.Proxy_URL__c.getDescribe().getName(),
			Unit_File__c.Type__c.getDescribe().getName(),
			Unit_File__c.Sequence_Number__c.getDescribe().getName(),
			Unit_File__c.Shared_Link__c.getDescribe().getName(),
			Unit_File__c.LastModifiedDate.getDescribe().getName()
		});
		for (Unit_File__c uFile : unitFiles) {

			if(String.isNotBlank(uFile.Proxy_URL__c)) {

				if (!resultMap.containsKey(uFile.Unit__c)) {
					resultMap.put(uFile.Unit__c, new List<ZooplaSendPropertyStructure.Content>());
				}
				String type_Z;
				if (uFile.Type__c == 'Image') {
					type_Z = 'image';
				} else if (uFile.Type__c == 'FloorPlan') {
					type_Z = 'floor_plan';
				}
				resultMap.get(uFile.Unit__c).add(new ZooplaSendPropertyStructure.Content(
						uFile.Proxy_URL__c,
						type_Z,
						null
				));

			}

		}
		return resultmap;
	}


	/**
	* Method to get entrance value for Unit
	* @param unitList list of unit added to aggregator
	*
	* @returns Map<Id, Integer> key - unit Id
	*/
	private static  Map<Id, Integer> getEntranceForUnit(List<Unit__c> unitList) {
		Map<Id, Integer> entranceFloorByUnitId = new Map<Id, Integer>();

		Map<Integer, Integer> entranceFloorByUnitFloor = new Map<Integer, Integer>();
		Integer defaultEntrance = 6;
		for (RightMove_Entrance_Floor_Setting__mdt floorSettings : [
		            SELECT MasterLabel, Default_Entrance_Floor__c, Entrance_Floor_Number__c, Unit_Floor_Value__c
		            FROM RightMove_Entrance_Floor_Setting__mdt
		        ]) {
			if (floorSettings.Unit_Floor_Value__c != null) {
				entranceFloorByUnitFloor.put(Integer.valueOf(floorSettings.Unit_Floor_Value__c), Integer.valueOf(floorSettings.Entrance_Floor_Number__c));
			} else if (floorSettings.Default_Entrance_Floor__c) {
				defaultEntrance = Integer.valueOf(floorSettings.Entrance_Floor_Number__c);
			}
		}

		for (Unit__c unit : unitList) {
			if (!entranceFloorByUnitFloor.containsKey(Integer.valueOf(unit.Floor__c))) {
				entranceFloorByUnitId.put(unit.Id, defaultEntrance);
			} else {
				entranceFloorByUnitId.put(unit.Id, entranceFloorByUnitFloor.get(Integer.valueOf(unit.Floor__c)));
			}
		}

		return entranceFloorByUnitId;
	}


	/**
	* Method to get list of unit features
	* @param units list of units added to aggreagator
	*
	* @returns Map<Id, List<String>>  key - unit Id
	*/
	private static Map<Id, List<String>> getFeaturesForUnits(List<Unit__c> units) {
		Map<Id, List<String>> featuresByUnit = new Map<Id, List<String>>();
		for (Unit__c unit : units) {
			if (!featuresByUnit.containsKey(unit.Id)) {
				featuresByUnit.put(unit.Id, new List<String>());
			}
			if (unit.Unit_Key_Data_01__c != null) {
				featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_01__c);
			}
			if (unit.Unit_Key_Data_02__c != null) {
				featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_02__c);
			}
			if (unit.Unit_Key_Data_03__c != null) {
				featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_03__c);
			}
			if (unit.Unit_Key_Data_04__c != null) {
				featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_04__c);
			}
			if (unit.Unit_Key_Data_05__c != null) {
				featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_05__c);
			}
			if (unit.Unit_Key_Data_06__c != null) {
				featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_06__c);
			}
			if (unit.Unit_Key_Data_07__c != null) {
				featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_07__c);
			}
			if (unit.Unit_Key_Data_08__c != null) {
				featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_08__c);
			}
			if (unit.Unit_Key_Data_09__c != null) {
				featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_09__c);
			}
			if (unit.Unit_Key_Data_10__c != null) {
				featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_10__c);
			}
		}
		return featuresByUnit;
	}

	/**
	 * Method to get String value for furnished type
	 *
	 * @returns Map<String, String>  Map with String value for Unit furnished type
	 */
	private static Map<String, String> getValueForFurnishedType() {
		Map<String, String> rtmMap = new Map<String, String>();

		for (Zoopla_Furnished_Type_Setting__mdt rtm : [SELECT MasterLabel, Furnished_Unit_Field_Value__c FROM Zoopla_Furnished_Type_Setting__mdt]) {
			rtmMap.put(rtm.MasterLabel, rtm.Furnished_Unit_Field_Value__c);
		}
		return rtmMap;
	}

	public static Map<Id, String> generateRemoveJson(List<Unit__c> units){
		Map<Id, String> resultmap = new Map<Id, String>();
		for(Unit__c unit : units){
			resultmap.put(unit.Id , JSON.serialize(new ZooplaRemoveRequestStructure(unit.Id)));
		}
		return resultmap;
	}

	public static Map<Id, Boolean> checkIsPresentLease(List<Unit__c> units) {
		Map<Id, Boolean> resultmap = new Map<Id, Boolean>();
		Set<Id> unitsIds = new Set<Id>();
		for(Unit__c unit : units) {
			unitsIds.add(unit.Id);
		}
		for(Contract c : [SELECT Id, StartDate, ContractTerm, Unit__c, Unit__r.Name FROM Contract WHERE Unit__c in :unitsIds]){
			Boolean isLeaseAct = c.StartDate <= Date.today()  && Date.today() < c.StartDate.addMonths(c.ContractTerm);
			if(resultmap.containsKey(c.Unit__c)){
				if(isLeaseAct){
					resultmap.put(c.Unit__c, true);
				}
			} else {
				resultmap.put(c.Unit__c, isLeaseAct);
			}
		}
		return resultmap;
	}
}