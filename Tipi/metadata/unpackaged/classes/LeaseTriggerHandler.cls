/**
    * @Name:        LeaseTriggerHandler
    * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 30/05/2018    Andrey Korobovich       Created Class
    * 21/06/2018    Eugene Konstantinov     Added beforeUpdate()
*/

public with sharing class LeaseTriggerHandler extends TriggerHandler {

    protected override void afterUpdate() {
        LeaseTriggerHelper.processAfterUpdate(Trigger.oldMap, Trigger.newMap);
    }

    protected override void beforeUpdate() {
        LeaseTriggerHelper.processBeforeUpdate(Trigger.oldMap, Trigger.newMap);
    }

    protected override void afterInsert() {
        LeaseTriggerHelper.processAfterInsert(Trigger.newMap);
    }
}