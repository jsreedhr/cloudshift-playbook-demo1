/**
    * @Name:        RefundTriggerHelper
    * @Description: This is helper class, will hold all methods and functionalities that are to be build for Refund Trigger
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 23/05/2018    Andrey Korobovich    Created Class
*/

public with sharing class RefundTriggerHelper {

	public static void processAfterInsert(Map<Id, sObject> newMap) {
		Id refundTypeId = Schema.SObjectType.Tipi_Payments__c.getRecordTypeInfosByName().get('Refund').getRecordTypeId();

		List<Tipi_Payments__c> paymentsForInsert = new List<Tipi_Payments__c>();
		Map<Id, asp04__Refund__c> refundsMap = (Map<Id, asp04__Refund__c>) newMap;
		Set<Id> paymentIds = new Set<Id>();
		for(Id refundId : refundsMap.keySet()) {
			paymentIds.add(refundsMap.get(refundId).asp04__Payment__c);
		}
		Map<Id, Tipi_Payments__c> tipiPaymentsMap = getTipiPyamentRecords(paymentIds);
        System.debug(refundsMap);
        System.debug(tipiPaymentsMap);
		for(asp04__Refund__c refund : refundsMap.values()){
			Tipi_Payments__c tPayment = tipiPaymentsMap.get(refund.asp04__Payment__c);
			if(tPayment != null){
				Tipi_Payments__c tipiRefund = tPayment.clone(false, true, false, false);
				tipiRefund.Amount__c = -refund.asp04__Amount__c;
				tipiRefund.Tipi_Payment__c = tPayment.Id;
            	tipiRefund.Payment__c = null;
				tipiRefund.RecordTypeId = refundTypeId;
				paymentsForInsert.add(tipiRefund);
			}
		}
		System.debug(paymentsForInsert);
		insert paymentsForInsert;
	}


	private static Map<	Id, Tipi_Payments__c> getTipiPyamentRecords(Set<Id> pymemtIds) {
		Map<Id, Tipi_Payments__c> resultMap = new Map<Id, Tipi_Payments__c>();
		Set<String> fieldSet = Tipi_Payments__c.sObjectType.getDescribe().fields.getMap().keySet();
		for(Tipi_Payments__c tPayment : (List<Tipi_Payments__c>) new TipiPaymentSelector().getRecordsByPaymentIds(pymemtIds, fieldSet)){
			resultMap.put(tPayment.Payment__c , tPayment);	
		}
		return resultMap;
	}
}