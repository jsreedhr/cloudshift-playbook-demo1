/**
 * @Name:        SendInitialPaymentController
 * @Description: Controller class for c:sendInitialPayment LEX component
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 07/06/2018    Patrick Fischer    Created Class
 */
public without sharing class SendInitialPaymentController {

    private static final String PAYMENT_TYPE_DEPOSIT = 'Deposit';

    public class InitialPaymentData {
        @AuraEnabled public Contact contact { get; private set; }
        @AuraEnabled public Tipi_Payments__c deposit { get; private set; }
        @AuraEnabled public Tipi_Payments__c initialPayment { get; private set; }
        @AuraEnabled public Tipi_Payments__c reservation { get; private set; }
        @AuraEnabled public Decimal totalAmount { get; private set; }
        @AuraEnabled public Boolean isFirstRequest { get; private set; }
        @AuraEnabled public Boolean isUpfrontCredit { get; private set; }
    }

    /**
     * To initialise the c:sendInitialPayment LEX component
     *
     * @param recordId Id of Lease/Contract record
     *
     * @return List of InitialPaymentData containing Contact, Deposit Tipi_Payments__c, and Initial Payment Tipi_Payments__c details
     */
    @AuraEnabled
    public static List<InitialPaymentData> getInitialPayments(Id recordId) {
        return getInitialPaymentsMap(recordId).values();
    }

    /**
     * To construct a Map of InitialPaymentData by Contact Id
     *
     * @param recordId Id of Lease/Contract record
     *
     * @return Map of InitialPaymentData by Contact Id (containing Contact, Deposit Tipi_Payments__c, and Initial Payment Tipi_Payments__c details)
     */
    private static Map<Id, InitialPaymentData> getInitialPaymentsMap(Id recordId) {

        Map<Id, InitialPaymentData> initialPaymentData = new Map<Id, InitialPaymentData>();

        Id opportunityId = getOpportunityId(recordId);

        for(Tipi_Payments__c tipiPayment : [
                SELECT Id, Amount__c, Amount_without_Concession__c, Authorisation__c, Building__c, Calculation_Long__c,
                        Concession_Amount__c, Credit_Payment_Type__c, Contact__c, Contact__r.Id, Contact__r.FirstName,
                        Contact__r.LastName, Contact__r.Email, Contact__r.AccountId, Contact__r.Account.Name,
                        Development__c, Name, Nth_Month_Payment__c, Opportunity__c, Payment__c, Payment__r.Name,
                        Payment__r.asp04__Amount__c, Payment_Date__c, Payment_Gateway__c, Payment_Share__c,
                        Status__c, Status_Code__c, Tipi_Payment__c, Total_Amount__c, Total_Remaining_Balance__c, Type__c, Unit__c
                FROM Tipi_Payments__c
                WHERE
                Opportunity__c = :opportunityId
                AND (
                        Nth_Month_Payment__c = 1
                        OR
                        Type__c = :Utils_Constants.PAYMENT_TYPE_RESERVATION
                )
        ]) {
            if(!initialPaymentData.containsKey(tipiPayment.Contact__c)) {
                initialPaymentData.put(tipiPayment.Contact__c, new InitialPaymentData());
            }
            initialPaymentData.get(tipiPayment.Contact__c).contact = tipiPayment.Contact__r;

            // Deposit Payment
            if(tipiPayment.Type__c == PAYMENT_TYPE_DEPOSIT) {
                initialPaymentData.get(tipiPayment.Contact__c).deposit = tipiPayment;
            }

            // Initial Rent Payment
            if(Utils_Constants.TIPI_RENT_PAYMENT_TYPES.contains(tipiPayment.Type__c)) {

                if(tipiPayment.Credit_Payment_Type__c == Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT) {
                    initialPaymentData.get(tipiPayment.Contact__c).initialPayment = tipiPayment;
                } else if (tipiPayment.Tipi_Payment__c == null) {
                    initialPaymentData.get(tipiPayment.Contact__c).initialPayment = tipiPayment;
                } else if (initialPaymentData.get(tipiPayment.Contact__c).initialPayment == null) {
                    initialPaymentData.get(tipiPayment.Contact__c).initialPayment = tipiPayment;
                }
            }

            // Reservation Payment
            if(tipiPayment.Type__c == Utils_Constants.PAYMENT_TYPE_RESERVATION) {
                initialPaymentData.get(tipiPayment.Contact__c).reservation = tipiPayment;
            }
        }

        for(InitialPaymentData payee : initialPaymentData.values()) {
            payee.isUpfrontCredit = isUpfrontCredit(payee);
            payee.totalAmount = getTotalAmount(payee);
            payee.isFirstRequest = isFirstRequest(payee);
        }

        return initialPaymentData;
    }

    /**
     * To query the parent Opportunity Id of a Contract/Lease record
     *
     * @param contractId Id of Contract record
     *
     * @return Id of Opportunity record
     */
    private static Id getOpportunityId(Id contractId) {
        List<Contract> contracts = [SELECT Id, SBQQ__Opportunity__c FROM Contract WHERE Id = :contractId LIMIT 1];

        if(!contracts.isEmpty()) {
            return contracts[0].SBQQ__Opportunity__c;
        } else {
            return null;
        }
    }

    /**
     * To request a payment from a Contact for a specific Lease/Opportunity
     *
     * @param recordId Id of Contract/Lease record
     * @param contactId Id of Contact to send out email to
     *
     * @return True if successful request, else false
     */
    @AuraEnabled
    public static Boolean requestPayment(Id recordId, Id contactId) {
        Boolean isSuccess = false;
        Savepoint sp = Database.setSavepoint();

        try {
            InitialPaymentData initialPaymentData = getInitialPaymentsMap(recordId).get(contactId);

            if (initialPaymentData == null) {
                throw new TIPIException('Could not find initialPaymentData in Map for provided lease id: ' + recordId + ', contact id: ' + contactId);
            } else {
                List<asp04__Payment__c> aspPayments = new List<asp04__Payment__c>();

                // if first request for payment
                if (initialPaymentData.isFirstRequest) {
                    asp04__Payment__c aspPayment = createPaymentWithAmount(initialPaymentData);
                    aspPayment.asp04__Payment_Stage__c = Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION;
                    aspPayments.add(aspPayment);
                    insert aspPayments;

                    List<Tipi_Payments__c> tipiPaymentsToUpdate = new List<Tipi_Payments__c>();

                    if (initialPaymentData.deposit != null) {
                        initialPaymentData.deposit.Payment__c = aspPayments[0].Id;
                        initialPaymentData.deposit.Status__c = Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION;
                        tipiPaymentsToUpdate.add(initialPaymentData.deposit);
                    }

                    if (initialPaymentData.initialPayment != null) {
                        if(!initialPaymentData.isUpfrontCredit && initialPaymentData.reservation != null) {
                            initialPaymentData.initialPayment = drawDownReservationCredit(initialPaymentData);
                        } else {
                            initialPaymentData.initialPayment.Status__c = Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION;
                        }
                        initialPaymentData.initialPayment.Payment__c = aspPayments[0].Id;
                        tipiPaymentsToUpdate.add(initialPaymentData.initialPayment);
                    }
                    update tipiPaymentsToUpdate;
                }

                // if resend payment request
                Set<Id> payments = new Set<Id>();
                if (initialPaymentData.deposit != null) {
                    payments.add(initialPaymentData.deposit.Payment__c);
                }
                if (initialPaymentData.initialPayment != null) {
                    payments.add(initialPaymentData.initialPayment.Payment__c);
                }
                aspPayments = [SELECT Id FROM asp04__Payment__c WHERE Id IN :payments];

                // send out email notification for all asp payments
                for(asp04__Payment__c aspPayment : aspPayments) {
                    EmailHandler.sendEmailToContact(new Map<Contact, asp04__Payment__c>{ initialPaymentData.contact => aspPayment }, 'Initial_Payment_URL');
                }
                isSuccess = true;
            }
        } catch (Exception e) {
            isSuccess = false;
            Database.rollback(sp);
            CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, e);
            CustomLogger.save();
        }

        return isSuccess;
    }

    /**
     * To construct an asp04__Payment__c Asperato Payment record
     *
     * @param payee Wrapper containing Contact, deposit Tipi_Payments__c, initialPayment Tipi_Payments__c
     *
     * @return asp04__Payment__c record
     */
    private static asp04__Payment__c createPaymentWithAmount(InitialPaymentData payee) {
        asp04__Payment__c aspPayment = PaymentAuthorisationHelper.createPaymentRecordWithoutAmount(payee.contact);
        aspPayment.asp04__Amount__c = payee.totalAmount;

        aspPayment.Amount_of_Deposit__c                 = 0;
        aspPayment.Amount_of_Upfront_Payment__c         = 0;
        aspPayment.Amount_of_Rent_Payment__c            = 0;
        aspPayment.Amount_of_Rent_without_Concession__c = 0;
        aspPayment.Amount_of_Concession_Applied__c      = 0;
        aspPayment.Amount_of_Reservation__c             = 0;

        if(payee.deposit != null) {
            if(payee.deposit.Amount__c > 0 && payee.deposit.Status__c != Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER) {
                aspPayment.Amount_of_Deposit__c = payee.deposit.Amount__c;
            }
        }
        if(payee.initialPayment != null) {
            if(payee.initialPayment.Amount__c > 0 && payee.initialPayment.Status__c != Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER) {
                if(payee.isUpfrontCredit) {
                    // Upfront Payment
                    aspPayment.Amount_of_Upfront_Payment__c = payee.initialPayment.Amount__c;
                } else {
                    // First Rent Payment
                    aspPayment.Amount_of_Rent_Payment__c = payee.initialPayment.Amount__c;
                    aspPayment.Amount_of_Rent_without_Concession__c = payee.initialPayment.Amount__c + payee.initialPayment.Concession_Amount__c;
                    aspPayment.Amount_of_Concession_Applied__c = payee.initialPayment.Concession_Amount__c;
                }
            }
        }
        if(payee.reservation != null) {
            if(payee.reservation.Amount__c > 0 && payee.reservation.Total_Remaining_Balance__c > 0 && !payee.isUpfrontCredit) {
                aspPayment.Amount_of_Reservation__c = payee.reservation.Total_Remaining_Balance__c;
            }
        }

        return aspPayment;
    }

    /**
     * To fetch the amount of non-collected deposit/initial payments
     *
     * @param payee Wrapper containing Contact, deposit Tipi_Payments__c, initialPayment Tipi_Payments__c
     *
     * @return Decimal amount that the Contact has to pay upfront
     */
    private static Decimal getTotalAmount(InitialPaymentData payee) {
        Decimal totalAmount = 0;

        if(payee.deposit != null) {
            if(payee.deposit.Amount__c > 0 && payee.deposit.Status__c != Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER) {
                totalAmount += payee.deposit.Amount__c.setScale(2, RoundingMode.HALF_UP);
            }
        }
        if(payee.initialPayment != null) {
            if(payee.initialPayment.Amount__c > 0 && payee.initialPayment.Status__c != Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER) {
                totalAmount += payee.initialPayment.Amount__c.setScale(2, RoundingMode.HALF_UP);
            }
        }

        if(payee.reservation != null) {
            if(payee.reservation.Amount__c > 0 && payee.reservation.Total_Remaining_Balance__c > 0 && !payee.isUpfrontCredit) {
                totalAmount -= payee.reservation.Total_Remaining_Balance__c.setScale(2, RoundingMode.HALF_UP);
            }
        }

        if(totalAmount < 0) {
            totalAmount = 0;
        }

        return totalAmount;
    }

    /**
     * To fetch the if a payment request has been send out previously
     *
     * @param payee Wrapper containing Contact, deposit Tipi_Payments__c, initialPayment Tipi_Payments__c
     *
     * @return Boolean true, if neither deposit nor initialPayment Tipi_Payments__c have an Asperato Payment assigned
     */
    private static Boolean isFirstRequest(InitialPaymentData payee) {
        Boolean isFirstRequest = true;

        if(payee.deposit != null) {
            if (payee.deposit.Payment__c != null) {
                isFirstRequest = false;
            }
        }

        if(payee.initialPayment != null) {
            if(payee.initialPayment.Payment__c != null) {
                isFirstRequest = false;
            }
        }

        return isFirstRequest;
    }

    /**
     * To fetch the if a payment request has been send out previously
     *
     * @param payee Wrapper containing Contact, deposit Tipi_Payments__c, initialPayment Tipi_Payments__c
     *
     * @return Boolean true, if neither deposit nor initialPayment Tipi_Payments__c have an Asperato Payment assigned
     */
    private static Boolean isUpfrontCredit(InitialPaymentData payee) {
        Boolean isUpfrontCredit = true;

        if(payee.initialPayment != null) {
            if(payee.initialPayment.Credit_Payment_Type__c == Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT) {
                isUpfrontCredit = false;
            }
        }

        return isUpfrontCredit;
    }

    /**
     * To drawdown (incl. clone) a Tipi_Payments__c records against a reservation payment
     *
     * @param tipiPayment Tipi_Payments__c record to be drawn down
     * @param reservation Tipi_Payments__c record containing the property reservation payment
     *
     * @return Tipi_Payments__c record containing the drawdown Amount
     */
    private static Tipi_Payments__c drawDownReservationCredit(InitialPaymentData initialPaymentData) {

        Tipi_Payments__c tipiPayment = initialPaymentData.initialPayment;

        if (initialPaymentData.reservation.Total_Remaining_Balance__c > 0 && tipiPayment.Tipi_Payment__c == null) {

            String creditMessage = Utils_Constants.PAYMENT_DRAW_DOWN_PART1 + initialPaymentData.reservation.Total_Remaining_Balance__c + Utils_Constants.PAYMENT_DRAW_DOWN_PART2;

            // if rent amount is less than credit balance
            if (tipiPayment.Amount__c <= initialPaymentData.reservation.Total_Remaining_Balance__c) {
                tipiPayment.Tipi_Payment__c = initialPaymentData.reservation.Id;
                tipiPayment.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
                tipiPayment.Calculation_Long__c += '; ' + creditMessage;
            }

            // if rent amount is larger than credit balance
            else {
                Tipi_Payments__c paymentClone = tipiPayment.clone(false, true, false, false);
                paymentClone.Tipi_Payment__c = initialPaymentData.reservation.Id;
                paymentClone.Amount__c = initialPaymentData.reservation.Total_Remaining_Balance__c;
                paymentClone.Amount_without_Concession__c = paymentClone.Amount__c;
                paymentClone.Concession_Amount__c = 0;
                paymentClone.Calculation_Long__c = creditMessage;
                paymentClone.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
                paymentClone.Original_Charge_Split__c = tipiPayment.Id;
                insert paymentClone;

                tipiPayment.Amount__c -= initialPaymentData.reservation.Total_Remaining_Balance__c;
                tipiPayment.Calculation_Long__c += Utils_Constants.PAYMENT_PARTIAL_DRAW_DOWN + initialPaymentData.reservation.Total_Remaining_Balance__c + ']';
                tipiPayment.Status__c = Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION;
            }
        }

        return tipiPayment;
    }
}