/**
* @Name:        OpportunityTriggerHandler
* @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
*
* @author:      Patrick Fischer
* @version:     1.0
* Change Log
*
* Date          author              Change Description
* -----------------------------------------------------------------------------------
* 06/06/2018    Patrick Fischer     Created Class
*/
public with sharing class OpportunityTriggerHandler extends TriggerHandler {

    protected override void beforeInsert() {
        OpportunityTriggerHelper.processBeforeInsert((List<Opportunity>) Trigger.new);
    }

    protected override void afterInsert() {
        OpportunityTriggerHelper.processAfterInsert((List<Opportunity>) Trigger.new);
    }

    protected override void beforeUpdate() {
        OpportunityTriggerHelper.processBeforeUpdate((List<Opportunity>) Trigger.new, (Map<Id, Opportunity>) Trigger.oldMap);
    }

    protected override void afterUpdate() {
        OpportunityTriggerHelper.processAfterUpdate((List<Opportunity>) Trigger.new, (Map<Id, Opportunity>) Trigger.oldMap);
    }
}