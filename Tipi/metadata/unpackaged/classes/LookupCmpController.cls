/**
    * @Name:        LookupCmpController
    * @Description: Controller for Lookup Lightning Component
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 20/04/2018    Eugene Konstantinov    Created Class
*/
public with sharing class LookupCmpController {
	/**
	 * Aura enabled method to search a specified SObject for a specific string
	 */
	@AuraEnabled
	public static Result[] lookup(String searchString, String sObjectAPIName, String userType, List<String> excludeIds) {
		// Sanitze the input
		String sanitizedSearchString    = String.escapeSingleQuotes(searchString);
		String sanitizedSObjectAPIName  = String.escapeSingleQuotes(sObjectAPIName);
		String sanitizedUserType        = String.escapeSingleQuotes(userType);
		String guestUsersLicenseName    = 'Guest';
		Set<String> excludeIdsSet       = new Set<String>(excludeIds);
		Set<String> internalUserTypes   = new Set<String>{'Standard'};
		Set<String> communityUserTypes  = new Set<String>{'PowerPartner',
														  'CSPLitePortal',
														  'CustomerSuccess',
														  'PowerCustomerSuccess'};

		List<Result> results = new List<Result>();

		String searchQuery = '';

		// Build our SOSL query
		if (sanitizedUserType == null || sanitizedUserType == '') {
			searchQuery = 'FIND \'' + sanitizedSearchString + '*\' IN ALL FIELDS RETURNING ' + sanitizedSObjectAPIName +
					'(id, name WHERE IsActive = true AND Id NOT IN :excludeIdsSet) LIMIT 50';
		} else {
			if (sanitizedUserType == 'Internal') {
				searchQuery = 'FIND \'' + sanitizedSearchString + '*\' IN ALL FIELDS RETURNING ' + sanitizedSObjectAPIName +
						'(id, name WHERE IsActive = true ' +
						'AND Profile.UserType IN :internalUserTypes ' +
						'AND Id NOT IN :excludeIdsSet) LIMIT 50';
			} else if (sanitizedUserType == 'Community'){
				searchQuery = 'FIND \'' + sanitizedSearchString + '*\' IN ALL FIELDS RETURNING ' + sanitizedSObjectAPIName +
						'(id, name WHERE IsActive = true ' +
						'AND Profile.UserType IN :communityUserTypes ' +
						'AND Id NOT IN :excludeIdsSet) LIMIT 50';
			}
		}

		// Execute the Query
		List<List<SObject>> searchList = search.query(searchQuery);

		// Create a list of matches to return
		for (SObject so : searchList[0])
			{
				results.add(new Result((String)so.get('Name'), so.Id));
			}

		return results;
	}

	@AuraEnabled
	public static Result lookupById (String recordId, String sObjectAPIName) {
		// Sanitze the input
		String sanitizedId              = String.escapeSingleQuotes(recordId);
		String sanitizedSObjectAPIName  = String.escapeSingleQuotes(sObjectAPIName);
		Result r;

		// Build our SOSL query
		String searchQuery = 'SELECT Id, Name FROM ' + sanitizedSObjectAPIName + ' WHERE Id = :sanitizedId LIMIT 1';

		// Execute the Query
		List<SObject> searchList = database.query(searchQuery);

		// Create a list of matches to return
		if (searchList != null && !searchList.isEmpty()) {
			SObject so  = searchList[0];
			r           = new Result((String)so.get('Name'), so.Id);
		}

		return r;
	}

	/**
	 * Inner class to wrap up an SObject Label and its Id
	 */
	public class Result
	{
		@AuraEnabled public String  SObjectLabel {get; set;}
		@AuraEnabled public Id      SObjectId {get; set;}

		public Result(String sObjectLabel, Id sObjectId)
			{
				this.SObjectLabel   = sObjectLabel;
				this.SObjectId      = sObjectId;
			}
	}
}