/**
 * @Name:        UserTriggerHandler
 * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 03/08/2018    Patrick Fischer    Created Class
 */
public with sharing class UserTriggerHandler extends TriggerHandler {

    protected override void afterInsert() { 
        UserTriggerHelper.processAfterInsert((Map<Id, User>) Trigger.newMap);
    }

    protected override void afterUpdate() {
        UserTriggerHelper.processAfterUpdate((Map<Id, User>) Trigger.oldMap, (Map<Id, User>) Trigger.newMap);
    }

}