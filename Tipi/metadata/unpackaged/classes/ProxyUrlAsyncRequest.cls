/**
    * @Name:        ProxyUrlAsyncRequest
    * @Description: Class to create Queueable callout for getting proxy url
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 12/04/2018    Eugene Konstantinov    Created Class
*/
public class ProxyUrlAsyncRequest implements Queueable, Database.AllowsCallouts {
	private static final String CLASS_NAME = 'ProxyUrlAsyncRequest';
	private static final String SUCCESS_RESPONSE = 'OK';
	private List<ProxyUrlWrapper> proxyUrlWrapperList;

	public ProxyUrlAsyncRequest(List<ProxyUrlWrapper> proxyUrlWrapperList) {
		this.proxyUrlWrapperList = proxyUrlWrapperList;
	}

	public void execute(QueueableContext context) {
		String host = URL.getSalesforceBaseUrl().toExternalForm();
		HTTPResponse response = sendRequest(
				'POST',
				Label.ProxyURL,
				//'https://build-link-app.herokuapp.com',
				//'https://some-poc.herokuapp.com/url',
				new Map<String, String>{'Content-Type'=>'application/json; charset=utf-8'},
				System.JSON.serialize(this.proxyUrlWrapperList)
		);
		if (response != null && response.getStatus() == SUCCESS_RESPONSE) {
			String responseString = response.getBody();
			List<ProxyUrlWrapper.Response> parsedResponses = (List<ProxyUrlWrapper.Response>)System.JSON.deserialize(responseString, List<ProxyUrlWrapper.Response>.class);
			System.debug(parsedResponses);
			if (parsedResponses != null && !parsedResponses.isEmpty()) {
				upsertUnitFiles(parsedResponses);
			}
		}
	}

	/**
	 * Send Request Method
	 * @param  method  string value
	 * @param  url  string value of endpoint
	 * @param  additionalHeader  map of additional header information
	 * @param  body  string value
	 *
	 * @returns HTTPResponse response from endpoint
	 */
	private HTTPResponse sendRequest(String method, String url, Map<String, String> additionalHeader, String body){
		try {
			HttpRequest Request = new HttpRequest();
			Request.setMethod(method);
			Request.setEndpoint(url);
			Request.setTimeout(20000);

			for (String key : additionalHeader.keySet()) {
				Request.setHeader(key, (String)additionalHeader.get(key));
			}

			if (!String.isEmpty(body)) {
				Request.setBody(body);
			}
			System.debug(body);

			Http http = new Http();
			HTTPResponse response = http.send(Request);
			System.debug(response.getBody());
			return response;
		}
		catch (CalloutException ex) {
			CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'Callout Exception:' + CLASS_NAME + ex.getMessage());
		}
		catch (Exception ex) {
			CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'Exception:' + CLASS_NAME + ex.getMessage());
		}
		finally {
			CustomLogger.save();
		}
		return null;
	}

	/**
	 * Method to Update UnitFile records with Proxy URL
	 * @param  parsedResponses  list of ProxyUrlWrapper.Response records with answers from external system
	 */
	private static void upsertUnitFiles(List<ProxyUrlWrapper.Response> parsedResponses) {
		List<Unit_File__c> unitFilesWithLinks = new List<Unit_File__c>();
		for (ProxyUrlWrapper.Response response : parsedResponses) {
			Unit_File__c uFile = new Unit_File__c();
			uFile.Id = response.recordId;
			uFile.Proxy_URL__c = response.url.replaceAll(' ','%20');
			unitFilesWithLinks.add(uFile);
		}
		if (unitFilesWithLinks != null && !unitFilesWithLinks.isEmpty()) {
			try{
				Database.upsert(unitFilesWithLinks);
			}
			catch(DMLException e){
				CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'DML Exception:' + CLASS_NAME + e.getMessage());
			}
			catch(Exception e){
				CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'Exception:' + CLASS_NAME + e.getMessage());
			}
			finally{
				CustomLogger.save();
			}
		}
	}
}