/**
    * @Name:        SelectItem
    * @Description: Controller class for SelectItem Lightning Component
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 29/03/2018    Eugene Konstantinov    Created Class
*/
public class SelectItem {
    
    @AuraEnabled public String  label  {get; set;}
    @AuraEnabled public String  value  {get; set;}
    @AuraEnabled public Boolean  selected  {get; set;}
    
    public SelectItem( String value, String label, Boolean selected) {
        this(label,value);
        this.selected = selected;
    }
    
    public SelectItem( String value, String label) {
        this.label = label;
        this.value = value;
        this.selected = false;
    }
}