/**
 * @Name:        SendInitialPaymentControllerTest
 * @Description: Test class of SendInitialPaymentController for c:sendInitialPayment LEX component
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 08/06/2018    Patrick Fischer    Created Class
 */
@IsTest
public with sharing class SendInitialPaymentControllerTest {

    /**
     * Method tested: getInitialPayments()
     * Expected result: initialises LEX component successfully (with Rent Only initial payment)
     */
    private static testMethod void getInitialPaymentsSuccess() {
        Id leaseId = [SELECT Id FROM Contract LIMIT 1].Id;

        Test.startTest();
        List<SendInitialPaymentController.InitialPaymentData> initialPaymentData = SendInitialPaymentController.getInitialPayments(leaseId);
        Test.stopTest();

        System.assert(initialPaymentData[0].isFirstRequest, 'Expecting first request, i.e. no asperato payments assigned');
        System.assert(!initialPaymentData[0].isUpfrontCredit, 'Expecting Rent Only payment (no upfront credit amount)');
        System.assertEquals(3600, initialPaymentData[0].totalAmount, 'Expecting initial payment of 3600 (Deposit + First Payment - Reservation)');
    }

    /**
     * Method tested: getInitialPayments()
     * Expected result: initialises LEX component successfully (with Credit initial payment)
     */
    private static testMethod void getInitialPaymentsCreditSuccess() {
        List<Contact> contacts = [SELECT Id FROM Contact];
        List<Opportunity> opportunities = [SELECT Id FROM Opportunity];
        List<Development__c> developments = [SELECT Id FROM Development__c];

        // add credit payment
        Tipi_Payments__c tipiPaymentsCredit = TestFactoryPayment.getTipiPayments(10000, contacts[0].Id,
                Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT, developments[0].Id, opportunities[0], null, Date.newInstance(2014, 1, 1),
                Utils_Constants.AWAITING_SUBMISSION_STATUS, 'Rent - AST', 1, false)[0];
        tipiPaymentsCredit.Nth_Month_Payment__c = 1;
        insert tipiPaymentsCredit;

        Id leaseId = [SELECT Id FROM Contract LIMIT 1].Id;

        Test.startTest();
        List<SendInitialPaymentController.InitialPaymentData> initialPaymentData = SendInitialPaymentController.getInitialPayments(leaseId);
        Test.stopTest();

        System.assert(initialPaymentData[0].isFirstRequest, 'Expecting first request, i.e. no asperato payments assigned');
        System.assert(initialPaymentData[0].isUpfrontCredit, 'Expecting upfront Credit payment');
        System.assertEquals(11000, initialPaymentData[0].totalAmount, 'Expecting initial payment of 11000 (reservation amount is excluded)');
    }


    /**
     * Method tested: requestPayment()
     * Expected result: saves LEX component successfully & creates Tipi_Payment__c drawdown record
     */
    private static testMethod void requestPaymentUpfrontSuccess() {
        List<Contact> contacts = [SELECT Id FROM Contact];
        List<Opportunity> opportunities = [SELECT Id FROM Opportunity];
        List<Development__c> developments = [SELECT Id FROM Development__c];

        // add credit payment
        Tipi_Payments__c tipiPaymentsCredit = TestFactoryPayment.getTipiPayments(10000, contacts[0].Id,
                Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT, developments[0].Id, opportunities[0], null, Date.newInstance(2014, 1, 1),
                Utils_Constants.AWAITING_SUBMISSION_STATUS, 'Rent - AST', 1, false)[0];
        tipiPaymentsCredit.Nth_Month_Payment__c = 1;
        insert tipiPaymentsCredit;

        Id leaseId = [SELECT Id FROM Contract LIMIT 1].Id;
        Id contactId = [SELECT Id FROM Contact LIMIT 1].Id;

        Test.startTest();
        Boolean isSuccess = SendInitialPaymentController.requestPayment(leaseId, contactId);
        List<SendInitialPaymentController.InitialPaymentData> initialPaymentData = SendInitialPaymentController.getInitialPayments(leaseId);
        Test.stopTest();

        System.assert(isSuccess, 'Expecting successful requestPayment operation');
        System.assert(!initialPaymentData[0].isFirstRequest, 'Expecting subsequent request, i.e. asperato payments have been assigned');
        System.assert(initialPaymentData[0].isUpfrontCredit, 'Expecting Upfront credit payment');
        System.assertEquals(11000, initialPaymentData[0].totalAmount, 'Expecting initial payment of 11000 (reservation amount is excluded)');
    }

    /**
     * Method tested: requestPayment()
     * Expected result: saves LEX component successfully & creates Tipi_Payment__c drawdown record
     */
    private static testMethod void requestPaymentSuccess() {

        Id leaseId = [SELECT Id FROM Contract LIMIT 1].Id;
        Id contactId = [SELECT Id FROM Contact LIMIT 1].Id;

        Test.startTest();
        Boolean isSuccess = SendInitialPaymentController.requestPayment(leaseId, contactId);
        List<SendInitialPaymentController.InitialPaymentData> initialPaymentData = SendInitialPaymentController.getInitialPayments(leaseId);
        Test.stopTest();

        System.assert(isSuccess, 'Expecting successful requestPayment operation');
        System.assert(!initialPaymentData[0].isFirstRequest, 'Expecting subsequent request, i.e. asperato payments have been assigned');
        System.assert(!initialPaymentData[0].isUpfrontCredit, 'Expecting Rent Only payment (no upfront credit amount)');
        System.assertEquals(3600, initialPaymentData[0].totalAmount, 'Expecting initial payment of 3600 (Deposit + First Payment - Reservation)');
    }

    /**
     * Method tested: requestPayment()
     * Expected result: saves LEX component successfully & assigns Tipi_Payment__c drawdown records to reservation
     */
    private static testMethod void requestPaymentHighReservationFeeFailure() {
        Tipi_Payments__c reservation = [SELECT Id, Amount__c FROM Tipi_Payments__c WHERE Type__c = :Utils_Constants.PAYMENT_TYPE_RESERVATION LIMIT 1];
        reservation.Amount__c = 20000; // to mock a reservation payment larger than rent & deposit payments
        update reservation;

        Id leaseId = [SELECT Id FROM Contract LIMIT 1].Id;
        Id contactId = [SELECT Id FROM Contact LIMIT 1].Id;

        Test.startTest();
        Boolean isSuccess = SendInitialPaymentController.requestPayment(leaseId, contactId);
        List<SendInitialPaymentController.InitialPaymentData> initialPaymentData = SendInitialPaymentController.getInitialPayments(leaseId);
        Test.stopTest();

        System.assert(isSuccess, 'Expecting failure requestPayment operation due to high amount');
        System.assert(!initialPaymentData[0].isFirstRequest, 'Expecting subsequent request, i.e. asperato payments have been assigned');
        System.assert(!initialPaymentData[0].isUpfrontCredit, 'Expecting Rent Only payment (no upfront credit amount)');
        System.assertEquals(0, initialPaymentData[0].totalAmount, 'Expecting initial payment of 0 due to high reservation payment');
    }

    /**
     * Method tested: requestPayment()
     * Expected result: saves LEX component successfully and no reservation credit handled
     */
    private static testMethod void requestPaymentNoReservationSuccess() {
        Tipi_Payments__c reservation = [SELECT Id, Amount__c FROM Tipi_Payments__c WHERE Type__c = :Utils_Constants.PAYMENT_TYPE_RESERVATION LIMIT 1];
        delete reservation;

        Id leaseId = [SELECT Id FROM Contract LIMIT 1].Id;
        Id contactId = [SELECT Id FROM Contact LIMIT 1].Id;

        Test.startTest();
        Boolean isSuccess = SendInitialPaymentController.requestPayment(leaseId, contactId);
        List<SendInitialPaymentController.InitialPaymentData> initialPaymentData = SendInitialPaymentController.getInitialPayments(leaseId);
        Test.stopTest();

        System.assert(isSuccess, 'Expecting successful requestPayment operation');
        System.assert(!initialPaymentData[0].isFirstRequest, 'Expecting subsequent request, i.e. asperato payments have been assigned');
        System.assert(!initialPaymentData[0].isUpfrontCredit, 'Expecting Rent Only payment (no upfront credit amount)');
        System.assertEquals(4000, initialPaymentData[0].totalAmount, 'Expecting initial payment of 4000');
    }

    /**
     * Method tested: requestPayment()
     * Expected result: Exception thrown and handled
     */
    private static testMethod void requestPaymentFailure() {
        Test.startTest();
        Boolean isSuccess = SendInitialPaymentController.requestPayment(null, null);
        Test.stopTest();

        System.assert(!isSuccess, 'Expecting failed requestPayment operation');
    }

    /**
     * Helper method to setup testdata including Developments, Buildings, Units, Accounts, Opportunities, Products, Quotes
     */
    @TestSetup
    private static void testSetup() {
        TestFactory.createSObject(new Global_Application_Settings__c(), true);

        // Testdata Items
        List<Development__c> developments = TestFactoryPayment.getDevelopments(1, true);
        List<Building__c> buildings = TestFactoryPayment.getBuildings(developments[0], 1, true);
        List<Unit__c> units = TestFactoryPayment.getUnits(buildings[0], 1, true);
        List<Pricebook2> pricebooks = TestFactoryPayment.getPricebooks(1, true);
        List<Product2> products = new List<Product2>();
        products.addAll(TestFactoryPayment.getProducts('Unit', 3000, 1, false));
        products.addAll(TestFactoryPayment.getProducts('Storage', 200, 4, false));
        products.addAll(TestFactoryPayment.getProducts('Parking', 400, 2, false));
        insert products;
        List<PricebookEntry> pricebookEntries = TestFactoryPayment.getPricebookEntries(products, true);
        List<Rentable_Item__c> rentableItems = TestFactoryPayment.getRentableItems(products, buildings[0], true);

        // Transactional Items
        List<Account> accounts = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 11, true);
        List<Opportunity> opportunities = TestFactoryPayment.getOpportunities(accounts[0], contacts, 1, true);
        List<SBQQ__Quote__c> quotes = TestFactoryPayment.getQuotes(accounts[0], contacts[0], opportunities[0], 1, true);
        List<SBQQ__QuoteLine__c> quoteLines = TestFactoryPayment.getQuoteLines(quotes[0], products, true);
        List<Contract> contracts = TestFactoryPayment.getContracts(accounts[0], quotes[0], opportunities[0], units[0], 1, true);
        List<asp04__Authorisation__c> authorisations = TestFactoryPayment.getAuthorisations(contacts, true);

        List<Tipi_Payments__c> tipiPayments = new List<Tipi_Payments__c>();
        // add 12 rent payments
        for(Integer i = 0; i < 12; i++) {
            Tipi_Payments__c tipiRentPayment = TestFactoryPayment.getTipiPayments(3000, contacts[0].Id, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
                    developments[0].Id, opportunities[0], null,  Date.newInstance(2014, 1, 1).addMonths(i),
                    Utils_Constants.AWAITING_SUBMISSION_STATUS, 'Rent - AST', 1, false)[0];
            tipiRentPayment.Nth_Month_Payment__c = i + 1;
            tipiPayments.add(tipiRentPayment);
        }

        // add deposit payment
        Tipi_Payments__c tipiPaymentsDeposit = TestFactoryPayment.getTipiPayments(1000, contacts[0].Id,
                '', developments[0].Id, opportunities[0], null, Date.newInstance(2013, 12, 1),
                Utils_Constants.AWAITING_SUBMISSION_STATUS, 'Deposit', 1, false)[0];
        tipiPaymentsDeposit.Nth_Month_Payment__c = 1;
        tipiPayments.add(tipiPaymentsDeposit);

        // add reservation payment
        Tipi_Payments__c tipiPaymentsReservation = TestFactoryPayment.getTipiPayments(400, contacts[0].Id,
                '', developments[0].Id, opportunities[0], null, Date.newInstance(2013, 12, 1),
                Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, Utils_Constants.PAYMENT_TYPE_RESERVATION, 1, false)[0];
        tipiPayments.add(tipiPaymentsReservation);

        insert tipiPayments;
    }
}