/**
 * @Name:        PaymentSummaryCmpControllerTest
 * @Description: Test Class for Payment Summary Lightning Component Controller class
 *
 * @author:      Eugene Konstantinov
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 08/06/2018    Eugene Konstantinov    Created Class
 * 16/06/2018    Patrick Fischer        Refactored Test Class, Improved Code Coverage due to changes of PaymentSummaryCmpController
 */
@IsTest
public class PaymentSummaryCmpControllerTest {

    /**
     * Method tested: getPaymentsForContact()
     * Expected result: initialises LEX component successfully populating all rows
     */
    private static testMethod void getPaymentsForContactSuccess() {
        List<Contact> contacts = [ SELECT Id FROM Contact ];
        System.assertEquals(1, contacts.size());
        Test.startTest();
        List<PaymentSummaryCmpController.PaymentRowWrapper> resultList = PaymentSummaryCmpController.getPaymentsForContact(String.valueOf(contacts[0].Id));
        Test.stopTest();

        System.assertEquals(13, resultList.size());
    }

    /**
     * Method tested: getPaymentsForContacts()
     * Expected result: initialises LEX component successfully populating all rows
     */
    private static testMethod void getPaymentsForContactsSuccess() {
        List<Contact> contacts = [ SELECT Id FROM Contact ];
        System.assertEquals(1, contacts.size());
        Test.startTest();
        List<PaymentSummaryCmpController.PaymentRowWrapper> resultList = PaymentSummaryCmpController.getPaymentsForContacts('[ "' + contacts[0].Id + '" ]');
        Test.stopTest();

        System.assertEquals(13, resultList.size());
    }

    /**
     * Method tested: getPaymentsForContacts()
     * Expected result: Exception thrown and handled
     */
    private static testMethod void getPaymentsForContactsFailure() {
        Test.startTest();
        List<PaymentSummaryCmpController.PaymentRowWrapper> resultList = PaymentSummaryCmpController.getPaymentsForContacts(null);
        Test.stopTest();

        System.assert(resultList.isEmpty());
    }

    /**
     * Method tested: getPaymentsForContact()
     * Expected result: Empty list returned as no records found for contact
     */
    private static testMethod void getPaymentsForContactAsCommunityUserFailure() {
        User portalUser = TestFactoryPayment.createPortallUser();
        System.runAs(portalUser) {

            List<PaymentSummaryCmpController.PaymentRowWrapper> resultList = PaymentSummaryCmpController.getPaymentsForContact(null);

            System.assert(resultList.isEmpty(), 'Expecting no rows to be returned');
        }
    }

    /**
     * Method tested: getPaymentsForContact()
     * Expected result: Empty list returned as no records found for contact
     */
    private static testMethod void getPaymentsForContactFailure() {
        Test.startTest();
        List<PaymentSummaryCmpController.PaymentRowWrapper> resultList = PaymentSummaryCmpController.getPaymentsForContact(null);
        Test.stopTest();

        System.assert(resultList.isEmpty(), 'Expecting no rows to be returned');
    }

    /**
     * Method tested: compareTo()
     * Expected result: PaymentRowWrapper compared successfully using custom implementation of Comparable
     */
    private static testMethod void compareToSuccess() {
        PaymentSummaryCmpController.PaymentRowWrapper wrapper1 = new PaymentSummaryCmpController.PaymentRowWrapper(
                null, null, null, null, Datetime.newInstance(2014, 1, 20, 7, 0, 0), Date.newInstance(2014, 1, 20), null, null, null, 0, 0);
        PaymentSummaryCmpController.PaymentRowWrapper wrapper2 = new PaymentSummaryCmpController.PaymentRowWrapper(
                null, null, null, null, Datetime.newInstance(2014, 1, 20, 7, 0, 0), Date.newInstance(2014, 1, 20), null, null, null, 0, 0);

        Test.startTest();

        wrapper1.paymentDate = Date.newInstance(2014, 1, 21);
        System.assertEquals(1, wrapper1.compareTo(wrapper2), 'Expecting correct date comparison');
        wrapper1.paymentDate = Date.newInstance(2014, 1, 19);
        System.assertEquals(-1, wrapper1.compareTo(wrapper2), 'Expecting correct date comparison');
        wrapper1.paymentDate = Date.newInstance(2014, 1, 20);
        wrapper2.charge = 5;
        System.assertEquals(1, wrapper1.compareTo(wrapper2), 'Expecting correct date comparison');
        wrapper2.charge = 0;
        wrapper2.payment = 5;
        System.assertEquals(-1, wrapper1.compareTo(wrapper2), 'Expecting correct date comparison');
        wrapper2.payment = 0;
        System.assertEquals(0, wrapper1.compareTo(wrapper2), 'Expecting correct date comparison');
        wrapper1.createdDateTime = Datetime.newInstance(2014, 1, 20, 8, 0, 0);
        System.assertEquals(1, wrapper1.compareTo(wrapper2), 'Expecting correct date comparison');
        wrapper1.createdDateTime = Datetime.newInstance(2014, 1, 20, 6, 0, 0);
        System.assertEquals(-1, wrapper1.compareTo(wrapper2), 'Expecting correct date comparison');

        Test.stopTest(); 
    }

    /** Helper Methods for test data setup **/
    @TestSetup
    private static void testSetup() {
        Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);
        gas.Run_Process_Builder__c = false;
        update gas;
        List<Account> accounts = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.addContacts(accounts, 1, true);
        Opportunity opportunity = TestFactoryPayment.getOpportunity(accounts[0], contacts[0], true);
        List<asp04__Authorisation__c> authorisations = TestFactoryPayment.getAuthorisations(contacts, true);
        List<Development__c> developments = [ SELECT Id FROM Development__c ];
        System.assertEquals(1, developments.size());

        asp04__Payment__c aspPayment = new asp04__Payment__c(
                Contact__c = contacts[0].Id,
                asp04__Amount__c = 1000,
                asp04__Payment_Date__c = Date.today(),
                asp04__Payment_Stage__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER
        );
        insert aspPayment;

        asp04__Refund__c refund = new asp04__Refund__c(
                asp04__Amount__c = 100,
                asp04__Payment_Date__c = Date.today(),
                asp04__Payment__c = aspPayment.Id,
                asp04__Refund_Stage__c = 'Refunded to customer'
        );
        insert refund;

        List<Tipi_Payments__c> tipiPayments = new List<Tipi_Payments__c>();

        // add refund payments
        Tipi_Payments__c tipiRefundPayment = TestFactoryPayment.getTipiPayments(100, contacts[0].Id, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
                developments[0].Id, opportunity, null,  Date.today().toStartOfMonth(),
                Utils_Constants.AWAITING_SUBMISSION_STATUS, 'Rent - AST', 1, false)[0];
        tipiRefundPayment.RecordTypeId = Schema.SObjectType.Tipi_Payments__c.getRecordTypeInfosByName().get(Utils_Constants.PAYMENT_RECORDTYPE_REFUND).getRecordTypeId();
        tipiPayments.add(tipiRefundPayment);

        // add 12 rent payments (1 in the past; 11 in the future)
        for(Integer i = 0; i < 12; i++) {
            tipiPayments.add(TestFactoryPayment.getTipiPayments(3000, contacts[0].Id, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
                    developments[0].Id, opportunity, null,  Date.today().toStartOfMonth().addMonths(i),
                    Utils_Constants.AWAITING_SUBMISSION_STATUS, 'Rent - AST', 1, false)[0]);
        }

        // add parking payment
        Tipi_Payments__c tipiPaymentsParking = TestFactoryPayment.getTipiPayments(300, contacts[0].Id, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
                developments[0].Id, opportunity, null,  Date.today().toStartOfMonth(),
                Utils_Constants.AWAITING_SUBMISSION_STATUS, Utils_Constants.PAYMENT_TYPE_PARKING, 1, false)[0];
        tipiPaymentsParking.Concession_Amount__c = 10;
        tipiPaymentsParking.Amount__c = 290;
        tipiPayments.add(tipiPaymentsParking);

        // add storage payment
        Tipi_Payments__c tipiPaymentsStorage = TestFactoryPayment.getTipiPayments(300, contacts[0].Id, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
                developments[0].Id, opportunity, null,  Date.today().toStartOfMonth(),
                Utils_Constants.AWAITING_SUBMISSION_STATUS, Utils_Constants.PAYMENT_TYPE_STORAGE, 1, false)[0];
        tipiPayments.add(tipiPaymentsStorage);

        // add guest parking payment
        Tipi_Payments__c tipiPaymentsGuestParking = TestFactoryPayment.getTipiPayments(300, contacts[0].Id, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
                developments[0].Id, opportunity, null,  Date.today().toStartOfMonth(),
                Utils_Constants.AWAITING_SUBMISSION_STATUS, 'Guest Parking', 1, false)[0];
        tipiPayments.add(tipiPaymentsGuestParking);

        // add credit payment
        Tipi_Payments__c tipiPaymentsCredit = TestFactoryPayment.getTipiPayments(10000, contacts[0].Id,
                Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT, developments[0].Id, opportunity, null, Date.newInstance(2014, 1, 1),
                Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, 'Rent - AST', 1, false)[0];
        tipiPayments.add(tipiPaymentsCredit);

        // add tipi BACS payment
        Tipi_Payments__c tipiBACSPayment = TestFactoryPayment.getTipiPayments(400, contacts[0].Id,
                Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT, developments[0].Id, opportunity, null, Date.newInstance(2013, 12, 1),
                Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, 'Rent - AST', 1, false)[0];
        tipiBACSPayment.Payment_Gateway__c = Utils_Constants.PAYMENT_GATEWAY_OTHER;
        tipiPayments.add(tipiBACSPayment);

        // add deposit payment
        Tipi_Payments__c tipiDepositPayment = TestFactoryPayment.getTipiPayments(1000, contacts[0].Id,
                '', developments[0].Id, opportunity, null, Date.newInstance(2013, 12, 1),
                Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, Utils_Constants.PAYMENT_TYPE_DEPOSIT, 1, false)[0];
        tipiDepositPayment.Type__c = Utils_Constants.PAYMENT_TYPE_DEPOSIT;
        tipiPayments.add(tipiDepositPayment);

        // add reservation payment
        Tipi_Payments__c tipiPaymentsReservation = TestFactoryPayment.getTipiPayments(400, contacts[0].Id,
                '', developments[0].Id, opportunity, null, Date.newInstance(2013, 12, 1),
                Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, Utils_Constants.PAYMENT_TYPE_RESERVATION, 1, false)[0];
        insert tipiPaymentsReservation;

        // add reservation draw down
        Tipi_Payments__c tipiPaymentsReservationDD = TestFactoryPayment.getTipiPayments(400, contacts[0].Id, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
                developments[0].Id, opportunity, null,  Date.today().toStartOfMonth(),
                Utils_Constants.AWAITING_SUBMISSION_STATUS, 'Rent - AST', 1, false)[0];
        tipiPaymentsReservationDD.Tipi_Payment__c = tipiPaymentsReservation.Id;
        tipiPayments.add(tipiPaymentsReservationDD);

        insert tipiPayments;
    }
}