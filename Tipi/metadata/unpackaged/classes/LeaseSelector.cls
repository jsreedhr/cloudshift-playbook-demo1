/**
    * @Name:        LeaseSelector
    * @Description: Class for fetching data stored in salesforce.
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 29/05/2018    Eugene Konstantinov    Created Class
    * 01/06/2018    Eugene Konstantinov    Added getLeaseRecords method
*/
public with sharing class LeaseSelector extends Selector {
    public static final String objectAPIName = Contract.SobjectType.getDescribe().getName();

    /**
    * Method to fetch data stored in salesforce
    * @param    Set<Id>       Contract records Id
    *           Set<String>   fields is a set of Strings that determines which fields are queried of the Booked Social Space object
    * @returns  List<SObject> a list of Renable Item objects that are treated and stored as a generic type
    **/
    public List<SObject> getLeaseRecords(Set<Id> contractIds, Set<String> fields) {
        Set<Id> cIds = contractIds;
        if (cIds == null){return new List<SObject>();}
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Id IN :cIds LIMIT 10000';
        return Database.query(query);
    }
}