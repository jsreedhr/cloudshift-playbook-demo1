/**
 * @Name:        UserTriggerTest
 * @Description: Testclass for UserTrigger Handler and Helper methods
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 03/08/2018    Patrick Fischer    Created Class
 */
@IsTest
public with sharing class UserTriggerTest {

    private static testMethod void setActiveCommunityUserFlagInsertSuccess() {
        List<Account> accounts = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 1, true);
        TestFactory.createSObject(new Global_Application_Settings__c(), true);

        Id communityUserProfileId = [SELECT Id FROM Profile WHERE Name LIKE '%community%' LIMIT 1].Id;
        User portalUser = new User(
                Alias = 'test33',
                Email = 'test33@noemail.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'Testing',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId = communityUserProfileId,
                Country = 'United States',
                IsActive = true,
                ContactId = contacts[0].Id,
                TimeZoneSidKey = 'America/Los_Angeles',
                Username = 'tester@noemail.com' + Math.random()
        );

        Test.startTest();
        insert portalUser;
        Test.stopTest();

        contacts = [SELECT Id, Has_Active_Community_User__c FROM Contact WHERE Id IN :contacts];
        System.assert(contacts[0].Has_Active_Community_User__c, 'Expecting checkbox to be true.');
    }

    private static testMethod void setActiveCommunityUserFlagUpdateSuccess() {
        List<Account> accounts = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 1, true);
        TestFactory.createSObject(new Global_Application_Settings__c(), true);

        Id communityUserProfileId = [SELECT Id FROM Profile WHERE Name LIKE '%community%' LIMIT 1].Id;
        User portalUser = new User(
                Alias = 'test33',
                Email = 'test33@noemail.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'Testing',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId = communityUserProfileId,
                Country = 'United States',
                IsActive = false,
                ContactId = contacts[0].Id,
                TimeZoneSidKey = 'America/Los_Angeles',
                Username = 'tester@noemail.com' + Math.random()
        );
        insert portalUser;

        Test.startTest();
        UserTriggerHelper.processAfterUpdate(new Map<Id, User>(), new Map<Id, User> { portalUser.Id => portalUser });
        Test.stopTest();

        contacts = [SELECT Id, Has_Active_Community_User__c FROM Contact WHERE Id IN :contacts];
        System.assert(!contacts[0].Has_Active_Community_User__c, 'Expecting checkbox to be false.');
    }
}