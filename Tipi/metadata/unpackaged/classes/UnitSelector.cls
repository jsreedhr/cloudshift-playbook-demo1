/**
    * @Name:        UnitSelector
    * @Description: Class for fetching data stored in salesforce.
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 13/04/2018    Eugene Konstantinov    Created Class
*/
public with sharing class UnitSelector extends Selector{
	public static final String objectAPIName = Unit__c.SobjectType.getDescribe().getName();
	/**
	* Method to fetch data stored in salesforce
	* @param    Set<Id>       recordIds is a set of Units ids
	*           Set<String>   fields is a set of Strings that determines which fields are queried of the Unit object
	* @returns  List<SObject> a list of Units objects that are treated and stored as a generic type
	**/
	public List<SObject> getRecordsByIds(Set<Id> recordIds, Set<String> fields) {
		if (recordIds.isEmpty()){return new List<SObject>();}
		String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Id in ' + ConstructInClauseString(recordIds);
		return Database.query(query);
	}
}