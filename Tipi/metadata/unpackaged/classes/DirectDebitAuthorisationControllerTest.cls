/**
    * @Name:        DirectDebitAuthorisationControllerTest
    * @Description: Test class for DirectDebitAuthorisationController
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 15/05/2018    Andrey Korobovich    Created Class
*/

@isTest
public with sharing class DirectDebitAuthorisationControllerTest {
	@testSetup
	public static void generateTestData() {
		List<Account> accs = TestFactoryPayment.addAccounts(1, true);
		List<Contact> conts = TestFactoryPayment.addContacts(accs, 1, true);
	}

	@isTest
	public static void directDebitAuthorizationTest() {
		Contact c = [SELECT Id FROM Contact LIMIT 1];
		DirectDebitAuthorisationController.directDebitAuthorization(c.Id);
		asp04__Authorisation__c auth = [SELECT Id, Contact__c FROM asp04__Authorisation__c WHERE Contact__c = :c.id LIMIT 1];
		System.assertNotEquals(auth, null);
	}

}