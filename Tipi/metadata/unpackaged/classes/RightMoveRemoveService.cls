/**
    * @Name:        RightMoveRemoveService
    * @Description: Class to process remove callouts for  RightMove API.
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 29/05/2018    Andrey Korobovich    Created Class
*/

public with sharing class RightMoveRemoveService implements AggregationsInterface {
	/**
	 * Main method to process callouts for any Aggregator
	 * @param  aggregator  argument contains all needed values to do callout
	 * @param  units list of units to be processed to Zoopla API
	 */

	public Map<Id, String> processRequest(List<Unit__c> units, Aggregator_Setting__mdt aggregator) {
		Map<Id, String> responsesByUnitId = new Map<Id, String>();
		Map<Id, String> jsonByUnitId = RightMoveUtilities.generateRemoveJson(units, aggregator);
		if (aggregator != null && units != null && !units.isEmpty()) {
			for (Unit__C unit : units) {
				RightMoveAsyncExecutionRemoveRequest request = new RightMoveAsyncExecutionRemoveRequest(
						aggregator.Method__c,
						aggregator.URL__c,
						new Map<String, String> {'Content-Type' => aggregator.Content_Type__c },
						jsonByUnitId.get(unit.Id),
						aggregator.Certificate__c,
						unit
				);
				if (!Test.isRunningTest()) {
					System.enqueueJob(request);
				}
			}
		}
		return responsesByUnitId;
	}
}