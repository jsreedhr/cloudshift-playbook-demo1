/**
 * Created by jaredwatson on 21/03/2018.
 */

public with sharing class Utils_Constants {


    /************************** USER ***********************************/
    public static final String USER_PROFILE_ADMIN               = 'System Administrator';

    /************************** Payments *******************************/
    public static final Map<String, String> LEASE_TO_PAYMENT_TYPE_MAP = new Map<String, String> {
            'AST'               => 'Rent - AST',
            'Corporate'         => 'Rent - Corporate',
            'Rent To Rent'      => 'Rent - Rent to Rent',
            'Other'             => 'Other Rental Income',
            'Deposit'           => 'Deposit'
    };
    public static Set<String> TIPI_PAYMENT_TYPES = new Set<String> {
            'Rent - AST',
            'Rent - Corporate',
            'Rent - Rent to Rent',
            'Other Rental Income',
            'Guest Parking',
            'Parking',
            'Storage',
            'Social Space'
    };
    public static Set<String> TIPI_RENT_PAYMENT_TYPES = new Set<String> {
            'Rent - AST',
            'Rent - Corporate',
            'Rent - Rent to Rent',
            'Other Rental Income'
    };
    public static final String PAYMENT_TYPE_RESERVATION         = 'Reservation';
    public static final String PAYMENT_TYPE_DEPOSIT             = 'Deposit';
    public static final String PAYMENT_TYPE_PARKING             = 'Parking';
    public static final String PAYMENT_TYPE_STORAGE             = 'Storage';
    public static final String PAYMENT_COLLECTED_FROM_CUSTOMER  = 'Collected from customer';
    public static final String PAYMENT_SUBMITTED_FOR_COLLECTION = 'Submitted for collection';
    public static final String CREDIT_PAYMENT_TYPE_CREDIT       = 'Credit';
    public static final String CREDIT_PAYMENT_TYPE_DEBIT        = 'Debit';
    public static final String PAYMENT_GATEWAY_DD               = 'Direct Debit';
    public static final String PAYMENT_GATEWAY_OTHER            = 'Other';
    public static final String FINANCE_QUEUE_NAME               = 'Finance_Queue';
    public static final String PAYMENT_DESCRIPTION              = 'Payment schedule created value for submission';
    public static final String AWAITING_SUBMISSION_STATUS       = 'Awaiting submission';
    public static final String CANCELLED_STATUS                 = 'Cancelled';
    public static final String FAILED_STATUS                    = 'Failed';
    public static final String PAYMENT_DRAW_DOWN_PART1          = '[Debit payment drawn down against ';
    public static final String PAYMENT_DRAW_DOWN_PART2          = ' Credit Balance]';
    public static final String PAYMENT_PARTIAL_DRAW_DOWN        = ' - [Partial draw down against Credit Balance ';
    public static final String PAYMENT_RECORDTYPE_REFUND        = 'Refund';
    public static final String GATEWAY_SAGEPAY                  = 'SagePay';
}