/**
 * @Name:        YardiResidentTriggerHandler
 * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
 *
 * @author:      Jared tson
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 28/06/2018    Jared Watson    Created Class
 */
public with sharing class YardiResidentTriggerHandler extends TriggerHandler {

    protected override void afterInsert(){
        YardiResidentTriggerHelper.processAfterInsert((Map<Id, Yardi_Data_Resident__c>) Trigger.newMap);
    }

}