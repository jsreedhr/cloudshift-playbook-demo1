/**
 * @Name:        YardiResidentTriggerTest
 * @Description: This is Test class to cover the YardiResidentTriggerHandler
 *
 * @author:      Jared Watson
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 03/07/2018    Jared Watson       Created Class
 * 06/07/2018    Patrick Fischer    Extended testSetup(), added logic to test multi-tenants
 * 10/07/2018    Patrick Fischer    Added logic to test creation of Tipi Payment schedule (using Yardi_Data_Lease_Charge__c records)
 */
@IsTest
public with sharing class YardiResidentTriggerTest {

    private static final String CONST_ACC_EXTID = 'resId';
    private static final String CONST_OPP_EXTID = 'resId';
    private static final String CONST_UNIT_EXTID = 'unit1';
    private static final String CONST_UNIT_MATCH = 'unit1';
    private static final String CONST_PRIMCON_EXTID = 'resId';
    private static final String CONST_TENANT_2_EXTID = 'ten2Id';
    private static final String CONST_TENANT_3_EXTID = 'ten3Id';
    private static final String CONST_TENANT_4_EXTID = 'ten4Id';
    private static final String CONST_TENANT_5_EXTID = 'ten5Id';
    private static final String CONST_PRIMGUA_EXTID = 'gua1Id';

    private static testMethod void processAfterInsertSuccess() {
        String exceptionString = '';
        Yardi_Data_Resident__c yardiDataResident = new Yardi_Data_Resident__c(
                Resident_Id__c = CONST_PRIMCON_EXTID,
                Tenant_2__c = CONST_TENANT_2_EXTID,
                Tenant_3__c = CONST_TENANT_3_EXTID,
                Tenant_4__c = CONST_TENANT_4_EXTID,
                Tenant_5__c = CONST_TENANT_5_EXTID,
                Primary_Guarantor__c = CONST_PRIMGUA_EXTID,
                Unit__c = CONST_UNIT_MATCH,
                Deposit__c = 1500,
                Move_In__c = Date.newInstance(2018, 4, 3),
                Address_Line_1__c = 'street 1',
                Address_Line_2__c = 'street 2',
                Address_Line_3__c = 'street 3',
                City__c = 'London',
                Postcode__c = 'HA9 0GS',
                Lease_Signed_Date__c = Date.newInstance(2018, 3, 15),
                Lease_Term__c = 12,
                Month_to_Month__c = false,
                First_Name__c = 'TestFirst',
                Last_Name__c = 'TestLast',
                Rent_Monthly__c = 2500,
                Lease_From_Date__c = Date.newInstance(2018, 4, 1),
                Lease_Type__c = 'AST'
        );

        Test.startTest();
        try {
            insert yardiDataResident;
        } catch (Exception e) {
            exceptionString = e.getMessage();
        }
        Test.stopTest();

        List<Contract> insertedContracts = [SELECT Id, ActivatedDate, BillingCity, BillingPostalCode, CompanySignedDate, ContractTerm, Tenant_2__c, Tenant_3__c FROM Contract];
        List<Tipi_Payments__c> insertedPaymentsCollected = [SELECT Id, Amount__c, Payment_Date__c FROM Tipi_Payments__c WHERE Status__c = :Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER ORDER BY Payment_Date__c ASC];
        List<Tipi_Payments__c> insertedPaymentsAwaitingSub = [SELECT Id, Amount__c, Payment_Date__c FROM Tipi_Payments__c WHERE Status__c = :Utils_Constants.AWAITING_SUBMISSION_STATUS ORDER BY Payment_Date__c ASC];

        System.assertEquals('', exceptionString, 'Expecting no errors during insertion');
        System.assert(!insertedContracts.isEmpty(), 'Contracts exist');
        System.assertEquals(1, insertedContracts.size(), '1 Contract exist');
        System.assertEquals('London', insertedContracts[0].BillingCity);
        System.assertEquals(Date.newInstance(2018, 3, 15), insertedContracts[0].CompanySignedDate);
        System.assertEquals(12, insertedContracts[0].ContractTerm);
        System.assertNotEquals(null, insertedContracts[0].Tenant_2__c);
        System.assertNotEquals(null, insertedContracts[0].Tenant_3__c);
        System.assertEquals(2, insertedPaymentsCollected.size(), 'Expecting 2 Payments in the past');
        System.assertEquals(8, insertedPaymentsAwaitingSub.size(), 'Expecting 8 Payments in the future');
        System.assertEquals(1500, insertedPaymentsAwaitingSub[0].Amount__c, 'Expecting August Payment of 1500');
        System.assertEquals(1500, insertedPaymentsAwaitingSub[1].Amount__c, 'Expecting September Payment of 1500');
        System.assertEquals(1500, insertedPaymentsAwaitingSub[2].Amount__c, 'Expecting October Payment of 1500');
        System.assertEquals(1700, insertedPaymentsAwaitingSub[3].Amount__c, 'Expecting Nov Payment of 1700');
        System.assertEquals(1700, insertedPaymentsAwaitingSub[4].Amount__c, 'Expecting Dec Payment of 1700');
        System.assertEquals(1700, insertedPaymentsAwaitingSub[5].Amount__c, 'Expecting Jan Payment of 1700');
        System.assertEquals(1700, insertedPaymentsAwaitingSub[6].Amount__c, 'Expecting Feb Payment of 1700');
        System.assertEquals(1732.60, insertedPaymentsAwaitingSub[7].Amount__c.setScale(2, RoundingMode.HALF_UP), 'Expecting Mar Payment of ~1732.60... (-> Prorata: 1700 * 12 / 365 * 31 days)');
    }

    private static testMethod void processAfterInsertFailure() {
        Yardi_Data_Resident__c yardiDataResident = new Yardi_Data_Resident__c();
        String exceptionString = '';
        try {
            insert yardiDataResident;
        } catch (Exception e) {
            exceptionString = e.getMessage();
        }

        List<Contract> insertedContracts = [SELECT Id FROM Contract];
        List<Custom_Log__c> insertedErrors = [SELECT Id FROM Custom_Log__c];
        System.assert(insertedContracts.isEmpty(), 'No Contracts exist');
        System.assertNotEquals('', exceptionString, 'Errors thrown');
    }

    @TestSetup
    public static void testSetup() {
        List<Pricebook2> pricebooks = TestFactoryPayment.getPricebooks(1, true);
        List<Account> accounts = TestFactoryPayment.addAccounts(1, false);
        accounts[0].External_Id__c = CONST_ACC_EXTID;
        insert accounts;

        List<Contact> contacts = TestFactoryPayment.addContacts(accounts, 6, false);
        contacts[0].AccountId = accounts[0].Id;
        contacts[1].AccountId = accounts[0].Id;
        contacts[2].AccountId = accounts[0].Id;
        contacts[3].AccountId = accounts[0].Id;
        contacts[4].AccountId = accounts[0].Id;
        contacts[5].AccountId = accounts[0].Id;
        contacts[0].External_Id__c = CONST_PRIMCON_EXTID;
        contacts[1].External_Id__c = CONST_TENANT_2_EXTID;
        contacts[2].External_Id__c = CONST_TENANT_3_EXTID;
        contacts[3].External_Id__c = CONST_TENANT_4_EXTID;
        contacts[4].External_Id__c = CONST_TENANT_5_EXTID;
        contacts[5].External_Id__c = CONST_PRIMGUA_EXTID;
        insert contacts;

        Opportunity opp = new Opportunity(
                Name = 'Test Opportunity',
                AccountId = accounts[0].Id,
                Primary_Contact__c = contacts[0].Id,
                Type = '1 Bed',
                CloseDate = System.today() + 3,
                StageName = 'Interested',
                External_Id__c = CONST_OPP_EXTID
        );
        insert opp;

        Unit__c unit = [SELECT Id FROM Unit__c LIMIT 1];
        unit.Name = CONST_UNIT_MATCH;
        unit.External_Id__c = CONST_UNIT_EXTID;
        update unit;

        delete [SELECT Id FROM Contract];

        List<Yardi_Data_Ledger_Reports__c> yardiDataLedgerReports = new List<Yardi_Data_Ledger_Reports__c>();
        // add charge
        yardiDataLedgerReports.add(new Yardi_Data_Ledger_Reports__c(
                Lease_Code__c = CONST_PRIMCON_EXTID,
                Chg_Rec__c = 'C-281',
                Description__c = 'RentAST - AST Rent',
                From__c = Date.newInstance(2018,6,1),
                To__c = Date.newInstance(2018,6,30),
                Total__c = 1500,
                Payment__c = 0,
                Trans_Date__c = Date.newInstance(2018,6,1)
        ));
        // add payment
        yardiDataLedgerReports.add(new Yardi_Data_Ledger_Reports__c(
                Lease_Code__c = CONST_PRIMCON_EXTID,
                Chg_Rec__c = 'R-1135',
                Description__c = ':Direct Debit',
                From__c = null,
                To__c = null,
                Total__c = 0,
                Payment__c = 1500,
                Trans_Date__c = Date.newInstance(2018,6,3)
        ));
        insert yardiDataLedgerReports;

        // add Lease Charge schedule (baseline for future payments)
        List<Yardi_Data_Lease_Charge__c> yardiDataLeaseCharges = new List<Yardi_Data_Lease_Charge__c>();
        yardiDataLeaseCharges.add(new Yardi_Data_Lease_Charge__c(
                Resident_Id__c = CONST_PRIMCON_EXTID,
                Charge_Code__c = 'RentAST',
                From_Date__c = Date.newInstance(2018, 1, 1),
                To_Inactive__c = Date.newInstance(2018, 10, 31),
                Amount__c = 1500
        ));
        yardiDataLeaseCharges.add(new Yardi_Data_Lease_Charge__c(
                Resident_Id__c = CONST_PRIMCON_EXTID,
                Charge_Code__c = 'RentAST',
                From_Date__c = Date.newInstance(2018, 11, 1),
                To_Inactive__c = Date.newInstance(2019, 12, 31),
                Amount__c = 1700
        ));
        insert yardiDataLeaseCharges;

        TestFactory.createSObject(new Global_Application_Settings__c(), true);
    }
}