/**
    * @Name:        RightMoveUtilities
    * @Description: Class that contains all needed components to process RightMove integration
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 06/04/2018    Eugene Konstantinov    Created Class
*/
public with sharing class RightMoveUtilities {
    // Map that contains fields mapping RightMove JSON Value => Field API Name on Unit object
    private static Map<String, String> getMTDFields = getMTDFields();
    // Map that contains values mapping for RightMove Status based on Status on Unit object
    private static Map<String, Integer> getIntegerValueForStatus = getIntegerValueForStatus();
    // Map that contains values mapping for RightMove Property Type based on Unit Status on Unit object
    private static Map<String, Integer> getIntegerValueForPropertyType = getIntegerValueForPropertyType();
    // Map that contains values mapping for RightMove Furnished Type based on Furnished Type on Unit object
    private static Map<String, Integer> getIntegerValueForFurnishedType = getIntegerValueForFurnishedType();

    private static String getField(String fieldName){
        return (String)getMTDFields.get(fieldName);
    }

    private static Integer getIntegerValueForStatus(String fieldName){
        System.debug('fieldname ' + fieldName);
        System.debug('getIntegerValueForStatus ' + getIntegerValueForStatus);
        System.debug('return ' + getIntegerValueForStatus.get(fieldName));
        System.debug('return Int ' + (Integer)getIntegerValueForStatus.get(fieldName));
        return (Integer)getIntegerValueForStatus.get(fieldName);
    }

    private static Integer getIntegerValueForPropertyType(String fieldName){
        return (Integer)getIntegerValueForPropertyType.get(fieldName);
    }

    private static Integer getIntegerValueForFurnishedType(String fieldName){
        return (Integer)getIntegerValueForFurnishedType.get(fieldName);
    }

    // Generating JSON based on RightMove Schema
    public static Map<Id, String> generateJson(List<Unit__c> units, String network_id, String branch_id, String channel, Boolean overseas){
        Map<Id, String> jsonByUnit = new Map<Id, String>();
        //Map with Unit Id and related Buildings
        Map<Id, Building__c> buildings = getBuildingsById(units);
        //Map with Building ID and related Rentable Items
        Map<Id, List<Rentable_Item__c>> rentableItemByBuildingId = getRentableItemsByBuildings(buildings.values());
        //Map with Parking options for the RightMove. Key is the Building Id
        Map<Id, List<Integer>> parkingByBuilding = getParkingListForBuilding(rentableItemByBuildingId);
        //Map with list of Key Fetures for the RightMove. Key is the Unit Id
        Map<Id, List<String>> featuresListByUnitId = getFeaturesForUnits(units);
        //Map with list of Media for the RightMove. Key is the Unit Id
        Map<Id, List<RighMoveSendPropertyStructure.Media>> mediaListByUnitId = getMediaListByUnitId(units);
        //Map with Floor value for the RightMove. Key is the Unit Id
        Map<Id, Integer> entranceByUnitId = getEntranceForUnit(units);

        // This loop generates Map with the RightMove JSON. Key is the Unit Id
        for (Unit__c unit : units) {
            //Generating Price_information section for JSON
            RighMoveSendPropertyStructure.Price_information price = new RighMoveSendPropertyStructure.Price_information(
                String.isBlank(getField('price')) ? null : Integer.valueOf(unit.get(getField('price'))),
                String.isBlank(getField('price_qualifier')) ? null : Integer.valueOf(unit.get(getField('price_qualifier'))),
                String.isBlank(getField('deposit')) ? null : Integer.valueOf(unit.get(getField('deposit'))),
                String.isBlank(getField('administration_fee')) ? null : String.valueOf(unit.get(getField('administration_fee'))),
                String.isBlank(getField('rent_frequency')) ? null : Integer.valueOf(unit.get(getField('rent_frequency'))),
                String.isBlank(getField('price_per_unit_per_annum')) ? null : Integer.valueOf(unit.get(getField('price_per_unit_per_annum')))
            );

            //Generating Address section for JSON
            RighMoveSendPropertyStructure.Address address = new RighMoveSendPropertyStructure.Address(
                String.isBlank(getField('House_Name_Number')) ? null : String.valueOf(unit.get(getField('House_Name_Number'))),
                String.isBlank(getField('Address_2')) ? null : String.valueOf(unit.get(getField('Address_2'))),
                String.isBlank(getField('Address_3')) ? null : String.valueOf(unit.get(getField('Address_3'))),
                String.isBlank(getField('Address_4')) ? null : String.valueOf(unit.get(getField('Address_4'))),
                String.isBlank(getField('town')) ? null : String.valueOf(unit.get(getField('town'))),
                String.isBlank(getField('postcode_1')) ? null : String.valueOf(unit.get(getField('postcode_1'))).split(' ').get(0),
                String.isBlank(getField('postcode_2')) ? null : String.valueOf(unit.get(getField('postcode_2'))).split(' ').size() > 1 ? String.valueOf(unit.get(getField('postcode_2'))).split(' ').get(1) : ' ',
                String.isBlank(getField('display_address')) ? null : String.valueOf(unit.get(getField('display_address'))), //maxLength = 120
                String.isBlank(getField('latitude')) ? null : Double.valueOf(unit.get(getField('latitude'))),
                String.isBlank(getField('longitude')) ? null : Double.valueOf(unit.get(getField('longitude'))),
                String.isBlank(getField('latitude')) ? null : Double.valueOf(unit.get(getField('latitude'))), //String.isBlank(getField('pov_latitude')) ? null : Double.valueOf(unit.get(getField('pov_latitude'))),
                String.isBlank(getField('longitude')) ? null : Double.valueOf(unit.get(getField('longitude'))), //String.isBlank(getField('pov_longitude')) ? null : Double.valueOf(unit.get(getField('pov_longitude'))),
                0, //String.isBlank(getField('pov_pitch')) ? null : Double.valueOf(unit.get(getField('pov_pitch'))),
                0, //String.isBlank(getField('pov_heading')) ? null : Double.valueOf(unit.get(getField('pov_heading'))),
                0 //String.isBlank(getField('pov_zoom')) ? null : Integer.valueOf(unit.get(getField('pov_zoom')))
            );

            //Generating Sizing section for JSON
            RighMoveSendPropertyStructure.Sizing sizing = new RighMoveSendPropertyStructure.Sizing(
                String.isBlank(getField('minimum')) ? null : Integer.valueOf(unit.get(getField('minimum'))),
                String.isBlank(getField('maximum')) ? null : Integer.valueOf(unit.get(getField('maximum'))),
                String.isBlank(getField('area_unit')) ? null : Integer.valueOf(unit.get(getField('area_unit')))
            );

            //Generating Details section for JSON
            RighMoveSendPropertyStructure.Details details = new RighMoveSendPropertyStructure.Details(
                String.isBlank(getField('summary')) ? null : String.valueOf(unit.get(getField('summary'))),
                String.isBlank(getField('description')) ? null : String.valueOf(unit.get(getField('description'))),
                featuresListByUnitId.get(unit.Id), // features
                String.isBlank(getField('bedrooms')) ? null : Integer.valueOf(unit.get(getField('bedrooms'))),
                String.isBlank(getField('bathrooms')) ? null : Integer.valueOf(unit.get(getField('bathrooms'))),
                buildings.containsKey(unit.Id) ? parkingByBuilding.containsKey(buildings.get(unit.Id).Id) ? parkingByBuilding.get(buildings.get(unit.Id).Id) : new List<Integer>() : new List<Integer>(), //parking
                String.isBlank(getField('year_built')) ? null : Integer.valueOf(unit.get(getField('year_built'))),
                String.isBlank(getField('internal_area')) ? null : Integer.valueOf(unit.get(getField('internal_area'))),
                String.isBlank(getField('internal_area_unit')) ? null : Integer.valueOf(unit.get(getField('internal_area_unit'))),
                sizing,
                String.isBlank(getField('floors')) ? null : Integer.valueOf(unit.get(getField('floors'))),
                entranceByUnitId.get(unit.Id),
                String.isBlank(getField('condition')) ? null : Integer.valueOf(unit.get(getField('condition'))),
                new List<Integer>(), //accessibility
                new List<Integer>(), //heating
                String.isBlank(getField('furnished_type')) ? null : getIntegerValueForFurnishedType(String.valueOf(unit.get(getField('furnished_type')))),
                String.isBlank(getField('business_for_sale')) ? null : Boolean.valueOf(unit.get(getField('business_for_sale'))),
                new List<Integer>(), //comm_use_class
                buildings.containsKey(unit.Id) ? Boolean.valueOf(buildings.get(unit.Id).Allows_Pets__c) : false
            );

            //Generating Principal section for JSON
            RighMoveSendPropertyStructure.Principal principal = new RighMoveSendPropertyStructure.Principal(
                String.isBlank(getField('principal_email_address')) ? null : String.valueOf(unit.get(getField('principal_email_address'))),
                String.isBlank(getField('auto_email_when_live')) ? null : Boolean.valueOf(unit.get(getField('auto_email_when_live'))),
                String.isBlank(getField('auto_email_updates')) ? null : Boolean.valueOf(unit.get(getField('auto_email_updates')))
            );
            System.debug('UNIT STATUS>>> ' +String.valueOf(unit.get(getField('status'))));
            System.debug('STATUS IN JSON HARDCODED>>>>>>> ' + getIntegerValueForStatus('Unoccupied – No New Lease'));


            System.debug('STATUS IN JSON>>>>>>> ' + getIntegerValueForStatus(String.valueOf(unit.get(getField('status')))));

            //Generating Property section for JSON
            RighMoveSendPropertyStructure.Property property = new RighMoveSendPropertyStructure.Property(
                String.isBlank(getField('agent_ref')) ? null : String.valueOf(unit.get(getField('agent_ref'))),
                String.isBlank(getField('published')) ? null : Boolean.valueOf(unit.get(getField('published'))),
                String.isBlank(getField('property_type')) ? null : getIntegerValueForPropertyType(String.valueOf(unit.get(getField('property_type')))),
                String.isBlank(getField('status')) ? null : getIntegerValueForStatus(String.valueOf(unit.get(getField('status')))),
                String.isBlank(getField('create_date')) ? null : String.valueOf(unit.get(getField('create_date'))),
                String.isBlank(getField('update_date')) ? null : String.valueOf(unit.get(getField('update_date'))),
                String.isBlank(getField('Date_Available')) ? null : convertDate(Date.valueOf(unit.get(getField('Date_Available'))),'d-MM-YYY'),
                String.isBlank(getField('contract_months')) ? null : Integer.valueOf(unit.get(getField('contract_months'))),
                String.isBlank(getField('minimum_term')) ? null : Integer.valueOf(unit.get(getField('minimum_term'))),
                String.isBlank(getField('let_type')) ? null : Integer.valueOf(unit.get(getField('let_type'))),
                address,
                price,
                details,
                mediaListByUnitId.containsKey(unit.Id) ? mediaListByUnitId.get(unit.Id) : new List<RighMoveSendPropertyStructure.Media>(),
                principal
            );

            //Generating Network section for JSON
            RighMoveSendPropertyStructure.Network network = new RighMoveSendPropertyStructure.Network(
                Integer.valueOf(network_id)//10430 //String.isBlank(getField('network_id')) ? null : Integer.valueOf(unit.get(getField('network_id')))
            );

            //Generating Branch section for JSON
            RighMoveSendPropertyStructure.Branch branch = new RighMoveSendPropertyStructure.Branch(
                Integer.valueOf(branch_id), //String.isBlank(getField('branch_id')) ? null : Integer.valueOf(unit.get(getField('branch_id'))), 148778
                Integer.valueOf(channel), //String.isBlank(getField('channel')) ? null : Integer.valueOf(unit.get(getField('channel'))), 2
                overseas//String.isBlank(getField('overseas')) ? null : Boolean.valueOf(unit.get(getField('overseas')))
            );

            Map<String, Object> jsonMap = new Map<String, Object>();
            jsonMap.put('network', network);
            jsonMap.put('branch', branch);
            jsonMap.put('property', property);
            jsonByUnit.put(unit.Id, JSON.serialize(jsonMap, true));
        }

        return jsonByUnit;
    }

    /**
    * Method to get parking options for building
    * @param  rentableItemsById  map of rentable item records by Id
    *
    * @returns Map<Id, List<Integer>> Map with Parking options for the RightMove. Key is the Building Id
    */
    private static Map<Id, List<Integer>> getParkingListForBuilding(Map<Id, List<Rentable_Item__c>> rentableItemsById){
        Map<Id, List<Integer>> parkingMap = new Map<Id, List<Integer>>();

        if (rentableItemsById != null && !rentableItemsById.isEmpty()) {
            for (Id buildingId : rentableItemsById.keySet()) {
                parkingMap.put(buildingId, new List<Integer>());
                if (rentableItemsById.containsKey(buildingId) && rentableItemsById.get(buildingId) != null && !rentableItemsById.get(buildingId).isEmpty()) {
                    for (Rentable_Item__c rentableItem : rentableItemsById.get(buildingId)) {
                        if (rentableItem.RecordType.DeveloperName == 'Parking') {
                            if (rentableItem.Allocated__c) {
                                parkingMap.get(buildingId).add(13);
                            } else {
                                parkingMap.get(buildingId).add(22);
                            }
                        }
                    }
                }
            }
        }

        return parkingMap;
    }

    /**
    * Method to get Unit Field - RightMove field mappting
    *
    * @returns Map<String,String> Unit Field Api Names by RightMove field name
    */
    private static Map<String,String> getMTDFields() {
        Map<String,String> rtmMap = new Map<String,String>();

        for (RightMove__mdt rtm : (List<RightMove__mdt>)new RightMoveSelector().getRecords(new Set<String>{
            RightMove__mdt.MasterLabel.getDescribe().getName(),
            RightMove__mdt.FieldAPIName__c.getDescribe().getName()
        })) {
            rtmMap.put(rtm.MasterLabel, rtm.FieldAPIName__c);
        }

        SYstem.debug('FIELD MAPPING >>>' + JSON.serialize(rtmMap));

        return rtmMap;
    }

    /**
     * Method to get Integer value for status
     *
     * @returns Map<String, Integer>  Map with Integer value for Unit status
    */
    private static Map<String, Integer> getIntegerValueForStatus() {
        Map<String, Integer> rtmMap = new Map<String, Integer>();

        for (RightMove_Status_Field_Setting__mdt rtm : [
            SELECT MasterLabel, Unit_Status_Value__c, Status_Number__c
            FROM RightMove_Status_Field_Setting__mdt
        ]) {
            rtmMap.put(rtm.Unit_Status_Value__c, Integer.valueOf(rtm.Status_Number__c));
        }

        System.debug('STATUS MAP>>>>>>>' + JSON.serializePretty(rtmMap));

        return rtmMap;
    }

    /**
    * Method to get Integer value for property type
    *
    * @returns Map<String, Integer>  Map with Integer value for Unit property type
    */
    private static Map<String, Integer> getIntegerValueForPropertyType() {
        Map<String, Integer> rtmMap = new Map<String, Integer>();

        for (RightMove_Property_Type_Setting__mdt rtm : [
            SELECT MasterLabel, Unit_Type__c, Property_Type_Number__c
            FROM RightMove_Property_Type_Setting__mdt
        ]) {
            rtmMap.put(rtm.Unit_Type__c, Integer.valueOf(rtm.Property_Type_Number__c));
        }

        return rtmMap;
    }

    /**
    * Method to get Integer value for furnished type
    *
    * @returns Map<String, Integer>  Map with Integer value for Unit furnished type
    */
    private static Map<String, Integer> getIntegerValueForFurnishedType(){
        Map<String, Integer> rtmMap = new Map<String, Integer>();

        for (RightMove_Furnished_Type_Setting__mdt rtm : [
            SELECT MasterLabel, Furnished_Unit_Field_Value__c, Furnished_Type_Number__c
            FROM RightMove_Furnished_Type_Setting__mdt
        ]) {
            rtmMap.put(rtm.Furnished_Unit_Field_Value__c, Integer.valueOf(rtm.Furnished_Type_Number__c));
        }

        return rtmMap;
    }

    /**
    * Method to get Building for unit
    * @param unitList list of unit added to aggregator
    *
    * @returns Map<Id, Building__c>  key - unit Id
    */
    private static  Map<Id, Building__c> getBuildingsById(List<Unit__c> unitList) {
        Map<Id, Building__c> buildingByUnit = new Map<Id, Building__c>();

        Set<Id> buildingIds = new Set<Id>();
        for (Unit__c unit : unitList) {
            buildingIds.add(unit.Building__c);
        }

        List<Building__c> buildings = (List<Building__c>)new BuildingSelector().getRecordsByUnitIds(buildingIds, new Set<String>{
            Building__c.Name.getDescribe().getName(),
            Building__c.Allows_Pets__c.getDescribe().getName()
        });

        if (buildings != null && !buildings.isEmpty()) {
            Map<Id, Building__c> buildingById = new Map<Id, Building__c>(buildings);
            for (Unit__c unit : unitList) {
                if (unit.Building__c != null && buildingById.containsKey(unit.Building__c)) {
                    buildingByUnit.put(unit.Id, buildingById.get(unit.Building__c));
                }
            }
        }

        return buildingByUnit;
    }

    /**
    * Method to get entrance value for Unit
    * @param unitList list of unit added to aggregator
    *
    * @returns Map<Id, Integer> key - unit Id
    */
    private static  Map<Id, Integer> getEntranceForUnit(List<Unit__c> unitList) {
        Map<Id, Integer> entranceFloorByUnitId = new Map<Id, Integer>();

        Map<Integer, Integer> entranceFloorByUnitFloor = new Map<Integer, Integer>();
        Integer defaultEntrance = 6;
        for (RightMove_Entrance_Floor_Setting__mdt floorSettings : [
            SELECT MasterLabel, Default_Entrance_Floor__c, Entrance_Floor_Number__c, Unit_Floor_Value__c
            FROM RightMove_Entrance_Floor_Setting__mdt
        ]) {
            if (floorSettings.Unit_Floor_Value__c != null) {
                entranceFloorByUnitFloor.put(Integer.valueOf(floorSettings.Unit_Floor_Value__c), Integer.valueOf(floorSettings.Entrance_Floor_Number__c));
            } else if (floorSettings.Default_Entrance_Floor__c) {
                defaultEntrance = Integer.valueOf(floorSettings.Entrance_Floor_Number__c);
            }
        }

        for (Unit__c unit : unitList) {
            if (!entranceFloorByUnitFloor.containsKey(Integer.valueOf(unit.Floor__c))) {
                entranceFloorByUnitId.put(unit.Id, defaultEntrance);
            } else {
                entranceFloorByUnitId.put(unit.Id, entranceFloorByUnitFloor.get(Integer.valueOf(unit.Floor__c)));
            }
        }

        return entranceFloorByUnitId;
    }

    /**
    * Method to get list of rentable items for Building
    * @param buildings list of buildings related to units
    *
    * @returns Map<Id, List<Rentable_Item__c>>  key - building Id
    */
    private static Map<Id, List<Rentable_Item__c>> getRentableItemsByBuildings(List<Building__c> buildings){
        Map<Id, List<Rentable_Item__c>> rentableItemByBuilding  = new Map<Id, List<Rentable_Item__c>>();

        if (buildings != null && !buildings.isEmpty()) {
            List<Rentable_Item__c> rentableItems = (List<Rentable_Item__c>)new RentableItemSelector().getRecordsByBuildings(buildings, new Set<String>{
                Rentable_Item__c.Name.getDescribe().getName(),
                Rentable_Item__c.Building__c.getDescribe().getName(),
                Rentable_Item__c.Allocated__c.getDescribe().getName(),
                'RecordType.DeveloperName'
            });

            if (rentableItems != null && !rentableItems.isEmpty()) {
                for (Rentable_Item__c rentableItem : rentableItems) {
                    if (rentableItem.Building__c != null) {
                        if (!rentableItemByBuilding.containsKey(rentableItem.Building__c)) {
                            rentableItemByBuilding.put(rentableItem.Building__c, new List<Rentable_Item__c>());
                        }
                        rentableItemByBuilding.get(rentableItem.Building__c).add(rentableItem);
                    }
                }
            }
        }

        return rentableItemByBuilding;
    }

    /**
    * Method to get list of unit features
    * @param units list of units added to aggreagator
    *
    * @returns Map<Id, List<String>>  key - unit Id
    */
    private static Map<Id, List<String>> getFeaturesForUnits(List<Unit__c> units) {
        Map<Id, List<String>> featuresByUnit = new Map<Id, List<String>>();
        for (Unit__c unit : units) {
            if (!featuresByUnit.containsKey(unit.Id)) {
                featuresByUnit.put(unit.Id, new List<String>());
            }
            if (unit.Unit_Key_Data_01__c != null) {
                featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_01__c);
            }
            if (unit.Unit_Key_Data_02__c != null) {
                featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_02__c);
            }
            if (unit.Unit_Key_Data_03__c != null) {
                featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_03__c);
            }
            if (unit.Unit_Key_Data_04__c != null) {
                featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_04__c);
            }
            if (unit.Unit_Key_Data_05__c != null) {
                featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_05__c);
            }
            if (unit.Unit_Key_Data_06__c != null) {
                featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_06__c);
            }
            if (unit.Unit_Key_Data_07__c != null) {
                featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_07__c);
            }
            if (unit.Unit_Key_Data_08__c != null) {
                featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_08__c);
            }
            if (unit.Unit_Key_Data_09__c != null) {
                featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_09__c);
            }
            if (unit.Unit_Key_Data_10__c != null) {
                featuresByUnit.get(unit.Id).add(unit.Unit_Key_Data_10__c);
            }
        }

        return featuresByUnit;
    }

    /**
    * Method to get media list of unit
    * @param units list of units added to aggreagator
    *
    * @returns Map<Id, List<RighMoveSendPropertyStructure.Media>>  key - unit Id
    */
    private static Map<Id, List<RighMoveSendPropertyStructure.Media>> getMediaListByUnitId(List<Unit__c> units) {
        Map<Id, List<RighMoveSendPropertyStructure.Media>> resultMap = new Map<Id, List<RighMoveSendPropertyStructure.Media>>();
        Map<Id, Unit__c> unitById = new Map<Id, Unit__c>(units);
        List<Unit_File__c> unitFiles = (List<Unit_File__c>)new UnitFileSelector().getRecordsByUnitIds(unitById.keySet(), new Set<String>{
            Unit_File__c.Name.getDescribe().getName(),
            Unit_File__c.Unit__c.getDescribe().getName(),
            Unit_File__c.Proxy_URL__c.getDescribe().getName(),
            Unit_File__c.Type__c.getDescribe().getName(),
            Unit_File__c.Sequence_Number__c.getDescribe().getName(),
            Unit_File__c.Shared_Link__c.getDescribe().getName(),
            Unit_File__c.LastModifiedDate.getDescribe().getName()
        });

        if (unitFiles != null && !unitFiles.isEmpty()) {

            for (Unit_File__c uFile : unitFiles) {

				if (String.isNotBlank(uFile.Proxy_URL__c)) {

					if (!resultMap.containsKey(uFile.Unit__c)) {
						resultMap.put(uFile.Unit__c, new List<RighMoveSendPropertyStructure.Media>());
					}
					Integer mediaType = 1;
					if (uFile.Type__c == 'Image') {
						mediaType = 1;
					} else if (uFile.Type__c == 'FloorPlan') {
						mediaType = 2;
					}
					resultMap.get(uFile.Unit__c).add(new RighMoveSendPropertyStructure.Media(
							mediaType,
							uFile.Proxy_URL__c,
							unitById.get(uFile.Unit__c).Name,
							Integer.valueOf(uFile.Sequence_Number__c),
							String.valueOf(uFile.LastModifiedDate.format('dd-MM-yyyy HH:mm:ss'))
					));
				}
			}
        }

        return resultMap;
    }

	/**
	 * Method to convert date
	 * @param dateValue Date to convert
	 * @param format string format for date
	 *
	 * @returns String  formatted date value
	 */
	private static String convertDate(Date dateValue, String format) {
		if (dateValue == null) return null;
		return DateTime.newInstance(dateValue.year(),dateValue.month(),dateValue.day()).format(format);

	}

    public static Map<Id, String> generateRemoveJson(List<Unit__c> units, Aggregator_Setting__mdt aggregator){
        Map<Id, String> resultmap = new Map<Id, String>();
        for(Unit__c unit : units){
            resultmap.put(unit.Id , JSON.serialize(new RightMoveRemoveRequestStructure(Integer.valueOf(aggregator.Network_Id__c), Integer.valueOf(aggregator.Branch_Id__c), aggregator.Channel__c, unit.Id)));
        }
        return resultmap;
    }

}