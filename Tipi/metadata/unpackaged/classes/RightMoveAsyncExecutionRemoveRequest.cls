/**
    * @Name:        RightMoveAsyncExecutionRemoveRequest
    * @Description: Class to create Queueable callout to RightMove
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 29/05/2018    Andrey Korobovich    Created Class
*/
public with sharing class RightMoveAsyncExecutionRemoveRequest  implements Queueable, Database.AllowsCallouts{
 	private static final String CLASS_NAME = 'RightMoveAsyncExecutionRemoveRequest';
	private static final String SUCCESS_RESPONSE = 'OK';
	private String method;
	private String url;
	private Map<String, String> additionalHeader;
	private String body;
	private String certificate;
	private Unit__c unit;

	public RightMoveAsyncExecutionRemoveRequest(String method, String url, Map<String, String> additionalHeader, String body, String certificate, Unit__c unit) {
		this.method = method;
		this.url = url;
		this.additionalHeader = additionalHeader;
		this.body = body;
		this.certificate = certificate;
		this.unit = unit;
	}



	public void execute(QueueableContext context) {
        HTTPResponse response = sendRequest(this.method, this.url, this.additionalHeader, this.body, this.certificate);
        if (response != null) {
           	String responseString = response.getBody();
            System.debug(responseString);
            RightMoveRemoveResponse parsedResponse = (RightMoveRemoveResponse) JSON.deserialize(responseString, RightMoveRemoveResponse.class);
            if (response.getStatus() == SUCCESS_RESPONSE && parsedResponse.success) {
                List<Unit__c> unitToUpdate = (List<Unit__c>) new UnitSelector().getRecordsByIds(new Set<Id>{this.unit.Id},new Set<String>{
                        Unit__c.Name.getDescribe().getName()
                        });
                if (unitToUpdate != null && !unitToUpdate.isEmpty()) {
                    unitToUpdate[0].RightMove_Id__c = '';
                    unitToUpdate[0].RightMove_URL__c = '';
                    update unitToUpdate;
                }
    
            }
        }
	}

	/**
	 * Send Request Method
	 * @param  method  string value
	 * @param  url  string value of endpoint
	 * @param  additionalHeader  map of additional header information
	 * @param  body  string value
	 * @param  certificate  string name of cetificate
	 *
	 * @returns HTTPResponse response from endpoint
	 */
	private HTTPResponse sendRequest(String method, String url, Map<String, String> additionalHeader, String body, String certificate){
		try {
			HttpRequest Request = new HttpRequest();
			Request.setMethod(method);
			Request.setEndpoint(url);
			Request.setTimeout(120000);

			if (!String.isEmpty(certificate)) {
				Request.setClientCertificateName(certificate);
			}
			for (String key : additionalHeader.keySet()) {
				Request.setHeader(key, (String)additionalHeader.get(key));
			}
			if (!String.isEmpty(body)) {
				Request.setBody(body);
			}
			System.debug(body);

			Http http = new Http();
			HTTPResponse response = http.send(Request);
			System.debug(response.getBody());
			return response;
		} catch (CalloutException ex) {
			CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'Callout Exception:' + CLASS_NAME + ex.getMessage());
		} catch (Exception ex) {
			CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'Exception:' + CLASS_NAME + ex.getMessage());
		} finally {
			CustomLogger.save();
		}
		return null;
	}
}