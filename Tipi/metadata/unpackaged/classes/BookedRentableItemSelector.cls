/**
    * @Name:        BookedRentableItemSelector
    * @Description: Class for fetching data stored in salesforce.
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 23/04/2018    Eugene Konstantinov    Created Class
    * 25/05/2018    Eugene Konstantinov    Added getRecordsByContactId method
    * 01/06/2018    Eugene Konstantinov    Added getRecordsByLeaseIds method
*/
public with sharing class BookedRentableItemSelector extends Selector {
    public static final String objectAPIName = Booked_Rentable_Item__c.SobjectType.getDescribe().getName();

    /**
    * Method to fetch data stored in salesforce
    * @param    Id            Rentable_Item__c record Id
    *           Set<String>   fields is a set of Strings that determines which fields are queried of the Booked Social Space object
    * @returns  List<SObject> a list of Renable Item objects that are treated and stored as a generic type
    **/
    public List<SObject> getGuestParkingRecordsByBuildingId(Id objectId, Set<String> fields) {
        Id buildingId = objectId;
        if (buildingId == null){return new List<SObject>();}
        Id rtId = getRecordType('Guest_Parking', 'Rentable_Item__c');
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Rentable_Item__r.Building__c = :buildingId AND Rentable_Item__r.RecordTypeId = :rtId LIMIT 10000';
        return Database.query(query);
    }

    /**
    * Method to fetch data stored in salesforce
    * @param    Id            Social_Space__c record Id
    *           Set<String>   fields is a set of Strings that determines which fields are queried of the Booked Social Space object
    * @returns  List<SObject> a list of Social Space objects that are treated and stored as a generic type
    **/
    public List<SObject> getRecordsBySocialSpaceId(Id objectId, Set<String> fields) {
        Id socialSpaceId = objectId;
        if (socialSpaceId == null){return new List<SObject>();}
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Social_Space__c = :socialSpaceId LIMIT 10000';
        return Database.query(query);
    }

    /**
    * Method to fetch data stored in salesforce
    * @param    Id            Contact record Id
    *           Set<String>   fields is a set of Strings that determines which fields are queried of the Booked Social Space object
    * @returns  List<SObject> a list of Social Space objects that are treated and stored as a generic type
    **/
    public List<SObject> getRecordsByContactId(Id objectId, Set<String> fields) {
        Id contactId = objectId;
        if (contactId == null){return new List<SObject>();}
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Contact__c = :contactId LIMIT 10000';
        return Database.query(query);
    }

    /**
    * Method to fetch data stored in salesforce
    * @param    Id            Development record Id
    *           Set<String>   fields is a set of Strings that determines which fields are queried of the Booked Social Space object
    *           Id            Lease Id
    * @returns  List<SObject> a list of Social Space objects that are treated and stored as a generic type
    **/
    public List<SObject> getUnitBookingsForLeaseTenants(Id developmentId, Set<Id> contactIds, Id leaseId, Set<String> fields) {
        Set<Id> cIds = contactIds;
        Id buildId = developmentId;
        Id contractId = leaseId;
        if (cIds == null || cIds.isEmpty() || buildId == null || contractId == null){return new List<SObject>();}
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Contract__c = :contractId AND Contact__c IN :cIds AND (Rentable_Item__r.Building__r.Development_Site__c = :buildId OR Social_Space__r.Building__r.Development_Site__c = :buildId) LIMIT 10000';
        return Database.query(query);
    }

    /**
    * Method to fetch data stored in salesforce
    * @param    Set<Id>       Lease records Id
    *           Set<String>   fields is a set of Strings that determines which fields are queried of the Booked Social Space object
    * @returns  List<SObject> a list of Social Space objects that are treated and stored as a generic type
    **/
    public List<SObject> getRecordsByLeaseIds(Set<Id> objectIds, Set<String> fields) {
        Set<Id> leases = objectIds;
        if (leases == null){return new List<SObject>();}
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Contract__c = :leases LIMIT 10000';
        return Database.query(query);
    }

    /**
    * Method to get Record Type Id
    * @param    recordTypeAPIName   String value of Record Type Api Name
    *           objApiName   String value of sObject Api name
    * @returns  Id Record Type Id
    **/
    public Id getRecordType(String recordTypeAPIName, String objApiName) {
        List<RecordType> recordTypes = [
                SELECT Id
                FROM RecordType
                WHERE
                sObjectType = :objApiName AND
                DeveloperName = :recordTypeAPIName
        LIMIT 1
        ];
        if (recordTypes != null && !recordTypes.isEmpty()) {
            return recordTypes[0].Id;
        }
        return null;
    }

}