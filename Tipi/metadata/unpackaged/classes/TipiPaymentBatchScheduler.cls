/**
 * @Name:        TipiPaymentBatchScheduler
 * @Description: Aggregate all payments for each user that are for current month in Tipi Payments
 *               table and insert the corresponding rows into the Payment table (Asperato)
 *
 * @author:      Eugene Konstantinov
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 15/05/2018    Eugene Konstantinov    Created Class
 * 04/07/2018    Patrick Fischer        Introducted executionDate.addDays(10).toStartOfMonth()
 */
public with sharing class TipiPaymentBatchScheduler implements Schedulable {

    public void execute(SchedulableContext sc) {

        Integer numberOfDays = 0;
        List<Batch_Run_Days_Before__c> settings = [SELECT Number_of_Days__c FROM Batch_Run_Days_Before__c];
        if(!settings.isEmpty()) {
            if(settings[0].Number_of_Days__c != null) {
                numberOfDays = Integer.valueOf(settings[0].Number_of_Days__c);
            }
        }

        if(Date.today().addDays(numberOfDays).day() == 1) {
            Database.executeBatch(new TipiPaymentBatch(Date.today().addDays(numberOfDays)), 10);
        }
    }
}