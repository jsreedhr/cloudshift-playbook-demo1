/**
    * @Name:        CalendarCmpControllerTest
    * @Description: Test class for CalendarCmpControllerT
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 03/05/2018    Eugene Konstantinov    Created Class
    * 06/06/2018    Eugene Konstantinov    Class Refactoring; additional scenarios added
*/
@isTest
private class CalendarCmpControllerTest {

    @isTest
    static void getEventsTest() {
        List<Account> acc = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contact = TestFactoryPayment.addContacts(acc, 1, true);
        List<Unit__c> existedUnits = [
            SELECT Id, Name, Building__c, Building__r.Development_Site__c
            FROM Unit__c
            WHERE
                Id = :contact[0].Leased_Reserved_Unit__c
            LIMIT 1
        ];
        List<RecordType> recordTypes = [
            SELECT Id
            FROM RecordType
            WHERE
                sObjectType = 'Rentable_Item__c' AND
                DeveloperName = 'Guest_Parking'
            LIMIT 1
        ];
        Rentable_Item__c newRentableItem = new Rentable_Item__c(Building__c = existedUnits[0].Building__c, Development__c = existedUnits[0].Building__r.Development_Site__c, RecordTypeId = recordTypes[0].Id);
        insert newRentableItem;
        Social_Space__c newSocialSpaceItem = new Social_Space__c(Building__c = existedUnits[0].Building__c);
        insert newSocialSpaceItem;

        Test.startTest();
            System.assertEquals(0, CalendarCmpController.getEvents(String.valueOf(newRentableItem.Id), 'Rentable_Item__c').size());
            System.assertEquals(0, CalendarCmpController.getEvents(String.valueOf(newSocialSpaceItem.Id), 'Social_Space__c').size());
            CalendarCmpController.createBooking(String.valueOf(newRentableItem.Id), System.JSON.serialize(contact[0]), String.valueOf(Datetime.now()), String.valueOf(Datetime.now().addHours(1)), 'Test');
            CalendarCmpController.createBooking(String.valueOf(newSocialSpaceItem.Id), System.JSON.serialize(contact[0]), '2011-01-10T00:00:00.000Z', '2011-01-10T04:00:00.000Z', 'Test');
        Test.stopTest();

        List<Booked_Rentable_Item__c> bookings = [
            SELECT Id
            FROM Booked_Rentable_Item__c
        ];
        System.assertEquals(2, bookings.size());
    }


}