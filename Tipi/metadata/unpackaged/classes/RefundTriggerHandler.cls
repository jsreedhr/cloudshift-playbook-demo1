/**
    * @Name:        RefundTriggerHandler
    * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 23/05/2018    Andrey Korobovich    Created Class
*/

public with sharing class RefundTriggerHandler extends TriggerHandler{
    protected override void afterInsert(){
    	RefundTriggerHelper.processAfterInsert(Trigger.newMap);
    }
}