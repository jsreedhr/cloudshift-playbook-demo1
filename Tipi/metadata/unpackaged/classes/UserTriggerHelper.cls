/**
 * @Name:        UserTriggerHelper
 * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 03/08/2018    Patrick Fischer    Created Class
 */
public with sharing class UserTriggerHelper {

    public static void processAfterInsert(Map<Id, User> newMap) {
        updateFlagOnContactFuture(newMap.keySet());
    }

    public static void processAfterUpdate(Map<Id, User> oldMap, Map<Id, User> newMap) {
        updateFlagOnContactFuture(newMap.keySet());
    }

    /**
     * To set the has active community user flag on the specified contact related to the user.
     * future method was implemented to get around the Mixed_DML_Operation exception
     *
     * @param userIds Set of Ids of user records
     */
    @Future
    public static void updateFlagOnContactFuture(Set<Id> userIds) {
        List<Contact> contactsToInsert = new List<Contact>();

        Map<Id, User> usersByContactIds = new Map<Id, User>();
        for (User user : [SELECT Id, IsActive, ContactId FROM User WHERE Id IN :userIds]) {
            if (user.ContactId != null) {
                usersByContactIds.put(user.ContactId, user);
            }
        }

        for (Contact contact : [SELECT Id, Has_Active_Community_User__c FROM Contact WHERE Id IN :usersByContactIds.keySet()]) {
            if( (contact.Has_Active_Community_User__c && !usersByContactIds.get(contact.Id).IsActive) ||
                    (!contact.Has_Active_Community_User__c && usersByContactIds.get(contact.Id).IsActive) ) {
                contact.Has_Active_Community_User__c = usersByContactIds.get(contact.Id).IsActive;
                contactsToInsert.add(contact);
            }
        }

        if(!contactsToInsert.isEmpty()) {
            update contactsToInsert;
        }
    }
}