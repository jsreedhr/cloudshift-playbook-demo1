/**
    * @Name:        UnitEventTriggerHandler
    * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 07/06/2018    Andrey Korobovich    Created Class
*/

public with sharing class UnitEventTriggerHandler extends TriggerHandler {
    protected override void afterInsert() {            
        UnitEventTriggerHelper.processAfterInsert(Trigger.New);
    }
}