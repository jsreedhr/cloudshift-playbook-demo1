/**
 * @Name:        PaymentTriggerHandler
 * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
 *
 * @author:      Andrey Korobovich
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 14/05/2018    Andrey Korobovich    Created Class
 * 21/06/2018	Patrick Fischer		 Added beforeInsert()
 */
public with sharing class PaymentTriggerHandler extends TriggerHandler {

	protected override void beforeInsert(){ 
		PaymentTriggerHelper.processBeforeInsert(Trigger.new);
	}

	protected override void afterInsert(){
		PaymentTriggerHelper.processAfterInsert(Trigger.newMap);
	}

	protected override void afterUpdate(){
		PaymentTriggerHelper.processAfterUpdate(Trigger.oldMap, Trigger.newMap);
	}

	protected override void beforeDelete(){
		PaymentTriggerHelper.processBeforeDelete(Trigger.oldMap);
	}

	protected override void afterUndelete(){
		PaymentTriggerHelper.processAfterUnDelete(Trigger.newMap);
	}
}