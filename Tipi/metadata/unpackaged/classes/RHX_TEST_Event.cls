@IsTest
public class RHX_TEST_Event {
	static testMethod void RHX_Testmethod() {
        List<SObject> sourceList = new List<Event>();
        sourceList.add(
                new Event(
                        DurationInMinutes = 1,
                        ActivityDateTime = System.now()
                )
        );
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}