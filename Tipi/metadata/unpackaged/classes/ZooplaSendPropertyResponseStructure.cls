/**
    * @Name:        ZooplaSendPropertyResponseStructure
    * @Description: Wrapper class for creating response structure from RightMove
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 03/05/2018    Andrey Korobovich    Created Class
*/

public with sharing class ZooplaSendPropertyResponseStructure {
	public String status;
	public String listing_reference;
	public String listing_etag;
	public String url;
	public String schema;
	public String method;
	public String profile;
	public Boolean new_listing;
	public String error_name;
	public String error_advice;
	public Error[] errors;

	public class Error {
		public String message;
		public String path;
	}

	public static ZooplaSendPropertyResponseStructure parse(String json) {
		return (ZooplaSendPropertyResponseStructure) System.JSON.deserialize(json, ZooplaSendPropertyResponseStructure.class);
	}
}