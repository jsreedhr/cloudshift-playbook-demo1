/**
    * @Name:        PaymentAuthorisationController
    * @Description: Lease Object Standard Controller Extension for Reservation Payment Quick Action
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 08/05/2018    Eugene Konstantinov    Created Class
    * 15/06/2018    Eugene Konstantinov    Refactored class, added getReservedUnitName method
*/
public without sharing class PaymentAuthorisationController {
    private static final Set<String> RESERVED_STATUSES = new Set<String> { 'Notice Given – New Lease', 'Unoccupied – New Lease', 'Awaiting Reservation Payment' };

    @AuraEnabled
    public static String getAllContactsForLease(String contractId) {
        List<Contract> currentLease = [
            SELECT Id, SBQQ__Opportunity__c
            FROM Contract
            WHERE
                Id = :Id.valueOf(contractId)
            LIMIT 1
        ];

        if (currentLease != null && !currentLease.isEmpty()) {
            return getAllContactsForOpportunity(String.valueOf(currentLease[0].SBQQ__Opportunity__c));
        }

        Map<String, Object> resultMap = new Map<String, Object>();
        return System.JSON.serialize(resultMap);
    }

    @AuraEnabled
    public static Opportunity getOpportunity(String leaseId) {
        Contract lease = [
            SELECT Id, SBQQ__Opportunity__c, SBQQ__Opportunity__r.Leased_Reserved_Unit__r.Name
            FROM Contract
            WHERE
                Id = :leaseId
            LIMIT 1
        ];
        return lease.SBQQ__Opportunity__r;
    }

    @AuraEnabled
    public static String getReservedUnitName(String opportunityId) {
        Opportunity opportunityRecord = [
            SELECT Id, Leased_Reserved_Unit__r.Name
            FROM Opportunity
            WHERE
                Id = :opportunityId
            LIMIT 1
        ];
        return opportunityRecord.Leased_Reserved_Unit__r.Name;
    }

    @AuraEnabled
    public static String getAllContactsForOpportunity(String opportunityId) {
        Opportunity currentOpportunity = [
            SELECT Id, Primary_Contact__c, Tenant_2__c, Tenant_3__c, Tenant_4__c, Tenant_5__c,
                Leased_Reserved_Unit__c, Leased_Reserved_Unit_Status__c, Leased_Reserved_Unit__r.Name, Leased_Reserved_Unit__r.Rent__c,
                Leased_Reserved_Unit__r.Building__c, Leased_Reserved_Unit__r.Building__r.Development_Site__c, Leased_Reserved_Unit__r.Description__c
            FROM Opportunity
            WHERE
                Id = :Id.valueOf(opportunityId)
            LIMIT 1
        ];
        Set<Id> contactIds = new Set<Id>();
        if (currentOpportunity.Primary_Contact__c != null) {
            contactIds.add(currentOpportunity.Primary_Contact__c);
        }
        if (currentOpportunity.Tenant_2__c != null) {
            contactIds.add(currentOpportunity.Tenant_2__c);
        }
        if (currentOpportunity.Tenant_3__c != null) {
            contactIds.add(currentOpportunity.Tenant_3__c);
        }
        if (currentOpportunity.Tenant_4__c != null) {
            contactIds.add(currentOpportunity.Tenant_4__c);
        }
        if (currentOpportunity.Tenant_5__c != null) {
            contactIds.add(currentOpportunity.Tenant_5__c);
        }

        List<Contact> contactsForOpportunity = (List<Contact>)new ContactSelector().getRecordsByIds(contactIds, new Set<String>{
            Contact.Name.getDescribe().getName(),
            Contact.FirstName.getDescribe().getName(),
            Contact.LastName.getDescribe().getName(),
            Contact.Email.getDescribe().getName(),
            Contact.Current_Lease__c.getDescribe().getName(),
            Contact.Leased_Reserved_Unit__c.getDescribe().getName(),
            'Account.Name',
            'Leased_Reserved_Unit__r.Name'
        });

        Map<Id, Tipi_Payments__c> paymentByContactId = PaymentAuthorisationHelper.getPaymentsForContactsInOpportunity(currentOpportunity.Id);

        Map<String, Object> resultMap = new Map<String, Object>();
        resultMap.put('Contacts', contactsForOpportunity);
        resultMap.put('Payments', paymentByContactId);
        resultMap.put('OpportunityId', opportunityId);

        return System.JSON.serialize(resultMap);
    }

    @AuraEnabled
    public static Boolean createAndSendPayment(String contactId, String opportunityId) {
        Opportunity opportunityRecord = [
            SELECT Id, Leased_Reserved_Unit__c, Leased_Reserved_Unit_Status__c
            FROM Opportunity
            WHERE
                Id = :opportunityId
        ];
        if (opportunityRecord.Leased_Reserved_Unit__c != null && !RESERVED_STATUSES.contains(opportunityRecord.Leased_Reserved_Unit_Status__c)) {
            PaymentAuthorisationHelper.createAndSendReservationPayment(contactId, opportunityId);
            return true;
        } else {
            return false;
        }
    }
}