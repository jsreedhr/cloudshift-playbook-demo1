/**
 * @Name:        PaymentConfiguratorServiceTest
 * @Description: Test class for PaymentConfiguratorService
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 23/05/2018    Patrick Fischer    Created Class
 */
@IsTest
public with sharing class PaymentConfiguratorServiceTest {

    /**
     * Method tested: constructPaymentRecords()
     * Expected result: Successfully constructed Tipi_Payments__c records with concession first X months
     */
    private static testMethod void constructPaymentRecordsConcessionXMonthsSuccess() {
        SBQQ__QuoteLine__c quoteLine = [SELECT Id, Monthly_Concession__c, Concession_Length__c FROM SBQQ__QuoteLine__c
        WHERE SBQQ__Product__r.Product_Type__c = 'Unit' LIMIT 1];
        quoteLine.Monthly_Concession__c = 100;
        quoteLine.Concession_Length__c = 3;
        update quoteLine;

        Contract contract = PaymentConfiguratorService.getContractById([SELECT Id FROM Contract LIMIT 1].Id);
        PaymentDataWrapper paymentDataWrapper = getCompletedPaymentDataWrapper(contract);
 
        Test.startTest();
        List<Tipi_Payments__c> tipiPayments = PaymentConfiguratorService.constructPaymentRecords(contract, paymentDataWrapper);
        Test.stopTest();

        System.assertEquals(15, tipiPayments.size(), 'Expecting 15 Tipi Payment Records to be created. ' +
                'Tenant 1: 13 payments + 1 Deposit + 1 Upfront');
    }

    /**
     * Method tested: constructPaymentRecords()
     * Expected result: Successfully constructed Tipi_Payments__c records with concession for first month
     */
    private static testMethod void constructPaymentRecordsConcessionFirstMonthSuccess() {
        SBQQ__QuoteLine__c quoteLine = [SELECT Id, Off_First_Month__c FROM SBQQ__QuoteLine__c
        WHERE SBQQ__Product__r.Product_Type__c = 'Unit' LIMIT 1];
        quoteLine.Off_First_Month__c = 100000000;
        update quoteLine;

        Contract contract = PaymentConfiguratorService.getContractById([SELECT Id FROM Contract LIMIT 1].Id);
        contract.StartDate = Date.newInstance(2018, 4, 1);
        update contract;
        PaymentDataWrapper paymentDataWrapper = getCompletedPaymentDataWrapper(contract);

        Test.startTest();
        List<Tipi_Payments__c> tipiPayments = PaymentConfiguratorService.constructPaymentRecords(contract, paymentDataWrapper);
        Test.stopTest();

        System.assertEquals(15, tipiPayments.size(), 'Expecting 15 Tipi Payment Records to be created.' +
                'Tenant 1: 13 payments + 1 Deposit + 1 Upfront');
    }

    /**
     * Method tested: constructPaymentRecords()
     * Expected result: Successfully constructed Tipi_Payments__c records with concession for specific month
     */
    private static testMethod void constructPaymentRecordsConcessionSpecificMonthSuccess() {
        String freeMonth = 'July';
        SBQQ__QuoteLine__c quoteLine = [SELECT Id, Free_Month__c, SBQQ__Quote__c FROM SBQQ__QuoteLine__c
        WHERE SBQQ__Product__r.Product_Type__c = 'Unit' LIMIT 1];
        quoteLine.Free_Month__c = freeMonth;
        update quoteLine;

        SBQQ__Quote__c quote = [SELECT Id, Free_Month_Text__c FROM SBQQ__Quote__c
        WHERE Id = :quoteLine.SBQQ__Quote__c LIMIT 1];
        quote.Free_Month_Text__c = freeMonth;
        update quote;

        Contract contract = PaymentConfiguratorService.getContractById([SELECT Id FROM Contract LIMIT 1].Id);
        PaymentDataWrapper paymentDataWrapper = getCompletedPaymentDataWrapper(contract);

        Test.startTest();
        List<Tipi_Payments__c> tipiPayments = PaymentConfiguratorService.constructPaymentRecords(contract, paymentDataWrapper);
        Test.stopTest();

        System.assertEquals(15, tipiPayments.size(), 'Expecting 15 Tipi Payment Records to be created. ' +
                'Tenant 1: 13 payments + 1 Deposit + 1 Upfront');
    }

    /**
     * Method tested: constructPaymentRecords()
     * Expected result: Successfully constructed Tipi_Payments__c records considering StartDate after 15th of Month
     */
    private static testMethod void constructPaymentRecordsEndOfMonthStartSuccess() {
        Contract contract = PaymentConfiguratorService.getContractById([SELECT Id FROM Contract LIMIT 1].Id);
        contract.StartDate = Date.newInstance(2018, 4, 20);
        update contract;

        PaymentDataWrapper paymentDataWrapper = getCompletedPaymentDataWrapper(contract);
        paymentDataWrapper.tenants[0].rentUpfront = 0; // removing upfront payment
        paymentDataWrapper.tenants[0].monthsUpfront = 0; // removing upfront payment

        Test.startTest();
        List<Tipi_Payments__c> tipiPayments = PaymentConfiguratorService.constructPaymentRecords(contract, paymentDataWrapper);
        Test.stopTest();

        System.assertEquals(13, tipiPayments.size(), 'Expecting 13 Tipi Payment Records to be created. ' +
                'Tenant 1: 12 payments + 1 Deposit');
    }

    /**
     * Method tested: constructPaymentRecords()
     * Expected result: Successfully constructed Tipi_Payments__c records considering StartDate after 15th of Month
     */
    private static testMethod void constructPaymentRecordsShortTermSuccess() {
        Contract contract = PaymentConfiguratorService.getContractById([SELECT Id FROM Contract LIMIT 1].Id);
        contract.ContractTerm = 1;
        update contract;
        contract = PaymentConfiguratorService.getContractById(contract.Id);

        PaymentDataWrapper paymentDataWrapper = getCompletedPaymentDataWrapper(contract);

        Test.startTest();
        List<Tipi_Payments__c> tipiPayments = PaymentConfiguratorService.constructPaymentRecords(contract, paymentDataWrapper);
        Test.stopTest();

        System.assertEquals(3, tipiPayments.size(), 'Expecting 3 Tipi Payment Records to be created. ' +
                'Tenant 1: 1 payments + 1 Deposit + 1 Upfront');
    }

    /**
     * Method tested: constructBookedRentableItems()
     * Expected result: Exception thrown and handled during construction of Booked Rentable Items
     */
    private static testMethod void constructBookedRentableItemsError() {
        Contract contract = PaymentConfiguratorService.getContractById([SELECT Id FROM Contract LIMIT 1].Id);
        PaymentDataWrapper paymentDataWrapper = getCompletedPaymentDataWrapper(contract);
        paymentDataWrapper.tenants[0].storageString = 'falsevalue';

        Test.startTest();
        List<Booked_Rentable_Item__c> rentableItems = PaymentConfiguratorService.constructBookedRentableItems(contract, paymentDataWrapper);
        Test.stopTest();

        System.assertEquals(0, rentableItems.size(), 'Expecting 0 Booked Rentable Items to be created.');
    }

    /**
     * Method tested: constructBookedRentableItems()
     * Expected result: Successfull during construction of Booked Rentable Items
     */
    private static testMethod void constructBookedRentableItemsSuccess() {
        Contract contract = PaymentConfiguratorService.getContractById([SELECT Id FROM Contract LIMIT 1].Id);
        PaymentDataWrapper paymentDataWrapper = getCompletedPaymentDataWrapper(contract);

        List<SBQQ__QuoteLine__c> quoteLines = PaymentConfiguratorController.getQuoteLineItemsByQuoteId(contract.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__c);
        quoteLines[0].SBQQ__EndDate__c = null;
        update quoteLines;

        List<SBQQ__Quote__c> quotes = [SELECT Id, SBQQ__EndDate__c, SBQQ__StartDate__c FROM SBQQ__Quote__c WHERE Id = :quoteLines[0].SBQQ__Quote__c];
        quotes[0].SBQQ__EndDate__c = null;
        quotes[0].SBQQ__StartDate__c = null;
        update quotes;

        Test.startTest();
        List<Booked_Rentable_Item__c> rentableItems = PaymentConfiguratorService.constructBookedRentableItems(contract, paymentDataWrapper);
        Test.stopTest();

        System.assertEquals(2, rentableItems.size(), 'Expecting 2 Booked Rentable Items to be created');
    }

    /**
     * Method tested: getQueueIdByName()
     * Expected result: Queue Id not found; Exception handled by returning null
     */
    private static testMethod void getQueueIdByNameError() {
        Test.startTest();
        Id queueId = PaymentConfiguratorService.getQueueIdByName(null);
        Test.stopTest();

        System.assertEquals(null, queueId, 'Expecting Payment Type to not be mapped and handled by returning Other');
    }

    /**
     * Method tested: getPaymentTypeFromContract()
     * Expected result: Payment Type is not mapped and handled by returning 'Other'
     */
    private static testMethod void getPaymentTypeFromContractError() {
        Contract contract = PaymentConfiguratorService.getContractById([SELECT Id FROM Contract LIMIT 1].Id);
        contract.Unit_Type__c = 'LLR';
        contract.Type__c = 'LLR';
        update contract;

        Test.startTest();
        String paymentType = PaymentConfiguratorService.getPaymentTypeFromContract(contract);
        Test.stopTest();

        System.assertEquals('Other Rental Income', paymentType, 'Expecting Payment Type to not be mapped and handled by returning Other');
    }

    /**
     * Method tested: getContractById()
     * Expected result: Exception thrown and handled
     */
    private static testMethod void getContractByIdEndOfMonthStartError() {
        Test.startTest();
        Contract contract = PaymentConfiguratorService.getContractById(null);
        Test.stopTest();

        System.assertEquals(null, contract, 'Expecting null to be returned');
    }

    /**
     * Method tested: getTipiPaymentsByContactIds()
     * Expected result: Retrieve Tipi_Payments__c reservation records
     */
    private static testMethod void getTipiPaymentsByContactIdsSuccess() {
        List<Contact> contacts = [SELECT Id FROM Contact];
        Id developmentId = [SELECT Id FROM Development__c LIMIT 1].Id;
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].Id;

        List<Tipi_Payments__c> tipiPayments = new List<Tipi_Payments__c>();
        tipiPayments.add(new Tipi_Payments__c(
                Amount__c = 400,
                Total_Remaining_Balance__c = 400,
                Contact__c = contacts[1].Id,
                Development__c = developmentId,
                Opportunity__c = opportunityId,
                Type__c = Utils_Constants.PAYMENT_TYPE_RESERVATION
        ));
        insert tipiPayments;
        Map<Id, Tipi_Payments__c> tipiPaymentsByContactId = PaymentConfiguratorService.getTipiPaymentsByContactIds(
                new Map<Id, Tipi_Payments__c> {
                        contacts[0].Id => null,
                        contacts[1].Id => null,
                        contacts[2].Id => null
                }
        );

        Test.startTest();
        Map<Id, Tipi_Payments__c> tipiPaymentsByContactIdResponse = PaymentConfiguratorService.getTipiPaymentsByContactIds(tipiPaymentsByContactId);
        Test.stopTest();

        System.assertEquals(null, tipiPaymentsByContactIdResponse.get(contacts[0].Id), 'Expecting No Reservation Payment to exist');
        System.assertEquals(400, tipiPaymentsByContactIdResponse.get(contacts[1].Id).Amount__c, 'Expecting Reservation Payment to exist');
        System.assertEquals(null, tipiPaymentsByContactIdResponse.get(contacts[2].Id), 'Expecting No Reservation Payment to exist');
    }
    
    /**
    * Method tested: normalisePaymentRecordsForBRI()
    * Expected result: Retrieve Tipi_Payments__c records for leased Rentable Items
    */
    private static testMethod void getTipiPaymentsForBRISuccess() {
        List<Contact> contacts = [SELECT Id FROM Contact LIMIT 1];
        List<Contract> contracts = [SELECT Id FROM Contract LIMIT 1];
        Rentable_Item__c item  = [SELECT Id FROM Rentable_Item__c LIMIT 1];

        Booked_Rentable_Item__c bookedItem = new Booked_Rentable_Item__c(
            Contract__c = contracts[0].Id,
            Contact__c = contacts[0].Id,
            End_Date__c =  Date.newInstance(2018, 10, 13),
            Start_Date__c = Date.newInstance(2018, 06, 5),
            Rentable_Item__c = item.Id,
            Status__c = 'Allocated'
        );
        insert bookedItem;
        List<Booked_Rentable_Item__c> items = selectRentableItmes(new Set<Id>{bookedItem.Id});

        List<Tipi_Payments__c> tipiPayments = [SELECT Id FROM Tipi_Payments__c WHERE Rentable_Item__c = :item.Id];
        System.assertEquals(0, tipiPayments.size(), 'Expecting No Payment to exist');

        Test.startTest();
        	List<Tipi_Payments__c> listPaymentsForBRI = PaymentConfiguratorService.normalisePaymentRecordsForBRI(items[0], null, 50, 50, Utils_Constants.PAYMENT_GATEWAY_DD);
        Test.stopTest();

        System.assertNotEquals(0, listPaymentsForBRI.size(), 'Expecting Payment to exist');
    }
    
    /**
    * Method tested: getAuthorisationsMapByContact()
    * Expected result: Retrieve authorisationsByContactId map
    */
    private static testMethod void getAuthorisationsByContactIdSuccess() {
        List<Contact> contacts = [SELECT Id FROM Contact LIMIT 1];
        
        Test.startTest();
        Map<Id, Id> authorisationsByContactId = PaymentConfiguratorService.getAuthorisationsMapByContact(new Set<Id>{contacts[0].Id});
        Test.stopTest();

        System.assertEquals(true, authorisationsByContactId.containsKey(contacts[0].Id), 'Expecting Contact Id to exist');
        System.assertNotEquals(null, authorisationsByContactId.get(contacts[0].Id), 'Expecting Authorisation to exist');
    }

    /**
     * Helper method to setup testdata including Developments, Buildings, Units, Accounts, Opportunities, Products, Quotes
     */
    @TestSetup
    private static void testSetup() {
        // Testdata Items
        List<Development__c> developments = TestFactoryPayment.getDevelopments(1, true);
        List<Building__c> buildings = TestFactoryPayment.getBuildings(developments[0], 1, true);
        List<Unit__c> units = TestFactoryPayment.getUnits(buildings[0], 1, true);
        List<Pricebook2> pricebooks = TestFactoryPayment.getPricebooks(1, true);
        List<Product2> products = new List<Product2>();
        products.addAll(TestFactoryPayment.getProducts('Unit', 3000, 1, false));
        products.addAll(TestFactoryPayment.getProducts('Storage', 200, 4, false));
        products.addAll(TestFactoryPayment.getProducts('Parking', 400, 2, false));
        insert products;
        List<PricebookEntry> pricebookEntries = TestFactoryPayment.getPricebookEntries(products, true);
        List<Rentable_Item__c> rentableItems = TestFactoryPayment.getRentableItems(products, buildings[0], true);

        // Transactional Items
        List<Account> accounts = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 11, true);
        List<Opportunity> opportunities = TestFactoryPayment.getOpportunities(accounts[0], contacts, 1, true);
        List<SBQQ__Quote__c> quotes = TestFactoryPayment.getQuotes(accounts[0], contacts[0], opportunities[0], 1, true);
        List<SBQQ__QuoteLine__c> quoteLines = TestFactoryPayment.getQuoteLines(quotes[0], products, true);
        List<Contract> contracts = TestFactoryPayment.getContracts(accounts[0], quotes[0], opportunities[0], units[0], 1, true);
        List<asp04__Authorisation__c> authorisations = TestFactoryPayment.getAuthorisations(contacts, true);
    }

    private static PaymentDataWrapper getCompletedPaymentDataWrapper(Contract contract) {
        PaymentDataWrapper paymentDataWrapper = PaymentConfiguratorController.initialiseComponent(contract.Id);
        paymentDataWrapper.tenants[0].rent = 3000;
        paymentDataWrapper.tenants[0].monthsUpfront = 3;
        paymentDataWrapper.tenants[0].rentUpfront = paymentDataWrapper.tenants[0].monthsUpfront * paymentDataWrapper.rent;
        paymentDataWrapper.tenants[0].upfrontPaymentType = 'SagePay';
        List<Rentable_Item__c> rentableItems = [SELECT Id FROM Rentable_Item__c];
        paymentDataWrapper.tenants[0].storageString = rentableItems[0].Id + ';' + rentableItems[1].Id;

        // Save Operation within LEX Component
        return PaymentConfiguratorController.savePaymentConfiguration(JSON.serialize(paymentDataWrapper), contract.Id);
    }
    
    private static List<Booked_Rentable_Item__c> selectRentableItmes(Set<Id> ids) {
        List<Booked_Rentable_Item__c> res =  new List<Booked_Rentable_Item__c>();
        for (Booked_Rentable_Item__c item : [ SELECT Id, Start_Date__c, End_Date__c, Rentable_Item__c, Rentable_Item__r.Cost__c, Rentable_Item__r.Building__c, Contract__c, Contact__c,
                                               Rentable_Item__r.Product__r.Size__c, Rentable_Item__r.Name, Rentable_Item__r.Product__r.Product_Type__c, Contact__r.Name, Contract__r.Type__c,
                                               Rentable_Item__r.Development__c, Contract__r.SBQQ__Opportunity__c, Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__c, Contract__r.Payment_Config_Done__c,
                                               Contract__r.SBQQ__Opportunity__r.Deposit__c, Contract__r.StartDate, Contract__r.EndDate, Contract__r.Unit__r.Building__r.Development_Site__c,
                                               Contract__r.SBQQ__Opportunity__r.Primary_Contact__r.Name, Contract__r.Actual_Tenancy_End_Date__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Checkbox__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Length__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Concession_Amount__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_Checkbox__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Off_First_Month__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Checkbox__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Free_Month_Text__c,
                                               Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__r.Unit_Rent_Rollup__c
                                               FROM Booked_Rentable_Item__c
                                               WHERE Id IN: ids]) {
            if (item.Rentable_Item__c == null) {
                Trigger.newMap.get(item.Id).addError(Label.Rentable_Item_field_should_be_filled);
            } else if (item.Contract__r.SBQQ__Opportunity__c == null) {
                Trigger.newMap.get(item.Id).addError(Label.Opportunity_field_on_related_Lease_should_be_filled);
            }  else if (item.Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__c == null) {
                Trigger.newMap.get(item.Id).addError(Label.Primary_Quote_field_on_Opportunity);
            } else {
                res.add(item);
            }
        }
        return res;
    }
}