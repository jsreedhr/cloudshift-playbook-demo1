/**
    * @Name:        PaymentController
    * @Description: Contact Object Standard Controller Extension for Payment
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 11/05/2018    Andrey Korobovich    Created Class
*/

public with sharing class PaymentController {
	private static final String DEPOSIT_TYPE = 'Deposit';
	private static final String AWAITING_STATUS = 'Awaiting submission';

	private static Contact currentContact;

	@AuraEnabled
	public static String payDeposit(Id contactId) {
		Contact currentContact = [SELECT Id, FirstName, LastName, Email FROM Contact WHERE Id =: contactId];
		List<Tipi_Payments__c> paymentsList = [
		        SELECT Id, Type__c, Status__c, Amount__c, Contact__c, Contact__r.Account.Name, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email,
		        Building__c, Building__r.City__c, Building__r.Country__c, Building__r.Street__c, Building__r.Postcode__c
		        FROM Tipi_Payments__c
		        WHERE
		        	Type__c = :DEPOSIT_TYPE
		        AND
		            Status__c = :AWAITING_STATUS
		        AND
		            Contact__c = :currentContact.Id
		        LIMIT 10000 
		];
		Payment_Setting__mdt defaultPaymentSetting = [
		    SELECT Id, MasterLabel, DeveloperName, Building_Id__c, Reservation_Fee__c
		    FROM Payment_Setting__mdt
		    WHERE MasterLabel = 'Default'
		    LIMIT 1 ];
		Map<Contact, asp04__Payment__c> cntPaymentsMap = new Map<Contact, asp04__Payment__c>();
		Map<Tipi_Payments__c, asp04__Payment__c> paymentsMap = new Map<Tipi_Payments__c, asp04__Payment__c>();
		for (Tipi_Payments__c payment : paymentsList) {
			asp04__Payment__c newPayment = new asp04__Payment__c();
			newPayment.asp04__Amount__c = payment.Amount__c != null ? payment.Amount__c: defaultPaymentSetting.Reservation_Fee__c;
			newPayment.asp04__Account_Name__c = payment.Contact__r.Account.Name;
			newPayment.asp04__Billing_Address_Country__c = payment.Building__r.Country__c;
			newPayment.asp04__Billing_Address_City__c = payment.Building__r.City__c;
			newPayment.asp04__Billing_Address_PostalCode__c = payment.Building__r.Postcode__c;
			newPayment.asp04__Billing_Address_Street__c = payment.Building__r.Street__c;
			newPayment.asp04__Email__c = payment.Contact__r.Email;
			newPayment.asp04__First_Name__c = payment.Contact__r.FirstName;
			newPayment.asp04__Last_Name__c = payment.Contact__r.LastName;
			newPayment.asp04__Payment_Route_Options__c = 'Card';
			newPayment.asp04__Payment_Route_Selected__c = 'Card'; 
			paymentsMap.put(payment, newPayment);
			cntPaymentsMap.put(currentContact, newPayment);
		}
		insert paymentsMap.values();
		for(Tipi_Payments__c payment : paymentsMap.keySet()){
			payment.Payment__c = paymentsMap.get(payment).Id;
		}
		update new List<Tipi_Payments__c> (paymentsMap.keySet());
		EmailHandler.sendEmailToContact(cntPaymentsMap, 'Payment URL');
		return JSON.serialize(cntPaymentsMap.keySet());
	}
}