/**
 * @Name:        BookedRentableItemTriggerTest
 * @Description: Test class for BookedRentableItemTrigger functionality
 *
 * @author:      Andrey Korobovich
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 21/05/2018    Andrey Korobovich       Created Class
 * 20/06/2018    Eugene Konstantinov     Refactored Class; Additional scenarios added; Additional code coverage
 * 19/07/2018    Patrick Fischer         Added test coverage for Concession calculation
 */
@IsTest
public with sharing class BookedRentableItemTriggerTest {

    private static testMethod void testBookedRentableItemTrigger() {
        Booked_Rentable_Item__c item = [SELECT Id, Status__c, Rentable_Item__c, Contract__r.SBQQ__Opportunity__r.SBQQ__PrimaryQuote__c FROM Booked_Rentable_Item__c LIMIT 1];
        Map<Id, Booked_Rentable_Item__c> oldMap = new Map<Id, Booked_Rentable_Item__c>();
        Map<Id, Booked_Rentable_Item__c> newMap = new Map<Id, Booked_Rentable_Item__c>();
        oldMap.put(item.Id, item);
        Booked_Rentable_Item__c updatedItem = item.clone(true, true, true, true);
        updatedItem.Status__c = 'Allocated';
        update updatedItem;
        newMap.put(updatedItem.Id, updatedItem);
        BookedRentableItemTriggerHelper.processAfterUpdate(oldMap, newMap);
    }

    private static testMethod void testBookedRentableItemTriggerWithConcession() {
        Booked_Rentable_Item__c item = [SELECT Id, Status__c, Quote_Line__c FROM Booked_Rentable_Item__c LIMIT 1];

        List<Tipi_Payments__c> tipiPayments = [SELECT Id FROM Tipi_Payments__c];
        System.assert(tipiPayments.isEmpty(), 'No Tipi Payments exist');

        Test.startTest();
        item.Status__c = 'Allocated';
        item.Quote_Line__c = [SELECT Id FROM SBQQ__QuoteLine__c LIMIT 1].Id;
        update item;
        Test.stopTest();

        tipiPayments = [SELECT Id FROM Tipi_Payments__c];
        System.assertEquals(13, tipiPayments.size(), 'Expecting 13 Tipi Payments to exist');
    } 

    public static testMethod void testSocialSpaceBooking() {
        List<Contact> contactRecord = [SELECT Id, Current_Lease__c FROM Contact LIMIT 1];
        List<Building__c> buildings = [SELECT Id, Development_Site__c FROM Building__c LIMIT 1];
        Social_Space__c item  = new Social_Space__c(
            Type__c = 'Lounge',
            Building__c = buildings[0].Id,
            Name = 'Test',
            Cost__c = 25
        );
        insert item;

        Booked_Rentable_Item__c bookedItem = new Booked_Rentable_Item__c(
            Contract__c = contactRecord[0].Current_Lease__c,
            Contact__c = contactRecord[0].Id,
            Booking_End__c =  Datetime.now().addHours(4),
            Booking_Start__c = Datetime.now(),
            Social_Space__c = item.Id,
            Status__c = 'Pending Document Send'
        );
        insert bookedItem;
        List<Tipi_Payments__c> paymentsList = [SELECT Id, Payment_Date__c FROM Tipi_Payments__c];
        System.assertEquals(1, paymentsList.size());
    }

    @TestSetup
    public static void testSetup() {

        // Testdata Items
        insert new Global_Application_Settings__c(Run_Process_Builder__c = false, Run_Validation_Rules__c =  false, Run_Flow__c = false, Run_Workflow_Rules__c = false, Run_Triggers__c = true);
        List<Development__c> developments = TestFactoryPayment.getDevelopments(1, true);
        List<Building__c> buildings = TestFactoryPayment.getBuildings(developments[0], 1, true);
        List<Unit__c> units = TestFactoryPayment.getUnits(buildings[0], 1, true);
        List<Pricebook2> pricebooks = TestFactoryPayment.getPricebooks(1, true);
        List<Product2> products = new List<Product2>();
        products.addAll(TestFactoryPayment.getProducts('Storage', 200, 4, false));
        products.addAll(TestFactoryPayment.getProducts('Parking', 400, 2, false));
        insert products;
        List<PricebookEntry> pricebookEntries = TestFactoryPayment.getPricebookEntries(products, true);
        List<Rentable_Item__c> rentableItems = TestFactoryPayment.getRentableItems(products, buildings[0], true);

        // Transactional Items
        List<Account> accounts = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 11, true);
        List<Opportunity> opportunities = TestFactoryPayment.getOpportunities(accounts[0], contacts, 1, true);
        List<SBQQ__Quote__c> quotes = TestFactoryPayment.getQuotes(accounts[0], contacts[0], opportunities[0], 1, true);
        List<SBQQ__QuoteLine__c> quoteLines = TestFactoryPayment.getQuoteLines(quotes[0], products, false);
        quoteLines[0].SBQQ__StartDate__c = Date.newInstance(2017, 4, 13);
        quoteLines[0].SBQQ__EndDate__c = Date.newInstance(2018, 4, 12);
        quoteLines[0].Off_First_Month__c = 20;
        insert quoteLines;
        List<Contract> contracts = TestFactoryPayment.getContracts(accounts[0], quotes[0], opportunities[0], units[0], 1, true);

        Rentable_Item__c rentableItem  = new Rentable_Item__c(
                RecordTypeId = Schema.SObjectType.Rentable_Item__c.getRecordTypeInfosByName().get('Parking').getRecordTypeId(),
                Building__c = buildings[0].Id,
                Name = 'Test',
                Product__c = products[0].Id,
                Development__c = developments[0].Id
        );
        insert rentableItem;

        Booked_Rentable_Item__c bookedRentableItem = new Booked_Rentable_Item__c(
                Contract__c = contracts[0].Id,
                Contact__c = contacts[0].Id,
                Start_Date__c = quoteLines[0].SBQQ__StartDate__c,
                End_Date__c = quoteLines[0].SBQQ__EndDate__c,
                Rentable_Item__c = rentableItem.Id,
                Status__c = 'Pending Document Send'
        );
        insert bookedRentableItem;
    }
}