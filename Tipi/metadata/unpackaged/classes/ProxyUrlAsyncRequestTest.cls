/**
    * @Name:        ProxyUrlAsyncRequestTest
    * @Description: Test class for ProxyUrlAsyncRequest Queueable class with callouts
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 13/04/2018    Eugene Konstantinov    Created Class
*/
@isTest
public class ProxyUrlAsyncRequestTest {
	@isTest
	static void testCallout() {
		Test.setMock(HttpCalloutMock.class, new ProxyUrlMockHttpResponseGenerator());

		Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);

		List<ProxyUrlWrapper> proxyUrlWrapperList = new List<ProxyUrlWrapper>();
		proxyUrlWrapperList.add(new ProxyUrlWrapper(
					'recordId',
					'contentId',
					'contentVersionId',
					UserInfo.getOrganizationId(),
					'distributionPublicUrl',
					'File.ext'
            ));

		Test.startTest();
			ProxyUrlAsyncRequest request = new ProxyUrlAsyncRequest(proxyUrlWrapperList);
			System.enqueueJob(request);
		Test.stopTest();

	}
}