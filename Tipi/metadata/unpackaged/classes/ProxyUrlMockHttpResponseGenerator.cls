/**
    * @Name:        ProxyUrlMockHttpResponseGenerator
    * @Description: Class for generating Response for ProxyUrlAsyncRequestTest
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 13/04/2018    Eugene Konstantinov    Created Class
*/
@isTest
global class ProxyUrlMockHttpResponseGenerator implements HttpCalloutMock{

	global HTTPResponse respond(HTTPRequest req) {
		System.assertEquals('POST', req.getMethod());

		Map<String, List<sObject>> testObjects = TestDataFactoryForNewViewingController.getTestUnits();
		List<Unit__c> units = testObjects.get('unit');
		Unit_File__c newUnitFile = new Unit_File__c(Unit__c = units[0].Id);
		insert newUnitFile;

		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody('[{"recordId":"' + newUnitFile.Id +'","url":"https://images.png"}]');
		res.setStatusCode(200);
        res.setStatus('OK'); 
		return res;
	}

}