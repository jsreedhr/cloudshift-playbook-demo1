/**
    * @Name:        RighMoveSendPropertyStructure
    * @Description: Wrapper class for creating request structure to RightMove
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 06/04/2018    Eugene Konstantinov    Created Class
*/
public class RighMoveSendPropertyStructure {

	public class Details {
		public String summary;
		public String description;
		public List<String> features;
		public Integer bedrooms;
		public Integer bathrooms;
		public List<Integer> parking;
		public Integer year_built;
		public Integer internal_area;
		public Integer internal_area_unit;
		public Sizing sizing;
		public Integer floors;
		public Integer entrance_floor;
		public Integer condition;
		public List<Integer> accessibility;
		public List<Integer> heating;
		public Integer furnished_type;
		public Boolean business_for_sale;
		public List<Integer> comm_use_class;
		public Boolean pets_allowed;

		public Details(String summary,String description,List<String> features,Integer bedrooms, Integer bathrooms,
					   List<Integer> parking, Integer year_built, Integer internal_area, Integer internal_area_unit,
					   Sizing sizing, Integer floors,Integer entrance_floor,Integer condition, List<Integer> accessibility,
					   List<Integer> heating,Integer furnished_type,Boolean business_for_sale,List<Integer> comm_use_class,
					   Boolean pets_allowed
					   ) {
			this.summary = summary;
			this.description = description;
			this.features = features;
			this.bedrooms = bedrooms;
			this.bathrooms = bathrooms;
			this.parking = parking;
			this.year_built = year_built;
			this.internal_area = internal_area;
			this.internal_area_unit = internal_area_unit;
			this.sizing = sizing;
			this.floors = floors;
			this.entrance_floor = entrance_floor;
			this.condition = condition;
			this.accessibility = accessibility;
			this.heating = heating;
			this.furnished_type = furnished_type;
			this.business_for_sale = business_for_sale;
			this.comm_use_class = comm_use_class;
			this.pets_allowed = pets_allowed;
		}
	}

	public class Address {
		public String house_name_number;
		public String address_2;
		public String address_3;
		public String address_4;
		public String town;
		public String postcode_1;
		public String postcode_2;
		public String display_address;
		public Decimal latitude;
		public Decimal longitude;
		public Decimal pov_latitude;
		public Decimal pov_longitude;
		public Decimal pov_pitch;
		public Decimal pov_heading;
		public Integer pov_zoom;

		public Address(String house_name_number, String address_2, String address_3,String address_4,String town,String postcode_1,
					   String postcode_2,String display_address,Decimal latitude,Decimal longitude,Decimal pov_latitude,Decimal pov_longitude,
					   Decimal pov_pitch,Decimal pov_heading,Integer pov_zoom){

			this.house_name_number = house_name_number;
			this.address_2 = address_2;
			this.address_3 = address_3;
			this.address_4 = address_4;
			this.town = town;
			this.postcode_1 = postcode_1;
			this.postcode_2 = postcode_2;
			this.display_address = display_address;
			this.latitude = latitude;
			this.longitude = longitude;
			this.pov_latitude = pov_latitude;
			this.pov_longitude = pov_longitude;
			this.pov_pitch = pov_pitch;
			this.pov_heading = pov_heading;
			this.pov_zoom = pov_zoom;
		}
	}

	public class Price_information {
		public Integer price;
		public Integer price_qualifier;
		public Integer deposit;
		public String administration_fee;
		public Integer rent_frequency;
		public Integer price_per_unit_per_annum;

		public Price_information(Integer price,Integer price_qualifier,Integer deposit,String administration_fee,
								 Integer rent_frequency,Integer price_per_unit_per_annum ){

			this.price = price;
			this.price_qualifier = price_qualifier;
			this.deposit = deposit;
			this.administration_fee = administration_fee;
			this.rent_frequency = rent_frequency;
			this.price_per_unit_per_annum = price_per_unit_per_annum;

		}

	}

	public class Branch {
		public Integer branch_id;
		public Integer channel;
		public Boolean overseas;

		public Branch(Integer branch_id,Integer channel, Boolean overseas ) {

			this.branch_id = branch_id;
			this.channel = channel;
			this.overseas = overseas;

		}
	}

	public class Network {

		public Integer network_id;

		public Network(Integer network_id) {

			this.network_id = network_id;

		}
	}

	public class Media {
		public Integer media_type;
		public String media_url;
		public String caption;
		public Integer sort_order;
		public String media_update_date;

		public Media(Integer media_type, String media_url, String caption, Integer sort_order,String media_update_date ) {

			this.media_type = media_type;
			this.media_url = media_url;
			this.sort_order = sort_order;
			this.media_update_date = media_update_date;

		}
	}

	public class Sizing {

		public Integer minimum;
		public Integer maximum;
		public Integer area_unit;

		public Sizing (Integer minimum, Integer maximum, Integer area_unit) {

			this.minimum = minimum;
			this.maximum = maximum;
			this.area_unit = area_unit;

		}
	}

	public class Property {
		public String agent_ref;
		public Boolean published;
		public Integer property_type;
		public Integer status;
		public String create_date;
		public String update_date;
		public String date_available;
		public Integer contract_months;
		public Integer minimum_term;
		public Integer let_type;
		public Address address;
		public Price_information price_information;
		public Details details;
		public List<Media> media;
		public Principal principal;

		public Property(String agent_ref,Boolean published,Integer property_type,Integer status,
						String create_date,String update_date,String date_available,
						Integer contract_months,Integer minimum_term,Integer let_type,Address address,
						Price_information price_information,Details details,List<Media> media, Principal principal){

			this.agent_ref = agent_ref;
			this.published = published;
			this.property_type = property_type;
			this.status = status;
			this.create_date = create_date;
			this.update_date = update_date;
			this.date_available = date_available;
			this.contract_months = contract_months;
			this.minimum_term = minimum_term;
			this.let_type = let_type;
			this.address = address;
			this.price_information = price_information;
			this.details = details;
			this.media = media;
			this.principal = principal;

		}
	}

	public class Principal {
		public String principal_email_address;
		public Boolean auto_email_when_live;
		public Boolean auto_email_updates;

		public Principal(String principal_email_address,Boolean auto_email_when_live,Boolean auto_email_updates){

			this.principal_email_address = principal_email_address;
			this.auto_email_when_live = auto_email_when_live;
			this.auto_email_updates = auto_email_updates;

		}
	}


	public static RighMoveSendPropertyStructure parse(String json) {
		return (RighMoveSendPropertyStructure) System.JSON.deserialize(json, RighMoveSendPropertyStructure.class);
	}

}