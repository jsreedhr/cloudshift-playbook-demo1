/**
 * @Name:        RelatedContactsPaymentRollupCtrlTest
 * @Description: Test class for RelatedContactsPaymentRollupController
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 18/07/2018    Patrick Fischer    Created Class
 */
@IsTest
public with sharing class RelatedContactsPaymentRollupCtrlTest {

    /**
     * Method tested: initialiseComponent()
     * Expected result: Successfully initialised the Component in Contact context
     */
    private static testMethod void initialiseComponentOnContactSuccess() {
        Id contactId = [SELECT Id FROM Contact LIMIT 1].Id;

        Test.startTest();
        List<RelatedContactsPaymentRollupController.ContactFinancialSummary> contactFinancialSummaries =
                RelatedContactsPaymentRollupController.initialiseComponent(contactId);
        Test.stopTest();

        System.assertEquals(1, contactFinancialSummaries.size(), '1 Contact rows returned');
    }

    /**
     * Method tested: initialiseComponent()
     * Expected result: Successfully initialised the Component in Contract context
     */
    private static testMethod void initialiseComponentOnContractSuccess() {
        Id contractId = [SELECT Id FROM Contract LIMIT 1].Id;

        Test.startTest();
        List<RelatedContactsPaymentRollupController.ContactFinancialSummary> contactFinancialSummaries =
                RelatedContactsPaymentRollupController.initialiseComponent(contractId);
        Test.stopTest();

        System.assertEquals(2, contactFinancialSummaries.size(), '2 Contact rows returned');
    }

    /**
     * Method tested: initialiseComponent()
     * Expected result: Successfully initialised the Component in Opportunity context
     */
    private static testMethod void initialiseComponentOnOpportunitySuccess() {
        Id oppId = [SELECT Id FROM Opportunity LIMIT 1].Id;

        Test.startTest();
        List<RelatedContactsPaymentRollupController.ContactFinancialSummary> contactFinancialSummaries =
                RelatedContactsPaymentRollupController.initialiseComponent(oppId);
        Test.stopTest();

        System.assertEquals(10, contactFinancialSummaries.size(), '10 Contact rows returned');
    }

    /**
     * Method tested: initialiseComponent()
     * Expected result: Exception handled and no Payments returned
     */
    private static testMethod void initialiseComponentFailure() {
        Test.startTest();
        List<RelatedContactsPaymentRollupController.ContactFinancialSummary> contactFinancialSummaries =
                RelatedContactsPaymentRollupController.initialiseComponent(null);
        Test.stopTest();

        System.assert(contactFinancialSummaries.isEmpty(), 'No Contact rows returned');
    }

    /**
     * Helper method to setup testdata including Developments, Buildings, Units, Accounts, Opportunities, Products, Quotes
     */
    @TestSetup
    private static void testSetup() {
        // Testdata Items
        List<Development__c> developments = TestFactoryPayment.getDevelopments(1, true);
        List<Building__c> buildings = TestFactoryPayment.getBuildings(developments[0], 1, true);
        List<Unit__c> units = TestFactoryPayment.getUnits(buildings[0], 1, true);
        List<Pricebook2> pricebooks = TestFactoryPayment.getPricebooks(1, true);
        List<Product2> products = new List<Product2>();
        products.addAll(TestFactoryPayment.getProducts('Unit', 3000, 1, false));
        products.addAll(TestFactoryPayment.getProducts('Storage', 200, 4, false));
        products.addAll(TestFactoryPayment.getProducts('Parking', 400, 2, false));
        insert products;
        List<PricebookEntry> pricebookEntries = TestFactoryPayment.getPricebookEntries(products, true);
        List<Rentable_Item__c> rentableItems = TestFactoryPayment.getRentableItems(products, buildings[0], true);

        // Transactional Items
        List<Account> accounts = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 11, true);
        List<Opportunity> opportunities = TestFactoryPayment.getOpportunities(accounts[0], contacts, 1, true);
        List<SBQQ__Quote__c> quotes = TestFactoryPayment.getQuotes(accounts[0], contacts[0], opportunities[0], 1, true);
        List<SBQQ__QuoteLine__c> quoteLines = TestFactoryPayment.getQuoteLines(quotes[0], products, true);
        List<Contract> contracts = TestFactoryPayment.getContracts(accounts[0], quotes[0], opportunities[0], units[0], 1, false);
        contracts[0].Primary_Contact__c = contacts[0].Id;
        contracts[0].Tenant_2__c = contacts[1].Id;
        insert contracts;
        List<asp04__Authorisation__c> authorisations = TestFactoryPayment.getAuthorisations(contacts, true);

        TestFactory.createSObject(new Global_Application_Settings__c(), true);

        List<Tipi_Payments__c> tipiPayments = new List<Tipi_Payments__c>();
        // add 12 rent payments
        for(Integer i = 0; i < 12; i++) {
            tipiPayments.add(TestFactoryPayment.getTipiPayments(3000, contacts[0].Id, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
                    developments[0].Id, opportunities[0], null,  Date.newInstance(2014, 1, 1).addMonths(i),
                    Utils_Constants.AWAITING_SUBMISSION_STATUS, 'Rent - AST', 1, false)[0]);
        }

        // add credit payment
        Tipi_Payments__c tipiPaymentsCredit = TestFactoryPayment.getTipiPayments(10000, contacts[0].Id,
                Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT, developments[0].Id, opportunities[0], null, Date.newInstance(2014, 1, 1),
                Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, 'Rent - AST', 1, false)[0];
        tipiPayments.add(tipiPaymentsCredit);

        // add reservation payment
        Tipi_Payments__c tipiPaymentsReservation = TestFactoryPayment.getTipiPayments(400, contacts[0].Id,
                '', developments[0].Id, opportunities[0], null, Date.newInstance(2013, 12, 1),
                Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER, Utils_Constants.PAYMENT_TYPE_RESERVATION, 1, false)[0];
        tipiPayments.add(tipiPaymentsReservation);

        // add future payment
        tipiPayments.add(TestFactoryPayment.getTipiPayments(3000, contacts[0].Id, Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT,
                developments[0].Id, opportunities[0], null,  Date.newInstance(2050, 1, 1),
                Utils_Constants.AWAITING_SUBMISSION_STATUS, 'Rent - AST', 1, false)[0]);

        insert tipiPayments;

        // Mock Payment (creates Financial Summary via PB)
        asp04__Payment__c aspPayment = new asp04__Payment__c(
                Contact__c = contacts[0].Id,
                asp04__Amount__c = 1000,
                asp04__Payment_Date__c = Date.today(),
                asp04__Payment_Stage__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER
        );
        insert aspPayment;
    }
}