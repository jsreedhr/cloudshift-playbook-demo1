/**
    * @Name:        BuildingSelector
    * @Description: Class for fetching data stored in salesforce.
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 10/04/2018    Eugene Konstantinov    Created Class
    * 14/06/2018    Eugene Konstantinov    Added getAllRecords method
*/
public with sharing class BuildingSelector extends Selector{
    public static final String objectAPIName = Building__c.SobjectType.getDescribe().getName();

    /**
    * Method to fetch data stored in salesforce
    * @param    Set<Id>       recordIds is a set of Building ids
    *           Set<String>   fields is a set of Strings that determines which fields are queried of the Building object
    * @returns  List<SObject> a list of Building objects that are treated and stored as a generic type
    **/
    public List<SObject> getRecordsByUnitIds(Set<Id> buildingIds, Set<String> fields) {
        if (buildingIds.isEmpty()){return new List<SObject>();}
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' WHERE Id in' + ConstructInClauseString(buildingIds)+ ' LIMIT 10000';
        return Database.query(query);
    }

    /**
    * Method to fetch data stored in salesforce
    * @param    Set<String>   fields is a set of Strings that determines which fields are queried of the Building object
    * @returns  List<SObject> a list of Building objects that are treated and stored as a generic type
    **/
    public List<SObject> getAllRecords(Set<String> fields) {
        String query = 'SELECT ' + ConstructFieldString(fields) + ' FROM ' + objectAPIName + ' LIMIT 10000';
        return Database.query(query);
    }

}