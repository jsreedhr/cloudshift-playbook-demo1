/**
 * @Name:        OpportunityDepositDeductionsController
 * @Description: Aura Controller class to handle server side operations of c:opportunityDepositDeductions component
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 05/06/2018    Patrick Fischer    Created Class
 */
public without sharing class OpportunityDepositDeductionsController {

    private static final String DEPOSIT_STRING = 'Deposit';

    @AuraEnabled
    public static List<Tipi_Payments__c> initialiseComponent(Id recordId) {
        return getTipiDepositPaymentsByOpportunityId(recordId);
    }
    
    private static List<Tipi_Payments__c> getTipiDepositPaymentsByOpportunityId(Id recordId) {
        return [
                SELECT Id, Amount__c, Name, Status__c, Payment_Date__c, Payment__c, Payment__r.Name,
                        Contact__c, Contact__r.FirstName, Contact__r.LastName,
                        Opportunity__r.Total_Refresh_Charged_To_Customer__c, Opportunity__r.Total_Refresh_Charge__c
                FROM Tipi_Payments__c
                WHERE Opportunity__c = :recordId AND Type__c = :DEPOSIT_STRING
                ORDER BY Contact__r.FirstName, Payment_Date__c
        ];
    }
}