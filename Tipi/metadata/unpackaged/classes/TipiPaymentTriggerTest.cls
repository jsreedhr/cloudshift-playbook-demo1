/**
 * @Name:        TipiPaymentTriggerTest
 * @Description: Test class for TipiPaymentTrigger functionality
 *
 * @author:      Eugene Konstantinov
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 24/05/2018    Eugene Konstantinov    Created Class
 * 30/05/2018    Patrick Fischer        Covered undelete & calculateRemainingBalance()
 */
@IsTest
public class TipiPaymentTriggerTest {

    private static final String REFUND_RECORD_TYPE_NAME = 'Refund';
    private static final String PAYMENT_RECORD_TYPE_NAME = 'Payment';
    private static final Map<String, Schema.RecordTypeInfo> recordTypeInfoNameMap = Schema.SObjectType.Tipi_Payments__c.getRecordTypeInfosByName();
    private static final Id refundRecType = recordTypeInfoNameMap.get(REFUND_RECORD_TYPE_NAME).getRecordTypeId();
    private static final Id paymentRecType = recordTypeInfoNameMap.get(PAYMENT_RECORD_TYPE_NAME).getRecordTypeId();

    /**
     * Method tested: updateRefundProperty()
     * Expected result: Successfully updated the refund property after insert/update/delete/undelete
     */
    private static testMethod void refundTipiPaymentsSuccess() {
        //must set the custom settings flags to run all automation items
        Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);
        gas.Run_Process_Builder__c = false;
        update gas;

        List<Account> accs = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accs[0], 1, true);
        Opportunity opp = TestFactoryPayment.getOpportunity(accs[0], contacts[0], true);
        List<Development__c> devs = TestFactoryPayment.getDevelopments(1, true);

        Tipi_Payments__c payment = new Tipi_Payments__c();
        payment.Contact__c = contacts[0].Id;
        payment.RecordTypeId = paymentRecType;
        payment.Type__c = 'Reservation';
        payment.Amount__c = 250;
        payment.Development__c = devs[0].Id;
        payment.Unit__c = contacts[0].Leased_Reserved_Unit__c;
        payment.Opportunity__c = opp.Id;
        payment.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
        insert payment;

        Test.startTest();

        Tipi_Payments__c refund = new Tipi_Payments__c();
        refund.Tipi_Payment__c = payment.Id;
        refund.Contact__c = contacts[0].Id;
        refund.RecordTypeId = refundRecType;
        refund.Development__c = devs[0].Id;
        refund.Type__c = 'Reservation';
        refund.Amount__c = -100;
        refund.Unit__c = contacts[0].Leased_Reserved_Unit__c;
        refund.Opportunity__c = opp.Id;
        refund.Status__c = Utils_Constants.AWAITING_SUBMISSION_STATUS;

        // test insertion
        insert refund;
        Tipi_Payments__c paymentAfterRefund = [SELECT Id, Refund_Amount__c FROM Tipi_Payments__c WHERE Id =: payment.Id];
        System.assertEquals(100, paymentAfterRefund.Refund_Amount__c);

        // test update
        refund.Amount__c = -50;
        refund.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
        update refund;
        Tipi_Payments__c paymentAfterUpdateRefund = [SELECT Id, Refund_Amount__c FROM Tipi_Payments__c WHERE Id =: payment.Id];
        System.assertEquals(0, paymentAfterUpdateRefund.Refund_Amount__c);

        // test delete
        delete refund;
        Tipi_Payments__c paymentAfterDeleteRefund = [SELECT Id, Refund_Amount__c FROM Tipi_Payments__c WHERE Id =: payment.Id];
        System.assertEquals(0, paymentAfterDeleteRefund.Refund_Amount__c);

        Test.stopTest();
    }

    /**
     * Method tested: calculateRemainingBalance()
     * Expected result: Successfully calculated the Total Remaining Balance after insert/update/delete/undelete
     */
    private static testMethod void calculateRemainingBalanceSuccess() {
        // must set the custom settings flags to run all automation items
        Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);
        gas.Run_Process_Builder__c = false;
        update gas;

        List<Account> accs = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accs[0], 1, true);
        Opportunity opp = TestFactoryPayment.getOpportunity(accs[0], contacts[0], true);
        List<Development__c> devs = TestFactoryPayment.getDevelopments(1, true);

        Tipi_Payments__c creditPayment = new Tipi_Payments__c();
        creditPayment.Contact__c = contacts[0].Id;
        creditPayment.Type__c = 'Rent - AST';
        creditPayment.Amount__c = 1000;
        creditPayment.Development__c = devs[0].Id;
        creditPayment.Unit__c = contacts[0].Leased_Reserved_Unit__c;
        creditPayment.Opportunity__c = opp.Id;
        creditPayment.Credit_Payment_Type__c = Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT;
        creditPayment.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
        creditPayment.Calculation_Long__c = 'Abc';
        insert creditPayment;

        // Assert before any rent payments
        creditPayment = [SELECT Id, Amount__c, Total_Remaining_Balance__c FROM Tipi_Payments__c WHERE Id = :creditPayment.Id];
        System.assertEquals(1000, creditPayment.Amount__c);
        System.assertEquals(1000, creditPayment.Total_Remaining_Balance__c);

        Test.startTest();

        Tipi_Payments__c rentPayment = new Tipi_Payments__c();
        rentPayment.Tipi_Payment__c = creditPayment.Id;
        rentPayment.Contact__c = contacts[0].Id;
        rentPayment.Type__c = 'Rent - AST';
        rentPayment.Development__c = devs[0].Id;
        rentPayment.Amount__c = 100;
        rentPayment.Unit__c = contacts[0].Leased_Reserved_Unit__c;
        rentPayment.Opportunity__c = opp.Id;
        rentPayment.Credit_Payment_Type__c = Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT;
        rentPayment.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
        insert rentPayment;

        // Assert after first rent payment
        creditPayment = [SELECT Id, Amount__c, Total_Remaining_Balance__c FROM Tipi_Payments__c WHERE Id = :creditPayment.Id];
        System.assertEquals(1000, creditPayment.Amount__c);
        System.assertEquals(900, creditPayment.Total_Remaining_Balance__c);

        Tipi_Payments__c rentPayment2 = new Tipi_Payments__c();
        rentPayment2.Tipi_Payment__c = creditPayment.Id;
        rentPayment2.Contact__c = contacts[0].Id;
        rentPayment2.Type__c = 'Rent - AST';
        rentPayment2.Development__c = devs[0].Id;
        rentPayment2.Amount__c = 300;
        rentPayment2.Unit__c = contacts[0].Leased_Reserved_Unit__c;
        rentPayment2.Opportunity__c = opp.Id;
        rentPayment2.Credit_Payment_Type__c = Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT;
        rentPayment2.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
        insert rentPayment2;

        Test.stopTest();

        // Assert after second rent payment
        creditPayment = [SELECT Id, Amount__c, Total_Remaining_Balance__c FROM Tipi_Payments__c WHERE Id = :creditPayment.Id];
        System.assertEquals(1000, creditPayment.Amount__c);
        System.assertEquals(600, creditPayment.Total_Remaining_Balance__c);
    }

    /**
     * Method tested: validateParentTipiCredit()
     * Expected result: Successfully validate if a Payment is assigned with insufficient Credit on the Parent Tipi Payment
     */
    private static testMethod void validateParentTipiCreditInsertFailure() {
        // must set the custom settings flags to run all automation items
        Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);
        gas.Run_Process_Builder__c = false;
        update gas;

        List<Account> accs = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accs[0], 1, true);
        Opportunity opp = TestFactoryPayment.getOpportunity(accs[0], contacts[0], true);
        List<Development__c> devs = TestFactoryPayment.getDevelopments(1, true);

        Tipi_Payments__c creditPayment = new Tipi_Payments__c();
        creditPayment.Contact__c = contacts[0].Id;
        creditPayment.Type__c = 'Rent - AST';
        creditPayment.Amount__c = 1000;
        creditPayment.Development__c = devs[0].Id;
        creditPayment.Unit__c = contacts[0].Leased_Reserved_Unit__c;
        creditPayment.Opportunity__c = opp.Id;
        creditPayment.Credit_Payment_Type__c = Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT;
        creditPayment.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
        creditPayment.Calculation_Long__c = 'Abc';
        insert creditPayment;

        // Assert before any rent payments
        creditPayment = [SELECT Id, Amount__c, Total_Remaining_Balance__c FROM Tipi_Payments__c WHERE Id = :creditPayment.Id];
        System.assertEquals(1000, creditPayment.Amount__c);
        System.assertEquals(1000, creditPayment.Total_Remaining_Balance__c);

        Test.startTest();
        Database.SaveResult sr = null;
        Tipi_Payments__c rentPayment = new Tipi_Payments__c();
        rentPayment.Tipi_Payment__c = creditPayment.Id;
        rentPayment.Contact__c = contacts[0].Id;
        rentPayment.Type__c = 'Rent - AST';
        rentPayment.Development__c = devs[0].Id;
        rentPayment.Amount__c = 1234;
        rentPayment.Unit__c = contacts[0].Leased_Reserved_Unit__c;
        rentPayment.Opportunity__c = opp.Id;
        rentPayment.Credit_Payment_Type__c = Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT;
        rentPayment.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
        try {
            sr = Database.insert(rentPayment, false);
        } catch (Exception e) {
            // continue
        }
        Test.stopTest();

        // Assert failure insertion
        System.assertEquals(1, sr.getErrors().size(), 'Expecting record to not be inserted');
        System.assert(sr.getErrors()[0].getMessage().startsWith(
                System.Label.ERROR_INSUFFICIENT_FUNDS_AVAILABLE_ON_PARENT_TIPI_PAYMENT),
                'Expecting record to not be inserted');

        // Assert after first rent payment
        creditPayment = [SELECT Id, Amount__c, Total_Remaining_Balance__c FROM Tipi_Payments__c WHERE Id = :creditPayment.Id];
        System.assertEquals(1000, creditPayment.Amount__c);
        System.assertEquals(1000, creditPayment.Total_Remaining_Balance__c);
    }

    /**
     * Method tested: validateParentTipiCredit()
     * Expected result: Successfully validate if a Payment is assigned with insufficient Credit on the Parent Tipi Payment
     */
    private static testMethod void validateParentTipiCreditUpdateFailure() {
        // must set the custom settings flags to run all automation items
        Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);
        gas.Run_Process_Builder__c = false;
        update gas;

        List<Account> accs = TestFactoryPayment.addAccounts(1, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accs[0], 1, true);
        Opportunity opp = TestFactoryPayment.getOpportunity(accs[0], contacts[0], true);
        List<Development__c> devs = TestFactoryPayment.getDevelopments(1, true);

        Tipi_Payments__c creditPayment = new Tipi_Payments__c();
        creditPayment.Contact__c = contacts[0].Id;
        creditPayment.Type__c = 'Rent - AST';
        creditPayment.Amount__c = 1000;
        creditPayment.Development__c = devs[0].Id;
        creditPayment.Unit__c = contacts[0].Leased_Reserved_Unit__c;
        creditPayment.Opportunity__c = opp.Id;
        creditPayment.Credit_Payment_Type__c = Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT;
        creditPayment.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
        creditPayment.Calculation_Long__c = 'Abc';
        insert creditPayment;

        // Assert before any rent payments
        creditPayment = [SELECT Id, Amount__c, Total_Remaining_Balance__c FROM Tipi_Payments__c WHERE Id = :creditPayment.Id];
        System.assertEquals(1000, creditPayment.Amount__c);
        System.assertEquals(1000, creditPayment.Total_Remaining_Balance__c);

        Test.startTest();
        Database.SaveResult sr = null;
        Tipi_Payments__c rentPayment = new Tipi_Payments__c();
        rentPayment.Tipi_Payment__c = creditPayment.Id;
        rentPayment.Contact__c = contacts[0].Id;
        rentPayment.Type__c = 'Rent - AST';
        rentPayment.Development__c = devs[0].Id;
        rentPayment.Amount__c = 950;
        rentPayment.Unit__c = contacts[0].Leased_Reserved_Unit__c;
        rentPayment.Opportunity__c = opp.Id;
        rentPayment.Credit_Payment_Type__c = Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT;
        rentPayment.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
        insert rentPayment;

        // Assert after first rent payment
        creditPayment = [SELECT Id, Amount__c, Total_Remaining_Balance__c FROM Tipi_Payments__c WHERE Id = :creditPayment.Id];
        System.assertEquals(1000, creditPayment.Amount__c);
        System.assertEquals(50, creditPayment.Total_Remaining_Balance__c);

        rentPayment.Amount__c = 40000; // setting error here
        try {
            sr = Database.update(rentPayment, false);
        } catch (Exception e) {
            // continue
        }
        Test.stopTest();

        // Assert failure insertion
        System.assertEquals(1, sr.getErrors().size(), 'Expecting record to not be updated');
        System.assert(sr.getErrors()[0].getMessage().startsWith(
                System.Label.ERROR_INSUFFICIENT_FUNDS_AVAILABLE_ON_PARENT_TIPI_PAYMENT),
                'Expecting record to not be updated');

        // Assert after first rent payment
        creditPayment = [SELECT Id, Amount__c, Total_Remaining_Balance__c FROM Tipi_Payments__c WHERE Id = :creditPayment.Id];
        System.assertEquals(1000, creditPayment.Amount__c);
        System.assertEquals(50, creditPayment.Total_Remaining_Balance__c);
    }
}