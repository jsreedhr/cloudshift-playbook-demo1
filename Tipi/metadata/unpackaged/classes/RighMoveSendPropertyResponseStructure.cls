/**
    * @Name:        RighMoveSendPropertyResponseStructure
    * @Description: Wrapper class for creating response structure from RightMove
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 11/04/2018    Eugene Konstantinov    Created Class
*/
public class RighMoveSendPropertyResponseStructure {
	public String message;
	public Boolean success;
	public Error[] errors;
	public Warning[] warnings;
	public String request_timestamp;
	public String response_timestamp;
	public String request_id;
	public Property property;

	public class Property {
		public String agent_ref;
		public Integer rightmove_id;
		public String change_type;
		public String rightmove_url;
	}

	public class Warning {
		public String warning_code;
		public String warning_value;
		public String warning_description;
	}

	public class Error {
		public String error_code;
		public String error_value;
		public String error_description;
	}

	public static RighMoveSendPropertyResponseStructure parse(String json) {
		return (RighMoveSendPropertyResponseStructure) System.JSON.deserialize(json, RighMoveSendPropertyResponseStructure.class);
	}
}