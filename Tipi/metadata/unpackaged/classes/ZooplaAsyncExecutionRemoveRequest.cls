/**
    * @Name:        ZooplaAsyncExecutionRemoveRequest
    * @Description: Class to create Queueable callout to Zoopla
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 28/05/2018    Andrey Korobovich    Created Class
*/
public with sharing class ZooplaAsyncExecutionRemoveRequest implements Queueable, Database.AllowsCallouts {

	private static final String CLASS_NAME = 'ZooplaAsyncExecutionRemoveRequest';
	private static final String SUCCESS_RESPONSE = 'OK';
	private static final String OCCUPIED_STATUS = 'Occupied';
	private static final String UNOCCUPIED_STATUS = 'Unoccupied - New Lease';

	private String method;
	private String url;
	private Map<String, String> additionalHeader;
	private String body;
	private String certificate;
	private Unit__c unit;
	private Boolean isLeaseExist;

	public ZooplaAsyncExecutionRemoveRequest(String method, String url, Map<String, String> additionalHeader, String body, String certificate, Unit__c unit, Boolean isLeaseExist) {
		this.method = method;
		this.isLeaseExist = isLeaseExist;
		this.url = url;
		this.additionalHeader = additionalHeader;
		this.body = body;
		this.certificate = certificate;
		this.unit = unit;
	}



	public void execute(QueueableContext context) {
        HTTPResponse response = sendRequest(this.method, this.url, this.additionalHeader, this.body, this.certificate);
        if (response != null) {
            String responseString = response.getBody();
            System.debug(responseString);
            if (response.getStatus() == SUCCESS_RESPONSE) {
                List<Unit__c> unitToUpdate = (List<Unit__c>) new UnitSelector().getRecordsByIds(new Set<Id>{this.unit.Id},new Set<String>{
                       Unit__c.Name.getDescribe().getName()
                });
                if (unitToUpdate != null && !unitToUpdate.isEmpty()) {
                	if(this.isLeaseExist){
                		unitToUpdate[0].Status__c = OCCUPIED_STATUS;
                	} else {
                		unitToUpdate[0].Status__c = UNOCCUPIED_STATUS;

                	}
                	unitToUpdate[0].Zoopla_URL__c = '';
                    update unitToUpdate;
                }
            }
        }
	}

	/**
	 * Send Request Method
	 * @param  method  string value
	 * @param  url  string value of endpoint
	 * @param  additionalHeader  map of additional header information
	 * @param  body  string value
	 * @param  certificate  string name of cetificate
	 *
	 * @returns HTTPResponse response from endpoint
	 */
	private HTTPResponse sendRequest(String method, String url, Map<String, String> additionalHeader, String body, String certificate){
		try {
			HttpRequest Request = new HttpRequest();
			Request.setMethod(method);
			Request.setEndpoint(url);
			Request.setTimeout(120000);

			if (!String.isEmpty(certificate)) {
				Request.setClientCertificateName(certificate);
			}
			for (String key : additionalHeader.keySet()) {
				Request.setHeader(key, (String)additionalHeader.get(key));
			}
			if (!String.isEmpty(body)) {
				Request.setBody(body);
			}
			System.debug(body);

			Http http = new Http();
			HTTPResponse response = http.send(Request);
			System.debug(response.getBody());
			return response;
		} catch (CalloutException ex) {
			CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'Callout Exception:' + CLASS_NAME + ex.getMessage());
		} catch (Exception ex) {
			CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'Exception:' + CLASS_NAME + ex.getMessage());
		} finally {
			CustomLogger.save();
		}
		return null;
	}
}