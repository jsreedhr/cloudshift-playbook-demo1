/**
 * @Name:        AuthorisationTriggerTest
 * @Description: Test class to cover AuthorisationTrigger class
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 03/07/2018    Patrick Fischer    Created Class
 */
@IsTest
public with sharing class AuthorisationTriggerTest {

    /**
     * Method tested: beforeInsert()
     * Expected result: Successfully set the asp04__Customer_ID__c on Authorisation records
     */
    private static testMethod void beforeInsertSuccess() {
        List<Contact> contacts = [SELECT Id, Leased_Reserved_Unit__r.Building__r.Development_Site__c FROM Contact];

        Test.startTest();
        List<asp04__Authorisation__c> authorisations = TestFactoryPayment.getAuthorisations(contacts, false);
        System.assertEquals(null, authorisations[0].asp04__Customer_ID__c, 'Expecting Customer Id to not be populated');
        insert authorisations;
        Test.stopTest();

        authorisations = [SELECT Id, asp04__Customer_ID__c FROM asp04__Authorisation__c];
        System.assertEquals('1234', authorisations[0].asp04__Customer_ID__c, 'Expecting Customer Id to be populated');
    }

    /**
     * Method tested: beforeUpdate()
     * Expected result: Successfully set the asp04__Customer_ID__c on Authorisation records
     */
    private static testMethod void beforeUpdateSuccess() {
        List<Contact> contacts = [SELECT Id, Leased_Reserved_Unit__r.Building__r.Development_Site__c FROM Contact];
        List<asp04__Authorisation__c> authorisations = TestFactoryPayment.getAuthorisations(contacts, true);
        authorisations = [SELECT Id, asp04__Customer_ID__c FROM asp04__Authorisation__c];
        System.assertEquals('1234', authorisations[0].asp04__Customer_ID__c, 'Expecting Customer Id to be populated');

        Test.startTest();
        authorisations[0].asp04__Customer_ID__c = null;
        System.assertEquals(null, authorisations[0].asp04__Customer_ID__c, 'Expecting Customer Id to not be populated');
        update authorisations;
        Test.stopTest();

        authorisations = [SELECT Id, asp04__Customer_ID__c FROM asp04__Authorisation__c];
        System.assertEquals('1234', authorisations[0].asp04__Customer_ID__c, 'Expecting Customer Id to be populated');
    }

    /**
     * Method tested: afterUpdate()
     * Expected result: Successfully set the DD_Authorisation_In_Force__c checkbox on related Contact records
     */
    private static testMethod void afterUpdateSuccess() {
        List<Contact> contacts = [SELECT Id, DD_Authorisation_In_Force__c FROM Contact];
        System.assert(!contacts[0].DD_Authorisation_In_Force__c, 'Expecting No Authorisations to be In Force');
        List<asp04__Authorisation__c> authorisations = TestFactoryPayment.getAuthorisations(contacts, false);

        Test.startTest();

        authorisations[0].asp04__Status__c = 'Pending';
        insert authorisations;

        contacts = [SELECT Id, DD_Authorisation_In_Force__c FROM Contact];
        System.assert(!contacts[0].DD_Authorisation_In_Force__c, 'Expecting No Authorisations to be In Force');

        authorisations[0].asp04__Status__c = 'In Force';
        update authorisations;

        contacts = [SELECT Id, DD_Authorisation_In_Force__c FROM Contact];
        System.assert(contacts[0].DD_Authorisation_In_Force__c, 'Expecting Authorisation to be In Force');

        authorisations[0].asp04__Status__c = 'Cancelled';
        update authorisations;

        contacts = [SELECT Id, DD_Authorisation_In_Force__c FROM Contact];
        System.assert(!contacts[0].DD_Authorisation_In_Force__c, 'Expecting No Authorisations to be In Force');

        Test.stopTest();
    }

    @TestSetup
    private static void testSetup() {
        List<Account> accs = TestFactoryPayment.addAccounts(1, true);
        List<Contact> conts = TestFactoryPayment.addContacts(accs, 1, true);
        Opportunity opp = new Opportunity(
                Name = 'Test Opportunity',
                AccountId = accs[0].Id,
                Primary_Contact__c = conts[0].Id,
                Type = '1 Bed',
                CloseDate = Date.newInstance(2018, 4, 7),
                StageName = 'Interested'
        );
        insert opp;
        Unit__c unit = [SELECT Id FROM Unit__c LIMIT 1];
        Development__c development = [SELECT Id, Name FROM Development__c LIMIT 1];

        TestFactory.createSObject(new Global_Application_Settings__c(), true);

        Asperato_Customer_Id__c aspCustomerId = new Asperato_Customer_Id__c(
                Name = development.Name,
                Development_Id__c = development.Id,
                Customer_Id__c = '1234'
        );
        insert aspCustomerId;
    }
}