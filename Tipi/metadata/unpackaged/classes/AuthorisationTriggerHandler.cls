/**
 * @Name:        AuthorisationTriggerHandler
 * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 03/07/2018    Patrick Fischer    Created Class
 */
public with sharing class AuthorisationTriggerHandler extends TriggerHandler { 

    protected override void beforeInsert() {
        AuthorisationTriggerHelper.processBeforeInsert((List<asp04__Authorisation__c>) Trigger.new);
    }

    protected override void afterInsert() {
        AuthorisationTriggerHelper.processAfterInsert((Map<Id, asp04__Authorisation__c>) Trigger.newMap);
    }

    protected override void beforeUpdate() {
        AuthorisationTriggerHelper.processBeforeUpdate((List<asp04__Authorisation__c>) Trigger.new, (Map<Id, asp04__Authorisation__c>) Trigger.oldMap);
    }

    protected override void afterUpdate() {
        AuthorisationTriggerHelper.processAfterUpdate((Map<Id, asp04__Authorisation__c>) Trigger.newMap, (Map<Id, asp04__Authorisation__c>) Trigger.oldMap);
    }
}