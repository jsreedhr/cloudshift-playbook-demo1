/**
 * @Name:        YardiResidentTriggerHelper
 * @Description: This is helper class, will hold all methods and functionalities that are to be build for Yardi Resident Trigger
 *
 * @author:      Jared Watson
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * --------------------------------------------------------------------------------
 * 28/06/2018    Jared Watson       Created Class
 * 06/07/2018    Patrick Fischer    Created added typeToUnitType mapping, added conditional logic for optional Lookups
 * 09/07/2018    Patrick Fischer    Changed Lease field mapping, add conditions for Month-To-Month mapping
 * 10/07/2018    Patrick Fischer    Added logic to create Tipi Payment schedule using Yardi_Data_Lease_Charge__c records
 */
public with sharing class YardiResidentTriggerHelper {

    private static String country;
    private static Id priceBookId;
    private static Map<String, String> typeToUnitType;
    private static Map<String,Unit__c> unitsByExternalId;
    private static Id userId;
    private static Date thresholdDate = Date.newInstance(2018, 7, 31);

    /**
     * To process inserted Yardi Resident records
     *
     * @param newMap map of inserted Yardi_Data_resident__c records
     */
    public static void processAfterInsert(Map<Id, Yardi_Data_Resident__c> newMap) {
        setStaticVariables();
        List<Contract> contracts = new List<Contract>();
        Savepoint sp = Database.setSavepoint();

        try {
            for(Yardi_Data_Resident__c ydr : newMap.values()) {
                contracts.add(constructLease(ydr));
            }
            insert contracts;

            // update Contract status to 'Activated' (Salesforce restriction -> Contracts have to be created as 'Draft')
            for(Contract contract : contracts) {
                contract.Status = 'Activated';
            }
            update contracts;

            List<Tipi_Payments__c> tipiPaymentsToInsert = constructTipiPayments(contracts);
            insert tipiPaymentsToInsert;

            CustomLogger.save();

        } catch (Exception e) {
            CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, e);
            Database.rollback(sp);
            CustomLogger.save();
            throw e;
        }
    }

    /**
     * To construct a Lease with Yaerdi Data
     *
     * @param yardiResident Yardi_Data_Resident__c record from which to construct a Contract
     *
     * @return single Contract record
     */
    public static Contract constructLease(Yardi_Data_Resident__c yardiResident) {

        String accountExternalId = [SELECT External_Id__c, Account.External_Id__c FROM Contact WHERE External_Id__c = :yardiResident.Resident_Id__c ].Account.External_Id__c;
        Account accReference = new Account(External_Id__c = accountExternalId);

//        Account accReference = new Account(External_Id__c = yardiResident.Resident_Id__c);
        Contact conPrimaryReference = new Contact(External_Id__c = yardiResident.Resident_Id__c);
        Opportunity oppReference = new Opportunity(External_Id__c = yardiResident.Resident_Id__c);
        Unit__c unitReference = new Unit__c(External_Id__c = unitsByExternalId.get(yardiResident.Unit__c).External_Id__c);

        Contract c = new Contract();
        c.Account = accReference;
        c.Bedrooms__c = unitsByExternalId.get(yardiResident.Unit__c).Unit_Type__c;
        c.BillingStreet = yardiResident.Address_Line_1__c + ' ' + yardiResident.Address_Line_2__c + ' ' + yardiResident.Address_Line_3__c;
        c.BillingCity = yardiResident.City__c;
        c.BillingPostalCode = yardiResident.Postcode__c;
        c.BillingCountry = country;
        c.CompanySignedId = userId;
        c.CompanySignedDate = yardiResident.Lease_Signed_date__c;
        c.ContractTerm = yardiResident.Lease_Term__c.intValue();
        c.CustomerSigned = conPrimaryReference;
        c.CustomerSignedDate = yardiResident.Lease_Signed_date__c;
        c.Description = 'Mapped from Yardi record (' + yardiResident.Resident_Id__c + '): '
                + 'Deposit: ' + yardiResident.Deposit__c + ', '
                + 'Market Rent: ' + yardiResident.Market_Rent__c + ', '
                + 'Other Charges: ' + yardiResident.Other_Charges__c;
        c.Deposit_Paid__c = false;
        if(yardiResident.Deposit__c != 0 && yardiResident.Deposit__c != null) {
            c.Deposit_Paid__c = true;
        }
        c.Landlord__c = userId;
        c.Month_to_Month__c = yardiResident.Month_to_Month__c;
        c.Name = yardiResident.First_Name__c + ' ' + yardiResident.Last_Name__c;
        c.OwnerId = userId;
        c.Payment_Config_Done__c = true;
        c.Preferred_Rent__c = yardiResident.Rent_Monthly__c;
        c.Pricebook2Id = priceBookId;
        c.Primary_Contact__r = conPrimaryReference;
        c.Rent_Increase__c = 3;
        c.StartDate = yardiResident.Lease_From_Date__c;
        c.Status = 'Draft'; // Salesforce restriction -> have to create Contracts as 'Draft' then update to 'Activated'
        c.Type__c = yardiResident.Lease_Type__c;
        c.Unit_Type__c = typeToUnitType.get(yardiResident.Lease_Type__c);
        c.Unit__r = unitReference;
        c.External_Id__c = yardiResident.Resident_Id__c;

        //set the other tenant info
        if(yardiResident.Primary_Guarantor__c != null) {
            c.Primary_Contact_Guarantor__r = new Contact(External_Id__c = yardiResident.Primary_Guarantor__c);
        }
        if(yardiResident.Tenant_2__c != null) {
            c.Tenant_2__r = new Contact(External_Id__c = yardiResident.Tenant_2__c);
        }
        if(yardiResident.Tenant_3__c != null) {
            c.Tenant_3__r = new Contact(External_Id__c = yardiResident.Tenant_3__c);
        }
        if(yardiResident.Tenant_4__c != null) {
            c.Tenant_4__r = new Contact(External_Id__c = yardiResident.Tenant_4__c);
        }
        if(yardiResident.Tenant_5__c != null) {
            c.Tenant_5__r = new Contact(External_Id__c = yardiResident.Tenant_5__c);
        }

        //set the CPQ Fields
        c.SBQQ__Opportunity__r = oppReference;
        c.SBQQ__AmendmentRenewalBehavior__c = 'Latest End Date';
        c.SBQQ__DefaultRenewalContactRoles__c = true;
        c.SBQQ__DefaultRenewalPartners__c = true;
        c.SBQQ__DisableAmendmentCoTerm__c = false;
        c.SBQQ__MasterContract__c = false;
        c.SBQQ__PreserveBundleStructureUponRenewals__c = true;
        c.SBQQ__RenewalForecast__c = false;
        c.SBQQ__RenewalQuoted__c = false;
        c.SBQQ__RenewalTerm__c = yardiResident.Lease_Term__c;

        // non-writable fields
        // c.ActivatedById = userId;
        // c.ActivatedDate = yardiResident.Move_In__c;
        // c.EndDate = yardiResident.Lease_to_Date__c;
        // c.SBQQ__OpportunityPricebookId__c = priceBookId;

        return c;
    }

    /**
     * To construct all Tipi Payments from Yardi Data
     *
     * @param contracts List of previously (same transaction) inserted Contract records
     *
     * @return List of Tipi_Payments__c records to be inserted
     */
    public static List<Tipi_Payments__c> constructTipiPayments(List<Contract> contracts) {
        List<Tipi_Payments__c> tipiPayments = new List<Tipi_Payments__c>();

        // get ResidentId's (= LeaseCode's) for inserted records
        Set<String> residentIds = new Set<String>();
        for(Contract contract : contracts) {
            residentIds.add(contract.External_Id__c);
        }
        contracts = getContractsWithRelatedDetails(contracts);
        Map<String, List<Yardi_Data_Ledger_Reports__c>> ledgerRowsByLeaseCode = getLedgerRowsByLeaseCode(residentIds);
        Map<String, List<Yardi_Data_Lease_Charge__c>> futureLeaseChargesByResidentId = getFutureLeaseChargesByResidentId(residentIds);

        for(Contract contract : contracts) {
            List<Tipi_Payments__c> tipiPaymentsByContract = new List<Tipi_Payments__c>();
            if(ledgerRowsByLeaseCode.containsKey(contract.External_Id__c)) {
                // construct historic records (mapping types, statuses, amounts etc.)
                for(Yardi_Data_Ledger_Reports__c yardiDataLedgerReports : ledgerRowsByLeaseCode.get(contract.External_Id__c)) {
                    Tipi_Payments__c newTipiPayment = constructHistoricTipiPaymentFromLedger(contract, yardiDataLedgerReports);
                    tipiPaymentsByContract.add(newTipiPayment);
                }
            } else {
                CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'YARDI DATA RESIDENT trigger message: ' +
                        'Could not find any Payment records in the \'Ledger Report\' for the Lease/Contract: ' +
                        contract.Name + ' (Name); ' + contract.Id + ' (Id); ' + contract.External_Id__c + ' (External Id). ' +
                        'Please create the payment schedule manually.');
            }

            // construct future records
            if(contract.EndDate > thresholdDate && futureLeaseChargesByResidentId.containsKey(contract.External_Id__c)) {
                tipiPaymentsByContract.addAll(constructFutureTipiPayments(contract, futureLeaseChargesByResidentId.get(contract.External_Id__c)));
            }

            tipiPayments.addAll(tipiPaymentsByContract);
        }

        return tipiPayments;
    }

    /**
     * To construct a schedule of all future Tipi_Payments__c record
     *
     * @param contract related Contract Record
     * @param futureLeaseCharges List of Yardi_Data_Lease_Charge__c records (to generate Tipi Payments in the future)
     *
     * @return List of future Tipi_Payments__c record
     */
    public static List<Tipi_Payments__c> constructFutureTipiPayments(Contract contract, List<Yardi_Data_Lease_Charge__c> futureLeaseCharges) {
        List<Tipi_Payments__c> futureTipiPayments = new List<Tipi_Payments__c>();
        Map<Date, List<Yardi_Data_Lease_Charge__c>> futureLeaseChargesByPaymentDate = getFutureLeaseChargesByPaymentDate(contract, futureLeaseCharges);

        Date paymentStartDate = thresholdDate.addDays(1);
        if(contract.StartDate > thresholdDate) {
            paymentStartDate = contract.StartDate;
        }

        Integer iterator = 0;
        for(Date paymentDate : futureLeaseChargesByPaymentDate.keySet()) {
            Date monthEndDate = Date.newInstance(paymentDate.year(), paymentDate.month(), Date.daysInMonth(paymentDate.year(), paymentDate.month()));

            if(!contract.Month_to_Month__c || (contract.Month_to_Month__c && iterator == 0)) {
                for (Yardi_Data_Lease_Charge__c leaseCharge : futureLeaseChargesByPaymentDate.get(paymentDate)) {

                        Tipi_Payments__c futureTipiPayment = constructFutureTipiPaymentFromChargeRow(contract, leaseCharge, paymentDate, monthEndDate, iterator);

                    // prorata calculation for first and last payment
                    Decimal prorataAmount = futureTipiPayment.Amount__c * 12 / 365;
                    // prorata first month
                    if(iterator == 0 && paymentStartDate.day() != 1) {
                        futureTipiPayment.Amount__c = paymentStartDate.day() * prorataAmount;
                        futureTipiPayment.Amount_without_Concession__c = paymentStartDate.day() * prorataAmount;
                        futureTipiPayment.Calculation_Long__c = '[Pro-rata first month from '
                                + paymentStartDate + '; Amount set to: ' + (paymentStartDate.day() * prorataAmount) + ']';
                    }
                    // prorata last month
                    if(iterator == (futureLeaseChargesByPaymentDate.size() - 1)) {
                        futureTipiPayment.Amount__c = contract.EndDate.day() * prorataAmount;
                        futureTipiPayment.Amount_without_Concession__c = contract.EndDate.day() * prorataAmount;
                        futureTipiPayment.Calculation_Long__c = '[Pro-rata last month until '
                                + contract.EndDate + '; Amount set to: ' + (contract.EndDate.day() * prorataAmount).setScale(2, RoundingMode.HALF_UP) + ']';
                    }

                    futureTipiPayments.add(futureTipiPayment);
                }
            }
            iterator++;
        }

        return futureTipiPayments;
    }

    /**
     * To construct a historic Tipi_Payments__c record from a Ledger Report row
     *
     * @param contract related Contract record
     * @param ydlr Yardi_Date_Ledger_Reports__c record to use as a baseline to construct a Tipi_Payment__c
     *
     * @return single Tipi_Payments__c record
     */
    public static Tipi_Payments__c constructHistoricTipiPaymentFromLedger(Contract contract, Yardi_Data_Ledger_Reports__c ydlr) {

        Decimal amount;
        String creditPaymentType;
        String calculation;

        // setting the default Type
        String type = Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.get('Other');
        if(Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.containsKey(contract.Type__c)) {
            type = Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.get(contract.Type__c);
        }

        if(ydlr.Payment__c != 0 && ydlr.Payment__c != null) {
            // is PAYMENT
            creditPaymentType = Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT;
            amount = ydlr.Payment__c;
            calculation = ydlr.Description__c;
        } else {
            // is CHARGE
            creditPaymentType = Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT;
            amount = ydlr.Total__c;
            calculation = ydlr.Description__c;
            if(ydlr.From__c != null && ydlr.To__c != null) {
                calculation += ' (' + ydlr.From__c.format() + ' - ' + ydlr.To__c.format() + ')';
            }
            String description = ydlr.Description__c.toLowerCase();

            // setting specific Type if applicable
            if(description.contains('reservation')) {
                type = Utils_Constants.PAYMENT_TYPE_RESERVATION;
            } else if (description.contains('deposit')) {
                type = Utils_Constants.PAYMENT_TYPE_DEPOSIT;
            } else if (description.contains('guest parking')) {
                type = 'Guest Parking';
            } else if (description.contains('parking')) {
                type = Utils_Constants.PAYMENT_TYPE_PARKING;
            } else if (description.contains('social space')) {
                type = 'Social Space';
            } else if (description.contains('store')) {
                type = Utils_Constants.PAYMENT_TYPE_STORAGE;
            }
            // else continue with type as mapped from lease -> review LEASE_TO_PAYMENT_TYPE_MAP
        }

        Tipi_Payments__c tipiPayment = new Tipi_Payments__c();
        tipiPayment.Amount__c = amount;
        tipiPayment.Amount_without_Concession__c = amount;
        tipiPayment.Building__c = contract.Unit__r.Building__c;
        tipiPayment.Calculation_Long__c = calculation;
        tipiPayment.Concession_Amount__c = 0;
        tipiPayment.Contact__r = new Contact(External_Id__c = ydlr.Lease_Code__c);
        tipiPayment.Credit_Payment_Type__c = creditPaymentType;
        tipiPayment.Description__c = ydlr.Description__c;
        tipiPayment.Development__c = contract.Unit__r.Building__r.Development_Site__c;
        tipiPayment.External_Id__c = ydlr.Chg_Rec__c;
        tipiPayment.Nth_Month_Payment__c = 9999;
        tipiPayment.Opportunity__c = contract.SBQQ__Opportunity__c;
        tipiPayment.OwnerId = userId;
        tipiPayment.Payment_Date__c = ydlr.Trans_Date__c;
        tipiPayment.Payment_Gateway__c = Utils_Constants.PAYMENT_GATEWAY_OTHER;
        tipiPayment.Payment_Share__c = 1;
        tipiPayment.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
        tipiPayment.Unit__c = contract.Unit__c;
        tipiPayment.Type__c = type;

        return tipiPayment;
    }

    /**
     * To construct a future Tipi_Payments__c record from a Lease Charge row
     *
     * @param contract related Contract record
     * @param ydlr Yardi_Data_Lease_Charge__c record to use as a base-rate to construct a future Tipi_Payment__c
     * @param paymentDateFrom Start of Payment Period i.e. 1/9/2018
     * @param paymentDateTo End of Payment Period i.e. 30/9/2018
     * @param iterator Integer counter of Payment
     *
     * @return single Tipi_Payments__c record
     */
    public static Tipi_Payments__c constructFutureTipiPaymentFromChargeRow(Contract contract, Yardi_Data_Lease_Charge__c ydlc,
            Date paymentDateFrom, Date paymentDateTo, Integer iterator) {

        String type;

        // getting specific type for Yardi_Data_Lease_Charge__c record
        String description = ydlc.Charge_Code__c.toLowerCase();
        if(description.contains('resoff') || description.contains('reservation')) {
            type = Utils_Constants.PAYMENT_TYPE_RESERVATION;
        } else if (description.contains('deposit')) {
            type = Utils_Constants.PAYMENT_TYPE_DEPOSIT;
        } else if (description.contains('rentast') || description.contains('rentdisc') || description.contains('conc2') || description.contains('cc-one') || description.contains('cc-rec')) {
            type = Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.get('AST');
        } else if (description.contains('lounge') || description.contains('social')) {
            type = 'Social Space';
        } else if (description.contains('park')) {
            type = Utils_Constants.PAYMENT_TYPE_PARKING;
        } else if (description.contains('store')) {
            type = Utils_Constants.PAYMENT_TYPE_STORAGE;
        } else if(Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.containsKey(contract.Type__c)) {
            type = Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.get(contract.Type__c);
        } else {
            type = Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.get('Other');
        }

        Tipi_Payments__c tipiPayment = new Tipi_Payments__c();
        tipiPayment.Amount__c = ydlc.Amount__c;
        tipiPayment.Amount_without_Concession__c = ydlc.Amount__c;
        tipiPayment.Building__c = contract.Unit__r.Building__c;
        tipiPayment.Calculation_Long__c = '[' + type + ' (' + paymentDateFrom.format() + ' - ' + paymentDateTo.format() + ') ' + ydlc.Amount__c + ']';
        tipiPayment.Concession_Amount__c = 0;
        tipiPayment.Contact__r = new Contact(External_Id__c = ydlc.Resident_Id__c);
        tipiPayment.Credit_Payment_Type__c = Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT;
        tipiPayment.Description__c = Utils_Constants.PAYMENT_DESCRIPTION;
        tipiPayment.Development__c = contract.Unit__r.Building__r.Development_Site__c;
        tipiPayment.Nth_Month_Payment__c = iterator;
        tipiPayment.Opportunity__c = contract.SBQQ__Opportunity__c;
        tipiPayment.OwnerId = userId;
        tipiPayment.Payment_Date__c = paymentDateFrom;
        tipiPayment.Payment_Gateway__c = Utils_Constants.PAYMENT_GATEWAY_DD;
        tipiPayment.Payment_Share__c = 1;
        tipiPayment.Status__c = Utils_Constants.AWAITING_SUBMISSION_STATUS;
        tipiPayment.Unit__c = contract.Unit__c;
        tipiPayment.Type__c = type;

        return tipiPayment;
    }

    /**
     * To construct a schedule of all future Tipi_Payments__c record
     *
     * @param contract related Contract Record
     * @param yardiDataLeaseCharges List of Yardi_Data_Lease_Charge__c records (to generate Tipi Payments in the future)
     *
     * @return List of future Tipi_Payments__c record
     */
    public static Map<Date, List<Yardi_Data_Lease_Charge__c>> getFutureLeaseChargesByPaymentDate(Contract contract, List<Yardi_Data_Lease_Charge__c> yardiDataLeaseCharges) {
        Map<Date, List<Yardi_Data_Lease_Charge__c>> leaseChargesByPaymentDate = new Map<Date, List<Yardi_Data_Lease_Charge__c>>();

        Date startDate = thresholdDate.addDays(1);
        if(contract.StartDate > thresholdDate) {
            startDate = contract.StartDate;
        }
        Integer monthsToGenerate = startDate.monthsBetween(contract.EndDate) + 1;

        for(Integer i = 0; i < monthsToGenerate; i++) {
            Date currentDate = startDate.toStartOfMonth().addMonths(i);
            List<Yardi_Data_Lease_Charge__c> leaseChargesByMonth = new List<Yardi_Data_Lease_Charge__c>();

            for(Yardi_Data_Lease_Charge__c yardiDataLeaseCharge : yardiDataLeaseCharges) {
                /**
                 * add when startdate (From_Date__c) is TODAY/IN PAST and when enddate is NULL/BLANK or IN FUTURE
                 * skip if startdate (From_Date__c) is in the future
                 * skip if enddate (To_Inactive__c) is in the past
                 */
                if(yardiDataLeaseCharge.From_Date__c <= currentDate) {
                    if (yardiDataLeaseCharge.To_Inactive__c == null || yardiDataLeaseCharge.To_Inactive__c.format() == '') {
                        leaseChargesByMonth.add(yardiDataLeaseCharge);
                    } else if (yardiDataLeaseCharge.To_Inactive__c >= currentDate) {
                        leaseChargesByMonth.add(yardiDataLeaseCharge);
                    }
                }
            }

            leaseChargesByPaymentDate.put(currentDate, leaseChargesByMonth);
        }

        return leaseChargesByPaymentDate;
    }

    /**
     * To query all relevant contract records including related details, such as Building and Development
     *
     * @param insertedContracts List of Contract records (& Id's) that have been inserted
     *
     * @return List of Contract records
     */
    public static List<Contract> getContractsWithRelatedDetails(List<Contract> insertedContracts) {
        return [
                SELECT  Id,
                        Actual_Tenancy_End_Date__c,
                        ContractTerm,
                        External_Id__c,
                        EndDate,
                        Month_to_Month__c,
                        Payment_Config_Done__c,
                        SBQQ__Opportunity__c,
                        StartDate,
                        Type__c,
                        Unit__c,
                        Unit__r.Building__c,
                        Unit__r.Building__r.Development_Site__c
                FROM Contract
                WHERE Id IN :insertedContracts
        ];
    }

    /**
     * To query all Yardi Ledger Rows by ResidentId's (same as LeaseCode's)
     *
     * @param residentIds Set of External Ids (i.e. Lease_Code__c) records
     *
     * @return Map of List of Yardi_Data_Ledger_Reports__c records (value) by String Resident Id/Lease Code (key)
     */
    public static Map<String, List<Yardi_Data_Ledger_Reports__c>> getLedgerRowsByLeaseCode(Set<String> residentIds) {
        Map<String, List<Yardi_Data_Ledger_Reports__c>> ledgerRowsByLeaseCode = new Map<String, List<Yardi_Data_Ledger_Reports__c>>();

        for(Yardi_Data_Ledger_Reports__c yardiDataLedgerReport : [
                SELECT Id, Name, Chg_Rec__c, Description__c, From__c, Lease_Code__c, Payment__c, To__c, Total__c, Trans_Date__c
                FROM Yardi_Data_Ledger_Reports__c
                WHERE Lease_Code__c IN :residentIds
        ]) {
            if(!ledgerRowsByLeaseCode.containsKey(yardiDataLedgerReport.Lease_Code__c)) {
                ledgerRowsByLeaseCode.put(yardiDataLedgerReport.Lease_Code__c, new List<Yardi_Data_Ledger_Reports__c>());
            }
            ledgerRowsByLeaseCode.get(yardiDataLedgerReport.Lease_Code__c).add(yardiDataLedgerReport);
        }

        return ledgerRowsByLeaseCode;
    }

    /**
     * To query all Yardi Lease Charge Rows (future) by ResidentId's (same as LeaseCode's)
     *
     * @param residentIds Set of External Ids (i.e. Lease_Code__c) records
     *
     * @return Map of List of Yardi_Data_Lease_Charge__c records (value) by String Resident Id (key)
     */
    public static Map<String, List<Yardi_Data_Lease_Charge__c>> getFutureLeaseChargesByResidentId(Set<String> residentIds) {
        Map<String, List<Yardi_Data_Lease_Charge__c>> leaseChargesByResidentId = new Map<String, List<Yardi_Data_Lease_Charge__c>>();

        for(Yardi_Data_Lease_Charge__c yardiDataLeaseCharge : [
                SELECT Id, Name, Amount__c, Charge_Code__c, Charge_Type__c, From_Date__c, Resident_Id__c, To_Inactive__c
                FROM Yardi_Data_Lease_Charge__c
                WHERE Resident_Id__c IN :residentIds
        ]) {
            if(!leaseChargesByResidentId.containsKey(yardiDataLeaseCharge.Resident_Id__c)) {
                leaseChargesByResidentId.put(yardiDataLeaseCharge.Resident_Id__c, new List<Yardi_Data_Lease_Charge__c>());
            }
            leaseChargesByResidentId.get(yardiDataLeaseCharge.Resident_Id__c).add(yardiDataLeaseCharge);
        }

        return leaseChargesByResidentId;
    }

    /**
     * To initialise static variables
     */
    public static void setStaticVariables() {
        country = 'United Kingdom';

        // map Type__c (key: dependent picklist) to Unit_Type__c (value: controlling picklist)
        typeToUnitType = new Map<String, String> {
                'AST' => 'OMR',
                'Corporate' => 'OMR',
                'Rent To Rent' => 'OMR',
                'Common Law' => 'OMR',
                'LHB' => 'LHB',
                'LLR' => 'LLR',
                'DMR' => 'DMR',
                'Other' => 'Other'
        };

        // get the user id for owner, creator
        userId = UserInfo.getUserId();
        List<User> users = [SELECT Id FROM User WHERE Name = 'Rajesh Shah' LIMIT 1];
        if(!users.isEmpty()) {
//            userId = users[0].Id; // TODO @Jared
        }

        // get pricebook Id
        List<Pricebook2> pb2 = [SELECT Id FROM Pricebook2 LIMIT 1];
        if(!pb2.isEmpty()) {
            priceBookId = pb2[0].Id;
        }

        // construct a map of all units (value) by External_Id__c (key)
        unitsByExternalId = new Map<String,Unit__c>();
        for(Unit__c unit : [SELECT External_Id__c, Name, Id, Unit_Type__c, Tenancy_Type__c, Number_of_Bedrooms__c FROM Unit__c]) {
            unitsByExternalId.put(unit.Name, unit);
        }
    }
}