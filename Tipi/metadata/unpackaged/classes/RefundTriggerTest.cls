/**
    * @Name:        RefundTriggerTest
    * @Description: Test class for RefundTrigger functionality
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 28/05/2018    Andrey Korobovich    Created Class
*/
@isTest
public with sharing class RefundTriggerTest {
	@testSetup
	public static void genData() {
		List<Account> accs = TestFactoryPayment.addAccounts(1, true);
		List<Contact> conts = TestFactoryPayment.addContacts(accs, 1, true);
		Opportunity opp = new Opportunity(
        	Name = 'Test Opportunity',
            AccountId = accs[0].Id,
            Primary_Contact__c = conts[0].Id,
            Type = '1 Bed',
            CloseDate = Date.newInstance(2018, 4, 7),
            StageName = 'Interested'
        ); 
        insert opp;
		Unit__c unit = [SELECT Id FROM Unit__c LIMIT 1];
		Development__c development = [SELECT Id FROM Development__c LIMIT 1];
		Tipi_Payments__c newPayment = new Tipi_Payments__c();
		newPayment.Development__c = development.Id;
		newPayment.Contact__c = conts[0].Id;
		newPayment.Unit__c = unit.Id;
		newPayment.Amount__c = 500;
        newPayment.Status__c = 'Awaiting Submission';
        newPayment.Type__c = 'Deposit';
        newPayment.Opportunity__c = opp.Id;
        insert newPayment;	

		asp04__Payment__c payment = new asp04__Payment__c();
		payment.asp04__Amount__c = newPayment.Amount__c;
		payment.asp04__Account_Name__c = newPayment.Contact__r.Account.Name;
		payment.asp04__Billing_Address_Country__c = 'Test county';
		payment.asp04__Billing_Address_City__c = 'Test city';
		payment.asp04__Billing_Address_PostalCode__c = 'Test postcode';
		payment.asp04__Billing_Address_Street__c = 'Test street';
		payment.asp04__Payment_Stage__c = 'Awaiting submission';
		payment.asp04__Email__c = newPayment.Contact__r.Email;
		payment.asp04__First_Name__c = newPayment.Contact__r.FirstName;
		payment.asp04__Last_Name__c = newPayment.Contact__r.LastName;
		payment.asp04__Payment_Route_Options__c = 'Card';
		insert payment;
        
        newPayment.Payment__c = payment.Id;
        update newPayment;
		Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);
        gas.Run_Process_Builder__c = false;
        update gas;
	}

	@isTest
	public static void testRefundTrigger() {
        Id refundTypeId = Schema.SObjectType.Tipi_Payments__c.getRecordTypeInfosByName().get('Refund').getRecordTypeId();
        List<Contact> currentContact = [SELECT Id FROM Contact LIMIT 1];

		asp04__Payment__c payment = [SELECT Id, asp04__Amount__c FROM asp04__Payment__c LIMIT 1];
		asp04__Refund__c refund = new asp04__Refund__c();
		refund.asp04__Asperato_Reference__c = 'Test';
		refund.asp04__Payment__c = payment.Id;
		refund.asp04__Amount__c = 200;
		insert refund;
        List<Tipi_Payments__c> refundPayments = [SELECT Id FROM Tipi_Payments__c WHERE RecordTypeId =: refundTypeId LIMIT 1];  
        System.assert(refundPayments.size() == 1);
        delete refundPayments;
	}

}