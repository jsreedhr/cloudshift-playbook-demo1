/**
* @Name:        OpportunityTriggerTest
* @Description: Test class of OpportunityTriggerHandler
*
* @author:      Patrick Fischer
* @version:     1.0
* Change Log
*
* Date          author              Change Description
* -----------------------------------------------------------------------------------
* 06/06/2018    Patrick Fischer     Created Class
*/
@IsTest
public with sharing class OpportunityTriggerTest {

    /**
     * Method tested: beforeInsert()
     * Expected result: Pricebook Id has been set successfully
     */
    private static testMethod void beforeInsertTest() {
        TestFactory.createSObject(new Global_Application_Settings__c(), true);

        update new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
        List<Account> accounts = TestFactoryPayment.addAccounts(2, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 11, true);
        List<Opportunity> opportunities = new List<Opportunity> {
                TestFactoryPayment.getOpportunity(accounts[0], contacts[0], false),
                TestFactoryPayment.getOpportunity(accounts[1], contacts[0], false)
        };

        Test.startTest();
        insert opportunities;
        Test.stopTest();

        opportunities = [SELECT Id, Pricebook2Id FROM  Opportunity];
        System.assertEquals(Test.getStandardPricebookId(), opportunities[0].Pricebook2Id, 'Expecting PriceBook Id to be populated');
        System.assertEquals(Test.getStandardPricebookId(), opportunities[1].Pricebook2Id, 'Expecting PriceBook Id to be populated');
    }

    /**
     * Method tested: setUnitId()
     * Expected result: Primary Quote has been set successfully on insert
     */
    private static testMethod void setUnitIdBeforeInsertSuccess() {
        List<SBQQ__Quote__c> quotes = testQuoteSetup();
        List<Account> accounts = [SELECT Id FROM Account];
        List<Contact> contacts = [SELECT Id FROM Contact];
        List<Opportunity> opportunities = new List<Opportunity> {
                TestFactoryPayment.getOpportunity(accounts[0], contacts[0], false),
                TestFactoryPayment.getOpportunity(accounts[1], contacts[0], false)
        };

        Test.startTest();
        opportunities[0].SBQQ__PrimaryQuote__c = quotes[0].Id;
        opportunities = OpportunityTriggerHelper.setUnitId(opportunities, null);
        Test.stopTest();

        System.assertNotEquals(null, opportunities[0].Leased_Reserved_Unit__c, 'Expecting Leased_Reserved_Unit__c to be populated');
    }

    /**
     * Method tested: setUnitId()
     * Expected result: Primary Quote has been set successfully on update
     */
    private static testMethod void setUnitIdBeforeUpdateSuccess() {
        List<SBQQ__Quote__c> quotes = testQuoteSetup();
        List<Account> accounts = [SELECT Id FROM Account];
        List<Contact> contacts = [SELECT Id FROM Contact];
        List<Opportunity> opportunities = new List<Opportunity> {
                TestFactoryPayment.getOpportunity(accounts[0], contacts[0], false),
                TestFactoryPayment.getOpportunity(accounts[1], contacts[0], false)
        };
        opportunities[0].SBQQ__PrimaryQuote__c = quotes[0].Id;
        Id unitId = [SELECT Id FROM Unit__c][1].Id;
        opportunities[0].Leased_Reserved_Unit__c = unitId;
        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity> { opportunities[0].Id => TestFactoryPayment.getOpportunity(accounts[0], contacts[0], true) };

        Test.startTest();
        opportunities = OpportunityTriggerHelper.setUnitId(opportunities, oldMap);
        Test.stopTest();

        System.assertNotEquals(null, opportunities[0].Leased_Reserved_Unit__c, 'Expecting Leased_Reserved_Unit__c to be populated');
        System.assertNotEquals(unitId, opportunities[0].Leased_Reserved_Unit__c, 'Expecting proper unit Id to be populated');
    }

    /**
     * Method tested: afterUpdate()
     * Expected result: Child Contacts have been updated successfully
     */
    private static testMethod void afterUpdateChildContactsSuccess() {
        List<Account> accs = TestFactoryPayment.addAccounts(1, true);
        List<Contact> conts = TestFactoryPayment.addContacts(accs, 5, true);
        List<Building__c> buildings = [SELECT Id FROM Building__c];
        List<Unit__c> units = TestFactoryPayment.getUnits(buildings[0], 1, true);
        TestFactory.createSObject(new Global_Application_Settings__c(), true);

        Opportunity opp = new Opportunity(
                Name = 'Test Opportunity',
                AccountId = accs[0].Id,
                Primary_Contact__c = conts[0].Id,
                Tenant_2__c = conts[1].Id,
                Tenant_3__c = conts[2].Id,
                Tenant_4__c = conts[3].Id,
                Tenant_5__c = conts[4].Id,
                Type = '1 Bed',
                CloseDate = Date.newInstance(2018, 4, 7),
                StageName = 'Interested',
                Amount = 1000,
                Term_No_of_Months__c = 1
        );
        insert opp;

        Test.startTest();
        Contract contr = new Contract(
                Unit__c = units[0].Id,
                SBQQ__Opportunity__c = opp.Id,
                AccountId = accs[0].Id
        );
        insert contr;
        Test.stopTest();

        Opportunity oppAfterInsert = [
                SELECT Id, Leased_Reserved_Unit__c, ContractId,
                        Primary_Contact__r.Leased_Reserved_Unit__c, Primary_Contact__r.Current_Lease__c,
                        Tenant_2__r.Leased_Reserved_Unit__c, Tenant_2__r.Current_Lease__c,
                        Tenant_3__r.Leased_Reserved_Unit__c, Tenant_3__r.Current_Lease__c
                FROM Opportunity
        ];

        System.assertEquals(oppAfterInsert.Primary_Contact__r.Leased_Reserved_Unit__c , oppAfterInsert.Leased_Reserved_Unit__c);
        System.assertEquals(oppAfterInsert.Tenant_2__r.Leased_Reserved_Unit__c , oppAfterInsert.Leased_Reserved_Unit__c);
        System.assertEquals(oppAfterInsert.Tenant_3__r.Leased_Reserved_Unit__c , oppAfterInsert.Leased_Reserved_Unit__c);
    }

    /**
     * Method tested: afterInsert()
     * Expected result: Child Contacts have been updated successfully
     */
    private static testMethod void afterInsertUpdateChildContactsSuccess() {
        List<Account> accs = TestFactoryPayment.addAccounts(1, true);
        List<Contact> conts = TestFactoryPayment.addContacts(accs, 5, true);
        List<Contract> contracts = [SELECT Id FROM Contract LIMIT 1];
        TestFactory.createSObject(new Global_Application_Settings__c(), true);

        Test.startTest();
        Opportunity opp = new Opportunity(
                Name = 'Test Opportunity',
                AccountId = accs[0].Id,
                Primary_Contact__c = conts[0].Id,
                Tenant_2__c = conts[1].Id,
                Tenant_3__c = conts[2].Id,
                Tenant_4__c = conts[3].Id,
                Tenant_5__c = conts[4].Id,
                ContractId = contracts[0].Id,
                Type = '1 Bed',
                CloseDate = Date.newInstance(2018, 4, 7),
                StageName = 'Interested',
                Amount = 1000,
                Term_No_of_Months__c = 1
        );
        insert opp;
        Test.stopTest();

        Opportunity oppAfterInsert = [
                SELECT Id, Leased_Reserved_Unit__c, ContractId,
                        Primary_Contact__r.Leased_Reserved_Unit__c, Primary_Contact__r.Current_Lease__c,
                        Tenant_2__r.Leased_Reserved_Unit__c, Tenant_2__r.Current_Lease__c,
                        Tenant_3__r.Leased_Reserved_Unit__c, Tenant_3__r.Current_Lease__c
                FROM Opportunity
        ];

        System.assertEquals(oppAfterInsert.Primary_Contact__r.Leased_Reserved_Unit__c , oppAfterInsert.Leased_Reserved_Unit__c);
        System.assertEquals(oppAfterInsert.Tenant_2__r.Leased_Reserved_Unit__c , oppAfterInsert.Leased_Reserved_Unit__c);
        System.assertEquals(oppAfterInsert.Tenant_3__r.Leased_Reserved_Unit__c , oppAfterInsert.Leased_Reserved_Unit__c);
        System.assertEquals(oppAfterInsert.Primary_Contact__r.Current_Lease__c , oppAfterInsert.ContractId);
        System.assertEquals(oppAfterInsert.Tenant_2__r.Current_Lease__c , oppAfterInsert.ContractId);
        System.assertEquals(oppAfterInsert.Tenant_3__r.Current_Lease__c , oppAfterInsert.ContractId);
    }

    /**
     * Helper method to setup testdata including Developments, Buildings, Units, Accounts, Opportunities, Products, Quotes
     */
    private static List<SBQQ__Quote__c> testQuoteSetup() {
        // Testdata Items
        List<Development__c> developments = TestFactoryPayment.getDevelopments(1, true);
        List<Building__c> buildings = TestFactoryPayment.getBuildings(developments[0], 1, true);
        List<Unit__c> units = TestFactoryPayment.getUnits(buildings[0], 2, true);
        update new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
        List<Product2> products = new List<Product2>();
        products.addAll(TestFactoryPayment.getProducts('Unit', 3000, 1, false));
        products.addAll(TestFactoryPayment.getProducts('Storage', 200, 4, false));
        products.addAll(TestFactoryPayment.getProducts('Parking', 400, 2, false));
        products[0].Related_Apartment__c = units[0].Id;
        insert products;
        List<PricebookEntry> pricebookEntries = TestFactoryPayment.getPricebookEntries(products, true);
        List<Rentable_Item__c> rentableItems = TestFactoryPayment.getRentableItems(products, buildings[0], true);

        // Transactional Items
        List<Account> accounts = TestFactoryPayment.addAccounts(2, true);
        List<Contact> contacts = TestFactoryPayment.getContacts(accounts[0], 11, true);
        List<Opportunity> opportunities = TestFactoryPayment.getOpportunities(accounts[0], contacts, 1, true);
        List<SBQQ__Quote__c> quotes = TestFactoryPayment.getQuotes(accounts[0], contacts[0], opportunities[0], 1, true);
        List<SBQQ__QuoteLine__c> quoteLines = TestFactoryPayment.getQuoteLines(quotes[0], products, true);

        return [SELECT Id FROM SBQQ__Quote__c];
    }
}