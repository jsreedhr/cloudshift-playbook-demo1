/**
 * @Name:        CreditPaymentController
 * @Description: Aura Controller class to handle server side operations of c:creditPayment component
 *
 * @author:      Andrey Korobovich
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 31/05/2018    Andrey Korobovich      Created Class
 * 14/06/2018    Eugene Konstantinov    Added Billing Address for Authorisation
 * 03/07/2018    Patrick Fischer        Fixed Rent Type to dynamically map on new Tipi Payment
 */
public with sharing class CreditPaymentController {
    private static final String AWAITING_STATUS = 'Awaiting Submission';
    private static final String CREDIT_TYPE = 'Credit';
    private static final String ROUTE_OPTION = 'Card';

    @AuraEnabled
    public static String createPayment(Id contactId, Decimal paymentAmount) {
        Contact cont = getContactField(contactId);
        if(cont != null) {
            if (cont.Current_Lease__c != null && cont.Leased_Reserved_Unit__c != null) {
                try {
                    Map<Contact, asp04__Payment__c> paymentsMap = createAsperatoPayment(cont, paymentAmount);
                    asp04__Payment__c asperatoPayment = paymentsMap.get(cont);
                    Tipi_Payments__c payment = createTipiPaymentRecord(cont, paymentAmount, asperatoPayment);
                    EmailHandler.sendEmailToContact(paymentsMap, 'Payment URL');
                    return JSON.serialize(new ResultWrapper(true, System.Label.Credit_payment_successfully_created));
                } catch (Exception ex) {
                    System.debug(ex);
                    return JSON.serialize(new ResultWrapper(false, System.Label.Something_went_wrong_Please_try_again));
                }
            }
        }

        return JSON.serialize(new ResultWrapper(false, System.Label.Please_fill_Leased_Reserved_Unit_and_Current_Lease_fields));
    }

    private static Tipi_Payments__c createTipiPaymentRecord(Contact c, Decimal paymentAmount, asp04__Payment__c asperatoPayment) {
        Tipi_Payments__c newPayment = new Tipi_Payments__c();
        newPayment.Contact__c = c.Id;
        newPayment.Payment__c = asperatoPayment.Id;
        newPayment.Opportunity__c = c.Current_Lease__r.SBQQ__Opportunity__c;
        newPayment.Amount__c = paymentAmount;
        newPayment.Building__c = c.Leased_Reserved_Unit__r.Building__c;
        newPayment.Development__c = c.Leased_Reserved_Unit__r.Building__r.Development_Site__c;
        newPayment.Unit__c = c.Leased_Reserved_Unit__c;
        newPayment.Status__c = Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION;
        newPayment.Payment_Date__c = Date.today();
        newPayment.Credit_Payment_Type__c = CREDIT_TYPE;

        String type = Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.get('Other');
        if(c.Leased_Reserved_Unit__c != null) {
            if (Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.containsKey(c.Current_Lease__r.Type__c)) {
                type = Utils_Constants.LEASE_TO_PAYMENT_TYPE_MAP.get(c.Current_Lease__r.Type__c);
            }
        }
        newPayment.Type__c = type;

        insert newPayment;

        return newPayment;
    }

    private static Contact getContactField(Id contactId) {
        List<Contact> cont = [
            SELECT
                Id, FirstName, LastName, Email, Account.Name, Current_Lease__c, Current_Lease__r.SBQQ__Opportunity__c, Current_Lease__r.Type__c,
                Leased_Reserved_Unit__c, Leased_Reserved_Unit__r.Building__c, Leased_Reserved_Unit__r.Building__r.Country__c,
                Leased_Reserved_Unit__r.Building__r.City__c, Leased_Reserved_Unit__r.Building__r.Postcode__c,
                Leased_Reserved_Unit__r.Building__r.Development_Site__c, Leased_Reserved_Unit__r.Building__r.Street__c,
                MailingCountry, MailingState, MailingCity, MailingPostalCode, MailingStreet
            FROM Contact
            WHERE
                Id = :contactId
            LIMIT 1];

        if(cont.isEmpty()) {
            return null;
        } else {
            return cont[0];
        }
    }

    private static Map<Contact , asp04__Payment__c> createAsperatoPayment( Contact cont, Decimal paymentAmount) {
        Map<Contact , asp04__Payment__c> paymentMap = new Map<Contact , asp04__Payment__c>();
        asp04__Payment__c newPayment = new asp04__Payment__c();
        newPayment.asp04__Amount__c = paymentAmount;
        newPayment.Contact__c = cont.Id;
        newPayment.asp04__Account_Name__c = cont.Account.Name;
        newPayment.asp04__Billing_Address_Country__c = cont.MailingCountry;
        newPayment.asp04__Billing_Address_City__c = cont.MailingCity;
        newPayment.asp04__Billing_Address_PostalCode__c = cont.MailingPostalCode;
        newPayment.asp04__Billing_Address_Street__c = cont.MailingStreet;
        newPayment.asp04__Billing_Address_State__c = cont.MailingState;
        newPayment.asp04__Email__c = cont.Email;
        newPayment.asp04__First_Name__c = cont.FirstName;
        newPayment.asp04__Last_Name__c = cont.LastName;
        newPayment.asp04__Payment_Route_Options__c = ROUTE_OPTION;
        newPayment.asp04__Payment_Route_Selected__c = ROUTE_OPTION;
        newPayment.asp04__Payment_Stage__c = Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION;

        insert newPayment;

        paymentMap.put(cont, newPayment);
        return paymentMap;
    }

    public class ResultWrapper {
        public Boolean success {get; set;}
        public String message {get; set;}

        public ResultWrapper(Boolean success, String msg) {
            this.success = success;
            this.message = msg;
        }
    }

}