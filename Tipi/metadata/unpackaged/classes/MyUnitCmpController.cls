/**
    * @Name:        MyUnitCmpController
    * @Description: Class that contains all needed functionality for MyUnit Lightning Component
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 25/05/2018    Eugene Konstantinov    Created Class
*/
public with sharing class MyUnitCmpController {

    /**
    * Method to get Contact for current User
    * @returns Contact current contact or null if user has not Contact
    */
    @AuraEnabled
    public static Contact getCurrentContact() {
        User currentUser = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if (currentUser != null && currentUser.ContactId != null) {
            List<Contact> currentContact = [
                SELECT
                    Id, Name, Email, Phone, Current_Lease__c, Leased_Reserved_Unit__c,
                    Leased_Reserved_Unit__r.Building__r.Development_Site__c,
                    Current_Lease__r.Primary_Contact__c,
                    Current_Lease__r.SBQQ__Opportunity__c,
                    Current_Lease__r.Tenant_2__c,
                    Current_Lease__r.Tenant_3__c,
                    Current_Lease__r.Tenant_4__c,
                    Current_Lease__r.Tenant_5__c,
                    Current_Lease__r.Resident_2_Guarantor__c,
                    Current_Lease__r.Resident_3_Guarantor__c,
                    Current_Lease__r.Resident_4_Guarantor__c,
                    Current_Lease__r.Resident_5_Guarantor__c
                FROM Contact
                WHERE
                    Id = :currentUser.ContactId
                LIMIT 1];
            if (currentContact != null && !currentContact.isEmpty()) {
                return currentContact[0];
            }
        }
        return null;
    }

    /**
    * Method to get bookings for current User
    * @returns List<Booked_Rentable_Item__c>
    */
    @AuraEnabled
    public static List<Booked_Rentable_Item__c> getContactBookings() {
        Contact currentContact = getCurrentContact();
        if (currentContact != null) {
            List<Booked_Rentable_Item__c> bookingsForContact = (List<Booked_Rentable_Item__c>)new BookedRentableItemSelector().getRecordsByContactId(currentContact.Id, new Set<String>{
                Booked_Rentable_Item__c.Name.getDescribe().getName(),
                Booked_Rentable_Item__c.Rentable_Item__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Social_Space__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Booking_Start__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Booking_End__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Start_Date__c.getDescribe().getName(),
                Booked_Rentable_Item__c.End_Date__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Total_Number_of_Days__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Total_Number_of_Hours__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Total_Amount__c.getDescribe().getName(),
                'Social_Space__r.Name',
                'Social_Space__r.Cost__c',
                'Rentable_Item__r.Name',
                'Rentable_Item__r.Cost__c',
                'Rentable_Item__r.Building__r.Development_Site__r.Guest_Parking_Cost__c',
                'RecordType.Name'
            });
            return bookingsForContact;
        }

        return null;
    }

    /**
    * Method to get bookings another users
    * @returns List<Booked_Rentable_Item__c>
    */

    @AuraEnabled
    public static List<Booked_Rentable_Item__c> getUnitBookings() {
        Contact currentContact = getCurrentContact();
        if (currentContact != null && currentContact.Leased_Reserved_Unit__c != null) {
            Set<Id> tenantsIds = new Set<Id>();
            tenantsIds.add(currentContact.Id);
            if (currentContact.Current_Lease__c != null) {
                if (currentContact.Current_Lease__r.Primary_Contact__c != null) {
                    tenantsIds.add(currentContact.Current_Lease__r.Primary_Contact__c);
                }
                if (currentContact.Current_Lease__r.Tenant_2__c != null) {
                    tenantsIds.add(currentContact.Current_Lease__r.Tenant_2__c);
                }
                if (currentContact.Current_Lease__r.Tenant_3__c != null) {
                    tenantsIds.add(currentContact.Current_Lease__r.Tenant_3__c);
                }
                if (currentContact.Current_Lease__r.Tenant_4__c != null) {
                    tenantsIds.add(currentContact.Current_Lease__r.Tenant_4__c);
                }
                if (currentContact.Current_Lease__r.Tenant_5__c != null) {
                    tenantsIds.add(currentContact.Current_Lease__r.Tenant_5__c);
                }
                if (currentContact.Current_Lease__r.Resident_2_Guarantor__c != null) {
                    tenantsIds.add(currentContact.Current_Lease__r.Resident_2_Guarantor__c);
                }
                if (currentContact.Current_Lease__r.Resident_3_Guarantor__c != null) {
                    tenantsIds.add(currentContact.Current_Lease__r.Resident_3_Guarantor__c);
                }
                if (currentContact.Current_Lease__r.Resident_4_Guarantor__c != null) {
                    tenantsIds.add(currentContact.Current_Lease__r.Resident_4_Guarantor__c);
                }
                if (currentContact.Current_Lease__r.Resident_5_Guarantor__c != null) {
                    tenantsIds.add(currentContact.Current_Lease__r.Resident_5_Guarantor__c);
                }
            }
            List<Booked_Rentable_Item__c> bookingsForContact = (List<Booked_Rentable_Item__c>)new BookedRentableItemSelector().getUnitBookingsForLeaseTenants(currentContact.Leased_Reserved_Unit__r.Building__r.Development_Site__c, tenantsIds, currentContact.Current_Lease__c, new Set<String>{
                Booked_Rentable_Item__c.Name.getDescribe().getName(),
                Booked_Rentable_Item__c.Rentable_Item__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Social_Space__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Booking_Start__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Booking_End__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Start_Date__c.getDescribe().getName(),
                Booked_Rentable_Item__c.End_Date__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Total_Number_of_Days__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Total_Number_of_Hours__c.getDescribe().getName(),
                Booked_Rentable_Item__c.Total_Amount__c.getDescribe().getName(),
                'Contact__r.Name',
                'Social_Space__r.Name',
                'Social_Space__r.Cost__c',
                'Rentable_Item__r.Name',
                'Rentable_Item__r.Cost__c',
                'Rentable_Item__r.Building__r.Development_Site__r.Guest_Parking_Cost__c',
                'RecordType.Name'
            });
            return bookingsForContact;
        }

        return null;
    }

}