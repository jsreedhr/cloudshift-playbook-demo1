/**
 * @Name:        AuthorisationTriggerHelper
 * @Description: This class will handle the helper methods for all trigger contexts
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 03/07/2018    Patrick Fischer    Created Class
 */
public with sharing class AuthorisationTriggerHelper {

    public static String STATUS_IN_FORCE = 'In Force';

    /**
     * To process Authorisation records before insert
     *
     * @param newList List of to be inserted Authorisation records
     */
    public static void processBeforeInsert(List<asp04__Authorisation__c> newList) {
        newList = setAspAuthorisationsCustomerId(newList);
    }

    /**
     * To process Authorisation records after insert
     *
     * @param newMap Map of updated Authorisation records
     */
    public static void processAfterInsert(Map<Id, asp04__Authorisation__c> newMap) {
        List<Contact> contactsToUpdate = new List<Contact>();

        contactsToUpdate.addAll(setContactDDInForceFlag(newMap, new Map<Id, asp04__Authorisation__c>()));

        if(!contactsToUpdate.isEmpty()) {
            update contactsToUpdate;
        }
    }

    /**
     * To process Authorisation records before update
     *
     * @param newList List of to be updated Authorisation records
     * @param oldMap Map of old Authorisation records
     */
    public static void processBeforeUpdate(List<asp04__Authorisation__c> newList, Map<Id, asp04__Authorisation__c> oldMap) {
        newList = setAspAuthorisationsCustomerId(newList);
    }

    /**
     * To process Authorisation records after update
     *
     * @param newMap Map of updated Authorisation records
     * @param oldMap Map of old Authorisation records
     */
    public static void processAfterUpdate(Map<Id, asp04__Authorisation__c> newMap, Map<Id, asp04__Authorisation__c> oldMap) {
        List<Contact> contactsToUpdate = new List<Contact>();

        contactsToUpdate.addAll(setContactDDInForceFlag(newMap, oldMap));

        if(!contactsToUpdate.isEmpty()) {
            update contactsToUpdate;
        }
    }

    /**
     * To set the DD Authorisation In Force Flag on related Contacts if an Auth has changed to/from 'In Force' status
     *
     * @param newMap Map of updated Authorisation records
     * @param oldMap Map of old Authorisation records
     */
    private static List<Contact> setContactDDInForceFlag(Map<Id, asp04__Authorisation__c> newMap, Map<Id, asp04__Authorisation__c> oldMap) {
        List<Contact> contactsToUpdate = new List<Contact>();
        Set<Id> contactIds = new Set<Id>();
        for(asp04__Authorisation__c auth : newMap.values()) {
            contactIds.add(auth.Contact__c);
        }
        for(asp04__Authorisation__c auth : oldMap.values()) {
            contactIds.add(auth.Contact__c);
        }

        for(Contact contact : [SELECT Id, DD_Authorisation_In_Force__c, (SELECT Id FROM Authorisations__r WHERE asp04__Status__c = :STATUS_IN_FORCE)
                FROM Contact WHERE Id IN :contactIds]) {
            if(contact.Authorisations__r.isEmpty()) {
                if(contact.DD_Authorisation_In_Force__c != false) {
                    contact.DD_Authorisation_In_Force__c = false;
                    contactsToUpdate.add(contact);
                }
            } else {
                if(contact.DD_Authorisation_In_Force__c != true) {
                    contact.DD_Authorisation_In_Force__c = true;
                    contactsToUpdate.add(contact);
                }
            }
        }

        return contactsToUpdate;
    }

    private static List<asp04__Authorisation__c> setAspAuthorisationsCustomerId(List<asp04__Authorisation__c> aspAuthorisations) {
        Set<Id> relatedContactIds = new Set<Id>();
        for(asp04__Authorisation__c aspAuthorisation : aspAuthorisations) {
            relatedContactIds.add(aspAuthorisation.Contact__c);
        }

        Map<Id, String> aspCustomerIds = getAspCustomerIds();

        Map<Id, Contact> contactsByIds = new Map<Id, Contact> ([
                SELECT Id, Leased_Reserved_Unit__c, Leased_Reserved_Unit__r.Building__c, Leased_Reserved_Unit__r.Building__r.Development_Site__c
                FROM Contact
                WHERE Id IN :relatedContactIds
        ]);

        for(asp04__Authorisation__c aspAuthorisation : aspAuthorisations) {
            if(contactsByIds.containsKey(aspAuthorisation.Contact__c)) {
                if(contactsByIds.get(aspAuthorisation.Contact__c).Leased_Reserved_Unit__c != null) {
                    Id relatedDevelopmentId = contactsByIds.get(aspAuthorisation.Contact__c).Leased_Reserved_Unit__r.Building__r.Development_Site__c;
                    if(aspCustomerIds.containsKey(relatedDevelopmentId)) {
                        aspAuthorisation.asp04__Customer_ID__c = aspCustomerIds.get(relatedDevelopmentId);
                    }
                }
            }
        }

        return aspAuthorisations;
    }

    private static Map<Id, String> getAspCustomerIds() {
        Map<Id, String> aspCustomerIds = new Map<Id, String>();

        for(Asperato_Customer_Id__c customerId : [SELECT Id, Name, Development_Id__c, Customer_Id__c FROM Asperato_Customer_Id__c]) {
            aspCustomerIds.put(customerId.Development_Id__c, customerId.Customer_Id__c);
        }

        return aspCustomerIds;
    }
}