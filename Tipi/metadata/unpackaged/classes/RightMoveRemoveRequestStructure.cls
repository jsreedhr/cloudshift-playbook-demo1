/**
    * @Name:        RightMoveRemoveRequestStructure
    * @Description: Wrapper class for remove request to RightMove
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 29/05/2018    Andrey Korobovich    Created Class
*/

public with sharing class RightMoveRemoveRequestStructure {
	public Network network {get; set;}
	public Branch branch {get; set;}
	public Property property {get; set;}

	public RightMoveRemoveRequestStructure(Integer networkId, Integer branchId, String channel, String agent_ref) {
		this.network = new Network(networkId);
		this.branch = new Branch(branchId, channel);
		this.property = new Property(agent_ref);
	}



	public class Network {
		public Integer network_id {get; set;}
		public Network(Integer network_id){
			this.network_id = network_id;
		}
	}

	public class Branch {
		public Integer branch_id {get; set;}
		public String channel {get; set;}

		public Branch(Integer branch_id, String channel){
			this.branch_id = branch_id;
			this.channel = channel;
		}
	}

	public class Property {
		public String agent_ref {get; set;}
		public Property(String agent_ref){
			this.agent_ref = agent_ref;
		}
	}

}