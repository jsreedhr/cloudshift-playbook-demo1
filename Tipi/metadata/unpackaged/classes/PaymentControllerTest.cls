/**
    * @Name:        PaymentControllerTest
    * @Description: Test class for PaymentController
    *
    * @author:      Andrey Korobovich
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 11/05/2018    Andrey Korobovich    Created Class
*/

@isTest
public with sharing class PaymentControllerTest {

	@testSetup
	public static void generateTestData() {
		List<Account> accs = TestFactoryPayment.addAccounts(1, true);
		List<Contact> conts = TestFactoryPayment.addContacts(accs, 1, true);
		Opportunity opp = new Opportunity(
        	Name = 'Test Opportunity',
            AccountId = accs[0].Id,
            Primary_Contact__c = conts[0].Id,
            Type = '1 Bed',
            CloseDate = Date.newInstance(2018, 4, 7),
            StageName = 'Interested'
        ); 
        insert opp;
		Unit__c unit = [SELECT Id FROM Unit__c LIMIT 1];
		Development__c development = [SELECT Id FROM Development__c LIMIT 1];
		Tipi_Payments__c newPayment = new Tipi_Payments__c();
		newPayment.Development__c = development.Id;
		newPayment.Contact__c = conts[0].Id;
		newPayment.Unit__c = unit.Id;
		newPayment.Amount__c = 500;
        newPayment.Status__c = 'Awaiting Submission';
        newPayment.Type__c = 'Deposit';
        newPayment.Opportunity__c = opp.Id;
        insert newPayment;	
    }

	@isTest
	public static void payDepositTest() {
		Contact c = [SELECT Id FROM Contact LIMIT 1];
		String listJsonString = PaymentController.payDeposit(c.Id);
		List<asp04__Payment__c> payments = (List<asp04__Payment__c>) JSON.deserialize(listJsonString, List<asp04__Payment__c>.class);
		System.assertEquals(payments.size(), 1);
	}

}