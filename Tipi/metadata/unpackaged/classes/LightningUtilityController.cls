/**
 * @Name:        LightningUtilityController
 * @Description: Aura Controller class to handle server side operations of c:lightningUtility component
 *
 * @author:      Patrick Fischer
 * @version:     1.0
 * Change Log
 *
 * Date          author             Change Description
 * -----------------------------------------------------------------------------------
 * 22/06/2018    Patrick Fischer    Created Class
 */
public with sharing class LightningUtilityController {

    @AuraEnabled
    public static Boolean validatePermissionSet(String permissionSetName) {
        Boolean isValid = false;

        List<PermissionSetAssignment> assignments = [
                SELECT AssigneeId, Assignee.Name, PermissionSetId, PermissionSet.Name
                FROM PermissionSetAssignment
                WHERE PermissionSet.Name = :permissionSetName AND AssigneeId = :UserInfo.getUserId()
        ];

        if(!assignments.isEmpty()) {
            isValid = true;
        }

        return isValid;
    }
}