/**
    * @Name:        UnitTriggerTest
    * @Description: Test class for UnitTrigger functionality
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 12/04/2018    Eugene Konstantinov    Created Class
*/
@isTest
private class UnitTriggerTest {

	@isTest
	static void testUnitInsert() {
		//must set the custom settings flags to run all automation items
		Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);

		Test.startTest();
			Map<String, List<sObject>> testObjects = TestDataFactoryForNewViewingController.getTestUnits();
			List<Unit__c> units = testObjects.get('unit');
			for (Unit__c unit : units) {
                unit.Square_Foot__c = 600;
				unit.Release_to_Aggregators__c = true;
			}
			update units;
			for (Unit__c unit : units) {
                unit.Square_Foot__c = 600;
				unit.Release_to_Aggregators__c = false;
			}
        	update units;
        	for (Unit__c unit : units) {
                unit.Square_Foot__c = 600;
				unit.Release_to_Aggregators__c = true;
			}
        	update units;
		Test.stopTest();
	}

}