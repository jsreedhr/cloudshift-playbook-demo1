/**
 * @Name:        CreditPaymentControllerTest
 * @Description: Test clas for CreditPaymentController
 *
 * @author:      Andrey Korobovich
 * @version:     1.0
 * Change Log
 *
 * Date          author               Change Description
 * -----------------------------------------------------------------------------------
 * 01/06/2018    Andrey Korobovich    Created Class
 * 03/07/2018	 Patrick Fischer	  Added coveragte for Type
 */
@IsTest
public with sharing class CreditPaymentControllerTest {

	private static testMethod void createPaymentSuccess() {
		Contact c = [SELECT Id FROM Contact LIMIT 1];
		String responseString = CreditPaymentController.createPayment(c.Id, 500);

		CreditPaymentController.ResultWrapper resultWrapper = (CreditPaymentController.ResultWrapper) JSON.deserialize(responseString, CreditPaymentController.ResultWrapper.class);
		System.assert(resultWrapper.success, 'Expecting successful operation');
	}

	private static testMethod void createPaymentFailureWrongId() {
		String responseString = CreditPaymentController.createPayment(null, 500);

		CreditPaymentController.ResultWrapper resultWrapper = (CreditPaymentController.ResultWrapper) JSON.deserialize(responseString, CreditPaymentController.ResultWrapper.class);
		System.assert(!resultWrapper.success, 'Expecting failure operation due to false Id');
	}

	private static testMethod void createPaymentFailureAmount() {
		Contact c = [SELECT Id FROM Contact LIMIT 1];
		String responseString = CreditPaymentController.createPayment(c.Id, null);

		CreditPaymentController.ResultWrapper resultWrapper = (CreditPaymentController.ResultWrapper) JSON.deserialize(responseString, CreditPaymentController.ResultWrapper.class);
		System.assert(!resultWrapper.success, 'Expecting failure operation due to false amount');
	}

	@TestSetup
	private static void testSetup() {
		List<Account> accs = TestFactoryPayment.addAccounts(1, true);
		List<Contact> conts = TestFactoryPayment.addContacts(accs, 1, true);
		Opportunity opp = new Opportunity(
				Name = 'Test Opportunity',
				AccountId = accs[0].Id,
				Primary_Contact__c = conts[0].Id,
				Type = '1 Bed',
				CloseDate = Date.newInstance(2018, 4, 7),
				StageName = 'Interested'
		);
		insert opp;

		Contract lease = [SELECT Id, SBQQ__Opportunity__c, Unit_Type__c, Type__c FROM Contract LIMIT 1];
		lease.SBQQ__Opportunity__c = opp.Id;
		lease.Unit_Type__c = 'OMR';
		lease.Type__c = 'AST';
		update lease;
	}
}