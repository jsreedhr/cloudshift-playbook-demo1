/**
 * @Name:        TipiPaymentBatch
 * @Description: Aggregate all payments for each user that are for current month in Tipi Payments table and
 *               insert the corresponding rows into the Payment table (Asperato)
 *
 * @author:      Eugene Konstantinov
 * @version:     1.0
 * Change Log
 *
 * Date          author          Change Description
 * -----------------------------------------------------------------------------------
 * 15/05/2018    Eugene Konstantinov    Created Class
 * 31/05/2018    Patrick Fischer        Refactored class; extended scenarios to support credit and reservation payments;
 *                                      removed sending Email within finish;
 * 14/06/2018    Eugene Konstantinov    Added Billing Address for Authorisation
 * 04/07/2018    Patrick Fischer        Added logic for maxPaymentDate and dueDate
 */
public without sharing class TipiPaymentBatch implements Database.Batchable<SObject>, Database.Stateful {
    private Date maxPaymentDate;
    private Date paymentDueDate;

    public TipiPaymentBatch(Date maxPaymentDate) {
        this.maxPaymentDate = maxPaymentDate;
        this.paymentDueDate = maxPaymentDate;

        Integer BACSDelay = 0;
        List<asp04__AsperatoOneSettings__c> settings = [SELECT asp04__BACS_Delay__c FROM asp04__AsperatoOneSettings__c];
        if(!settings.isEmpty()) {
            if(settings[0].asp04__BACS_Delay__c != null) {
                BACSDelay = Integer.valueOf(settings[0].asp04__BACS_Delay__c);
            }
        }
        if(maxPaymentDate < Date.today().addDays(BACSDelay)) {
            this.paymentDueDate = Date.today().addDays(BACSDelay);
        }
    }

    /**
     * Inner class to return multiple Lists per contact as part of constructDrawdownCreditByContact() method
     */
    public class DrawdownCredit {
        List<Tipi_Payments__c> debitRecordsByContact;
        List<Tipi_Payments__c> tipiPaymentsToInsert;
        List<Tipi_Payments__c> tipiPaymentsToUpdate;

        /**
         * Constructor used as part of constructDrawdownCreditByContact() to return multiple Lists
         *
         * @param debitRecordsByContact List of Tipi_Payments__c Debit records
         * @param tipiPaymentsToInsert List of Tipi_Payments__c records to insert
         * @param tipiPaymentsToUpdate List of Tipi_Payments__c records to update
         */
        public DrawdownCredit(List<Tipi_Payments__c> debitRecordsByContact, List<Tipi_Payments__c> tipiPaymentsToInsert,
                List<Tipi_Payments__c> tipiPaymentsToUpdate) {
            this.debitRecordsByContact = debitRecordsByContact;
            this.tipiPaymentsToInsert = tipiPaymentsToInsert;
            this.tipiPaymentsToUpdate = tipiPaymentsToUpdate;
        }
    }

    /**
     * Default Batch start method
     *
     * @param bc BatchableContext
     *
     * @return Query to be used as part of every execute iteration
     */
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
                SELECT Id, FirstName, LastName, Account.Name, Email, MailingCountry, MailingState, MailingCity, MailingPostalCode, MailingStreet,
                (
                        SELECT  Id, Amount__c, Amount_without_Concession__c, Authorisation__c, Building__c, Calculation_Long__c,
                                Concession_Amount__c,  Contact__c, Contact__r.AccountId, Credit_Payment_Type__c, Development__c,
                                Name, Nth_Month_Payment__c, Opportunity__c, Payment__c, Payment_Date__c, Payment_Gateway__c,
                                Payment_Share__c, Status__c, Status_Code__c, Tipi_Payment__c, Total_Amount__c,
                                Total_Remaining_Balance__c, Type__c, Unit__c
                        FROM    Tipi_Payments__r
                        WHERE (
                                // Debit
                                Credit_Payment_Type__c = :Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT AND
                                Payment_Date__c <= :maxPaymentDate AND
                                RecordType.DeveloperName != 'Refund' AND
                                Status__c = :Utils_Constants.AWAITING_SUBMISSION_STATUS AND
                                Tipi_Payment__c = :NULL AND
                                Type__c IN :Utils_Constants.TIPI_PAYMENT_TYPES AND
                                Payment_Gateway__c = :Utils_Constants.PAYMENT_GATEWAY_DD
                        ) OR (
                                // Credit
                                External_Id__c = '' AND
                                Credit_Payment_Type__c = :Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT AND
                                Payment_Date__c <= :maxPaymentDate AND
                                RecordType.DeveloperName != 'Refund' AND
                                Status__c = :Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER AND
                                Total_Remaining_Balance__c > 0 AND
                                Type__c IN :Utils_Constants.TIPI_RENT_PAYMENT_TYPES
                        ) OR (
                                // Reservation Payments
                                External_Id__c = '' AND
                                RecordType.DeveloperName != 'Refund' AND
                                Status__c = :Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER AND
                                Total_Remaining_Balance__c > 0 AND
                                Type__c = :Utils_Constants.PAYMENT_TYPE_RESERVATION
                        )
                        ORDER BY Payment_Date__c
                )
                FROM Contact
        ]);
    }

    /**
     * Default Batch execute method
     *
     * @param bc BatchableContext
     * @param scope List of Contact records injected as via QueryLocator
     */
    public void execute(Database.BatchableContext bc, List<Contact> scope) {
        execute(scope);
    }

    /**
     * Runs the Batchable execute context
     * (moved into separate method to make it properly testable by injecting List of Contacts)
     *
     * @param scope List of Contact records defined by scope
     */
    @TestVisible
    private void execute(List<Contact> scope) {
        Savepoint savepoint = Database.setSavepoint();
        try {
            Map<Id, List<Tipi_Payments__c>> debitRecordsByContactId = getDebitRecordsByContactId(scope);
            Map<Id, List<Tipi_Payments__c>> reservationRecordsByContactId = getReservationRecordsByContactId(scope);
            Map<Id, List<Tipi_Payments__c>> creditRecordsByContactId = getCreditRecordsByContactId(scope);
            Map<Id, Contact> contactsByIds = new Map<Id, Contact>(scope);
            Map<Id, Decimal> contactsMissingAuthorisation = new Map<Id, Decimal>();
            List<Tipi_Payments__c> tipiPaymentsToInsert = new List<Tipi_Payments__c>();
            List<Tipi_Payments__c> tipiPaymentsToUpdate = new List<Tipi_Payments__c>();
            Map<Id, asp04__Payment__c> newPaymentByContactId = new Map<Id, asp04__Payment__c>();
            Map<Id, Id> authorisationsByContactId = getAuthorisationsByContactId(contactsByIds.keySet());

            // Loop over every contact in batch to Drawdown Reservation & Credit
            for (Id contactId : debitRecordsByContactId.keySet()) {
                // Drawdown Reservation
                DrawdownCredit drawdownReservation = constructDrawdownCreditByContact(contactId, debitRecordsByContactId.get(contactId), reservationRecordsByContactId);
                debitRecordsByContactId.put(contactId, drawdownReservation.debitRecordsByContact);
                tipiPaymentsToInsert.addAll(drawdownReservation.tipiPaymentsToInsert);
                tipiPaymentsToUpdate.addAll(drawdownReservation.tipiPaymentsToUpdate);

                // Drawdown Credit
                DrawdownCredit drawdownCredit = constructDrawdownCreditByContact(contactId, debitRecordsByContactId.get(contactId), creditRecordsByContactId);
                debitRecordsByContactId.put(contactId, drawdownCredit.debitRecordsByContact);
                tipiPaymentsToInsert.addAll(drawdownCredit.tipiPaymentsToInsert);
                tipiPaymentsToUpdate.addAll(drawdownCredit.tipiPaymentsToUpdate);
            }

            // Calculate total debit amount and construct asperator payment record
            for (Id contactId : debitRecordsByContactId.keySet()) {
                // add up all remaining debit amounts and construct asp04__Payment__c record per contact
                Decimal amount = getTotalAmountForPayments(debitRecordsByContactId.get(contactId));
                if (authorisationsByContactId.containsKey(contactId)) {
                    if(amount > 0) {
                        newPaymentByContactId.put(contactId, constructAsperatoPayment(contactsByIds.get(contactId), amount, authorisationsByContactId.get(contactId)));
                    }
                } else {
                    contactsMissingAuthorisation.put(contactId, amount.setScale(2, RoundingMode.HALF_UP));
                }
            }

            // insert new asp04__Payment__c records
            if (!newPaymentByContactId.values().isEmpty()) {
                insert newPaymentByContactId.values();
            }

            // insert new/cloned Tipi_Payment__c records (constructed by drawdown calculation)
            if (!tipiPaymentsToInsert.isEmpty()) {
                tipiPaymentsToInsert = getTipiPaymentsWithAuthorisation(authorisationsByContactId, tipiPaymentsToInsert);
                insert tipiPaymentsToInsert;
            }

            // update Status__c and related asp04__Payment__c.Id on every Tipi_Payments__c debit record
            tipiPaymentsToUpdate.addAll(updatePaymentIdAndStatus(debitRecordsByContactId, newPaymentByContactId));

            // update Tipi Payment records
            if (!tipiPaymentsToUpdate.isEmpty()) {
                tipiPaymentsToUpdate = getTipiPaymentsWithAuthorisation(authorisationsByContactId, tipiPaymentsToUpdate);
                update tipiPaymentsToUpdate;
            }

            createAuthorisationTasks(contactsMissingAuthorisation);

        } catch (Exception e) {
            Database.rollback(savepoint);
            CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, e);
        }
    }

    /**
     * Default Batch finish method
     *
     * @param bc BatchableContext
     */
    public void finish(Database.BatchableContext bc) {
        CustomLogger.save();
    }

    /**
     * To drawdown credit/reservation with Total_Remaining_Balance__c > 0 and assign Tipi_Payments
     *
     * @param contactId Id of contact iterating through
     * @param debitRecordsList List of Tipi_Payments__c debit records for this contact
     * @param creditRecordsByContactId Map of Contact Id to List of Tipi_Payments credit/reservation records
     *
     * @return Object DrawdownCredit containing 3 updated Lists: debit records, records to insert, records to update
     */
    private DrawdownCredit constructDrawdownCreditByContact(Id contactId, List<Tipi_Payments__c> debitRecordsList,
            Map<Id, List<Tipi_Payments__c>> creditRecordsByContactId) {
        List<Tipi_Payments__c> tipiPaymentsToInsert = new List<Tipi_Payments__c>();
        Map<Id, Tipi_Payments__c> tipiPaymentsToUpdateMap = new Map<Id, Tipi_Payments__c>();

        if(creditRecordsByContactId.containsKey(contactId)) {
            for(Tipi_Payments__c tipiPayment : debitRecordsList) {

                // only drawdown for rent types
                if(Utils_Constants.TIPI_RENT_PAYMENT_TYPES.contains(tipiPayment.Type__c)) {
                    for (Tipi_Payments__c creditPayment : creditRecordsByContactId.get(contactId)) {
                        if (creditPayment.Total_Remaining_Balance__c > 0 && tipiPayment.Tipi_Payment__c == null) {

                            String creditMessage = Utils_Constants.PAYMENT_DRAW_DOWN_PART1 + creditPayment.Total_Remaining_Balance__c + Utils_Constants.PAYMENT_DRAW_DOWN_PART2;

                            // if rent amount is less than credit balance
                            if (tipiPayment.Amount__c <= creditPayment.Total_Remaining_Balance__c) {
                                creditPayment.Total_Remaining_Balance__c -= tipiPayment.Amount__c;
                                tipiPayment.Tipi_Payment__c = creditPayment.Id;
                                tipiPayment.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
                                tipiPayment.Calculation_Long__c += '; ' + creditMessage;
                            }

                            // if rent amount is larger than credit balance
                            else {
                                Tipi_Payments__c paymentClone = tipiPayment.clone(false, true, false, false);
                                paymentClone.Tipi_Payment__c = creditPayment.Id;
                                paymentClone.Amount__c = creditPayment.Total_Remaining_Balance__c;
                                paymentClone.Amount_without_Concession__c = paymentClone.Amount__c;
                                paymentClone.Concession_Amount__c = 0;
                                paymentClone.Calculation_Long__c = creditMessage;
                                paymentClone.Status__c = Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER;
                                paymentClone.Original_Charge_Split__c = tipiPayment.Id;
                                tipiPaymentsToInsert.add(paymentClone);

                                tipiPayment.Amount__c -= creditPayment.Total_Remaining_Balance__c;
                                tipiPayment.Calculation_Long__c += Utils_Constants.PAYMENT_PARTIAL_DRAW_DOWN + creditPayment.Total_Remaining_Balance__c + ']';
                                creditPayment.Total_Remaining_Balance__c = 0;
                            }
                            tipiPaymentsToUpdateMap.put(creditPayment.Id, creditPayment);
                        }
                    }
                }
            }
        }

        return new DrawdownCredit(debitRecordsList, tipiPaymentsToInsert, tipiPaymentsToUpdateMap.values());
    }

    /**
     * To update Status__c and related asp04__Payment__c.Id on every Tipi_Payments__c debit record
     *
     * @param debitRecordsByContactId Map of ContactId to List of Tipi_Payments__c Debit records
     * @param newPaymentByContactId Map of ContactId to Asperato payment records
     *
     * @return List of Tipi_Payments__c records to update
     */
    private List<Tipi_Payments__c> updatePaymentIdAndStatus(Map<Id, List<Tipi_Payments__c>> debitRecordsByContactId,
            Map<Id, asp04__Payment__c> newPaymentByContactId) {
        List<Tipi_Payments__c> tipiPaymentsToUpdate = new List<Tipi_Payments__c>();

        for (Id contactId : debitRecordsByContactId.keySet()) {
            for (Tipi_Payments__c tipiPayment : debitRecordsByContactId.get(contactId)) {
                if (tipiPayment.Status__c != Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER) {
                    if (newPaymentByContactId.containsKey(contactId)) {
                        tipiPayment.Payment__c = newPaymentByContactId.get(contactId).Id;
                    }
                    tipiPayment.Status__c = Utils_Constants.PAYMENT_SUBMITTED_FOR_COLLECTION;
                }
                tipiPaymentsToUpdate.add(tipiPayment);
            }
        }

        return tipiPaymentsToUpdate;
    }

    /**
     * To construct an Asperato payment record
     *
     * @param contactRecord Contact details including Account Name, Email, Name
     * @param amount Decimal value to bill the customer via Direct Debit
     * @param authorisationId In Force authorisation record
     *
     * @return asp04__Payment__c asperato payment record
     */
    private asp04__Payment__c constructAsperatoPayment(Contact contactRecord, Decimal amount, Id authorisationId) {
        asp04__Payment__c newPayment = new asp04__Payment__c();
        newPayment.asp04__Amount__c = amount;
        newPayment.asp04__Account_Name__c = contactRecord.Account.Name;
        newPayment.asp04__Billing_Address_Country__c = contactRecord.MailingCountry;
        newPayment.asp04__Billing_Address_City__c = contactRecord.MailingCity;
        newPayment.asp04__Billing_Address_State__c = contactRecord.MailingState;
        newPayment.asp04__Billing_Address_PostalCode__c = contactRecord.MailingPostalCode;
        newPayment.asp04__Billing_Address_Street__c = contactRecord.MailingStreet;
        newPayment.asp04__Due_Date__c = paymentDueDate;
        newPayment.asp04__Email__c = contactRecord.Email;
        newPayment.asp04__First_Name__c = contactRecord.FirstName;
        newPayment.asp04__Last_Name__c = contactRecord.LastName;
        newPayment.asp04__Payment_Route_Options__c = Utils_Constants.PAYMENT_GATEWAY_DD;
        newPayment.asp04__Payment_Route_Selected__c	= Utils_Constants.PAYMENT_GATEWAY_DD;
        newPayment.asp04__Payment_Stage__c = Utils_Constants.AWAITING_SUBMISSION_STATUS;
        newPayment.asp04__Authorisation__c = authorisationId;
        newPayment.Contact__c = contactRecord.Id;

        return newPayment;
    }

    /**
     * To fetch all 'Debit records' from Contact.Tipi_Payments__r
     *
     * @param contacts List of Contact records being processed in scope
     *
     * @return Map of Contact Id to List of 'Debit Tipi_Payments__c' records
     */
    private Map<Id, List<Tipi_Payments__c>> getDebitRecordsByContactId(List<Contact> contacts) {
        Map<Id, List<Tipi_Payments__c>> debitRecordsByContactId = new Map<Id, List<Tipi_Payments__c>>();

        for(Contact contact : contacts) {
            for (Tipi_Payments__c tipiPayment : contact.Tipi_Payments__r) {
                if (tipiPayment.Credit_Payment_Type__c == Utils_Constants.CREDIT_PAYMENT_TYPE_DEBIT &&
                        tipiPayment.Status__c == Utils_Constants.AWAITING_SUBMISSION_STATUS) {
                    if (!debitRecordsByContactId.containsKey(contact.Id)) {
                        debitRecordsByContactId.put(contact.Id, new List<Tipi_Payments__c>());
                    }
                    debitRecordsByContactId.get(contact.Id).add(tipiPayment);
                }
            }
        }

        return debitRecordsByContactId;
    }

    /**
     * To fetch all 'Credit records' from Contact.Tipi_Payments__r
     *
     * @param contacts List of Contact records being processed in scope
     *
     * @return Map of contactId to List of 'Credit Tipi_Payments__c' records
     */
    private Map<Id, List<Tipi_Payments__c>> getCreditRecordsByContactId(List<Contact> contacts) {
        Map<Id, List<Tipi_Payments__c>> creditRecordsByContactId = new Map<Id, List<Tipi_Payments__c>>();

        for(Contact contact : contacts) {
            for (Tipi_Payments__c tipiPayment : contact.Tipi_Payments__r) {
                if (tipiPayment.Credit_Payment_Type__c == Utils_Constants.CREDIT_PAYMENT_TYPE_CREDIT &&
                        tipiPayment.Type__c != Utils_Constants.PAYMENT_TYPE_RESERVATION &&
                        tipiPayment.Status__c == Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER) {
                    if (!creditRecordsByContactId.containsKey(contact.Id)) {
                        creditRecordsByContactId.put(contact.Id, new List<Tipi_Payments__c>());
                    }
                    creditRecordsByContactId.get(contact.Id).add(tipiPayment);
                }
            }
        }

        return creditRecordsByContactId;
    }

    /**
     * To fetch all 'Reservation records' from Contact.Tipi_Payments__r
     *
     * @param contacts List of Contact records being processed in scope
     *
     * @return Map of contactId to List of 'Reservation Tipi_Payments__c' records
     */
    private Map<Id, List<Tipi_Payments__c>> getReservationRecordsByContactId(List<Contact> contacts) {
        Map<Id, List<Tipi_Payments__c>> reservationRecordsByContactId = new Map<Id, List<Tipi_Payments__c>>();

        for(Contact contact : contacts) {
            for (Tipi_Payments__c tipiPayment : contact.Tipi_Payments__r) {
                if (tipiPayment.Type__c == Utils_Constants.PAYMENT_TYPE_RESERVATION &&
                        tipiPayment.Status__c == Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER) {
                    if (!reservationRecordsByContactId.containsKey(contact.Id)) {
                        reservationRecordsByContactId.put(contact.Id, new List<Tipi_Payments__c>());
                    }
                    reservationRecordsByContactId.get(contact.Id).add(tipiPayment);
                }
            }
        }

        return reservationRecordsByContactId;
    }

    /**
     * To retrieve the latest Authorisation Id for every Contact
     *
     * @param contactIds Set of Contact Ids to require an in force
     *
     * @return Map of Contact Id (key) to Authorisation Id (value)
     */
    private static Map<Id, Id> getAuthorisationsByContactId(Set<Id> contactIds) {
        Map<Id, Id> authorisationsByContactId = new Map<Id, Id>();

        for (asp04__Authorisation__c auth : [
                SELECT Id, Contact__c
                FROM asp04__Authorisation__c
                WHERE asp04__Status__c = 'In Force' AND Contact__c IN :contactIds
                ORDER BY asp04__Expiry_Date__c DESC
        ]) {
            if (!authorisationsByContactId.containsKey(auth.Contact__c)) {
                authorisationsByContactId.put(auth.Contact__c, auth.Id);
            }
        }

        return authorisationsByContactId;
    }

    /**
     * To populate lookup to Authorisation for every Tipi_Payments__c record
     *
     * @param authorisedContactIds Map of Contact Ids to asp04__Authorisation__c Id
     * @param tipiPayments List of Tipi_Payments__c records
     *
     * @return List of Tipi_Payments__c including populated Authorisation Lookup
     */
    private static List<Tipi_Payments__c> getTipiPaymentsWithAuthorisation(Map<Id, Id> authorisationsByContactId, List<Tipi_Payments__c> tipiPayments) {
        List<Tipi_Payments__c> tipiPaymentsWithAuth = new List<Tipi_Payments__c>();

        for(Tipi_Payments__c tipiPayment : tipiPayments) {
            if(authorisationsByContactId.containsKey(tipiPayment.Contact__c)) {
                tipiPayment.Authorisation__c = authorisationsByContactId.get(tipiPayment.Contact__c);
                tipiPaymentsWithAuth.add(tipiPayment);
            }
        }

        return tipiPaymentsWithAuth;
    }

    /**
     * To sum up the total amount to be collected for a single contact
     *
     * @param tipiPayments List of Tipi_Payments__c records for a single contact
     *
     * @return Decimal amount to be collected
     */
    private static Decimal getTotalAmountForPayments(List<Tipi_Payments__c> tipiPayments) {
        Decimal amount = 0;

        for (Tipi_Payments__c tipiPayment : tipiPayments) {
            // exclude rent payments that are linked to any credit/reservation
            if(tipiPayment.Status__c != Utils_Constants.PAYMENT_COLLECTED_FROM_CUSTOMER) {
                amount += tipiPayment.Amount__c;
            }
        }

        return amount;
    }

    /**
     * To construct and insert tasks on Contacts missing Authorisation records
     *
     * @param contactsMissingAuthorisation Map of contactId (key) to Amount (value) per batch scope
     *
     * @return List of inserted Tasks
     */
    private static List<Task> createAuthorisationTasks(Map<Id, Decimal> contactsMissingAuthorisation) {

        List<Task> tasks = new List<Task>();

        if(!contactsMissingAuthorisation.isEmpty()) {

            Id assignedTaskOwner = UserInfo.getUserId();
            List<GroupMember> groupMembers = [SELECT UserOrGroupId FROM GroupMember
                    WHERE Group.DeveloperName = 'Finance_Authorisation_Task_Assignment' ORDER BY SystemModstamp LIMIT 1];
            if(!groupMembers.isEmpty()) {
                assignedTaskOwner = groupMembers[0].UserOrGroupId;
            }

            for (Contact contact : [SELECT Id, Name, AccountId FROM Contact WHERE Id IN :contactsMissingAuthorisation.keySet()]) {
                tasks.add(
                        new Task(
                                WhoId = contact.Id, // Contact Id
                                WhatId = contact.AccountId, // Account Id
                                Subject = contact.Name + ' is missing an \'In Force\' Authorisation record for an amount of £'
                                        + contactsMissingAuthorisation.get(contact.Id),
                                Description = 'This Contact is missing an \'In Force\' Authorisation record for the amount of £'
                                        + contactsMissingAuthorisation.get(contact.Id) + '. '
                                        + 'No Asperato Direct Debit Payment record was created due to the missing Authorisation.',
                                ActivityDate = System.today(),
                                Priority = 'High',
                                OwnerId = assignedTaskOwner // assigned to User
                        )
                );
            }

            insert tasks;
        }

        return tasks;
    }
}