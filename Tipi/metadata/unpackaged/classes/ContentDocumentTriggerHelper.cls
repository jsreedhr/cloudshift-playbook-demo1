/**
    * @Name:        ContentDocumentTriggerHelper
    * @Description: This is helper class, will hold all methods and functionalities that are to be build for ContentDocument
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 09/04/2018    Eugene Konstantinov    Created Class
*/
public with sharing class ContentDocumentTriggerHelper {

	private static final String UNIT_FILE_PREFIX = Unit_File__c.SObjectType.getDescribe().getKeyPrefix();
	private static final String CLASS_NAME = 'ContentDocumentTriggerHelper';

	/**
		 * Method for setting ContentDocument to shared link field on UnitFile
		 * @param  newDocContent  name of ContentDistribution
	 */
	public static void setSharedLink(List<ContentDocument> newDocContent){

		Map<Id,ContentDistribution> cdMap = new Map<Id,ContentDistribution>();
		List<Unit_File__c> unitFilesWithLinks = new List<Unit_File__c>();
		List<Id> cdId = new List<Id>();

		for(ContentDocument cd: newDocContent){
			cdId.add(cd.Id);
		}

		List<ContentDocumentLink> cdlList = [SELECT ID,LinkedEntityId, LinkedEntity.Name, ContentDocument.LatestPublishedVersionId
											 FROM ContentDocumentLink
											 WHERE ContentDocumentId
											 IN: cdId];

		for(ContentDocumentLink cdocLink: cdlList) {

			if(String.valueOf(cdocLink.LinkedEntityId).startsWith(UNIT_FILE_PREFIX)) {

				cdMap.putAll(createContentDistribution(cdocLink.LinkedEntity.Name,
														cdocLink.LinkedEntityId,
														cdocLink.ContentDocument.LatestPublishedVersionId));

			}

		}

		try{
			Database.insert(cdMap.values());
		}
		catch(DMLException e){
			CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'DML Exception:' + CLASS_NAME + e.getMessage());
		}
		catch(Exception e){
			CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'Exception:' + CLASS_NAME + e.getMessage());
		}
		finally{
			CustomLogger.save();
		}

		List<ProxyUrlWrapper> proxyUrlWrapperList = new List<ProxyUrlWrapper>();
		for(ContentDistribution cd: [SELECT DistributionPublicUrl, RelatedRecordId, ContentVersion.IsLatest,
											ContentVersionId, ContentDownloadUrl,ContentVersion.ContentUrl,
											ContentVersion.PathOnClient, ContentVersion.ContentBodyId,
											ContentVersion.ContentDocument.FileExtension, ContentVersion.ContentDocument.Title
									 FROM ContentDistribution
									 WHERE RelatedRecordId IN : cdMap.keySet()
									 AND ContentVersion.IsLatest = TRUE]) {


			proxyUrlWrapperList.add(new ProxyUrlWrapper(
					cd.RelatedRecordId,
					cd.ContentVersion.ContentBodyId,
					cd.ContentVersionId,
					UserInfo.getOrganizationId(),
					cd.DistributionPublicUrl,
					cd.ContentVersion.ContentDocument.Title + '.' + cd.ContentVersion.ContentDocument.FileExtension
			));

			Unit_File__c uFile = new Unit_File__c();

				uFile.Shared_Link__c = cd.DistributionPublicUrl;
				uFile.Id = cd.RelatedRecordId;
				uFile.Latest_Image_Version_ID__c = cd.ContentVersionId;

			unitFilesWithLinks.add(uFile);

		}

		if (proxyUrlWrapperList != null && !proxyUrlWrapperList.isEmpty()) {
			ProxyUrlAsyncRequest request = new ProxyUrlAsyncRequest(proxyUrlWrapperList);
			System.enqueueJob(request); // call request for getting proxy url for Shared_Link__c on Unit File
		}

		try{
			Database.upsert(unitFilesWithLinks); // update UnitFile records with creted Shared_Link__c
		}
		catch(DMLException e){
			CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'DML Exception:' + CLASS_NAME + e.getMessage());
		}
		catch(Exception e){
			CustomLogger.error(CustomLogger.PROJECT_TIPI_CORE, 'Exception:' + CLASS_NAME + e.getMessage());
		}
		finally{
			CustomLogger.save();
		}



	}

	/**
	 * Method for creating ContentDistribution
	 * @param  name  name of ContentDistribution
	 * @param  unitFileId  id of UnitFile record for RelatedRecordId
	 * @param  latestVerisonId  id of latest version for RelatedRecordId
	 *
	 * @returns  Map<Id,ContentDistribution> a map of ContentDistribution objects by UnitFile Id
	 */
	private static Map<Id,ContentDistribution> createContentDistribution(String name, Id unitFileId, Id latestVerisonId) {

		ContentDistribution cd = new ContentDistribution();
			cd.name = name;
			cd.ContentVersionId = latestVerisonId;
			cd.PreferencesAllowOriginalDownload = true;
			cd.PreferencesAllowPDFDownload = false;
			cd.PreferencesAllowViewInBrowser = true;
			cd.RelatedRecordId = unitFileId;

		return new Map<Id,ContentDistribution>{cd.RelatedRecordId => cd};

	}

	public static void removeWhiteSpaces(List<ContentDocument> newFiles) {

		for(ContentDocument cd: newFiles) {
			cd.Title = cd.Title.trim();
		}
	}



}