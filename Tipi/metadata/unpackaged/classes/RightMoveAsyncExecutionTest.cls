/**
    * @Name:        RightMoveAsyncExecutionTest
    * @Description: Test class for RightMoveAsyncExecutionSendRequest Queueable class with callouts
    *
    * @author:      Eugene Konstantinov
    * @version:     1.0
    * Change Log
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 13/04/2018    Eugene Konstantinov    Created Class
*/
@isTest
private class RightMoveAsyncExecutionTest {
	private static Aggregator_Setting__mdt rigthMoveSettings;
	private static Aggregator_Setting__mdt rigthMoveRemoveSettings;

	static{
		List<Aggregator_Setting__mdt> aggregators = [
			SELECT Id, Label, Certificate__c, Method__c, URL__c, Content_Type__c, Network_Id__c, Channel__c, Branch_Id__c, Overseas__c
			FROM Aggregator_Setting__mdt
			WHERE
				Active__c = true AND
				Label = 'RightMoveService'
            	AND Action__c = 'Insert'
			LIMIT 10000
		];
		System.assert(aggregators != null);
		System.assert(aggregators.size() == 1);
      	List<Aggregator_Setting__mdt> aggregatorDelete= [
			SELECT Id, Label, Certificate__c, Method__c, URL__c, Content_Type__c, Network_Id__c, Channel__c, Branch_Id__c, Overseas__c
			FROM Aggregator_Setting__mdt
			WHERE
				Active__c = true AND
				Label = 'ZooplaRemoveService'
				AND Action__c = 'Delete'
			LIMIT 10000
		];
		rigthMoveSettings = aggregators[0];
        rigthMoveRemoveSettings = aggregatorDelete[0];
	}

	@isTest
	static void testCallout() {
		Test.setMock(HttpCalloutMock.class, new RightMoveMockHttpResponseGenerator());

		Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);

		Map<String, List<sObject>> testObjects = TestDataFactoryForNewViewingController.getTestUnits();
		List<Unit__c> units = testObjects.get('unit');
		System.assert(units[0].RightMove_Id__c == null);
		System.assert(units[0].RightMove_URL__c == null);

		Test.startTest();
			RightMoveAsyncExecutionSendRequest request = new RightMoveAsyncExecutionSendRequest(
				rigthMoveSettings.Method__c,
				rigthMoveSettings.URL__c,
				new Map<String, String>{
				'Content-Type'=>'application/json; charset=utf-8'
				},
				'Test Body',
				rigthMoveSettings.Certificate__c,
				units[0]
			);
			System.enqueueJob(request);
		Test.stopTest();

		Unit__c unitAfterCallout = [
			SELECT  Id, RightMove_Id__c, RightMove_URL__c
			FROM Unit__c
			WHERE
				Id = :units[0].Id
			LIMIT 1
		];

		System.assert(unitAfterCallout.RightMove_Id__c == '11002200');
		System.assert(unitAfterCallout.RightMove_URL__c == 'http://some-url.html');
	}
    
   	@isTest
	static void testRemoveCallout() {
		Test.setMock(HttpCalloutMock.class, new RightMoveMockHttpResponseGenerator());
		Global_Application_Settings__c gas = (Global_Application_Settings__c) TestFactory.createSObject(new Global_Application_Settings__c(), true);
		Test.startTest();
		Map<String, List<sObject>> testObjects = TestDataFactoryForNewViewingController.getTestUnits();
		List<Unit__c> units = testObjects.get('unit');
		units[0].RightMove_Id__c = '12345678';
		units[0].RightMove_URL__c ='testurl.com/12345678';
        units[0].Release_to_Aggregators__c = false;
		update units;
			RightMoveAsyncExecutionSendRequest request = new RightMoveAsyncExecutionSendRequest(
				rigthMoveSettings.Method__c,
				rigthMoveSettings.URL__c,
				new Map<String, String>{
				'Content-Type'=>'application/json; charset=utf-8'
				},
				'Test Body',
				rigthMoveSettings.Certificate__c,
				units[0]
			);
			System.enqueueJob(request);
		Test.stopTest();

		Unit__c unitAfterCallout = [
			SELECT  Id, RightMove_Id__c, RightMove_URL__c
			FROM Unit__c
			WHERE
				Id = :units[0].Id
			LIMIT 1
		];
		// System.assert(unitAfterCallout.RightMove_Id__c == '');
		// System.assert(unitAfterCallout.RightMove_URL__c == '');
	}
}