trigger Milestone1_Task_Trigger on Milestone1_Task__c ( before insert, before update,after insert,after update ) {

	System.debug('Milestone1_Task__c Trigger Context Started');

	if(Trigger.isBefore) {
		Milestone1_Task_Trigger_Utility.handleTaskBeforeTrigger(Trigger.new);
	} 
	
	if(Trigger.isAfter) {
		if(Trigger.isUpdate) {
	        //shift Dates of successor Tasks if Task Due Date is shifted
	        Milestone1_Task_Trigger_Utility.checkSuccessorDependencies(Trigger.oldMap, Trigger.newMap);
		}
		Milestone1_Task_Trigger_Utility.handleTaskAfterTrigger(Trigger.new, Trigger.old);
	}
}