trigger RefundTrigger on asp04__Refund__c (after insert, after update, before delete, after undelete) {

    System.debug('asp04__Refund__c Trigger Context Started');

    new RefundTriggerHandler().run();

    CustomLogger.save();
}