trigger TIPIPaymentTrigger on Tipi_Payments__c (before insert, after insert, before update, after update, before delete, after delete, after undelete) {

    System.debug('Tipi_Payments__c Trigger Context Started');

    new TipiPaymentTriggerHandler().run();

    CustomLogger.save();
}