trigger UnitTrigger on Unit__c (after insert, after update) {

	System.debug('Unit__c Trigger Context Started');

	new UnitTriggerHandler().run();

	CustomLogger.save();

}