trigger AuthorisationTrigger on asp04__Authorisation__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    System.debug('asp04__Authorisation__c Trigger Context Started');

    new AuthorisationTriggerHandler().run();

    CustomLogger.save();
}