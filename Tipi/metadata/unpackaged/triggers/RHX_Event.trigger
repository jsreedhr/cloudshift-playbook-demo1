trigger RHX_Event on Event (after delete, after insert, after undelete, after update, before delete) {

	System.debug('Event Trigger Context Started');

	Type rollClass = System.Type.forName('rh2', 'ParentUtil');
	if(rollClass != null) {
		rh2.ParentUtil pu = (rh2.ParentUtil) rollClass.newInstance();
		if (Trigger.isAfter) {
			pu.performTriggerRollups(Trigger.oldMap, Trigger.newMap, new String[]{'Event'}, null);
		}
	}
}