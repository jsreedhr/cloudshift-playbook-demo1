trigger BookedRentableItemTrigger on Booked_Rentable_Item__c (after insert, after update) {

    System.debug('Booked_Rentable_Item__c Trigger Context Started');

    new BookedRentableItemTriggerHandler().run();

    CustomLogger.save();
}