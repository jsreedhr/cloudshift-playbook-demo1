trigger LeaseTrigger on Contract (before update, after insert, after update) {

    System.debug('Contract (Lease) Trigger Context Started');

    new LeaseTriggerHandler().run();

    CustomLogger.save();
}