trigger UserTrigger on User (after insert, after update) {
    System.debug('User Trigger Context Started');

    new UserTriggerHandler().run();
 
    CustomLogger.save();
}