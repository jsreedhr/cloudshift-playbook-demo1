trigger UnitEventTrigger on Unit_Event__e (after insert) {

    System.debug('Unit_Event__e Trigger Context Started');

    new UnitEventTriggerHandler().run();

    CustomLogger.save();
}