trigger FeedItemTrigger on FeedItem (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    System.debug('FeedItem Trigger Context Started');

    new FeedItemTriggerHandler().run();

    CustomLogger.save();
}