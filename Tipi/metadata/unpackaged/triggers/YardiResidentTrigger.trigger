trigger YardiResidentTrigger on Yardi_Data_Resident__c (after insert) {

    System.debug('Yardi_Data_Resident__c Trigger Context Started');

    new YardiResidentTriggerHandler().run();

    CustomLogger.save();
}