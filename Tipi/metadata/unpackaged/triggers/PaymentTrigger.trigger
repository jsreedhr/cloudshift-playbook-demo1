trigger PaymentTrigger on asp04__Payment__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

	System.debug('asp04__Payment__c Trigger Context Started');

	new PaymentTriggerHandler().run();

	CustomLogger.save();
}