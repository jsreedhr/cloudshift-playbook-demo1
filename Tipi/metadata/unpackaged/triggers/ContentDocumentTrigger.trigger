trigger ContentDocumentTrigger on ContentDocument (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

	System.debug('ContentDocument Trigger Context Started');

	new ContentDocumentTriggerHandler().run();

	CustomLogger.save();
}