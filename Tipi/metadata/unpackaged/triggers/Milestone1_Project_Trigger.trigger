trigger Milestone1_Project_Trigger on Milestone1_Project__c (before update, before delete, before insert ) {

    System.debug('Milestone1_Project__c Trigger Context Started');

    if( Trigger.isUpdate ){
    	//TODO can we delete this?
        Milestone1_Project_Trigger_Utility.handleProjectUpdateTrigger(Trigger.new);
    }
    else if( Trigger.isDelete ) {
    	//cascades through milestones
        Milestone1_Project_Trigger_Utility.handleProjectDeleteTrigger(Trigger.old);
    }
    else if( Trigger.isInsert ) {
    	//checks for duplicate names
        Milestone1_Project_Trigger_Utility.handleProjectInsertTrigger(Trigger.new );
    }
}